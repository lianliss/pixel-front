# pixel-front

## 2.9.0

### Minor Changes

- add shop

## 2.8.1

### Patch Changes

- confirm wallet creation

## 2.8.0

### Minor Changes

- Add Hello Starter

## 2.7.0

### Minor Changes

- 0c9101f: remove telegram actions

### Patch Changes

- 0c9101f: refactor wallet connect
- add entry screen

## 2.6.0

### Minor Changes

- add unit chain contracts

### Patch Changes

- 782fa03: add disposable NFT
- 9bc7b55: cancel wallet connection

## 2.5.0

### Minor Changes

- 466afa6: zar gates miner
- 867a285: lottery page

## 2.4.1

### Patch Changes

- fix sgb claimer

## 2.4.0

### Minor Changes

- add skale claimer & payable

## 2.3.1

### Patch Changes

- update achievements by groups

## 2.3.0

### Minor Changes

- add lottery & bounty

## 2.2.1

### Patch Changes

- update sidebar

## 2.2.0

### Minor Changes

- add swap

## 2.1.0

### Minor Changes

- add localization

## 2.0.1

### Patch Changes

- setup wagmi
- 222978b: versions added
