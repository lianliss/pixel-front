import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import { resolve } from 'path';
// import vitePluginRequire from "vite-plugin-require";
import vitePluginCommonJs from "vite-plugin-commonjs";
import fs from 'fs';
import path from 'path';

const packageJson = JSON.parse(fs.readFileSync(path.resolve('package.json')));

export default () => {
    process.env.VITE_VERSION = packageJson.version;
    process.env.VITE_BUILD_ID = new Date().getTime().toString();

    return defineConfig({
        plugins: [
          react(),
          vitePluginCommonJs(),
        ],
        resolve: {
            alias: [
                { find: 'app', replacement: resolve(__dirname, 'app') },
                { find: 'assets', replacement: resolve(__dirname, 'app/assets') },
                { find: 'root', replacement: resolve(__dirname, 'app/root') },
                { find: 'utils', replacement: resolve(__dirname, 'app/utils') },
                { find: 'const', replacement: resolve(__dirname, 'app/const') },
                { find: 'schemas', replacement: resolve(__dirname, 'app/const/schemas') },
                { find: 'styles', replacement: resolve(__dirname, 'styles') },
                { find: 'lib', replacement: resolve(__dirname, 'app/lib') },
                { find: 'ui', replacement: resolve(__dirname, 'app/lib/ui') },
                { find: 'data', replacement: resolve(__dirname, 'app/lib/data') },
                { find: 'calls', replacement: resolve(__dirname, 'app/calls') },
                { find: 'icons', replacement: resolve(__dirname, 'app/utils/icons') },
                { find: 'slices', replacement: resolve(__dirname, 'app/store/slices') },
                { find: 'services', replacement: resolve(__dirname, 'app/services') },
                { find: 'shared', replacement: resolve(__dirname, 'app/shared') },
                { find: '@ui-kit', replacement: resolve(__dirname, 'app/shared/ui') },
                { find: '@cfg', replacement: resolve(__dirname, 'app/config') },
                { find: '@chain', replacement: resolve(__dirname, 'app/shared/chain') },
                { find: 'https', replacement: resolve(__dirname, 'node_modules/https-browserify') },
                { find: 'fs', replacement: resolve(__dirname, 'node_modules/fs-extra') },
            ],
            extensions: ['.js', '.jsx', '.ts', '.tsx', '.scss'],
        },
        // build: {
        //   commonjsOptions: { transformMixedEsModules: true } // Change
        // },
        optimizeDeps: {
            include: ['**/*.scss'], // Include all .scss files
        },
        css: {
            modules: {
                // Enable CSS Modules for all .scss files
                localsConvention: 'dashes',
            },
        },
        server: {
            port: 3000,
        },
        // build: {
        //     minify: false,
        //     terserOptions: {
        //         compress: false,
        //         mangle: false,
        //     },
        // },
    })
};
