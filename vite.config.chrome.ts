import { resolve } from 'path';
import { mergeConfig, defineConfig, BuildOptions } from 'vite';
import { crx, ManifestV3Export } from '@crxjs/vite-plugin';
import baseConfig from './vite.config'
import manifest from './manifest.json';
import pkg from './package.json';

export const baseManifest = {
    ...manifest,
    version: pkg.version,
    // ...(isDev ? devManifest : {} as ManifestV3Export),
    // ...(localize ? {
    //     name: '__MSG_extName__',
    //     description: '__MSG_extDescription__',
    //     default_locale : 'en'
    // } : {})
} as ManifestV3Export

export const baseBuildOptions: BuildOptions = {
    sourcemap: false,
    emptyOutDir: true
}

const outDir = resolve(__dirname, 'dist_chrome');

export default mergeConfig(
  baseConfig(),
  defineConfig({
    plugins: [
      crx({
        manifest: {
          ...baseManifest,
          // background: {
          //   service_worker: 'src/pages/background/index.ts',
          //   type: 'module'
          // },
        } as ManifestV3Export,
        browser: 'chrome',
        contentScripts: {
          injectCss: true,
        }
      })
    ],
    build: {
      ...baseBuildOptions,
      outDir
    },
  })
)
