import '@types/telegram-web-app';
import type { TypeBackground as BaseTypeBackground } from '@mui/material/styles';

declare module '*.module.css' {
  const classes: { readonly [key: string]: string };
  export default classes;
}

declare module '*.module.scss' {
  const classes: { readonly [key: string]: string };
  export default classes;
}

declare global {
  interface ChildrenProps {
    children: React.ReactNode;
  }
}

declare module '@mui/material/styles/createPalette' {
  interface TypeBackground extends BaseTypeBackground {
    back1: string;
    back2: string;
    back3: string;
    back4: string;

    /**
     * @deprecated
     */
    black2: string;
  }
}
