# 🚀 Hello Pixel frontend

## Requirements

**Nodejs** version 16.14.2

## Install

```shell
npm i
```

## Run Developer Mode

```shell
npm run dev
```

## Build

```shell
npm run build
```