'use strict';
import { STORAGE_URL } from '@cfg/config.ts';

export const getStorageUrl = (url) => new URL(url, STORAGE_URL);
