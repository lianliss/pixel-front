'use strict';
import React from 'react';
import { useSelector } from 'react-redux';
import { classNames as cn } from 'utils/index';
import { Button as BluePrintButton, Icon } from '@blueprintjs/core';
import { IS_TELEGRAM } from '@cfg/config.ts';

import styles from './styles.module.scss';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Sidebar } from 'app/widget/Sidebar/Sidebar';
import { $sidebarState, toggleSidebar } from 'app/widget/Sidebar/sidebar.store';
import { IS_DECOR_ENABLED, IS_MOBILE, IS_MOBILE_RELATIVE_SIDEBAR } from '@cfg/app';
import { useUnit } from 'effector-react';
import Header from 'shared/layout/Page/Header/Header';
import { adaptiveSelector } from 'app/store/selectors';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import { DynamicDecor } from 'shared/decor/DynamicDecor';

const StyledRoot = styled(Box)(({ theme }) => ({
  backgroundColor: theme.palette.background.default,
}));

function Page({ children, match, title }) {
  const account = useAccount();
  const { isConnected } = account;
  const sidebarState = useUnit($sidebarState);

  const adaptive = useSelector(adaptiveSelector);

  return (
    <StyledRoot
      className={cn(styles.root, 'page-container', adaptive && 'adaptive')}
      style={{
        flexDirection: IS_MOBILE && IS_MOBILE_RELATIVE_SIDEBAR ? 'row-reverse' : 'row',
        position: 'relative',
      }}
    >
      {IS_DECOR_ENABLED && <DynamicDecor />}

      <div className={styles.sidebar__area}>
        <Sidebar match={match} />
      </div>

      <div className={styles.content}>
        <div className={styles.header__area}>
          {!IS_TELEGRAM && (
            <Header
              setIsMenuOpen={(v) => {
                toggleSidebar();
              }}
              isMenuOpen={sidebarState.openState}
            />
          )}
        </div>
        <div className={styles.content__area}>
          <div className='page'>
            {IS_TELEGRAM && isConnected && sidebarState.mode === 'temporary' && (
              <BluePrintButton
                icon={<Icon icon={sidebarState.openState ? 'cross' : 'menu'} size={20} />}
                onClick={() => toggleSidebar()}
                className={'page-menu-button'}
                minimal
              />
            )}

            <div
              className={cn('page-content', adaptive && sidebarState.openState && 'menu-open')}
              style={{ position: 'relative' }}
            >
              {children || <>Empty page</>}
            </div>
          </div>
        </div>
      </div>
    </StyledRoot>
  );
}

export default Page;
