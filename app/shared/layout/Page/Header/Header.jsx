'use strict';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import logo from 'styles/svg/logo_small.svg';
import { useNavigate } from 'react-router-dom';
import {
  FormGroup,
  ControlGroup,
  InputGroup,
  Button as BluePrintButton,
  Icon,
  Spinner,
  Popover,
  Menu,
  MenuItem,
} from '@blueprintjs/core';
import Button from 'ui/deprecated/ButtonDeprecated';
import routes from 'const/routes.tsx';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import toaster from 'services/toaster';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTranslation } from 'react-i18next';
import { IS_DEVELOP, IS_STAGE } from '@cfg/config';
import { adaptiveSelector } from 'app/store/selectors';
import { showConnectWallet } from 'app/widget/Connect/ConnectWallet.store';

function Header({ isMenuOpen, setIsMenuOpen }) {
  const account = useAccount();
  const { accountAddress, isConnected } = account;

  const { t } = useTranslation('common');

  const navigate = useNavigate();
  const adaptive = useSelector(adaptiveSelector);
  const [isConnecting, setIsConnecting] = React.useState(false);

  const onLogoClick = () => {
    navigate(routes.dashboard.path);
  };

  const onWalletConnect = () => {
    // connectToWalletModal();
    showConnectWallet();

    // setIsConnecting(true);
    //
    // connectWallet().then(data => {
    //   setIsConnecting(false);
    // }).catch(error => {
    //   setIsConnecting(false);
    // });
  };

  const connectText = accountAddress
    ? accountAddress.slice(0, 6) + '...' + accountAddress.slice(accountAddress.length - 4)
    : 'Connect Wallet';

  const userMenu = (
    <Menu>
      <MenuItem text='Support' disabled icon='chat' />
    </Menu>
  );

  return (
    <div className='header'>
      <div className={'header-left'}>
        {(!IS_STAGE || !IS_DEVELOP) && adaptive && (
          <BluePrintButton
            icon={<Icon icon={isMenuOpen ? 'cross' : 'menu'} size={20} />}
            onClick={() => setIsMenuOpen(!isMenuOpen)}
            className={'header-round header-menu-icon'}
            minimal
          />
        )}
      </div>
      <div className={'header-right'}>
        <Button icon={'box'} minimal onClick={() => navigate(routes.airdrop.path)}>
          Airdrop
        </Button>
        {isConnected ? (
          <CopyToClipboard
            text={accountAddress}
            className='copy-text'
            onCopy={() =>
              toaster.show({
                intent: 'warning',
                message: (
                  <>
                    Address <b>{accountAddress}</b>
                    <br />
                    copied to clipboard
                  </>
                ),
                icon: 'clipboard',
              })
            }
          >
            <Button icon={'antenna'} minimal>
              {connectText}
            </Button>
          </CopyToClipboard>
        ) : (
          <Button icon={'antenna'} onClick={onWalletConnect} primary minimal>
            {t('Connect Wallet')}
          </Button>
        )}
      </div>
    </div>
  );
}

export default Header;
