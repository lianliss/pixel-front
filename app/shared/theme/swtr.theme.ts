import createTheme from '@mui/material/styles/createTheme';
import { baseLightTheme } from './base-light.theme.ts';
import { darken } from '@mui/material/styles';

export const swtrTheme = createTheme(baseLightTheme, {
  palette: {
    mode: 'light',
    primary: {
      main: '#8527D4',
      contrastText: '#ffffff',
    },
    text: {
      primary: '#000000',
      secondary: '#4892ff',
    },
  },
});
