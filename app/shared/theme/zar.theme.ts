import createTheme from '@mui/material/styles/createTheme';
import { skaleTheme } from './skale.theme.ts';

export const zerGatesTheme = createTheme(skaleTheme, {
  palette: {
    mode: 'dark',
    primary: {
      main: '#01DEEC',
      contrastText: '#000000',
    },
  },
});
