import createTheme, { ThemeOptions } from '@mui/material/styles/createTheme';
import { baseDarkTheme } from './base-dark.theme.ts';

const skalePalette: ThemeOptions['palette'] = {
  mode: 'dark',
  primary: {
    main: '#FFFFFF',
    contrastText: '#000000',
  },
  success: {
    main: '#A3F8DA',
    contrastText: '#000000',
  },
  background: {
    default: '#001529',
    paper: '#000f1c',
    back2: '#000f1c',
    back3: '#09203C',
  },
  text: {
    primary: '#ffffff',
    secondary: '#8abbff',
  },
  divider: '#ffffff12',
};
export const skaleTheme = createTheme(baseDarkTheme, {
  palette: skalePalette,
  components: {
    MuiIconButton: {
      styleOverrides: {
        colorPrimary: {
          [`&.IconButton_outlined`]: {
            background: '#ffffff',

            '& svg': {
              color: '#000000',
            },

            [`&.Mui-disabled`]: {
              opacity: 0.5,

              '& svg': {
                opacity: 0.3,
              },
            },
          },
        },
      },
    },
  },
});
