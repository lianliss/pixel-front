import createTheme, { Theme } from '@mui/material/styles/createTheme';
import { TypeBackground } from '@mui/material/styles/createPalette';
import { components, typography, shape, breakpoints } from './base.params.ts';

const paletteBackground: TypeBackground = {
  default: '#001529',
  paper: '#09203C',
  black2: '#061930',
  back4: '#8527D41A',
  back3: '#09203C',
  back2: '#061930',
  back1: '#011324',
};

export const baseDarkTheme = createTheme({
  palette: {
    mode: 'dark',
    primary: {
      main: '#8527D4',
      contrastText: '#ffffff',
      dark: '#8527D4',
    },
    secondary: {
      main: '#4E99DE',
      contrastText: '#ffffff',
    },
    success: {
      main: '#07AA34',
      contrastText: '#ffffff',
    },
    background: paletteBackground,
    text: {
      primary: '#ffffff',
      secondary: '#8abbff',
    },
  } as any,
  typography,
  components,
  shape,
  breakpoints,
});
