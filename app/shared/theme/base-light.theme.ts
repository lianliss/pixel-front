import createTheme from '@mui/material/styles/createTheme';
import { alpha, darken } from '@mui/material/styles';
import { components, typography, shape, breakpoints } from './base.params.ts';
import { TypeBackground } from '@mui/material/styles/createPalette';

const paletteBackground: TypeBackground = {
  default: '#ffffff',
  paper: '#ffffff',
  black2: darken('#ffffff', 0.1),
  back1: darken('#ffffff', 0.3),
  back2: darken('#ffffff', 0.3),
  back3: darken('#ffffff', 0.3),
  back4: darken('#ffffff', 0.3),
};

export const baseLightTheme = createTheme({
  palette: {
    mode: 'light',
    primary: {
      main: '#8527D4',
      contrastText: '#ffffff',
    },
    success: {
      main: '#07AA34',
      contrastText: '#ffffff',
    },
    background: paletteBackground,
    divider: alpha('#000000', 0.12),
    text: {
      primary: '#000000',
      secondary: '#8abbff',
    },
  },
  typography,
  components,
  shape,
  breakpoints,
});
