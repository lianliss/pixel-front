import createTheme from '@mui/material/styles/createTheme';
import { baseDarkTheme } from './base-dark.theme.ts';

export const flareTheme = createTheme(baseDarkTheme, {
  palette: {
    mode: 'dark',
    primary: {
      main: '#fb0055',
      contrastText: '#ffffff',
    },
    background: {
      default: '#001529',
      paper: '#001a32',
    },
    text: {
      primary: '#ffffff',
      secondary: '#8abbff',
    },
  },
});
