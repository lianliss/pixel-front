import createTheme from '@mui/material/styles/createTheme';
import { baseDarkTheme } from './base-dark.theme.ts';

export const buddaTheme = createTheme(baseDarkTheme, {
  palette: {
    mode: 'dark',
    primary: {
      main: '#4CAE2BFF',
      contrastText: '#ffffff',
    },
  },
});
