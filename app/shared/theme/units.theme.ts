import createTheme from '@mui/material/styles/createTheme';
import { alpha } from '@mui/system/colorManipulator';
import { baseDarkTheme } from './base-dark.theme.ts';

export const unitsTheme = createTheme(baseDarkTheme, {
  components: {
    MuiPaper: {
      styleOverrides: {
        outlined: ({ theme }) => ({
          border: `solid 1px ${alpha(theme.palette.primary.main, 0.3)}`,
        }),
      },
    },
  },
  palette: {
    mode: 'dark',
    primary: {
      main: '#9FE0C1',
      contrastText: '#000000',
    },
    secondary: {
      main: '#EDCA5D',
      contrastText: '#000000',
    },
    background: {
      default: '#000000',
      paper: '#101010',
      back2: '#000f1c',
      back3: '#09203C',
    },
    text: {
      primary: '#ffffff',
      secondary: '#8abbff',
    },
    divider: '#ffffff12',
  },
});
