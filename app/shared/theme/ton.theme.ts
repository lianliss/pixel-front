import createTheme from '@mui/material/styles/createTheme';
import { alpha } from '@mui/system/colorManipulator';
import { baseDarkTheme } from './base-dark.theme.ts';

export const tonTheme = createTheme(baseDarkTheme, {
  components: {
    MuiPaper: {
      styleOverrides: {
        outlined: ({ theme }) => ({
          border: `solid 1px ${alpha(theme.palette.primary.main, 0.3)}`,
        }),
      },
    },
  },
  palette: {
    mode: 'dark',
    primary: {
      main: '#0098EA',
      contrastText: '#FFFFFF',
    },
    secondary: {
      main: '#4E99DE',
      contrastText: '#000000',
    },
    background: {
      default: '#000000',
      paper: '#101010',
      back2: '#1E2337',
      back3: '#09203C',
    },
    text: {
      primary: '#ffffff',
      secondary: '#8abbff',
    },
    divider: '#ffffff12',
  },
});
