import createTheme from '@mui/material/styles/createTheme';
import { baseDarkTheme } from './base-dark.theme.ts';

export const polygonTheme = createTheme(baseDarkTheme, {
  palette: {
    mode: 'dark',
    primary: {
      main: '#8527D4',
      contrastText: '#ffffff',
    },
    background: {
      default: '#000000',
      paper: '#1D1C20',
      back2: '#27262C',
    },
    text: {
      primary: '#ffffff',
      secondary: '#8abbff',
    },
  },
  components: {
    MuiInputBase: {
      styleOverrides: {
        root: {
          borderRadius: 8,
          background: 'rgb(0,0,0)',
          border: 'none',
        },
      },
    },
    MuiInput: {
      styleOverrides: {
        root: {
          padding: 8,
          borderRadius: 8,
          fontSize: 14,
          color: 'rgb(255,255,255)',
          gap: 7,
          background: 'rgb(0,0,0)',
          boxShadow: '0px 1px 0px 0px rgba(255, 255, 255, 0.03) inset',
        },
      },
    },
  },
});
