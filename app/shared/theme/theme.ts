import Provider from '@mui/material/styles/ThemeProvider';
import { Chain } from 'services/multichain/chains.js';
import { sgbTheme } from './sgb.theme.ts';
import { flareTheme } from './flare.theme.ts';
import { swtrTheme } from './swtr.theme.ts';
import { skaleTheme } from './skale.theme.ts';
import { polygonTheme } from './polygon.theme.ts';
import { unitsTheme } from './units.theme.ts';
import { baseDarkTheme } from './base-dark.theme.ts';
import { tonTheme } from "./ton.theme.ts";

export const ThemeProvider = Provider;

export const THEME_MAP = {
  [Chain.SONGBIRD]: sgbTheme,
  // [Chain.SONGBIRD]: swtrTheme,
  // [Chain.SONGBIRD]: unitsTheme,
  [Chain.COSTON2]: flareTheme,
  [Chain.FLARE]: flareTheme,
  [Chain.SWISSTEST]: swtrTheme,
  [Chain.SKALE_TEST]: skaleTheme,
  [Chain.SKALE]: skaleTheme,
  // [Chain.SKALE]: swtrTheme,
  [Chain.POLYGON_MAINNET]: polygonTheme,
  [Chain.UNITS]: unitsTheme,
  [Chain.TON]: tonTheme,
  default: baseDarkTheme,
};
