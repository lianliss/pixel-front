import type { TypographyOptions } from '@mui/material/styles/createTypography';
import type { Components } from '@mui/material/styles';
import { Theme } from '@mui/material/styles/createTheme';
import { alpha, darken } from '@mui/system/colorManipulator';
import { Breakpoints } from '@mui/material';
import { BreakpointsOptions } from '@mui/system/createTheme/createBreakpoints';

const typography: TypographyOptions = {
  fontSize: 14,
  fontFamily: 'Inter, sans-serif',
  h1: {
    fontSize: 24,
  },
  h2: {
    fontSize: 18,
  },
  h3: {
    fontSize: 16,
  },
  h4: {
    fontSize: 14,
  },
  h5: {},
  h6: {},
  subtitle1: {},
  subtitle2: {},
  body1: {},
  body2: {},
  button: {},
  caption: {
    fontSize: 12,
  },
  overline: {},
};

const shape = {
  borderRadius: 14,
};

const iOSBoxShadow =
  '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)';

const components: Components<Theme> = {
  MuiContainer: {
    styleOverrides: {
      maxWidthXs: ({ theme }) => ({
        [theme.breakpoints.up('xs')]: {
          maxWidth: 390,
        },
      }),
    },
  },
  MuiButton: {
    styleOverrides: {
      root: {
        textTransform: 'capitalize',
        borderRadius: 6,
      },
      sizeSmall: {
        height: 36,
      },
      sizeMedium: {
        height: 40,
      },
      sizeLarge: {
        height: 48,
        lineHeight: 1.4,
      },
    },
  },

  MuiInput: {
    styleOverrides: {
      root: {
        padding: 8,
        borderRadius: 8,
        fontSize: 14,
        gap: 7,
        background: 'rgba(133, 39, 212, 0.1)',
        boxShadow: '0px 1px 0px 0px rgba(255, 255, 255, 0.03) inset',
      },
    },
  },

  MuiSelect: {
    styleOverrides: {
      root: {},
    },
  },

  MuiInputBase: {
    styleOverrides: {
      root: {
        borderRadius: 8,
        background: 'rgba(133, 39, 212, 0.1)',
        border: 'none',
      },
    },
  },

  MuiOutlinedInput: {
    styleOverrides: {
      root: {
        borderRadius: 8,
      },
      notchedOutline: {
        borderColor: 'rgba(255, 255, 255, 0.03)',
      },
    },
  },

  MuiTab: {
    styleOverrides: {
      root: (opts) => ({
        padding: '6px 10px',
        minWidth: '64px',
        textTransform: 'capitalize',
        fontWeight: 600,
        color: alpha(opts.theme.palette.text.primary, 0.5),
        '&.Mui-selected': { color: opts.theme.palette.text.primary },
      }),
    },
  } as any,

  MuiPaper: {
    styleOverrides: {
      root: {
        backgroundImage: 'none',
      },
      elevation2: ({ theme }) => ({
        border: `solid 1px ${darken(theme.palette.primary.main, 0.2)}`,
      }),
      elevation4: ({ theme }) => ({
        border: `solid 4px ${darken(theme.palette.primary.main, 0.2)}`,
      }),
    },
  },

  MuiIconButton: {
    styleOverrides: {
      colorPrimary: ({ theme }) => ({
        [`&.IconButton_outlined`]: {
          border: `1px solid ${theme.palette.secondary.main}`,
          background: theme.palette.background.default,

          '& svg': {
            color: theme.palette.secondary.main,
          },

          [`&.Mui-disabled`]: {
            opacity: 0.5,

            '& svg': {
              opacity: 0.3,
            },
          },
        },
      }),
      sizeMedium: {
        padding: '7px',
      },
      sizeLarge: {
        padding: '16px',

        '& > svg': {
          fontSize: 34,
        },
      },
    },
  },

  MuiFormLabel: {
    styleOverrides: {
      root: (opts) => ({
        color: opts.theme.palette.text.primary,
        fontWeight: 600,
      }),
    },
  },

  MuiFormControlLabel: {
    styleOverrides: {
      label: {
        fontWeight: 600,
      },
    },
  },

  MuiPopover: {
    styleOverrides: {
      root: {
        zIndex: 13001,
      },
    },
  },

  MuiListItemIcon: {
    styleOverrides: {
      root: {
        minWidth: 40,
      },
    },
  },

  MuiListItemButton: {
    styleOverrides: {
      root: ({ theme }) => ({
        '&.Mui-selected': {
          backgroundColor: 'rgba(255, 255, 255, 0.1)',
        },
      }),
    },
  },

  MuiSlider: {
    styleOverrides: {
      root: ({ theme }) => ({
        boxSizing: 'border-box',
        // color: '#007bff',
        height: 16,
        padding: '0 !important',
        border: `solid 4px ${darken(theme.palette.background.default, 0.05)}`,

        '& .MuiSlider-thumb': {
          height: 16,
          width: 16,
          backgroundColor: '#fff',
          boxShadow: '0 0 2px 0px rgba(0, 0, 0, 0.1)',
          '&:focus, &:hover, &.Mui-active': {
            boxShadow: '0px 0px 3px 1px rgba(0, 0, 0, 0.1)',
            // Reset on touch devices, it doesn't add specificity
            '@media (hover: none)': {
              boxShadow: iOSBoxShadow,
            },
          },
          '&:before': {
            boxShadow:
              '0px 0px 1px 0px rgba(0,0,0,0.2), 0px 0px 0px 0px rgba(0,0,0,0.14), 0px 0px 1px 0px rgba(0,0,0,0.12)',
          },
        },
        '& .MuiSlider-valueLabel': {
          fontSize: 12,
          fontWeight: 'normal',
          top: -6,
          background: theme.palette.background.paper,
          border: `solid 1px ${theme.palette.divider}`,
          color: theme.palette.text.primary,
          '&::before': {
            display: 'none',
          },
          '& *': {
            color: '#000',
            ...theme.applyStyles('dark', {
              color: '#fff',
            }),
          },
        },
        '& .MuiSlider-track': {
          border: 'none',
          height: 8,
        },
        '& .MuiSlider-rail': {
          opacity: 0.5,
          boxShadow: 'inset 0px 0px 4px -2px #000',
          backgroundColor: '#d0d0d0',
        },
        // ...theme.applyStyles('dark', {
        //   color: '#0a84ff',
        // }),
        '& .MuiSlider-mark[data-index="5"]': {
          transform: 'translate(-4px, -50%)',
        },
      }),
      rail: {
        height: 8,
      },
    },
  },
};

const breakpoints: BreakpointsOptions = {
  values: {
    xs: 430 + 32,
    sm: 600,
    md: 900,
    lg: 1200,
    xl: 1536,
  },
};

export { typography, shape, components, breakpoints };
