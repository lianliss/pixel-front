import { useToken } from '../hooks/useToken.ts';
import { MINING_TOKENS } from 'services/multichain/initialTokens';
import { useAccount } from '../hooks/useAccount.ts';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';

export function useCoreToken() {
  const account = useAccount();

  const coreAddress = MINER_CONTRACT_ID[account.chainId]?.core;
  const miningToken = MINING_TOKENS[account.chainId];

  const token = useToken({
    address: coreAddress,
    skip: !coreAddress,
  });

  return {
    ...token,
    data: token.data ? { ...token.data, logoURI: miningToken?.logoURI } : undefined,
  };
}
