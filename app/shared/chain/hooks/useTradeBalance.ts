import { useBalance } from './useBalance.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { TRADE_TOKENS } from '@chain/hooks/useTradeToken.ts';

export function useTradeBalance({ skip }: { skip?: boolean } = {}) {
  const account = useAccount();
  const tokenData = TRADE_TOKENS[account.chainId];
  const tokenAddress = tokenData?.address;

  return useBalance({
    address: account.address,
    token: tokenAddress,
    skip: skip || !tokenAddress || !account.address,
  });
}
