import { useBalance } from './useBalance.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';

export function useCoreBalance({ skip }: { skip?: boolean } = {}) {
  const account = useAccount();
  const contractId = MINER_CONTRACT_ID[account.chainId]?.core;

  return useBalance({
    address: account.address,
    token: contractId,
    skip: skip || !contractId || !account.address,
  });
}
