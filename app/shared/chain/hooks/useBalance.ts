import { useBalance as useBalanceWagmi } from 'wagmi';
import type { Config, ResolvedRegister } from '@wagmi/core';
import type { GetBalanceData } from '@wagmi/core/query';
import { UseBalanceParameters } from 'wagmi/dist/types/hooks/useBalance';

export function useBalance<
  config extends Config = ResolvedRegister['config'],
  selectData = GetBalanceData
>({ skip, query = {}, ...parameters }: UseBalanceParameters<config, selectData>) {
  const isErc = parameters.token === '0x0000000000000000000000000000000000000000';
  const overrided = {
    ...parameters,
    query: {
      ...query,
      enabled: typeof query.enabled === 'boolean' ? query.enabled : !skip || !parameters.address,
    },
    token: isErc ? undefined : parameters.token,
    abi: isErc ? undefined : parameters.abi,
  };

  const balance = useBalanceWagmi(overrided);

  return {
    ...balance,
    isLoading: balance.isLoading,
    isFetched: balance.isFetched,
    refetch: balance.refetch,
    data: balance.data
      ? { ...balance.data, formatted: +(+balance.data.formatted).toFixed(6) }
      : balance.data,
  };
}
