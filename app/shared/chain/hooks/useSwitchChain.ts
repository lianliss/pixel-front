import { useSwitchChain as useSwitchChainWagmi } from 'wagmi';
import type { Config } from '@wagmi/core';

export function useSwitchChain() {
  const run = useSwitchChainWagmi();

  return run as {
    chains: Config['chains'];
    switchChain: (opts: { chainId: number }) => void;
    switchChainAsync: (opts: { chainId: number }) => Promise<void>;
  };
}
