import { useReadContract as useReadContractWagmi } from 'wagmi';
import type { ReadContractReturnType } from 'viem';

type IReadContractOpts<Result, Args extends unknown[]> = {
  initData?: Result;
  abi: any;
  functionName: string;
  address: string;
  args?: Args;
  skip?: boolean;
  select?: (res: any) => Result;
};

type IReadContractResult<Result, Args extends unknown[]> = {
  data?: Result;
  loading: boolean;
  refetch: (payload?: { args?: Args[] }) => Promise<Result>;
  error?: Error;
  isFetched: boolean;
  isFetching: boolean;
};

export function useReadContract<Result, Args extends unknown[]>({
  skip,
  select,
  initData,
  ...opts
}: IReadContractOpts<Result, Args>): IReadContractResult<Result, Args> {
  const readContract = useReadContractWagmi({
    ...opts,
    query: {
      enabled: !skip,
      initialData: initData,
      select: (res: ReadContractReturnType) => {
        if (select) {
          return select(res);
        }

        return res;
      },
    },
  });

  return {
    ...readContract,
    loading: readContract.isLoading,
  } as IReadContractResult<Result, Args>;
}
