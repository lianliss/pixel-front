import { useBlock, useTransactionReceipt } from 'wagmi';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain } from 'services/multichain/chains';
import { useEffect, useState } from 'react';
import { createEvent, createStore } from 'effector';
import { useUnit } from 'effector-react';

// end of store

const CONFIRMATION_KEY = (key: string, chainId: number) => `confirm-${chainId}-${key}`;

export type IWaitConfirmationsResult = {
  confirmations: number;
  isConfirmed: boolean;
  isPending: boolean;
};

export function useTransactionConfirmation(payload: {
  txHash?: string;
  cacheKey?: string;
}): IWaitConfirmationsResult {
  const [initialBlock, setInitialBlock] = useState<number | undefined>(undefined);

  const account = useAccount();
  const isEnabled = account.chainId === Chain.UNITS;

  const recipe = useTransactionReceipt({
    hash: payload.txHash,
    query: {
      enabled: isEnabled && !!payload.txHash,
      refetchInterval: 2_000,
    },
  });

  const finalizedBlock = useBlock({
    blockTag: 'finalized',
    query: {
      enabled: isEnabled & !!payload.txHash || initialBlock,
    },
  });

  const finalizedBlockNumber = finalizedBlock.data ? Number(finalizedBlock.data.number) : undefined;
  const recipeBlockNumber = recipe.data ? Number(recipe.data.blockNumber) : initialBlock;
  const confirmations =
    recipeBlockNumber > finalizedBlockNumber ? recipeBlockNumber - finalizedBlockNumber : 0;
  const isConfirmed = payload.txHash || initialBlock ? confirmations <= 0 : true;
  const isConfirmationPending =
    ((!!payload.txHash || initialBlock) && confirmations > 0) ||
    (payload.txHash && recipe.isPending);

  useEffect(() => {
    if (isEnabled && payload.cacheKey && recipeBlockNumber && recipe.data) {
      localStorage.setItem(
        CONFIRMATION_KEY(payload.cacheKey, account.chainId),
        recipeBlockNumber.toString()
      );
    }
  }, [payload.cacheKey, recipeBlockNumber, recipe.data, account.chainId]);
  useEffect(() => {
    if (isEnabled && payload.cacheKey) {
      const blockFromCache = localStorage.getItem(
        CONFIRMATION_KEY(payload.cacheKey, account.chainId)
      );

      if (blockFromCache && !Number.isNaN(+blockFromCache)) {
        setInitialBlock(Number(blockFromCache));
      }
    }
  }, [payload.cacheKey, account.chainId]);
  useEffect(() => {
    if (isEnabled && finalizedBlockNumber && initialBlock) {
      if (initialBlock < finalizedBlockNumber) {
        setInitialBlock(undefined);
      }
    }
  }, [finalizedBlockNumber, account.chainId, initialBlock]);

  return {
    confirmations: isEnabled ? confirmations : 0,
    isConfirmed: isEnabled ? isConfirmed : true,
    isPending: isEnabled ? isConfirmationPending : false,
  };
}
