import { useBalance } from './useBalance.ts';

export function useGasBalance({ address, skip }: { address: string; skip?: boolean }) {
  return useBalance({
    address,
    token: '0x0000000000000000000000000000000000000000',
    skip,
  });
}
