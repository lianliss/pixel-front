import { useToken } from '../hooks/useToken.ts';
import { useAccount } from '../hooks/useAccount.ts';
import { ZERO_ADDRESS } from '@cfg/config.ts';
import { Chain } from 'services/multichain/chains';
import {
  SGB_TOKEN,
  SKALE_TESTNET_TOKEN,
  SKALE_TOKEN,
} from 'services/multichain/token.constants.ts';

export const TRADE_TOKENS = {
  [Chain.SONGBIRD]: SGB_TOKEN,
  [Chain.SKALE_TEST]: SKALE_TESTNET_TOKEN,
  [Chain.SKALE]: SKALE_TOKEN,
  // [Chain.UNITS]: UNITS_TOKEN,
};

export function useTradeToken() {
  const account = useAccount();
  const tokenData = TRADE_TOKENS[account.chainId];
  const tokenAddress = tokenData?.address || ZERO_ADDRESS;

  const token = useToken({
    address: tokenAddress,
    skip: !tokenAddress,
  });

  return token;
}
