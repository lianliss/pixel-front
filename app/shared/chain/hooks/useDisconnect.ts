import { useDisconnect as useDisconnectWagmi } from 'wagmi';

export function useDisconnect() {
  const run = useDisconnectWagmi();

  return run;
}
