import { useReadContracts } from 'wagmi';
import ERC_20_ABI from 'lib/Wallet/api-contract/abi/erc-20.abi.ts';
import { useAccount } from './useAccount.ts';
import { TOKEN_IMAGE } from '../data/token-image.ts';

export type ITokenData = {
  symbol: string;
  address: string;
  name: string;
  image?: string;
  decimals: number;
  isCoin: boolean;
};
type Return = {
  data: ITokenData | null;
  isLoading: boolean;
  isFetched: boolean;
};

export function useToken(opts: { address?: string; skip?: boolean }): Return {
  const account = useAccount();

  const isErc20 = opts.address && opts.address !== '0x0000000000000000000000000000000000000000';

  const result = useReadContracts({
    allowFailure: false,
    contracts: [
      {
        address: opts.address,
        abi: ERC_20_ABI,
        functionName: 'decimals',
      },
      {
        address: opts.address,
        abi: ERC_20_ABI,
        functionName: 'name',
      },
      {
        address: opts.address,
        abi: ERC_20_ABI,
        functionName: 'symbol',
      },
    ],
    skip: isErc20 || opts.skip,
  }) as any;

  if (isErc20) {
    return {
      data: {
        address: opts.address,
        name: result.data?.[1] || '',
        symbol: result.data?.[2] || '',
        decimals: result.data?.[0] || 0,
        image: TOKEN_IMAGE[account.chainId]?.[opts.address.toLowerCase()],
        isCoin: false,
      },
      isFetched: result.isFetched,
      isLoading: result.isLoading,
    };
  }

  if (!isErc20 && opts.skip) {
    return {
      data: null,
      isFetched: false,
      isLoading: false,
    };
  }

  return {
    data: account.chainId
      ? {
          address: '0x0000000000000000000000000000000000000000',
          name: account.chain.nativeCurrency.name,
          symbol: account.chain.nativeCurrency.symbol,
          decimals: account.chain.nativeCurrency.decimals,
          image: TOKEN_IMAGE[account.chainId]?.coin,
          isCoin: true,
        }
      : null,
    isFetched: account.isConnected,
    isLoading: account.isConnecting,
  };
}
