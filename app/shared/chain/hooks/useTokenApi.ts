import { useWriteContract } from './useWriteContract.ts';
import wei from 'utils/wei';
import { useToken } from './useToken.ts';
import { useAccount } from './useAccount.ts';
import { useState } from 'react';
import { useSendTransaction } from 'wagmi';

const ERC_20_ABI = require('const/ABI/Erc20Token');

export function useTokenApi({ address }: { address: string }) {
  const account = useAccount();
  const { data: hash, sendTransactionAsync } = useSendTransaction();
  const write = useWriteContract({
    abi: ERC_20_ABI,
    address: address,
  });
  const token = useToken({
    address,
    skip: !account.isConnected,
  });
  const isErc20 = address !== '0x0000000000000000000000000000000000000000';

  const [loading, setLoading] = useState(false);

  const handleSendCoin = async (opts: { receiver: string; amount: number }) => {
    if (!token.data) {
      return;
    }

    setLoading(true);

    try {
      const amount = wei.to(String(opts.amount), token.data.decimals);
      await sendTransactionAsync({ to: opts.receiver, value: amount });

      setLoading(false);
    } catch (e) {
      setLoading(false);
      throw e;
    }
  };
  const handleSendErc = async (opts: { receiver: string; amount: number }) => {
    if (!token.data) {
      return;
    }

    setLoading(true);

    try {
      const amount = wei.to(Number(opts.amount), token.data.decimals);

      await write.writeContractAsync({
        functionName: 'transfer',
        args: [opts.receiver, amount],
      });

      setLoading(false);
    } catch (e) {
      setLoading(false);
      throw e;
    }
  };

  return {
    send: isErc20 ? handleSendErc : handleSendCoin,
    loading,
  };
}
