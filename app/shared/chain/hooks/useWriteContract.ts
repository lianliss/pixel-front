import { useBlock, useClient, useConfig, useWriteContract as useWriteContractWagmi } from 'wagmi';
import { UseWriteContractParameters } from 'wagmi/dist/types/hooks/useWriteContract';
import type { Config, ResolvedRegister } from '@wagmi/core';
import { waitForTransactionReceipt } from '@wagmi/core';
import type { WriteContractMutateAsync, WriteContractData } from '@wagmi/core/query';
import wait from 'utils/wait';
import { showTransactionConfirm } from '../../blocks/TransactionConfirm/transaction.store.ts';
import { estimateContractGas } from 'viem/actions';
import { formatEther, formatGwei, parseGwei } from 'viem';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain } from 'services/multichain/chains';
import { IS_CONFIRM_TX } from '@cfg/app.ts';

const GAS_CONFIRM_CHAINS = [Chain.UNITS];

const MAX_VALUE = {
  [Chain.UNITS]: 0.005,
  [Chain.SONGBIRD]: 0.1,
};
const BASE_FEE = {
  [Chain.UNITS]: '1',
  [Chain.SONGBIRD]: '30',
};
const BASE_FEE_PRIORITY = {
  [Chain.UNITS]: '1',
  [Chain.SONGBIRD]: '1',
};

export function useWriteContract<
  config extends Config = ResolvedRegister['config'],
  context = unknown
>(
  parameters: UseWriteContractParameters<config, context> & {
    abi: any[];
    address: string;
    ignoreRecipe?: boolean;
    waitTime?: number;
  }
) {
  const account = useAccount();
  const client = useClient();
  const writeContract = useWriteContractWagmi(parameters);
  const config = useConfig();

  const writeContractAsync: Omit<
    WriteContractMutateAsync<config, context>,
    'abi' | 'address'
  >['writeContractAsync'] = (async ({ overrides, ...opts }): Promise<WriteContractData> => {
    const vars = {
      ...opts,
      abi: parameters.abi,
      address: parameters.address,
      // maxFeePerGas: parseGwei('20'),
      // maxPriorityFeePerGas: parseGwei('2'),
      nonce: opts.nonce,
      value: overrides?.value,
    };

    const tx = await writeContract.writeContractAsync(vars);

    if (!parameters.ignoreRecipe) {
      try {
        const receipt = await waitForTransactionReceipt(config, {
          confirmations: 1,
          hash: tx,
          pollingInterval: 1_000,
        });

        return receipt as any;
      } catch (e) {
        console.error('[write]', e, { ...e });
        throw e;
      }
    }

    if (parameters.waitTime) {
      await wait(parameters.waitTime);
    }

    return tx;
  }) as any;

  const writeContractAsyncConfirm = async ({
    overrides,
    gas: gasOverrided,
    ...opts
  }): Promise<WriteContractData> => {
    const maxFeePerGas =
      opts.maxFeePerGas ||
      (account.chainId in BASE_FEE ? parseGwei(BASE_FEE[account.chainId]) : undefined);
    const maxPriorityFeePerGas =
      opts.maxPriorityFeePerGas ||
      (account.chainId in BASE_FEE_PRIORITY
        ? parseGwei(BASE_FEE_PRIORITY[account.chainId])
        : undefined);

    const vars = {
      ...opts,
      abi: parameters.abi,
      address: parameters.address,
      nonce: opts.nonce,
      value: overrides?.value,
      maxFeePerGas,
      maxPriorityFeePerGas,
    };

    const gas =
      gasOverrided ||
      (await estimateContractGas(client, {
        ...vars,
        account: account.accountAddress,
      }));
    const totalValue = Number(formatGwei(gas)) + Number(formatEther(overrides?.value || '0'));

    if (totalValue < MAX_VALUE[account.chainId] || !(account.chainId in MAX_VALUE)) {
      const tx = await writeContract.writeContractAsync({
        ...vars,
        gas,
        maxFeePerGas,
        maxPriorityFeePerGas,
      });

      return tx;
    }

    return new Promise((resolve, reject) => {
      showTransactionConfirm({
        handler: async () => {
          try {
            const tx = await writeContract.writeContractAsync({
              ...vars,
              gas,
              maxFeePerGas,
              maxPriorityFeePerGas,
            });

            return resolve(tx);
          } catch (e) {
            return reject(e);
          }
        },
        cancel: () => reject(new Error('Transaction Rejected')),
        gas: gas,
        fee: overrides?.value || 0n,
      });
    });
  };

  return {
    ...writeContract,
    writeContractAsync: GAS_CONFIRM_CHAINS.includes(account.chainId)
      ? writeContractAsyncConfirm
      : writeContractAsync,
  };
}
