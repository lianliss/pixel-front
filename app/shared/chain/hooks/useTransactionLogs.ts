import { TransactionReceipt } from 'viem';
import { useCallback, useMemo } from 'react';
import { ethers } from 'ethers6';
import { InterfaceAbi } from 'ethers6/src.ts/abi/interface.ts';

type Log = { name: string; args: Array<string | number | bigint | boolean> };

export function useTransactionLogs({
  receipt,
  abi,
}: {
  receipt?: TransactionReceipt;
  abi: InterfaceAbi;
}) {
  const abiInterface = useMemo(() => new ethers.Interface(abi), [abi]);

  const parse = useCallback(
    (r: TransactionReceipt) => {
      return r.logs
        .map((log) => abiInterface.parseLog(log))
        .filter(Boolean)
        .map((log): Log => {
          return {
            name: log.name,
            args: [...log.args],
          };
        });
    },
    [abiInterface]
  );

  const result = useMemo(() => {
    if (receipt) {
      return parse(receipt);
    }

    return undefined;
  }, [parse, receipt]);

  return {
    data: result,
    parse,
  };
}
