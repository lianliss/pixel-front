import { useGasBalance } from '@chain/hooks/useGasBalance.ts';
import { useUnit } from 'effector-react';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { BALANCE_CONTRACT_ID } from 'lib/Wallet/api-contract/wallet.contracts.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import BALANCES_ABI from 'lib/Wallet/api-contract/abi/balances.abi.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';
import { useToken } from '@chain/hooks/useToken.ts';
import { tokenPricesStore } from 'lib/Wallet/api-server/oracle.store.ts';
import { useMemo } from 'react';
import { ZERO_ADDRESS } from '@cfg/config.ts';
import wei from 'utils/wei';

export function useSumBalance({
  address,
  skip,
  tokens = [],
}: {
  address: string;
  skip?: boolean;
  tokens: Array<{ address: string; decimals: number }>;
}) {
  // const account = useAccount();
  // const tokens = (DISPLAY_TOKENS[account.chainId] || []).filter(el => el.isPrice);

  const account = useAccount();
  const balanceContractId = BALANCE_CONTRACT_ID[account.chainId];

  const tokenBalances = useReadContract({
    initData: [],
    abi: BALANCES_ABI,
    address: balanceContractId,
    skip: !balanceContractId,
    functionName: 'getBalances',
    args: [account.accountAddress, tokens.map((el) => el.address)],
  });

  const gasBalance = useBalance({
    address: address,
    skip,
  });
  const gasToken = useToken({
    skip,
  });

  const tokensPrices = useUnit(tokenPricesStore);

  let balance = useMemo(() => {
    let value = 0;
    const gasPriceRate = tokensPrices[account.chainId]?.[ZERO_ADDRESS] || 0;

    value += (gasBalance.data?.formatted || 0) * gasPriceRate;

    if (tokens.length === tokenBalances.data.length) {
      for (let i = 0; i < tokens.length; i++) {
        const token = tokens[i];
        const tokenEthersBalance = tokenBalances.data[i];
        const tokenBalance = wei.from(tokenEthersBalance, token.decimals);
        const tokenRate = tokensPrices[account.chainId]?.[token.address.toLowerCase()] || 0;

        value += tokenBalance * tokenRate;
      }
    }

    return value;
  }, [
    gasToken.data?.symbol,
    gasBalance.data,
    tokensPrices,
    tokenBalances,
    tokens,
    account.chainId,
  ]);

  return {
    data: {
      value: balance,
    },
  };
}
