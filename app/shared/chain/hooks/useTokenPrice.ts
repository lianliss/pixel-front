import { useAccount } from './useAccount.ts';
import { loadOraclePrices, useOracleTokenPrice } from 'lib/Wallet/api-server/oracle.store.ts';
import { useUnit } from 'effector-react';

export function useTokenPrice({ address }: { address?: string }): {
  data: number | undefined;
  loading: boolean;
} {
  const { chainId } = useAccount();
  const price = useOracleTokenPrice({
    address: address || '0x0000000000000000000000000000000000000000',
    chainId,
  });
  const loading = useUnit(loadOraclePrices.pending);

  return {
    loading,
    data: price,
  };
}
