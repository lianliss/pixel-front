import { useAccount as useAccountWagmi } from 'wagmi';
import { UseAccountReturnType } from 'wagmi/src/hooks/useAccount.ts';

export function useAccount(): UseAccountReturnType & { accountAddress?: string } {
  const account = useAccountWagmi() as UseAccountReturnType;

  return {
    ...account,
    accountAddress: account.address,
  };
}
