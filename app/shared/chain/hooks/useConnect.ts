import { useConnect as useConnectWagmi } from 'wagmi';

export function useConnect() {
  const { connect } = useConnectWagmi();

  return connect;
}
