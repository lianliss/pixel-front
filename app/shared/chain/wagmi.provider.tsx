import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import React from 'react';
import { WagmiProvider as BaseWagmiProvider } from 'wagmi';
import { wagmiConfig } from './wagmi.ts';

const queryClient = new QueryClient();

function WagmiProvider(props: React.PropsWithChildren) {
  return (
    <BaseWagmiProvider config={wagmiConfig}>
      <QueryClientProvider client={queryClient}>{props.children}</QueryClientProvider>
    </BaseWagmiProvider>
  );
}

export default WagmiProvider;
