import { Chain } from 'services/multichain/chains.js';
import { DISPLAY_TOKENS } from 'services/multichain/initialTokens.js';
import skaleSFuel from 'assets/img/token/skale-testnet/gas.png';
import unitGas from 'assets/img/token/unit/native.png';

const TOKEN_IMAGE = {
  [Chain.SONGBIRD]: {
    coin: require('assets/svg/songbird.svg'),
  },
  [Chain.SKALE_TEST]: {
    coin: skaleSFuel,
  },
  [Chain.SKALE]: {
    coin: skaleSFuel,
  },
  [Chain.UNITS]: {
    coin: unitGas,
  },
};

Object.entries(DISPLAY_TOKENS).map(([chainId, tokens]) => {
  if (!TOKEN_IMAGE[chainId]) {
    TOKEN_IMAGE[chainId] = {};
  }

  for (const token of tokens) {
    const tokenAddress = (
      token.address || '0x0000000000000000000000000000000000000000'
    ).toLowerCase();

    if (TOKEN_IMAGE[chainId][tokenAddress]?.coin) {
      continue;
    }

    TOKEN_IMAGE[chainId][tokenAddress] = token.logoURI;
  }
});

export { TOKEN_IMAGE };
