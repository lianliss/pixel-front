import { UseAccountReturnType } from 'wagmi/src/hooks/useAccount.ts';

export function createTxUrl(account: UseAccountReturnType, txHash: string): string | null {
  const explorer = account.chain?.blockExplorers?.default;

  if (explorer?.url) {
    return `${explorer.url}/tx/${txHash}`;
  }

  return null;
}
