import { IFtToken } from '@chain/tokens/types.ts';
import { Chain } from 'services/multichain/chains';
import { BASE_PXL_IMAGE, BASE_USDC_IMAGE, BASE_USDT_IMAGE } from '@chain/tokens/base.ts';

export const COSTON2_WRAPPED_FLARE: IFtToken = {
  name: 'Wrapped Coston2Flare',
  address: '0xC67DCE33D7A8efA5FfEB961899C73fe01bCe9273',
  symbol: 'WC2FLR',
  decimals: 18,
  chainId: Chain.COSTON2,
};

export const COSTON2_PXL_SHARD: IFtToken = {
  name: 'Pixel Shard',
  address: '0xa6FfF50C671023eCbd83F4a259bB0fDA20faEbC4',
  symbol: 'PXLs',
  decimals: 18,
  chainId: Chain.COSTON2,
  logoURI: BASE_PXL_IMAGE,
};

export const COSTON2_USDT: IFtToken = {
  name: 'USDT',
  address: '0x02518f90d87826F2E67d3Cf5D8b45882f8874Ab3',
  symbol: 'USDT',
  decimals: 18,
  chainId: Chain.COSTON2,
  logoURI: BASE_USDT_IMAGE,
};

export const COSTON2_USDC: IFtToken = {
  name: 'USD Coin',
  address: '0x4Df23a8DBB77aCD6Bdb05dbd5D48b4781FbF952E',
  symbol: 'USDC',
  decimals: 18,
  chainId: Chain.COSTON2,
  logoURI: BASE_USDC_IMAGE,
};
