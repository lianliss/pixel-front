import { Chain } from 'services/multichain/chains';
import { IFtToken } from '@chain/tokens';
import { BASE_USDC_IMAGE } from '@chain/tokens/base.ts';

export const POLYGON_USDC: IFtToken = {
  name: 'USD Coin',
  address: '0x3c499c542cEF5E3811e1192ce70d8cC03d5c3359',
  symbol: 'USDC',
  decimals: 6,
  chainId: Chain.POLYGON_MAINNET,
  logoURI: BASE_USDC_IMAGE,
};
