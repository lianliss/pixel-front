import { Chain } from 'services/multichain/chains';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { IFtToken } from '@chain/tokens';
import { BASE_PXL_IMAGE } from '@chain/tokens/base.ts';

export const SKALE_SKL: IFtToken = {
  name: 'SKALE',
  address: '0xE0595a049d02b7674572b0d59cd4880Db60EDC50',
  symbol: 'SKL',
  decimals: 18,
  chainId: Chain.SKALE,
};
export const SKALE_PXL: IFtToken = {
  name: 'Pixel Shard',
  address: MINER_CONTRACT_ID[Chain.SKALE].core,
  symbol: 'PXLs',
  decimals: 18,
  chainId: Chain.SKALE,
  logoURI: BASE_PXL_IMAGE,
};
