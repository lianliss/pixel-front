import { Chain } from 'services/multichain/chains';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { IFtToken } from '@chain/tokens';
import { BASE_PXL_IMAGE } from '@chain/tokens/base.ts';

export const SKALETEST_SKL: IFtToken = {
  name: 'SKALE',
  address: '0x6c71319b1F910Cf989AD386CcD4f8CC8573027aB',
  symbol: 'SKL',
  decimals: 18,
  chainId: Chain.SKALE_TEST,
};
export const SKALETEST_PXL: IFtToken = {
  name: 'Pixel Shard',
  address: MINER_CONTRACT_ID[Chain.SKALE_TEST].core,
  symbol: 'PXLs',
  decimals: 18,
  chainId: Chain.SKALE_TEST,
  logoURI: BASE_PXL_IMAGE,
};
