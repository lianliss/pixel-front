import pixelImage from 'assets/img/token/common/pixel.png';

export const BASE_USDC_IMAGE =
  'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/ethereum/assets/0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48/logo.png';
export const BASE_USDT_IMAGE =
  'https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/ethereum/assets/0xdAC17F958D2ee523a2206206994597C13D831ec7/logo.png';

export const BASE_PXL_IMAGE = `${location.origin}/${pixelImage}`;
