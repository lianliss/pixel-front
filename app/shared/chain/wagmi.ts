import { http, createConfig, fallback } from 'wagmi';
import {
  songbird,
  flare,
  flareTestnet,
  skaleEuropa,
  skaleEuropaTestnet,
  polygon,
} from 'wagmi/chains';
import { metaMask } from '@wagmi/connectors';
import { swtrTestnetChain } from './transports/swtr.chain.ts';
import { swtrHttp } from './transports/swtr.transport.ts';
import { createPixelConnector } from './connectors/pixel.connector.ts';
import { unitChain } from '@chain/transports/unit.chain.ts';

export const pixelConnectorState = createPixelConnector();
export const wagmiConfig = createConfig({
  chains: [
    songbird,
    flare,
    flareTestnet,
    swtrTestnetChain,
    skaleEuropa,
    skaleEuropaTestnet,
    polygon,
    unitChain,
  ],
  // pollingInterval: 2_000,
  connectors: [pixelConnectorState, metaMask()],
  transports: {
    [songbird.id]: fallback([
      http('https://songbird-api.flare.network/ext/C/rpc'),
      // http('https://rpc.ftso.au/songbird'),
    ]),
    [flare.id]: http(),
    [flareTestnet.id]: http(),
    [swtrTestnetChain.id]: swtrHttp(),
    [skaleEuropa.id]: http(),
    [skaleEuropaTestnet.id]: http(),
    [polygon.id]: http(),
    [unitChain.id]: http(),
  },
});

// declare module 'wagmi' {
//   interface Register {
//     config: typeof wagmiConfig;
//   }
// }
//
// setTimeout(async () => {
//   if (IS_USE_PIXEL && !IS_TELEGRAM) {
//     if (pixelStorage.getPrivateKey()) {
//       const account = getAccount(wagmiConfig);
//       // console.log(connections);
//       // if (!account) {
//         // connect(wagmiConfig, { connector: pixelConnectorState }).then();
//       // }
//     }
//   }
// }, 100);

//
