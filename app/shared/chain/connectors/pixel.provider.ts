import { sign } from 'web3-eth-accounts';
import { JsonRpcProvider, Wallet, Network } from 'ethers6';
import type { Chain } from 'viem';
import type { Transport } from '@wagmi/core/dist/types/createConfig';
import { encryptDataFieldWithPublicKey } from '@swisstronik/utils';
import { numberToHex } from 'viem';
import { privateKeyToAccount } from 'viem/accounts';
import { UrlRequiredError } from 'viem';
import type { TransactionRequest } from 'ethers6/src.ts/providers/provider.ts';

export const STORAGE_KEY_CHAIN_ID = 'wallet-chainId';
let privateKey_: string | undefined;

const pixelProviderLog = (text: string) => console.info(`[pixel] - ${text}`);

export const pixelStorage = {
  savePrivateKey: (key: string) => localStorage.setItem('pk', key),
  getPrivateKey: () => {
    const value = localStorage.getItem('pk');

    if (value === 'null') {
      return undefined;
    }

    return value;
  },
  clearPrivateKey: () => localStorage.removeItem('pk'),
  refreshPrivateKey: () => {
    const storage = localStorage;

    for (const key in storage) {
      if (key.startsWith('wallet-') && key.endsWith('-privateKey') && localStorage.getItem(key)) {
        pixelStorage.savePrivateKey(localStorage.getItem(key));
        // localStorage.removeItem(key);
      }
    }
  },
};

export class PixelProvider {
  protected listeners: Record<string, Function[]> = {};
  protected account: string | undefined;
  protected chainId: number = Number(localStorage.getItem(STORAGE_KEY_CHAIN_ID)) || 19;
  //
  protected rpc: JsonRpcProvider | undefined;
  protected signer: Wallet;
  //
  protected readonly chains: readonly [Chain, ...Chain[]];
  protected readonly transports: Record<number, Transport>;

  constructor(opts: {
    chains: readonly [Chain, ...Chain[]];
    transports?: Record<number, Transport>;
  }) {
    pixelProviderLog('init');

    this.chains = opts.chains;
    this.transports = opts.transports;

    this.updatePixelClient().then();
  }

  protected async updatePixelClient() {
    const chain = this.chains.find((el) => el.id === this.chainId);
    const rpcUrl = chain?.rpcUrls.default.http[0];

    const network = new Network(chain.name, chain.id);
    this.rpc = new JsonRpcProvider(rpcUrl, network, { staticNetwork: network });

    if (privateKey_) {
      this.signer = new Wallet(privateKey_, this.rpc);
    }
  }

  public async connect(privateKey: string) {
    pixelProviderLog('connect');

    const accountAddress = privateKeyToAccount(privateKey as `0x${string}`).address;
    this.account = accountAddress.toString();
    privateKey_ = privateKey;

    await this.updatePixelClient();

    this.emit('connect', { chainId: this.chainId, accountAddress: this.account });

    return [this.account];
  }

  public disconnect() {
    pixelProviderLog('disconnect');

    privateKey_ = undefined;
    this.account = undefined;
    this.signer = undefined;

    pixelStorage.clearPrivateKey();
    this.emit('disconnect', {});
  }

  request = async ({ method, params = [] }: { method: string; params?: any[] }) => {
    try {
      switch (method) {
        case 'eth_accounts': {
          return this.requestAccounts();
        }
        case 'eth_requestAccounts':
          return this.requestAccounts();
        case 'eth_sendTransaction':
          if (!this.signer) {
            throw new Error('no singer');
          }

          const tx: TransactionRequest = {
            ...params[0],
            gasLimit: params[0].gas,
          };

          if (this.chainId === 1291) {
            const blockNumber = await this.rpc.getBlockNumber();
            const nodePublicKey = await this.rpc.send('eth_getNodePublicKey', [
              numberToHex(blockNumber),
            ]);

            const [encryptedData, encryptionKey] = encryptDataFieldWithPublicKey(
              nodePublicKey,
              tx.data
            );
            tx.data = encryptedData;
          }

          // console.log(tx);
          // throw new Error('test');

          const res = await this.signer.sendTransaction(tx);

          return res.hash;
        case 'personal_sign':
          return sign(params[0], privateKey_);
        case 'wallet_switchEthereumChain':
          const nextChangeId = params[0].chainId;

          const chain = this.chains.find((el) => el.id === nextChangeId);

          const rpcUrl = chain?.rpcUrls.default.http[0];
          if (!rpcUrl) {
            if (!rpcUrl) throw new UrlRequiredError();
          }

          this.chainId = nextChangeId;
          localStorage.setItem(STORAGE_KEY_CHAIN_ID, nextChangeId);

          await this.updatePixelClient();

          this.emit('chainChanged', nextChangeId);
          return true;
        case 'eth_chainId': {
          return this.chainId;
        }
        case 'wallet_addEthereumChain':
        default:
          return null;
      }
    } catch (error) {
      // pixelLogger.error((error as Error).message);
      throw error;
    }
  };

  async requestAccounts() {
    // return ['0x4B9Ac70305761c8f1c4e88AD6C54499b15674a07'];
    return this.account ? [this.account] : [];
  }

  emit = (eventName: string, params: any) => {
    this.listeners[eventName]?.map((listener) => {
      listener(params);
    });
  };

  on = (eventType: string, callback: Function) => {
    let listeners = this.listeners[eventType];

    if (!listeners) {
      this.listeners[eventType] = [];
      listeners = this.listeners[eventType];
    }

    if (!listeners.find((l) => l === callback)) {
      listeners.push(callback);
    }
  };

  removeListener = (eventType: string, callback: Function) => {
    this.listeners[eventType] = this.listeners[eventType].filter((l) => l !== callback);
  };
}
