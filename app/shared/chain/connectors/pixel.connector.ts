import { ChainNotConfiguredError, Connector, createConnector } from '@wagmi/core';
import { SwitchChainError, UserRejectedRequestError, getAddress, numberToHex, Address } from 'viem';
import { PixelProvider, pixelStorage } from './pixel.provider.ts';
import { telegramIdKey } from '../../const/localStorage.ts';

createPixelConnector.type = 'pixel';

const log = (text: string) => {
  // console.info(`[pixel-connector] - ${text}`);
};
export let WAGMI_PROVIDER_REF = { ref: null as PixelProvider | null };
//
export function createPixelConnector() {
  let walletProvider: PixelProvider;
  let accountsChanged: Connector['onAccountsChanged'] | undefined;
  let chainChanged: Connector['onChainChanged'] | undefined;
  let connect: Connector['onConnect'] | undefined;
  let disconnect: Connector['onDisconnect'] | undefined;

  return createConnector((config) => {
    const connector = {
      id: 'pixel',
      name: 'Pixel Wallet',
      supportsSimulation: true,
      type: createPixelConnector.type,
      async setup() {
        const provider = await this.getProvider();

        if (!connect) {
          connect = this.onConnect.bind(this);
          provider.on('connect', connect);
        }
      },
      async connect({ chainId, ...other }: { chainId?: number } = {}) {
        const provider = await this.getProvider();
        pixelStorage.refreshPrivateKey();

        const telegramData =
          Telegram.WebApp.initDataUnsafe?.user?.id || localStorage.getItem(telegramIdKey);
        const privateKey =
          window.localStorage.getItem(`wallet-${telegramData}-privateKey`) ||
          pixelStorage.getPrivateKey();

        log('try to connect ' + (!!privateKey ? 'with key' : 'no key'));

        if (!privateKey) {
          throw new Error('failed to connect');
        }

        let accounts: Address[] = [];

        try {
          if (!accounts?.length) {
            const requestedAccounts = (await provider.connect(privateKey)) as string[];
            accounts = requestedAccounts.map((x) => getAddress(x));
          }

          // Switch to chain if provided
          let currentChainId = await this.getChainId();

          if (chainId && currentChainId !== chainId) {
            const chain = await this.switchChain({ chainId }).catch((error) => {
              if (error.code === UserRejectedRequestError.code) throw error;
              return { id: currentChainId };
            });
            currentChainId = chain?.id ?? currentChainId;
          }

          if (connect) {
            provider.removeListener('connect', connect);
            connect = undefined;
          }
          if (!accountsChanged) {
            accountsChanged = this.onAccountsChanged.bind(this);
            provider.on('accountsChanged', accountsChanged);
          }
          if (!chainChanged) {
            chainChanged = this.onChainChanged.bind(this);
            provider.on('chainChanged', chainChanged);
          }
          if (!disconnect) {
            console.log('subscribe disconnect');
            disconnect = this.onDisconnect.bind(this);
            provider.on('disconnect', disconnect);
          }

          return { accounts, chainId: currentChainId };
        } catch (error) {
          console.log('[pixel]', error);
          if (
            /(user closed modal|accounts received is empty|user denied account|request rejected)/i.test(
              (error as Error).message
            )
          )
            throw new UserRejectedRequestError(error as Error);
          throw error;
        }
      },
      async disconnect() {
        log('disconnect');

        const provider = await this.getProvider();
        provider.disconnect();

        if (accountsChanged) {
          provider.removeListener('accountsChanged', accountsChanged);
          accountsChanged = undefined;
        }
        if (chainChanged) {
          provider.removeListener('chainChanged', chainChanged);
          chainChanged = undefined;
        }
        if (disconnect) {
          provider.removeListener('disconnect', disconnect);
          disconnect = undefined;
        }
        if (!connect) {
          connect = this.onConnect.bind(this);
          provider.on('connect', connect);
        }

        // provider.close?.();
      },
      async getAccounts() {
        const provider = await this.getProvider();
        const list = (await provider.requestAccounts()).map((x: string) => getAddress(x));

        return list;
      },
      async getChainId() {
        const provider = await this.getProvider();
        const chainId = await provider.request({
          method: 'eth_chainId',
        });
        return Number(chainId);
      },
      async getProvider() {
        // if (!WAGMI_PROVIDER_REF.ref) {
        //   WAGMI_PROVIDER_REF.ref = new PixelProvider({
        //     chains: config.chains,
        //     transports: config.transports,
        //   });
        // }
        //
        // walletProvider = WAGMI_PROVIDER_REF.ref;
        if (!walletProvider) {
          walletProvider = new PixelProvider({
            chains: config.chains,
            transports: config.transports,
          });
        }

        WAGMI_PROVIDER_REF.ref = walletProvider;

        return walletProvider;
      },
      async isAuthorized() {
        try {
          const accounts = await this.getAccounts();
          return !!accounts.length;
        } catch {
          return false;
        }
      },
      async switchChain({
        addEthereumChainParameter,
        chainId,
      }: {
        addEthereumChainParameter?: any;
        chainId: number;
      }) {
        console.log('[switchChain]', chainId);
        const chain = config.chains.find((chain) => chain.id === chainId);

        if (!chain) throw new SwitchChainError(new ChainNotConfiguredError());
        const provider = await this.getProvider();

        try {
          await Promise.all([
            provider
              .request({
                method: 'wallet_switchEthereumChain',
                params: [{ chainId: chainId }],
              })
              // During `'wallet_switchEthereumChain'`, MetaMask makes a `'net_version'` RPC call to the target chain.
              // If this request fails, MetaMask does not emit the `'chainChanged'` event, but will still switch the chain.
              // To counter this behavior, we request and emit the current chain ID to confirm the chain switch either via
              // this callback or an externally emitted `'chainChanged'` event.
              // https://github.com/MetaMask/metamask-extension/issues/24247
              .then(async () => {
                const currentChainId = await this.getChainId();
                if (currentChainId === chainId) config.emitter.emit('change', { chainId });
              }),
            new Promise<void>((resolve) =>
              config.emitter.once('change', ({ chainId: currentChainId }) => {
                if (currentChainId === chainId) resolve();
              })
            ),
          ]);

          await provider.request({
            method: 'wallet_switchEthereumChain',
            params: [{ chainId: chain.id }],
          });
          return chain;
        } catch (error) {
          console.error(error);

          // Indicates chain is not added to provider
          if ((error as any).code === 4902) {
            try {
              let blockExplorerUrls;
              if (addEthereumChainParameter?.blockExplorerUrls)
                blockExplorerUrls = addEthereumChainParameter.blockExplorerUrls;
              else
                blockExplorerUrls = chain.blockExplorers?.default.url
                  ? [chain.blockExplorers?.default.url]
                  : [];
              let rpcUrls;
              if (addEthereumChainParameter?.rpcUrls?.length)
                rpcUrls = addEthereumChainParameter.rpcUrls;
              else rpcUrls = [chain.rpcUrls.default?.http[0] ?? ''];
              const addEthereumChain = {
                blockExplorerUrls,
                chainId: numberToHex(chainId),
                chainName: addEthereumChainParameter?.chainName ?? chain.name,
                iconUrls: addEthereumChainParameter?.iconUrls,
                nativeCurrency: addEthereumChainParameter?.nativeCurrency ?? chain.nativeCurrency,
                rpcUrls,
              };
              await provider.request({
                method: 'wallet_addEthereumChain',
                params: [addEthereumChain],
              });
              return chain;
            } catch (error) {
              throw new UserRejectedRequestError(error as Error);
            }
          }

          throw new SwitchChainError(error as Error);
        }
      },
      onAccountsChanged(accounts: string[]) {
        if (accounts.length === 0) {
          log('onAccountsChanged force disconnect');
          this.onDisconnect();
        } else {
          config.emitter.emit('change', {
            accounts: accounts.map((x) => getAddress(x)),
          });
        }
      },
      onChainChanged(chain: string) {
        log(Number(chain).toString());

        const chainId = Number(chain);
        config.emitter.emit('change', { chainId });
      },
      // async onConnect(connectInfo) {
      //   return super.onConnect()
      // },
      async onDisconnect(_error?: Error) {
        log('onDisconnect');

        config.emitter.emit('disconnect');

        const provider = await this.getProvider();

        if (accountsChanged) {
          provider.removeListener('accountsChanged', accountsChanged);
          accountsChanged = undefined;
        }
        if (chainChanged) {
          provider.removeListener('chainChanged', chainChanged);
          chainChanged = undefined;
        }
        if (disconnect) {
          provider.removeListener('disconnect', disconnect);
          disconnect = undefined;
        }
        if (!connect) {
          connect = this.onConnect.bind(this);
          provider.on('connect', connect);
        }
      },
      async onConnect(connectInfo: any) {
        log('onConnect');
        const accounts = await this.getAccounts();

        if (accounts.length === 0) return;

        const chainId = Number(connectInfo.chainId);
        config.emitter.emit('connect', { accounts, chainId });

        const provider = await this.getProvider();
        if (connect) {
          provider.removeListener('connect', connect);
          connect = undefined;
        }
        if (!accountsChanged) {
          accountsChanged = this.onAccountsChanged.bind(this);
          provider.on('accountsChanged', accountsChanged);
        }
        if (!chainChanged) {
          chainChanged = this.onChainChanged.bind(this);
          provider.on('chainChanged', chainChanged);
        }
        if (!disconnect) {
          disconnect = this.onDisconnect.bind(this);
          provider.on('disconnect', disconnect);
        }
      },
    };

    return connector;
  });
}
