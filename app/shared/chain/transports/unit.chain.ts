import { defineChain } from 'viem';

export const unitChain = /*#__PURE__*/ defineChain({
  id: 88811,
  testnet: false,
  name: 'Unit Zero',
  nativeCurrency: { name: 'Unit Zero', symbol: 'UNIT0', decimals: 18 },
  rpcUrls: {
    default: {
      http: ['https://rpc.unit0.dev'],
      // webSocket: ['wss://json-rpc.testnet.swisstronik.com'],
    },
  },
  blockExplorers: {
    default: {
      name: 'UNIT Explorer',
      url: 'https://explorer.unit0.dev',
    },
  },
});
