import { defineChain } from 'viem';

export const swtrTestnetChain = /*#__PURE__*/ defineChain({
  id: 1291,
  testnet: true,
  name: 'SWTR | Swisstronik',
  nativeCurrency: { name: 'SWTR', symbol: 'SWTR', decimals: 18 },
  rpcUrls: {
    default: {
      http: [
        'https://swiss-citadel.hellopixel.network',
        'https://json-rpc.testnet.swisstronik.com',
      ],
      webSocket: ['wss://json-rpc.testnet.swisstronik.com'],
    },
  },
  blockExplorers: {
    default: {
      name: 'SWTR Explorer',
      url: 'https://explorer-evm.testnet.swisstronik.com',
    },
  },
});
