import { RpcRequestError } from 'viem';
import { CustomTransport, CustomTransportConfig } from 'viem/clients/transports/custom.ts';
import { format } from 'web3-utils';
import { DEFAULT_RETURN_FORMAT } from 'web3-types';
import { getHttpRpcClient } from 'viem/utils';
import { createTransport } from 'viem';

export function customHttp(
  url?: string,
  config: Omit<CustomTransportConfig, 'key' | 'name'> = {}
): CustomTransport {
  return ({ retryCount: defaultRetryCount, chain }) => {
    const url_ = url || chain?.rpcUrls.default.http[0];

    const rpcClient = getHttpRpcClient(url_, {
      fetchOptions: undefined,
      onRequest: undefined,
      onResponse: undefined,
      timeout: 10_000,
    });

    const { retryDelay } = config;

    const provider = {
      async request({ method, params }) {
        const body = { method, params };

        if (method !== 'eth_call') {
          const { result, error } = await rpcClient.request({ body });

          if (error)
            throw new RpcRequestError({
              body: body,
              error: error,
              url: url_,
            });

          return format({ format: 'bytes' }, result, DEFAULT_RETURN_FORMAT);
        }

        const { result: response, error } = await rpcClient.request({
          body: body,
        });

        if (error)
          throw new RpcRequestError({
            body: body,
            error,
            url: url_,
          });

        return format({ format: 'bytes' }, response, DEFAULT_RETURN_FORMAT);
      },
    };

    return createTransport({
      key: 'pixel',
      name: 'pixel HTTP JSON-RPC',
      request: provider.request.bind(provider),
      retryCount: config.retryCount ?? defaultRetryCount,
      retryDelay,
      type: 'custom',
    });
  };
}
