import { RpcRequestError } from 'viem';
import { CustomTransport, CustomTransportConfig } from 'viem/clients/transports/custom.ts';
import { format } from 'web3-utils';
import {
  encryptDataFieldWithPublicKey,
  decryptNodeResponseWithPublicKey,
} from '@swisstronik/utils';
import { DEFAULT_RETURN_FORMAT } from 'web3-types';
import { getHttpRpcClient } from 'viem/utils';
import { createTransport } from 'viem';

export function swtrHttp(
  url?: string,
  config: Omit<CustomTransportConfig, 'key' | 'name'> = {}
): CustomTransport {
  let nodeKeyCache: { block: number; key: string } | null = null;

  return ({ retryCount: defaultRetryCount, chain }) => {
    const url_ = url || chain?.rpcUrls.default.http[0];

    const rpcClient = getHttpRpcClient(url_, {
      fetchOptions: undefined,
      onRequest: undefined,
      onResponse: undefined,
      timeout: 10_000,
    });

    const { retryDelay } = config;

    const provider = {
      async request({ method, params }) {
        if (method === 'eth_call') {
          return this.encryptedCall(method, params);
        } else if (method === 'eth_estimateGas') {
          return this.estimateGas(method, params);
        } else if (method === 'eth_sendTransaction') {
          return this.sendTransaction(method, params);
        } else if (method === 'eth_getTransactionByHash') {
          return this.rawRequest(method, params);
        } else if (method === 'eth_getTransactionReceipt') {
          return this.rawRequest(method, params);
        } else if (method === 'eth_getBlockByNumber') {
          return this.rawRequest(method, params);
        }

        return this.basicRequest(method, params);
      },
      async sendTransaction(method: string, params: any) {
        const blockNumber = await this.getCurrentBlock();
        const nodePublicKey = await this.getPublicKey(blockNumber);

        const data = params[0].data;
        const [encryptedData, encryptionKey] = encryptDataFieldWithPublicKey(nodePublicKey, data);
        const encryptedParams = [{ ...params[0], data: encryptedData }];

        const encryptedBody = {
          method,
          params: encryptedParams,
        };

        const { result: response, error } = await rpcClient.request({
          body: encryptedBody,
        });

        if (error)
          throw new RpcRequestError({
            body: encryptedBody,
            error,
            url: url_,
          });

        return response;
      },
      async estimateGas(method: string, params: any) {
        const blockNumber = await this.getCurrentBlock();
        const nodePublicKey = await this.getPublicKey(blockNumber);

        const data = params[0].data;
        const [encryptedData, encryptionKey] = encryptDataFieldWithPublicKey(nodePublicKey, data);
        const encryptedParams = [{ ...params[0], data: encryptedData }, blockNumber];

        const encryptedBody = {
          method,
          params: encryptedParams,
        };

        const { result: response, error } = await rpcClient.request({
          body: encryptedBody,
        });

        if (error)
          throw new RpcRequestError({
            body: encryptedBody,
            error,
            url: url_,
          });

        const result = BigInt(Number(response));

        return result;
      },
      async encryptedCall(method: string, params: any) {
        const blockNumber = await this.getCurrentBlock();
        const nodePublicKey = await this.getPublicKey(blockNumber);

        const data = params[0].data;
        const [encryptedData, encryptionKey] = encryptDataFieldWithPublicKey(nodePublicKey, data);
        const encryptedParams = [{ ...params[0], data: encryptedData }, params[1]]; // params[1] | blockNumber

        const encryptedBody = {
          method,
          params: encryptedParams,
        };

        const { result: response, error } = await rpcClient.request({
          body: encryptedBody,
        });

        if (error)
          throw new RpcRequestError({
            body: encryptedBody,
            error,
            url: url_,
          });

        return this.formatResult({ response, encryptionKey, nodePublicKey });
      },
      async getCurrentBlock() {
        return this.rawRequest('eth_blockNumber', []);
      },
      async getPublicKey(blockNumber: number) {
        if (nodeKeyCache?.block === blockNumber) {
          return nodeKeyCache.key;
        }

        const key = await this.rawRequest('eth_getNodePublicKey', [blockNumber]);

        nodeKeyCache = { block: blockNumber, key };

        return key;
      },
      async basicRequest(method: string, params: any) {
        const result = await this.rawRequest(method, params);

        return format({ format: 'bytes' }, result, DEFAULT_RETURN_FORMAT);
      },
      async rawRequest(method: string, params: any) {
        const body = { method, params };
        const { result, error } = await rpcClient.request({ body });

        if (error)
          throw new RpcRequestError({
            body: body,
            error: error,
            url: url_,
          });

        return result;
      },
      // utils
      async formatResult({
        response,
        encryptionKey,
        nodePublicKey,
      }: {
        response: string;
        nodePublicKey: string;
        encryptionKey: Uint8Array;
      }) {
        const decryptedResponse = decryptNodeResponseWithPublicKey(
          nodePublicKey,
          response,
          encryptionKey
        );

        const result = format({ format: 'bytes' }, decryptedResponse, DEFAULT_RETURN_FORMAT);

        return result;
      },
    };

    return createTransport({
      key: 'http',
      name: 'SWTR HTTP JSON-RPC',
      request: provider.request.bind(provider),
      retryCount: config.retryCount ?? defaultRetryCount,
      retryDelay,
      type: 'http',
    });
  };
}
