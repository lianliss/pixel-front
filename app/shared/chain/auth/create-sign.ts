import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';
import { signMessage } from '@wagmi/core';
import { wagmiConfig } from '@chain/wagmi.ts';

export function hasSign(accountAddress: string): boolean {
  const hash = hmacSha256(accountAddress, 'pixel-signature').toString(encoderHex);
  const key = `pixel-sign-${hash}`;

  return !!localStorage.getItem(key);
}

export async function createSign(accountAddress: string) {
  const hash = hmacSha256(accountAddress, 'pixel-signature').toString(encoderHex);
  const message = `Sign up with code ${hash}`;
  const key = `pixel-sign-${hash}`;

  const signResult = await signMessage(wagmiConfig, {
    account: accountAddress,
    message: { raw: message },
  });
  const sign = signResult.signature || signResult;

  localStorage.setItem(key, sign);
}

export function clearSign() {
  for (const k in localStorage) {
    if (k.startsWith('pixel-sign')) {
      localStorage.removeItem(k);
    }
  }
}
