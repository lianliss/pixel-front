import React from 'react';
import Box, { BoxProps } from '@mui/material/Box';

export type ImgProps = BoxProps<'img'>;

export const Img = React.forwardRef((props: ImgProps, ref) => {
  const { ...other } = props;

  return <Box ref={ref} component='img' {...other} />;
});
