import React from 'react';
import MuiSwitch from '@mui/material/Switch';
import type { SwitchProps as MuiSwitchProps } from '@mui/material/Switch';

interface SwitchProps extends MuiSwitchProps {}

export const Switch = (props: SwitchProps) => {
  return <MuiSwitch {...props} />;
};
