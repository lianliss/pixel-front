import React from 'react';
import Fade from '@mui/material/Fade';
import MuiModal, { ModalProps as MuiModalProps } from '@mui/material/Modal';

interface ModalProps extends MuiModalProps {}

export const Modal = (props: ModalProps) => {
  return (
    <MuiModal
      {...props}
      aria-labelledby='test-modal-title'
      aria-describedby='test-modal-description'
      closeAfterTransition
      slotProps={{
        backdrop: {
          timeout: 500,
        },
      }}
    >
      <Fade in={props.open}>{props.children}</Fade>
    </MuiModal>
  );
};
