import React from 'react';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import { TextFieldProps } from '@mui/material/TextField';

const NUMBER_REGEX = /[^\d.-]+/g;

type Props = TextFieldProps & { step?: number };

const NumberField = (props: Props) => {
  const { onChange, step, inputProps = {}, ...other } = props;

  return (
    <TextField
      inputProps={{ ...inputProps, step }}
      type='number'
      onChange={(e) => {
        e.target.value = e.target.value.replace(NUMBER_REGEX, '');
        onChange?.(e);
      }}
      {...other}
    />
  );
};

export default NumberField;
