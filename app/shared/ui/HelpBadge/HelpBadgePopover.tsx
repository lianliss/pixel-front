import Badge, { BadgeProps } from '@mui/material/Badge';
import Tooltip from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import React from 'react';
import Typography from '@mui/material/Typography';
import Popover from '@mui/material/Popover';
import { styled } from '@mui/material/styles';

const StyledBadge = styled(Badge)(() => ({
  '& .MuiBadge-badge': { top: -2, right: 4 },
}));

export type IHelpBadgeProps = React.PropsWithChildren<{ title?: string; onClick?: () => void }> &
  Omit<BadgeProps, 'badgeContent'>;

function HelpBadgePopover(props: IHelpBadgeProps) {
  const { title, onClick, children, ...other } = props;

  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const handleClick = (event: React.MouseEvent) => {
    if (onClick) {
      onClick();

      return;
    }

    setAnchorEl(event.currentTarget as HTMLElement);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  if (!title) {
    return children;
  }

  return (
    <>
      <StyledBadge
        badgeContent={<InfoIcon sx={{ fontSize: 20 }} onClick={handleClick} />}
        color='text.primary'
        {...other}
      >
        {children}
      </StyledBadge>

      {title && (
        <Popover
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
        >
          <Typography sx={{ p: 2 }} variant='h4' color='text.primary'>
            {title}
          </Typography>
        </Popover>
      )}
    </>
  );
}

export default HelpBadgePopover;
