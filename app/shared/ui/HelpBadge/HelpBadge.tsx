import Badge, { BadgeProps } from '@mui/material/Badge';
import InfoIcon from '@mui/icons-material/Info';
import React from 'react';
import { styled } from '@mui/material/styles';

const StyledBadge = styled(Badge)(() => ({
  '& .MuiBadge-badge': { top: 4, right: 8 },
}));

export type IHelpBadgeProps = React.PropsWithChildren<{ onClick?: () => void }> &
  Omit<BadgeProps, 'badgeContent'>;

function HelpBadge(props: IHelpBadgeProps) {
  const { children, ...other } = props;

  return (
    <StyledBadge badgeContent={<InfoIcon sx={{ fontSize: 20 }} />} color='text.primary' {...other}>
      {children}
    </StyledBadge>
  );
}

export default HelpBadge;
