import cls from './styles.module.scss';
import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'react-i18next';

interface Props {
  open: boolean;
  children: React.ReactNode;
  rightButton: React.ReactNode;

  onSkip: () => void;
  skipButton?: boolean;
  target?: string;
}

const TutorialModal = ({ open, onSkip, skipButton = false, children, rightButton }: Props) => {
  const { t } = useTranslation('tutorial');

  const handleSkip = (e: React.MouseEvent) => {
    e.stopPropagation();
    onSkip();
  };

  const handleModalClick = (e: React.MouseEvent) => {
    e.stopPropagation();
  };

  if (!open) return null;

  return (
    <div id='#tutorial-overlay' className={cls.modal__overlay}>
      <div
        className={cls.modal__header}
        style={{ justifyContent: skipButton && rightButton ? 'space-between' : 'flex-end' }}
      >
        {skipButton && (
          <Button className={cls.modal__close} onClick={handleSkip}>
            <Typography sx={{ fontSize: 14 }} fontWeight='bold'>
              {t('Skip')}
            </Typography>
          </Button>
        )}
        {rightButton && rightButton}
      </div>

      <div className={cls.modal__content} onClick={handleModalClick} role='dialog'>
        {children}
      </div>
    </div>
  );
};

export default TutorialModal;
