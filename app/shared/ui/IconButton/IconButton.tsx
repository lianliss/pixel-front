import React from 'react';
import type { IconButtonProps as MuiIconButtonProps } from '@mui/material/IconButton';
import MuiIconButton from '@mui/material/IconButton';
import { styled } from '@mui/material/styles';
import clsx from 'clsx';

const PREFIX = 'IconButton';
const classes = {
  outlined: `${PREFIX}_outlined`,
  square: `${PREFIX}_square`,
};

const Styled = styled(MuiIconButton)(({ theme }) => ({
  [`&.${classes.square}`]: {
    borderRadius: `8px`,
  },
}));

type IconButtonProps = MuiIconButtonProps & {
  loading?: boolean;
  variant?: 'outlined';
  shape?: 'circle' | 'square';
};

export const IconButton = (props: IconButtonProps) => {
  const { loading, disabled, className, variant, shape, ...other } = props;

  return (
    <Styled
      disabled={disabled || loading}
      className={clsx(
        { [classes.outlined]: variant === 'outlined', [classes.square]: shape === 'square' },
        className
      )}
      {...other}
    />
  );
};
