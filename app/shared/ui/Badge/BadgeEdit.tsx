import EditIcon from '@mui/icons-material/Edit';
import React from 'react';
import Badge, { BadgeProps } from '@mui/material/Badge';

type Props = Omit<BadgeProps, 'badgeContent' | 'componentsProps'> & { show?: boolean };

function BadgeEdit(props: Props) {
  const { show, ...other } = props;

  return (
    <Badge
      badgeContent={show ? <EditIcon style={{ width: 8, transform: 'scale(1.4)' }} /> : undefined}
      color='success'
      componentsProps={{
        badge: { sx: { transform: 'scale(1.0) translate(6px, -6px)' } },
      }}
      {...other}
    />
  );
}

export default BadgeEdit;
