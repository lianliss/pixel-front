import { useUpdateInterval } from 'utils/hooks/useUpdateInterval';
import React from 'react';

type Props = { render: () => React.ReactNode };

export function IntervalRender(props: Props) {
  const { render } = props;

  useUpdateInterval(1000);

  return <>{render()}</>;
}
