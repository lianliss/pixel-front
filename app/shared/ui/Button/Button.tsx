import React from 'react';
import type { ButtonProps as MuiButtonProps } from '@mui/material/Button';
import MuiButton from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';

type ButtonProps = MuiButtonProps & { loading?: boolean; loadingText?: string };

export const Button = (props: ButtonProps) => {
  const { loading, endIcon, disabled, children, loadingText = 'Loading...', ...other } = props;
  return (
    <MuiButton
      disabled={disabled || loading}
      endIcon={loading ? <CircularProgress size={16} /> : endIcon}
      {...other}
    >
      {children}
    </MuiButton>
  );
};
