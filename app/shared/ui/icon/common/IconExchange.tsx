import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

export const IconExchange = () => {
  return (
    <SvgIcon
      width='24'
      height='24'
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <svg
        width='25'
        height='22'
        viewBox='0 0 25 22'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path
          d='M22.3747 4.39648H5.841C3.16739 4.39648 1 6.70977 1 9.56324V11.0001M2.12498 17.6038H18.6587C21.3323 17.6038 23.4997 15.2906 23.4997 12.4371V11.0001'
          stroke='currentColor'
          strokeOpacity='0.5'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M19.7539 7.79198L22.9358 4.39599L19.7539 1'
          stroke='currentColor'
          strokeOpacity='0.5'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M5.30693 14.2085L2.125 17.6045L5.30693 21.0005'
          stroke='currentColor'
          strokeOpacity='0.5'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
      </svg>
    </SvgIcon>
  );
};
