import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

export const IconTiming = () => {
  return (
    <SvgIcon
      width='24'
      height='24'
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <svg
        width='16'
        height='16'
        viewBox='0 0 16 16'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path d='M4.5 2H11.5' stroke='#7EA1CA' strokeWidth='1.5' strokeLinecap='round' />
        <path d='M4.5 14H11.5' stroke='#7EA1CA' strokeWidth='1.5' strokeLinecap='round' />
        <path
          d='M5.5 2.5C5.5 2.22386 5.72386 2 6 2H10C10.2761 2 10.5 2.22386 10.5 2.5V4.5C10.5 5.88071 9.38071 7 8 7V7C6.61929 7 5.5 5.88071 5.5 4.5V2.5Z'
          stroke='#7EA1CA'
          strokeWidth='1.5'
        />
        <path
          d='M5.5 13.5C5.5 13.7761 5.72386 14 6 14H10C10.2761 14 10.5 13.7761 10.5 13.5V11.5C10.5 10.1193 9.38071 9 8 9V9C6.61929 9 5.5 10.1193 5.5 11.5V13.5Z'
          stroke='#7EA1CA'
          strokeWidth='1.5'
        />
        <circle cx='8' cy='4.5' r='1' fill='#7EA1CA' />
      </svg>
    </SvgIcon>
  );
};
