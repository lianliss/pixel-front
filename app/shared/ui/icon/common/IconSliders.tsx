import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

const IconSliders = () => {
  return (
    <SvgIcon
      width='24'
      height='24'
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <svg
        width='24'
        height='24'
        viewBox='0 0 24 24'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path
          fillRule='evenodd'
          clipRule='evenodd'
          d='M12 2C3.76471 2 2 3.76471 2 12C2 20.2353 3.76471 22 12 22C20.2353 22 22 20.2353 22 12C22 3.76471 20.2353 2 12 2Z'
          stroke='#4E99DE'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <g opacity='0.66'>
          <path d='M17.5 9H10' stroke='#4E99DE' strokeLinecap='round' strokeLinejoin='round' />
          <circle cx='8' cy='9' r='1.5' stroke='#4E99DE' />
        </g>
        <path d='M6.5 15H14' stroke='#4E99DE' strokeLinecap='round' strokeLinejoin='round' />
        <circle cx='16' cy='15' r='1.5' transform='rotate(180 16 15)' stroke='#4E99DE' />
      </svg>
    </SvgIcon>
  );
};

export default IconSliders;
