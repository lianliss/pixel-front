import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

export const IconCopy = () => {
  return (
    <SvgIcon
      width='24'
      height='24'
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <svg
        width='24'
        height='24'
        viewBox='0 0 24 24'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <rect x='3' y='6.6' width='14.4' height='14.4' rx='2' stroke='#A62CFF' />
        <path
          d='M17.4001 17.4H19.0001C20.1047 17.4 21.0001 16.5046 21.0001 15.4V5C21.0001 3.89543 20.1047 3 19.0001 3H8.6001C7.49553 3 6.6001 3.89546 6.6001 5.00003C6.6001 5.55318 6.6001 6.08438 6.6001 6.6'
          stroke='#A62CFF'
        />
      </svg>
    </SvgIcon>
  );
};
