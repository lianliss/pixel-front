import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

export const IconDuration = ({ props }) => {
  return (
    <SvgIcon
      width='24'
      height='24'
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      {...props}
    >
      <svg
        width='16'
        height='16'
        viewBox='0 0 16 16'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path
          d='M1 8C1 8.29042 1.01769 8.57672 1.05204 8.85786C1.475 12.3192 4.42443 15 8 15C11.866 15 15 11.866 15 8C15 4.47353 12.3923 1.55612 9 1.07089C8.6734 1.02417 8.33952 1 8 1'
          stroke='#7EA1CA'
          strokeWidth='1.5'
        />
        <path
          d='M8 4.5V7.58579C8 7.851 8.10536 8.10536 8.29289 8.29289L10 10'
          stroke='#7EA1CA'
          strokeWidth='1.5'
          strokeLinecap='round'
        />
        <path
          d='M1 8C1 4.13401 4.13401 1 8 1'
          stroke='#7EA1CA'
          strokeWidth='1.5'
          strokeDasharray='0.1 3'
        />
      </svg>
    </SvgIcon>
  );
};
