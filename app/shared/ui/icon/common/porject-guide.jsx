import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

function ProjectGuideIcon(props) {
  return (
    <SvgIcon
      width='24'
      height='24'
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      {...props}
    >
      <g filter='url(#filter0_d_2329_676)'>
        <rect x='7' y='3' width='14' height='16' fill='#A62CFF' />
        <rect x='15' y='6' width='9' height='4' fill='#A62CFF' />
        <path
          d='M13.7886 6.82763C14.3788 6.82763 14.8591 7.30785 14.8591 7.89811C14.8591 8.2792 14.6537 8.63474 14.323 8.826C13.3331 9.3987 12.7181 10.4643 12.7181 11.6072V13.2505H14.8591V11.6072C14.8591 11.2261 15.0644 10.8705 15.3951 10.6792C16.3851 10.1066 17 9.04095 17 7.89811C17 6.12732 15.5594 4.68668 13.7886 4.68668C12.0178 4.68668 10.5771 6.12732 10.5771 7.89811H12.7181C12.7181 7.30785 13.1983 6.82763 13.7886 6.82763Z'
          fill='white'
        />
        <path d='M12.7181 14.3914H14.8591V16.5324H12.7181V14.3914Z' fill='white' />
        <path
          d='M24.4376 7.93773L26 5.5941H21.2335V2.00055H5.75748C4.78839 2.00055 4 2.78894 4 3.75803V20.2417C4 21.2109 4.78851 21.9995 5.75772 21.9995H21.2335C21.2335 21.8616 21.2335 11.4533 21.2335 10.2813H26L24.4376 7.93773ZM5.75748 3.17236H6.34362V18.484H5.75772C5.55234 18.484 5.3552 18.5198 5.17181 18.5848V3.75803C5.17181 3.43508 5.43453 3.17236 5.75748 3.17236ZM20.0617 20.8276H5.75772C5.43465 20.8276 5.17181 20.5648 5.17181 20.2417C5.17181 19.9187 5.43465 19.6558 5.75772 19.6558H20.0617V20.8276ZM7.51543 18.484V3.17236H20.0617V18.484H7.51543ZM23.8104 9.10954H21.2335V6.76591H23.8104L23.0292 7.93773L23.8104 9.10954Z'
          fill='#A62CFF'
        />
      </g>
      <defs>
        <filter
          id='filter0_d_2329_676'
          x='0'
          y='0.000549316'
          width='30'
          height='27.9989'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
            result='hardAlpha'
          />
          <feOffset dy='2' />
          <feGaussianBlur stdDeviation='2' />
          <feComposite in2='hardAlpha' operator='out' />
          <feColorMatrix
            type='matrix'
            values='0 0 0 0 0.65098 0 0 0 0 0.172549 0 0 0 0 1 0 0 0 0.8 0'
          />
          <feBlend mode='normal' in2='BackgroundImageFix' result='effect1_dropShadow_2329_676' />
          <feBlend
            mode='normal'
            in='SourceGraphic'
            in2='effect1_dropShadow_2329_676'
            result='shape'
          />
        </filter>
      </defs>
    </SvgIcon>
  );
}

export default ProjectGuideIcon;
