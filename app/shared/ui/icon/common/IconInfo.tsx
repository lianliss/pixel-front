import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

export const IconInfo = () => {
  return (
    <SvgIcon
      width='24'
      height='24'
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <svg
        width='10'
        height='10'
        viewBox='0 0 10 10'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <g opacity='0.6'>
          <circle cx='5' cy='5' r='4.5' stroke='#A62CFF' />
          <path d='M5 4.46094L5 7.27344' stroke='#A62CFF' strokeLinecap='round' />
          <path d='M5 4.46094L4.21875 4.46094' stroke='#A62CFF' strokeLinecap='round' />
          <path d='M5.625 7.27344L4.21875 7.27344' stroke='#A62CFF' strokeLinecap='round' />
          <circle cx='5.00023' cy='2.89844' r='0.625' fill='#A62CFF' />
        </g>
      </svg>
    </SvgIcon>
  );
};
