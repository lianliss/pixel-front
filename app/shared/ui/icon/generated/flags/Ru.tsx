import * as React from 'react';
import type { SVGProps } from 'react';
const SvgRu = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={513} height={343} fill='none' {...props}>
    <path fill='#fff' d='M0 .986v341.999h513V.986z' />
    <path fill='#0052B4' d='M0 .99h513v342.003H0z' />
    <path fill='#fff' d='M0 .99h513v113.996H0z' />
    <path fill='#D80027' d='M0 228.984h513v113.998H0z' />
  </svg>
);
export default SvgRu;
