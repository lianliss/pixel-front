import * as React from 'react';
import type { SVGProps } from 'react';
const SvgDigitalSoul = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M12 2C3.765 2 2 3.765 2 12s1.765 10 10 10 10-1.765 10-10S20.235 2 12 2'
      clipRule='evenodd'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M11.156 7.326a1 1 0 0 1 1.688 0l5.178 8.137A1 1 0 0 1 17.178 17H6.822a1 1 0 0 1-.844-1.537z'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='m9 7.5-2.5-3M7 10 4.5 8.5M5.5 13 4 12.5M15 7.5l2.5-3M17 10l2.5-1.5M18.5 13l1.5-.5M14 13s-.895 1-2 1-2-1-2-1 .895-1 2-1 2 1 2 1'
      opacity={0.66}
    />
  </svg>
);
export default SvgDigitalSoul;
