import * as React from 'react';
import type { SVGProps } from 'react';
const SvgAchievements = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M11.159 4.31a1 1 0 0 1 1.683 0l2.357 3.67a1 1 0 0 0 .587.427l4.219 1.107a1 1 0 0 1 .52 1.6l-2.762 3.377a1 1 0 0 0-.224.69l.25 4.355a1 1 0 0 1-1.362.989l-4.064-1.583a1 1 0 0 0-.726 0l-4.064 1.583a1 1 0 0 1-1.361-.99l.25-4.354a1 1 0 0 0-.225-.69l-2.761-3.376a1 1 0 0 1 .52-1.6l4.218-1.108a1 1 0 0 0 .588-.427z'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='m9.5 13 2 2 3-3.5'
      opacity={0.66}
    />
  </svg>
);
export default SvgAchievements;
