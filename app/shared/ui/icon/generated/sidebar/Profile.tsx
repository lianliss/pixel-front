import * as React from 'react';
import type { SVGProps } from 'react';
const SvgProfile = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <path
      stroke='currentColor'
      d='M9 11a2 2 0 1 0 0-4 2 2 0 0 0 0 4ZM13 15c0 1.105 0 2-4 2s-4-.895-4-2 1.79-2 4-2 4 .895 4 2Z'
    />
    <path stroke='currentColor' strokeLinecap='round' d='M19 12h-4m4-3h-5m5 6h-3' opacity={0.66} />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M12 4C3.765 4 2 5.412 2 12s1.765 8 10 8 10-1.412 10-8-1.765-8-10-8'
      clipRule='evenodd'
    />
  </svg>
);
export default SvgProfile;
