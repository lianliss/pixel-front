import * as React from 'react';
import type { SVGProps } from 'react';
const SvgQuest = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M12 4c-5.765 0-7 1.412-7 8s1.235 8 7 8 7-1.412 7-8-1.235-8-7-8'
      clipRule='evenodd'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M12 4c-2.47 0-3 .265-3 1.5S9.53 7 12 7s3-.265 3-1.5S14.47 4 12 4'
      clipRule='evenodd'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M8 10.357 9.2 11.5l1.8-2M8 14.357 9.2 15.5l1.8-2'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M13.5 10.5H16M13.5 13.5H16M13.5 16.5H16'
      opacity={0.66}
    />
  </svg>
);
export default SvgQuest;
