import * as React from 'react';
import type { SVGProps } from 'react';
const SvgMarketplace = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M12 6c-5.765 0-7 1.324-7 7.5S6.235 21 12 21s7-1.323 7-7.5S17.765 6 12 6'
      clipRule='evenodd'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M15 6a3 3 0 1 0-6 0'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M15 9a3 3 0 1 1-6 0'
      opacity={0.66}
    />
  </svg>
);
export default SvgMarketplace;
