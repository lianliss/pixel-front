import * as React from 'react';
import type { SVGProps } from 'react';
const SvgHome = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M5 9v6.654c0 3.99 1.235 4.846 7 4.846s7-.855 7-4.846V9.5'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='m21.383 11.617-5.498-5.498c-3.296-3.296-4.708-3.296-8.004 0l-5.498 5.498'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M10 20v-5c0-1.647.353-2 2-2s2 .353 2 2v5'
      opacity={0.66}
    />
  </svg>
);
export default SvgHome;
