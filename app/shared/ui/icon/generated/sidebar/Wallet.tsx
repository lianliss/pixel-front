import * as React from 'react';
import type { SVGProps } from 'react';
const SvgWallet = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M20.264 9c-.659-3.219-2.87-4-8.764-4-7.412 0-9 1.235-9 7s1.588 7 9 7c5.894 0 8.105-.781 8.764-4'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M19 9c-6.177 0-6.5.53-6.5 3s.323 3 6.5 3c.311 0 .949-.001 1.52-.004a.986.986 0 0 0 .98-.99V9.995a.986.986 0 0 0-.98-.99C19.948 9.001 19.31 9 19 9'
      clipRule='evenodd'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M5 8.5h5M5 10.5h3'
      opacity={0.66}
    />
    <circle cx={15.5} cy={12} r={1} fill='currentColor' />
  </svg>
);
export default SvgWallet;
