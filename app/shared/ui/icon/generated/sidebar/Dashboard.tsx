import * as React from 'react';
import type { SVGProps } from 'react';
const SvgDashboard = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M17 3c3.294 0 4 .794 4 4.5s-.706 4.5-4 4.5-4-.794-4-4.5.706-4.5 4-4.5'
      clipRule='evenodd'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M17 15c3.294 0 4 .53 4 3s-.706 3-4 3-4-.53-4-3 .706-3 4-3M6 3c3.294 0 4 .441 4 2.5S9.294 8 6 8s-4-.441-4-2.5S2.706 3 6 3'
      clipRule='evenodd'
      opacity={0.66}
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M6 11c3.294 0 4 .882 4 5s-.706 5-4 5-4-.882-4-5 .706-5 4-5'
      clipRule='evenodd'
    />
  </svg>
);
export default SvgDashboard;
