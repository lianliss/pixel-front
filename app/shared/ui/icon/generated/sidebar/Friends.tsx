import * as React from 'react';
import type { SVGProps } from 'react';
const SvgFriends = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <path
      stroke='currentColor'
      d='M12 11a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5ZM17 16c0 1.381 0 2.5-5 2.5S7 17.381 7 16s2.238-2.5 5-2.5 5 1.119 5 2.5Z'
    />
    <g stroke='currentColor' opacity={0.66}>
      <path d='M5.5 12a1.75 1.75 0 1 0 0-3.5 1.75 1.75 0 0 0 0 3.5Z' />
      <path
        strokeLinecap='round'
        strokeLinejoin='round'
        d='M5.5 17.25C2 17.25 2 16.467 2 15.5s1.566-1.75 3.5-1.75q.523 0 1 .072'
      />
    </g>
    <g stroke='currentColor' opacity={0.66}>
      <path d='M18.5 12a1.75 1.75 0 1 0 0-3.5 1.75 1.75 0 0 0 0 3.5Z' />
      <path
        strokeLinecap='round'
        strokeLinejoin='round'
        d='M18.5 17.25c3.5 0 3.5-.783 3.5-1.75s-1.566-1.75-3.5-1.75q-.523 0-1 .072'
      />
    </g>
  </svg>
);
export default SvgFriends;
