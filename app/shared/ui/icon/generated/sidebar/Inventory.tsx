import * as React from 'react';
import type { SVGProps } from 'react';
const SvgInventory = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M12 3c-7.412 0-9 .618-9 3.5S4.588 10 12 10s9-.618 9-3.5S19.412 3 12 3'
      clipRule='evenodd'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M4.112 9C4.032 9.875 4 10.87 4 12c0 7.412 1.412 9 8 9s8-1.588 8-9c0-1.13-.033-2.125-.112-3'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M10 14h4'
      opacity={0.66}
    />
  </svg>
);
export default SvgInventory;
