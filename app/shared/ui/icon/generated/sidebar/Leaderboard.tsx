import * as React from 'react';
import type { SVGProps } from 'react';
const SvgLeaderboard = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M4.5 15c-1.647 0-2 .53-2 3s.353 3 2 3 2-.53 2-3-.353-3-2-3'
      clipRule='evenodd'
      opacity={0.55}
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M12 3c-2.059 0-2.5 1.588-2.5 9s.441 9 2.5 9 2.5-1.588 2.5-9-.44-9-2.5-9'
      clipRule='evenodd'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M19.5 8c-1.647 0-2 1.147-2 6.5s.353 6.5 2 6.5 2-1.147 2-6.5-.353-6.5-2-6.5'
      clipRule='evenodd'
      opacity={0.66}
    />
  </svg>
);
export default SvgLeaderboard;
