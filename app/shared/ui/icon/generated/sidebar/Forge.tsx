import * as React from 'react';
import type { SVGProps } from 'react';
const SvgForge = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <g stroke='currentColor' strokeLinecap='round' strokeLinejoin='round' opacity={0.66}>
      <path
        d='M14.45 2.52c-1.591.426-1.773 1.114-1.027 3.898s1.247 3.29 2.838 2.863c1.591-.426 1.772-1.114 1.026-3.899a17 17 0 0 0-.439-1.435C16.29 2.43 15.715 2.18 14.45 2.52'
        clipRule='evenodd'
      />
      <path d='M12.697 5.637c-.711.146-1.632.377-2.825.697C5.822 7.42 5 7.822 5.212 8.618c.214.795 1.128.733 5.177-.352 1.193-.32 2.106-.58 2.795-.809' />
    </g>
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M13 19.5c-4.187 0-5.587.38-5.916 2h11.832c-.33-1.62-1.728-2-5.916-2'
      clipRule='evenodd'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M9 16s1 .778 1 1.945c0 1.166-.5 1.555-.5 1.555M17 16s-1 .778-1 1.945c0 1.166.5 1.555.5 1.555'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M14.5 12h-6v4h6.857c3.857 0 4.714-1.5 5.143-4zM6.086 13H8.5v3H6.741C5.19 16 3.672 14.875 3.5 13z'
      clipRule='evenodd'
    />
  </svg>
);
export default SvgForge;
