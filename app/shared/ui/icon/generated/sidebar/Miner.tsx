import * as React from 'react';
import type { SVGProps } from 'react';
const SvgMiner = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns='http://www.w3.org/2000/svg' width={24} height={24} fill='none' {...props}>
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M10.518 4.67c1.093-1.439 1.66-1.573 2.316-1.194.656.378.823.936.124 2.603M8.703 7.42c-.56.914-1.203 2.006-1.952 3.304-3.837 6.645-4.408 8.215-3.235 8.892 1.172.677 2.246-.602 6.083-7.248.749-1.297 1.373-2.4 1.885-3.342'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M11.738 5.375c4.747 2.74 5.001 6.685 5.001 6.685L3.447 4.386s3.544-1.752 8.29.989'
    />
    <circle
      cx={18}
      cy={18}
      r={3}
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
    <path
      stroke='currentColor'
      strokeLinecap='round'
      strokeLinejoin='round'
      d='M13.5 19a2.5 2.5 0 1 1 1.743-4.293'
      opacity={0.66}
    />
  </svg>
);
export default SvgMiner;
