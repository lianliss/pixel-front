import * as React from 'react';
import type { SVGProps } from 'react';
const SvgPixelMining = (props: SVGProps<SVGSVGElement>) => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    xmlnsXlink='http://www.w3.org/1999/xlink'
    width={24}
    height={24}
    fill='none'
    {...props}
  >
    <path fill='url(#pixel-mining_svg__a)' d='M0 0h24v24H0z' />
    <defs>
      <pattern
        id='pixel-mining_svg__a'
        width={1}
        height={1}
        patternContentUnits='objectBoundingBox'
      >
        <use xlinkHref='#pixel-mining_svg__b' transform='scale(.005)' />
      </pattern>
      <image
        xlinkHref='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAG5ElEQVR4nO3dP4wUVRwH8NuEQhItLEzkLjaoiQWF4TBc659OODsTCxtjwMLEgpKCo6C0IKEQQii1NB7YqTUmd4RCo4lCowcmV1hIYueKBnHn3d3MzW/m7c7Mfj7VDYe74y7f/N5v3nszBxaAPR3Y8zeAgEAZFQQEBGJUEBAQiFFBQEAgRgUBAYEYFQQEBGJUEBAQiFFBQEAgRgUBAYEYFQQEBGJUEBAQiFFBQEAgRgUBAYEYFQQEBGJUEBAQiFFBQEAgRgXpj3HG1x5lfO1eExAQEIhRQUBA+t9TbK2/UDhePBJvSe59V2w5llZ/Tl9MT/KICgIlBAQEBGJUkOkZ1+opDr6W7UQWj3xTOB7ffb5wPDp8R0/yiIBACQEBAYEYFWQOeo4d0vf6s9iTpOe6NMfzJAICAgIxKggIyJz3HKmk50jXZqW25rgnUUFAQCBGBQEBmfOeI+k7Nj9/qvCrQy8+qPVSGx+/XDg+dub2YHsSFQQEBGJUkHrGfRxSpcOqukOqVNP/vk8EBAQEYlQQEJA8l3Lr9ADZe5KSniN337Ax4Mu+KggICMSoICAgHZD2JANyaMDzIioICAjEqCAgILsbj8sfITAa7X35/v5PTxaOF4/8Ufpa9JMKAgICMSoICMj+POw5ws81q7p1Tp+k/dU8r81SQUBAIEYFgTkOyLjOPEf6KLIdDr76+MfFV+InNY21Xs3mZR7U6lGOnbnd4L26begBgUYEBAQEYg4MuefY0VPM+l5VOVX9v5XsR6maw7lf0XNcfH+r/L0TH11d6s28yNACAq0SEBAQmI8KUmut1OjwneRP0mMiLtbsOfqsbwGBqRIQEBAYZgUp9ByfXFov/PK5l5YbvfgvP24Wjj/4cLVw/O258PaQQXkm+Zivr99rtYfp8rxI1wMCMyUgICAwjApSGIt++VXe6+1pD5O+3/E3ikPhee1Jtout2sLJ1cVWe5Iu61pAoFMEBAQEulNBygbqoyY9RzpvkUrnMabp+PnOXLr/1+qJc4Xjs8tr2d5rZanYk9zcujeYeRFDLBAQmF0FKZTDjUvvFn65eev3xz+fvnYj95CqqhSP27pse2FzrdajFOoqe/RC5P3eOrnW2nBre7N8KcrCgFbDG2KBgECMCgLTDMhkz/GP5aNPP/5542jSn3x6unA8Pnoqa8/RdKvo5KXctnuO3L64vrZnf9N0Cc12y0tRunTZVwUBAYEYFQQyB6QwHkznOi4vnNi1H9ntePPWlRZOZ/9LImapap5jSFbyLkXJ+kGqICAgEKOCQOaAlK6v+nVirqNsjmS34/Tv1z2XebpFZpdsD2itlgoCAgIxKgi0HJBaezom11eNknmOuj3J5JzKbnMuac+RXn9vanLN0uT+inSt035Mey3X5PlO+/ZF2y2v1ZrcTrx+43zWdVoqCAgIxKgg0DAgre0jf9gzFI4vv3ei3Z7k6lLhOOdYO93X3fW1VV2+bepKzbVarz/7f1+7vnA+yzn9RwUBAYEYFQRqBmRqtwPN3ZOktwPNOQ7v8hi/a7ZbXKuV3mK17XkRFQQEBGJUEKgISOmjlqdwv9y997PX7EnSnqbuIwr0EbvPQ6xk3svfZK3W5JxIjnkRFQQEBGJUEKgTkPTRyFWa9By177GV9CRXPrvbqIdIx9rTnDfpsq9/K+7bWVnK9/i23b6Hk8uL4bVabc+LqCAgIBCjgkCbAWm550iV7jFP93vcXR+XXk+vko5t0/eb154k3etyPPPnkH4P6bxHOi9Stlar7XkRFQQEBGJUEOhrQKrWAFWNVZv2JPOqque4kDwjPh33t/05pt/z5OtX7V9v+rzDTgcEZk1AQECgnxWkdN6j7rPsKq+fV/z9J/4qPrc9Xfs1OTaf9ZxI+tk8HFu39tpVz1U5m8yTtK1q7dXkcZ11WpG1WrMOCHSagECdgKRbausuf88pHdaklyPToUHVbfW//+HtwvGpdw6Xn8C1ihOconRoMeshX84tvmXDqPSWQSsLVZeY6y1FUUFAQCBGBYGKgIyS5ey1bj2aU9UlvKqepErd2woxm+855xKgqsu+KgiUEBAQEGivghQG8m++sWN58NTUXWqS9iRVY9nKniWZ9+jzXEOf3Sz5d5BuaUiX4jdliAUCAjEqCDQMSM7nG4/KtkNWLbsuG5vu5/q5nqIbbtbsNSe/16Y9x8N5j/SPLHeH/TLEAgGBYVSQ0p4Ecv+b63pAoFMEBAQEhllBcs7BQO8DAjMlICAgEKOCgIBAjAoCAgIxKggICMSoICAgEKOCgIBAjAoCAgIxKggICMSoICAgEKOCgIBAjAoCAgIxKggICMSoICAgEKOCgIBAjAoCAgIxKggICMT8DRrBPiCaJ6eQAAAAAElFTkSuQmCC'
        id='pixel-mining_svg__b'
        width={200}
        height={200}
      />
    </defs>
  </svg>
);
export default SvgPixelMining;
