import React from 'react';
import { Tooltip } from '@blueprintjs/core';
import { IconHints } from '@ui-kit/icon/common/IconHints.tsx';

type Props = {
  content?: string;
};
const HintPage = ({ content = '' }: Props) => {
  return (
    <Tooltip content={content}>
      <IconHints />
    </Tooltip>
  );
};

export default HintPage;
