import { useUpdateInterval } from 'utils/hooks/useUpdateInterval';
import { diffTime } from 'utils/format/date';
import { useEffect } from 'react';
import moment from 'moment/moment';

type Props = { date: Date; onStart?: () => void; short?: boolean };

export function Timer(props: Props) {
  const { date, onStart, short = false } = props;

  useUpdateInterval(1000);

  const result = diffTime(date, short);

  useEffect(() => {
    if (!onStart) {
      return;
    }

    const inverseOffset = moment().utcOffset();
    const now = moment().add(1, 'second');
    const target = moment(date).add(inverseOffset / 60, 'hour');

    if (target <= now) {
      setTimeout(() => {
        onStart?.();
      }, 500);
    }
  }, [result]);

  return result;
}
