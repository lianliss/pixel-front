import React from 'react';
import MuiTextField, { type TextFieldProps } from '@mui/material/TextField';

export const TextField = (props: TextFieldProps) => {
  return <MuiTextField {...props} />;
};
