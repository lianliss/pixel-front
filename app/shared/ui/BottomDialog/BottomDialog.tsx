import React from 'react';
import styles from './BottomDialog.module.scss';
import clsx from 'clsx';
import Dialog from '@mui/material/Dialog';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import DialogContent from '@mui/material/DialogContent';
import { styled } from '@mui/material/styles';
import CloseIcon from '@mui/icons-material/Close';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Divider from '@mui/material/Divider';

const Line = styled('div')(({ theme }) => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'center',

  '& > div': {
    flex: 1,
    maxWidth: 70,
    height: 3,
    backgroundColor: theme.palette.divider,
    borderRadius: 1,
  },
}));
const CloseButton = styled(IconButton)(({ theme }) => ({
  position: 'absolute',
  top: 10,
  right: 10,
  border: `solid 1px ${theme.palette.divider}`,
  padding: 5,
}));
const StyledDialog = styled(Dialog)(() => ({
  '& > .MuiDialog-container': {
    alignItems: 'flex-end',
  },
}));
const StyledPaper = styled(Paper)(({ theme }) => ({
  '&.MuiPaper-root': {
    position: 'relative',
    margin: 0,
    width: '100%',
    borderRadius: '16px 16px 0 0',
    borderTop: `solid 1px ${theme.palette.divider}`,
    backgroundColor: theme.palette.background.paper,
  },
}));

export type IBottomDialogProps = {
  children: React.ReactNode;
  footer?: React.ReactNode;
  className?: string;
  title?: string;
  subTitle?: string;
  isOpen?: boolean;
  open?: boolean;
  onClose?: () => void;
  keepMounted?: boolean;
};

const BottomDialog = (props: IBottomDialogProps) => {
  const {
    children,
    className,
    title,
    subTitle,
    footer,
    isOpen,
    open,
    onClose,
    keepMounted,
    ...other
  } = props;

  return (
    <StyledDialog
      fullWidth
      PaperComponent={StyledPaper}
      open={isOpen || open || false}
      onClose={onClose}
      className={clsx(styles.bottomDialog, className)}
      keepMounted={keepMounted}
      maxWidth='xs'
      {...other}
    >
      <DialogContent
        sx={{ overflowX: 'hidden', paddingBottom: '32px', position: 'relative', pt: 1.5 }}
        className={styles.bottomDialog__content}
      >
        {onClose && (
          <CloseButton sx={{ color: 'text.primary' }} onClick={onClose}>
            <CloseIcon />
          </CloseButton>
        )}

        <Line sx={{ mb: 2 }}>
          <div />
        </Line>

        {title && (
          <Typography color='text.primary' fontWeight='bold' align='center'>
            {title}
          </Typography>
        )}
        {subTitle && (
          <Typography
            color='text.primary'
            variant='subtitle2'
            className={styles.bottomDialog__subTitle}
            sx={{ mt: 1, opacity: 0.6 }}
          >
            {subTitle}
          </Typography>
        )}

        {children}
      </DialogContent>
      {!!footer && <Divider sx={{ mb: 2 }} />}
      {footer}
    </StyledDialog>
  );
};

export default BottomDialog;
