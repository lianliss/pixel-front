import React from 'react';
import { styled, darken } from '@mui/material/styles';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import { LinearProgressProps } from '@mui/material/LinearProgress/LinearProgress';

const StyledRoot = styled('div')(({ theme }) => ({
  backgroundColor: darken(theme.palette.background.default, 0.05),
}));

type Props = {
  value: number;
  total: number;
  size?: 'medium' | 'small';
  noZero?: boolean;
  color?: LinearProgressProps['color'];
} & React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

export const ProgressBar = ({ value, total, size = 'small', noZero, color, ...other }: Props) => {
  const percentage = (total === 0 ? 0 : (Math.min(value, total) / total) * 100) || (noZero ? 1 : 0);

  return (
    <StyledRoot
      style={{
        position: 'relative',
        width: '100%',
        borderRadius: size === 'small' ? '26px' : '32px',
        padding: 4,
        overflow: 'hidden',
        boxSizing: 'border-box',
        ...(other.style || {}),
      }}
      {...other}
    >
      <Box sx={{ width: '100%' }}>
        <LinearProgress
          variant='determinate'
          value={percentage}
          color={color}
          sx={size === 'small' ? undefined : { height: 8, borderRadius: '32px' }}
        />
      </Box>
    </StyledRoot>
  );
};
