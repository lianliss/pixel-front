import React, { ReactElement } from 'react';

import styles from './styles.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { ProgressBar } from '@ui-kit/ProgressBar/ProgressBar.tsx';
import Box, { BoxProps } from '@mui/material/Box';

type Props = {
  icon?: ReactElement;
  name: string;
  description?: string;
  value?: number;
  total: number;
  size?: 'small' | 'medium';
} & BoxProps;

const ProgressLine = (props: Props) => {
  const { icon, name, description, value = 0, total = 0, size, ...other } = props;

  return (
    <Box className={styles.progress__line} {...other}>
      <div className={styles.head}>
        <div className={styles.name__line}>
          {icon}
          <Typography variant='caption' fontWeight='bold'>
            {name}
          </Typography>
        </div>
        <Typography sx={{ fontSize: 10 }}>{!!description && description}</Typography>
      </div>
      <ProgressBar value={value} total={total} size={size} />
    </Box>
  );
};

export default ProgressLine;
