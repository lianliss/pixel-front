import React from 'react';
import type { TabProps as MuiTabProps } from '@mui/material/Tab';
import MuiTab from '@mui/material/Tab';
import { styled } from '@mui/material/styles';
import { alpha } from '@mui/system/colorManipulator';

interface TabProps extends MuiTabProps {
  count?: number;
}

const PREFIX = 'Tab';
const classes = {
  count: `${PREFIX}_count`,
  label: `${PREFIX}_label`,
};

const StyledTab = styled(MuiTab)(({ theme }) => ({
  [`& .${classes.label}`]: {
    display: 'flex',
    alignItems: 'center',
    gap: '4px',
  },
  [`& .${classes.count}`]: {
    padding: '1px 5px',
    background: alpha(theme.palette.primary.main, 0.3),
    color: alpha(theme.palette.text.primary, 0.8),
    fontSize: 12,
    borderRadius: '3px',
  },
}));

export const Tab = (props: TabProps) => {
  const { label, count, ...other } = props;

  return (
    <StyledTab
      {...other}
      label={
        <span className={classes.label}>
          {label}
          {count > 0 && <span className={classes.count}>{count}</span>}
        </span>
      }
    />
  );
};
