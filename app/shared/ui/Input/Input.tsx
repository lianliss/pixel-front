import React from 'react';
import MuiInput, { type InputProps } from '@mui/material/Input';

export const Input = (props: InputProps) => {
  return <MuiInput {...props} disableUnderline={true} />;
};
