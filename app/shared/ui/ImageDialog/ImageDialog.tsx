import Dialog, { DialogProps } from '@mui/material/Dialog';
import React from 'react';

type IImageDialogProps = Omit<DialogProps, 'children'> & { image?: string };

const ImageDialog: React.FC<IImageDialogProps> = (props) => {
  const { image, ...other } = props;

  return (
    <Dialog {...other}>
      <img src={image} alt='image' />
    </Dialog>
  );
};

export default ImageDialog;
