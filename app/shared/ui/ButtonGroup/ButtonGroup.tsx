import React from 'react';
import type { ButtonGroupProps as MuiButtonGroupProps } from '@mui/material/ButtonGroup';
import MuiButtonGroup from '@mui/material/ButtonGroup';

interface ButtonGroupProps extends MuiButtonGroupProps {}

export const ButtonGroup = (props: ButtonGroupProps) => {
  return (
    <MuiButtonGroup {...props} aria-label='Basic button group'>
      {props.children}
    </MuiButtonGroup>
  );
};
