import React from 'react';
import MuiTabs, { type TabsProps } from '@mui/material/Tabs';

export const Tabs = (props: TabsProps) => {
  return (
    <MuiTabs value={props.value} onChange={props.onChange} aria-label='tabs basic' {...props}>
      {props.children}
    </MuiTabs>
  );
};
