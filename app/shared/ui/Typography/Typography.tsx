import React from 'react';
import MuiTypography, {
  type TypographyProps as MuiTypographyProps,
} from '@mui/material/Typography';

interface TypographyProps extends MuiTypographyProps {
  black?: boolean;
  bold?: boolean;
  medium?: boolean;
  regular?: boolean;
  light?: boolean;
}

export const Typography = (props: TypographyProps) => {
  return <MuiTypography {...props} />;
};
