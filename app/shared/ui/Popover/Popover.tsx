import React from 'react';
import MuiPopover from '@mui/material/Popover';
import type { PopoverProps as MuiPopoverProps } from '@mui/material/Popover';

interface PopoverProps extends MuiPopoverProps {}

export const Popover = (props: PopoverProps) => {
  return (
    <MuiPopover
      {...props}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
    >
      {props.children}
    </MuiPopover>
  );
};
