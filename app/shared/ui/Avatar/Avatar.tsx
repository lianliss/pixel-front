import React from 'react';
import type { AvatarProps as MuiAvatarProps } from '@mui/material/Avatar';
import MuiAvatar from '@mui/material/Avatar';
import { styled } from '@mui/material/styles';
import { alpha, darken } from '@mui/material/styles';
import clsx from 'clsx';
import clipPathImg from 'assets/img/common/avatar-outlined.svg';
import { hexToRgb } from '@mui/material';

const PREFIX = 'Avatar';
const classes = {
  outlined: `${PREFIX}_outlined`,
  filled: `${PREFIX}_filled`,
  success: `${PREFIX}_success`,
  primary: `${PREFIX}_primary`,
  error: `${PREFIX}_error`,
  selected: `${PREFIX}_selected`,
  disabled: `${PREFIX}_disabled`,
};

const getBorderSvg = (color: string) =>
  `<svg viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.16794 31.9691C1.16794 25.3986 1.52058 20.1529 2.4489 15.9755C3.37596 11.8037 4.87047 8.72806 7.13323 6.46529C9.396 4.20253 12.4717 2.70802 16.6434 1.78096C20.8209 0.852642 26.0666 0.5 32.637 0.5C39.2075 0.5 44.4532 0.852642 48.6307 1.78096C52.8024 2.70802 55.8781 4.20253 58.1409 6.46529C60.4036 8.72806 61.8981 11.8037 62.8252 15.9755C63.7535 20.1529 64.1062 25.3986 64.1062 31.9691C64.1062 38.5396 63.7535 43.7853 62.8252 47.9627C61.8981 52.1345 60.4036 55.2102 58.1409 57.4729C55.8781 59.7357 52.8024 61.2302 48.6307 62.1573C44.4532 63.0856 39.2075 63.4382 32.637 63.4382C26.0666 63.4382 20.8209 63.0856 16.6434 62.1573C12.4717 61.2302 9.396 59.7357 7.13323 57.4729C4.87047 55.2102 3.37596 52.1345 2.4489 47.9627C1.52058 43.7853 1.16794 38.5396 1.16794 31.9691Z" stroke="${hexToRgb(
    color
  )}" strokeWidth="2px"/></svg>`;

const StyledAvatar = styled(MuiAvatar)(({ theme }) => ({
  [`&.${classes.outlined}`]: {
    backgroundColor: 'transparent',
    color: theme.palette.primary.main,
    border: `1px solid ${theme.palette.primary.main}`,
    // borderImage: `url('data:image/svg+xml,${getBorderSvg(theme.palette.primary.main)}')`,
    // borderImageSlice: 7,

    [`&.${classes.success}`]: {
      borderColor: theme.palette.success.main,
      color: theme.palette.success.main,
    },
    [`&.${classes.error}`]: {
      borderColor: theme.palette.error.main,
      color: theme.palette.error.main,
    },
  },
  [`&.${classes.filled}`]: {
    backgroundColor: alpha(theme.palette.primary.main, 0.2),
    color: theme.palette.primary.main,
    border: `1px solid ${theme.palette.primary.main}`,

    [`&.${classes.success}`]: {
      backgroundColor: darken(theme.palette.success.main, 0.8),
      color: theme.palette.success.main,
      border: `1px solid ${theme.palette.success.main}`,
    },
    [`&.${classes.error}`]: {
      backgroundColor: darken(theme.palette.error.main, 0.8),
      color: theme.palette.error.main,
      border: `1px solid ${theme.palette.error.main}`,
    },
  },

  [`&.${classes.selected}`]: {
    outline: `4px solid ${alpha(theme.palette.success.main, 0.5)}`,
    outlineOffset: 0,
  },
  [`&.${classes.disabled}`]: {
    opacity: 0.5,
  },
}));

type AvatarProps = MuiAvatarProps & {
  variantStyle?: 'outlined' | 'filled';
  color?: 'success' | 'primary' | 'error';
  selected?: boolean;
  disabled?: boolean;
  loading?: boolean;
};

export const Avatar = (props: AvatarProps) => {
  const {
    variantStyle,
    color,
    className,
    selected,
    disabled,
    src,
    loading = false,
    ...other
  } = props;

  return (
    <StyledAvatar
      src={loading ? undefined : src}
      className={clsx(
        {
          [classes[variantStyle]]: !!variantStyle,
          [classes[color]]: !!color,
          [classes.selected]: selected,
          [classes.disabled]: disabled,
        },
        className
      )}
      {...other}
    />
  );
};
