import React from 'react';
import MuiToggleButton, {
  type ToggleButtonProps as MuiToggleButtonProps,
} from '@mui/material/ToggleButton';

interface ToggleButtonProps extends MuiToggleButtonProps {}

export const ToggleButton = (props: ToggleButtonProps) => {
  return <MuiToggleButton {...props} />;
};
