import 'swiper/css';
import 'swiper/css/pagination';
import styles from './Swiper.module.scss';
import { Pagination as SwiperPagination } from 'swiper/modules';
import { Swiper as BaseSwiper, SwiperSlide } from 'swiper/react';
import Box, { BoxProps } from '@mui/material/Box';

export type SwiperProps<T> = { options: T[]; renderOption: (option: T) => JSX.Element } & BoxProps;

function Swiper<T>(props: SwiperProps<T>) {
  const { options = [], renderOption, ...other } = props;

  return (
    <Box {...other}>
      <BaseSwiper
        slidesPerView={1}
        className={styles.miners}
        pagination={{
          dynamicBullets: false,
        }}
        modules={[SwiperPagination]}
      >
        {options.map((option, i) => (
          <SwiperSlide key={`slide-${i}`}>{renderOption(option)}</SwiperSlide>
        ))}
      </BaseSwiper>
    </Box>
  );
}

export default Swiper;
