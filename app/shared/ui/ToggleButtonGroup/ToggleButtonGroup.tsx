import React from 'react';
import MuiToggleButtonGroup, {
  ToggleButtonGroupProps as MuiToggleButtonGroupProps,
} from '@mui/material/ToggleButtonGroup';

interface ToggleButtonGroupProps extends MuiToggleButtonGroupProps {}

export const ToggleButtonGroup = (props: ToggleButtonGroupProps) => {
  return <MuiToggleButtonGroup {...props}>{props.children}</MuiToggleButtonGroup>;
};
