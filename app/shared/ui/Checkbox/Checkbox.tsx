import React from 'react';
import type { CheckboxProps as MuiCheckboxProps } from '@mui/material/Checkbox';
import MuiCheckbox from '@mui/material/Checkbox';

interface CheckboxProps extends MuiCheckboxProps {}

export const Checkbox = (props: CheckboxProps) => {
  return <MuiCheckbox {...props} />;
};
