import { TextField } from '@ui-kit/TextField/TextField.tsx';
import React from 'react';
import { TextFieldProps } from '@mui/material/TextField';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import isValidImageUrl from 'utils/validate/url.ts';

type Props = Omit<TextFieldProps, 'value'> & { value?: string };

function ImageUrlField(props: Props) {
  const { value, ...other } = props;
  const error = value ? !isValidImageUrl(value) : false;

  return (
    <TextField
      error={error}
      value={value}
      InputProps={{
        startAdornment: (
          <Avatar variant='rounded' variantStyle='filled' src={value} sx={{ mr: 1 }} />
        ),
      }}
      {...other}
    />
  );
}

export default ImageUrlField;
