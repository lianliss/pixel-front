import { Img, ImgProps } from '@ui-kit/Img/Img.tsx';
import React from 'react';
import { styled } from '@mui/material/styles';
import hatImage from 'assets/img/decor/new-year/hat.png';

const StyledRoot = styled(Img)(() => ({
  position: 'absolute',
}));

type Props = Omit<ImgProps, 'src'> & {
  top?: number | string;
  left?: number | string;
  bottom?: number | string;
  right?: number | string;
};

export function FixedDecor(props: Props) {
  const { top, left, right, bottom, ...other } = props;

  return <StyledRoot style={{ top, left, right, bottom }} src={hatImage as string} {...other} />;
}
