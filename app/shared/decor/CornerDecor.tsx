import { Img, ImgProps } from '@ui-kit/Img/Img.tsx';
import React from 'react';
import { styled } from '@mui/material/styles';
import hatImage from 'assets/img/decor/new-year/hat.png';
import { addDynamicDecor } from './DynamicDecor.tsx';

const StyledRoot = styled(Img)(() => ({
  position: 'absolute',
  top: 0,
  width: 50,
  variants: [
    {
      props: {
        position: 'right',
      },
      style: {
        transform: 'translate(16px, -16px) scaleX(-1)',
        right: 0,
      },
    },
    {
      props: {
        position: 'left',
      },
      style: {
        transform: 'translate(-16px, -16px)',
        left: 0,
      },
    },
  ],
}));

type Props = Omit<ImgProps, 'src'> & { position?: 'left' | 'right' };

export function CornerDecor(props: Props) {
  const { position = 'left', ...other } = props;

  const ownerState = { position };
  return (
    <StyledRoot
      ownerState={ownerState}
      src={hatImage as string}
      onClick={() => addDynamicDecor()}
      {...other}
    />
  );
}
