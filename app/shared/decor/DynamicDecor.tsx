import { Img, ImgProps } from '@ui-kit/Img/Img.tsx';
import React, { useEffect, useRef } from 'react';
import { styled } from '@mui/material/styles';
import snowflake from 'assets/img/decor/new-year/snowflake.png';
import { useUpdateInterval } from 'utils/hooks/useUpdateInterval';
import { createEvent, createStore } from 'effector';
import { useUnit } from 'effector-react';

const StyledRoot = styled(Img)(() => ({
  position: 'fixed',
  width: 14,
  height: 14,
  zIndex: 10_000,
  userSelect: 'none',
  pointerEvents: 'none',
}));

const screenHeight = window.innerHeight;
const speed = 20;

function Element(props: Omit<ImgProps, 'src'>) {
  const { ...other } = props;

  const imageRef = useRef<HTMLImageElement>(null);
  const intervalRef = useRef<any | null>(null);
  const start = useRef(Math.random() * 100000);

  useEffect(() => {
    intervalRef.current = setInterval(() => {
      const top = ((Date.now() - start.current) / speed) % screenHeight;

      if (imageRef.current) {
        imageRef.current.style.top = `${top}px`;
      }
    }, 10);

    return () => {
      if (intervalRef.current) {
        clearInterval(intervalRef.current);
      }
    };
  }, []);

  return <StyledRoot ref={imageRef} src={snowflake as string} {...other} />;
}

const store = createStore([1.5, 1, 1, 1, 1.5, 1, 1.2, 1, 1.5, 1, 1.2, 1, 1, 1, 1, 1.2, 1, 1.2]);
export const addDynamicDecor = createEvent();

store.on(addDynamicDecor, (prev) => [...prev, 1]);

export function DynamicDecor() {
  const arr = useUnit(store);

  return (
    <>
      {arr.map((size, i) => (
        <Element
          key={`decor-${i}`}
          style={{ left: `${(100 / arr.length) * i}%`, transform: `scale(${size})` }}
        />
      ))}
    </>
  );
}
