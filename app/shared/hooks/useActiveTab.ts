import {useEffect} from "react";

export function useActiveTab(handler: () => void, deps: any[]) {
    useEffect(() => {
        window.addEventListener("focus", handler);

        return () => {
            window.removeEventListener("focus", handler);
        };
    }, deps);
}
