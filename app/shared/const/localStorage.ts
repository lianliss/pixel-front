export const useGaslessKey = 'use-gasless';
export const dhPrivateKey = 'dh-private';
export const dhPublicKey = 'dh-public';
export const nrfxSlippageKey = 'nrfx-slippage';
export const nrfxRefKey = 'nrfx-ref';
export const pixelSlippageKey = 'pixel-slippage';
export const walletPrivateKey = 'wallet-privateKey';
export const pixelMessageKey = 'pixel-message';
export const pixelSignKey = 'pixel-sign';
export const walletDeviceIdKey = 'wallet-deviceId';
export const telegramAuthKey = 'telegram-auth';
export const telegramIdKey = 'telegram-id';
export const langKey = 'pixel-lng';

export const tutorialForGettingGasKey = 'tutorials-for-getting-gas-key';
export const tutorialForClaimStorageKey = 'tutorials-for-claim-storage-key';

export const marketItemsFixedKey = 'market-items-fixed';
