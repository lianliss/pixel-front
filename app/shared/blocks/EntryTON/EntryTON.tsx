import Backdrop from '@mui/material/Backdrop';
import { styled } from '@mui/material/styles';
import { getDefaultChainImage } from '@cfg/image.ts';
import Box from '@mui/material/Box';
import { Button } from '@ui-kit/Button/Button.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Container from '@mui/material/Container';
import { Img } from '@ui-kit/Img/Img.tsx';
import CheckIcon from '@mui/icons-material/CheckOutlined';
import CloseIcon from '@mui/icons-material/CloseOutlined';
import CircularProgress from '@mui/material/CircularProgress';
import {PROJECT_VERSION} from "@cfg/config.ts";
import {ENTRY_SCREEN_INFO} from "../EntryScreen/constants.ts";
import {Chain} from "services/multichain/chains.js";

const InfoRow = ({
                   text,
                   loading,
                   success,
                 }: {
  text: string;
  loading?: boolean;
  success?: boolean;
}) => (
  <Box sx={{ display: 'flex', alignItems: 'center', gap: 2, width: '100%', maxWidth: 170 }}>
    {loading ? <CircularProgress size={24} /> : success ? <CheckIcon sx={{ color: 'success.main'}} /> : <CloseIcon sx={{ color: 'error.main'}} />}
    <Typography variant='subtitle2'>{text}</Typography>
  </Box>
);

const StyledBackdrop = styled(Backdrop)(() => ({
  backgroundSize: 'cover',
  backgroundColor: '#000000',
  display: 'flex',
  alignItems: 'center',
  zIndex: 10000,
}));
const StyledContainer = styled(Container)(() => ({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
  height: '100%',
  flex: 1,
  maxHeight: 600,
}));

type Props = {
  onStart?: () => void;
  loading: boolean;
  disabled: boolean;
  status?: string;

  sign?: {
    success: boolean;
    loading: boolean;
  };
  access?: {
    success: boolean;
    loading: boolean;
  };
  freeGas?: {
    success: boolean;
    loading: boolean;
  };
  rpc?: {
    success?: boolean;
    loading?: boolean;
  };
  hello?: {
    success?: boolean;
    loading?: boolean;
  };
};

export function EntryTON(props: Props) {
  const { onStart, loading, disabled} = props;

  const info = ENTRY_SCREEN_INFO[Chain.TON];

  if (!info) {
    return null;
  }

  return (
    <StyledBackdrop
      open
      style={{ backgroundImage: `url(${getDefaultChainImage(Chain.TON)})` }}
    >
      <StyledContainer maxWidth='xs'>
        <div />
        <Box sx={{ px: 2, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          {info.image && <Img src={info.image} sx={{ width: '100%', maxWidth: 250 }} />}
          <Typography variant='h6' align='center' fontWeight='bold' sx={{ fontSize: 24, mt: 12 }}>
            {info.title}
          </Typography>
          <Typography variant='subtitle2' align='center' sx={{ mt: 1 }}>
            {info.summary}
          </Typography>
        </Box>
        <Box sx={{ width: '100%', p: 1 }}>
          <Button
            variant='contained'
            size='large'
            color='primary'
            loading={loading}
            disabled={disabled}
            fullWidth
            onClick={onStart}
          >
            {'TON Connect'}
          </Button>

          <Box
            sx={{ mt: 2, display: 'flex', flexDirection: 'column', alignItems: 'center', pr: 2 }}
          >
            <InfoRow text='Wallet Connected' success={false} />
            <Typography variant='caption' color='textSecondary' align='center' component='p' sx={{ mt: 1 }}>{PROJECT_VERSION}</Typography>
          </Box>
        </Box>
      </StyledContainer>
    </StyledBackdrop>
  );
}
