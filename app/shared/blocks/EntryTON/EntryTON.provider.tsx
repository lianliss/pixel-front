import React, { useEffect, useState } from 'react';
import ChainSwitcher from '../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import {EntryTON} from "./EntryTON.tsx";
import useIsTonChain from "services/ton/hooks/useIsTonChain.tsx";
import {useTonConnectModal, useTonConnectUI} from "@tonconnect/ui-react";
import useTonConnected from "services/ton/hooks/useTonConnected.tsx";

export function EntryTONProvider(props: React.PropsWithChildren) {
  const {isTonChain} = useIsTonChain();
  const [tonConnectUI, setOptions] = useTonConnectUI();
  const { state } = useTonConnectModal();
  const isConnected = useTonConnected();
  const enabled = isTonChain && !isConnected;
  const isDisabled = false;
  const isLoading = state.status === 'opened';

  const onStartHandle = () => {
    tonConnectUI.openModal().then().catch(error => {
      console.error('[onStartHandle]', error);
    });
  };

  return (
    <>
      {props.children}
      {enabled && !isConnected && (
        <>
          <ChainSwitcher style={{ zIndex: 20000, position: 'fixed' }} />
          <EntryTON
            onStart={onStartHandle}
            loading={isLoading}
            disabled={isDisabled}
          />
        </>
      )}
    </>
  );
}
