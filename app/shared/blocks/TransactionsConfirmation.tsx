import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import { useBlock } from 'wagmi';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain } from 'services/multichain/chains';
import { useMemo } from 'react';
import { useUpdateInterval } from 'utils/hooks/useUpdateInterval';

const Root = styled(Box)(({ theme }) => ({
  position: 'fixed',
  bottom: 16,
  left: 16,
  background: theme.palette.primary.main,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: 12,
  padding: '4px 6px',
  // width: 20,
  // height: 20,
  fontSize: 12,
  lineHeight: '12px',
  color: theme.palette.primary.contrastText,
}));

function loadConfirmations(chainId: number) {
  const list = localStorage;
  const blocks: number[] = [];

  for (const key in list) {
    if (key.startsWith(`confirm-${chainId}-`)) {
      blocks.push(Number(list[key]));
    }
  }

  return blocks;
}

export function TransactionsConfirmation() {
  const account = useAccount();
  const isEnabled = account.chainId === Chain.UNITS;

  const finalizedBlock = useBlock({
    blockTag: 'finalized',
    query: {
      enabled: isEnabled,
    },
  });
  const finalizedBlockNumber = finalizedBlock.data ? Number(finalizedBlock.data.number) : undefined;

  const date = useUpdateInterval(1000);

  const list = useMemo(() => {
    const data = loadConfirmations(account.chainId);

    return finalizedBlockNumber ? data.filter((el) => el > finalizedBlockNumber) : [];
  }, [finalizedBlockNumber, account.chainId, date]);

  if (list.length === 0) {
    return null;
  }

  return <Root>{list.length} tx pending..</Root>;
}
