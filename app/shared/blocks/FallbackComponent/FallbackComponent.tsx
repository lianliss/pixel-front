import React from 'react';
import { FallbackProps } from 'react-error-boundary';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import styles from './FallbackComponent.module.scss';
import { BUILD_ID } from '@cfg/config.ts';
import { Button } from '@ui-kit/Button/Button.tsx';

function FallbackComponent(props: FallbackProps) {
  return (
    <div className={styles.root}>
      <Typography variant='h6' fontWeight='bold'>
        Failed to load wallet
      </Typography>
      <Typography variant='caption'>
        BUILD_ID:{' '}
        <Typography component='span' variant='caption' fontWeight='bold'>
          {BUILD_ID}
        </Typography>
      </Typography>
      <Typography variant='subtitle1' align='center' sx={{ mt: 2 }}>
        {props.error?.message}
      </Typography>

      <Button
        variant='contained'
        color='primary'
        sx={{ mt: 2 }}
        onClick={() => {
          window.location.reload();
          localStorage.removeItem('wagmi.store');
        }}
      >
        Reload Wallet
      </Button>
    </div>
  );
}

export default FallbackComponent;
