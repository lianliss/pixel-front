import { createEvent, createStore } from 'effector';

type IState = {
  open: boolean;
  gas: bigint;
  fee: bigint;
  cancel: () => void | null;
  handler: () => Promise<void> | null;
};

export const transactionConfirmStore = createStore<IState>({
  open: false,
  handler: null,
  cancel: null,
  gas: 0n,
  fee: 0n,
});

//

export const showTransactionConfirm = createEvent<{
  handler: () => Promise<void>;
  cancel: () => void;
  cancel: () => void;
  gas: bigint;
  fee: bigint;
}>();

export const hideTransactionConfirm = createEvent();

//

transactionConfirmStore.on(showTransactionConfirm, (_prev, payload) => ({
  open: true,
  handler: payload.handler,
  cancel: payload.cancel,
  gas: payload.gas,
  fee: payload.fee,
}));

transactionConfirmStore.on(hideTransactionConfirm, (state, _payload) => {
  return { open: false, handler: null, cancel: null, gas: 0n, fee: 0n };
});
