import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { useUnit } from 'effector-react';
import { hideTransactionConfirm, transactionConfirmStore } from './transaction.store.ts';
import wei from 'utils/wei';
import getFinePrice from 'utils/getFinePrice';
import Typography from '@mui/material/Typography';
import { Button } from '@ui-kit/Button/Button.tsx';
import React, { useMemo } from 'react';
import Box from '@mui/material/Box';
import { formatEther, formatGwei } from 'viem';
import { useOracleTokenPrice } from 'lib/Wallet/api-server/oracle.store.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useToken } from '@chain/hooks/useToken.ts';
import Alert from '@mui/material/Alert';

type Props = {};

export function TransactionConfirmDialog(props: Props) {
  const account = useAccount();
  const token = useToken({});

  const state = useUnit(transactionConfirmStore);
  const rate = useOracleTokenPrice({
    address: token.data?.address,
    chainId: account.chainId,
  });

  const info = useMemo(() => {
    return {
      gas: formatGwei(state.gas),
      fee: formatEther(state.fee),
      sum: Number(formatGwei(state.gas)) + Number(formatEther(state.fee)),
    };
  }, [state.gas, state.fee]);

  const symbol = token.data?.symbol;

  return (
    <BottomDialog
      open={state.open}
      onClose={() => {
        hideTransactionConfirm();
        state.cancel();
      }}
      {...props}
    >
      <Typography align='center' fontWeight='bold' variant='h6' sx={{ mb: 1, fontSize: 18 }}>
        Transaction Request
      </Typography>

      <Alert severity='warning' variant='outlined' sx={{ mb: 2 }}>
        You see this message because transaction cost more than to usual
      </Alert>

      <Typography
        align='center'
        variant='subtitle2'
        sx={{ fontSize: 18 }}
        component='p'
        fontWeight='bold'
      >
        Chain Commission
      </Typography>
      <Typography
        align='center'
        variant='subtitle2'
        component='p'
        sx={{ mt: 1, fontSize: 22 }}
        fontWeight='bold'
      >
        {info.sum} {symbol}
      </Typography>

      {/*<Typography align='center' variant='subtitle2' component='p' sx={{ opacity: 0.5 }}>*/}
      {/*    {info.gas} + {info.fee} {symbol}*/}
      {/*</Typography>*/}

      <Typography align='center' variant='subtitle2' component='p' sx={{ opacity: 0.5 }}>
        {+(Number(info.sum) * rate).toFixed(6)}$
      </Typography>
      <Box sx={{ width: '100%', mt: 2, display: 'flex', gap: 1 }}>
        <Button
          fullWidth
          variant='contained'
          color='error'
          onClick={() => {
            state.cancel();
            hideTransactionConfirm();
          }}
        >
          Cancel
        </Button>
        <Button
          fullWidth
          variant='contained'
          color='primary'
          onClick={() => {
            state.handler();
            hideTransactionConfirm();
          }}
        >
          Confirm
        </Button>
      </Box>
    </BottomDialog>
  );
}
