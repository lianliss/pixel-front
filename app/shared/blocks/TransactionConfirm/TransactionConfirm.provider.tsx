import React from 'react';
import { TransactionConfirmDialog } from './TransactionConfirm.tsx';
import { TransactionsConfirmation } from '../TransactionsConfirmation.tsx';

export function TransactionConfirmProvider(props: React.PropsWithChildren) {
  return (
    <>
      {props.children}
      <TransactionConfirmDialog />
      <TransactionsConfirmation />
    </>
  );
}
