import { ENTRY_SCREEN_INFO } from './constants.ts';
import { Chain } from 'services/multichain/chains';

function getStorageKey(chainId: number): string {
  if (chainId === Chain.UNITS) {
    return `start-x1-${chainId}`;
  }

  return `start-4-${chainId}`;
}

export function setEntryScreenHidden(chainId: number): void {
  localStorage.setItem(getStorageKey(chainId), 'yes');
}

export function clearEntryScreenHidden() {
  localStorage.removeItem(getStorageKey(Chain.SONGBIRD));
  localStorage.removeItem(getStorageKey(Chain.SKALE));
  localStorage.removeItem(getStorageKey(Chain.UNITS));
  localStorage.removeItem(getStorageKey(Chain.SKALE_TEST));
}

export function isEntryScreen(chainId: number): boolean {
  return chainId in ENTRY_SCREEN_INFO && !localStorage.getItem(getStorageKey(chainId));
}
