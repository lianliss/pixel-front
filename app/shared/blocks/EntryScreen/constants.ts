import { Chain } from 'services/multichain/chains';
import unitsImage from 'assets/img/wallet/entry/units.png';

type IEntry = {
  title: string;
  summary: string;
  image?: string;
};

export const ENTRY_SCREEN_INFO: Record<number, IEntry> = {
  [Chain.TON]: {
    title: 'Welcome to TON',
    summary: `FOUNDATIONAL LAYER CONNECTING ALL ECOSYSTEM CHAINS IN A FULLY INTEROPERABLE AND TRUSTLESS MANNER.`,
  },
  [Chain.UNITS]: {
    title: 'Welcome to UNITS',
    summary: `FOUNDATIONAL LAYER CONNECTING ALL ECOSYSTEM CHAINS IN A FULLY INTEROPERABLE AND TRUSTLESS MANNER.`,
    image: unitsImage as string,
  },
  [Chain.SONGBIRD]: {
    title: 'Welcome to Songbird',
    summary: `FOUNDATIONAL LAYER CONNECTING ALL ECOSYSTEM CHAINS IN A FULLY INTEROPERABLE AND TRUSTLESS MANNER.`,
    // image: sgbImage as string,
  },
  [Chain.SKALE]: {
    title: 'Welcome to SKALE',
    summary: `FOUNDATIONAL LAYER CONNECTING ALL ECOSYSTEM CHAINS IN A FULLY INTEROPERABLE AND TRUSTLESS MANNER.`,
    // image: skaleImage as string,
  },
  [Chain.SKALE_TEST]: {
    title: 'Welcome to SKALE TESTNET',
    summary: `FOUNDATIONAL LAYER CONNECTING ALL ECOSYSTEM CHAINS IN A FULLY INTEROPERABLE AND TRUSTLESS MANNER.`,
    // image: skaleImage as string,
  },
};
