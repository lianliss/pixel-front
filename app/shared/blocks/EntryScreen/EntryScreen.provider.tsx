import React, { useEffect, useState } from 'react';
import { EntryScreen } from './EntryScreen.tsx';
import { isEntryScreen, setEntryScreenHidden } from './utils.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import useFreeGas from 'lib/Wallet/hooks/useFreeGas.ts';
import { Chain } from 'services/multichain/chains';
import { useMiningAccess } from 'lib/Mining/hooks/useMiningAccess.ts';
import { useBlock } from 'wagmi';
import { createSign, hasSign } from '@chain/auth/create-sign.ts';
import { getSelfUserInfo } from 'lib/User/api-server/user.store.ts';
import ChainSwitcher from '../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import {useHello} from "lib/Hello/hooks/useHello.ts";
import toaster from "services/toaster.tsx";
import useIsTonChain from "services/ton/hooks/useIsTonChain.tsx";
import {minerApi} from "lib/Mining/api-server/miner.api.ts";
import {IS_TELEGRAM} from "@cfg/config.ts";

function getStatusText({
  isClaimGas,
  isAccessLoading,
  isInitialize,
  isRequireAccess,
  isFreeGas,
  isAccess,
  isRpcConnected,
  isRpcConnect,
}: {
  isClaimGas?: boolean;
  isAccessLoading?: boolean;
  isInitialize?: boolean;
  isRequireAccess?: boolean;
  isFreeGas?: boolean;
  isAccess?: boolean;
  isRpcConnected?: boolean;
  isRpcConnect?: boolean;
}): string {
  if (isRpcConnect) {
    return 'Connect to RPC';
  }
  if (!isRpcConnected) {
    return 'No connection to RPC';
  }
  if (isInitialize) {
    return 'Initialize..';
  }
  if (isAccessLoading && isRequireAccess) {
    return 'Grant Access..';
  }
  if (isClaimGas && isFreeGas) {
    return 'Receive Free Gas..';
  }
  if (!isAccess) {
    return 'No Access';
  }
}

export function EntryScreenProvider(props: React.PropsWithChildren) {
  const account = useAccount();

  const [_update, setUpdate] = useState(Date.now());
  const [hidden, setHidden] = useState(false);
  const [loadingSign, setLoadingSign] = useState(false);

  const {isTonChain} = useIsTonChain();
  const enabled = !isTonChain && isEntryScreen(account.chainId);
  const freeGas = useFreeGas();
  const miningAccess = useMiningAccess();
  const hello = useHello();
  const [backendUserId, setBackendUserId] = useState(null);

  const block = useBlock({
    blockTag: 'latest',
  });

  const isSign = hasSign(account.accountAddress);
  const isRequireAccess = account.chainId === Chain.UNITS;
  const isDisabled = (isRequireAccess
    ? !miningAccess.access ||
      miningAccess.loadingAccess ||
      freeGas.claimLoading ||
      freeGas.loading ||
      block.isPending ||
      hello.loading ||
      !hello.received
    : block.isPending) || loadingSign;
  const isLoading = (isRequireAccess
    ? miningAccess.loadingAccess || freeGas.claimLoading || block.isPending || hello.loading
    : block.isPending) || loadingSign;

  useEffect(() => {
    if (!hidden && account.isConnected && freeGas.enabled && freeGas.available && enabled) {
      freeGas.claim().catch(console.error);
    }
  }, [account.isConnected, freeGas.enabled, freeGas.available, hidden]);
  useEffect(() => {
    if (account.isConnected && !isSign) {
      setLoadingSign(true);

      createSign(account.accountAddress)
        .then(() => {
          setUpdate(Date.now());
        })
        .finally(() => setLoadingSign(false));
    }
  }, [account.isConnected, account.accountAddress, isSign]);
  useEffect(() => {
    if (account.isConnected && isSign && !hidden) {
      getSelfUserInfo({ userAddress: account.accountAddress }).then();
    }
  }, [account.isConnected, account.accountAddress, isSign, hidden]);
  useEffect(() => {
    if (account.isConnected && isSign && !hidden && hello.isFetched && !hello.error && miningAccess.access && !hello.received) {
      hello.claim().then().catch(e => toaster.captureException(e));
    }
  }, [account.isConnected && isSign && !hidden, account.chainId, hello.isFetched, hello.error, miningAccess.access, hello.received]);
  useEffect(() => {
    if (account.isConnected && !IS_TELEGRAM) {
      minerApi.getTelegramId().then(telegramId => {
        setBackendUserId(telegramId);
      }).catch(error => {
        console.error('[getTelegramId]', error);
      })
    }
  }, [account.isConnected]);

  const onStartHandle = async () => {
    setHidden(true);
    setEntryScreenHidden(account.chainId);
  };

  const status = getStatusText({
    isAccessLoading: miningAccess.loadingAccess,
    isAccess: miningAccess.access,
    isClaimGas: freeGas.claimLoading,
    isInitialize: freeGas.loading || miningAccess.loadingData,
    isRequireAccess: isRequireAccess,
    isFreeGas: freeGas.enabled,
    isRpcConnected: !!block.data,
    isRpcConnect: block.isPending,
  });

  return (
    <>
      {props.children}
      {enabled && !hidden && (
        <>
          <ChainSwitcher style={{ zIndex: 20000, position: 'fixed' }} />
          <EntryScreen
            onStart={onStartHandle}
            loading={isLoading}
            disabled={isDisabled}
            status={status}

            rpc={{
              success: !!block.data,
              loading: block.isPending,
            }}
            sign={{
              success: isSign,
              loading: loadingSign,
            }}
            access={
              isRequireAccess
                ? {
                    loading: miningAccess.loadingAccess,
                    success: miningAccess.access,
                  }
                : undefined
            }
            freeGas={
              freeGas.enabled
                ? {
                    loading: freeGas.claimLoading || freeGas.loading,
                    success: !freeGas.available,
                  }
                : undefined
            }
            hello={
            hello.enabled ?
                {
                  success: hello.received,
                  loading: hello.loading,
                }
            : undefined
            }
          />
        </>
      )}
    </>
  );
}
