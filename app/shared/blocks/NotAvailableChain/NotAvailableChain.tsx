import React from 'react';
import styles from './NotAvailableChain.module.scss';
import { NETWORKS_DATA } from 'services/multichain/chains';
import { useNavigate } from 'react-router-dom';
import { useSwitchChain } from '@chain/hooks/useSwitchChain.ts';
import pixelImage from 'assets/img/common/in-work.png';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useTranslation } from 'react-i18next';
import ChainSwitcher from '../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import Container from '@mui/material/Container';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { showConnectWallet } from '../../../widget/Connect/ConnectWallet.store.ts';
import { styled } from '@mui/material/styles';

const StyledContainer = styled(Container)(() => ({
  padding: '32px 0',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
}));

export type INotAvailableChainProps = {
  title: string;
  summary?: string;
  icon?: React.ReactNode;
  chainId?: number;
};

const NotAvailableChain: React.FC<INotAvailableChainProps> = (props) => {
  const { title, summary, icon, chainId } = props;

  const { t } = useTranslation('common');
  const account = useAccount();
  const switchChain = useSwitchChain();

  const onSwitch = async () => {
    await switchChain.switchChain({ chainId: chainId });

    Telegram.WebApp.HapticFeedback.notificationOccurred('success');
  };

  return (
    <div>
      <StyledContainer maxWidth='xs'>
        {icon && <div className={styles.icon}>{icon}</div>}
        {!icon && (
          <div style={{ width: 170, height: 170 }}>
            <img src={pixelImage as string} alt='' style={{ margin: -32 }} />
          </div>
        )}
        <h2>{title}</h2>

        {summary && <p style={{ textAlign: 'center' }}>{summary}</p>}
        {!!chainId && account.isConnected && (
          <Button
            variant='contained'
            color='primary'
            size='large'
            fullWidth
            onClick={onSwitch}
            sx={{ mt: 1 }}
          >
            {t('Switch to')} {NETWORKS_DATA[chainId]?.title || t('Available')}
          </Button>
        )}
        {!account.isConnected && (
          <Button
            variant='contained'
            color='success'
            size='large'
            fullWidth
            onClick={() => showConnectWallet()}
            sx={{ mt: 1 }}
          >
            Connect Wallet
          </Button>
        )}
      </StyledContainer>
    </div>
  );
};

export default NotAvailableChain;
