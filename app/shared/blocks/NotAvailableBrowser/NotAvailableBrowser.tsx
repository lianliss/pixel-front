import React from 'react';
import styles from './NotAvailableBrowser.module.scss';
import pixelImage from 'assets/img/common/in-work.png';
import { useTranslation } from 'react-i18next';

export type INotAvailableBrowserProps = {};

const NotAvailableBrowser: React.FC<INotAvailableBrowserProps> = (props) => {
  const { t } = useTranslation('common');

  return (
    <div className={styles.root}>
      <div style={{ width: 170, height: 170 }}>
        <img src={pixelImage as string} alt='' style={{ margin: -32 }} />
      </div>
      <h2>Not available</h2>

      <p style={{ textAlign: 'center' }}>Available only from telegram app</p>
    </div>
  );
};

export default NotAvailableBrowser;
