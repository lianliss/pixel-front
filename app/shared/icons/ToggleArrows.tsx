import React from 'react';
import SvgIcon, { SvgIconProps } from '@mui/material/SvgIcon';

function ToggleArrows(props: SvgIconProps) {
  return (
    <SvgIcon viewBox='0 0 24 24' fill='none' {...props}>
      <svg
        width='16'
        height='16'
        viewBox='0 0 16 16'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path
          d='M3.66663 1.66699V11.667'
          stroke='currentColor'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M6.33333 4.33366L3.66667 1.66699L1 4.33366'
          stroke='currentColor'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M12.3334 14.333V4.33301'
          stroke='currentColor'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M15.0001 11.6663L12.3334 14.333L9.66675 11.6663'
          stroke='currentColor'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
      </svg>
    </SvgIcon>
  );
}

export default ToggleArrows;
