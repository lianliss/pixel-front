import React from 'react';
import SvgIcon, { SvgIconProps } from '@mui/material/SvgIcon';

function StorageIcon(props: SvgIconProps) {
  return (
    <SvgIcon viewBox='0 0 24 24' fill='none' {...props}>
      <svg
        width='16'
        height='16'
        viewBox='0 0 16 16'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path
          d='M0 3.78078C0 3.32191 0.312297 2.92193 0.757464 2.81063L7.75746 1.06063C7.9167 1.02082 8.0833 1.02082 8.24254 1.06063L15.2425 2.81063C15.6877 2.92193 16 3.32191 16 3.78078V5C16 5.55228 15.5523 6 15 6H1C0.447715 6 0 5.55228 0 5V3.78078Z'
          fill='currentColor'
        />
        <path
          d='M1.5 14.5C1.5 14.7761 1.72386 15 2 15C2.27614 15 2.5 14.7761 2.5 14.5H1.5ZM1.5 6V14.5H2.5V6H1.5Z'
          fill='currentColor'
        />
        <path
          d='M13.5 14.5C13.5 14.7761 13.7239 15 14 15C14.2761 15 14.5 14.7761 14.5 14.5H13.5ZM13.5 6V14.5H14.5V6H13.5Z'
          fill='currentColor'
        />
        <rect x='4' y='11' width='4' height='4' rx='1' fill='currentColor' />
        <rect x='8' y='11' width='4' height='4' rx='1' fill='currentColor' />
        <rect x='6' y='7' width='4' height='4' rx='1' fill='currentColor' />
      </svg>
    </SvgIcon>
  );
}

export default StorageIcon;
