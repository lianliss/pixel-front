import React from 'react';
import SvgIcon, { SvgIconProps } from '@mui/material/SvgIcon';

function QuestIcon(props: SvgIconProps) {
  return (
    <SvgIcon viewBox='0 0 24 24' fill='currentColor' {...props}>
      <svg
        width='34'
        height='39'
        viewBox='0 0 34 39'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path
          fillRule='evenodd'
          clipRule='evenodd'
          d='M16.75 1.5C3.77941 1.5 1 4.67647 1 19.5C1 34.3235 3.77941 37.5 16.75 37.5C29.7206 37.5 32.5 34.3235 32.5 19.5C32.5 4.67647 29.7206 1.5 16.75 1.5Z'
          stroke='currentColor'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          fillRule='evenodd'
          clipRule='evenodd'
          d='M16.75 1.5C11.1912 1.5 10 2.09559 10 4.875C10 7.65441 11.1912 8.25 16.75 8.25C22.3088 8.25 23.5 7.65441 23.5 4.875C23.5 2.09559 22.3088 1.5 16.75 1.5Z'
          stroke='currentColor'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M7.75 15.8036L10.45 18.375L14.5 13.875'
          stroke='currentColor'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M7.75 24.8036L10.45 27.375L14.5 22.875'
          stroke='currentColor'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          opacity='0.66'
          d='M20.125 16.125H25.75'
          stroke='currentColor'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          opacity='0.66'
          d='M20.125 22.875H25.75'
          stroke='currentColor'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          opacity='0.66'
          d='M20.125 29.625H25.75'
          stroke='currentColor'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
      </svg>
    </SvgIcon>
  );
}

export default QuestIcon;
