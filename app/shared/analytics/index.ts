import * as Sentry from '@sentry/react';
import type Countly from 'countly-sdk-web';
import { BUILD_ID, IS_DEVELOP } from '@cfg/config.ts';
import './countly.min';

const SENTRY_DSN = import.meta.env.VITE_SENTRY_DSN;
const COUNTLY_APP_KEY = import.meta.env.VITE_COUNTLY_APP_KEY;
const COUNTLY_URL = import.meta.env.VITE_COUNTLY_URL;

class Analytics {
  protected countly: Countly;

  public init() {
    if (!IS_DEVELOP) {
      try {
        if (SENTRY_DSN && false) {
          this.initSentry(SENTRY_DSN);
        }
        if (COUNTLY_URL && COUNTLY_APP_KEY) {
          this.initCountly(COUNTLY_URL, COUNTLY_APP_KEY);
        }
      } catch (e) {
        console.error('failed to init analytics', e);
      }
    }
  }

  protected initSentry(dsn: string) {
    Sentry.init({
      dsn: dsn,
      integrations: [Sentry.browserTracingIntegration()], // , Sentry.replayIntegration()
      ignoreErrors: [
        'Network Error',
        'Failed to fetch dynamically imported module',
        'BOT_INVALID',
        'Request failed with status code',
        'timeout exceeded',
      ],
      allowUrls: [/^https:\/\/hellopixel\.network/, /^https:\/\/testing\.hellopixel\.network/],
      release: BUILD_ID,
      maxBreadcrumbs: 5,
      // Tracing
      ignoreTransactions: [],
      tracesSampleRate: 1.0, //  Capture 100% of the transactions
      // Set 'tracePropagationTargets' to control for which URLs distributed tracing should be enabled
      tracePropagationTargets: ['localhost', /^https:\/\/hellopixel\.network/],
      // Session Replay
      replaysSessionSampleRate: 0.1, // This sets the sample rate at 10%. You may want to change it to 100% while in development and then sample at a lower rate in production.
      replaysOnErrorSampleRate: 1.0, // If you're not already sampling the entire session, change the sample rate to 100% when sampling sessions where errors occur.

      beforeSend: (event, hint) => {
        // see all errors, what you wants.
        // using console.log(hint.originalException)

        // for example, not send when error code 404 when using axios
        const { response } = hint.originalException || {};

        if (response && response.status && [404, 401].includes(response.status)) {
          return null;
        }
        return event;
      },
    });
  }

  protected initCountly(url: string, key: string) {
    const countly = (window.Countly as Countly) || {};
    countly.q = countly.q || [];

    this.countly = countly;

    if (countly) {
      countly.url = url;
      countly.app_key = key;

      countly.q.push(['track_sessions']);
      countly.q.push(['track_errors']);

      countly.init();
    }
  }

  public track(
    event: string,
    context?: { count?: number; sum?: number; dur?: number },
    tags?: Record<string, string | number>
  ) {
    if (this.countly) {
      this.countly.q.push([
        'add_event',
        {
          key: event,
          ...(context || {}),
          segmentation: tags,
        },
      ]);
    }
  }

  public trackError(event: string, exception: Error) {
    if (this.countly) {
      this.countly.q.push(['log_error', exception]);
    }
  }

  public startEvent(
    event: string,
    context?: { count?: number; sum?: number; dur?: number },
    tags?: Record<string, string | number>
  ) {
    if (this.countly) {
      this.countly.q.push(['start_event', event]);

      return () => {
        this.countly.q.push([
          'end_event',
          {
            key: event,
            ...context,
            segmentation: tags,
          },
        ]);
      };
    }

    return () => {};
  }

  public trackPage(name: string | null = null) {
    if (this.countly) {
      this.countly.q.push(['track_pageview', name]);
    }
  }

  public setWallet(payload: { accountAddress: string; chainId: number }) {
    Sentry.setContext('wallet', {
      accountAddress: payload.accountAddress,
      chainId: payload.chainId,
    });
    Sentry.setTag('chain', payload.chainId.toString());
  }

  public setTelegram(payload: { telegramId: number; username: string }) {
    Sentry.setContext('telegram', { telegramId: payload.telegramId, username: payload.username });
  }
}

const analytics = new Analytics();

export default analytics;
