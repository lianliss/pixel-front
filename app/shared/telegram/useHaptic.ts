import { IS_TELEGRAM } from '@cfg/config.ts';

export function useHaptic() {
  return {
    click: () => {
      if (!IS_TELEGRAM) {
        return;
      }
      Telegram.WebApp.HapticFeedback.impactOccurred('light');
    },
    clickHeavy: () => {
      if (!IS_TELEGRAM) {
        return;
      }
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
    },
    success: () => {
      if (!IS_TELEGRAM) {
        return;
      }
      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    },
    error: () => {
      if (!IS_TELEGRAM) {
        return;
      }
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    },
  };
}
