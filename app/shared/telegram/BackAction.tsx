import { IconButton } from '@ui-kit/IconButton/IconButton.tsx';
import { useBackAction } from './useBackAction.ts';
import ArrowBack from '@mui/icons-material/ArrowBack';

function BackAction() {
  const back = useBackAction('/wallet');

  if (!back.available) {
    return null;
  }

  return (
    <IconButton
      onClick={() => back.click()}
      sx={{ position: 'fixed', top: 60 + 24, left: 24, zIndex: 10000 }}
    >
      <ArrowBack />
    </IconButton>
  );
}

export default BackAction;
