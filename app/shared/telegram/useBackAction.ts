import { useLocation, useNavigate } from 'react-router-dom';
import { useCallback, useEffect } from 'react';
import { IS_TELEGRAM } from '@cfg/config.ts';

// могут быть баги
const history: string[] = [];

export function useBackAction(path: string | null) {
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    if (history.at(-1) !== location.pathname) {
      history.push(location.pathname);
    }
  }, [location.pathname, location]);

  const nextPath = history.at(-2) || path;

  const back = useCallback(() => {
    Telegram.WebApp.BackButton.hide();

    if (history.length > 1) {
      history.pop();
      navigate(history.at(-1));
    } else {
      navigate(nextPath);
    }
  }, [nextPath]);
  const available = location.pathname !== nextPath;

  useEffect(() => {
    if (IS_TELEGRAM && path) {
      Telegram.WebApp.BackButton.onClick(back);
      Telegram.WebApp.BackButton.show();

      return () => {
        Telegram.WebApp.BackButton.offClick(back);
        Telegram.WebApp.BackButton.hide();
      };
    }
  }, [back, path]);

  return {
    available: available,
    click: back,
    clear: () => {
      while (history.length) {
        history.pop();
      }
    },
  };
}
