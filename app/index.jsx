'use strict';
/**
 * @module app/app
 */
import 'core-js';
import 'array-flat-polyfill';
const getRoot = () => import('./root/index.jsx');
import * as es6Promise from 'es6-promise';
import '../styles/link.scss';
import './i18n';
import analytics from "shared/analytics";

es6Promise.polyfill();
analytics.init();

const start = async () => {
  const { default: Root } = await getRoot();

  Root.start();
};

if (
  'Map' in window &&
  'URL' in window &&
  'keys' in Object &&
  'fetch' in window &&
  'assign' in Object &&
  'forEach' in NodeList.prototype &&
  'entries' in Object &&
  'endsWith' in String.prototype &&
  'includes' in String.prototype &&
  'includes' in Array.prototype &&
  'onFinally' in Promise.prototype &&
  'startsWith' in String.prototype
) {
  start();
} else {
  import('utils/polyfills.jsx').then(start);
}

if (![].at) {
  function at(n) {
    // ToInteger() abstract op
    n = Math.trunc(n) || 0;
    // Allow negative indexing from the end
    if (n < 0) n += this.length;
    // OOB access is guaranteed to return undefined
    if (n < 0 || n >= this.length) return undefined;
    // Otherwise, this is just normal property access
    return this[n];
  }

  const TypedArray = Reflect.getPrototypeOf(Int8Array);
  for (const C of [Array, String, TypedArray]) {
    Object.defineProperty(C.prototype, 'at', {
      value: at,
      writable: true,
      enumerable: false,
      configurable: true,
    });
  }
}
