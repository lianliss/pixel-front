import i18n from 'i18next';
// import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';
import resourcesToBackend from 'i18next-resources-to-backend';
import { langKey } from './shared/const/localStorage.ts';
// import {IS_TELEGRAM} from "@cfg/config.ts";
import {IS_LANG} from "@cfg/app.ts";

i18n
  // load translation using http -> see /public/locales
  // learn more: https://github.com/i18next/i18next-http-backend
  // .use(Backend)
  .use(
    resourcesToBackend((language, namespace) => import(`../locales/${language}/${namespace}.json`))
  )
  // detect user language
  // learn more: https://github.com/i18next/i18next-browser-languageDetector
  // .use(LanguageDetector)
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    fallbackLng: 'en',
    debug: false,
    lng: IS_LANG ? localStorage.getItem(langKey) : 'en',
    load: 'currentOnly',
    defaultNS: 'wallet',
  });

export default i18n;
