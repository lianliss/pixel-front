'use strict';
import React from 'react';
import styles from './Airdrop.module.scss';
import Button from 'app/lib/ui/deprecated/ButtonDeprecated';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTranslation } from 'react-i18next';
import { showConnectWallet } from 'app/widget/Connect/ConnectWallet.store';

function Airdrop() {
  const { t } = useTranslation('airdrop');

  const account = useAccount();
  const { isConnected } = account;

  return (
    <div className={styles.airdrop}>
      <h1>{t('Pixel Airdrop')}</h1>
      <h2>{t('Introducing HELLO PIXEL')}</h2>
      <p>{t('HELLO PIXEL')}</p>
      <h2>{t('Tokenomics & Utility')}</h2>
      <p>
        {t(
          "The ecosystem's native cryptocurrency is the PIXEL token, which serves multiple purposes:"
        )}
      </p>
      <ul>
        <li>
          <b>{t('Dividend Receival:')}</b> {t('Holders')}
        </li>
        <li>
          <b>{t('NFT Crafting:')}</b> {t('Users can craft NFTs')}
        </li>
      </ul>
      <div className={styles.airdropImages}>
        <div className={styles.airdropImagesItem}>
          <img src={require('assets/img/airdrop1.jpg')} alt={''} />
        </div>
        <div className={styles.airdropImagesItem}>
          <img src={require('assets/img/airdrop2.jpg')} alt={''} />
        </div>
      </div>
      <h2>{t('Phased Events')}</h2>
      <p>{t('The project will')}</p>
      <h2>{t('NFT Staking with Cyber Hamster')}</h2>
      <p>{t('The Cyber Hamster is an example')}</p>
      <h2>{t('4 Ways to Generate PIXEL Tokens')}</h2>
      <ul dangerouslySetInnerHTML={{ __html: t('NFT Staking with Cyber Hamster content') }}></ul>
      <h2>{t('Cross-Chain Presence')}</h2>
      <p>{t('The HELLO PIXEL project will')}</p>
      <center>
        {isConnected ? (
          <>
            <Button large icon={'box'} className='' disabled>
              {t('Participate in Airdrop')}
            </Button>
            <small>{/*Will be active in 7 days*/}</small>
          </>
        ) : (
          <Button large primary icon={'antenna'} className='' onClick={() => showConnectWallet()}>
            {t('Connect wallet')}
          </Button>
        )}
      </center>
    </div>
  );
}

export default Airdrop;
