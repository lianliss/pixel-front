import React, { useState } from 'react';

import styles from '../MinerClaimerDialog.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import BalancesMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/BalancesMinerDrone/BalancesMinerDrone.tsx';
import BreadwinnerImageMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/BreadwinnerImageMinerDrone/BreadwinnerImageMinerDrone.tsx';
import AutoClaimMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/AutoclaimMinerDrone/AutoclaimMinerDrone.tsx';
import { IconDuration } from '@ui-kit/icon/common/IconDuration.tsx';
import { IconDurability } from '@ui-kit/icon/common/IconDurability.tsx';
import { IUserMinerDroneDto } from 'lib/MinerClaimer/api-server/miner-claimer.types.ts';
import ProgressTimingMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/ProgressTimingMinerDrone/ProgressTimingMinerDrone.tsx';
import { IconTiming } from '@ui-kit/icon/common/IconTiming.tsx';
import { diffTime, getDateDays, toUTC } from 'utils/format/date';
import { wei } from 'utils/index';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { MINER_CLAIMER_ABI } from 'lib/MinerClaimer/api-contract/miner-claimer.abi.ts';
import { MINER_CLAIMER_CONTRACT_ID } from 'lib/MinerClaimer/api-contract/miner-claimer.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';
import ProgressLine from '@ui-kit/ProgressLine/ProgressLine.tsx';
import { useTranslation } from 'react-i18next';
import Paper from '@mui/material/Paper';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import Box from '@mui/material/Box';
import pixelImage from 'assets/img/token/common/pixel.png';
import { getMinerDroneTimedProgress } from 'lib/MinerClaimer/utils/progress.ts';
import { MinerDroneExtraInfo } from 'lib/MinerClaimer/components/MinerClaimerDialog/components/MinerDroneExtraInfo.tsx';
import Alert from '@mui/material/Alert';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { IntervalRender } from '@ui-kit/IntervalRender/IntervalRender.tsx';
import Divider from '@mui/material/Divider';

type Props = {
  handleTake: () => void;
  minerDrone?: IUserMinerDroneDto;
  handleStart: (intervalSeconds: number) => void;
  handleStop: () => void;
  loadingAction?: boolean;

  telegramId?: number;
  noGas?: boolean;
};

export function MinerDroneEquipped(props: Props) {
  const {
    handleTake,
    minerDrone,
    handleStart,
    handleStop,
    loadingAction = false,
    telegramId,
    noGas,
  } = props;

  const account = useAccount();
  const address = MINER_CLAIMER_CONTRACT_ID[account.chainId];

  const { t } = useTranslation('inventory');

  const [timing, setTiming] = useState(60);
  const timingValue = minerDrone.timing ? +minerDrone.timing / 60 / 1000 : timing;

  const isStarted = !!minerDrone.startTime;
  const freezeAmount = minerDrone?.depositSize ? +wei.from(minerDrone.depositSize).toFixed(2) : 0;
  const isTimed = minerDrone.isTimeSession;
  const startPrice = minerDrone?.nft?.type?.minerDroneType?.startPrice;
  const txPriceMod = minerDrone?.nft?.type?.minerDroneType?.txPriceMod;
  //
  const currentClaims = minerDrone.sessionClaims;
  const sessionClaims = minerDrone?.nft?.type?.minerDroneType?.sessionClaims || 0;
  //
  const timedProgress = getMinerDroneTimedProgress(minerDrone);
  //
  const totalDuration = Number(minerDrone?.nft?.type?.minerDroneType?.duration || 0);
  // const currentDuration = minerDrone?.duration ? +minerDrone.duration || 0 : 0;
  const currentDuration = minerDrone?.duration
    ? minerDrone.startTime
      ? minerDrone.duration - (Date.now() - minerDrone.startTime)
      : minerDrone.duration
    : 0;
  const currentDurationDays = minerDrone?.duration ? getDateDays(currentDuration) : undefined;
  const isBroken = isStarted ? currentDuration <= 1000 * 60 : currentDuration <= 1000 * 60 * timing;

  const depositForLock = useReadContract({
    abi: MINER_CLAIMER_ABI,
    address: address,
    functionName: 'getTimingDepositSize',
    args: [telegramId, timing * 60],
    skip: !telegramId || isStarted,
    select: (r) => {
      return +(+wei.from(r)).toFixed(4);
    },
  });

  const tradeToken = useTradeToken();
  const balance = useBalance({
    address: account.accountAddress,
    token: tradeToken.data?.address,
    skip: !account.isConnected || !tradeToken.data?.address,
  });

  const isNoBalance = balance.data?.formatted < depositForLock?.data;

  return (
    <div className={styles.miner__drone}>
      <div className={styles.heading}>
        <Typography fontWeight='bold'>Smart Claimer</Typography>
        <Typography variant='caption' className={styles.description}>
          {t('You can use NFT Smart Claimer for auto claim PXLs')}
        </Typography>
      </div>

      <Paper className={styles.content}>
        <BalancesMinerDrone
          freezeAmount={isStarted ? freezeAmount : depositForLock.data}
          isStarted={isStarted}
          balanceAmount={balance.data?.formatted}
          tokenSymbol={tradeToken.data?.symbol}
        />
        {/*<AlertBalancesMinerDrone text='First, insert the NFT into the slot' />*/}

        <Divider />

        <Paper
          elevation={1}
          className={styles.breadwinner__container}
          sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }}
        >
          <div className={styles.breadwinner}>
            <Typography variant='body1' align='center' fontWeight='bold'>
              {minerDrone ? minerDrone.nft?.type?.name : t('Empty NFT Slot')}
            </Typography>

            <div className={styles.breadwinner__inner}>
              <div className={styles.breadwinner__left}>
                <BreadwinnerImageMinerDrone image={minerDrone?.nft?.type?.uri} />

                {minerDrone && !isStarted && (
                  <div className={styles.take}>
                    <Button
                      fullWidth
                      variant='outlined'
                      onClick={handleTake}
                      disabled={isStarted || loadingAction}
                    >
                      Take Off
                    </Button>
                  </div>
                )}
              </div>

              <div className={styles.breadwinner__right}>
                <ProgressTimingMinerDrone
                  icon={<IconTiming />}
                  name={t('Timing')}
                  value={timingValue}
                  disabled={isStarted}
                  onChange={(nextTiming) => setTiming(nextTiming)}
                />

                {!isTimed && (
                  <ProgressLine
                    size='medium'
                    icon={<IconDuration />}
                    name={t('Duration')}
                    description={`${currentClaims}/${sessionClaims} Claim`}
                    value={currentClaims}
                    total={sessionClaims}
                  />
                )}
                {isTimed && (
                  <ProgressLine
                    size='medium'
                    icon={<IconDuration />}
                    name={t('Duration')}
                    description={`${timedProgress.currentHours}/${timedProgress.totalHours} h`}
                    value={timedProgress.current}
                    total={timedProgress.total}
                  />
                )}
                <ProgressLine
                  size='medium'
                  icon={<IconDurability />}
                  name={t('Durability')}
                  description={`${currentDurationDays || '0'}/Days`}
                  value={currentDuration}
                  total={totalDuration}
                />
              </div>
            </div>
          </div>
        </Paper>

        <Divider />

        <div
          style={{
            display: 'flex',
            justifyContent: isStarted ? 'flex-end' : 'space-between',
            alignItems: 'center',
            padding: 12,
          }}
        >
          {!isStarted && (
            <Box sx={{ display: 'flex', flexDirection: 'column', gap: '2px' }}>
              <Typography color='text.secondary' sx={{ fontSize: 11 }}>
                Start Autoclaim for:
              </Typography>
              <Box sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
                <Avatar src={`${pixelImage}`} sx={{ width: 24, height: 24 }} />
                <Typography noWrap sx={{ fontSize: 14 }}>
                  {startPrice || 0}{' '}
                  <Typography component='span' variant='inherit' sx={{ opacity: 0.5 }}>
                    PXLs
                  </Typography>
                </Typography>
              </Box>
            </Box>
          )}

          {(isStarted ? true : !isBroken) && (
            <AutoClaimMinerDrone
              sx={{ width: '100%', maxWidth: isStarted ? '100%' : 178 }}
              isActive={isStarted}
              handleStart={() => handleStart(timing * 60)}
              handleStop={handleStop}
              loading={loadingAction}
              disabled={isNoBalance || noGas}
            >
              {isNoBalance ? (
                t('No deposit')
              ) : isStarted ? (
                <IntervalRender
                  render={() => {
                    return (
                      t('Autoclaim on') +
                      ': ' +
                      (minerDrone.nextClaimAt ? diffTime(toUTC(minerDrone.nextClaimAt), true) : '')
                    );
                  }}
                />
              ) : (
                t('Autoclaim off')
              )}
            </AutoClaimMinerDrone>
          )}
        </div>

        {isBroken && (
          <Alert severity='warning' variant='outlined' sx={{ mx: 2, mb: 2 }}>
            {t('The smart claimer durability is over')}
          </Alert>
        )}

        {!isBroken && (
          <MinerDroneExtraInfo
            sx={{ p: '8px 16px', pt: 0 }}
            commission={txPriceMod}
            completedClaims={minerDrone?.totalClaims}
          />
        )}
      </Paper>
    </div>
  );
}
