import React from 'react';

import styles from './styles.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useTranslation } from 'react-i18next';

type Props = {
  text: string;
};

const AlertBalancesMinerDrone = ({ text }: Props) => {
  const { t } = useTranslation('inventory');

  return (
    <div>
      {/*<Typography className={styles.status__balance} variant='caption' light textAlign='center'>*/}
      {/*  You have enough SGB balance to use Claimer*/}
      {/*</Typography>*/}
      <Typography
        className={styles.status__balance}
        variant='caption'
        light
        // style={{ color: '#E62058' }}
      >
        {text}
      </Typography>
    </div>
  );
};

export default AlertBalancesMinerDrone;
