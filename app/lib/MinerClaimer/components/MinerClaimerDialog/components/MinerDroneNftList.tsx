import React from 'react';

import styles from '../MinerClaimerDialog.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import BalancesMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/BalancesMinerDrone/BalancesMinerDrone.tsx';
import AlertBalancesMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/AlertBalancesMinerDrone/AlertBalancesMinerDrone.tsx';
import BreadwinnerImageMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/BreadwinnerImageMinerDrone/BreadwinnerImageMinerDrone.tsx';
import AutoClaimMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/AutoclaimMinerDrone/AutoclaimMinerDrone.tsx';
import { IconTiming } from '@ui-kit/icon/common/IconTiming.tsx';
import { IconDuration } from '@ui-kit/icon/common/IconDuration.tsx';
import { IconDurability } from '@ui-kit/icon/common/IconDurability.tsx';
import { IFindUserMinerDroneResponse } from 'lib/MinerClaimer/api-server/miner-claimer.types.ts';
import InventoryItem from 'lib/Inventory/components/InventoryItem/InventoryItem.tsx';
import { useTranslation } from 'react-i18next';

type Props = {
  selectedMinerNft?: string;
  minerDrones: IFindUserMinerDroneResponse;
  selectMinerNft: (assetId: string) => void;
};

export function MinerDroneNftList(props: Props) {
  const { minerDrones, selectMinerNft, selectedMinerNft } = props;

  const { t } = useTranslation('inventory');

  return (
    <div className={styles.miner__drone}>
      <div className={styles.heading}>
        <Typography fontWeight='bold'>{t('Smart Claimer NFT')}</Typography>
        <Typography variant='caption' className={styles.description}>
          {t('Use NFT for autoclaiming PXLs')}
        </Typography>
      </div>

      <div className={styles.nft__slots}>
        {minerDrones?.rows.map((minerDrone) => {
          const isSelected = selectedMinerNft === minerDrone.assetId;

          return (
            <InventoryItem
              selected={isSelected}
              key={`item-${minerDrone.id}`}
              item={minerDrone}
              onPreview={() => selectMinerNft(minerDrone.assetId)}
            />
          );
        })}
      </div>
    </div>
  );
}
