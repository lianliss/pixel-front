import React from 'react';

import styles from './styles.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useTranslation } from 'react-i18next';
import { Button } from '@ui-kit/Button/Button.tsx';
import Box, { BoxProps } from '@mui/material/Box';
import clsx from 'clsx';

type Props = {
  children: React.ReactNode;
  isActive: boolean;
  handleStart?: () => void;
  handleStop?: () => void;
  loading?: boolean;
  disabled?: boolean;
} & BoxProps;

const AutoClaimMinerDrone = (props: Props) => {
  const {
    children,
    isActive = false,
    handleStart,
    handleStop,
    loading = false,
    disabled = false,
    className,
    ...other
  } = props;

  const { t } = useTranslation('inventory');

  // TODO fix
  return (
    <Box className={clsx(className, styles.autoclaim)} {...other}>
      <Button
        className={styles.autoclaim__control}
        disabled={loading || disabled || !(handleStart || handleStop)}
        onClick={isActive ? handleStop : handleStart}
      >
        <div className={styles.control__checkbox} aria-selected={isActive}>
          <Typography
            sx={{ color: isActive && !loading && !disabled ? 'success.main' : 'gray' }}
            variant='caption'
          >
            {loading ? t('Loading...') : children}
          </Typography>
        </div>
      </Button>
    </Box>
  );
};

export default AutoClaimMinerDrone;
