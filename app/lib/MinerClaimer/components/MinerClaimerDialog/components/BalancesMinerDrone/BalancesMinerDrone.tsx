import React from 'react';

import styles from './styles.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import sgbIcon from 'assets/img/telegram-stars/sgb/preview.png';
import { useTranslation } from 'react-i18next';

type Props = {
  freezeAmount?: number;
  isStarted?: boolean;
  balanceAmount?: number;
  tokenSymbol?: string;
};

const BalancesMinerDrone: React.FC<Props> = (props) => {
  const { freezeAmount, isStarted = false, balanceAmount = 0, tokenSymbol } = props;
  const { t } = useTranslation('inventory');

  return (
    <div className={styles.balances}>
      <div className={styles.balance__section}>
        <Typography className={styles.title} variant='caption'>
          {t('Your Balance:')}
        </Typography>
        <div className={styles.value}>
          <Avatar src={`${sgbIcon}`} className={styles.icon} />
          <Typography className={styles.text} noWrap>
            {balanceAmount} {tokenSymbol}
          </Typography>

          {/*<a>Buy more sgb</a>*/}
        </div>
      </div>
      {typeof freezeAmount === 'number' && (
        <div className={styles.balance__section}>
          <Typography className={styles.title} variant='caption' align='right'>
            {isStarted ? 'Locked amount' : 'Amount for freeze'}:
          </Typography>
          <div className={styles.value}>
            <Avatar src={`${sgbIcon}`} className={styles.icon} />
            <Typography className={styles.text} noWrap>
              {freezeAmount} {tokenSymbol}
            </Typography>
          </div>
        </div>
      )}
    </div>
  );
};

export default BalancesMinerDrone;
