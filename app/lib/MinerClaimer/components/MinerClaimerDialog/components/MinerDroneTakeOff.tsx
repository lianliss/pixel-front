import React from 'react';
import { IUserMinerDroneDto } from 'lib/MinerClaimer/api-server/miner-claimer.types.ts';
import styles from 'lib/MinerClaimer/components/MinerClaimerDialog/MinerClaimerDialog.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import BreadwinnerImageMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/BreadwinnerImageMinerDrone/BreadwinnerImageMinerDrone.tsx';
import { useTranslation } from 'react-i18next';

type Props = {
  minerDrone?: IUserMinerDroneDto;
};
const MinerDroneTakeOff = ({ minerDrone }: Props) => {
  const { t } = useTranslation('inventory');

  return (
    <div className={styles.miner__drone}>
      <div className={styles.heading}>
        <Typography fontWeight='bold'>Take Off NFT</Typography>
        <Typography variant='caption' className={styles.description}>
          {t('Do you really want to remove NFTs from the')}
          <br /> {t('smart claimer?')}
        </Typography>
      </div>

      <div className={styles.leveling}>
        <BreadwinnerImageMinerDrone image={minerDrone?.nft?.type?.uri} />
      </div>
      <div className={styles.leveling}>
        <Typography sx={{ color: '#E62058' }} variant='caption' align='center'>
          {t('WARNING! If you TAKE OFF Harvester, YOU WILL LOSE YOUR NFT FOREVER!')}
        </Typography>
      </div>
    </div>
  );
};

export default MinerDroneTakeOff;
