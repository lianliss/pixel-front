import React from 'react';

import styles from './styles.module.scss';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import PlusIcon from 'lib/Marketplace/components/icons/PlusIcon';
import clsx from 'clsx';
import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import AddIcon from '@mui/icons-material/Add';
import BadgeEdit from '@ui-kit/Badge/BadgeEdit.tsx';
import SlotDroneCraft from 'lib/NFT/components/icons/SlotDroneCraft';

type Props = {
  image?: string;
  onClick?: () => void;
  rarity?: NftItemRarity;
  showEdit?: boolean;
};

const BreadwinnerImageMinerDrone = ({ image, onClick, rarity, showEdit }: Props) => {
  return (
    <BadgeEdit show={showEdit}>
      <Avatar
        variant='rounded'
        className={clsx(styles.avatar, { [styles.empty]: !image })}
        onClick={onClick}
        style={{ borderColor: rarity ? getNftRarityColor(rarity) : undefined }}
        sx={{
          width: 104,
          height: 104,
          fontSize: 12,
          border: 'solid 1px transparent',
          backgroundColor: '#011324',
          padding: '1px',
        }}
      >
        {image ? (
          <img
            className={styles.image}
            src={image}
            alt=''
            onError={(e) => {
              (e.target as HTMLImageElement).src =
                'https://storage.hellopixel.network/assets/achievement/pixel.png';
            }}
          />
        ) : (
          <SlotDroneCraft sx={{ color: 'white', fontSize: 40 }} />
        )}
      </Avatar>
    </BadgeEdit>
  );
};

export default BreadwinnerImageMinerDrone;
