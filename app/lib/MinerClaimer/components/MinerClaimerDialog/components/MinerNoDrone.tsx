import React from 'react';

import styles from '../MinerClaimerDialog.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import BalancesMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/BalancesMinerDrone/BalancesMinerDrone.tsx';
import AlertBalancesMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/AlertBalancesMinerDrone/AlertBalancesMinerDrone.tsx';
import BreadwinnerImageMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/BreadwinnerImageMinerDrone/BreadwinnerImageMinerDrone.tsx';
import AutoClaimMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/AutoclaimMinerDrone/AutoclaimMinerDrone.tsx';
import { IconTiming } from '@ui-kit/icon/common/IconTiming.tsx';
import { IconDuration } from '@ui-kit/icon/common/IconDuration.tsx';
import { IconDurability } from '@ui-kit/icon/common/IconDurability.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import ProgressLine from '@ui-kit/ProgressLine/ProgressLine.tsx';
import { useBalance } from '@chain/hooks/useBalance.ts';
import { useTranslation } from 'react-i18next';
import Paper from '@mui/material/Paper';
import { MinerDroneExtraInfo } from 'lib/MinerClaimer/components/MinerClaimerDialog/components/MinerDroneExtraInfo.tsx';

type Props = {
  handleInsert: () => void;
  loadingAction?: boolean;
};

export function MinerNoDrone(props: Props) {
  const { handleInsert, loadingAction } = props;
  const { t } = useTranslation('inventory');

  const account = useAccount();
  const balance = useBalance({
    address: account.accountAddress,
    skip: !account.isConnected,
  });

  return (
    <Paper className={styles.miner__drone} sx={{ background: 'transparent' }}>
      <div className={styles.heading}>
        <Typography fontWeight='bold'>{t('Smart Claimer')}</Typography>
        <Typography variant='caption' className={styles.description}>
          {t('You can use NFT Smart Claimer for auto claim PXLs')}
        </Typography>
      </div>

      <Paper className={styles.content} sx={{ bgcolor: 'background.black2' }}>
        <BalancesMinerDrone balanceAmount={balance.data?.formatted || 0} />
        <AlertBalancesMinerDrone text={t('First, insert the NFT into the slot')} />

        <Paper
          elevation={1}
          className={styles.breadwinner__container}
          sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }}
        >
          <div className={styles.breadwinner}>
            <Typography variant='body1' align='center' fontWeight='bold'>
              {t('Empty NFT Slot')}
            </Typography>

            <div className={styles.breadwinner__inner}>
              <div className={styles.breadwinner__left}>
                <BreadwinnerImageMinerDrone />

                <div className={styles.take}>
                  <Button
                    fullWidth
                    variant='contained'
                    onClick={handleInsert}
                    loading={loadingAction}
                  >
                    {t('Insert')}
                  </Button>
                </div>
              </div>

              <div className={styles.breadwinner__right}>
                <ProgressLine icon={<IconTiming />} name='Timing' value={60} total={60} />
                <ProgressLine
                  icon={<IconDuration />}
                  name={t('Duration')}
                  description={`? Claim`}
                  value={100}
                  total={100}
                />
                <ProgressLine
                  icon={<IconDurability />}
                  name={t('Durability')}
                  description={`?/Days`}
                  value={100}
                  total={100}
                />
              </div>
            </div>
          </div>
        </Paper>

        <div style={{ display: 'flex', justifyContent: 'flex-end', padding: 16 }}>
          <AutoClaimMinerDrone isActive={false} sx={{ width: '100%', maxWidth: 178 }}>
            <Typography sx={{ color: 'gray' }} variant='caption'>
              {t('Not available')}
            </Typography>
          </AutoClaimMinerDrone>
        </div>

        <MinerDroneExtraInfo sx={{ p: '8px 16px', pt: 0 }} commission={1} />
      </Paper>
    </Paper>
  );
}
