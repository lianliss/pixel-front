import React, { ReactElement, useEffect, useMemo, useRef, useState } from 'react';

import styles from './styles.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Slider from '@mui/material/Slider';

type Props = {
  icon: ReactElement;
  name: string;
  value?: number;
  onChange?: (value: number) => void;
  disabled?: boolean;
};

const ProgressTimingMinerDrone = (props: Props) => {
  const { icon, name, value, onChange, disabled } = props;

  const [timing, setTiming] = useState(60);
  const timeoutRef = useRef<any | null>(null);

  useEffect(() => {
    if (value) {
      setTiming(value);
    }
  }, [value]);
  useEffect(() => {
    return () => {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
      }
    };
  }, []);

  const handleChange = (_event: Event, value: number, _activeThumb: number) => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }

    timeoutRef.current = setTimeout(() => {
      setTiming(value);
      onChange?.(value);
    }, 500);
  };

  return (
    <div className={styles.progress__line}>
      <div className={styles.head}>
        <div className={styles.name__line}>
          {icon}
          <Typography variant='caption' fontWeight='bold'>
            {name}
          </Typography>
        </div>
        <span>{`Every ${+(timing / 60).toFixed(2)} hour`}</span>
      </div>
      <Slider
        disabled={disabled}
        defaultValue={value || 60}
        valueLabelFormat={(value) => `${+(value / 60).toFixed(2)}`}
        valueLabelDisplay='auto'
        shiftStep={60}
        step={15}
        marks={[
          { value: 60 },
          { value: 120 },
          { value: 180 },
          { value: 240 },
          { value: 300 },
          { value: 360 },
        ]}
        min={15}
        max={6 * 60}
        onChange={handleChange}
        sx={{ padding: '8px 0', '& .MuiSlider-valueLabel': { minWidth: 60 } }}
      />
    </div>
  );
};

export default ProgressTimingMinerDrone;
