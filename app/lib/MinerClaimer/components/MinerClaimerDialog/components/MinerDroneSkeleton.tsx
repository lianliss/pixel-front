import React from 'react';

import styles from '../MinerClaimerDialog.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import BalancesMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/BalancesMinerDrone/BalancesMinerDrone.tsx';
import AlertBalancesMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/AlertBalancesMinerDrone/AlertBalancesMinerDrone.tsx';
import BreadwinnerImageMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/BreadwinnerImageMinerDrone/BreadwinnerImageMinerDrone.tsx';
import AutoClaimMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/AutoclaimMinerDrone/AutoclaimMinerDrone.tsx';
import { IconTiming } from '@ui-kit/icon/common/IconTiming.tsx';
import { IconDuration } from '@ui-kit/icon/common/IconDuration.tsx';
import { IconDurability } from '@ui-kit/icon/common/IconDurability.tsx';
import Skeleton from '@mui/material/Skeleton';
import { alpha, darken, styled } from '@mui/material/styles';

export function MinerDroneSkeleton() {
  return (
    <div className={styles.miner__drone}>
      <div className={styles.heading}>
        <Typography fontWeight='bold'>Smart Claimer</Typography>
        <Typography variant='caption' className={styles.description}>
          You can use NFT Smart Claimer for auto claim PXLs
        </Typography>
      </div>

      <div className={styles.content}>
        <div className={styles.head__skeleton} />

        <div className={styles.breadwinner__container}>
          <div className={styles.breadwinner}>
            <Typography variant='body1' align='center' fontWeight='bold'>
              <Skeleton className={styles.skeleton} variant='rectangular' />
            </Typography>

            <div className={styles.breadwinner__inner}>
              <div className={styles.breadwinner__left}>
                <BreadwinnerImageMinerDrone />

                <div className={styles.take}>
                  <Button fullWidth variant='contained'></Button>
                </div>
              </div>

              <div className={styles.breadwinner__right}>
                <Skeleton className={styles.skeleton__progress} variant='rectangular' />
                <Skeleton className={styles.skeleton__progress} variant='rectangular' />
                <Skeleton className={styles.skeleton__progress} variant='rectangular' />
              </div>
            </div>

            <div style={{ display: 'flex', justifyContent: 'flex-end', marginTop: 16 }}>
              <AutoClaimMinerDrone isActive={false} sx={{ width: '100%', maxWidth: 178 }}>
                <Skeleton className={styles.skeleton__AutoClaim} variant='rectangular' />
              </AutoClaimMinerDrone>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
