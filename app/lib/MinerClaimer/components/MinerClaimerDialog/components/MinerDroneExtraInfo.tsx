import Box, { BoxProps } from '@mui/material/Box';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

type Props = BoxProps & { commission: number; completedClaims?: number };

export function MinerDroneExtraInfo(props: Props) {
  const { commission, completedClaims, ...other } = props;

  return (
    <Box sx={{ width: '100%' }} {...other}>
      <Typography component='p' variant='caption' align='center' color='text.secondary'>
        Commission discount {commission ? -((1 - commission) * 100).toFixed(2) : 0}%
      </Typography>
      {typeof completedClaims === 'number' && (
        <Typography component='p' variant='caption' align='center' color='text.secondary'>
          Completed claims {completedClaims}
        </Typography>
      )}
    </Box>
  );
}
