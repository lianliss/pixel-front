import { useUnit } from 'effector-react';
import {
  loadUserMinerClaimer,
  loadUserMinerClaimers,
  userMinerClaimersStore,
  userMinerClaimerStore,
} from 'lib/MinerClaimer/api-server/miner-claimer.store.ts';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useMinerClaimerApi } from 'lib/MinerClaimer/hooks/useMinerClaimerApi.ts';
import toaster from 'services/toaster.tsx';
import wait from 'utils/wait';
import { MinerClaimerDialogUi } from 'lib/MinerClaimer/components/MinerClaimerDialog/MinerClaimerDialog.ui.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';

import { MINER_CLAIMER_CONTRACT_ID } from 'lib/MinerClaimer/api-contract/miner-claimer.constants.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import { useGasBalance } from '@chain/hooks/useGasBalance.ts';
import { useUserMinerClaimer } from 'lib/MinerClaimer/hooks/useUserMinerClaimer.ts';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

type Props = {
  open: boolean;
  onClose?: () => void;
  reloadNfts?: () => void;
};

function MinerDroneDialog(props: Props) {
  const { open, onClose, reloadNfts } = props;

  // const accountAddress = '0x4B9Ac70305761c8f1c4e88AD6C54499b15674a07';
  // const isConnected = true;

  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;
  const { accountAddress, isConnected, chainId } = useAccount();
  const minerDroneApi = useMinerClaimerApi();
  const gasBalance = useGasBalance({
    address: accountAddress,
  });

  const minerDroneAddress = MINER_CLAIMER_CONTRACT_ID[chainId];
  const nftAddress = NFT_CONTRACT_ID[chainId];
  const noGas = gasBalance.isFetched && !gasBalance.data?.formatted;

  const userMinerDrone = useUserMinerClaimer({
    minerDroneAddress: minerDroneAddress,
    accountAddress: accountAddress,
  });
  const minerDrones = useUnit(userMinerClaimersStore);
  const [loading, setLoading] = useState(false);

  const reloadMinerDrones = useCallback(() => {
    loadUserMinerClaimers({ userAddress: accountAddress, contractId: nftAddress }).then();
  }, [accountAddress, nftAddress]);

  useEffect(() => {
    if (isConnected && open && !minerDrones.isFetched) {
      reloadMinerDrones();
    }
  }, [open, minerDrones.isFetched, reloadMinerDrones, isConnected]);

  const handleEquip = async (assetId: string) => {
    setLoading(true);

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      await minerDroneApi.equip(+assetId);

      await wait(5000);

      reloadMinerDrones();
      userMinerDrone.refetch();
      reloadNfts?.();

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      toaster.captureException(e);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }

    setLoading(false);
  };
  const handleUnEquip = async () => {
    setLoading(true);

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      await minerDroneApi.unEquip();

      await wait(5000);

      reloadMinerDrones();
      userMinerDrone.refetch();
      reloadNfts?.();

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      toaster.captureException(e);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }

    setLoading(false);
  };
  const handleStart = async (intervalSeconds: number) => {
    setLoading(true);

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      await minerDroneApi.start(intervalSeconds);

      await wait(5000);

      userMinerDrone.refetch();

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      toaster.captureException(e);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }

    setLoading(false);
  };
  const handleStop = async () => {
    setLoading(true);

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      await minerDroneApi.stop();

      await wait(5000);

      userMinerDrone.refetch();

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      toaster.captureException(e);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }

    setLoading(false);
  };

  return (
    <MinerClaimerDialogUi
      open={open}
      onClose={onClose}
      minerDrones={minerDrones.data}
      minerDrone={userMinerDrone.data}
      loading={userMinerDrone.loading}
      loadingAction={loading}
      reloadNfts={reloadNfts}
      handleEquip={handleEquip}
      handleUnEquip={handleUnEquip}
      handleStart={handleStart}
      handleStop={handleStop}
      telegramId={telegramId}
      noGas={noGas}
    />
  );
}

export default React.memo(MinerDroneDialog);
