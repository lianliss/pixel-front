import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import React, { useEffect, useState } from 'react';

import styles from './MinerClaimerDialog.module.scss';

import { Button } from '@ui-kit/Button/Button.tsx';
import { MinerNoDrone } from 'lib/MinerClaimer/components/MinerClaimerDialog/components/MinerNoDrone.tsx';
import { MinerDroneNftList } from 'lib/MinerClaimer/components/MinerClaimerDialog/components/MinerDroneNftList.tsx';
import {
  IFindUserMinerDroneResponse,
  IUserMinerDroneDto,
} from 'lib/MinerClaimer/api-server/miner-claimer.types.ts';
import { MinerDroneEquipped } from 'lib/MinerClaimer/components/MinerClaimerDialog/components/MinerDroneEquipped.tsx';
import { MinerDroneSkeleton } from 'lib/MinerClaimer/components/MinerClaimerDialog/components/MinerDroneSkeleton.tsx';
import MinerDroneTakeOff from 'lib/MinerClaimer/components/MinerClaimerDialog/components/MinerDroneTakeOff.tsx';
import { useTranslation } from 'react-i18next';
import BuyGasAlertLazy from 'lib/TelegramStars/components/BuyGasAlert/BuyGasAlert.lazy.tsx';

export type MinerClaimerDialogUiProps = {
  open: boolean;
  onClose?: () => void;
  reloadNfts?: () => void;

  handleUnEquip: () => void;
  handleStart: (intervalSeconds: number) => void;
  handleStop: () => void;
  handleEquip: (assetId: string) => void;

  loading: boolean;
  loadingAction?: boolean;
  minerDrone: IUserMinerDroneDto | null;
  minerDrones: IFindUserMinerDroneResponse;
  telegramId?: number;
  noGas?: boolean;
};

export function MinerClaimerDialogUi(props: MinerClaimerDialogUiProps) {
  const {
    open,
    onClose,
    reloadNfts,
    handleStart,
    handleStop,
    handleUnEquip,
    handleEquip,
    minerDrones,
    minerDrone,
    loading = false,
    loadingAction = false,
    telegramId,
    noGas = false,
  } = props;

  const { t } = useTranslation('inventory');

  const [nftListVisible, setNftListVisible] = useState(false);
  const [takeOffVisible, setTakeOffVisible] = useState(false);
  const [minerNft, setMinerNft] = useState<string | null>(null);

  const selectMinerNft = (assetId: string) => {
    setMinerNft(assetId);
  };

  useEffect(() => {
    if (!open) {
      setTakeOffVisible(false);
      setNftListVisible(false);
    }
  }, [open]);

  return (
    <BottomDialog
      isOpen={open}
      onClose={onClose}
      footer={
        <>
          {!loading && !minerDrone && nftListVisible ? (
            <div className={styles.btns}>
              <Button
                size='large'
                variant='outlined'
                onClick={() => setNftListVisible(false)}
                loading={loadingAction}
              >
                {t('Cancel')}
              </Button>
              <Button
                size='large'
                variant='contained'
                onClick={() => {
                  if (minerNft) {
                    handleEquip(minerNft);
                    setNftListVisible(false);
                  }
                }}
                disabled={!minerNft}
                loading={loadingAction}
              >
                {t('Apply')}
              </Button>
            </div>
          ) : !takeOffVisible ? (
            <div className={styles.btns}>
              <Button
                size='large'
                variant='outlined'
                onClick={onClose}
                fullWidth
                disabled={loadingAction}
              >
                Ok
              </Button>
            </div>
          ) : (
            <div className={styles.btns}>
              <Button
                size='large'
                variant='outlined'
                onClick={() => setTakeOffVisible(false)}
                loading={loadingAction}
              >
                {t('Cancel')}
              </Button>
              <Button
                size='large'
                variant='contained'
                onClick={handleUnEquip}
                loading={loadingAction}
              >
                {t('Apply')}
              </Button>
            </div>
          )}
        </>
      }
    >
      {loading && <MinerDroneSkeleton />}
      {!loading && !minerDrone && nftListVisible && (
        <MinerDroneNftList
          minerDrones={minerDrones}
          selectMinerNft={selectMinerNft}
          selectedMinerNft={minerNft || undefined}
        />
      )}
      {!loading && !minerDrone && !nftListVisible && (
        <MinerNoDrone
          handleInsert={() => {
            setNftListVisible(true);
            setTakeOffVisible(false);
          }}
          loadingAction={loadingAction}
        />
      )}
      {!loading && minerDrone && !takeOffVisible && (
        <MinerDroneEquipped
          handleStop={handleStop}
          handleStart={handleStart}
          minerDrone={minerDrone}
          handleTake={() => setTakeOffVisible(true)}
          loadingAction={loadingAction}
          telegramId={telegramId}
          noGas={noGas}
        />
      )}
      {!loading && minerDrone && takeOffVisible && <MinerDroneTakeOff minerDrone={minerDrone} />}

      {noGas && <BuyGasAlertLazy variant='outlined' sx={{ mt: 2 }} />}
    </BottomDialog>
  );
}
