import React, { useCallback, useEffect } from 'react';
import AutoClaimMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/AutoclaimMinerDrone/AutoclaimMinerDrone.tsx';
import { useTranslation } from 'react-i18next';
import { styled } from '@mui/material/styles';
import MinerDroneDialog from 'lib/MinerClaimer/components/MinerClaimerDialog/MinerClaimerDialog.container.tsx';
import { useUnit } from 'effector-react';
import {
  loadUserMinerClaimer,
  userMinerClaimerStore,
} from 'lib/MinerClaimer/api-server/miner-claimer.store.ts';
import Box, { BoxProps } from '@mui/material/Box';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import SettingsIcon from '@mui/icons-material/Settings';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import { MINER_CLAIMER_CONTRACT_ID } from 'lib/MinerClaimer/api-contract/miner-claimer.constants.ts';
import { getMinerDroneTimedProgress } from 'lib/MinerClaimer/utils/progress.ts';
import { useUserMinerClaimer } from 'lib/MinerClaimer/hooks/useUserMinerClaimer.ts';

const Root = styled(Box)(() => ({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',

  '& > *:first-child': {
    width: '50%',
    maxWidth: 140,
  },
}));
const Info = styled(Box)(() => ({
  display: 'flex',
  alignItems: 'center',
  gap: 8,

  '& > *:first-child': {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  '& > .MuiButton-root': {
    minWidth: 'initial',
    padding: '7px',
    height: 40,
  },
}));

type Props = BoxProps;

function MinerClaimerStatus(props: Props) {
  const { t } = useTranslation('mining');
  const { accountAddress, isConnected, chainId } = useAccount();

  const droneAddress = MINER_CLAIMER_CONTRACT_ID[chainId];

  const [openMinerDrone, setOpenMinerDrone] = React.useState(false);
  const userMinerDrone = useUserMinerClaimer({
    minerDroneAddress: droneAddress,
    accountAddress: accountAddress,
  });

  const isStarted = !!userMinerDrone.data?.startTime;
  const sessionClaims = userMinerDrone.data?.sessionClaims;
  const timedProgress = getMinerDroneTimedProgress(userMinerDrone.data);
  const totalClaims = userMinerDrone.data?.nft?.type?.minerDroneType?.sessionClaims;

  return (
    <Root {...props}>
      {userMinerDrone.data ? (
        <AutoClaimMinerDrone
          isActive={isStarted}
          handleStart={() => setOpenMinerDrone(true)}
          handleStop={() => setOpenMinerDrone(true)}
          loading={false}
          disabled={false}
        >
          {isStarted ? t('Autoclaim on') : t('Autoclaim off')}
        </AutoClaimMinerDrone>
      ) : (
        <div />
      )}

      <Info onClick={() => setOpenMinerDrone(true)}>
        <div>
          <Typography variant='subtitle2' color='text.primary' fontWeight='bold'>
            Smart Claimer
          </Typography>
          <Typography variant='caption' align='right'>
            {userMinerDrone.data && (
              <>
                {!userMinerDrone.data.isTimeSession && (
                  <>
                    <Typography variant='caption' component='span' color='success.main'>
                      {sessionClaims || 0}
                    </Typography>
                    <Typography variant='caption' component='span' color='text.secondary'>
                      /{totalClaims || 0} claims
                    </Typography>
                  </>
                )}
                {!!userMinerDrone.data.isTimeSession && (
                  <>
                    <Typography variant='caption' component='span' color='success.main'>
                      {timedProgress.currentHours}
                    </Typography>
                    <Typography variant='caption' component='span' color='text.secondary'>
                      /{timedProgress.totalHours} h
                    </Typography>
                  </>
                )}
              </>
            )}
            {!userMinerDrone.data && userMinerDrone.isFetched && (
              <Typography variant='caption' component='span' color='text.secondary'>
                {t('Not Selected')}
              </Typography>
            )}
          </Typography>
        </div>
        <Button variant='outlined' size='small'>
          <SettingsIcon />
        </Button>
      </Info>

      {/** Miner Drone **/}
      <MinerDroneDialog
        open={openMinerDrone}
        data={userMinerDrone.data}
        onClose={() => setOpenMinerDrone(false)}
      />
    </Root>
  );
}

export default MinerClaimerStatus;
