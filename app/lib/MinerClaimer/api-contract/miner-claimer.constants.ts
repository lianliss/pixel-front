import { Chain } from 'services/multichain/chains';

export const MINER_CLAIMER_CONTRACT_ID = {
  [Chain.SONGBIRD]: '0x5eD0CF57400211B846cB38A5d02724Dadf287a9D'.toLowerCase(),
  [Chain.SKALE]: '0xBdB897cc1A592F0143624D1ABBAAB3bB4D626BCC'.toLowerCase(),
  [Chain.UNITS]: '0x3f7467d21E4b2a5a21C24bE6E5101af537C57bB0'.toLowerCase(),
};
