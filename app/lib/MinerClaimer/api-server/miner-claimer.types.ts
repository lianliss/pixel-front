import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { IPaginatedResult } from 'lib/Quests/api-server/quests.types.ts';

// request

export type IGetMinerClaimerDto = { userAddress: string; contractId: string };

export type IFindUserMinerClaimersDto = { userAddress: string; contractId: string };

export type IGetMinerClaimerStatsDto = { contractId: string };

// response

export type IGetMinerClaimerResponse = IUserMinerClaimerDto | null;

export type IFindUserMinerClaimerResponse = IPaginatedResult<INftItemDto>;

export type IGetMinerClaimerStatsResponse = { total: number; stopped: number; started: number };

// entities

export type IMinerClaimerDto<TDate = number> = Omit<IMinerClaimer<TDate>, 'removedAt'> & {
  removedAt: TDate | null;
};

export type IMinerClaimer<TDate = Date> = {
  id: string;

  minerAddress: string;
  hasReferral: boolean;

  disabled: boolean;
  removedAt?: TDate | null;
  createdAt: TDate;
};

export type IMinerClaimerType<TDate = Date> = {
  id: string;

  typeId: string;
  duration: string;
  sessionClaims: number;
  isTimeSession: boolean;
  sessionDuration: string;
  startPrice: number;
  startPriceWei: string;
  txPriceMod: number;

  removedAt?: TDate | null;
  createdAt: TDate;
};

export type IMinerClaimerTypeDto<TDate = number> = Omit<IMinerClaimerType<TDate>, 'removedAt'> & {
  removedAt: TDate | null;
};

export type IUserMinerClaimerDto<TDate = number> = Omit<
  IUserMinerClaimer<TDate>,
  'removedAt' | 'startTime' | 'timing' | 'depositSize' | 'nextClaimAt' | 'telegramId' | 'type'
> & {
  removedAt: TDate | null;
  nextClaimAt: TDate | null;
  startTime: string | null;
  timing: string | null;
  depositSize: string | null;

  // type?: INftTypeDto<TDate>;
  nft: INftItemDto | null;
};

export type IUserMinerClaimer<TDate = Date> = {
  id: string;
  userAddress: string;
  telegramId: number;

  typeId: string;
  tokenId: string;

  duration: string;
  sessionClaims: number;
  isTimeSession: boolean;

  startTime?: string | null;
  timing?: string | null;
  depositSize: string;

  totalClaims: number;

  nextClaimAt?: TDate | null;
  removedAt?: TDate | null;
  createdAt: TDate;

  // relations
  // type?: INftType;
};
