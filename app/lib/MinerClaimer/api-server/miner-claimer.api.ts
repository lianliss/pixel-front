'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  IGetMinerClaimerResponse,
  IGetMinerClaimerDto,
  IFindUserMinerClaimersDto,
  IFindUserMinerClaimerResponse,
  IGetMinerClaimerStatsResponse,
  IGetMinerClaimerStatsDto,
} from './miner-claimer.types.ts';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';

import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';
import {
  pixelMessageKey,
  pixelSignKey,
  telegramAuthKey,
} from '../../../shared/const/localStorage.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class MinerClaimerApi {
  url: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async getForUser({
    userAddress,
    contractId,
  }: IGetMinerClaimerDto): Promise<IGetMinerClaimerResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetMinerClaimerResponse },
      AxiosResponse<{ data: IGetMinerClaimerResponse }>,
      IGetMinerClaimerDto,
      {}
    >(`/api/v1/miner/claimer/current`, {
      baseURL: this.url,
      params: { userAddress, contractId: contractId.toLowerCase() },
    });
    return data.data;
  }

  async findUserDrones({
    userAddress,
    contractId,
  }: IFindUserMinerClaimersDto): Promise<IFindUserMinerClaimerResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindUserMinerClaimerResponse },
      AxiosResponse<{ data: IFindUserMinerClaimerResponse }>,
      IFindUserMinerClaimersDto
    >(`/api/v1/miner/claimer/find/user`, {
      baseURL: this.url,
      params: { userAddress, contractId: contractId.toLowerCase() },
    });

    return data.data;
  }

  async getStats({ contractId }: IGetMinerClaimerStatsDto): Promise<IGetMinerClaimerStatsResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetMinerClaimerStatsResponse },
      AxiosResponse<{ data: IGetMinerClaimerStatsResponse }>,
      IGetMinerClaimerStatsDto
    >(`/api/v1/miner/claimer/stats`, {
      baseURL: this.url,
      params: {
        contractId: contractId.toLowerCase(),
      },
    });

    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const minerClaimerApi = new MinerClaimerApi({ url: 'https://api.hellopixel.network' });
