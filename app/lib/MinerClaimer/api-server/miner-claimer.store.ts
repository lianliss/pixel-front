import { createEffect, createStore } from 'effector';
import {
  IFindUserMinerClaimerResponse,
  IFindUserMinerClaimersDto,
  IGetMinerClaimerDto,
  IGetMinerClaimerResponse,
  IGetMinerClaimerStatsDto,
  IGetMinerClaimerStatsResponse,
} from 'lib/MinerClaimer/api-server/miner-claimer.types.ts';
import { minerClaimerApi } from 'lib/MinerClaimer/api-server/miner-claimer.api.ts';

// test address 0x9f6627e8adf52430be0abf77d40654ab8f82def4

export const loadUserMinerClaimer = createEffect(async (params: IGetMinerClaimerDto) => {
  const res = await minerClaimerApi.getForUser({
    userAddress: params.userAddress.toLowerCase(),
    contractId: params.contractId,
  });

  return res;
});

export const userMinerClaimerStore = createStore<{
  data: IGetMinerClaimerResponse | null;
  isFetched: boolean;
  loading: boolean;
}>({
  data: null,
  isFetched: false,
  loading: false,
})
  .on(loadUserMinerClaimer.doneData, (prev, next) => {
    return { loading: false, isFetched: true, data: next };
  })
  .on(loadUserMinerClaimer.pending, (prev, next) => {
    return { ...prev, loading: next };
  })
  .on(loadUserMinerClaimer.failData, (prev, next) => {
    return { loading: false, data: null, isFetched: true };
  });

export const loadUserMinerClaimers = createEffect(async (params: IFindUserMinerClaimersDto) => {
  const res = await minerClaimerApi.findUserDrones({
    userAddress: params.userAddress.toLowerCase(),
    contractId: params.contractId,
  });

  return res;
});

export const userMinerClaimersStore = createStore<{
  data: IFindUserMinerClaimerResponse;
  isFetched: boolean;
  loading: boolean;
}>({
  data: {
    rows: [],
    count: 0,
  },
  isFetched: false,
  loading: false,
})
  .on(loadUserMinerClaimers.doneData, (prev, next) => {
    return { loading: false, isFetched: true, data: next };
  })
  .on(loadUserMinerClaimers.pending, (prev, next) => {
    return { ...prev, loading: next };
  })
  .on(loadUserMinerClaimers.failData, (prev, next) => {
    return { loading: false, data: { rows: [], count: 0 }, isFetched: true };
  });

// stats

export const loadMinerClaimerStats = createEffect(async (params: IGetMinerClaimerStatsDto) => {
  const res = await minerClaimerApi.getStats({ contractId: params.contractId });

  return res;
});

export const minerClaimerStatsStore = createStore<{
  data: IGetMinerClaimerStatsResponse | null;
  isFetched: boolean;
  loading: boolean;
}>({
  data: null,
  isFetched: false,
  loading: false,
})
  .on(loadMinerClaimerStats.doneData, (prev, next) => {
    return { loading: false, isFetched: true, data: next };
  })
  .on(loadMinerClaimerStats.pending, (prev, next) => {
    return { ...prev, loading: next };
  })
  .on(loadMinerClaimerStats.failData, (prev, next) => {
    return { loading: false, data: null, isFetched: true };
  });
