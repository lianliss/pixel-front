import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { MINER_CLAIMER_ABI } from 'lib/MinerClaimer/api-contract/miner-claimer.abi.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';

import { MINER_CLAIMER_CONTRACT_ID } from 'lib/MinerClaimer/api-contract/miner-claimer.constants.ts';
import { readContract } from '@wagmi/core';
import { wagmiConfig } from '@chain/wagmi.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

const ERC_20_ABI = require('const/ABI/Erc20Token');

export function useMinerClaimerApi() {
  const { chainId } = useAccount();
  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;

  const address = MINER_CLAIMER_CONTRACT_ID[chainId];

  const tradeToken = useTradeToken();
  const isCoin = tradeToken.data?.isCoin;

  const write = useWriteContract({
    abi: MINER_CLAIMER_ABI,
    address,
  });
  const writeTradeToken = useWriteContract({
    abi: ERC_20_ABI,
    address: tradeToken.data?.address,
  });

  const handleEquip = (tokenId: number) => {
    return write.writeContractAsync({ functionName: 'equip', args: [tokenId] });
  };
  const handleUnEquip = () => {
    return write.writeContractAsync({ functionName: 'unequip', args: [] });
  };
  const handleStart = async (intervalSeconds: number) => {
    console.log('[handleStart]', intervalSeconds);

    const requiredDeposit = await readContract(wagmiConfig, {
      abi: MINER_CLAIMER_ABI,
      address: address,
      functionName: 'getTimingDepositSize',
      args: [telegramId, intervalSeconds],
    });

    if (!isCoin) {
      await writeTradeToken.writeContractAsync({
        functionName: 'approve',
        args: [address, requiredDeposit],
      });
    }

    await write.writeContractAsync({
      functionName: 'start',
      args: [intervalSeconds],
      overrides: isCoin ? { value: requiredDeposit } : undefined,
    });
  };
  const handleStop = () => {
    return write.writeContractAsync({ functionName: 'stop', args: [] });
  };

  return {
    enabled: !!address,
    address,
    equip: handleEquip,
    unEquip: handleUnEquip,
    start: handleStart,
    stop: handleStop,
  };
}
