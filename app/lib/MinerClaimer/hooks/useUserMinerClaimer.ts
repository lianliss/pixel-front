import { useCallback, useEffect } from 'react';
import {
  loadUserMinerClaimer,
  userMinerClaimerStore,
} from 'lib/MinerClaimer/api-server/miner-claimer.store.ts';
import { useUnit } from 'effector-react';

export function useUserMinerClaimer({
  minerDroneAddress,
  accountAddress,
}: {
  minerDroneAddress: string;
  accountAddress?: string;
}) {
  const minerDrone = useUnit(userMinerClaimerStore);

  const reloadMinerDrone = useCallback(() => {
    loadUserMinerClaimer({
      userAddress: accountAddress?.toLowerCase(),
      contractId: minerDroneAddress,
    }).then();
  }, [accountAddress, minerDroneAddress]);

  useEffect(() => {
    if (accountAddress) {
      reloadMinerDrone();
    }
  }, [reloadMinerDrone]);

  return {
    data: minerDrone.data,
    loading: minerDrone.loading,
    refetch: reloadMinerDrone,
    isFetched: minerDrone.isFetched,
  };
}
