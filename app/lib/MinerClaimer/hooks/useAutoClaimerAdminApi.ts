import React from 'react';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { MINER_CLAIMER_CONTRACT_ID } from 'lib/MinerClaimer/api-contract/miner-claimer.constants.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { MINER_CLAIMER_ABI } from 'lib/MinerClaimer/api-contract/miner-claimer.abi.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import toaster from 'services/toaster.tsx';
import get from 'lodash/get';
import wei from 'utils/wei';

type AutoClaimBot = {
  typeId: number;
  sessionClaims: number;
  durationSeconds: number;
  isTimeSession: boolean;
  sessionSeconds: number;
  startPrice: number;
  txPriceMod: number;
};

export type AutoClaimBots = Record<number, AutoClaimBot>;

export function useAutoClaimerAdminApi() {
  const { chainId, isConnected } = useAccount();

  const [isLoading, setIsLoading] = React.useState(true);
  const [botsTypes, setBotsTypes] = React.useState<AutoClaimBots>({});
  const [list, setList] = React.useState<number[]>([]);

  const botAddress = MINER_CLAIMER_CONTRACT_ID[chainId];

  const typesData = useReadContract({
    abi: MINER_CLAIMER_ABI,
    address: botAddress,
    skip: !botAddress || !isConnected,
    functionName: 'getBotTypes',
  });

  const write = useWriteContract({
    abi: MINER_CLAIMER_ABI,
    address: botAddress,
  });

  const updateBots = React.useMemo(
    () => async () => {
      try {
        if (!isConnected || !chainId) return;
        setIsLoading(true);
        const data = await typesData.refetch();
        const rawData = get(data, 'data', data);

        const bots: AutoClaimBots = {};
        const list: number[] = [];
        rawData.map((bot) => {
          const typeId = Number(bot.typeId);

          bots[typeId] = {
            typeId,
            sessionClaims: Number(bot.sessionClaims),
            isTimeSession: Boolean(bot.isTimeSession),
            sessionSeconds: Number(bot.sessionSeconds),
            durationSeconds: Number(bot.durationSeconds),
            startPrice: wei.from(bot.startPrice),
            txPriceMod: Number(bot.txPriceMod),
          };
          list.push(typeId);
        });
        setBotsTypes(bots);
        setList(list);
      } catch (error) {
        console.error('[updateBots]', error);
      }
      setIsLoading(false);
    },
    [isConnected, chainId]
  );

  React.useEffect(() => {
    if (isConnected) {
      updateBots().then();
    }
  }, [updateBots]);

  const addBot = React.useMemo(
    () =>
      async (payload: {
        typeId: number;
        durationSeconds: number;
        sessionClaims: number;
        isTimeSession: boolean;
        sessionSeconds: number;
        startPrice: number;
        txPriceMod: number;
      }) => {
        try {
          console.log('[miner-drone]', payload);

          setIsLoading(true);
          await write.writeContractAsync({
            functionName: 'setBotType',
            args: [
              payload.typeId,
              payload.durationSeconds,
              payload.sessionClaims,
              wei.to(payload.txPriceMod, 4),
              payload.isTimeSession,
              payload.sessionSeconds,
              wei.to(payload.startPrice, 18),
            ],
          });
          await updateBots();
        } catch (error) {
          toaster.captureException(error, '[addBot]');
        }
        setIsLoading(false);
      },
    [updateBots]
  );

  const unsetBot = React.useMemo(
    () => async (typeId) => {
      try {
        setIsLoading(true);
        await write.writeContractAsync({
          functionName: 'unsetBot',
          args: [typeId],
        });
        await updateBots();
      } catch (error) {
        toaster.captureException(error, '[unsetBot]');
      }
      setIsLoading(false);
    },
    [updateBots]
  );

  return {
    list,
    data: botsTypes,
    loading: isLoading,
    refetch: updateBots,
    set: addBot,
    unset: unsetBot,
  };
}
