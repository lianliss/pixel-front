const TIME_IN_SECOND = 1000;
const TIME_IN_MINUTE = TIME_IN_SECOND * 60;
const TIME_IN_HOUR = TIME_IN_MINUTE * 60;

export function getMinerClaimerSessionTImeStr(time: number) {
  const hours = Math.floor(time / TIME_IN_HOUR);
  const minutes = Math.floor((time - hours * TIME_IN_HOUR) / TIME_IN_MINUTE);

  return [hours && `${hours} hours`, minutes && `${minutes} min`].filter(Boolean).join(' ');
}
