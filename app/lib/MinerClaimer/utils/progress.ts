import { IUserMinerDroneDto } from 'lib/MinerClaimer/api-server/miner-claimer.types.ts';

export function getMinerDroneTimedProgress(userMinerDrone?: IUserMinerDroneDto): {
  total: number;
  current: number;
  totalHours: number;
  currentHours: number;
} {
  if (!userMinerDrone) {
    return {
      total: 0,
      current: 0,
      totalHours: 0,
      currentHours: 0,
    };
  }

  const startTime = userMinerDrone?.startTime
    ? new Date(+userMinerDrone.startTime).getTime()
    : undefined;
  const sessionDuration = userMinerDrone?.startTime ? Number(new Date().getTime() - startTime) : 0;
  const totalDuration = Number(userMinerDrone?.nft?.type?.minerDroneType?.sessionDuration || '0');
  const totalHours = +(totalDuration / 60 / 60 / 1000).toFixed(2);
  const currentHours = +Math.floor(sessionDuration / 60 / 60 / 1000).toFixed(2);

  return {
    total: Math.floor(totalDuration),
    current: totalDuration - sessionDuration,
    totalHours: +totalHours.toFixed(2),
    currentHours: +Math.max(0, totalHours - currentHours).toFixed(2),
  };
}
