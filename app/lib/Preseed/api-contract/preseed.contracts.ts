import { Chain } from 'services/multichain/chains';

export const PRESEED_CONTRACT_ID = {
  [Chain.POLYGON_MAINNET]: {
    core: '0xc3C8aF5Cf071d35a2AeF21eF2369D2ea28109886',
    token: '0xc2132D05D31c914a87C6611C10748AEb04B58e8F',
  },
};
