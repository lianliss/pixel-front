import React, { useMemo } from 'react';
import styles from './Preseed.module.scss';
import { Input, WalletBlock } from 'ui';
import toaster from 'services/toaster';

import { wei } from 'utils';
import getFinePrice from 'utils/getFinePrice';
import { IS_TELEGRAM } from '@cfg/config.ts';
import TokenHeader from 'lib/Wallet/components-v2/TokenHeader/TokenHeader.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useSwitchChain } from '@chain/hooks/useSwitchChain.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useToken } from '@chain/hooks/useToken.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';
import { useTokenPrice } from '@chain/hooks/useTokenPrice.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { PRESEED_CONTRACT_ID } from 'lib/Preseed/api-contract/preseed.contracts.ts';
import { useTranslation } from 'react-i18next';
import PRESEED_ABI from 'lib/Preseed/api-contract/abi/preseed.abi';
import ERC_20_ABI from 'lib/Wallet/api-contract/abi/erc-20.abi';
import { useBackAction } from '../../shared/telegram/useBackAction.ts';
import { getDefaultChainImage } from '@cfg/image.ts';
import Typography from '@mui/material/Typography';
import { Button } from '@ui-kit/Button/Button.tsx';
import Divider from '@mui/material/Divider';
import Container from '@mui/material/Container';
import { showConnectWallet } from '../../widget/Connect/ConnectWallet.store.ts';
import { Chain } from 'services/multichain/chains';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

function Preseed() {
  const account = useAccount();
  const { chainId, isConnected, accountAddress } = account;
  const { t } = useTranslation('preseed');

  const { switchChain } = useSwitchChain();

  const preseedAddres = PRESEED_CONTRACT_ID[chainId]?.core;
  const tokenAddress = PRESEED_CONTRACT_ID[chainId]?.token;

  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;

  const tokenInfo = useToken({
    address: tokenAddress,
  });
  const balanceData = useBalance({
    address: account.accountAddress,
    token: tokenAddress,
    skip: !account.isConnected,
  });
  const tokenPrice = useTokenPrice({
    address: tokenAddress,
  });

  const preseedState = useReadContract<[number, number, number, number, number]>({
    initData: [0, 0, 0, 0, 0],
    abi: PRESEED_ABI,
    address: preseedAddres,
    functionName: 'getPreseedData',
    skip: !preseedAddres,
  });

  const preseedData = useMemo(() => {
    const data = preseedState.data;

    if (!data) {
      return {
        min: 0,
        max: 0,
        total: 0,
        invested: 0,
        investors: 0,
      };
    }

    return {
      min: wei.from(data[0], 6),
      max: wei.from(data[1], 6),
      total: wei.from(data[2], 6),
      invested: wei.from(data[3], 6),
      investors: Number(data[4]),
    };
  }, [preseedState.data]);

  const depositData = useReadContract<{
    deposit: number;
    timestamp: number;
    name: string;
    contact: string;
  } | null>({
    abi: PRESEED_ABI,
    address: preseedAddres,
    functionName: 'getDeposit',
    args: [accountAddress],
    skip: !preseedAddres || !account.isConnected,
    select: (data) => {
      const deposit = wei.from(data.deposit, 6);

      return {
        deposit: deposit,
        timestamp: Number(data.depositTimestamp),
        name: data.name,
        contact: data.contact,
      };
    },
  });

  const allowanceState = useReadContract({
    initData: 0,
    abi: ERC_20_ABI,
    address: tokenAddress,
    functionName: 'allowance',
    args: [preseedAddres, account.accountAddress],
    skip: !tokenAddress || !account.isConnected || !tokenInfo.data,
    select: (res) => wei.from(res, tokenInfo.data?.decimals),
  });

  const writeUsdt = useWriteContract({
    abi: ERC_20_ABI,
    address: tokenAddress,
  });
  const writePreseed = useWriteContract({
    abi: PRESEED_ABI,
    address: preseedAddres,
  });

  const [name, setName] = React.useState('');
  const [contact, setContact] = React.useState('');
  const [amount, setAmount] = React.useState(0);
  const [isApproving, setIsApproving] = React.useState(false);
  const [isApproved, setIsApproved] = React.useState(false);
  const [isInvesting, setIsInvesting] = React.useState(false);

  const minInvest = 30000;
  // const isApproved = allowanceState.data >= amount;
  const getIsValid = (amount) => {
    return (
      !!amount &&
      amount >= Math.floor(preseedData?.min || 0) &&
      amount <= Math.floor(preseedData?.max || 0) &&
      amount <= Math.floor(preseedData?.total || 0 - preseedData?.invested || 0)
    );
  };
  const isValid = getIsValid(amount);

  const onApprove = async (_val = amount) => {
    if (!tokenInfo.data) {
      return;
    }

    setIsApproving(true);

    try {
      const weiAmount = wei.to(amount, tokenInfo.data.decimals);
      await writeUsdt.writeContractAsync({
        functionName: 'approve',
        args: [preseedAddres, weiAmount],
      });

      await allowanceState.refetch();

      toaster.success(`${getFinePrice(_val)} approved for operation`);
      setIsApproving(false);
      setIsApproved(true);
    } catch (error) {
      console.error(error);
      toaster.logError(error, '[Preseed][onApprove]');
      setIsApproving(false);
    }
  };

  const onInvest = async (_val = amount) => {
    if (!tokenInfo.data || !depositData.data) {
      return;
    }

    setIsInvesting(true);

    try {
      await writePreseed.writeContractAsync({
        functionName: 'invest',
        args: [wei.to(_val, tokenInfo.data.decimals), name, depositData.data.contact],
      });

      toaster.success(`${getFinePrice(_val)} sent to contract`);

      await allowanceState.refetch();
      await depositData.refetch();
      await preseedState.refetch();
      await balanceData.refetch();

      setIsInvesting(false);
    } catch (error) {
      toaster.logError(error, '[Preseed][onInvest]');
      setIsInvesting(false);
    }
  };

  const onChangeAmount = (amount) => {
    setAmount(amount);
  };

  React.useEffect(() => {
    if (!isConnected || chainId !== 137) {
      if (IS_TELEGRAM) {
        switchChain({ chainId: 137 });
        toaster.warning('Network changed to Polygon');
      }
      return;
    }

    return () => {
      if (IS_TELEGRAM) {
        switchChain({ chainId: Chain.SONGBIRD });
        toaster.warning('Network changed to Songbird');
      }
    };
  }, [chainId, isConnected, accountAddress]);
  useBackAction('/wallet');

  const isAdmin = telegramId === 162131210 || telegramId === 24931187;

  const renderDepositTitle = () => {
    return (
      <div className={styles.preseedDeposit}>
        <div className={styles.preseedDepositTitle}>{t('Deposit')}</div>
        <div className={styles.preseedDepositRight}>
          {getFinePrice(preseedData.min)} - {getFinePrice(preseedData.max)}
        </div>
      </div>
    );
  };

  const renderForm = () => {
    if (!isConnected) {
      return (
        <div className={styles.preseedPolygon}>
          <p>{t('Please connect your wallet to continue')}</p>
        </div>
      );
    }
    if (chainId !== 137) {
      return (
        <div className={styles.preseedPolygon}>
          <img src={'https://cryptologos.cc/logos/polygon-matic-logo.png'} alt={'Chain ID: 137'} />
          <p>
            {t('PRE-SEED round is available')}
            <br />
            {t('only in Polygon network')}
          </p>
          <Button
            icon={'link'}
            variant='contained'
            color='primary'
            onClick={async () => {
              switchChain({ chainId: 137 });
            }}
            large
          >
            {t('Switch to Polygon')}
          </Button>
        </div>
      );
    } else {
      const isShow = isAdmin || preseedData?.invested >= minInvest;
      return (
        <div className={styles.preseedForm}>
          <div className={styles.preseedRaised}>
            <p>{isShow ? t('Total fundraised') : t('Total round fundraise')}</p>
            <div className={styles.preseedRaisedValues}>
              {isShow && (
                <div className={styles.preseedRaisedInvested}>
                  ${getFinePrice(preseedData?.invested || 0)}
                </div>
              )}
              <div className={styles.preseedRaisedTotal}>
                ${getFinePrice(preseedData?.total || 0)}
              </div>
            </div>
          </div>
          <div className={styles.preseedUserDeposit}>
            <small>{t('You have invested')}</small>
            <span>${getFinePrice(depositData.data?.deposit || 0)}</span>
          </div>
          <WalletBlock title={t('Your name')}>
            <Input type={'text'} onTextChange={setName} value={depositData.data?.name || name} />
          </WalletBlock>
          <WalletBlock title={t('Your Telegram contact')}>
            <Input
              type={'text'}
              disabled={IS_TELEGRAM}
              onTextChange={setContact}
              value={depositData.data?.contact || contact}
            />
          </WalletBlock>
          <WalletBlock title={renderDepositTitle()}>
            <Input
              type={'number'}
              onTextChange={onChangeAmount}
              positive
              indicator={'USDT'}
              max={preseedData?.min}
              min={preseedData?.max}
              value={amount}
            />
          </WalletBlock>
        </div>
      );
    }
  };

  const renderButtons = () => {
    // if (IS_TELEGRAM) {
    //   return <></>;
    // }
    if (!isConnected) {
      return (
        <Button
          size='large'
          variant='contained'
          icon={'antenna'}
          onClick={() => showConnectWallet()}
        >
          {t('Connect wallet')}
        </Button>
      );
    } else {
      if (chainId !== 137) {
        return <></>;
      }
      return (
        <div className={styles.preseedButtons}>
          <Button
            size='large'
            variant='contained'
            disabled={!amount || isApproved || !isValid || !tokenInfo.data}
            color='primary'
            loading={isApproving}
            // icon={isApproved ? 'unlock' : 'lock'}
            onClick={() => onApprove()}
          >
            {t('Approve USDT')}
          </Button>
          <Button
            size='large'
            variant='contained'
            disabled={!isValid || !isApproved || !tokenInfo.data || !depositData.data}
            color={isApproved ? 'primary' : undefined}
            loading={isInvesting}
            onClick={() => onInvest()}
          >
            {t('Deposit')}
          </Button>
        </div>
      );
    }
  };

  return (
    <div
      className={styles.preseedWrap}
      style={{
        backgroundImage: `url(${getDefaultChainImage(account.chainId)})`,
        backgroundSize: 'cover',
      }}
    >
      <Container maxWidth='xs'>
        {tokenInfo.data && (
          <TokenHeader
            token={tokenInfo.data}
            balance={balanceData.data?.formatted}
            price={tokenPrice.data}
            showBalance
          />
        )}

        <Divider />

        <div className={styles.preseed} style={{ marginTop: 12 }}>
          <Typography variant='h2' fontWeight='bold' color='text.primary'>
            {t('Early investors')}
          </Typography>
          <Typography color='text.primary' variant='caption' align='center'>
            {t('For early')}{' '}
            <a href={'https://docs.hellopixel.network/tokenomics/tokenomics'} target={'_blank'}>
              {t('tokenomics')}
            </a>{' '}
            {t('is allocated at the price of $0,01 per token')}
            <br />
            <a
              href={'https://docs.hellopixel.network/tokenomics/early-investors'}
              target={'_blank'}
            >
              {t('Read more')}
            </a>
          </Typography>
          {renderForm()}
          {renderButtons()}
        </div>
      </Container>
    </div>
  );
}

export default Preseed;
