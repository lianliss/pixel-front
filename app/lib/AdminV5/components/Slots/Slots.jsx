import React from 'react';
import styles from './Slots.module.scss';
import toaster from 'services/toaster';
import wei from 'utils/wei';
import getFinePrice from 'utils/getFinePrice';
import { CopyToClipboard } from 'react-copy-to-clipboard';

function Slots() {
  const contract = null;

  const [slots, setSlots] = React.useState([]);
  const updateSlots = async () => {
    try {
      const data = await contract.methods.getSlotsTypes().call();
      setSlots(
        data.map((slot) => ({
          title: slot.title,
          pixelPrice: wei.from(slot.pixelPrice),
          sgbPrice: wei.from(slot.sgbPrice),
          require: Number(slot.requireSlot),
        }))
      );
    } catch (error) {
      toaster.logError(error);
    }
  };
  React.useEffect(() => {
    if (!contract) return;
    updateSlots();
  }, [contract]);

  return (
    <div className={styles.slots}>
      {slots.map((slot, index) => (
        <div className={styles.slotsItem} key={index}>
          <b>
            <CopyToClipboard
              text={slot.title}
              onCopy={() => {
                toaster.warning(`${slot.title} copied`);
              }}
            >
              <span>
                {index}. {slot.title}
              </span>
            </CopyToClipboard>
          </b>
          <span>pixelPrice: {getFinePrice(slot.pixelPrice)}</span>
          <span>sgbPrice: {getFinePrice(slot.sgbPrice)}</span>
          <span>requireSlot: {slot.require}</span>
        </div>
      ))}
    </div>
  );
}

export default Slots;
