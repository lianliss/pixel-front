import React from 'react';
import { useUserRole } from 'lib/AdminV5/hooks/useUserRole.ts';
import styles from './AdminLayout.module.scss';
import { Tab } from '@ui-kit/Tab/Tab.tsx';
import { Tabs } from '@ui-kit/Tabs/Tabs.tsx';
import { useLocation, useNavigate } from 'react-router-dom';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';

export function AdminLayout({ children }: React.PropsWithChildren) {
  const role = useUserRole();
  const navigate = useNavigate();
  const location = useLocation();

  if (!role.isConnected) {
    return null;
  }
  if (!role.editor) {
    return 'no access';
  }

  return (
    <div className={styles.editor}>
      <ChainSwitcher />

      <Typography variant='h2' fontWeight='bold'>
        Inventory Editor
      </Typography>

      <Tabs
        onChange={(_, nextPath) => {
          if (nextPath !== location.pathname) {
            navigate(nextPath);
          }
        }}
        variant='scrollable'
        scrollButtons='auto'
        value={location.pathname}
      >
        <Tab value='/editor' label='NFT' />
        <Tab value='/admin/recipe' label='NFT Recipes' />
        <Tab value='/admin/blender' label='NFT Blender' />
        <Tab value='/editor/chests' label='Chests OLD' />
        <Tab value='/editor/chests' label='Chests' />
        <Tab value='/editor/sales' label='Sales' />
        <Tab value='/editor/quests' label='Quests' />
        <Tab value='/admin/quest' label='Quests V2' />
        {/*<Tab value='/editor/user' label='User Preview' />*/}
        <Tab value='/admin/bounty' label='Bounty' />
        <Tab value='/admin/lottery' label='Lottery' />
      </Tabs>

      <div style={{ marginTop: 12 }}>{children}</div>
    </div>
  );
}
