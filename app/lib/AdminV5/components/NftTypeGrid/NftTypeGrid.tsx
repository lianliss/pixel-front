import React from 'react';
import styles from './NFtTypeGrid.module.scss';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import AddType from 'lib/AdminV5/pages/nft-editor/components/AddType/AddType';
import EditVariants from 'lib/AdminV5/pages/nft-editor/components/EditVariants/EditVariants';
import { convertToNftItemDto } from 'lib/NFT/components/NFTCard/convertToNftItemDto.ts';
import { Button } from '@ui-kit/Button/Button.tsx';
import { ButtonGroup } from '@ui-kit/ButtonGroup/ButtonGroup.tsx';
import { useNftTypes } from 'lib/NFT/hooks/useNftTypes.ts';
import Grid from '@mui/material/Grid';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

function NftTypeGrid({ collectionId, collectionName }) {
  const itemTypesData = useNftTypes();
  const itemTypes = itemTypesData.data;

  const [editId, setEditId] = React.useState(null);
  const [clonedItem, setClonedItem] = React.useState(null);

  const types = React.useMemo(() => {
    const types = itemTypes.filter((t) => t.collectionId === collectionId);

    return [...types];
  }, [itemTypes.length, collectionId]);

  return (
    <div>
      <Grid container spacing={3}>
        {types.map((itemType) => {
          const { typeId } = itemType;

          return (
            <Grid item xs={12} lg={2} key={`item-${typeId}`}>
              <NFTCard nft={convertToNftItemDto(itemType)} />

              <Typography variant='caption'>id: {itemType.typeId}</Typography>

              <div style={{ display: 'flex', flexDirection: 'column', gap: 4 }}>
                <ButtonGroup>
                  <Button
                    variant='contained'
                    color='primary'
                    fullWidth
                    onClick={() => {
                      setEditId(typeId);
                    }}
                  >
                    Edit
                  </Button>
                  <Button
                    variant='contained'
                    color='primary'
                    fullWidth
                    onClick={() => {
                      setClonedItem(itemType);
                      setEditId(-1);
                    }}
                  >
                    Clone
                  </Button>
                </ButtonGroup>
              </div>
            </Grid>
          );
        })}
        <Grid item xs={12} lg={2}>
          <div
            className={styles.add}
            key={-1}
            onClick={() => {
              setEditId(-1);
            }}
          >
            + Add Type
          </div>
        </Grid>
      </Grid>
    </div>
  );
}

export default NftTypeGrid;
