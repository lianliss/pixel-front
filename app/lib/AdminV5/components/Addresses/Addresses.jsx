import React from 'react';
import styles from './Addresses.module.scss';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import toaster from 'services/toaster';
import { Icon } from '@blueprintjs/core';
import { CONTRACT_ADDRESSES } from 'services/multichain/contracts';
import { useAccount } from '@chain/hooks/useAccount';

function Addresses() {
  const account = useAccount();
  const pixelContracts = (CONTRACT_ADDRESSES[account.chainId] || {})?.pixel;

  // const pixelContracts = get(network, 'contractAddresses.pixel', {});

  return (
    <div className={styles.addresses}>
      {Object.keys(pixelContracts).map((name) => {
        const address = pixelContracts[name];
        return (
          <div className={styles.addressesCopy} key={name}>
            <CopyToClipboard
              text={address}
              onCopy={() => {
                toaster.warning(`${name} address copied`);
              }}
            >
              <div className={styles.addressesCopyContent}>
                <b>{name}:&nbsp;</b>
                <span>{address}</span>
                <Icon icon={'duplicate'} size={14} />
              </div>
            </CopyToClipboard>
          </div>
        );
      })}
    </div>
  );
}

export default Addresses;
