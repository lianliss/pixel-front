import React from 'react';
import styles from './Contract.module.scss';
import { useWeb3 } from 'services/web3Provider';
import toaster from 'services/toaster';
import { Tab, Tabs } from '@blueprintjs/core';
// import Roles from 'lib/Admin/components/Roles/Roles';
// import Write from 'lib/Admin/components/Write/Write';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTranslation } from 'react-i18next';

export const ContractContext = React.createContext();

function Contract({
  title,
  roles = [],
  contractAddress,
  abi,
  children,
  writeChildren,
  methods = {},
}) {
  const account = useAccount();
  const { chainId, transaction, accountAddress } = account;
  const { t } = useTranslation('notifications');

  const { getContract, getTransactionReceipt } = useWeb3() || {};

  // Contract
  const [contract, setContract] = React.useState();
  const updateContract = async () => {
    if (!contractAddress) {
      if (contract) {
        setContract();
      }
      return;
    }
    setContract(await getContract(abi, contractAddress));
  };
  React.useEffect(() => {
    updateContract();
  }, [chainId]);

  // Loadings
  const [loadings, setLoadings] = React.useState({});
  const setLoading = (state, key) => {
    setLoadings({
      ...loadings,
      [key]: !!state,
    });
  };
  const getLoading = (key) => !!loadings[key];
  const onTransaction = async (method, params) => {
    try {
      setLoading(true, method);
      const txHash = await transaction(contract, method, params);
      console.log(`[${method}]`, { txHash, params });
      const receipt = await getTransactionReceipt(txHash);
      console.log(`[${method}] Receipt`, receipt);
      toaster.success(t('Transaction success'));
    } catch (error) {
      console.error('[onTransaction]', method, params, error);
      toaster.logError(error);
    }
    setLoading(false, method);
  };

  // Values
  const [values, setValues] = React.useState({});
  const setValue = (value, key) => {
    setValues({
      ...values,
      [key]: value,
    });
  };
  const getValue = (key) => values[key] || '';

  const [tabId, setTabId] = React.useState('roles');

  return (
    <ContractContext.Provider
      className={styles.contract}
      value={{
        contract,
        onTransaction,
        getLoading,
        getValue,
        setValue,
      }}
    >
      <h3>{title}</h3>
      {!!contract && (
        <Tabs
          id={contractAddress}
          fill
          className={styles.contractTabs}
          onChange={setTabId}
          vertical
          selectedTabId={tabId}
        >
          {/*{!!roles.length && (*/}
          {/*  <Tab*/}
          {/*    id={'roles'}*/}
          {/*    title='Roles'*/}
          {/*    panel={<Roles contract={contract} rolesList={roles} />}*/}
          {/*  />*/}
          {/*)}*/}
          {/*{!!Object.keys(methods).length && (*/}
          {/*  <Tab*/}
          {/*    id={'write'}*/}
          {/*    title='Write'*/}
          {/*    panel={<Write methods={methods}>{writeChildren}</Write>}*/}
          {/*  />*/}
          {/*)}*/}
          {!!children && <Tab id={'children'} title='Other' panel={children} />}
        </Tabs>
      )}
    </ContractContext.Provider>
  );
}

export default Contract;
