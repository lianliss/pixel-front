import React from 'react';
import styles from './Roles.module.scss';
import toaster from 'services/toaster';
import { Button, ControlGroup, FormGroup, Icon, InputGroup, Spinner } from '@blueprintjs/core';
import { isAddress } from 'ethers6';
import { useWeb3 } from 'services/web3Provider';
// import Addresses from "lib/Admin/components/Addresses/Addresses";
import { useAccount } from '@chain/hooks/useAccount.ts';

function Roles({ contract, rolesList = [] }) {
  const account = useAccount();
  const { transaction } = account;

  const { getTransactionReceipt } = useWeb3() || {};

  const [roles, setRoles] = React.useState({});
  const [rolesLoading, setRolesLoading] = React.useState(true);
  const updateRoles = async () => {
    if (!contract) {
      if (roles) {
        setRoles({});
      }
      return;
    }
    try {
      setRolesLoading(true);
      const data = await Promise.all(rolesList.map((role) => contract.methods[role]().call()));
      const roles = {};
      data.map((roleHash, index) => {
        roles[rolesList[index]] = roleHash;
      });
      setRoles(roles);
    } catch (error) {
      console.error('[updateRoles]', error);
      toaster.logError(error);
    }
    setRolesLoading(false);
  };
  React.useEffect(() => {
    updateRoles();
  }, [contract]);

  const [values, setValues] = React.useState({});
  const setValue = (value, key) => {
    setValues({
      ...values,
      [key]: value,
    });
  };
  const getValue = (key) => values[key] || '';
  const [loadings, setLoadings] = React.useState({});
  const setLoading = (value, key) => {
    setLoadings({
      ...loadings,
      [key]: !!value,
    });
  };
  const getLoading = (key) => !!loadings[key];
  const onGrantRole = async (address, role) => {
    try {
      setLoading(true, role);
      const roleHash = roles[role];
      const params = [roleHash, address];
      const txHash = await transaction(contract, 'grantRole', params);
      console.log('[grantRole]', { txHash, params });
      const receipt = await getTransactionReceipt(txHash);
      console.log('[grantRole] receipt', receipt);
      toaster.success(`Role ${role} granted to ${address}`);
    } catch (error) {
      toaster.logError(error);
    }
    setLoading(false, role);
  };

  return (
    <div className={styles.roles}>
      {/*<Addresses />*/}
      {Object.keys(roles).map((role, index) => {
        const address = getValue(role);
        return (
          <FormGroup key={index} label={role}>
            <ControlGroup fill={true}>
              <InputGroup
                placeholder={'0xA9...'}
                leftIcon={'lock'}
                onValueChange={(value) => setValue(value, role)}
                value={address}
              />
              <Button
                loading={getLoading(role)}
                intent={'primary'}
                onClick={() => onGrantRole(address, role)}
                disabled={!isAddress(address)}
              >
                Grant Role
              </Button>
            </ControlGroup>
          </FormGroup>
        );
      })}
      {!!rolesLoading && <Spinner />}
    </div>
  );
}

export default Roles;
