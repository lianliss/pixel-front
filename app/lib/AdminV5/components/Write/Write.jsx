import React from 'react';
import styles from './Write.module.scss';
import { Button, ControlGroup, FormGroup, InputGroup, Tag } from '@blueprintjs/core';
import wei from 'utils/wei';

function Write({ methods, children }) {
  const { contract, onTransaction, getLoading, getValue, setValue } = {}; // React.useContext(ContractContext);

  const getParamFields = (param) => {
    const type = param?.type || 'number';
    const parameter = {
      type,
      decimals: param?.decimals,
    };
    if (type === 'array') {
      parameter.field = getParamFields(param.field);
      if (param?.length) {
        parameter.length = param.length;
      }
    }
    return parameter;
  };

  const writeMethods = React.useMemo(() => {
    if (!contract) return [];
    const abi = contract.jsonInterface || contract._jsonInterface;
    return Object.keys(methods)
      .map((method) => {
        const params = methods[method];
        if (!abi) return;
        const json = abi.find((item) => item.type === 'function' && item.name === method);
        if (!json) return;
        return {
          name: method,
          params: json.inputs.map((input, index) => ({
            ...getParamFields(params[index]),
            jsonType: input.type,
            name: input.name || `${method}${index}`,
          })),
        };
      })
      .filter((m) => !!m);
  }, [contract, methods]);

  return (
    <div className={styles.write}>
      {children}
      {writeMethods.map((method, index) => {
        const methodName = method.name;
        const onSubmit = () =>
          onTransaction(
            methodName,
            method.params.map((param) => {
              const { type, field, decimals, name, length } = param;
              try {
                if (type === 'number') {
                  return wei.to(getValue(name), decimals);
                } else if (type === 'array') {
                  const { type, decimals } = field;
                  const array = [];
                  for (let i = 0; i < length; i++) {
                    const fieldName = `${name}-${i}`;
                    const value =
                      type === 'number'
                        ? wei.to(getValue(fieldName), decimals)
                        : getValue(fieldName);
                    array.push(value);
                  }
                  return array;
                } else {
                  return getValue(name);
                }
              } catch (error) {
                console.error(
                  '[Write][onSubmit]',
                  methodName,
                  {
                    param,
                    value: getValue(name),
                  },
                  error
                );
              }
            })
          );
        return (
          <FormGroup label={methodName} key={index}>
            <ControlGroup fill={true} vertical={method.params.length > 2}>
              {method.params.map((param, index) => {
                const { name, type, decimals, jsonType, length, field } = param;
                if (type === 'array') {
                  if (length) {
                    const array = [];
                    for (let i = 0; i < length; i++) {
                      array.push(field);
                    }
                    return array.map((field, i) => {
                      const { type, decimals } = field;
                      const fieldName = `${name}-${i}`;
                      return (
                        <InputGroup
                          placeholder={fieldName || type}
                          key={`${index}-${i}`}
                          rightElement={
                            <>
                              {!!decimals && <Tag intent={'primary'}>Decimals: {decimals}</Tag>}
                              <Tag intent={'warning'}>{jsonType}</Tag>
                            </>
                          }
                          onValueChange={(value) => setValue(value, fieldName)}
                          value={getValue(fieldName)}
                        />
                      );
                    });
                  }
                }
                return (
                  <InputGroup
                    placeholder={name || type}
                    key={index}
                    rightElement={
                      <>
                        {!!decimals && <Tag intent={'primary'}>Decimals: {decimals}</Tag>}
                        <Tag intent={'warning'}>{jsonType}</Tag>
                      </>
                    }
                    onValueChange={(value) => setValue(value, name)}
                    value={getValue(name)}
                  />
                );
              })}
              <Button loading={getLoading(methodName)} intent={'primary'} onClick={onSubmit}>
                Send
              </Button>
            </ControlGroup>
          </FormGroup>
        );
      })}
    </div>
  );
}

export default Write;
