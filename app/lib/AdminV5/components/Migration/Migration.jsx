import React from 'react';
import styles from './Migration.module.scss';
import { Tab, Tabs } from '@blueprintjs/core';
import Dependencies from 'lib/AdminV5/components/Migration/components/Dependencies/Dependencies';
import NFTTypes from 'lib/AdminV5/components/Migration/components/NFTTypes/NFTTypes';

function Migration() {
  const [tabId, setTabId] = React.useState('dependencies');

  return (
    <div className={styles.block}>
      <h3>Migration</h3>
      <Tabs
        id={'migration'}
        fill
        className={styles.blockTabs}
        onChange={setTabId}
        vertical
        selectedTabId={tabId}
      >
        <Tab id='dependencies' title='Dependencies' panel={<Dependencies />} />
        <Tab id='nftTypes' title='NFT Types' panel={<NFTTypes />} />
      </Tabs>
    </div>
  );
}

export default Migration;
