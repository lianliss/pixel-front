import React from 'react';
import styles from './Dependencies.module.scss';
import { useWeb3 } from 'services/web3Provider';
import toaster from 'services/toaster';
import { AdminV5Context } from 'lib/AdminV5/pages/old/AdminV5';
import Button from 'ui/Button';
import ROLES from 'lib/AdminV5/pages/old/roles';
import { useAccount } from '@chain/hooks/useAccount.ts';

let currentStatus = [];

function Dependencies() {
  const account = useAccount();
  const { transaction } = account;

  const contracts = null;
  // const {contracts} = React.useContext(AdminV5Context);
  const { getTransactionReceipt } = useWeb3() || {};
  const [status, setStatus] = React.useState([]);
  const [isProcess, setIsProcess] = React.useState(false);

  const pushStatus = (title, value) => {
    currentStatus.push({ title, value });
    setStatus(currentStatus);
  };

  const sendTransaction = async (contract, method, params = [], value) => {
    const tx = await transaction(contract, method, params, value);
    return await getTransactionReceipt(tx);
  };

  const grantRole = async (contractKey, roleKey, receiverKey) => {
    try {
      const params = [ROLES[roleKey], contracts[receiverKey]._address];
      console.log('[grantRole]', {
        contractKey,
        roleKey,
        receiverKey,
        params,
      });
      const receipt = await sendTransaction(contracts[contractKey], 'grantRole', params);
      pushStatus(`${contractKey} [${roleKey}] > ${receiverKey}`, 'OK');
      return receipt;
    } catch (error) {
      toaster.error(`${contractKey} [${roleKey}] > ${receiverKey}`);
      toaster.logError(error);
      throw error;
    }
  };

  const onMakeDependencies = async () => {
    try {
      setIsProcess(true);
      setStatus([]);
      currentStatus = [];
      await grantRole('PXLsCoreV5', 'PROXY_ROLE', 'PXLsCoreV5Settings');
      await grantRole('PXLsCoreV5', 'PROXY_ROLE', 'PXLsCoreV5Migration');
      await grantRole('PXLsCoreV5', 'PROXY_ROLE', 'PXLsFreeChestV5');
      await grantRole('PXLsCoreV5', 'BURNER_ROLE', 'PXLsSlotsV5');
      await grantRole('PXLsCoreV5', 'MULTIPLIER_ROLE', 'PXLsSlotsV5');
      await grantRole('PXLsCoreV5', 'MINTER_ROLE', 'PXLsChestV5');
      await grantRole('PXLsNFTV5', 'PROXY_ROLE', 'PXLsNFTV5Editor');
      await grantRole('PXLsNFTV5', 'PROXY_ROLE', 'PXLsNFTV5Migration');
      await grantRole('PXLsNFTV5', 'MINTER_ROLE', 'PXLsNFTV5Migration');
      await grantRole('PXLsNFTV5', 'OPERATOR_ROLE', 'PXLsSlotsV5');
      await grantRole('PXLsNFTV5', 'MINTER_ROLE', 'PXLsSlotsV5Migration');
      await grantRole('PXLsNFTV5', 'MINTER_ROLE', 'PXLsChestV5');
      await grantRole('PXLsNFTV5Editor', 'EDITOR_ROLE', 'PXLsNFTV5Migration');
      await grantRole('PXLsChestV5', 'MULTIPLIER_ROLE', 'PXLsSlotsV5');
      await grantRole('PXLsSlotsV5', 'PROXY_ROLE', 'PXLsSlotsV5Settings');
      await grantRole('PXLsSlotsV5', 'PROXY_ROLE', 'PXLsSlotsV5Migration');
      await grantRole('PXLsSlotsV5Migration', 'BACKEND_ROLE', 'PXLsCoreV5Migration');
      await grantRole('PixelNFT', 'BURNER_ROLE', 'PXLsNFTV5Migration');
      await grantRole('PixelNFT', 'BURNER_ROLE', 'PXLsSlotsV5Migration');
      await grantRole('PXLsCoreV5', 'MINTER_ROLE', 'Quest');
      await grantRole('PXLsChestV5', 'MINTER_ROLE', 'Quest');
      await sendTransaction(contracts.PXLsSlotsV5Settings, 'setCoreAddress', [
        contracts.PXLsCoreV5._address,
      ]);
      await sendTransaction(contracts.PXLsSlotsV5Settings, 'setNFTAddress', [
        contracts.PXLsNFTV5._address,
      ]);
      await sendTransaction(contracts.PXLsSlotsV5Settings, 'setChestAddress', [
        contracts.PXLsChestV5._address,
      ]);
    } catch (error) {
      console.log('[onMakeDependencies]', error);
    }
    setIsProcess(false);
  };

  return (
    <div className={styles.block}>
      <p className={styles.status}>
        {status.map((row, index) => (
          <div className={styles.statusRow} key={index}>
            <div>{row.title}</div>
            <div>{row.value}</div>
          </div>
        ))}
      </p>
      <Button loading={isProcess} disabled={isProcess} onClick={onMakeDependencies}>
        Make Dependencies
      </Button>
    </div>
  );
}

export default Dependencies;
