import React from 'react';
import styles from './NFTTypes.module.scss';
import toaster from 'services/toaster';
import Button from 'ui/Button';
import { useAccount } from '@chain/hooks/useAccount.ts';

function NFTTypes() {
  const account = useAccount();
  const { transaction } = account;

  const contract = null;
  const [isProcess, setIsProcess] = React.useState(false);
  const [isCollections, setIsCollections] = React.useState(false);

  const [typesLength, setTypesLength] = React.useState(0);
  const [oldTypesLength, setOldTypesLength] = React.useState(0);

  const updateTypes = async () => {
    try {
      const data = await Promise.all([
        contracts.PXLsNFTV5.methods.getTypesLength().call(),
        contracts.PixelNFT.methods.getTypesLength().call(),
      ]);
      setTypesLength(Number(data[0]));
      setOldTypesLength(Number(data[1]));
    } catch (error) {
      toaster.logError(error);
    }
  };
  React.useEffect(() => {
    updateTypes();
  }, [contracts]);

  const onMigrateCollections = async () => {
    try {
      setIsCollections(true);
      const tx = await transaction(contracts.PXLsNFTV5Migration, 'migrateCollections', []);
      // await getTransactionReceipt(tx);
    } catch (error) {
      toaster.logError(error);
    }
    setIsCollections(false);
  };

  const onMigrateTypes = async () => {
    try {
      setIsProcess(true);
      const limit = 10;
      const left = oldTypesLength - typesLength || limit;
      //const length = limit < left ? limit : left;
      for (let i = 0; i < Math.floor(left / limit); i++) {
        const tx = await transaction(contracts.PXLsNFTV5Migration, 'migrateTypes', [limit]);
        // await getTransactionReceipt(tx);
      }
      const tail = left % limit;
      if (tail) {
        const tx = await transaction(contracts.PXLsNFTV5Migration, 'migrateTypes', [tail]);
        // await getTransactionReceipt(tx);
      }
      await updateTypes();
    } catch (error) {
      toaster.logError(error);
    }
    setIsProcess(false);
  };

  return (
    <div className={styles.types}>
      <p>
        Types: {typesLength} / {oldTypesLength}
      </p>
      <div>
        <Button
          loading={isCollections}
          disabled={isProcess || isCollections}
          onClick={onMigrateCollections}
        >
          Migrate Collections
        </Button>
        <Button loading={isProcess} disabled={isProcess || isCollections} onClick={onMigrateTypes}>
          Migrate Types
        </Button>
      </div>
    </div>
  );
}

export default NFTTypes;
