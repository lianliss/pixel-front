import { INftCollectionDto } from 'lib/NFT/api-server/nft.types.ts';

export function convertNftCollection(name: string, index: number): INftCollectionDto {
  return {
    id: name,
    name,
    index: index.toString(),
    createdAt: new Date().getTime(),
  };
}
