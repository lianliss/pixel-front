import React from 'react';
import styles from './AdminV5.module.scss';
import { Tab, Tabs, TabsExpander } from '@blueprintjs/core';
import Migration from 'lib/AdminV5/components/Migration/Migration';
import { CONTRACT_ADDRESSES } from 'services/multichain/contracts';
import { useAccount } from '@chain/hooks/useAccount';

export const AdminV5Context = React.createContext();

function AdminV5() {
  const account = useAccount();
  const [tabId, setTabId] = React.useState('migration');

  const V5 = (CONTRACT_ADDRESSES[account.chainId] || {})?.v5;
  // const V5 = network.contractAddresses.v5;

  return (
    <div className={styles.admin}>
      <h2>Administration Editor (v1)</h2>
      {
        <Tabs id='Admin' onChange={setTabId} renderActiveTabPanelOnly selectedTabId={tabId}>
          {/*<Tab id="coreMath"*/}
          {/*     title="Core Math"*/}
          {/*     panel={<Contract roles={[]}*/}
          {/*                      methods={CORE_MATH_METHODS}*/}
          {/*                      title={'Core Math Editor'}*/}
          {/*                      contractAddress={V5.coreMath}*/}
          {/*                      abi={require("const/ABI/V5/coreMath")}/>}/>*/}
          {/*<Tab id="core"*/}
          {/*     title="Core"*/}
          {/*     panel={<Contract roles={[CORE_ROLES]}*/}
          {/*                      methods={CORE_METHODS}*/}
          {/*                      title={'Core Editor'}*/}
          {/*                      contractAddress={V5.core}*/}
          {/*                      abi={require("const/ABI/V5/core")}/>}/>*/}
          <TabsExpander />
          <Tab id='migration' title='Migration' panel={<Migration />} />
        </Tabs>
      }
    </div>
  );
}

export default AdminV5;
