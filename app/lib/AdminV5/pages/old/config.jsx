const DEFAULT_DECIMALS = 18;
const PERCENT_DECIMALS = 4;
const fieldWei = (decimals = DEFAULT_DECIMALS) => ({
  decimals,
});
const fieldPercent = () => fieldWei(PERCENT_DECIMALS);
const fieldNumber = () => fieldWei(0);
const fieldAddress = () => ({
  type: 'address',
});
const fieldArray = (field, length) => ({
  type: 'array',
  field,
  length, // Important value for this type
});

export const CORE_ROLES = [
  'BURNER_ROLE',
  'CLAIMER_ROLE',
  'GRANTER_ROLE',
  'MINTER_ROLE',
  'MULTIPLIER_ROLE',
];
export const CORE_METHODS = {
  setBaseSize: { 0: fieldWei() },
  setBaseSpeed: { 0: fieldWei() },
  setBurnRate: { 0: fieldPercent() },
  setDiffuculty: { 0: fieldNumber() },
  setReceiver: { 0: fieldAddress() },
  setUSDT: { 0: fieldAddress() },
  setParentsPercents: { 0: fieldArray(fieldPercent(), 2) },
  // 'burnTotalSupply': {
  //     0: fieldWei(),
  // }
};

export const CORE_MATH_METHODS = {
  setBaseSize: { 0: fieldWei() },
  setBaseSpeed: { 0: fieldWei() },
  setBurnRate: { 0: fieldPercent() },
  setDiffuculty: { 0: fieldPercent() },
  setHalvingRatio: { 0: fieldPercent() },
  setReceiver: { 0: fieldAddress() },
  setUSDT: { 0: fieldAddress() },
  setParentsPercents: { 0: fieldArray(fieldPercent(), 2) },
  // 'burnTotalSupply': {
  //     0: fieldWei(),
  // }
};

export const NFT_ROLES = ['DEFAULT_ADMIN_ROLE', 'BURNER_ROLE', 'MINTER_ROLE', 'OPERATOR_ROLE'];
export const CHEST_ROLES = ['MINTER_ROLE', 'EDITOR_ROLE', 'MULTIPLIER_ROLE'];
export const CHEST_METHODS = {
  setCoreAddress: { 0: fieldAddress() },
  setNFTAddress: { 0: fieldAddress() },
  setReceiver: { 0: fieldAddress() },
  setUSDT: { 0: fieldAddress() },
  setUseUSDT: { 0: fieldNumber() },
  createMarketItem: {
    0: fieldNumber(),
    1: fieldNumber(),
    2: fieldWei(),
    3: fieldWei(),
  },
  updateMarketItem: {
    0: fieldNumber(),
    1: fieldWei(),
    2: fieldWei(),
  },
  setMarketItemCount: {
    0: fieldNumber(),
    1: fieldNumber(),
  },
  setMaxPXLsBonus: {
    0: fieldWei(),
  },
};
export const SLOTS_ROLES = ['EDITOR_ROLE'];

export const SLOTS_METHODS = {
  createSlot: {
    0: fieldAddress(),
    1: fieldWei(),
    2: fieldWei(),
    3: fieldNumber(),
  },
  updateSlot: {
    0: fieldNumber(),
    1: fieldAddress(),
    2: fieldWei(),
    3: fieldWei(),
    4: fieldNumber(),
  },
  setCoreAddress: { 0: fieldAddress() },
  setNFTAddress: { 0: fieldAddress() },
  setChestAddress: { 0: fieldAddress() },
  setReceiver: { 0: fieldAddress() },
  setUSDT: { 0: fieldAddress() },
  setUseUSDT: { 0: fieldNumber() },
  setParameterReceiver: {
    0: fieldNumber(),
    1: fieldAddress(),
  },
  setRegisteredParameters: { 0: fieldNumber() },
};

export const FREE_CHEST_ROLES = ['SETTER_ROLE'];

export const FREE_CHEST_METHODS = {
  setCoreAddress: { 0: fieldAddress() },
  setChestAddress: { 0: fieldAddress() },
  setWeeklyChest: { 0: fieldNumber() },
};

export const MASS_CHEST_ROLES = ['EDITOR_ROLE'];

export const MASS_CHEST_METHODS = {
  setChest: { 0: fieldAddress() },
};

export const NFT_IMPORT_ROLES = ['EDITOR_ROLE'];

export const NFT_IMPORT_METHODS = {
  setDestinationContract: { 0: fieldAddress() },
};

export const QUEST_ROLES = ['DEFAULT_ADMIN_ROLE', 'SETTER_ROLE'];

export const QUEST_METHODS = {
  setCoreAddress: { 0: fieldAddress() },
  setChestAddress: { 0: fieldAddress() },
  updateQuest: {
    0: fieldNumber(),
    1: fieldNumber(),
    2: fieldWei(),
    3: fieldNumber(),
  },
  setQuestComplete: {
    0: fieldNumber(),
    1: fieldNumber(),
  },
};

export const DRILL_ROLES = ['SETTER_ROLE'];

export const DRILL_METHODS = {
  setCoreAddress: { 0: fieldAddress() },
  setNFTAddress: { 0: fieldAddress() },
  getLevelItemType: {
    0: fieldNumber(),
  },
  sendDrill: {
    0: fieldNumber(),
    1: fieldNumber(),
  },
};
