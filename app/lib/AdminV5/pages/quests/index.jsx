import React from 'react';
import QuestsAdmin from './quests-admin';
import { AdminLayout } from 'lib/AdminV5/components/AdminLayout/AdminLayout.tsx';

function EditorWrap() {
  return (
    <AdminLayout>
      <QuestsAdmin />
    </AdminLayout>
  );
}

export default EditorWrap;
