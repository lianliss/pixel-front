import React from 'react';
import Quests from 'lib/AdminV5/pages/nft-editor/components/Quests/Quests';

function QuestsAdmin() {
  return <Quests />;
}

export default QuestsAdmin;
