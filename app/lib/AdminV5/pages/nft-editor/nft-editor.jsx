import React from 'react';
import Collections from 'lib/AdminV5/pages/nft-editor/components/Collections/Collections';
import { AdminLayout } from 'lib/AdminV5/components/AdminLayout/AdminLayout.tsx';

function NftEditor() {
  return (
    <AdminLayout>
      <Collections />
    </AdminLayout>
  );
}

export default NftEditor;
