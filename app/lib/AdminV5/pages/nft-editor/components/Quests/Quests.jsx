import React from 'react';
import styles from './Quests.module.scss';
import ABIQuest from 'const/ABI/PXL/Quest';
import { MINING_TOKENS } from 'services/multichain/initialTokens';
import { wei } from 'utils/index';
import { getChestNames } from 'const/chests';
import getFinePrice from 'utils/getFinePrice';
import {
  Button as BlueButton,
  FormGroup,
  InputGroup,
  MenuItem,
  NumericInput,
  Tag,
} from '@blueprintjs/core';
import { Select } from '@blueprintjs/select';
import get from 'lodash/get';
import toaster from 'services/toaster';
import Button from 'ui/Button';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { useOldChestsList } from 'lib/Chests/hooks/deprecated/useOldChestsList.ts';
import { CONTRACT_ADDRESSES } from 'services/multichain/contracts';

function Quests() {
  const account = useAccount();
  const { chainId } = account;

  const chestsApi = useOldChestsList();
  const chests = chestsApi.chests.data;

  const contractAddress = CONTRACT_ADDRESSES[chainId]?.pixel?.quest;

  const writeQuest = useWriteContract({
    abi: ABIQuest,
    address: contractAddress,
  });
  const questsLengthData = useReadContract({
    initData: 0,
    abi: ABIQuest,
    address: contractAddress,
    functionName: 'getQuestsLength',
    skip: !contractAddress || !account.isConnected,
    select: (res) => Number(res),
  });
  const questsData = useReadContract({
    initData: [],
    abi: ABIQuest,
    address: contractAddress,
    functionName: 'getUserQuests',
    args: [0, 0, questsLengthData.data],
    skip: !contractAddress || !account.isConnected || !questsLengthData.data,
    select: (res) => {
      return res.map((q) => ({
        questId: Number(q.questId),
        PXLs: wei.from(q.PXLs, decimals),
        chestId: Number(q.chestId),
        maxIterations: Number(q.maxIterations),
      }));
    },
  });
  const quests = questsData.data;

  const hasQuests = !!contractAddress;
  const miningToken = MINING_TOKENS[chainId];
  const miningSymbol = miningToken?.symbol || 'PXLs';
  const decimals = miningToken?.decimals || 18;

  const chestNames = React.useMemo(() => {
    return {
      ...getChestNames(chainId),
      0: 'No Chest',
    };
  }, [chainId]);
  const getChestName = (chestId) => chestNames[chestId] || 'Chest';

  const [questId, setQuestId] = React.useState();
  const [tokens, setTokens] = React.useState();
  const [chestId, setChestId] = React.useState(0);

  const renderItem = (chest, { handleClick, handleFocus, modifiers, query }) => {
    const chestName = getChestName(chest.chestId);
    let itemsCount = 0;
    get(chest, 'items[0]', []).map((items) => (itemsCount += items.length));
    return (
      <MenuItem
        onClick={handleClick}
        onFocus={handleFocus}
        key={chest.chestId}
        active={chest.chestId === chestId}
        label={itemsCount}
        text={
          <>
            {chest.chestId}. {chest.title}
          </>
        }
        roleStructure='listoption'
      />
    );
  };
  const itemPredicate = (query, chest, _index, exactMatch) => {
    const normalizedTitle = getChestName(chest.chestId).toLowerCase();
    const normalizedQuery = query.toLowerCase();

    if (exactMatch) {
      return normalizedTitle === normalizedQuery;
    } else {
      return `${chest.chestId}. ${normalizedTitle}`.indexOf(normalizedQuery) >= 0;
    }
  };

  const [isProcess, setIsProcess] = React.useState(false);
  const onSetChest = async (e) => {
    e.preventDefault();
    if (isProcess || !questId) return;

    try {
      setIsProcess(true);

      await writeQuest.writeContractAsync({
        functionName: 'updateQuest',
        args: [questId, Number(chestId) || 0, wei.to(tokens || 0, decimals), 1],
      });

      // const txHash = await transaction(contract, 'updateQuest', [
      //   questId,
      //   Number(chestId) || 0,
      //   wei.to(tokens || 0, decimals),
      //   1,
      // ]);
      // const receipt = await getTransactionReceipt(txHash);

      await questsLengthData.refetch();
      await questsData.refetch();

      toaster.success(`Quest ${questId} updated`);
    } catch (error) {
      toaster.logError(error);
    }
    setIsProcess(false);
  };

  return (
    <div className={styles.quests}>
      <h2>Quests</h2>
      <div className={styles.questsColumns}>
        <table className={[styles.questsList, 'bp5-html-table'].join(' ')}>
          <thead>
            <tr>
              <th>Quest ID</th>
              <th>Tokens</th>
              <th>Chest</th>
            </tr>
          </thead>
          <tbody>
            {quests.map((quest) => {
              const chestName = chestNames[quest.chestId] || 'Chest';
              const isActive = Number(questId) === quest.questId;
              const rowClasses = [styles.questsListItem];
              if (isActive) {
                rowClasses.push(styles.active);
              }
              return (
                <tr className={rowClasses.join(' ')} key={quest.questId}>
                  <td className={styles.questsListId}>{quest.questId}.</td>
                  <td
                    className={[styles.questsListPixels, !quest.PXLs ? styles.disabled : ''].join(
                      ' '
                    )}
                  >
                    {getFinePrice(quest.PXLs)} {miningSymbol}
                  </td>
                  <td className={styles.questsListChest}>
                    <Tag intent={!!quest.chestId ? 'primary' : undefined}>
                      {quest.chestId}. {chests[quest.chestId]?.title || ''}
                    </Tag>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <form className={styles.questsForm} onSubmit={onSetChest}>
          <FormGroup label={'QuestID'}>
            <InputGroup placeholder={'123...'} onValueChange={setQuestId} value={questId} />
          </FormGroup>
          <FormGroup label={'Tokens'}>
            <NumericInput
              placeholder={'0.00'}
              majorStepSize={1}
              stepSize={0.1}
              minorStepSize={0.05}
              onValueChange={setTokens}
              rightElement={<Tag>{miningSymbol}</Tag>}
              value={tokens}
            />
          </FormGroup>
          <FormGroup label={'Chest'}>
            <Select
              items={chests || []}
              onItemSelect={(item) => {
                setChestId(item.chestId);
              }}
              noResults={<MenuItem disabled={true} text='No chests.' roleStructure='listoption' />}
              itemPredicate={itemPredicate}
              itemRenderer={renderItem}
            >
              <BlueButton rightIcon='double-caret-vertical'>
                {chestId ? (
                  <>
                    {chestId}. {chests[chestId].title}
                  </>
                ) : (
                  'No Chest'
                )}
              </BlueButton>
            </Select>
          </FormGroup>
          <FormGroup>
            <Button onClick={onSetChest} disabled={isProcess || !questId} loading={isProcess}>
              Set Chest
            </Button>
          </FormGroup>
        </form>
      </div>
    </div>
  );
}

export default Quests;
