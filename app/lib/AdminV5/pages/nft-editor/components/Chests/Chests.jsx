import React from 'react';
import styles from './Chests.module.scss';
import { Tab, Tabs } from '@blueprintjs/core';
import get from 'lodash/get';
import { getChestNames } from 'const/chests';
import AddChest from 'lib/AdminV5/pages/nft-editor/components/AddChest/AddChest';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useOldChestsList } from 'lib/Chests/hooks/deprecated/useOldChestsList.ts';

function Chests() {
  const chestsApi = useOldChestsList();
  const account = useAccount();

  const chests = chestsApi.chests.data;
  const { chainId } = account;

  const [chestId, setChestId] = React.useState(0);

  const chestNames = React.useMemo(() => {
    return getChestNames(chainId);
  }, [chainId]);

  return (
    <div className={styles.chests}>
      <Tabs
        id='chests'
        onChange={setChestId}
        renderActiveTabPanelOnly
        vertical
        selectedTabId={chestId}
      >
        {chests.map((chest, chestIndex) => {
          let itemsCount = 0;
          get(chest, 'items[0]', []).map((items) => (itemsCount += items.length));
          return (
            <Tab
              id={chestIndex}
              key={chestIndex}
              title={`${chestIndex}. ${chest.title || 'Chest'}`}
              tagContent={itemsCount || undefined}
              panel={
                <AddChest setChestId={setChestId} chest={chest} refreshChests={chestsApi.reload} />
              }
            />
          );
        })}
        <Tab
          id={-1}
          title={'Add Chest'}
          tagContent={'+'}
          panel={<AddChest setChestId={setChestId} refreshChests={chestsApi.reload} />}
        />
      </Tabs>
    </div>
  );
}

export default Chests;
