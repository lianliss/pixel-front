import React, { useEffect } from 'react';
import styles from './Collections.module.scss';
import { ControlGroup, InputGroup, Tab, Tabs, TabsExpander } from '@blueprintjs/core';
import Types from 'lib/AdminV5/pages/nft-editor/components/Types/Types';
import Button from 'app/lib/ui/deprecated/ButtonDeprecated';
import toaster from 'services/toaster';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import NFT_ABI from 'lib/NFT/api-contract/nft.abi.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useNftTypes } from 'lib/NFT/hooks/useNftTypes.ts';
import { CONTRACT_ADDRESSES } from 'services/multichain/contracts';
import { useNftCollections } from 'lib/AdminV5/hooks/useNftCollections';
import { useNftStats } from 'lib/Dashboard/hooks/useNftStats';
// import NftTypeGrid from "lib/AdminV5/components/NftTypeGrid/NftTypeGrid";

const NFT_EDITOR_ABI = require('const/ABI/V5/nftEditor');

function Collections() {
  const account = useAccount();
  const nftEditorAddress = CONTRACT_ADDRESSES[account.chainId]?.v5?.nftEditor;

  const nftAddress = NFT_CONTRACT_ID[account.chainId];
  const writeCollection = useWriteContract({
    abi: NFT_EDITOR_ABI,
    address: nftEditorAddress,
  });
  const collectionsState = useNftCollections({
    contractId: nftAddress?.toLowerCase(),
    fromChain: false,
  });

  const collections = collectionsState.state.rows;
  const itemTypesData = useNftTypes();
  const itemTypes = itemTypesData.data;

  const [isAdding, setIsAdding] = React.useState(false);
  const [collectionId, setCollectionId] = React.useState(null);
  const [newCollectionName, setNewCollectionName] = React.useState('');
  const counts = React.useMemo(() => {
    const counts = collections.map((c) => 0);
    itemTypes.map((t) => counts[t.collectionId]++);
    return counts;
  }, [itemTypes.length, collections.length]);

  const addCollection = async () => {
    try {
      if (!newCollectionName || !newCollectionName.length) throw new Error('Wrong collection name');
      setIsAdding(true);

      await writeCollection.writeContractAsync({
        functionName: 'createCollection',
        args: [newCollectionName],
      });
      // const result = await transactions.createCollection(newCollectionName);
      setNewCollectionName('');
      await itemTypesData.refetch();
    } catch (error) {
      console.error('[addCollection]', error);
      toaster.logError(error, '[addCollection]');
    }
    setIsAdding(false);
  };

  useEffect(() => {
    if (collections.length) {
      setCollectionId(collections[0].id);
    }
  }, [collections]);

  return (
    <div className={styles.collections}>
      <Tabs
        id='collections'
        onChange={setCollectionId}
        renderActiveTabPanelOnly
        vertical
        selectedTabId={collectionId}
      >
        {collections.map((collection) => {
          return (
            <Tab
              id={collection.id}
              key={collection.index}
              title={collection.name}
              tagContent={counts[collection.index] || null}
              panel={
                <Types
                  collectionId={collection.id}
                  collectionIndex={collection.index}
                  collectionName={collection.name}
                />
              }
              // panel={<NftTypeGrid collectionId={collectionIndex} />}
            />
          );
        })}
        <ControlGroup fill={true} vertical={false}>
          <InputGroup
            placeholder='Add collection...'
            value={newCollectionName}
            intent={newCollectionName?.length ? 'success' : ''}
            onValueChange={setNewCollectionName}
          />
          <Button
            icon='small-plus'
            loading={isAdding}
            disabled={isAdding}
            onClick={addCollection}
          />
        </ControlGroup>
      </Tabs>
    </div>
  );
}

export default Collections;
