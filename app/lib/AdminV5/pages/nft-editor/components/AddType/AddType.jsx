import React from 'react';
import styles from './AddType.module.scss';
import {
  Button,
  Checkbox,
  FormGroup,
  HTMLSelect,
  InputGroup,
  Radio,
  RadioGroup,
  Switch,
} from '@blueprintjs/core';
import flatten from 'lodash-es/flatten';
import toaster from 'services/toaster';
import uniq from 'lodash/uniq';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import NFT_ABI from 'lib/NFT/api-contract/nft.abi.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useNftTypes } from 'lib/NFT/hooks/useNftTypes.ts';
import { Chain, NETWORKS_DATA } from 'services/multichain/chains';
import { CONTRACT_ADDRESSES } from 'services/multichain/contracts';
import { NftItemSlot } from 'lib/NFT/api-server/nft.types';
import { getNftSlotLabel } from 'lib/NFT/utils/getNftSlotLabel';

const NFT_EDITOR_ABI = require('const/ABI/V5/nftEditor');

const SLOTS_KEY = 'nft-add-type-slots';
const RARITY_KEY = 'nft-add-type-rarity';

const EMPTY_TYPE = {
  name: '',
  tokenURI: '',
  slots: [],
  rarity: 1,
  soulbound: false,
  disposable: false,
};

const RARITY = ['Uniq', 'Common', 'Uncommon', 'Rare', 'Epic', 'Legendary'];

const SLOTS = {
  pocket: {
    values: [0, 1, 3, 5],
    title: 'Pockets',
  },
  backpack: {
    values: [2],
    title: 'Backpack',
  },
  belt: {
    values: [4],
    title: 'Belt',
  },
  artifact: {
    values: [6],
    title: 'Artifact',
  },
  drill: {
    values: [7],
    title: 'Drill',
  },
  back: {
    values: [8],
    title: 'Back',
  },
  loyalty: {
    values: [9],
    title: 'Loyalty Card',
  },
  claimer: {
    values: [10],
    title: 'Smart Claimer',
  },
  recipe: {
    values: [11],
    title: 'Recipe',
  },
  ring: {
    values: [NftItemSlot.Ring],
    title: getNftSlotLabel(NftItemSlot.Ring),
  },
  drone: {
    values: [NftItemSlot.Drone],
    title: getNftSlotLabel(NftItemSlot.Drone),
  },
};

const convertSlots = (typeSlots) => {
  return uniq(
    typeSlots.map((slotId) => {
      return Object.keys(SLOTS).find((key) => SLOTS[key]?.values.includes(slotId));
    })
  );
};

const BUCKET_BY_CHAIN = {
  [Chain.SKALE]: 'nft-skale',
  [Chain.SONGBIRD]: 'nft',
  [Chain.UNIT0]: 'unit0',
};

function AddType({ collectionId, collectionName, onClose, type = EMPTY_TYPE, clonedItem }) {
  const account = useAccount();
  const itemTypesData = useNftTypes();
  const itemTypes = itemTypesData.data;
  const bucket = BUCKET_BY_CHAIN[account.chainId];

  const nftEditorAddress = CONTRACT_ADDRESSES[account.chainId]?.v5?.nftEditor;
  const writeNftEditor = useWriteContract({
    abi: NFT_EDITOR_ABI,
    address: nftEditorAddress,
  });

  const nftAddress = NFT_CONTRACT_ID[account.chainId];
  const nftCollections = useReadContract({
    initData: [],
    abi: NFT_ABI,
    address: nftAddress,
    functionName: 'getCollections',
    skip: !account.isConnected || !nftAddress,
  });
  const collections = nftCollections.data;

  const isNew = typeof type.typeId === 'undefined';
  const [typeName, setTypeName] = React.useState(type.name);
  const [tokenURI, setTokenURI] = React.useState(
    type.tokenURI || `https://storage.hellopixel.network/${bucket}/`
  );
  const [slots, setSlots] = React.useState(convertSlots(type.slots));
  const [rarity, setRarity] = React.useState(type.rarity);
  const [soulbound, setSoulbound] = React.useState(type.soulbound);
  const [disposable, setDisposable] = React.useState(type.disposable);
  const [collection, setCollection] = React.useState(collectionId);

  React.useEffect(() => {
    try {
      if (!isNew) return;
      setSlots(JSON.parse(window.localStorage.getItem(SLOTS_KEY)) || []);
      setRarity(Number(window.localStorage.getItem(RARITY_KEY)) || 1);
    } catch (error) {}
  }, []);
  React.useEffect(() => {
    if (clonedItem) {
      setTypeName(clonedItem.name);
      setTokenURI(clonedItem.tokenURI);
      setRarity(clonedItem.rarity);
      setSoulbound(clonedItem.soulbound);
      setDisposable(clonedItem.disposable);
    }
  }, [clonedItem]);
  React.useEffect(() => {
    setTokenURI(type.tokenURI || `https://storage.hellopixel.network/${bucket}/`);
  }, [type.tokenURI, account.chainId]);

  const updateSlots = (slots) => {
    setSlots(slots);
    window.localStorage.setItem(SLOTS_KEY, JSON.stringify(slots));

    if (tokenURI.endsWith('/')) {
      if (slots[0]) {
        setTokenURI(`https://storage.hellopixel.network/${bucket}/${slots[0].toLowerCase()}/`);
      } else {
        setTokenURI(`https://storage.hellopixel.network/${bucket}/`);
      }
    }
  };

  const updateRarity = (rarity) => {
    setRarity(rarity);
    window.localStorage.setItem(RARITY_KEY, rarity);
  };

  const slotsArray = flatten(slots.map((slotName) => SLOTS[slotName]?.values));

  const isWrongArguments = !typeName.length || !slotsArray.length || !tokenURI.length;
  const onAddType = async () => {
    try {
      if (isWrongArguments) throw new Error('Wrong arguments');
      const args = [typeName, tokenURI, slotsArray, collection, rarity, soulbound, disposable];

      if (isNew) {
        await writeNftEditor.writeContractAsync({
          functionName: 'createType',
          args,
        });
        // await transactions.createType(...props);
      } else {
        // await transactions.editType(type.typeId, ...props);
        await writeNftEditor.writeContractAsync({
          functionName: 'editType',
          args: [type.typeId, ...args],
        });
      }
      itemTypesData.refetch();
      onClose();
    } catch (error) {
      toaster.logError(error);
    }
  };

  return (
    <div className={styles.add}>
      <FormGroup label='Title'>
        <InputGroup placeholder='Some item name...' value={typeName} onValueChange={setTypeName} />
      </FormGroup>
      <FormGroup label='Image URI'>
        <InputGroup
          placeholder='https://domain...'
          value={tokenURI}
          leftElement={
            !!tokenURI?.length && !tokenURI.endsWith('/') ? (
              <img src={tokenURI} className={styles.addImg} alt={'img'} />
            ) : undefined
          }
          onValueChange={setTokenURI}
        />
      </FormGroup>
      <FormGroup label='Collection'>
        <HTMLSelect
          options={collections}
          value={collections[collection]}
          onChange={(event) => {
            setCollection(collections.indexOf(event.currentTarget.value));
          }}
        />
      </FormGroup>
      <div className={styles.addColumns}>
        <RadioGroup
          label='Rarity'
          onChange={(event) => {
            updateRarity(Number(event.target.value));
          }}
          selectedValue={rarity}
        >
          {RARITY.map((title, index) => {
            return <Radio label={title} value={index} key={index} />;
          })}
        </RadioGroup>
        <FormGroup label='Slots'>
          {Object.keys(SLOTS).map((slotName) => {
            const { title, values } = SLOTS[slotName];
            const checked = slots.includes(slotName);
            return (
              <Switch
                checked={checked}
                label={title}
                key={slotName}
                onChange={() => {
                  if (checked) {
                    updateSlots(slots.filter((s) => s !== slotName));
                  } else {
                    updateSlots([...slots, slotName]);
                  }
                }}
              />
            );
          })}
        </FormGroup>
      </div>
      <FormGroup label={'Settings'}>
        <Switch label={'Soulbound'} checked={soulbound} onChange={() => setSoulbound(!soulbound)} />
        <Switch
          label={'Disposable'}
          checked={disposable}
          onChange={() => setDisposable(!disposable)}
        />
      </FormGroup>
      <Button
        intent={isWrongArguments ? 'warning' : 'success'}
        disabled={isWrongArguments}
        icon={'cloud-upload'}
        onClick={onAddType}
      >
        {isNew ? 'Create new type' : 'Update params'}
      </Button>
    </div>
  );
}

export default AddType;
