import React from 'react';
import styles from './EditVariants.module.scss';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import {
  Button,
  ButtonGroup,
  ControlGroup,
  HTMLSelect,
  Icon,
  InputGroup,
  NumericInput,
  Tab,
  Tabs,
} from '@blueprintjs/core';
import toaster from 'services/toaster';
import wei from 'utils/wei';
import { convertToNftItemDto } from 'lib/NFT/components/NFTCard/convertToNftItemDto.ts';
import AddType from 'lib/AdminV5/pages/nft-editor/components/AddType/AddType';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { useNftTypes } from 'lib/NFT/hooks/useNftTypes.ts';
import { CONTRACT_ADDRESSES } from 'services/multichain/contracts';
import { useAccount } from '@chain/hooks/useAccount';
import { useAutoClaimerAdminApi } from 'lib/MinerClaimer/hooks/useAutoClaimerAdminApi';
import CreateAutoClaimBot from 'lib/AdminV5/pages/nft-editor/components/EditVariants/components/CreateAutoClaimBot/CreateAutoClaimBot.tsx';
import NftRecipeForm from 'lib/Forge/components/_admin/CreateBotRecipe/NftRecipeForm.tsx';
import { useAdminRecipeApi } from 'lib/Forge/hooks/useAdminRecipeApi';
import { useNftType } from 'lib/NFT/hooks/useNftType';
import { getNftTypeId } from 'lib/NFT/utils/id-formatter';
import { createNftFromType } from 'lib/NFT/utils/create-nft-from-type';

const NFT_EDITOR_ABI = require('const/ABI/V5/nftEditor');

function EditVariants({ typeId, onClose }) {
  const account = useAccount();
  const nftEditorAddress = (CONTRACT_ADDRESSES[account.chainId] || {})?.v5?.nftEditor;

  const [tabId, setTabId] = React.useState('modifiers');

  // const nftEditorAddress = network?.contractAddresses?.v5?.nftEditor;
  const writeNftEditor = useWriteContract({
    abi: NFT_EDITOR_ABI,
    address: nftEditorAddress,
  });
  const nftType = useNftType({
    typeId: getNftTypeId(account.chainId, typeId),
  });

  const itemTypesData = useNftTypes();
  const itemTypes = itemTypesData.data;
  const autoClaimers = useAutoClaimerAdminApi().data;
  const autoBotData = autoClaimers[typeId];
  const botsRecipes = useAdminRecipeApi().data;
  const botRecipe = botsRecipes[typeId];

  const itemType = React.useMemo(() => {
    return itemTypes.find((t) => t.typeId === typeId);
  }, [itemTypes.length, typeId]);

  const parameters = [
    'Drill speed multiplier',
    'Storage multiplier',
    'Referral storage multiplier',
    'Add PXLs per hour',
    'Add Storage capacity value',
    'Add Referral storage capacity',
    'Drill upgrade discount',
    'Storage upgrade discount',
    'Referral storage upgrade discount',
    'PXLs chance',
    'Common chance',
    'Uncommon chance',
    'Rare chance',
    'Epic chance',
    'Legendary chance',
    'Drop chance',
    'Second Miner',
    'Market commission sale discount', // 17
    'Market commission order discount', // 18
    '???', // 19
    'Transaction cost', // 20
    'Dust per hour', // 21
    'Mission duration seconds', // 22
    'Missions count', // 23
    'Upgrade cost discount', // 24
  ]; // Total 20

  const addersParameters = [3, 5, 21, 22, 23];

  const DEFAULT_DECIMALS = 18;
  const parameterDecimals = {
    22: 0,
    23: 0,
  };

  const [rows, setRows] = React.useState([
    {
      parameterId: 0,
      add: 0,
      mul: 0,
    },
    {
      parameterId: 1,
      add: 0,
      mul: 0,
    },
    {
      parameterId: 2,
      add: 0,
      mul: 0,
    },
  ]);

  const onAddRow = () => {
    setRows([
      ...rows,
      {
        parameterId: 0,
        add: 0,
        mul: 0,
      },
    ]);
  };
  React.useEffect(() => {
    //onAddRow();
  }, []);

  const onUpdateValue = (rowIndex, value) => {
    const row = rows[rowIndex];
    const isAdder = addersParameters.includes(row.parameterId);
    if (isAdder) {
      rows[rowIndex].add = value;
    } else {
      rows[rowIndex].mul = value;
    }
    setRows([...rows]);
  };

  const [isUpdating, setIsUpdating] = React.useState(false);
  const onSave = async () => {
    try {
      setIsUpdating(true);
      if (!rows.length) throw new Error('Empty modifiers');
      const zeroError = new Error('Modifiers can not be zero');
      const parameters = [];
      const multipliers = [];
      const adders = [];
      console.log('ROWS', rows);
      rows.map((row, i) => {
        const { parameterId, mul, add } = row;
        let multiplier = 1;
        let adder = 0;
        switch (parameterId) {
          case 0:
          case 1:
          case 2:
          case 16:
            if (mul === 0) return;
            multiplier = mul / 100 + 1;
            break;
          case 3:
            if (add === 0) throw zeroError;
            adder = add / 3600;
            break;
          case 4:
          case 5:
          case 21:
          case 22:
          case 23:
            if (add === 0) throw zeroError;
            adder = add;
            break;
          case 6:
          case 7:
          case 8:
          case 17:
          case 18:
          case 24:
            if (mul === 0) return;
            multiplier = 1 - mul / 100;
            break;
          default:
            multiplier = mul / 100 + 1;
            adder = add;
        }
        if (adder || multiplier !== 1) {
          parameters.push(parameterId);
          multipliers.push(wei.to(multiplier, 4));
          const decimals =
            typeof parameterDecimals[parameterId] === 'number'
              ? parameterDecimals[parameterId]
              : DEFAULT_DECIMALS;
          console.log('parameter', parameterId, decimals);
          if (decimals) {
            adders.push(
              adder ? wei.to(adder, parameterDecimals[parameterId] || DEFAULT_DECIMALS) : '0'
            );
          } else {
            adders.push((adder || 0).toFixed());
          }
        }
      });
      console.log('UPDATE', typeId, parameters, multipliers, adders);
      // await transactions.addVariant(typeId, parameters, multipliers, adders);
      await writeNftEditor.writeContractAsync({
        functionName: 'addVariant',
        args: [typeId, parameters, multipliers, adders],
      });
      itemTypesData.refetch();
      onClose();
    } catch (error) {
      toaster.logError(error);
    }
    setIsUpdating(false);
  };

  const onClear = async () => {
    try {
      setIsUpdating(true);
      // await transactions.clearVariants(typeId);
      await writeNftEditor.writeContractAsync({
        functionName: 'clearVariants',
        args: [typeId],
      });
      itemTypesData.refetch();
      onClose();
    } catch (error) {
      toaster.logError(error);
    }
    setIsUpdating(false);
  };

  const renderForm = () => {
    if (autoBotData) {
      return <CreateAutoClaimBot typeId={typeId} />;
    }
    if (botRecipe) {
      return <NftRecipeForm typeId={typeId} />;
    }
    return (
      <>
        <Tabs id='nft-type' onChange={setTabId} renderActiveTabPanelOnly selectedTabId={tabId}>
          <Tab
            id={'modifiers'}
            title={'Modifiers'}
            panel={
              <div className={styles.editForm}>
                {rows.map((row, rowIndex) => {
                  const isAdder = addersParameters.includes(row.parameterId);
                  const value = isAdder ? row.add : row.mul;
                  return (
                    <ControlGroup key={rowIndex}>
                      <HTMLSelect
                        options={parameters}
                        value={parameters[row.parameterId]}
                        onChange={(event) => {
                          rows[rowIndex].parameterId = parameters.indexOf(
                            event.currentTarget.value
                          );
                          setRows([...rows]);
                        }}
                      />
                      <NumericInput
                        placeholder='Some item name...'
                        value={value}
                        autoFocus={!rowIndex}
                        tabIndex={100 + rowIndex}
                        min={isAdder ? 0 : -50}
                        majorStepSize={10}
                        stepSize={1}
                        minorStepSize={0.5}
                        onKeyPress={(e) => {
                          if (e.key === 'Enter') {
                            onSave();
                          }
                        }}
                        intent={value > 0 ? 'success' : value < 0 ? 'danger' : undefined}
                        leftIcon={isAdder ? 'small-plus' : 'percentage'}
                        onValueChange={(value) => onUpdateValue(rowIndex, value)}
                      />
                      <Button
                        onClick={() => {
                          setRows(rows.filter((row, i) => i !== rowIndex));
                        }}
                        disabled={rows.length <= 1}
                        icon={'small-minus'}
                        intent={'danger'}
                      />
                    </ControlGroup>
                  );
                })}
                <ButtonGroup>
                  <Button
                    onClick={onAddRow}
                    disabled={isUpdating}
                    icon={'small-plus'}
                    intent={'primary'}
                  >
                    Add modificator row
                  </Button>
                  <Button
                    onClick={onSave}
                    icon={'cloud-upload'}
                    loading={isUpdating}
                    intent={'success'}
                    disabled={!rows.length || isUpdating}
                  >
                    Save
                  </Button>
                </ButtonGroup>
              </div>
            }
          />
          <Tab
            id={'autoclaim'}
            title={'AutoClaim Bot'}
            panel={<CreateAutoClaimBot typeId={typeId} />}
          />
          <Tab id={'botrecipe'} title={'Bot Recipe'} panel={<NftRecipeForm typeId={typeId} />} />
        </Tabs>
      </>
    );
  };

  return (
    <div className={styles.edit}>
      <div className={styles.editColumns}>
        <div>
          {!!nftType.state && <NFTCard nft={createNftFromType(nftType.state)} />}
          {!!itemType && (
            <AddType collectionId={itemType.collectionId} onClose={onClose} type={itemType} />
          )}
        </div>
        <div>
          {!!itemType && !itemType.variants.length ? (
            renderForm()
          ) : (
            <Button
              intent={'danger'}
              icon={'cloud-download'}
              loading={isUpdating}
              disabled={isUpdating}
              onClick={onClear}
            >
              Clear modifiers
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default EditVariants;
