import React from 'react';
import styles from './CreateAutoClaimBot.module.scss';
import { useAutoClaimerAdminApi } from 'lib/MinerClaimer/hooks/useAutoClaimerAdminApi.ts';
import {
  Button,
  ButtonGroup,
  FormGroup,
  ControlGroup,
  HTMLSelect,
  Icon,
  InputGroup,
  NumericInput,
  Tab,
  Tabs,
  Tag,
} from '@blueprintjs/core';
import wei from 'utils/wei';
import NumberField from '@ui-kit/NumberField/NumberField.tsx';
import Stack from '@mui/material/Stack';
import { Switch } from '@ui-kit/Switch/Switch.tsx';

const MINUTE = 60;
const HOUR = MINUTE * 60;
const DAY = HOUR * 24;

type Time = {
  seconds: number;
  minutes: number;
  hours: number;
  days: number;
};

function getTime(durationSeconds): Time {
  const days = Math.floor((durationSeconds || 0) / DAY);
  let seconds = durationSeconds % DAY;
  const hours = Math.floor(seconds / HOUR);
  seconds = seconds % HOUR;
  const minutes = Math.floor(seconds / MINUTE);
  seconds = seconds % MINUTE;
  return {
    seconds,
    minutes,
    hours,
    days,
  };
}

function CreateAutoClaimBot({ typeId }) {
  const autoClaimersData = useAutoClaimerAdminApi();
  const currentBot = autoClaimersData.data[typeId];
  const isNew = !currentBot;
  const currentSeconds = currentBot?.durationSeconds || 0;
  const currentTime = React.useMemo(() => {
    return getTime(currentSeconds);
  }, [currentSeconds]);
  const currentDurability = currentBot?.sessionClaims || 0;
  const currentSessionSeconds = currentBot?.sessionSeconds || 0;
  const currentStartPrice = currentBot?.startPrice || 0;
  const currentTxPriceMod = currentBot ? wei.from(currentBot.txPriceMod, 4) : 1;

  const [durationSeconds, setDurationSeconds] = React.useState<number>(currentTime.seconds);
  const [durationMinutes, setDurationMinutes] = React.useState<number>(currentTime.minutes);
  const [durationHours, setDurationHours] = React.useState<number>(currentTime.hours);
  const [durationDays, setDurationDays] = React.useState<number>(currentTime.days);
  const [durability, setDurability] = React.useState<string>(currentDurability.toString());
  const [startPrice, setStartPrice] = React.useState<string>(currentStartPrice.toString());
  const [txPriceMod, setTxPriceMod] = React.useState<string>(currentTxPriceMod.toString());
  const [sessionHours, setSessionHours] = React.useState<string>(
    (currentSessionSeconds / 60 / 60).toString()
  );

  const duration =
    durationSeconds + (durationMinutes + (durationHours + durationDays * 24) * 60) * 60;

  React.useEffect(() => {
    setDurationSeconds(currentTime.seconds);
    setDurationMinutes(currentTime.minutes);
    setDurationHours(currentTime.hours);
    setDurationDays(currentTime.days);
    setDurability(currentDurability.toString());
    setStartPrice(currentStartPrice.toString());
    setTxPriceMod(currentTxPriceMod.toString());
    setSessionHours((currentSessionSeconds / 60 / 60).toString());
  }, [currentBot]);

  const disabled =
    !(
      isNew ||
      durability !== currentDurability ||
      duration !== currentSeconds ||
      txPriceMod !== currentTxPriceMod ||
      startPrice !== currentStartPrice ||
      currentSessionSeconds / 60 / 60 !== sessionHours
    ) ||
    Number.isNaN(+durability) ||
    Number.isNaN(+txPriceMod) ||
    Number.isNaN(+startPrice) ||
    !+durability ||
    !+txPriceMod ||
    !+startPrice;

  return (
    <div className={styles.block}>
      <Stack direction='column' gap={2} sx={{ mb: 2 }}>
        <NumberField
          label='Start Price'
          placeholder='Start price'
          value={startPrice}
          min={0}
          size='small'
          onChange={(e) => setStartPrice(e.target.value)}
        />

        <NumberField
          label='Tx Cost Multiplier'
          placeholder='Tx Cost Multiplier'
          value={txPriceMod}
          min={0.01}
          max={10}
          size='small'
          onChange={(e) => setTxPriceMod(e.target.value)}
        />

        <NumberField
          label='Durability claims'
          placeholder='Durability claims'
          value={durability}
          min={0}
          size='small'
          onChange={(e) => setDurability(e.target.value)}
        />
        <NumberField
          label='Durability Hours'
          placeholder='Durability Hours'
          value={sessionHours}
          min={0}
          size='small'
          onChange={(e) => setSessionHours(e.target.value)}
        />
      </Stack>
      <FormGroup label={'Life time'}>
        <ControlGroup>
          <NumericInput
            placeholder='Days'
            value={durationDays}
            min={0}
            majorStepSize={10}
            stepSize={1}
            leftIcon={'time'}
            rightElement={<Tag>Days</Tag>}
            onValueChange={setDurationDays}
          />
          <NumericInput
            placeholder='Hours'
            value={durationHours}
            min={0}
            max={24}
            majorStepSize={8}
            stepSize={1}
            rightElement={<Tag>Hours</Tag>}
            onValueChange={setDurationHours}
          />
        </ControlGroup>
        <ControlGroup>
          <NumericInput
            placeholder='Mins'
            value={durationMinutes}
            min={0}
            max={60}
            majorStepSize={10}
            stepSize={1}
            rightElement={<Tag>minutes</Tag>}
            onValueChange={setDurationMinutes}
          />
          <NumericInput
            placeholder='Secs'
            value={durationSeconds}
            min={0}
            max={60}
            majorStepSize={10}
            stepSize={1}
            rightElement={<Tag>seconds</Tag>}
            onValueChange={setDurationSeconds}
          />
        </ControlGroup>
      </FormGroup>
      <FormGroup>
        {!isNew && (
          <Button
            loading={autoClaimersData.loading}
            disabled={autoClaimersData.loading}
            intent={'danger'}
            onClick={() => autoClaimersData.unset(typeId)}
          >
            Unset
          </Button>
        )}
        <Button
          loading={autoClaimersData.loading}
          disabled={autoClaimersData.loading || disabled}
          intent={'success'}
          onClick={() =>
            autoClaimersData.set({
              typeId,
              sessionClaims: Number(durability),
              sessionSeconds: Math.round(Number(sessionHours) * 60 * 60),
              durationSeconds: Math.round(Number(duration)),
              isTimeSession: Number(sessionHours) > 0,
              startPrice: Number(startPrice),
              txPriceMod: Number(txPriceMod),
            })
          }
        >
          {isNew ? 'Set as AutoClaim bot' : 'Update bot'}
        </Button>
      </FormGroup>
    </div>
  );
}

export default CreateAutoClaimBot;
