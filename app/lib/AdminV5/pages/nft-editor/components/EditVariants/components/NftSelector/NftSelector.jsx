import React from 'react';
import styles from './NftSelector.module.scss';
import { Button, MenuItem, Tag } from '@blueprintjs/core';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel';
import { Select } from '@blueprintjs/select';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor';
import uriTransform from 'utils/uriTransform';

function NftSelector({ itemTypes, typeId, setTypeId, collections }) {
  const renderItem = (itemType, { handleClick, handleFocus, modifiers, query }) => {
    const collection = collections ? collections[itemType.collectionId] : '';
    return (
      <MenuItem
        className={styles.wrap}
        onClick={handleClick}
        onFocus={handleFocus}
        key={itemType.typeId}
        active={itemType.typeId === typeId}
        label={collection}
        text={
          <div className={styles.item}>
            <div className={styles.itemImage}>
              <img src={uriTransform(itemType.tokenURI)} alt={''} />
            </div>
            <div className={styles.itemName}>{`${itemType.typeId}. ${itemType.name}`}</div>
            <Tag
              style={{
                backgroundColor: getNftRarityColor(itemType.rarity),
              }}
            >
              {getNftRarityLabel(itemType.rarity)}
            </Tag>
          </div>
        }
        roleStructure='listoption'
      />
    );
  };

  const itemPredicate = (query, type, _index, exactMatch) => {
    const normalizedTitle = type.name.toLowerCase();
    const normalizedQuery = query.toLowerCase();
    const collection =
      collections && collections[type.collectionId]
        ? collections[type.collectionId].toLowerCase()
        : collections;

    if (exactMatch) {
      return normalizedTitle === normalizedQuery;
    } else {
      return `${type.typeId}. ${normalizedTitle} ${collection}`.indexOf(normalizedQuery) >= 0;
    }
  };

  const selected = itemTypes.find((t) => t.typeId === typeId);

  return (
    <Select
      items={itemTypes}
      onItemSelect={(item) => {
        setTypeId(item.typeId);
      }}
      noResults={<MenuItem disabled={true} text='No results.' roleStructure='listoption' />}
      itemPredicate={itemPredicate}
      itemRenderer={renderItem}
    >
      <Button rightIcon='double-caret-vertical'>
        {selected ? `${selected.typeId}. ${selected.name}` : 'Select an Item'}
      </Button>
    </Select>
  );
}

export default NftSelector;
