import React from 'react';
import styles from './Types.module.scss';
import { Drawer } from '@blueprintjs/core';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import AddType from 'lib/AdminV5/pages/nft-editor/components/AddType/AddType';
import EditVariants from 'lib/AdminV5/pages/nft-editor/components/EditVariants/EditVariants';
import { Button } from '@ui-kit/Button/Button.tsx';
import { ButtonGroup } from '@ui-kit/ButtonGroup/ButtonGroup.tsx';
import { useNftTypes } from 'lib/AdminV5/hooks/useNftTypes';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants';
import { useAccount } from '@chain/hooks/useAccount';
import Pagination from '@mui/material/Pagination';
import { createNftFromType } from 'lib/NFT/utils/create-nft-from-type';
import LinearProgress from '@mui/material/LinearProgress';
import { Typography } from '@ui-kit/Typography/Typography';

function Types({ collectionId, collectionIndex, collectionName }) {
  const { chainId } = useAccount();
  const nftAddress = NFT_CONTRACT_ID[chainId];

  const itemTypesData = useNftTypes();

  const nftTypes = useNftTypes({
    contractId: nftAddress?.toLowerCase(),
    collectionId,
    skip: !nftAddress || !collectionId,
  });
  const itemTypes = nftTypes.types;

  const [editId, setEditId] = React.useState(null);
  const [clonedItem, setClonedItem] = React.useState(null);

  const renderPagination = () => (
    <Pagination
      showFirstButton
      showLastButton
      sx={{ margin: '16px auto 0 auto' }}
      count={nftTypes.pagination.pages}
      color='primary'
      variant='outlined'
      shape='rounded'
      size='large'
      siblingCount={3}
      boundaryCount={0}
      page={nftTypes.pagination.page}
      onChange={(_, p) => {
        nftTypes.pagination.setPage(p);
      }}
    />
  );

  return (
    <div className={styles.types}>
      <div className={styles.container}>
        {nftTypes.typesCount > 20 && renderPagination()}

        {nftTypes.loading ? (
          <LinearProgress sx={{ width: '100%' }} />
        ) : (
          <div style={{ height: 4 }} />
        )}

        <div className={styles.typesCards}>
          {itemTypes.map((nftType) => {
            const typeId = +nftType.id.split('-').pop();
            const modsCount = nftType.mods.length;
            const chainType = itemTypesData.data?.find((el) => el.typeId === typeId);
            const chainVariantsCount = chainType?.variants?.length;
            const invalidMods =
              itemTypesData.data?.length &&
              (chainVariantsCount > 1 || chainType?.variants?.[0]?.length !== modsCount);

            return (
              <div key={`item-${typeId}`}>
                <NFTCard nft={createNftFromType(nftType)} />

                <Typography
                  variant='caption'
                  align='center'
                  color='text.secondary'
                  sx={{ display: 'block' }}
                >
                  id: {nftType.id?.split('-').pop()}
                </Typography>

                <div style={{ display: 'flex', flexDirection: 'column', gap: 4 }}>
                  <ButtonGroup>
                    <Button
                      variant='contained'
                      color={invalidMods ? 'error' : 'primary'}
                      fullWidth
                      onClick={() => {
                        setEditId(typeId);
                      }}
                    >
                      Edit
                    </Button>
                    <Button
                      variant='contained'
                      color='primary'
                      fullWidth
                      onClick={() => {
                        setClonedItem(nftType);
                        setEditId(-1);
                      }}
                    >
                      Clone
                    </Button>
                  </ButtonGroup>
                </div>
              </div>
            );
          })}
          <div>
            <div
              className={styles.add}
              key={-1}
              onClick={() => {
                setEditId(-1);
              }}
            >
              + Add Type
            </div>
          </div>
        </div>

        {nftTypes.typesCount > 20 && renderPagination()}
      </div>

      <Drawer
        isOpen={editId === -1}
        onClose={() => {
          setEditId(null);
        }}
        title={'Add new Item Type'}
        className={'bp5-dark'}
      >
        <AddType
          collectionId={collectionIndex}
          onClose={() => setEditId(null)}
          collectionName={collectionName}
          clonedItem={clonedItem}
        />
      </Drawer>
      <Drawer
        isOpen={typeof editId === 'number' && editId >= 0}
        onClose={() => {
          setEditId(null);
        }}
        size={'75%'}
        title={'Edit variants'}
        className={'bp5-dark'}
      >
        <EditVariants typeId={editId} onClose={() => setEditId(null)} />
      </Drawer>
    </div>
  );
}

export default Types;
