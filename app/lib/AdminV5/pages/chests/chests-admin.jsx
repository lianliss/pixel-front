import React from 'react';
import Chests from 'lib/AdminV5/pages/nft-editor/components/Chests/Chests';

function ChestsAdmin() {
  return <Chests />;
}

export default ChestsAdmin;
