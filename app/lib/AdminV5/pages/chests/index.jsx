import React from 'react';
import ChestsAdmin from './chests-admin';
import { AdminLayout } from 'lib/AdminV5/components/AdminLayout/AdminLayout.tsx';

function EditorWrap() {
  return (
    <AdminLayout>
      <ChestsAdmin />
    </AdminLayout>
  );
}

export default EditorWrap;
