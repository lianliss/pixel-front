import React, { useEffect, useState } from 'react';
import { useReadFreeChests } from 'lib/Chests/hooks/deprecated/useOldUserChests.ts';
import { FREE_CHEST_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { TextField } from '@ui-kit/TextField/TextField.tsx';

function UserPreview() {
  const account = useAccount();

  const freeChestAddress = FREE_CHEST_CONTRACT_ID[account.chainId];

  const [telegramId, setTelegramId] = useState('');

  const freeChestsData = useReadFreeChests({
    freeChestAddress,
    telegramId: +telegramId,
    skip: !account.isConnected || !telegramId || Number.isNaN(telegramId),
  });
  const freeChests = freeChestsData.data
    .map((count, chestId) => ({ chestId: Number(chestId), count: Number(count) > 0 ? 1 : 0 }))
    .filter((el) => el.count);

  useEffect(() => {
    freeChestsData.clear();
  }, [telegramId]);

  return (
    <div>
      <Stack direction='column' gap={2}>
        <TextField
          label='Telegram ID'
          value={telegramId}
          onChange={(e) => setTelegramId(e.target.value)}
          sx={{ maxWidth: 200 }}
          fullWidth
        />

        <Paper sx={{ p: 2 }}>
          <Typography sx={{ mb: 1 }}>
            Chests {freeChestsData.loading ? 'loading...' : ''}
          </Typography>
          {freeChests.map((chest) => `${chest.chestId}: ~${chest.count}`)}
          {!freeChestsData.loading && !freeChests?.length && 'No Chests'}
        </Paper>
      </Stack>
    </div>
  );
}

export default UserPreview;
