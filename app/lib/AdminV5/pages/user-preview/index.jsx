import React from 'react';
import UserPreview from './user-preview.tsx';
import { AdminLayout } from 'lib/AdminV5/components/AdminLayout/AdminLayout.tsx';

function EditorWrap() {
  return (
    <AdminLayout>
      <UserPreview />
    </AdminLayout>
  );
}

export default EditorWrap;
