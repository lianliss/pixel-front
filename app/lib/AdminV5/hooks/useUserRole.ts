import { EDITOR_ACCOUNTS } from '@cfg/config.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';

export function useUserRole() {
  const { accountAddress, isConnected } = useAccount();

  return {
    isConnected,
    editor: isConnected && EDITOR_ACCOUNTS.includes(accountAddress?.toLowerCase()),
  };
}
