import { useUnit } from 'effector-react';
import { loadNftCollections, nftCollectionsStore } from 'lib/NFT/api-server/nft.store.ts';
import { useEffect } from 'react';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import NFT_ABI from 'lib/NFT/api-contract/nft.abi.ts';
import { INftCollectionDto } from 'lib/NFT/api-server/nft.types.ts';
import { convertNftCollection } from 'lib/AdminV5/utils/convert-nft-collection.ts';

export function useNftCollections({
  contractId,
  fromChain = false,
}: {
  contractId?: string;
  fromChain?: boolean;
}): { loading: boolean; state: { rows: INftCollectionDto[]; count: number }; error: Error | null } {
  const collections = useUnit(nftCollectionsStore);

  useEffect(() => {
    if (contractId && !fromChain) {
      loadNftCollections({ contractId, limit: 50, offset: 0 }).then().catch();
    }
  }, [contractId, fromChain]);

  const nftCollections = useReadContract({
    initData: [],
    abi: NFT_ABI,
    address: contractId,
    functionName: 'getCollections',
    skip: !(contractId && fromChain),
  });

  if (fromChain) {
    return {
      loading: nftCollections.loading,
      error: nftCollections.error,
      state: {
        rows: nftCollections.data.map((name, i) => convertNftCollection(name, i)),
        count: nftCollections.data.length,
      },
    };
  }

  return collections;
}
