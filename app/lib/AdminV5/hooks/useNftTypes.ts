import { useUnit } from 'effector-react';
import { loadNftTypes, nftCollectionsStore, nftTypesStore } from 'lib/NFT/api-server/nft.store.ts';
import { useEffect, useState } from 'react';
import { IPaginationResult, usePagination } from 'utils/hooks/usePagination.ts';
import { INftCollectionDto, INftTypeDto, NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';

export type INftTypesResult = {
  types: INftTypeDto[];
  typesCount: number;
  collectionId: string | undefined;
  changeCollectionId: (collectionId: string | undefined) => void;
  rarity: NftItemRarity | undefined;
  changeRarity: (rarity: NftItemRarity | undefined) => void;
  collections: INftCollectionDto[];
  pagination: IPaginationResult;
  loading: boolean;
};

export function useNftTypes({
  contractId,
  skip,
  collectionId: overrideCollectionId,
}: {
  contractId?: string;
  skip?: boolean;
  collectionId?: string;
} = {}): INftTypesResult {
  const data = useUnit(nftTypesStore);

  const collections = useUnit(nftCollectionsStore);

  const pagination = usePagination({ total: data.state.count, step: 20 });
  const [rarity, setRarity] = useState<NftItemRarity | undefined>(undefined);
  const [collectionId, setCollectionId] = useState<string | undefined>(undefined);

  useEffect(() => {
    if (contractId && !skip) {
      loadNftTypes({
        contractId,
        collectionId: overrideCollectionId || collectionId,
        limit: pagination.limit,
        offset: pagination.offset,
        rarity,
      })
        .then()
        .catch();
    }
  }, [
    contractId,
    collectionId,
    pagination.offset,
    pagination.limit,
    skip,
    rarity,
    overrideCollectionId,
  ]);

  return {
    loading: data.loading,
    types: data.state.rows,
    typesCount: data.state.count,
    pagination,
    rarity,
    changeRarity: (r: NftItemRarity | undefined) => setRarity(r),
    collectionId,
    changeCollectionId: (r: string | undefined) => setCollectionId(r),
    collections: collections.state.rows,
  };
}
