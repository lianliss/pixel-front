import { IAchievementDto } from 'lib/Achievement/api-server/achievement.types.ts';
import { AchievementsStatus } from 'lib/Achievement/components/AchievementsItem/AchievementsItem.tsx';
import { getPercentage } from 'utils/format/number.ts';
import { AchievementDtoWithStatus } from 'lib/Achievement/pages/achievements/achievement.page.tsx';

import { SONGBIRD, SKALE_TEST } from 'services/multichain/chains';
import sgbBgImage from 'assets/img/market/sgb-bg.png';
import React from 'react';

const bgImageByChain = {
  [SONGBIRD]: sgbBgImage as string,
  [SKALE_TEST]: sgbBgImage as string,
};

export function getAchievementRootStyles(chainId: number): React.CSSProperties {
  return {
    backgroundImage: bgImageByChain[chainId] ? `url(${bgImageByChain[chainId]})` : undefined,
  };
}

export function getAchievementPointSymbol() {
  return 'PTS';
}

export function getAchievementStatus(item: IAchievementDto) {
  const currentLevel = item.userAchievement?.level || 0;
  const nextTask = item.tasks
    .sort((a, b) => a.level - b.level)
    .find((task) => task.level > currentLevel);
  const isComplete = item.userAchievement?.value >= (nextTask?.value || 0);
  const isAllCompleted = !nextTask;
  const isStarted = item.userAchievement?.value > 0;

  let status = AchievementsStatus.noop;

  if (isStarted) {
    status = AchievementsStatus.processing;
  }
  if (isComplete) {
    status = AchievementsStatus.availableForClaim;
  }
  if (isAllCompleted) {
    status = AchievementsStatus.claimed;
  }

  return status;
}

// Функция для получения следующего задания
const getNextTask = (achievement: AchievementDtoWithStatus) => {
  return achievement.tasks.find((task) => task.level > (achievement.userAchievement?.level || 0));
};

// Функция сортировки ачивок по уровню значений следующего задания
export const sortAchievements = (a: AchievementDtoWithStatus, b: AchievementDtoWithStatus) => {
  const nextTaskA = getNextTask(a);
  const nextTaskB = getNextTask(b);

  const valueA = a.userAchievement?.value ?? 0;
  const totalA = nextTaskA?.value || 0;
  const percentageA = getPercentage(valueA, totalA);

  const valueB = b.userAchievement?.value ?? 0;
  const totalB = nextTaskB?.value || 0;
  const percentageB = getPercentage(valueB, totalB);

  return percentageB - percentageA;
};
