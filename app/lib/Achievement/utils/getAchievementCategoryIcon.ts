import { SvgIconProps } from '@mui/material/SvgIcon';
import TradeIcon from 'lib/Achievement/components/Icons/Trade.tsx';
import NftIcon from 'lib/Achievement/components/Icons/NFT.tsx';
import SocialIcon from 'lib/Achievement/components/Icons/Social.tsx';
import IncusionIcon from 'lib/Achievement/components/Icons/Incusion.tsx';
import ActivityIcon from 'lib/Achievement/components/Icons/Activity.tsx';
import Unknown from '@mui/icons-material/Help';
import React from 'react';

const map = {
  trade: TradeIcon,
  nft: NftIcon,
  social: SocialIcon,
  incusion: IncusionIcon,
  activity: ActivityIcon,
};

export function getAchievementCategoryIcon(category: string): React.FC<SvgIconProps> {
  const icon = map[category] || Unknown;

  return icon;
}
