export function getAchievementRangImage(index: number): string {
  return `https://storage.hellopixel.network/assets/achievement/rang/${index}.png`;
}
