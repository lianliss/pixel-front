import { useState } from 'react';
import {
  claimAchievement,
  loadAchievements,
  loadAchievementUserRang,
} from 'lib/Achievement/api-server/achievement.store.ts';
import { AchievementGroup } from 'lib/Achievement/api-server/achievement.types.ts';
import toaster from 'services/toaster.tsx';

export function useClaimAchievement({
  accountAddress,
  group,
}: {
  accountAddress?: string;
  group?: AchievementGroup;
}) {
  const [isLoading, setIsLoading] = useState(false);

  // handlers
  const onClaimAchievement = (achievementId: string) => {
    setIsLoading(true);

    void claimAchievement({ achievementId, userAddress: accountAddress, group })
      .then((_) => {
        void loadAchievementUserRang({
          userAddress: accountAddress,
          group: group,
        });
        void loadAchievements({ userAddress: accountAddress, group });

        toaster.success(`Successfully`);
      })
      .finally(() => setIsLoading(false));
  };

  return {
    loading: isLoading,
    claim: onClaimAchievement,
  };
}
