import { useEffect } from 'react';
import {
  achievementsRangListStore,
  achievementsStore,
  loadAchievementRangList,
} from 'lib/Achievement/api-server/achievement.store.ts';
import { AchievementGroup } from 'lib/Achievement/api-server/achievement.types.ts';
import { useUnit } from 'effector-react';
import { useAccount } from '@chain/hooks/useAccount.ts';

export function useUserAchievementRangList(
  opts: { skip?: boolean; group?: AchievementGroup } = {}
) {
  const account = useAccount();
  const achievementsRangList = useUnit(achievementsRangListStore);
  const loading = useUnit(loadAchievementRangList.pending);

  useEffect(() => {
    if (!opts.group) {
      achievementsRangListStore.reset();
    }
  }, [opts.group]);
  useEffect(() => {
    if (account.isConnected && !opts.skip && opts.group) {
      void loadAchievementRangList({
        userAddress: account.accountAddress,
        group: opts.group,
      });
    }
  }, [account.accountAddress, opts.skip, opts.group]);

  return {
    data: achievementsRangList,
    loading,
    reset: () => achievementsRangListStore.reset(),
  };
}
