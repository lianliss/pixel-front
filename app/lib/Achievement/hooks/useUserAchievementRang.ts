import { useEffect } from 'react';
import {
  achievementsStore,
  achievementUserRangStore,
  loadAchievementUserRang,
} from 'lib/Achievement/api-server/achievement.store.ts';
import { AchievementGroup } from 'lib/Achievement/api-server/achievement.types.ts';
import { useUnit } from 'effector-react';

export function useUserAchievementRang(
  address?: string,
  opts: { group?: AchievementGroup } = { group: AchievementGroup.Default }
) {
  const achievementUserRang = useUnit(achievementUserRangStore);

  useEffect(() => {
    if (!opts.group) {
      achievementUserRangStore.reset();
    }
  }, [opts.group]);

  useEffect(() => {
    if (address && opts.group) {
      void loadAchievementUserRang({
        userAddress: address,
        group: opts.group || AchievementGroup.Default,
      });
    }
  }, [address, opts.group]);

  return {
    data: achievementUserRang.data,
    loading: achievementUserRang.loading,
    reset: () => achievementUserRangStore.reset(),
  };
}
