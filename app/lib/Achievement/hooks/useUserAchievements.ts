import { useUnit } from 'effector-react';
import {
  achievementsStore,
  loadAchievements,
} from 'lib/Achievement/api-server/achievement.store.ts';
import { useEffect } from 'react';
import { useInterval } from 'utils/hooks/useInterval';
import { AchievementGroup } from 'lib/Achievement/api-server/achievement.types.ts';
import toaster from 'services/toaster.tsx';

export function useUserAchievements({
  accountAddress,
  group: achievementGroup,
}: {
  accountAddress?: string;
  group?: AchievementGroup;
}) {
  const achievements = useUnit(achievementsStore);

  useEffect(() => {
    if (!achievementGroup) {
      achievementsStore.reset();
    }
  }, [achievementGroup]);

  useEffect(() => {
    if (accountAddress && achievementGroup) {
      void loadAchievements({ userAddress: accountAddress, group: achievementGroup }).catch((e) =>
        toaster.captureException(e)
      );
    }
  }, [accountAddress, achievementGroup]);

  useInterval(
    () => {
      if (accountAddress && achievementGroup) {
        void loadAchievements({ userAddress: accountAddress, group: achievementGroup });
      }
    },
    30_000,
    [accountAddress, achievementGroup]
  );

  return {
    data: achievements,
  };
}
