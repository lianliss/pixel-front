import React from 'react';
import { IconInfo } from '@ui-kit/icon/common/IconInfo.tsx';
import Popover from '@mui/material/Popover';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

const AchievementTooltip = ({ title }) => {
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'achi-popover' : undefined;

  return (
    <div>
      <Button aria-describedby='ach-popover' onClick={handleClick}>
        <IconInfo />
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <Typography sx={{ p: 2 }} variant='h4'>
          {title}
        </Typography>
      </Popover>
    </div>
  );
};

export default AchievementTooltip;
