import React from 'react';
import { classNames } from '../../../../../shared/lib/classNames.ts';
import styles from './styles.module.scss';
import { IAchievementDto } from 'lib/Achievement/api-server/achievement.types.ts';
import pixelImage from 'assets/img/token/common/pixel.png';

type Props = {
  data?: IAchievementDto;
  isOpacity?: boolean;
  onSelect?: (achievement: IAchievementDto) => void;
};
const AchievementImage = ({ data, onSelect, isOpacity }: Props) => {
  return (
    <div
      className={classNames(styles.image__wrapper, {
        [styles.opacity]: isOpacity,
      })}
    >
      <img
        className={styles.image}
        src={data?.image}
        alt=''
        onClick={() => {
          onSelect?.(data);
        }}
        onError={(e) => {
          (e.target as HTMLImageElement).src = pixelImage as string;
        }}
      />
    </div>
  );
};

export default AchievementImage;
