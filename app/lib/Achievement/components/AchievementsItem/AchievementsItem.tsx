import styles from './styles.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

import { Button } from '@ui-kit/Button/Button.tsx';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import React, { useMemo, useState } from 'react';
import {
  claimAchievement,
  loadAchievements,
  loadAchievementUserRang,
} from 'lib/Achievement/api-server/achievement.store.ts';
import { getAchievementPointSymbol } from 'lib/Achievement/utils/utils.ts';
import { classNames } from '../../../../shared/lib/classNames.ts';
import toaster from 'services/toaster.tsx';
import { AchievementDtoWithStatus } from 'lib/Achievement/pages/achievements/achievement.page.tsx';
import { getAchievementCategoryIcon } from 'lib/Achievement/utils/getAchievementCategoryIcon.ts';
import levelImage from 'assets/img/achievements/level.png';
import { AchievementGroup, IAchievementDto } from 'lib/Achievement/api-server/achievement.types.ts';
import { ProgressBar } from '@ui-kit/ProgressBar/ProgressBar.tsx';
import AchievementTooltip from 'lib/Achievement/components/AchievementsItem/AchievementTooltip/AchievementTooltip.tsx';
import { useTranslation } from 'react-i18next';
import AchievementImage from 'lib/Achievement/components/AchievementsItem/AchievementImage/AchievementImage.tsx';

const DarkenPaper = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.background.default,
}));
const BlackPaper = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.background.black2,
}));
const CategoryButton = styled('button')(({ theme }) => ({
  backgroundColor: theme.palette.background.paper,
}));

export enum AchievementsStatus {
  noop = 'noop',
  processing = 'processing',
  availableForClaim = 'availableForClaim',
  claimed = 'claimed',
}

type Props = {
  data: AchievementDtoWithStatus;
  accountAddress: string;
  setCollectionFilter: (collection: string) => void;
  isCollectionFilter?: boolean;
  onSelect?: (el: IAchievementDto) => void;
  onClaim?: (id: string) => void;
  loading?: boolean;
};
export const AchievementsItem = (props: Props) => {
  const {
    data,
    accountAddress,
    setCollectionFilter,
    isCollectionFilter,
    onSelect,
    onClaim,
    loading = false,
  } = props;

  const { t } = useTranslation('achievements');

  const CategoryIcon = getAchievementCategoryIcon(data.category);

  // recalculate
  const currentLevel = data.userAchievement?.level || 0;
  const currentTask = data.tasks
    .sort((a, b) => a.level - b.level)
    .find((task) => task.level === currentLevel);
  const nextTask = data.tasks
    .sort((a, b) => a.level - b.level)
    .find((task) => task.level > currentLevel);
  const isComplete = data.userAchievement?.value >= nextTask?.value;

  const claimPoints = useMemo(() => {
    if (data.status === AchievementsStatus.claimed) {
      return data.tasks.reduce((acc, t) => acc + t.points, 0);
    }

    if (!data.userAchievement || currentLevel < nextTask?.level) {
      return nextTask?.points || 0;
    }

    const tasks = data.tasks.filter(
      (task) => task.level > currentLevel && data.userAchievement.value >= task.value
    );

    return tasks.reduce((acc, t) => acc + t.points, 0);
  }, [currentLevel, data, nextTask, isComplete]);

  const userAchievementValue = data.userAchievement?.value ?? 0;
  const taskValue = nextTask?.value || currentTask?.value || 0;

  const adjustedValue = userAchievementValue > taskValue ? taskValue : userAchievementValue;

  return (
    <DarkenPaper
      variant='outlined'
      elevation={0}
      className={styles.achievements__item}
      sx={{ opacity: data.disabled ? 0.5 : undefined }}
      aria-label={data.status === AchievementsStatus.availableForClaim ? 'complete' : 'default'}
    >
      <BlackPaper className={styles.top__side} elevation={0}>
        <div className={styles.info__tooltip}>
          <AchievementTooltip title={t(data.summary)} />
        </div>

        <AchievementImage
          data={data}
          onSelect={onSelect}
          isOpacity={data.status === AchievementsStatus.claimed}
        />

        <div className={styles.progress}>
          {data.tasks?.length > 1 && (
            <Stack className={styles.levels} direction='row'>
              {data.tasks.reverse().map((task, i) => (
                <img
                  key={`level-${task.id}`}
                  src={levelImage as string}
                  alt=''
                  style={
                    currentLevel < task.level
                      ? {
                          opacity: 0.8,
                          filter: 'grayscale(1)',
                        }
                      : undefined
                  }
                />
              ))}
            </Stack>
          )}

          <Typography className={styles.title} medium variant='caption'>
            {t(data.name)}
          </Typography>
          <div className={styles.reward}>
            <Typography
              className={styles.text}
              medium
              variant='caption'
              color='success.main'
              noWrap
            >
              <Typography variant='inherit' component='span' color='text.secondary'>
                {t('Reward')}
              </Typography>{' '}
              {claimPoints} {getAchievementPointSymbol()}
            </Typography>
            <Typography
              className={styles.text}
              medium
              variant='caption'
              noWrap
              color='success.main'
            >
              {adjustedValue}/{taskValue}
            </Typography>
          </div>
          {data.tasks.length > 0 && (
            <div
              className={classNames(styles.progress__line, {
                [styles.opacity]: data.status === AchievementsStatus.claimed,
              })}
            >
              <ProgressBar
                value={data.userAchievement?.value ?? 0}
                total={nextTask?.value || currentTask?.value || 0}
              />
            </div>
          )}
        </div>
      </BlackPaper>

      <div className={styles.bottom__side}>
        <CategoryButton
          data-selected={!!isCollectionFilter}
          className={classNames(styles.category, {
            [styles.opacity]: data.status === AchievementsStatus.claimed,
            [styles.category_selected]: isCollectionFilter,
          })}
          onClick={() => setCollectionFilter(data.category)}
        >
          <CategoryIcon />

          <div className={styles.text__wrapper}>
            <Typography variant='caption' color='text.primary' light>
              {t('Collection')}
            </Typography>
            <Typography variant='caption' color='text.primary'>
              {data.category}
            </Typography>
          </div>
        </CategoryButton>

        <div className={styles.btn__section}>
          {/* Если совершен claim */}
          {data.status === AchievementsStatus.claimed ? (
            <Typography variant='caption' fontWeight='bold' color='green-primary'>
              {t('Claimed')} ✔
            </Typography>
          ) : data.status === AchievementsStatus.availableForClaim ? (
            <Button
              loading={loading}
              variant='contained'
              disabled={!nextTask || !isComplete || !onClaim}
              onClick={() => onClaim?.(data.id)}
            >
              <Typography variant='caption' fontWeight='bold'>
                {t('Claim')} {claimPoints} {getAchievementPointSymbol()}
              </Typography>
            </Button>
          ) : (
            <Button variant='outlined' color='primary' disabled={!nextTask || !isComplete}>
              <Typography className={styles.progress} variant='caption' fontWeight='bold'>
                {t('In Progress')}
              </Typography>
            </Button>
          )}
        </div>
      </div>
    </DarkenPaper>
  );
};
