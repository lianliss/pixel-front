import React from 'react';
import styles from './styles.module.scss';
import Typography from '@mui/material/Typography';

type Props = {
  isDone: boolean;
};
export const CheckBox = ({ isDone }: Props) => {
  return (
    <div className={styles.check__box}>
      {isDone && (
        <Typography variant='caption' fontWeight='bold'>
          ok
        </Typography>
      )}
    </div>
  );
};
