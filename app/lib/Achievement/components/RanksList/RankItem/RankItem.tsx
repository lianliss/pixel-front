import { Typography } from '@ui-kit/Typography/Typography.tsx';
import React from 'react';
import CheckIcon from '@mui/icons-material/Check';

import styles from './styles.module.scss';
import {
  IAchievementRangDto,
  IUserAchievementRangDto,
} from 'lib/Achievement/api-server/achievement.types.ts';
import { classNames } from '../../../../../shared/lib/classNames.ts';
import { CheckBox } from 'lib/Achievement/components/RanksList/RankItem/CheckBox/CheckBox.tsx';
import { getAchievementPointSymbol } from 'lib/Achievement/utils/utils.ts';
import { getAchievementRangImage } from 'lib/Achievement/utils/getAchievementRangImage.ts';
import { useTranslation } from 'react-i18next';

type Props = {
  rank: IAchievementRangDto;
  userRank: IUserAchievementRangDto;
  onSelect?: (rank: IAchievementRangDto) => void;
};
export const RankItem = (props: Props) => {
  const { rank, userRank, onSelect } = props;
  const { t } = useTranslation('achievements');

  const isDone = userRank?.level >= rank.level;
  const isProgress = rank.level === userRank?.level + 1;

  return (
    <div className={styles.rank}>
      <div className={styles.rankItem__head}>
        <Typography
          variant='caption'
          fontWeight='bold'
          color='text.primary'
          sx={{ opacity: isDone ? 1 : 0.4 }}
        >
          {t('Level')} {rank.level}
        </Typography>
      </div>

      <div
        className={classNames(styles.rank__inner, { [styles.done]: isDone })}
        style={{ backgroundImage: `url(${getAchievementRangImage(rank.level)})` }}
        onClick={() => {
          onSelect?.(rank);
        }}
      >
        {isDone && (
          <div className={styles.rankItem__checkbox}>
            <CheckIcon
              sx={{
                color: 'white',
                fontSize: 16,
              }}
            />
          </div>
        )}
      </div>

      <span className={styles.rankItem__status}>
        <span className={styles.progress__section}>
          <Typography
            className={styles.points}
            variant='caption'
            color={isDone ? 'success.main' : 'text.primary'}
          >
            {rank.points}{' '}
            <Typography variant='inherit' component='span' color='text.secondary'>
              {getAchievementPointSymbol()}
            </Typography>
          </Typography>
        </span>
      </span>
    </div>
  );
};
