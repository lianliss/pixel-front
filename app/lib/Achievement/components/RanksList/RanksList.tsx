import React, { useMemo } from 'react';

import styles from './styles.module.scss';
import { RankItem } from './RankItem/RankItem.tsx';
import {
  IAchievementRangDto,
  IUserAchievementRangDto,
} from 'lib/Achievement/api-server/achievement.types.ts';
import Skeleton from '@mui/material/Skeleton';
import ImageDialog from '@ui-kit/ImageDialog/ImageDialog.tsx';
import { getAchievementRangImage } from 'lib/Achievement/utils/getAchievementRangImage.ts';

type Props = {
  rangList: IAchievementRangDto[];
  userRank: IUserAchievementRangDto;
  isLoading: boolean;
};
export const RanksList = (props: Props) => {
  const { rangList, isLoading, userRank } = props;

  const [selectedRank, setSelectedRank] = React.useState<IAchievementRangDto | null>(null);

  const sorted = useMemo(() => {
    if (!userRank) {
      return rangList;
    }

    return rangList.sort((a, b) => {
      const isDone = userRank.level > b.level;

      return isDone ? -1 : 1;
    });
  }, [rangList, userRank]);

  return (
    <div className={styles.ranks}>
      {isLoading ? (
        <>
          {Array.from({ length: 13 }).map((_, index) => (
            <Skeleton variant='rectangular' component='div' />
          ))}
        </>
      ) : (
        <>
          {sorted.map((item) => (
            <RankItem
              key={`achievement-rang-${item.id}`}
              rank={item}
              userRank={userRank}
              onSelect={(el) => setSelectedRank(el)}
            />
          ))}
        </>
      )}

      <ImageDialog
        image={selectedRank ? getAchievementRangImage(selectedRank.level) : ''}
        open={!!selectedRank}
        onClose={() => setSelectedRank(null)}
      />
    </div>
  );
};
