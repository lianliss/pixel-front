import React, { useMemo } from 'react';
import { AchievementsItem } from 'lib/Achievement/components/AchievementsItem/AchievementsItem.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import styles from './AchievementsList.module.scss';
import Stack from '@mui/material/Stack';
import { AchievementDtoWithStatus } from 'lib/Achievement/pages/achievements/achievement.page.tsx';
import { IAchievementDto } from 'lib/Achievement/api-server/achievement.types.ts';
import { useTranslation } from 'react-i18next';

interface AchievementsProps {
  title: string;
  achievements: AchievementDtoWithStatus[];
  accountAddress: string;
  filterByCollection: string;
  setCollectionFilter: (collection: string) => void;
  onSelect?: (el: IAchievementDto) => void;
  onClaim?: (id: string) => void;
  loading?: boolean;
}

export const AchievementsList: React.FC<AchievementsProps> = ({
  title,
  achievements,
  accountAddress,
  filterByCollection,
  setCollectionFilter,
  onSelect,
  onClaim,
  loading,
}) => {
  const { t } = useTranslation('achievements');

  const filteredAchievements = useMemo(() => {
    return filterByCollection
      ? achievements.filter((item) => filterByCollection === item.category)
      : achievements;
  }, [achievements, filterByCollection]);

  return (
    <Stack direction='column' gap={1}>
      <Typography color='text.primary'>{t(title)}</Typography>

      <div className={styles.achievements__list}>
        {filteredAchievements.map((option: AchievementDtoWithStatus) => (
          <AchievementsItem
            key={`ach-${option.id}`}
            data={option as AchievementDtoWithStatus}
            accountAddress={accountAddress}
            setCollectionFilter={setCollectionFilter}
            isCollectionFilter={!!filterByCollection}
            onSelect={onSelect}
            onClaim={onClaim}
            loading={loading}
          />
        ))}
      </div>
    </Stack>
  );
};
