import React from 'react';
import { AchievementsList } from 'lib/Achievement/components/AchievementsList/AchievementsList.tsx';
import Stack from '@mui/material/Stack';
import { AchievementsResponse } from 'lib/Achievement/pages/achievements/achievement.page.tsx';
import { IAchievementDto } from 'lib/Achievement/api-server/achievement.types.ts';
import ImageDialog from '@ui-kit/ImageDialog/ImageDialog.tsx';

import styles from './AchievementsList.module.scss';
import { useTranslation } from 'react-i18next';

type Props = {
  options: AchievementsResponse;
  accountAddress: string;
  onClaim?: (id: string) => void;
};

export const AchievementsLists = ({ accountAddress, options = [], onClaim }: Props) => {
  const { t } = useTranslation('achievements');
  const [collection, setCollection] = React.useState<string | null>(null);
  const [selectedAchievement, setSelectedAchievement] = React.useState<IAchievementDto | null>(
    null
  );

  const setCollectionFilter = (nextCollection: string) => {
    if (collection) {
      setCollection(null);
    } else {
      setCollection(nextCollection);
    }
  };

  return (
    <>
      <Stack direction='column' gap={3} className={styles.achievements__lists}>
        {!!options.availableForClaimAchievements.length && (
          <AchievementsList
            filterByCollection={collection}
            setCollectionFilter={setCollectionFilter}
            title='Forward to the victory'
            achievements={options.availableForClaimAchievements}
            accountAddress={accountAddress}
            onClaim={onClaim}
          />
        )}
        {!!options.processingAchievements.length && (
          <AchievementsList
            filterByCollection={collection}
            setCollectionFilter={setCollectionFilter}
            title={t('In progress')}
            achievements={options.processingAchievements}
            accountAddress={accountAddress}
            onSelect={(el) => setSelectedAchievement(el)}
            onClaim={onClaim}
          />
        )}
        {!!options.noopAchievements.length && (
          <AchievementsList
            filterByCollection={collection}
            setCollectionFilter={setCollectionFilter}
            title={t('Not started')}
            achievements={options.noopAchievements}
            accountAddress={accountAddress}
            onSelect={(el) => setSelectedAchievement(el)}
            onClaim={onClaim}
          />
        )}
        {!!options.claimedAchievements.length && (
          <AchievementsList
            filterByCollection={collection}
            setCollectionFilter={setCollectionFilter}
            title={t('Completed')}
            achievements={options.claimedAchievements}
            accountAddress={accountAddress}
            onSelect={(el) => setSelectedAchievement(el)}
            onClaim={onClaim}
          />
        )}
      </Stack>
      <ImageDialog
        image={selectedAchievement?.image || ''}
        open={!!selectedAchievement}
        onClose={() => setSelectedAchievement(null)}
      />
    </>
  );
};
