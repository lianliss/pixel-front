import { IPaginatedResult } from 'lib/Quests/api-server/quests.types.ts';

export enum AchievementGroup {
  Default = 'Default',
  Units = 'Units',
}

// request

export type IGetUserAchievementRangDto = { group: AchievementGroup } & { userAddress: string };

export type IFindUserAchievementDto = { group: AchievementGroup } & { userAddress: string };

export type IFindAchievementRangDto = { group: AchievementGroup } & { userAddress: string };

export type IClaimUserAchievementRangDto = { group: AchievementGroup } & {
  userAddress: string;
  achievementId: string;
};

// query response

export type IGetAchievementResponse = IAchievementDto;

export type IFindAchievementsResponse = IPaginatedResult<IAchievementDto>;

export type IGetAchievementRangListResponse = IPaginatedResult<IAchievementRangDto>;

export type IGetUserAchievementRangResponse = IUserAchievementRangDto;

// command response

export type IClaimUserAchievementRangResponse = IUserAchievementRangDto;

// dto

export type IAchievementDto<TDate = number> = Omit<
  IAchievement<TDate>,
  'tasks' | 'userAchievements'
> & {
  tasks?: IAchievementTaskDto<TDate>[];
  userAchievement?: IUserAchievementDto<TDate>;
};

export type IAchievementTaskDto<TDate = number> = Omit<IAchievementTask<TDate>, 'achievementId'>;

export type IUserAchievementDto<TDate = number> = Omit<
  IUserAchievement<TDate>,
  'achievementId' | 'userId' | 'context' | 'achievement' | 'updatedAt'
>;

export type IAchievementRangDto<TDate = number> = IAchievementRang<TDate>;

export type IUserAchievementRangDto<TDate = number> = IUserAchievementRang<TDate>;

// entities

export type IAchievementTask<TDate = Date> = {
  id: string;

  achievementId: string;

  level: number;
  value: number;
  points: number;

  createdAt: TDate;
};

export type IUserAchievement<TDate = Date> = {
  id: string;

  userId: string;
  achievementId: string;

  level: number;
  value: number;

  updatedAt: TDate;
  createdAt: TDate;

  // relations
  achievement?: IAchievement<TDate>;
};

export type IAchievement<TDate = Date> = {
  id: string;

  name: string;
  image?: string;
  type: AchievementType;
  period: AchievementPeriod;
  category: AchievementCategory;
  summary: string;

  disabled: boolean;

  createdAt: TDate;

  // relations
  userAchievements?: IUserAchievement<TDate>[];
  tasks?: IAchievementTask<TDate>[];
};

export type IAchievementRang<TDate = Date> = {
  id: string;

  name: string;
  level: number;
  points: number;

  createdAt: TDate;
};

export type IUserAchievementRang<TDate = Date> = {
  id: string;

  level: number;
  points: number;
  userId: string;

  createdAt: TDate;
};

// enums

export enum AchievementType {
  Transactions = 'Transactions',
  ReceivedWeeklyChests = 'ReceivedWeeklyChests',
  DrillLevel = 'DrillLevel',
  StorageLevel = 'StorageLevel',
  CompletedQuests = 'CompletedQuests',
  CompletedPartnersQuests = 'CompletedPartnersQuests',
  NewReferralsCount = 'NewReferralsCount',
  ReferralRewards = 'ReferralRewards',
  ClaimedTokens = 'ClaimedTokens',
  BurnedTokens = 'BurnedTokens',
  PaidCoinAmount = 'PaidCoinAmount',
  PaidMarketCoinAmount = 'PaidMarketCoinAmount',
  ReceivedMarketCoinAmount = 'ReceivedMarketCoinAmount',
  PaidTokenChests = 'PaidTokenChests',
  PaidCoinChests = 'PaidCoinChests',
}

export enum AchievementPeriod {
  Full = 'Full',
  Week = 'Week',
  Month = 'Month',
  Day = 'Day',
}

export enum AchievementCategory {
  defi = 'defi',
  trade = 'trade',
  activity = 'activity',
}
