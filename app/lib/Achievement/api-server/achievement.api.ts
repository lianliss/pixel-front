'use strict';

import { AxiosResponse } from 'axios';
import {
  IClaimUserAchievementRangDto,
  IClaimUserAchievementRangResponse,
  IFindAchievementRangDto,
  IFindAchievementsResponse,
  IFindUserAchievementDto,
  IGetAchievementRangListResponse,
  IGetUserAchievementRangDto,
  IGetUserAchievementRangResponse,
} from 'lib/Achievement/api-server/achievement.types.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class AchievementApi {
  url: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async rangList({
    userAddress,
    ...params
  }: IFindAchievementRangDto): Promise<IGetAchievementRangListResponse> {
    const response = await axiosInstance.get<{ data: IGetAchievementRangListResponse }>(
      `/api/v1/achievement/rang`,
      {
        baseURL: this.url,
        params,
      }
    );

    return response.data.data;
  }

  async userRang({
    userAddress,
    ...params
  }: IGetUserAchievementRangDto): Promise<IGetUserAchievementRangResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetUserAchievementRangResponse },
      AxiosResponse<{ data: IGetUserAchievementRangResponse }>
    >(`/api/v1/achievement/user/rang`, {
      baseURL: this.url,
      params: {
        ...params,
        userAddress: userAddress.toLowerCase(),
      },
    });

    return data.data;
  }

  async find({
    userAddress,
    ...params
  }: IFindUserAchievementDto): Promise<IFindAchievementsResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindAchievementsResponse },
      AxiosResponse<{ data: IFindAchievementsResponse }>
    >(`/api/v1/achievement/user/find`, {
      baseURL: this.url,
      params,
    });

    return data.data;
  }

  async claim({
    achievementId,
    userAddress,
    ...params
  }: IClaimUserAchievementRangDto): Promise<IClaimUserAchievementRangResponse> {
    const { data } = await axiosInstance.post<
      { data: IClaimUserAchievementRangResponse },
      AxiosResponse<{ data: IClaimUserAchievementRangResponse }>
    >(`/api/v1/achievement/${achievementId}/claim`, params, {
      baseURL: this.url,
    });

    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const achievementApi = new AchievementApi({ url: 'https://api.hellopixel.network' });
