import { createEffect, createStore } from 'effector';
import { achievementApi } from 'lib/Achievement/api-server/achievement.api.ts';
import {
  IAchievementDto,
  IAchievementRangDto,
  IClaimUserAchievementRangDto,
  IFindAchievementRangDto,
  IFindUserAchievementDto,
  IGetUserAchievementRangDto,
  IUserAchievementRangDto,
} from 'lib/Achievement/api-server/achievement.types.ts';
import { AxiosError } from 'axios';
import toaster from 'services/toaster.tsx';

// achievements list

export const loadAchievements = createEffect(async (params: IFindUserAchievementDto) => {
  const res = await achievementApi.find(params);
  return res?.rows ?? [];
});

export const achievementsStore = createStore<IAchievementDto[]>([]).on(
  loadAchievements.doneData,
  (prev, next) => next
);

// rang

export const loadAchievementRangList = createEffect(async (params: IFindAchievementRangDto) => {
  const res = await achievementApi.rangList(params);
  return res?.rows ?? [];
});

export const achievementsRangListStore = createStore<IAchievementRangDto[]>([]).on(
  loadAchievementRangList.doneData,
  (prev, next) => next
);

export const loadAchievementUserRang = createEffect(async (params: IGetUserAchievementRangDto) => {
  const res = await achievementApi.userRang(params);
  return res;
});

export const achievementUserRangStore = createStore<{
  data: IUserAchievementRangDto | null;
  loading: boolean;
}>({ data: null, loading: true })
  .on(loadAchievementUserRang.doneData, (prev, next) => ({ ...prev, data: next }))
  .on(loadAchievementUserRang.pending, (prev, loading) => ({ ...prev, loading }));

// claim

export const claimAchievement = createEffect(async (params: IClaimUserAchievementRangDto) => {
  const res = await achievementApi.claim(params);
  return res;
});

claimAchievement.fail.watch(({ error }) => {
  const err = (error as AxiosError).response?.data.error?.message;
  console.error((error as AxiosError).response?.data);
  toaster.show({ message: err || 'Failed to claim achievement', intent: 'danger' });
});
