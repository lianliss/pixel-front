import React from 'react';
import AbProvider from 'lib/ab/context/ab.provider.tsx';
import AchievementPage from './achievement.page.tsx';

export default function () {
  return (
    <AbProvider>
      <AchievementPage />
    </AbProvider>
  );
}
