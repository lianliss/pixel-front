import React, { useEffect, useMemo } from 'react';

import styles from './achievements.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { RanksList } from 'lib/Achievement/components/RanksList/RanksList.tsx';
import LinearProgress from '@mui/material/LinearProgress';
import { AchievementsLists } from 'lib/Achievement/components/AchievementsList/AchievementsLists.tsx';
import { useAb } from 'lib/ab/context/ab.context.ts';
import { TechnicalWorks } from 'ui/TechnicalWorks/TechnicalWorks.tsx';
import {
  getAchievementPointSymbol,
  getAchievementStatus,
  sortAchievements,
} from 'lib/Achievement/utils/utils.ts';
import { Tabs } from '@ui-kit/Tabs/Tabs.tsx';
import { Tab } from '@ui-kit/Tab/Tab.tsx';
import { AchievementsStatus } from 'lib/Achievement/components/AchievementsItem/AchievementsItem.tsx';
import { AchievementGroup, IAchievementDto } from 'lib/Achievement/api-server/achievement.types.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import HintPage from '@ui-kit/HintPage/HintPage.tsx';
import { useTranslation } from 'react-i18next';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import { useUserAchievementRang } from 'lib/Achievement/hooks/useUserAchievementRang.ts';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import { Chain } from 'services/multichain/chains';
import { useUserAchievementRangList } from 'lib/Achievement/hooks/useUserAchievementRangList.ts';
import { getDefaultChainImage } from '@cfg/image.ts';
import Paper from '@mui/material/Paper';
import { alpha, styled } from '@mui/material/styles';
import NotAvailableChain from '../../../../shared/blocks/NotAvailableChain/NotAvailableChain.tsx';
import { useUserAchievements } from 'lib/Achievement/hooks/useUserAchievements.ts';
import { useClaimAchievement } from 'lib/Achievement/hooks/useClaimAchievement.ts';
import Container from '@mui/material/Container';

const StyledList = styled(Paper)(({ theme }) => ({
  backgroundColor: alpha(theme.palette.background.paper, 0.7),
}));

export type AchievementDtoWithStatus = IAchievementDto & { status: AchievementsStatus };

// Тип для возвращаемого значения useMemo
export interface AchievementsResponse {
  availableForClaimAchievements: AchievementDtoWithStatus[];
  claimedAchievements: AchievementDtoWithStatus[];
  processingAchievements: AchievementDtoWithStatus[];
  noopAchievements: AchievementDtoWithStatus[];
}

export enum TabAchievementsStatusEnum {
  all = 'all',
  claim = 'claim',
  inProgress = 'inProgress',
  completed = 'completed',
}

const ACHIEVEMENT_GROUP = {
  [Chain.UNITS]: AchievementGroup.Units,
  [Chain.SONGBIRD]: AchievementGroup.Default,
};

function AchievementPage() {
  const ab = useAb();
  const account = useAccount();

  const { t } = useTranslation('achievements');

  const achievementGroup = ACHIEVEMENT_GROUP[account.chainId];

  const achievements = useUserAchievements({
    accountAddress: account.accountAddress,
    group: achievementGroup,
  });
  const achievementUserRang = useUserAchievementRang(account.accountAddress, {
    group: achievementGroup,
  });
  const achievementsRangList = useUserAchievementRangList({
    group: achievementGroup,
  });
  const claimState = useClaimAchievement({
    group: achievementGroup,
    accountAddress: account.accountAddress,
  });

  // const accountAddress = '0x4B9Ac70305761c8f1c4e88AD6C54499b15674a07';

  const [tab, setTab] = React.useState<TabAchievementsStatusEnum>(TabAchievementsStatusEnum.all);

  useBackAction('/wallet');

  const userRank = achievementsRangList.data?.find(
    (rank) => rank.level === achievementUserRang.data?.level
  );

  const {
    availableForClaimAchievements,
    claimedAchievements,
    processingAchievements,
    noopAchievements,
  }: AchievementsResponse = useMemo<AchievementsResponse>(() => {
    // Получаем статус для каждой ачивки и добавляем его в массив ачивок
    const items: AchievementDtoWithStatus[] = achievements.data.map((item) => ({
      ...item,
      status: getAchievementStatus(item),
    }));

    // Сортируем все ачивки сразу
    const sortedAchievements = items.sort(sortAchievements);

    // Фильтруем отсортированные ачивки по их статусу
    return {
      availableForClaimAchievements: sortedAchievements.filter(
        (item) => item.status === AchievementsStatus.availableForClaim
      ),
      claimedAchievements: sortedAchievements.filter(
        (item) => item.status === AchievementsStatus.claimed
      ),
      processingAchievements: sortedAchievements.filter(
        (item) => item.status === AchievementsStatus.processing
      ),
      noopAchievements: sortedAchievements.filter(
        (item) => item.status === AchievementsStatus.noop
      ),
    };
  }, [achievements.data, tab]);

  const isAll = tab === TabAchievementsStatusEnum.all;
  const isClaim = tab === TabAchievementsStatusEnum.claim;
  const isCompleted = tab === TabAchievementsStatusEnum.completed;
  const isInProgress = tab === TabAchievementsStatusEnum.inProgress;

  const achievementsGroups = {
    availableForClaimAchievements: isAll || isClaim ? availableForClaimAchievements : [],
    claimedAchievements: isAll || isCompleted ? claimedAchievements : [],
    processingAchievements: isAll || isInProgress ? processingAchievements : [],
    noopAchievements: isAll ? noopAchievements : [],
  };

  if (!achievementGroup) {
    return (
      <NotAvailableChain
        title={t('Not Available')}
        summary={t('Achievements not available on current network')}
        chainId={Chain.SONGBIRD}
      />
    );
  }

  return (
    <div
      className={styles.screen}
      style={{ backgroundImage: `url(${getDefaultChainImage(account.chainId)})` }}
    >
      <Container maxWidth='xs'>
        <ChainSwitcher chains={[Chain.SONGBIRD, Chain.UNITS]} />

        {ab.isEnabled('achievement_tech_works') && <TechnicalWorks />}

        <div className={styles.toolbar}>
          <HintPage />
          <Typography variant='h2' fontWeight='bold' align='center' color='text.primary'>
            {t('Achievements')}
          </Typography>
        </div>

        <div className={styles.loading__wrapper}>
          {(achievementsRangList.loading || achievementUserRang.loading) && <LinearProgress />}
        </div>

        <div className={styles.rank__aria}>
          <div className={styles.heading}>
            <div className={styles.user__rank}>
              <Typography className={styles.rank__label} color='text.primary'>
                {t('Rank')}
              </Typography>
              {userRank ? (
                <Typography className={styles.rank__title} color='text.primary'>
                  {userRank.name}
                </Typography>
              ) : (
                <Typography className={styles.rank__title} color='text.primary'>
                  {t('Get your first rank')}
                </Typography>
              )}
            </div>
            <div className={styles.rank__points}>
              <Typography
                className={styles.points}
                variant='h6'
                fontWeight='bold'
                color='success.main'
              >
                {achievementUserRang.data?.points ?? 0} <span>{getAchievementPointSymbol()}</span>
              </Typography>
            </div>
          </div>

          <RanksList
            rangList={achievementsRangList.data}
            userRank={achievementUserRang.data}
            isLoading={achievementsRangList.loading || achievementUserRang.loading}
          />
        </div>

        <StyledList variant='outlined' elevation={0} className={styles.achievements__aria}>
          <div className={styles.items__tabs}>
            <Tabs
              onChange={(_, v) => setTab(v as TabAchievementsStatusEnum)}
              value={tab}
              variant='scrollable'
              scrollButtons='auto'
            >
              <Tab
                id={TabAchievementsStatusEnum.all}
                label={t('All')}
                value={TabAchievementsStatusEnum.all}
                count={achievements.data.length || undefined}
              />
              <Tab
                id={TabAchievementsStatusEnum.claim}
                label={t('Claim')}
                value={TabAchievementsStatusEnum.claim}
                count={availableForClaimAchievements.length || undefined}
              />
              <Tab
                id={TabAchievementsStatusEnum.inProgress}
                label={t('In Progress')}
                value={TabAchievementsStatusEnum.inProgress}
                count={processingAchievements.length || undefined}
              />
              <Tab
                id={TabAchievementsStatusEnum.completed}
                label={t('Completed')}
                value={TabAchievementsStatusEnum.completed}
                count={claimedAchievements.length || undefined}
              />
            </Tabs>
          </div>

          <AchievementsLists
            options={achievementsGroups}
            accountAddress={account.accountAddress}
            onClaim={claimState.claim}
            loading={claimState.loading}
          />
        </StyledList>
      </Container>
    </div>
  );
}

export default AchievementPage;
