export const CHEST_V2_ABI = [
  {
    inputs: [
      {
        internalType: 'address',
        name: 'coreAddress',
        type: 'address',
      },
      {
        internalType: 'address',
        name: 'nftAddress',
        type: 'address',
      },
      {
        internalType: 'address',
        name: 'poolAddress',
        type: 'address',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'constructor',
  },
  {
    inputs: [],
    name: 'AccessControlBadConfirmation',
    type: 'error',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        internalType: 'bytes32',
        name: 'neededRole',
        type: 'bytes32',
      },
    ],
    name: 'AccessControlUnauthorizedAccount',
    type: 'error',
  },
  {
    inputs: [
      {
        internalType: 'bytes',
        name: 'data',
        type: 'bytes',
      },
    ],
    name: 'DelegateCallFailed',
    type: 'error',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'dropIndex',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'ratio',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'bool',
        name: 'isToken',
        type: 'bool',
      },
      {
        indexed: false,
        internalType: 'bool',
        name: 'isShard',
        type: 'bool',
      },
      {
        indexed: false,
        internalType: 'address',
        name: 'tokenAddress',
        type: 'address',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'amount',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'maxAmount',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256[]',
        name: 'items',
        type: 'uint256[]',
      },
    ],
    name: 'ChestDropAdded',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'count',
        type: 'uint256',
      },
    ],
    name: 'ChestDropCleared',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'dropIndex',
        type: 'uint256',
      },
    ],
    name: 'ChestDropDeleted',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'dropIndex',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256[]',
        name: 'items',
        type: 'uint256[]',
      },
    ],
    name: 'ChestDropItemsAdded',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'dropIndex',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'count',
        type: 'uint256',
      },
    ],
    name: 'ChestDropItemsCleared',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'dropIndex',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'ratio',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'bool',
        name: 'isToken',
        type: 'bool',
      },
      {
        indexed: false,
        internalType: 'bool',
        name: 'isShard',
        type: 'bool',
      },
      {
        indexed: false,
        internalType: 'address',
        name: 'tokenAddress',
        type: 'address',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'amount',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'maxAmount',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256[]',
        name: 'items',
        type: 'uint256[]',
      },
    ],
    name: 'ChestDropUpdated',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'string',
        name: 'title',
        type: 'string',
      },
      {
        indexed: false,
        internalType: 'string',
        name: 'imageURI',
        type: 'string',
      },
    ],
    name: 'CreateChest',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        indexed: true,
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
    ],
    name: 'DecreaseUserChest',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        indexed: true,
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
    ],
    name: 'IncreaseUserChest',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        indexed: true,
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        indexed: true,
        internalType: 'uint256',
        name: 'typeId',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'tokenId',
        type: 'uint256',
      },
    ],
    name: 'ItemDrop',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'previousAdminRole',
        type: 'bytes32',
      },
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'newAdminRole',
        type: 'bytes32',
      },
    ],
    name: 'RoleAdminChanged',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'sender',
        type: 'address',
      },
    ],
    name: 'RoleGranted',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'sender',
        type: 'address',
      },
    ],
    name: 'RoleRevoked',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        indexed: true,
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
    ],
    name: 'SendChest',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        indexed: true,
        internalType: 'uint64',
        name: 'userId',
        type: 'uint64',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'token',
        type: 'address',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'amount',
        type: 'uint256',
      },
    ],
    name: 'TokenDrop',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'string',
        name: 'title',
        type: 'string',
      },
      {
        indexed: false,
        internalType: 'string',
        name: 'imageURI',
        type: 'string',
      },
    ],
    name: 'UpdateChest',
    type: 'event',
  },
  {
    inputs: [],
    name: 'DEFAULT_ADMIN_ROLE',
    outputs: [
      {
        internalType: 'bytes32',
        name: '',
        type: 'bytes32',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'EDITOR_ROLE',
    outputs: [
      {
        internalType: 'bytes32',
        name: '',
        type: 'bytes32',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'MINTER_ROLE',
    outputs: [
      {
        internalType: 'bytes32',
        name: '',
        type: 'bytes32',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'PROXY_ROLE',
    outputs: [
      {
        internalType: 'bytes32',
        name: '',
        type: 'bytes32',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'rarity',
        type: 'uint256',
      },
    ],
    name: '_getChance',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
    ],
    name: '_rollRarity',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        components: [
          {
            internalType: 'uint256',
            name: 'ratio',
            type: 'uint256',
          },
          {
            internalType: 'bool',
            name: 'isToken',
            type: 'bool',
          },
          {
            internalType: 'bool',
            name: 'isShard',
            type: 'bool',
          },
          {
            internalType: 'uint256[]',
            name: 'items',
            type: 'uint256[]',
          },
          {
            internalType: 'address',
            name: 'tokenAddress',
            type: 'address',
          },
          {
            internalType: 'uint256',
            name: 'amount',
            type: 'uint256',
          },
          {
            internalType: 'uint256',
            name: 'maxAmount',
            type: 'uint256',
          },
        ],
        internalType: 'struct Reward',
        name: 'drop',
        type: 'tuple',
      },
    ],
    name: 'addChestDrop',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'dropIndex',
        type: 'uint256',
      },
      {
        internalType: 'uint256[]',
        name: 'items',
        type: 'uint256[]',
      },
    ],
    name: 'addChestDropItems',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
    ],
    name: 'claimFreeChest',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'string',
        name: 'title',
        type: 'string',
      },
      {
        internalType: 'string',
        name: 'imageURI',
        type: 'string',
      },
    ],
    name: 'createChest',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'string',
        name: 'method',
        type: 'string',
      },
      {
        internalType: 'bytes',
        name: 'callData',
        type: 'bytes',
      },
    ],
    name: 'delegate',
    outputs: [
      {
        internalType: 'bool',
        name: '',
        type: 'bool',
      },
      {
        internalType: 'bytes',
        name: '',
        type: 'bytes',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'dropIndex',
        type: 'uint256',
      },
    ],
    name: 'deleteChestDrop',
    outputs: [
      {
        components: [
          {
            internalType: 'uint256',
            name: 'ratio',
            type: 'uint256',
          },
          {
            internalType: 'bool',
            name: 'isToken',
            type: 'bool',
          },
          {
            internalType: 'bool',
            name: 'isShard',
            type: 'bool',
          },
          {
            internalType: 'uint256[]',
            name: 'items',
            type: 'uint256[]',
          },
          {
            internalType: 'address',
            name: 'tokenAddress',
            type: 'address',
          },
          {
            internalType: 'uint256',
            name: 'amount',
            type: 'uint256',
          },
          {
            internalType: 'uint256',
            name: 'maxAmount',
            type: 'uint256',
          },
        ],
        internalType: 'struct Reward',
        name: '',
        type: 'tuple',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
    ],
    name: 'deleteChestDrop',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'dropIndex',
        type: 'uint256',
      },
    ],
    name: 'deleteChestDropItems',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
    ],
    name: 'getChest',
    outputs: [
      {
        components: [
          {
            internalType: 'uint256',
            name: 'chestId',
            type: 'uint256',
          },
          {
            internalType: 'string',
            name: 'title',
            type: 'string',
          },
          {
            internalType: 'string',
            name: 'imageURI',
            type: 'string',
          },
          {
            components: [
              {
                internalType: 'uint256',
                name: 'ratio',
                type: 'uint256',
              },
              {
                internalType: 'bool',
                name: 'isToken',
                type: 'bool',
              },
              {
                internalType: 'bool',
                name: 'isShard',
                type: 'bool',
              },
              {
                internalType: 'uint256[]',
                name: 'items',
                type: 'uint256[]',
              },
              {
                internalType: 'address',
                name: 'tokenAddress',
                type: 'address',
              },
              {
                internalType: 'uint256',
                name: 'amount',
                type: 'uint256',
              },
              {
                internalType: 'uint256',
                name: 'maxAmount',
                type: 'uint256',
              },
            ],
            internalType: 'struct Reward[]',
            name: 'drop',
            type: 'tuple[]',
          },
        ],
        internalType: 'struct Chest',
        name: '',
        type: 'tuple',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'dropIndex',
        type: 'uint256',
      },
    ],
    name: 'getChestDrop',
    outputs: [
      {
        components: [
          {
            internalType: 'uint256',
            name: 'ratio',
            type: 'uint256',
          },
          {
            internalType: 'bool',
            name: 'isToken',
            type: 'bool',
          },
          {
            internalType: 'bool',
            name: 'isShard',
            type: 'bool',
          },
          {
            internalType: 'uint256[]',
            name: 'items',
            type: 'uint256[]',
          },
          {
            internalType: 'address',
            name: 'tokenAddress',
            type: 'address',
          },
          {
            internalType: 'uint256',
            name: 'amount',
            type: 'uint256',
          },
          {
            internalType: 'uint256',
            name: 'maxAmount',
            type: 'uint256',
          },
        ],
        internalType: 'struct Reward',
        name: '',
        type: 'tuple',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
    ],
    name: 'getChestDropsCount',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'offset',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'limit',
        type: 'uint256',
      },
    ],
    name: 'getChests',
    outputs: [
      {
        components: [
          {
            internalType: 'uint256',
            name: 'chestId',
            type: 'uint256',
          },
          {
            internalType: 'string',
            name: 'title',
            type: 'string',
          },
          {
            internalType: 'string',
            name: 'imageURI',
            type: 'string',
          },
          {
            components: [
              {
                internalType: 'uint256',
                name: 'ratio',
                type: 'uint256',
              },
              {
                internalType: 'bool',
                name: 'isToken',
                type: 'bool',
              },
              {
                internalType: 'bool',
                name: 'isShard',
                type: 'bool',
              },
              {
                internalType: 'uint256[]',
                name: 'items',
                type: 'uint256[]',
              },
              {
                internalType: 'address',
                name: 'tokenAddress',
                type: 'address',
              },
              {
                internalType: 'uint256',
                name: 'amount',
                type: 'uint256',
              },
              {
                internalType: 'uint256',
                name: 'maxAmount',
                type: 'uint256',
              },
            ],
            internalType: 'struct Reward[]',
            name: 'drop',
            type: 'tuple[]',
          },
        ],
        internalType: 'struct Chest[]',
        name: '',
        type: 'tuple[]',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'getChests',
    outputs: [
      {
        components: [
          {
            internalType: 'uint256',
            name: 'chestId',
            type: 'uint256',
          },
          {
            internalType: 'string',
            name: 'title',
            type: 'string',
          },
          {
            internalType: 'string',
            name: 'imageURI',
            type: 'string',
          },
          {
            components: [
              {
                internalType: 'uint256',
                name: 'ratio',
                type: 'uint256',
              },
              {
                internalType: 'bool',
                name: 'isToken',
                type: 'bool',
              },
              {
                internalType: 'bool',
                name: 'isShard',
                type: 'bool',
              },
              {
                internalType: 'uint256[]',
                name: 'items',
                type: 'uint256[]',
              },
              {
                internalType: 'address',
                name: 'tokenAddress',
                type: 'address',
              },
              {
                internalType: 'uint256',
                name: 'amount',
                type: 'uint256',
              },
              {
                internalType: 'uint256',
                name: 'maxAmount',
                type: 'uint256',
              },
            ],
            internalType: 'struct Reward[]',
            name: 'drop',
            type: 'tuple[]',
          },
        ],
        internalType: 'struct Chest[]',
        name: '',
        type: 'tuple[]',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'getChestsCount',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
    ],
    name: 'getRoleAdmin',
    outputs: [
      {
        internalType: 'bytes32',
        name: '',
        type: 'bytes32',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        internalType: 'uint256',
        name: 'offset',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'limit',
        type: 'uint256',
      },
    ],
    name: 'getUserChests',
    outputs: [
      {
        components: [
          {
            internalType: 'uint256',
            name: 'chestId',
            type: 'uint256',
          },
          {
            internalType: 'uint256',
            name: 'amount',
            type: 'uint256',
          },
        ],
        internalType: 'struct UserChest[]',
        name: '',
        type: 'tuple[]',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'getUserChests',
    outputs: [
      {
        components: [
          {
            internalType: 'uint256',
            name: 'chestId',
            type: 'uint256',
          },
          {
            internalType: 'uint256',
            name: 'amount',
            type: 'uint256',
          },
        ],
        internalType: 'struct UserChest[]',
        name: '',
        type: 'tuple[]',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'getUserChestsCount',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        internalType: 'uint256',
        name: 'offset',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'limit',
        type: 'uint256',
      },
    ],
    name: 'getUserChestsWithData',
    outputs: [
      {
        components: [
          {
            internalType: 'uint256',
            name: 'chestId',
            type: 'uint256',
          },
          {
            internalType: 'uint256',
            name: 'amount',
            type: 'uint256',
          },
          {
            components: [
              {
                internalType: 'uint256',
                name: 'chestId',
                type: 'uint256',
              },
              {
                internalType: 'string',
                name: 'title',
                type: 'string',
              },
              {
                internalType: 'string',
                name: 'imageURI',
                type: 'string',
              },
              {
                components: [
                  {
                    internalType: 'uint256',
                    name: 'ratio',
                    type: 'uint256',
                  },
                  {
                    internalType: 'bool',
                    name: 'isToken',
                    type: 'bool',
                  },
                  {
                    internalType: 'bool',
                    name: 'isShard',
                    type: 'bool',
                  },
                  {
                    internalType: 'uint256[]',
                    name: 'items',
                    type: 'uint256[]',
                  },
                  {
                    internalType: 'address',
                    name: 'tokenAddress',
                    type: 'address',
                  },
                  {
                    internalType: 'uint256',
                    name: 'amount',
                    type: 'uint256',
                  },
                  {
                    internalType: 'uint256',
                    name: 'maxAmount',
                    type: 'uint256',
                  },
                ],
                internalType: 'struct Reward[]',
                name: 'drop',
                type: 'tuple[]',
              },
            ],
            internalType: 'struct Chest',
            name: 'chest',
            type: 'tuple',
          },
        ],
        internalType: 'struct UserChestData[]',
        name: '',
        type: 'tuple[]',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'getUserChestsWithData',
    outputs: [
      {
        components: [
          {
            internalType: 'uint256',
            name: 'chestId',
            type: 'uint256',
          },
          {
            internalType: 'uint256',
            name: 'amount',
            type: 'uint256',
          },
          {
            components: [
              {
                internalType: 'uint256',
                name: 'chestId',
                type: 'uint256',
              },
              {
                internalType: 'string',
                name: 'title',
                type: 'string',
              },
              {
                internalType: 'string',
                name: 'imageURI',
                type: 'string',
              },
              {
                components: [
                  {
                    internalType: 'uint256',
                    name: 'ratio',
                    type: 'uint256',
                  },
                  {
                    internalType: 'bool',
                    name: 'isToken',
                    type: 'bool',
                  },
                  {
                    internalType: 'bool',
                    name: 'isShard',
                    type: 'bool',
                  },
                  {
                    internalType: 'uint256[]',
                    name: 'items',
                    type: 'uint256[]',
                  },
                  {
                    internalType: 'address',
                    name: 'tokenAddress',
                    type: 'address',
                  },
                  {
                    internalType: 'uint256',
                    name: 'amount',
                    type: 'uint256',
                  },
                  {
                    internalType: 'uint256',
                    name: 'maxAmount',
                    type: 'uint256',
                  },
                ],
                internalType: 'struct Reward[]',
                name: 'drop',
                type: 'tuple[]',
              },
            ],
            internalType: 'struct Chest',
            name: 'chest',
            type: 'tuple',
          },
        ],
        internalType: 'struct UserChestData[]',
        name: '',
        type: 'tuple[]',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        internalType: 'address[]',
        name: 'accounts',
        type: 'address[]',
      },
    ],
    name: 'giveTheChest',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        internalType: 'uint64[]',
        name: 'userId',
        type: 'uint64[]',
      },
    ],
    name: 'giveTheChestToUser',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'grantRole',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'hasRole',
    outputs: [
      {
        internalType: 'bool',
        name: '',
        type: 'bool',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        internalType: 'address',
        name: 'callerConfirmation',
        type: 'address',
      },
    ],
    name: 'renounceRole',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'revokeRole',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'sendChest',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'newAddress',
        type: 'address',
      },
    ],
    name: 'setCoreAddress',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'newAddress',
        type: 'address',
      },
    ],
    name: 'setMultipool',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'newAddress',
        type: 'address',
      },
    ],
    name: 'setNFTAddress',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes4',
        name: 'interfaceId',
        type: 'bytes4',
      },
    ],
    name: 'supportsInterface',
    outputs: [
      {
        internalType: 'bool',
        name: '',
        type: 'bool',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        internalType: 'string',
        name: 'title',
        type: 'string',
      },
      {
        internalType: 'string',
        name: 'imageURI',
        type: 'string',
      },
    ],
    name: 'updateChest',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'chestId',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'dropIndex',
        type: 'uint256',
      },
      {
        components: [
          {
            internalType: 'uint256',
            name: 'ratio',
            type: 'uint256',
          },
          {
            internalType: 'bool',
            name: 'isToken',
            type: 'bool',
          },
          {
            internalType: 'bool',
            name: 'isShard',
            type: 'bool',
          },
          {
            internalType: 'uint256[]',
            name: 'items',
            type: 'uint256[]',
          },
          {
            internalType: 'address',
            name: 'tokenAddress',
            type: 'address',
          },
          {
            internalType: 'uint256',
            name: 'amount',
            type: 'uint256',
          },
          {
            internalType: 'uint256',
            name: 'maxAmount',
            type: 'uint256',
          },
        ],
        internalType: 'struct Reward',
        name: 'drop',
        type: 'tuple',
      },
    ],
    name: 'updateChestDrop',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
];
