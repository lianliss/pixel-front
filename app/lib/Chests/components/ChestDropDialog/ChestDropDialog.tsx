import React from 'react';
import styles from './ChestDropDialog.module.scss';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import { Portal } from '@blueprintjs/core';
import { Button } from '@ui-kit/Button/Button.tsx';
import { convertToNftItemDto } from 'lib/NFT/components/NFTCard/convertToNftItemDto.ts';
import { useTranslation } from 'react-i18next';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { IChestDropToken } from 'lib/Chests/hooks/drop/useDropChestV2.ts';
import { useToken } from '@chain/hooks/useToken.ts';
import Box from '@mui/material/Box';
import { Img } from '@ui-kit/Img/Img.tsx';
import { Backdrop } from '@mui/material';
import Container from '@mui/material/Container';
import { IWaitConfirmationsResult } from '@chain/hooks/useTransactionConfirmation.ts';
import Alert from '@mui/material/Alert';

type Props = {
  nftItem?: INftItemDto;
  token?: IChestDropToken;
  onClose?: () => void;
  confirmation?: IWaitConfirmationsResult;
};

function ChestDropDialog(props: Props) {
  const { nftItem, token, onClose, confirmation } = props;

  const { t } = useTranslation('common');

  const tokenData = useToken({
    address: token?.address,
    skip: !token?.address,
  });

  return (
    <Backdrop open sx={{ zIndex: 1500 }}>
      <div className={styles.drop}>
        <Container maxWidth='xs'>
          {nftItem && <NFTCard nft={nftItem} />}

          {token && (
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', gap: 0.5 }}>
              <Typography variant='h6' fontWeight='bold' align='center'>
                Received tokens
              </Typography>
              <Typography variant='h1' fontWeight='bold' align='center'>
                {token.amount} {tokenData.data?.symbol}
              </Typography>

              <Img src={tokenData.data?.image} alt='' sx={{ mx: 'auto', width: 50, mt: 1 }} />
            </Box>
          )}

          {/*{renderButtons()}*/}

          {confirmation?.isPending && (
            <Alert severity='warning' variant='outlined' sx={{ mb: 2 }}>
              Wait for Confirmations {confirmation.confirmations || ''}
            </Alert>
          )}

          <Button
            sx={{ mt: 2 }}
            onClick={onClose}
            className={styles.dropButton}
            size='large'
            fullWidth
            color='primary'
            variant='contained'
            // disabled={isClosing || isAction}
            // loading={isClosing}
          >
            {nftItem ? t('Put to Inventory') : t('Return to Inventory')}
          </Button>
          {/*<div className={styles.dropText}>{text.length ? text : <>&nbsp;</>}</div>*/}
        </Container>
      </div>
    </Backdrop>
  );
}

export default React.memo(ChestDropDialog);
