const baseEmitterConfig = (direction, position, { color, quantity, delay }) => {
  return {
    direction,
    rate: {
      quantity: quantity,
      delay: delay,
    },
    size: {
      width: 0,
      height: 0,
    },
    spawnColor: {
      value: color, //"#ffaa00",
      animation: {
        h: {
          enable: false,
          offset: {
            min: -1.4,
            max: 1.4,
          },
          speed: 0,
          sync: false,
        },
        l: {
          enable: true,
          offset: {
            min: 10,
            max: 60,
          },
          speed: 10,
          sync: false,
        },
      },
    },
    position,
  };
};

const config = ({
  color = '#ffaa00',
  number = 500,
  minSpeed = 5,
  maxSpeed = 40,
  maxSize = 3,
  quantity = 50,
  delay = 0.1,
}) => ({
  background: {
    color: 'transparent',
  },
  particles: {
    angle: {
      value: 0,
      offset: 10,
    },
    move: {
      enable: true,
      outModes: {
        top: 'none',
        default: 'destroy',
      },
      gravity: {
        enable: true,
      },
      speed: { min: minSpeed, max: maxSpeed },
      decay: 0.01,
    },
    number: {
      value: 0,
      limit: number,
    },
    opacity: {
      animation: {
        enable: true,
        speed: 0.1,
        sync: false,
        startValue: 'max',
        count: 1,
        destroy: 'min',
      },
      value: {
        min: 0,
        max: 1,
      },
    },
    shape: {
      type: ['circle'],
    },
    size: {
      value: { min: 1, max: maxSize },
      animation: {
        count: 1,
        startValue: 'min',
        enable: true,
        speed: 5,
        sync: true,
      },
    },
    rotate: {
      value: {
        min: 0,
        max: 360,
      },
      direction: 'random',
      animation: {
        enable: true,
        speed: 60,
      },
    },
    tilt: {
      direction: 'random',
      enable: false,
      value: {
        min: 0,
        max: 360,
      },
      animation: {
        enable: false,
        speed: 60,
      },
    },
    roll: {
      darken: {
        enable: true,
        value: 25,
      },
      enable: false,
      speed: {
        min: 15,
        max: 25,
      },
    },
    wobble: {
      distance: 30,
      enable: true,
      speed: {
        min: -15,
        max: 15,
      },
    },
  },
  emitters: [
    baseEmitterConfig(
      'top',
      { x: 50, y: 70 },
      {
        color,
        quantity,
        delay,
      }
    ),
  ],
});

export default config;
