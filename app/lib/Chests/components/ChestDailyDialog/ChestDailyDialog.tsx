import React, { useEffect, useState } from 'react';
import toaster from 'services/toaster.tsx';
import Paper from '@mui/material/Paper';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTranslation } from 'react-i18next';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { IUserChestDailyDto } from 'lib/Chests/api-server/chest-daily.types.ts';
import DailyBoxTask from 'lib/Chests/components/DailyBoxTask';
import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';
import {
  claimChestDaily,
  readNewsChestDaily,
  checQuestChestDaily,
  checkPartnerChestDaily,
} from 'lib/Chests/api-server/chest-daily.store.ts';
import { Link, useNavigate } from 'react-router-dom';
import { useSwitchChain } from '@chain/hooks/useSwitchChain.ts';
import { Chain } from 'services/multichain/chains';
import TelegramIcon from '@mui/icons-material/Telegram';
import SkaleIcon from '../../../../shared/icons/SkaleIcon.tsx';
import { openLink } from 'utils/open-link.ts';
import questTaskImage from 'assets/img/chests/quest-task.png';
import { useUnit } from 'effector-react';
import DailyBoxCardUi from '../DailyBoxCard/DailyBoxCard.ui.tsx';
import { toUTC } from 'utils/format/date';
import Alert from '@mui/material/Alert';
import Badge from '@mui/material/Badge';
import { styled } from '@mui/material/styles';
import { getStartOfNextDay } from 'lib/Chests/utils/date.ts';
import { IS_CHEST_DAILY_SATOSHI } from '@cfg/app.ts';

const StyledBadge = styled(Badge)(() => ({
  '& .MuiBadge-badge': { top: 12, right: 12 },
}));

const zarIcon = require('assets/img/token/skale/second-1.svg') as string;
const sgbIcon = 'https://s2.coinmarketcap.com/static/img/coins/64x64/12186.png';

type Props = {
  open: boolean;
  dailyChest?: IUserChestDailyDto;
  dailyChestLoading?: boolean;
  onClose?: () => void;
  onOpenRewards?: () => void;
  onClaim?: () => void;
  achievementRang?: number;
  achievementRangLoading?: boolean;
};

function ChestDailyDialog(props: Props) {
  const {
    open,
    onClose,
    dailyChest,
    onOpenRewards,
    onClaim,
    dailyChestLoading = false,
    achievementRang = 0,
    achievementRangLoading = false,
  } = props;

  const account = useAccount();
  const navigate = useNavigate();
  const { switchChain } = useSwitchChain();
  const claiming = useUnit(claimChestDaily.pending);
  const readNewsLoading = useUnit(readNewsChestDaily.pending);
  const checkPartnerLoading = useUnit(checkPartnerChestDaily.pending);

  const { t } = useTranslation('chest-daily');

  const [isCheck, setIsCheck] = useState(false);
  const completedQuests = dailyChest?.userQuestCounter?.completed || 0;
  const completed = !!dailyChest?.completedAt;
  const claimed = !!dailyChest?.claimedAt;
  const rewarded = !!dailyChest?.rewardedAt;
  const totalTasks = account.chainId === Chain.SKALE ? 2 : 3;
  const completedTasks = (
    account.chainId === Chain.SKALE
      ? [dailyChest?.isZarClaimed, dailyChest?.isSgbClaimed]
      : [
          dailyChest?.isNewsRead,
          dailyChest?.isSkaleClaimed,
          dailyChest?.isQuestCompleted || dailyChest?.isPartner1,
        ]
  ).filter(Boolean).length;
  const disabledByAchievement = !achievementRang;

  const nextRewardAt = dailyChest?.rewardedAt
    ? getStartOfNextDay(new Date(dailyChest.rewardedAt))
    : undefined;

  const handleClaim = async () => {
    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      await claimChestDaily({ userAddress: account.accountAddress, chainId: account.chainId });

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');

      onClaim?.();
    } catch (error) {
      toaster.captureException(error);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }
  };

  const handleReadNews = async () => {
    try {
      openLink(`https://t.me/hellopixelapp`);

      await readNewsChestDaily({ userAddress: account.accountAddress, chainId: account.chainId });
    } catch (e) {
      toaster.logError(e, '[read]');
    }
  };
  const handleCompleteQuest = async () => {
    if (completedQuests > 0) {
      try {
        await checQuestChestDaily({
          userAddress: account.accountAddress,
          chainId: account.chainId,
        });
      } catch (e) {
        toaster.logError(e, '[read]');
      }
    } else {
      onClose?.();
      navigate('/wallet/quests');
    }
  };
  const handleClaimSkale = async () => {
    try {
      onClose?.();
      await switchChain({ chainId: Chain.SKALE });
    } catch (e) {
      toaster.captureException(e);
    }
  };
  const handleClaimSgb = async () => {
    try {
      await switchChain({ chainId: Chain.SONGBIRD });
      navigate('/wallet/mining');
    } catch (e) {
      toaster.captureException(e);
    }
  };
  const handlePartner1 = async () => {
    openLink(`https://t.me/satoshi_game_bot/game?startapp=24931187`);
    setIsCheck(true);

    localStorage.setItem('chest-daily-partner1', 'yes');
  };
  const handlePartner1Check = async () => {
    const lastCheck = localStorage.getItem('chest-daily-partner1');

    if (!lastCheck) {
      return handlePartner1();
    }

    try {
      const chest = await checkPartnerChestDaily({
        userAddress: account.accountAddress,
        chainId: account.chainId,
      });

      if (chest.isPartner1) {
        toaster.success('Quest completed');
      } else {
        toaster.warning('Quest does not completed');
        setIsCheck(false);
      }

      localStorage.removeItem('chest-daily-partner1');
    } catch (e) {
      toaster.captureException(e);
    }
  };

  const isAchievementAlert = !achievementRangLoading && achievementRang === 0;
  const isWaitRewardAlert = claimed && !rewarded;
  const isRewardAlert = !!rewarded;
  const isQuestTask = !!completedQuests || !!dailyChest?.isQuestCompleted;

  return (
    <BottomDialog open={open} onClose={onClose}>
      <Typography fontWeight='bold' align='center'>
        {t('You Have Daily Task')}
      </Typography>

      <DailyBoxCardUi
        sx={{ mt: 2 }}
        current={completedTasks}
        total={totalTasks}
        onOpen={handleClaim}
        disabled={!dailyChest || completedTasks < totalTasks || rewarded || disabledByAchievement}
        nextRewardAt={nextRewardAt}
        loading={claiming || dailyChestLoading || readNewsLoading || achievementRangLoading}
        claimed={claimed}
        rewarded={rewarded}
        onOpenRewards={onOpenRewards}
      />

      {isAchievementAlert && (
        <Link to='/achievement'>
          <Alert severity='warning' variant='outlined' sx={{ mt: 2, textDecoration: 'underline' }}>
            {t('Reach Rank level 1 in the Achievement System to unlock access to the Mystery Box!')}
          </Alert>
        </Link>
      )}

      {/*{account.chainId === Chain.SONGBIRD &&*/}
      {/*  !completedQuests &&*/}
      {/*  !isRewardAlert &&*/}
      {/*  !isAchievementAlert &&*/}
      {/*  !isWaitRewardAlert &&*/}
      {/*  !dailyChest?.isQuestCompleted && (*/}
      {/*    <Alert severity='warning' variant='outlined' sx={{ mt: 2 }}>*/}
      {/*      {t('Complete quest or wait for new quests')}*/}
      {/*    </Alert>*/}
      {/*  )}*/}
      {isWaitRewardAlert && (
        <Alert severity='warning' variant='outlined' sx={{ mt: 2 }}>
          {t('The Mystery Box will be available in the inventory within a few minutes')}
        </Alert>
      )}
      {isRewardAlert && (
        <Alert
          severity='success'
          variant='outlined'
          sx={{ mt: 2 }}
          onClick={() => navigate('/inventory')}
        >
          Check your inventory
        </Alert>
      )}

      <Paper elevation={2} sx={{ mt: 2, borderColor: '#8527D480' }}>
        <Stack direction='column' divider={<Divider />}>
          {account.chainId === Chain.SONGBIRD && (
            <DailyBoxTask
              title={t('Read news')}
              icon={<TelegramIcon />}
              completed={!!dailyChest?.isNewsRead}
              onClick={handleReadNews}
              loading={dailyChestLoading}
            />
          )}
          {account.chainId === Chain.SONGBIRD && (IS_CHEST_DAILY_SATOSHI ? isQuestTask : true) && (
            <StyledBadge badgeContent={completedQuests} showZero color='primary'>
              <DailyBoxTask
                sx={{ width: '100%' }}
                title={t('Finished partner quest')}
                image={questTaskImage as string}
                completed={!!dailyChest?.isQuestCompleted}
                onClick={handleCompleteQuest}
                unAvailable={!isQuestTask}
                loading={dailyChestLoading}
              />
            </StyledBadge>
          )}
          {account.chainId === Chain.SONGBIRD &&
            (IS_CHEST_DAILY_SATOSHI ? !isQuestTask : false) && (
              <DailyBoxTask
                title={t('Mine for Watching in SATOSHI GAME to get reward')}
                image={questTaskImage as string}
                completed={!!dailyChest?.isPartner1}
                onClick={handlePartner1}
                onCheck={handlePartner1Check}
                loading={dailyChestLoading || checkPartnerLoading}
              />
            )}
          {account.chainId === Chain.SONGBIRD && (
            <DailyBoxTask
              title={t('Claim SKALE miner')}
              icon={<SkaleIcon sx={{ transform: 'scale(1.5)' }} />}
              completed={!!dailyChest?.isSkaleClaimed}
              onClick={handleClaimSkale}
              loading={dailyChestLoading}
            />
          )}

          {account.chainId === Chain.SKALE && (
            <DailyBoxTask
              title={t('Claim SGB miner')}
              image={sgbIcon}
              completed={!!dailyChest?.isSgbClaimed}
              onClick={handleClaimSgb}
              loading={dailyChestLoading}
            />
          )}
          {account.chainId === Chain.SKALE && (
            <DailyBoxTask
              title={t('Claim ZAR miner')}
              image={zarIcon}
              completed={!!dailyChest?.isZarClaimed}
              onClick={handleClaimSkale}
              loading={dailyChestLoading}
            />
          )}
        </Stack>
      </Paper>
    </BottomDialog>
  );
}

export default ChestDailyDialog;
