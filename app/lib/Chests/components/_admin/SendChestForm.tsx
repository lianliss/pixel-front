import React, { useMemo, useState } from 'react';
import Box, { BoxProps } from '@mui/material/Box';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { isAddress } from 'viem';

type Props = BoxProps & { onSend?: (userAddresses: string[]) => Promise<void>; loading?: boolean };

function SendChestForm(props: Props) {
  const { onSend, loading, ...other } = props;

  const [list, setList] = useState<string[]>([]);

  const handleSend = () => {
    onSend?.(list).then(() => setList([]));
  };

  const empty = !list.length;
  const invalidAddress = useMemo(() => {
    return list.filter((el) => !isAddress(el));
  }, [list]);
  const invalid = !!invalidAddress.length;

  return (
    <Box {...other}>
      <TextField
        multiline
        minRows={5}
        label='User addresses'
        placeholder='0x000..00,0x111..11,0x222..22'
        fullWidth
        value={list.join(',')}
        onChange={(e) => setList(e.target.value.split('\n').join(',').split(','))}
        error={invalid}
        helperText={invalid ? `${invalidAddress.join(',')} invalid` : undefined}
        disabled={loading}
      />

      <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', mt: 1 }}>
        <Button
          variant='contained'
          color='primary'
          size='large'
          onClick={handleSend}
          disabled={empty || invalid}
          loading={loading}
        >
          Give Chests
        </Button>
      </Box>
    </Box>
  );
}

export default SendChestForm;
