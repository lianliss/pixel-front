import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { IChainChest } from 'lib/Chests/hooks/useChests.ts';
import defaultImage from 'assets/img/chests/mystery-box.png';

type Props = {
  chest: IChainChest;
  onEdit?: (chest: IChainChest) => void;
  onClone?: (chest: IChainChest) => void;
};

function ChestCard(props: Props) {
  const { chest, onEdit, onClone } = props;

  return (
    <Card variant='outlined' sx={{ maxWidth: 250 }}>
      <CardMedia
        sx={{ height: 120, backgroundSize: '50%' }}
        image={chest.imageURI || (defaultImage as string)}
        title={chest.title}
      />
      <CardContent sx={{ pb: 0 }}>
        <Typography variant='h6' component='div' align='center'>
          #{chest.chestId}: {chest.title}
        </Typography>
      </CardContent>
      <CardActions sx={{ justifyContent: 'center' }}>
        <Button variant='contained' size='small' onClick={() => onEdit?.(chest)} disabled={!onEdit}>
          Edit
        </Button>
        <Button
          variant='contained'
          size='small'
          onClick={() => onClone?.(chest)}
          disabled={!onClone}
        >
          Clone
        </Button>
      </CardActions>
    </Card>
  );
}

export default ChestCard;
