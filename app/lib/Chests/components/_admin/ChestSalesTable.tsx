import Paper from '@mui/material/Paper';
import TableContainer from '@mui/material/TableContainer';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableCell from '@mui/material/TableCell';
import TableBody from '@mui/material/TableBody';
import React from 'react';
import { BountyEntryStatus, IBountyUserEntryDto } from 'lib/Bounty/api-server/bounty.types.ts';
import { TablePagination, TableRow } from '@mui/material';
import { IPaginationResult } from 'utils/hooks/usePagination.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Link } from 'react-router-dom';
import { Button } from '@ui-kit/Button/Button.tsx';
import { IChainChestSale, useChainChestSales } from 'lib/Chests/hooks/sale/useChestSales.ts';

type Props = {};

export function ChestSalesTable(props: Props) {
  const result = useChainChestSales();

  const rows: IChainChestSale[] = result.data || [];

  return (
    <Paper variant='outlined' sx={{ overflow: 'hidden' }}>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              {/*<TableCell sx={{ width: '70px' }} />*/}
              <TableCell>Sale ID</TableCell>
              <TableCell>Chest ID</TableCell>
              <TableCell>Amount</TableCell>
              <TableCell>Price</TableCell>
              <TableCell>Token</TableCell>
              <TableCell>Delay</TableCell>
              <TableCell>Limit</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={`row-${row.id}`}>
                {/*<TableCell>*/}
                {/*  <Button variant='outlined' color='primary'>*/}
                {/*    Show*/}
                {/*  </Button>*/}
                {/*</TableCell>*/}
                <TableCell>#{row.saleId}</TableCell>
                <TableCell>#{row.chestId}</TableCell>
                <TableCell>{row.amount}</TableCell>
                <TableCell>{row.price}</TableCell>
                <TableCell>{row.isShardCurrency ? 'PXLs' : 'Erc20'}</TableCell>
                <TableCell>{row.additionalDelaySeconds}s</TableCell>
                <TableCell>{row.amountPerHandsLimit || '-'}</TableCell>
              </TableRow>
            ))}
            {!rows.length && (
              <TableRow>
                <TableCell colSpan={5}>No data</TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
}
