import React, { useEffect } from 'react';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import EditChestChances from 'lib/Chests/components/_admin/EditChestDialog/components/EditChestChances.tsx';
import { IChestV2DropDto, IChainChest } from 'lib/Chests/hooks/useChests.ts';
import { useChestAdminApi } from 'lib/Chests/hooks/useChestAdminApi.ts';
import toaster from 'services/toaster.tsx';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { loadNftCollections, nftCollectionsStore } from 'lib/NFT/api-server/nft.store.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import Box from '@mui/material/Box';
import { useNftTypes } from 'lib/AdminV5/hooks/useNftTypes.ts';
import { DialogTitle } from '@mui/material';
import { useChest } from 'lib/Chests/hooks/useChest.ts';
import { waitForTransactionReceipt } from '@wagmi/core';
import { useConfig } from 'wagmi';
import ImageUrlField from '@ui-kit/ImageUrlField/ImageUrlField.tsx';
import SendChestForm from 'lib/Chests/components/_admin/SendChestForm.tsx';
import { CHEST_TOKENS_BY_CHAIN, getDefaultChestDrop } from 'lib/Chests/utils/admin-constants.ts';

type Props = DialogProps & { chest?: IChainChest; onSave?: () => void; clone?: boolean };

function EditChestDialog(props: Props) {
  const { chest, onClose, onSave, clone = false, ...other } = props;

  const chestId = chest?.chestId;
  const isUpdate = typeof chestId === 'number';

  const config = useConfig();
  const account = useAccount();
  const api = useChestAdminApi();

  const NFT_ADDRESS = NFT_CONTRACT_ID[account.chainId]?.toLowerCase();

  const initialChest = useChest(chestId);

  const [title, setTitle] = React.useState('');
  const [image, setImage] = React.useState('');
  const [drop, setDrop] = React.useState<IChestV2DropDto[]>([
    // getDropItem({isShard: true}),
    // getDropItem({ isToken: true, isShard: false, ratio: 1.75 }),
    // getDropItem({ isShard: false, items: [502] }),
    // getDropItem(),
    getDefaultChestDrop({ isNew: true }),
  ]);

  const types = useNftTypes({
    contractId: NFT_ADDRESS,
    skip: !NFT_ADDRESS,
  });

  const handleCreate = async () => {
    try {
      const txHash = await api.create({ title, drop, image });

      await waitForTransactionReceipt(config, { hash: txHash, confirmations: 1 });

      onClose?.({}, 'escapeKeyDown');
      toaster.success('Chest created');

      onSave?.();
      await waitForTransactionReceipt(config, {
        confirmations: 1,
        hash: txHash,
        pollingInterval: 1_000,
      });

      onSave?.();
      toaster.success('Chest saved');
    } catch (e) {
      console.error(e);
      toaster.logError(e);
    }
  };
  const handleSaveInfo = async () => {
    try {
      await api.updateInfo({ chestId, title, image });
      await initialChest.refetch();
      onSave?.();
      toaster.success('Chest info updated');
    } catch (e) {
      toaster.captureException(e);
    }
  };
  const handleUpdateDrop = async (index: number, drop: IChestV2DropDto) => {
    if (typeof chestId === 'undefined') {
      return;
    }

    try {
      console.log(drop);
      if (drop.isNew) {
        await api.addDrop({ chestId, drop });
      } else {
        await api.updateDrop({ chestId, dropIndex: index, drop });
      }

      await initialChest.refetch();
      onSave?.();
      toaster.success('Chest saved');
    } catch (e) {
      console.error(e);
      toaster.logError(e);
    }
  };

  const handleGive = async (userAddresses: string[]) => {
    if (typeof chestId === 'undefined') {
      return;
    }

    try {
      await api.give({ chestId, userAddresses });

      toaster.success('Chest sent');
    } catch (e) {
      console.error(e);
      toaster.logError(e);
    }
  };

  const tokens = CHEST_TOKENS_BY_CHAIN[account.chainId] || [];
  const actionLoading = api.loading;

  useEffect(() => {
    loadNftCollections({ contractId: NFT_ADDRESS, limit: 1000, offset: 0 }).then((r) => {
      types.changeCollectionId(r.rows[0]?.id || null);
    });
  }, [NFT_ADDRESS]);

  useEffect(() => {
    if (typeof chestId === 'number') {
      if (initialChest.data) {
        setTitle(initialChest.data.title);
        setImage(initialChest.data.imageURI);
        setDrop(initialChest.data.drop);
      }
    } else {
      setTitle('');
      setImage('');
      setDrop([getDefaultChestDrop()]);
    }
  }, [initialChest.dataUpdatedAt, chestId]);

  const content = (
    <Box>
      <Box sx={{ display: 'flex', gap: 3, mb: 2 }}>
        <TextField
          label='Title'
          placeholder='Title'
          value={title}
          error={!title?.length}
          onChange={(e) => setTitle(e.target.value)}
          fullWidth
          required
          disabled={actionLoading}
          sx={{ flex: 1 }}
        />

        <ImageUrlField
          label='Image'
          placeholder='Image url'
          value={image}
          // error={!image?.length}
          onChange={(e) => setImage(e.target.value)}
          fullWidth
          // required
          disabled={actionLoading}
          sx={{ flex: 1 }}
        />
      </Box>

      <EditChestChances
        drop={drop}
        onChange={(next) => {
          setDrop(next);
        }}
        onCreate={!isUpdate ? handleCreate : undefined}
        onUpdate={handleSaveInfo}
        tokens={tokens}
        types={types}
        loading={actionLoading}
        isNew={typeof chestId === 'undefined'}
        onSaveDrop={handleUpdateDrop}
        btnText={clone ? 'Clone Chest' : 'Create Chest'}
      />

      {typeof chestId === 'number' && !clone && (
        <SendChestForm sx={{ mt: 2 }} onSend={handleGive} loading={actionLoading} />
      )}
    </Box>
  );

  // return  content;
  return (
    <Dialog onClose={onClose} {...other} fullWidth maxWidth='lg' scroll='body'>
      <DialogTitle>
        {typeof chestId !== 'undefined'
          ? clone
            ? `Clone Chest ${chestId}`
            : `Edit Chest ${chestId}`
          : 'Create new Chest'}
      </DialogTitle>
      <div style={{ padding: 32 }}>{content}</div>
    </Dialog>
  );
}

export default EditChestDialog;
