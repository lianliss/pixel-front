import React, { useMemo, useState } from 'react';
import EditChestChance from 'lib/Chests/components/_admin/EditChestDialog/components/EditChestChance.tsx';
import { IChestV2DropDto } from 'lib/Chests/hooks/useChests.ts';
import { Button } from '@ui-kit/Button/Button.tsx';
import { ButtonGroup } from '@ui-kit/ButtonGroup/ButtonGroup.tsx';
import Box from '@mui/material/Box';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { INftCollectionDto, INftTypeDto } from 'lib/NFT/api-server/nft.types.ts';
import { getDefaultChestDrop } from 'lib/Chests/utils/admin-constants.ts';
import { INftTypesResult } from 'lib/AdminV5/hooks/useNftTypes.ts';
import { getChestChances } from 'lib/Chests/utils/get-chest-chances.ts';

type Props = {
  drop: IChestV2DropDto[];
  onChange?: (drop: IChestV2DropDto[]) => void;
  onAdd?: () => void;
  onCreate?: () => void;
  tokens?: Array<{ symbol: string; address: string; isShard?: boolean }>;
  types?: INftTypesResult;
  loading?: boolean;
  btnText?: string;
  isNew?: boolean;
  onSaveDrop?: (index: number, drop: IChestV2DropDto) => void;
  onUpdate?: () => void;
};

function EditChestChances(props: Props) {
  const {
    drop,
    onChange,
    onCreate,
    tokens = [],
    types,
    loading = false,
    btnText,
    isNew,
    onSaveDrop,
    onUpdate,
  } = props;

  const [open, setOpen] = useState<Record<string, boolean>>({});

  const handleChangeType = (i: number) => (type: 'token' | 'nft') => {
    const item = drop[i];

    switch (type) {
      case 'token':
        item.items = [];
        item.isToken = true;
        item.isShard = false;
        break;
      default:
        item.isToken = false;
        item.isShard = false;
        break;
    }

    onChange?.([...drop]);
  };
  const handleChangeToken = (i: number) => (token: string) => {
    const item = drop[i];
    item.tokenAddress = token;

    const tokenInfo = tokens.find((el) => el.address?.toLowerCase() === item.tokenAddress?.toLowerCase());
    const isShard = tokenInfo?.isShard || false;
    item.isShard = isShard;
    item.isToken = !isShard;

    onChange?.([...drop]);
  };
  const handleChangeAmounts = (i: number) => (min: number, max: number) => {
    const item = drop[i];
    item.amount = min;
    item.maxAmount = max;

    onChange?.([...drop]);
  };
  const handleChangeItems = (i: number) => (items: number[]) => {
    const item = drop[i];
    item.items = items;

    onChange?.([...drop]);
  };
  const handleChangeRatio = (i: number) => (value: number) => {
    const item = drop[i];
    item.ratio = value;

    onChange?.([...drop]);
  };
  // const handleChangeChance = (i: number) => (value: number) => {
  //   const newParams = [...drop];
  //
  //   let sum = 0;
  //   for (let j = 0; j < i; j++) {
  //     if (chances[j]) {
  //       sum += chances[j];
  //     }
  //   }
  //   const param = value === 0 ? 0 : (100 - sum) / value;
  //
  //   newParams[i].ratio = param;
  //   onChange?.(newParams);
  // };
  const handleAdd = () => {
    onChange?.([...drop, getDefaultChestDrop({ ratio: 0, isNew: true })]);
    setOpen({ ...open, [drop.length]: true });
  };
  const handleDelete = (i: number) => () => {
    const next = [...drop];
    next.splice(i, 1);
    onChange?.(next);
  };

  // const chances = useMemo(() => {
  //   return getChestChances(drop.map((el) => el.ratio));
  // }, [drop]);

  return (
    <div>
      <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end', mb: 2 }}>
        <Typography>
          Summary Chance {(drop.reduce((acc, el) => acc + el.ratio, 0) * 100).toFixed(2)}%
        </Typography>

        <Box sx={{ display: 'flex', gap: 2 }}>
          <ButtonGroup>
            <Button
              onClick={() => setOpen(drop.reduce((acc, _, i) => ({ ...acc, [i]: true }), {}))}
            >
              Open All
            </Button>
            <Button onClick={() => setOpen({})}>Close All</Button>
          </ButtonGroup>
        </Box>
      </Box>
      <div>
        {drop.map((el, i) => {
          return (
            <EditChestChance
              key={`drop-${i}`}
              defaultExpanded={i < 5}
              expanded={open[i] || false}
              onChange={(_e, expanded) => {
                setOpen({ ...open, [i]: !!expanded });
              }}
              paramId={(i + 1).toString()}
              data={el}
              onValueChange={handleChangeRatio(i)}
              // onChanceChange={handleChangeChance(i)}
              onDelete={handleDelete(i)}
              onChangeType={handleChangeType(i)}
              onChangeToken={handleChangeToken(i)}
              tokens={tokens}
              types={types}
              onChangeAmounts={handleChangeAmounts(i)}
              onChangeItems={handleChangeItems(i)}
              disabled={loading}
              isNew={isNew}
              onUpdate={(drop) => {
                onSaveDrop?.(i, drop);
              }}
            />
          );
        })}
      </div>

      <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2, gap: 3 }}>
        {onCreate && (
          <Button
            variant='contained'
            size='large'
            color='primary'
            onClick={onCreate}
            loading={loading}
          >
            {btnText}
          </Button>
        )}

        {onUpdate && !onCreate && (
          <Button
            variant='contained'
            size='large'
            onClick={onUpdate}
            disabled={!onUpdate}
            loading={loading}
          >
            Update Info
          </Button>
        )}

        <Button
          variant='contained'
          size='large'
          color='primary'
          onClick={handleAdd}
          disabled={loading}
        >
          Add New Drop
        </Button>
      </Box>
    </div>
  );
}

export default EditChestChances;
