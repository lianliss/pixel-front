import React from 'react';
import styles from './EditChest.module.scss';
import {
  Button,
  ButtonGroup,
  ControlGroup,
  FormGroup,
  HTMLSelect,
  InputGroup,
  MenuItem,
  Radio,
  RadioGroup,
  Spinner,
  Tab,
  Tabs,
  Tag,
} from '@blueprintjs/core';
import toaster from 'services/toaster.tsx';
import { wei } from 'utils/index';
import { Select } from '@blueprintjs/select';
import flattenDeep from 'lodash-es/flattenDeep';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import get from 'lodash/get';
import { convertToNftItemDto } from 'lib/NFT/components/NFTCard/convertToNftItemDto.ts';
import CHESTS_ABI from 'lib/Chests/api-contract/abi/chest.abi.ts';
import {
  CHEST_CONTRACT_ID,
  CHEST_V2_CONTRACT_ID,
  FREE_CHEST_CONTRACT_ID,
} from 'lib/Chests/api-contract/contract.constants.ts';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import Stack from '@mui/material/Stack';
import Chip from '@mui/material/Chip';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { alpha } from '@mui/material/styles';
import FREE_CHEST_ABI from 'lib/Chests/api-contract/abi/free-chest.abi.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import NFT_ABI from 'lib/NFT/api-contract/nft.abi.ts';
import { useNftTypes } from 'lib/NFT/hooks/useNftTypes.ts';
import NumberField from '@ui-kit/NumberField/NumberField.tsx';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { CHEST_V2_ABI } from 'lib/Chests/api-contract/abi/chest-v2.abi.ts';
import { useChestAdminApi } from 'lib/Chests/hooks/useChestAdminApi.ts';

const BASE_VALUE = 100;

function EditChest({ chest, refreshChests }) {
  const itemTypesData = useNftTypes();
  const itemTypes = itemTypesData.data;

  const account = useAccount();
  const chestAddress = CHEST_V2_CONTRACT_ID[account.chainId];
  const freeChestAddress = FREE_CHEST_CONTRACT_ID[account.chainId];

  const chestAdminApi = useChestAdminApi();

  const nftAddress = NFT_CONTRACT_ID[account.chainId];
  const nftCollections = useReadContract({
    initData: [],
    abi: NFT_ABI,
    address: nftAddress,
    functionName: 'getCollections',
    skip: !account.isConnected || !nftAddress,
  });
  const collections = nftCollections.data;

  const writeChest = useWriteContract({
    abi: CHEST_V2_ABI,
    address: chestAddress,
  });
  const writeFreeChest = useWriteContract({
    abi: FREE_CHEST_ABI,
    address: freeChestAddress,
  });

  const [minPixels, setMinPixels] = React.useState(chest?.minPixels || 0);
  const [maxPixels, setMaxPixels] = React.useState(chest?.maxPixels || 0);
  const [title, setTitle] = React.useState<string>(chest?.title || '');
  const [collectionId, setCollectionId] = React.useState(0);
  const [params, setParams] = React.useState([0, 0, 0, 0, 0, 0]);
  React.useEffect(() => {
    if (chest) {
      setParams(chest.ratios);
    } else {
      setParams([0, 0, 0, 0, 0, 0]);
    }
  }, [chest]);

  const chances = [];
  let isValidChances = true;
  for (let i = 0; i < params.length; i++) {
    const param = params[i];
    if (!param) {
      chances.push(0);
      continue;
    }
    let sum = 0;
    for (let j = 0; j < i; j++) {
      if (chances[j]) {
        sum += chances[j];
      }
    }
    const value = (BASE_VALUE - sum) / param;
    if (value < 0) {
      isValidChances = false;
    }
    chances.push(value);
  }

  const setChance = (index, chance) => {
    let sum = 0;
    for (let j = 0; j < index; j++) {
      if (chances[j]) {
        sum += chances[j];
      }
    }
    const param = chance === 0 ? 0 : (BASE_VALUE - sum) / chance;
    const newParams = [...params];
    newParams[index] = param;
    setParams(newParams);
  };

  const [isUpdating, setIsUpdating] = React.useState(false);
  const handleCreateChest = async () => {
    try {
      await chestAdminApi.create({
        title,
        image: '',
        drop: [],
      });

      await refreshChests?.();
    } catch (error) {
      console.error(error);
      toaster.logError(error);
    }
    setIsUpdating(false);
  };

  const handleUpdate = async () => {
    try {
      setIsUpdating(true);

      await chestAdminApi.update({
        chestId: chest.chestId,
        title,
        image: '',
        drop: [],
      });

      await refreshChests?.();
    } catch (error) {
      toaster.logError(error);
    }
    setIsUpdating(false);
  };

  const onClearChest = async () => {
    try {
      if (!chest) throw new Error('Undefined chest');
      setIsUpdating(true);

      await writeChest.writeContractAsync({
        functionName: 'clearChestItems',
        args: [chest.chestId],
      });
      await refreshChests?.();
    } catch (error) {
      toaster.logError(error);
    }
    setIsUpdating(false);
  };

  const onSendFreeChest = async () => {
    try {
      if (!chest) throw new Error('Undefined chest');
      setIsUpdating(true);

      await writeFreeChest.writeContractAsync({
        functionName: 'setUsersFreeChest',
        args: [users.split(' ').map((u) => Number(u)), chest.chestId],
      });
    } catch (error) {
      toaster.logError(error);
    }
    setIsUpdating(false);
  };

  const rarities = ['PXLs Drop', 'Common', 'Uncommon', 'Rare', 'Epic', 'Legendary'];

  const [typeId, setTypeId] = React.useState();
  const [addTypes, setAddTypes] = React.useState([]);
  const [rarity, setRarity] = React.useState(1);
  const [users, setUsers] = React.useState('');

  React.useEffect(() => {
    for (let i = 1; i < rarities.length; i++) {
      if (get(chest, `items[0][${i}]`, []).length) {
        setRarity(i);
        return;
      }
    }
  }, [chest]);

  const onAddItemType = async () => {
    try {
      if (!addTypes.length) throw new Error('Undefined chest');
      setIsUpdating(true);

      await writeChest.writeContractAsync({
        functionName: 'addChestItems',
        args: [chest.chestId, 0, rarity, addTypes],
      });
      await refreshChests?.();

      setTypeId();
      setAddTypes([]);
    } catch (error) {
      toaster.logError(error);
    }
    setIsUpdating(false);
  };

  const renderItem = (itemType, { handleClick, handleFocus, modifiers, query }) => {
    return (
      <MenuItem
        onClick={handleClick}
        onFocus={handleFocus}
        key={itemType.typeId}
        active={itemType.typeId === typeId}
        label={collections[itemType.collectionId]}
        text={
          <>
            <Tag>{rarities[itemType.rarity]}</Tag>
            &nbsp;
            {`${itemType.typeId}. ${itemType.name}`}
          </>
        }
        roleStructure='listoption'
      />
    );
  };

  const currentTypes = React.useMemo(() => {
    if (!chest) return [];
    return flattenDeep(chest.items);
  }, [itemTypes]);
  const filteredTypes = React.useMemo(() => {
    return itemTypes.filter(
      (t) =>
        !currentTypes.includes(t.typeId) &&
        t.collectionId === collectionId &&
        t.rarity === rarity &&
        !addTypes.includes(t.typeId)
    );
  }, [currentTypes.length, collectionId, rarity, addTypes, chest]);

  const onRemoveType = (typeId) => {
    setAddTypes(addTypes.filter((t) => t !== typeId));
  };

  const onAddCollection = () => {
    setAddTypes(filteredTypes.map((t) => t.typeId));
  };

  const itemPredicate = (query, type, _index, exactMatch) => {
    const normalizedTitle = type.name.toLowerCase();
    const normalizedQuery = query.toLowerCase();
    const collection = collections[type.collectionId].toLowerCase();

    if (exactMatch) {
      return normalizedTitle === normalizedQuery;
    } else {
      return `${type.typeId}. ${normalizedTitle} ${collection}`.indexOf(normalizedQuery) >= 0;
    }
  };

  if (!itemTypes.length) {
    return (
      <div className={styles.add}>
        <Spinner />
      </div>
    );
  }

  return (
    <div className={styles.add}>
      <TextField
        label='Title'
        placeholder='Title'
        value={title}
        error={!title?.length}
        onChange={(e) => setTitle(e.target.value)}
        fullWidth
        style={{
          maxWidth: 600,
        }}
      />

      <div style={{ marginTop: 16, maxWidth: 600 }}>
        <FormGroup label='Min Max PXLs'>
          <div style={{ display: 'flex', gap: 16, flexDirection: 'row', marginTop: 8 }}>
            <TextField
              size='small'
              label={`Min PXLs`}
              placeholder={`Min PXLs`}
              min={0}
              step={0.01}
              onChange={(e) => {
                setMinPixels(+e.target.value);
              }}
              value={minPixels}
              fullWidth
              type='number'
            />
            <TextField
              size='small'
              label={`Max PXLs`}
              placeholder={`Max PXLs`}
              min={0}
              step={0.01}
              onChange={(e) => {
                setMaxPixels(+e.target.value);
              }}
              value={maxPixels}
              fullWidth
              type='number'
            />
          </div>
        </FormGroup>
      </div>

      <div className={styles.addColumns} style={{ marginTop: 16 }}>
        <FormGroup helperText='Params for the chest' label='Rarity Params'>
          <Stack direction='column' gap={1.5} style={{ marginTop: 8 }}>
            {params.map((value, rarity) => {
              return (
                <NumberField
                  key={rarity}
                  size='small'
                  min={0}
                  label={`Parameter ${rarity}`}
                  placeholder={`Parameter ${rarity}`}
                  InputProps={{
                    endAdornment: (
                      <Chip
                        label={rarities[rarity]}
                        style={{
                          color: getNftRarityColor(rarity),
                          backgroundColor: rarity
                            ? alpha(getNftRarityColor(rarity), 0.4)
                            : undefined,
                        }}
                      />
                    ),
                  }}
                  onChange={(e) => {
                    const value = +e.target.value;

                    const newParams = [...params];
                    newParams[rarity] = value;
                    setParams(newParams);
                  }}
                  value={value ? +value.toFixed(4) : 0}
                />
              );
            })}
          </Stack>
        </FormGroup>
        <FormGroup helperText='Chance to drop rarity' label='Chances'>
          <Stack direction='column' gap={1.5} style={{ marginTop: 8 }}>
            {chances.map((value, rarity) => {
              return (
                <NumberField
                  key={rarity}
                  size='small'
                  min={0}
                  error={value < 0}
                  label={`Chance ${rarity}`}
                  placeholder={`Chance ${rarity}`}
                  InputProps={{
                    endAdornment: (
                      <Chip
                        label='%'
                        size='small'
                        style={{
                          color: getNftRarityColor(rarity),
                          backgroundColor: rarity
                            ? alpha(getNftRarityColor(rarity), 0.4)
                            : undefined,
                        }}
                      />
                    ),
                  }}
                  onChange={(e) => {
                    const value = +e.target.value;

                    setChance(rarity, value);
                  }}
                  value={value ? +value.toFixed(4) : 0}
                />
              );
            })}
          </Stack>
        </FormGroup>
      </div>
      {chest ? (
        <ButtonGroup>
          <Button
            disabled={
              !isValidChances ||
              (chest.ratios.map((r) => r.toFixed(2)).join('-') ===
                params.map((r) => r.toFixed(2)).join('-') &&
                title === chest.title &&
                minPixels === chest.minPixels &&
                maxPixels === chest.maxPixels) ||
              isUpdating
            }
            onClick={handleUpdate}
            intent={'success'}
            icon={'cloud-upload'}
            loading={isUpdating}
          >
            Update Params
          </Button>
          <Button
            disabled={isUpdating}
            onClick={onClearChest}
            intent={'danger'}
            icon={'eraser'}
            loading={isUpdating}
          >
            Clear Items
          </Button>
          <InputGroup
            placeholder={'162131210 112331243...'}
            leftIcon={'person'}
            onValueChange={setUsers}
            value={users}
          />
          <Button
            disabled={isUpdating || !users.length}
            onClick={onSendFreeChest}
            intent={'success'}
            icon={'send-message'}
            loading={isUpdating}
          >
            Send Free
          </Button>
        </ButtonGroup>
      ) : (
        <Button
          disabled={!isValidChances || isUpdating || !title.length}
          onClick={handleCreateChest}
          intent={'success'}
          icon={'small-plus'}
          loading={isUpdating}
        >
          Create new Chest
        </Button>
      )}
      <div className={styles.addDivider} />
      <RadioGroup
        label='Collection'
        inline
        onChange={(event) => setCollectionId(Number(event.target.value))}
        selectedValue={collectionId}
      >
        {collections.map((collection, collectionId) => (
          <Radio label={collection} key={collectionId} value={collectionId} />
        ))}
        <Button onClick={onAddCollection} intent={'warning'}>
          All from Collection
        </Button>
      </RadioGroup>
      <div className={styles.addTypes}>
        {addTypes.map((typeId) => {
          const type = itemTypes[typeId];
          return (
            <Tag
              key={typeId}
              large
              intent={'warning'}
              interactive
              onRemove={() => onRemoveType(typeId)}
              onClick={() => onRemoveType(typeId)}
            >
              {type.typeId}. {type.name}
            </Tag>
          );
        })}
      </div>
      {!!chest && (
        <ControlGroup className={styles.addType} fill={true}>
          <HTMLSelect
            options={rarities}
            onChange={(event) => {
              setRarity(rarities.indexOf(event.currentTarget.value));
            }}
          />
          <Select
            items={filteredTypes}
            onItemSelect={(item) => {
              setTypeId(item.typeId);
              setAddTypes([...addTypes, item.typeId]);
            }}
            noResults={<MenuItem disabled={true} text='No results.' roleStructure='listoption' />}
            itemPredicate={itemPredicate}
            itemRenderer={renderItem}
          >
            <Button rightIcon='double-caret-vertical'>
              {itemTypes[typeId]
                ? `${itemTypes[typeId].typeId}. ${itemTypes[typeId].name}`
                : 'Select an Item'}
            </Button>
          </Select>
          <Button
            icon='small-plus'
            disabled={!addTypes.length || isUpdating}
            loading={isUpdating}
            onClick={onAddItemType}
            intent={addTypes.length ? 'success' : undefined}
          >
            Add Item
          </Button>
        </ControlGroup>
      )}
      <Tabs
        id='ChestRarities'
        onChange={(value) => {
          setRarity(Number(value.split('chest-rarity-')[1]));
        }}
        renderActiveTabPanelOnly
        selectedTabId={`chest-rarity-${rarity}`}
      >
        {get(chest, 'items[0]', []).map((items, rarity) => {
          if (!items.length) return <></>;
          return (
            <Tab
              id={`chest-rarity-${rarity}`}
              key={rarity}
              title={rarities[rarity]}
              tagContent={items.length}
              panel={
                <div className={styles.addItems}>
                  {items.map((typeId) => {
                    return (
                      <NFTCard
                        nft={convertToNftItemDto(itemTypes[typeId])}
                        {...itemTypes[typeId]}
                        key={typeId}
                      />
                    );
                  })}
                </div>
              }
            />
          );
        })}
      </Tabs>
    </div>
  );
}

export default EditChest;
