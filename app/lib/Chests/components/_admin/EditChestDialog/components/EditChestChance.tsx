import AccordionSummary from '@mui/material/AccordionSummary';
import Accordion, { AccordionProps } from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import React, { useMemo } from 'react';
import NumberField from '@ui-kit/NumberField/NumberField.tsx';
import Box from '@mui/material/Box';
import { IChestV2DropDto } from 'lib/Chests/hooks/useChests.ts';
import Chip from '@mui/material/Chip';
import { Button } from '@ui-kit/Button/Button.tsx';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import { INftCollectionDto, INftTypeDto, NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import Autocomplete from '@mui/material/Autocomplete';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import { createNftFromType } from 'lib/NFT/utils/create-nft-from-type.ts';
import Grid from '@mui/material/Grid';
import { ButtonGroup } from '@ui-kit/ButtonGroup/ButtonGroup.tsx';
import Pagination from '@mui/material/Pagination';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { INftTypesResult } from 'lib/AdminV5/hooks/useNftTypes.ts';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';

type Props = Omit<AccordionProps, 'children'> & {
  paramId?: string;
  onValueChange?: (value: number) => void;
  // onChanceChange?: (value: number) => void;
  data?: IChestV2DropDto;
  // chance?: number;
  onDelete?: () => void;
  onChangeType?: (type: 'token' | 'nft') => void;
  onChangeAmounts?: (min: number, max: number) => void;
  onChangeToken?: (address: string) => void;
  tokens?: Array<{ symbol: string; address: string }>;
  types?: INftTypesResult;
  onChangeItems?: (items: number[]) => void;
  disabled?: boolean;
  isNew?: boolean;
  onUpdate?: (drop: IChestV2DropDto) => void;
};

const getTypeLabel = (el: IChestV2DropDto) => {
  if (el.isToken) {
    return 'Token';
  }
  if (el.isShard) {
    return 'Shard';
  }
  return 'NFT';
};

const rarities = [
  NftItemRarity.Common,
  NftItemRarity.UnCommon,
  NftItemRarity.Rare,
  NftItemRarity.Epic,
  NftItemRarity.Legendary,
];

function EditChestChance(props: Props) {
  const {
    paramId,
    onValueChange,
    // onChanceChange,
    data,
    // chance,
    onDelete,
    onChangeType,
    onChangeAmounts,
    onChangeToken,
    tokens = [],
    onChangeItems,
    disabled,
    types: typesState,
    expanded,
    defaultExpanded,
    isNew,
    onUpdate,
    ...other
  } = props;

  const value = data?.ratio || 0;
  const minAmount = data?.amount || 0;
  const maxAmount = data?.maxAmount || 0;
  const tokenSymbol =
    tokens.find((el) => el.address?.toLowerCase() === data?.tokenAddress?.toLowerCase())?.symbol ||
    'Tokens';

  const selectedTypes: INftTypeDto[] = useMemo(() => {
    const typesMap = Object.fromEntries(typesState.types.map((t) => [t.id.split('-').pop()!, t]));

    return data?.items.map((el) => typesMap[el] || ({ id: el.toString(), name: el } as any));
  }, [typesState.types, data?.items]);
  const itemsMap = useMemo(() => {
    return Object.fromEntries((data?.items).map((el) => [el, true]));
  }, [data]);
  const includeTypes = useMemo(() => {
    return typesState.types.filter((el) => el.id.split('-').pop() in itemsMap);
  }, [typesState]);

  const allTypes = useMemo(() => {
    if (!typesState.rarity) {
      return typesState.types;
    }

    return typesState.types.filter((el) => el.rarity === typesState.rarity);
  }, [typesState.types, typesState.rarity]);

  return (
    <Accordion variant='outlined' disableGutters defaultExpanded={defaultExpanded} {...other}>
      <AccordionSummary aria-controls='panel1d-content'>
        <Typography>
          Drop {paramId} - {!!data && getTypeLabel(data)} ({+(data.ratio * 100).toFixed(2)}%) -{' '}
          {!data.isToken && !data.isShard ? data.items.length + ' NFTs' : data.amount + ' tokens'}
        </Typography>
      </AccordionSummary>
      {(expanded === true || defaultExpanded) && (
        <AccordionDetails>
          <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
            <Box sx={{ display: 'flex', gap: 3 }}>
              <FormControl fullWidth sx={{ maxWidth: 150 }}>
                <InputLabel id='demo-simple-select-label'>Type</InputLabel>
                <Select
                  labelId='demo-simple-select-label'
                  id='demo-simple-select'
                  value={data?.isToken || data.isShard ? 'token' : 'nft'}
                  label='Type'
                  size='small'
                  onChange={(e) => onChangeType?.(e.target.value as 'token' | 'nft')}
                  disabled={disabled}
                >
                  <MenuItem value='nft'>NFT</MenuItem>
                  <MenuItem value='token'>Token</MenuItem>
                </Select>
              </FormControl>

              <NumberField
                size='small'
                min={0}
                label={`Chance ${paramId}`}
                placeholder={`Chance ${paramId}`}
                onChange={(e) => {
                  const value = +e.target.value / 100;

                  onValueChange?.(value);
                }}
                value={value ? +(value * 100).toFixed(4) : 0}
                disabled={disabled}
                InputProps={{
                  endAdornment: <Chip label='%' size='small' />,
                }}
              />
              {/*<NumberField*/}
              {/*  size='small'*/}
              {/*  min={0}*/}
              {/*  label={`Chance ${paramId}`}*/}
              {/*  placeholder={`Chance ${paramId}`}*/}
              {/*  InputProps={{*/}
              {/*    endAdornment: <Chip label='%' size='small' />,*/}
              {/*  }}*/}
              {/*  onChange={(e) => {*/}
              {/*    const value = +e.target.value;*/}

              {/*    onChanceChange?.(value);*/}
              {/*  }}*/}
              {/*  value={chance}*/}
              {/*  disabled={disabled}*/}
              {/*/>*/}
            </Box>
            <ButtonGroup>
              {!isNew && (
                <Button
                  variant='outlined'
                  size='small'
                  color='success'
                  disabled={!onUpdate || disabled}
                  onClick={() => onUpdate?.(data)}
                >
                  Update Drop
                </Button>
              )}
              <Button
                variant='outlined'
                size='small'
                color='error'
                disabled={!onDelete || disabled}
                onClick={onDelete}
              >
                Delete Drop
              </Button>
            </ButtonGroup>
          </Box>

          {(data?.isToken || data?.isShard) && (
            <Box sx={{ display: 'flex', gap: 3, mt: 3 }}>
              <FormControl fullWidth sx={{ maxWidth: 150 }}>
                <InputLabel id='demo-simple-select-label'>
                  {data?.isShard ? 'Shard' : 'Token'}
                </InputLabel>
                <Select
                  value={data?.tokenAddress?.toLowerCase()}
                  label={data?.isShard ? 'Shard' : 'Token'}
                  size='small'
                  onChange={(e) => onChangeToken?.(e.target.value as string)}
                  disabled={disabled}
                >
                  {tokens.map((token) => (
                    <MenuItem key={token.symbol} value={token.address?.toLowerCase() || ''}>
                      {token.symbol}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <TextField
                size='small'
                label={`Min Amount`}
                placeholder={`Min Amount`}
                min={0}
                step={0.01}
                onChange={(e) => {
                  const value = +e.target.value;
                  onChangeAmounts?.(value, maxAmount);
                }}
                InputProps={{
                  endAdornment: <Chip label={tokenSymbol} size='small' />,
                }}
                value={minAmount}
                fullWidth
                type='number'
                disabled={disabled}
              />
              <TextField
                size='small'
                label={`Max Amount`}
                placeholder={`Max Amount`}
                min={0}
                step={0.01}
                InputProps={{
                  endAdornment: <Chip label={tokenSymbol} size='small' />,
                }}
                onChange={(e) => {
                  const value = +e.target.value;
                  onChangeAmounts?.(minAmount, value);
                }}
                value={maxAmount}
                fullWidth
                type='number'
                disabled={disabled}
              />
            </Box>
          )}

          {!data.isToken && !data.isShard && (
            <div style={{ marginTop: 24 }}>
              <div>
                <Box sx={{ display: 'flex', gap: 3, mb: 2 }}>
                  <FormControl disabled={disabled} fullWidth>
                    <InputLabel id='demo-simple-select-label'>Collection</InputLabel>
                    <Select
                      value={typesState.collectionId || ''}
                      label='Collection'
                      onChange={(e) => typesState.changeCollectionId(e.target.value as string)}
                    >
                      {typesState.collections.map((collection) => (
                        <MenuItem key={collection.id} value={collection.id}>
                          {collection.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>

                  <FormControl disabled={disabled} fullWidth>
                    <InputLabel>Rarity</InputLabel>
                    <Select
                      value={typesState.rarity || 'all'}
                      label='Rarity'
                      onChange={(e) =>
                        typesState.changeRarity(
                          e.target.value === 'all' ? undefined : (e.target.value as NftItemRarity)
                        )
                      }
                    >
                      <MenuItem value='all'>All</MenuItem>

                      {rarities.map((el: NftItemRarity) => (
                        <MenuItem
                          key={el}
                          value={el}
                          style={{ color: el !== 'all' ? getNftRarityColor(el) : undefined }}
                        >
                          {getNftRarityLabel(el)}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Box>

                {typesState.pagination.enabled && (
                  <Box sx={{ mb: 2, width: '100%', display: 'flex', justifyContent: 'center' }}>
                    <Pagination
                      count={typesState.pagination.pages}
                      color='primary'
                      variant='outlined'
                      shape='rounded'
                      size='large'
                      siblingCount={1}
                      boundaryCount={0}
                      page={typesState.pagination.page}
                      onChange={(_, p) => {
                        typesState.pagination.setPage(p);
                      }}
                    />
                  </Box>
                )}

                <Autocomplete<INftTypeDto, true>
                  multiple
                  sx={{ mb: 2 }}
                  options={allTypes || []}
                  value={selectedTypes}
                  getOptionLabel={(option) => option.name}
                  renderOption={(p, option) => (
                    <MenuItem {...p} sx={{ display: 'flex', gap: 1 }}>
                      <Avatar variant='rounded' src={option.uri} />
                      <Typography style={{ color: getNftRarityColor(option.rarity) }}>
                        {option.name} #{option.id?.split('-').pop()!}
                      </Typography>
                    </MenuItem>
                  )}
                  onChange={(e, arr) => {
                    onChangeItems?.(arr.map((el) => +el.id.split('-').pop()!));
                  }}
                  limitTags={10_000}
                  getOptionDisabled={(el) => el.id.split('-').pop() in itemsMap}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select nft Type'
                      placeholder='Enter nft type name..'
                    />
                  )}
                  disabled={disabled}
                />
                <Box sx={{ display: 'flex', gap: 3 }}>
                  <ButtonGroup sx={{ mb: 2 }}>
                    <Button
                      variant='contained'
                      color='primary'
                      onClick={() => {
                        const allTypeIds = allTypes.map((el) => +el.id.split('-').pop()!);
                        const deduplicate: Record<number, boolean> = {};

                        for (const el of [...data.items, ...allTypeIds]) {
                          if (deduplicate[el]) {
                            continue;
                          }

                          deduplicate[el] = true;
                        }

                        onChangeItems?.(Object.keys(deduplicate).map(Number));
                      }}
                    >
                      Add collection NFTs
                    </Button>
                    <Button
                      variant='contained'
                      color='primary'
                      onClick={() => {
                        const allTypeIds = allTypes.map((el) => +el.id.split('-').pop()!);

                        onChangeItems?.(allTypeIds);
                      }}
                    >
                      Replace collection NFTs
                    </Button>
                  </ButtonGroup>
                </Box>
              </div>
              <Grid container spacing={2}>
                {includeTypes?.map((type) => (
                  <Grid item xs={12} md={3} lg={2} key={type.id}>
                    <div style={{ marginTop: -50, marginBottom: -90 }}>
                      <NFTCard
                        nft={createNftFromType(type)}
                        sx={{ transform: 'scale(0.6)', ml: '-45px', mt: '-60px' }}
                      />
                    </div>
                  </Grid>
                ))}
              </Grid>
            </div>
          )}
        </AccordionDetails>
      )}
    </Accordion>
  );
}

export default EditChestChance;
