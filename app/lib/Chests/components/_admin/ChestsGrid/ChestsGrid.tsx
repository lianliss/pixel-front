import React, { useState } from 'react';
import { IChainChest, useChests } from 'lib/Chests/hooks/useChests.ts';
import Pagination from '@mui/material/Pagination';
import { usePagination } from 'utils/hooks/usePagination.ts';
import Grid from '@mui/material/Grid';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useDialog } from 'utils/hooks/useDialog.ts';
import EditChestDialog from 'lib/Chests/components/_admin/EditChestDialog/EditChestDialog.tsx';
import ChestCard from 'lib/Chests/components/_admin/ChestCard.tsx';
import Box from '@mui/material/Box';

function ChestsGrid() {
  const chests = useChests();

  const dialog = useDialog();
  const pagination = usePagination({ total: chests.data?.length || 0, step: 11 });

  const [selectedChest, setSelectedChest] = useState<IChainChest | undefined>(undefined);
  const [clone, setClone] = useState<boolean>(false);

  return (
    <div>
      <div>
        <Grid container spacing={2}>
          <Grid
            item
            xs={12}
            md={4}
            lg={3}
            xl={2}
            sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
          >
            <Button
              variant='contained'
              size='large'
              color='primary'
              onClick={() => {
                dialog.show();
                setSelectedChest(undefined);
                setClone(false);
              }}
            >
              Create New Chest
            </Button>
          </Grid>
          {chests.data
            ?.slice(pagination.offset, pagination.offset + pagination.limit)
            .map((chest, i) => (
              <Grid key={`chest-${i}`} item xs={12} md={4} lg={3} xl={2}>
                <ChestCard
                  chest={chest}
                  onEdit={() => {
                    setSelectedChest(chest);
                    dialog.show();
                    setClone(false);
                  }}
                  onClone={() => {
                    setSelectedChest(chest);
                    dialog.show();
                    setClone(true);
                  }}
                />
              </Grid>
            ))}
        </Grid>
      </div>

      {pagination.enabled && (
        <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', mt: 2 }}>
          <Pagination
            count={pagination.pages}
            color='primary'
            variant='outlined'
            shape='rounded'
            size='large'
            siblingCount={1}
            boundaryCount={0}
            page={pagination.page}
            onChange={(_, p) => {
              pagination.setPage(p);
            }}
          />
        </Box>
      )}

      <EditChestDialog
        open={dialog.open}
        clone={clone}
        onClose={dialog.onClose}
        chest={selectedChest}
        onSave={() => chests.refetch()}
      />
    </div>
  );
}

export default ChestsGrid;
