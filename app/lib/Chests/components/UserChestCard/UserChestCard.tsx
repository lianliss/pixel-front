import { Typography } from '@ui-kit/Typography/Typography.tsx';
import React from 'react';
import { alpha, styled } from '@mui/material/styles';
import Box, { BoxProps } from '@mui/material/Box';
import { IUserChestDto } from 'lib/Chests/api-server/chest.types.ts';
import { Img } from '@ui-kit/Img/Img.tsx';

const chestImage = require('assets/img/chest.png') as string;

const Root = styled(Box)(() => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  gap: '2px',
}));

const Wrapper = styled(Box)(({ theme }) => ({
  border: `solid 1px ${alpha('#ffffff', 0.5)}`,
  borderRadius: '12px',
  backgroundColor: alpha(theme.palette.primary.main, 0.2),
  height: 82,
}));

const StyledImage = styled(Img)(() => ({
  width: 80,
  height: 80,
  minWidth: 80,
  minHeight: 80,
  objectFit: 'contain',
  padding: 6,
  borderRadius: '12px',
}));

type Props = BoxProps & { userChest: IUserChestDto };

function UserChestCard(props: Props) {
  const { userChest, ...other } = props;

  const chest = userChest?.chest;

  return (
    <Root {...other}>
      <Wrapper>
        <StyledImage
          src={chest?.image || chestImage}
          alt='chest'
          onError={(e) => {
            e.target.src = chestImage;
          }}
        />
      </Wrapper>

      <Typography variant='caption' align='center' color='text.secondary'>
        Amount: {userChest.count}
      </Typography>
    </Root>
  );
}

export default UserChestCard;
