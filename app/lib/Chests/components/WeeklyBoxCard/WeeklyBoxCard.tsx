import React, { useEffect, useRef, useState } from 'react';
import { IMysteryBoxUIProps } from '../DailyBoxCard/DailyBoxCard.ui.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTranslation } from 'react-i18next';
import DailyBoxRewardsDialog from 'lib/Chests/components/DailyBoxRewardsDialog/DailyBoxRewardsDialog.tsx';
import { getStartOfNextDay, getStartOfNextWeek } from 'lib/Chests/utils/date.ts';
import WeeklyBoxCardUi from 'lib/Chests/components/WeeklyBoxCard/WeeklyBoxCard.ui.tsx';
import { useUserWeeklyBox } from 'lib/Chests/hooks/useUserWeeklyBox.ts';
import { CHEST_V2_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import toaster from 'services/toaster.tsx';
import { claimChestWeekly } from 'lib/Chests/api-server/chest-weekly.store.ts';
import WeeklyBoxRewardsDialog from 'lib/Chests/components/WeeklyBoxRewardsDialog/WeeklyBoxRewardsDialog.tsx';

function WeeklyBoxCard(
  props: Omit<IMysteryBoxUIProps, 'total' | 'current' | 'onOpen' | 'loading'>
) {
  const { soon, ...other } = props;

  const account = useAccount();
  const { t } = useTranslation('chest');

  const chestAddress = CHEST_V2_CONTRACT_ID[account.chainId];

  const timeoutRef = useRef<any | null>();
  const weeklyBox = useUserWeeklyBox({
    userAddress: account.accountAddress,
    chestAddress: chestAddress,
    skip: soon,
  });

  const [openRewards, setOpenRewards] = useState(false);

  const rewardedAt = weeklyBox.data?.rewardedAt;
  const nextRewardAt = rewardedAt ? getStartOfNextWeek(new Date(rewardedAt)) : undefined;

  const handleClaim = async () => {
    try {
      await claimChestWeekly({ userAddress: account.accountAddress, chestAddress: chestAddress });
    } catch (e) {
      toaster.captureException(e);
    }
  };

  useEffect(() => {
    return () => {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
      }
    };
  }, []);

  return (
    <>
      <WeeklyBoxRewardsDialog open={openRewards} onClose={() => setOpenRewards(false)} />

      <WeeklyBoxCardUi
        count={weeklyBox.data?.chestsCount || 0}
        onOpen={handleClaim}
        rewarded={!!rewardedAt}
        soon={soon}
        nextRewardAt={nextRewardAt}
        onOpenRewards={() => setOpenRewards(true)}
        achievementRank={weeklyBox.data?.rangLevel}
        claimed={!!weeklyBox.data?.claimedAt}
        {...other}
      />
    </>
  );
}

export default WeeklyBoxCard;
