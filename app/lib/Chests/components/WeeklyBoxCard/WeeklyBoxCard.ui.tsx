import React from 'react';
import { useTranslation } from 'react-i18next';
import { Button } from '@ui-kit/Button/Button.tsx';
import Paper, { PaperProps } from '@mui/material/Paper';
import { alpha, styled } from '@mui/material/styles';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
// import misteryBox from 'assets/img/chests/mystery-box.png';
// import misteryBoxOpen from 'assets/img/chests/mystery-box-open.png';
import misteryBox from 'assets/img/chests/newyear.png';
import misteryBoxOpen from 'assets/img/chests/newyear.png';
import { ProgressBar } from '@ui-kit/ProgressBar/ProgressBar.tsx';
import Box from '@mui/material/Box';
import { Timer } from '@ui-kit/Timer/Timer.tsx';
import HelpBadgePopover from '@ui-kit/HelpBadge/HelpBadgePopover.tsx';
import Alert from '@mui/material/Alert';
import HelpBadge from '@ui-kit/HelpBadge/HelpBadge.tsx';
import Badge from '@mui/material/Badge';

const PREFIX = 'WeeklyBox';
const classes = {
  img: `${PREFIX}_img`,
  box: `${PREFIX}_box`,
  info: `${PREFIX}_info`,
  content: `${PREFIX}_content`,
};

const StyledBadge = styled(Badge)(() => ({
  '& .MuiBadge-badge': { bottom: 4, right: 8, minWidth: 16, height: 16, fontSize: 10 },
}));
const StyledRoot = styled(Paper)(({ theme }) => ({
  padding: 20,
  paddingRight: 16,
  paddingTop: 12,
  paddingBottom: 12,
  // backgroundColor: alpha(theme.palette.primary.main, 0.1),
  display: 'flex',
  alignItems: 'center',
  gap: 10,
  border: `solid 1px ${alpha(theme.palette.primary.main, 1)}`,

  [`& .${classes.img}`]: {
    width: 94,
    height: 94,
    marginLeft: -8,
    marginRight: 8,
  },
  [`& .${classes.box}`]: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    gap: 4,
    flex: 1,
  },
  [`& .${classes.content}`]: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    gap: 16,
  },
  [`& .${classes.info}`]: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
}));

export type IWeeklyBoxUIProps = {
  onOpen?: () => void;
  btnText?: string;
  disabled?: boolean;
  loading?: boolean;
  soon?: boolean;
  rewarded?: boolean;
  claimed?: boolean;
  nextRewardAt?: Date;
  onOpenRewards?: () => void;
  count?: number;
  achievementRank?: number;
} & PaperProps;

function DailyBoxCardUi(props: IWeeklyBoxUIProps) {
  const {
    disabled,
    onOpen,
    loading = false,
    soon,
    btnText = 'Open',
    rewarded = false,
    claimed = false,
    nextRewardAt,
    onOpenRewards,
    count = 1,
    achievementRank = 0,
    ...other
  } = props;

  const { t } = useTranslation('chest-daily');

  return (
    <StyledRoot variant='elevation' {...other}>
      <StyledBadge
        badgeContent={count}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        color='secondary'
      >
        <HelpBadge onClick={onOpenRewards}>
          <img
            src={(rewarded ? misteryBoxOpen : misteryBox) as string}
            alt=''
            className={classes.img}
          />
        </HelpBadge>
      </StyledBadge>

      <div className={classes.box}>
        <Typography variant='subtitle2' fontWeight='bold' color='text.primary'>
          {t('Ranked Box')}
        </Typography>

        <div className={classes.content}>
          <div className={classes.info}>
            <Typography
              variant='caption'
              color='text.secondary'
              sx={{ fontSize: 10, maxWidth: 120 }}
            >
              The number of boxes and their capacity depends on your rank
            </Typography>
            <Typography
              variant='caption'
              color='secondary.main'
              sx={{ fontSize: 10, maxWidth: 120 }}
            >
              Your Rank:{' '}
              <Typography
                variant='inherit'
                component='span'
                color='text.primary'
                sx={{ fontSize: 10 }}
              >
                {achievementRank}
              </Typography>
            </Typography>
          </div>

          <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', gap: '3px' }}>
            <Button
              sx={{ maxWidth: 90, minWidth: 90 }}
              fullWidth
              onClick={onOpen}
              variant='contained'
              color='primary'
              disabled={disabled || soon || achievementRank === 0 || claimed}
              loading={loading}
            >
              {claimed
                ? 'Pending...'
                : achievementRank === 0
                ? 'No Rank'
                : soon
                ? t('Soon')
                : t(btnText)}
            </Button>

            {soon ? null : claimed && !rewarded ? (
              <Typography
                variant='caption'
                fontWeight='bold'
                sx={{ whiteSpace: 'nowrap', fontSize: 10 }}
              >
                Wait for reward
              </Typography>
            ) : claimed ? (
              <Typography
                variant='caption'
                fontWeight='bold'
                color='text.primary'
                sx={{ whiteSpace: 'nowrap', fontSize: 10 }}
              >
                {nextRewardAt ? <Timer date={nextRewardAt} /> : '-'}
              </Typography>
            ) : (
              <Typography
                variant='caption'
                fontWeight='bold'
                color='secondary'
                sx={{ whiteSpace: 'nowrap', fontSize: 10 }}
              >
                Ready
              </Typography>
            )}
          </Box>
        </div>
      </div>
    </StyledRoot>
  );
}

export default DailyBoxCardUi;
