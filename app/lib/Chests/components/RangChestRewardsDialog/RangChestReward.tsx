import { styled } from '@mui/material/styles';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import React from 'react';
import Box, { BoxProps } from '@mui/material/Box';
import { IChestDailyReward } from 'lib/Chests/utils/chest-daily-rewards.ts';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';

const StyledRoot = styled(Box)(() => ({
  padding: '3px 32px',
  display: 'flex',
  justifyContent: 'space-between',
}));

type Props = BoxProps & { reward: IChestDailyReward; tradeSymbol?: string };

function getLabel(reward: IChestDailyReward, { tradeSymbol }: { tradeSymbol: string }): string {
  switch (reward.type) {
    case 'nft':
      return `NFT ${getNftRarityLabel(reward.rarity)}`;
    case 'coin':
      return `${reward.amount} ${tradeSymbol || ''}`;
    case 'pixel':
      return `${reward.amount} PXLs`;
  }

  return '';
}

function RangChestReward(props: Props) {
  const { reward, tradeSymbol, ...other } = props;

  return (
    <StyledRoot {...other}>
      <Typography
        variant='subtitle2'
        fontWeight='bold'
        sx={{
          color: reward.rarity
            ? getNftRarityColor(reward.rarity)
            : reward.type === 'coin'
            ? 'text.secondary'
            : 'text.primary',
        }}
      >
        {getLabel(reward, { tradeSymbol })}
      </Typography>
      <Typography variant='subtitle2' fontWeight='bold' color='text.primary'>
        {reward.chance.toFixed(2)}%
      </Typography>
    </StyledRoot>
  );
}

export default RangChestReward;
