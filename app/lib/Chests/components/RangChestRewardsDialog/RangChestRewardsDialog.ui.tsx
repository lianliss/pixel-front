import React, { useEffect, useMemo, useState } from 'react';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTranslation } from 'react-i18next';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { styled } from '@mui/material/styles';
import RangChestReward from 'lib/Chests/components/RangChestRewardsDialog/RangChestReward.tsx';
import Box from '@mui/material/Box';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import AchievementRangGroup from 'lib/Chests/components/AchievementRangGroup/AchievementRangGroup.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Divider from '@mui/material/Divider';
import { useUserAchievementRang } from 'lib/Achievement/hooks/useUserAchievementRang.ts';
import { useUserAchievementRangList } from 'lib/Achievement/hooks/useUserAchievementRangList.ts';
import { useChestDaily } from 'lib/Chests/hooks/useChestDaily.ts';
import { convertChestDropsToRewards } from 'lib/Chests/utils/convert/convert-chest-drop-to-reward.ts';
import CircularProgress from '@mui/material/CircularProgress';
import { IChestDto } from 'lib/Chests/api-server/chest.types.ts';

const Rewards = styled(Box)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  margin: '0 -32px',

  '& > *:nth-child(2n + 1)': {
    backgroundColor: theme.palette.background.back1,
  },
}));

type Props = {
  open: boolean;
  onClose?: () => void;
  loading?: boolean;
  chest?: IChestDto;
  level?: number;
  userLevel?: number;
  onChangeLevel?: (level: number) => void;
  rangName?: string;
  symbol?: string;
};

function RangChestRewardsDialogUi(props: Props) {
  const { open, onClose, chest, loading, level, userLevel, onChangeLevel, rangName, symbol } =
    props;

  const { t } = useTranslation('chest-daily');

  const rewards = useMemo(() => {
    return convertChestDropsToRewards(chest?.drops || []);
  }, [chest]);

  return (
    <BottomDialog
      open={open}
      onClose={onClose}
      title={t('Mystery box')}
      subTitle={t('What you can find inside')}
    >
      <Divider sx={{ mb: 2, mx: '-32px', mt: 1 }} />

      <div>
        <Typography fontWeight='bold' sx={{ mb: '6px' }}>
          Rank:{' '}
          <Typography
            component='span'
            variant='inherit'
            sx={{
              color:
                typeof level === 'undefined' || userLevel === level
                  ? 'success.main'
                  : 'text.secondary',
            }}
          >
            {rangName || 'No rank'}
          </Typography>
        </Typography>
        <AchievementRangGroup
          current={userLevel}
          level={level}
          onChangeLevel={(v) => onChangeLevel?.(v)}
        />
      </div>

      {chest?.drops.length && !loading && (
        <Rewards sx={{ mt: 2, mb: 4 }}>
          {rewards.map((reward, i) => (
            <RangChestReward key={`reward-${i}`} reward={reward} tradeSymbol={symbol} />
          ))}
        </Rewards>
      )}

      {loading && (
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: 410,
            mt: 2,
            mb: 4,
          }}
        >
          <CircularProgress />
        </Box>
      )}
    </BottomDialog>
  );
}

export default RangChestRewardsDialogUi;
