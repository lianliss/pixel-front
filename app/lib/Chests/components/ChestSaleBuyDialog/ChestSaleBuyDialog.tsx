import React, { useMemo } from 'react';
import styles from './ChestSaleBuyDialog.module.scss';
import getFinePrice from 'utils/getFinePrice';
import { Button } from '@ui-kit/Button/Button.tsx';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { useTranslation } from 'react-i18next';
import { IChestSaleBox } from 'lib/Chests/utils/constants.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import BuyGasAlertLazy from 'lib/TelegramStars/components/BuyGasAlert/BuyGasAlert.lazy.tsx';
import BuyTokenAlertLazy from 'lib/TelegramStars/components/BuyTokenAlert/BuyTokenAlert.lazy.tsx';
import Alert from '@mui/material/Alert';
import { IChainChestSale, IChestSale } from 'lib/Chests/hooks/sale/useChestSales.ts';

type Props = {
  box: IChestSaleBox;
  sale: IChestSale;
  count?: number;
  onBuy?: (sale: IChestSale) => void;
  loading?: boolean;
  actionLoading?: boolean;
  gasBalanceAmount?: number;
  tradeBalanceAmount?: number;

  receipt?: {
    isConfirming?: boolean;
    isConfirmed?: boolean;
    isFailed?: boolean;
  };
  tx?: {
    isPending?: boolean;
    isSuccess?: boolean;
    isError?: boolean;
    isDelayed?: boolean;
  };
};

function ChestSaleBuyDialog({
  sale,
  isOpen,
  onClose,
  onBuy,
  loading = false,
  actionLoading = false,
  gasBalanceAmount = 0,
  tradeBalanceAmount = 0,
  tx,
  receipt,
}: Props) {
  const { t } = useTranslation('chest-sale');
  const handleClaim = () => {
    onBuy?.(sale);
  };

  const noTradeBalance = tradeBalanceAmount < sale.chain?.price;
  const noGas = !gasBalanceAmount;

  if (!isOpen || !sale) {
    return null;
  }

  return (
    <BottomDialog isOpen onClose={actionLoading ? undefined : onClose} className={styles.buy}>
      <h2 style={{ textAlign: 'center' }}>{sale.title}</h2>
      <p style={{ textAlign: 'center' }}>
        {t('Limited')} {sale.chain?.amount} / {sale.total || 0}
      </p>
      <h3 style={{ textAlign: 'center' }}>{t('Inside the Chest You can find')}</h3>

      <div className={styles.buyScroller}>
        <div className={styles.buyChances}>
          {sale.ratios.map((chance, index) => {
            if (!chance.percent) {
              return <React.Fragment key={`drop-${index}`}></React.Fragment>;
            }

            return (
              <div className={styles.buyItem} key={index}>
                <div
                  className={styles.buyItemImage}
                  style={{
                    borderColor: getNftRarityColor(index || 1),
                  }}
                >
                  <img src={chance.image} alt={''} />
                </div>
                <div className={styles.buyItemTitle}>
                  {index ? `NFT ${getNftRarityLabel(index)}` : 'PXLs drop'}
                </div>

                <div className={styles.buyItemChance}>{getFinePrice(chance.percent)}%</div>
              </div>
            );
          })}
        </div>
      </div>

      {noGas && (
        <BuyGasAlertLazy
          sx={{
            width: '100%',
            mb: 1,
          }}
          text='No enough gas'
        />
      )}

      {!noGas && noTradeBalance && (
        <BuyTokenAlertLazy
          sx={{
            width: '100%',
            mb: 1,
          }}
          text='No enough funds'
        />
      )}

      <Typography
        variant='caption'
        color='text.secondary'
        sx={{ mb: 1, display: 'block' }}
        align='center'
      >
        {t('Current Balance')}: {getFinePrice(tradeBalanceAmount)} {sale.symbol}
      </Typography>

      {receipt?.isFailed && (
        <Alert severity='warning' variant='outlined' sx={{ mb: 2 }}>
          {t('Transaction Failed, please try Again')}
        </Alert>
      )}

      <Button
        fullWidth
        size='large'
        variant='contained'
        color='primary'
        disabled={noGas || noTradeBalance || tx?.isDelayed}
        loading={loading || (receipt?.isConfirming && !receipt?.isFailed) || tx?.isPending}
        loadingText={
          tx?.isDelayed
            ? t('Wait for delay')
            : tx?.isPending
            ? `${t('Wait for transaction')}..`
            : !receipt?.isFailed && receipt?.isConfirming
            ? `${t('Wait for confirmation')}..`
            : undefined
        }
        onClick={handleClaim}
      >
        {sale?.chain.amount ? (
          <>
            {t('Buy for')} {`${getFinePrice(sale?.chain.price)} ${sale.symbol}`}
          </>
        ) : (
          t('Sold out')
        )}
      </Button>
    </BottomDialog>
  );
}

export default React.memo(ChestSaleBuyDialog);
