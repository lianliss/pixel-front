import React, { useState } from 'react';
import ChestSaleBuyDialog from 'lib/Chests/components/ChestSaleBuyDialog/ChestSaleBuyDialog.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import Paper, { PaperProps } from '@mui/material/Paper';
import toaster from 'services/toaster.tsx';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { useCoreToken } from '@chain/hooks/useCoreToken.ts';
import { useGasBalance } from '@chain/hooks/useGasBalance.ts';
import { useTranslation } from 'react-i18next';
import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';
import ChestSaleRow from 'lib/Chests/components/ChestSaleRow/ChestSaleRow.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useBalance } from '@chain/hooks/useBalance.ts';
import { IChainChestSale, IChestSale, useChestSales } from 'lib/Chests/hooks/sale/useChestSales.ts';
import { styled } from '@mui/material/styles';
import { useChestSaleBuy } from 'lib/Chests/hooks/sale/useChestSaleBuy.ts';
import { useChestSaleDrop } from 'lib/Chests/hooks/sale/useChestSaleDrop.ts';
import ChestDropDialog from 'lib/Chests/components/ChestDropDialog/ChestDropDialog.tsx';
import Button from '@mui/material/Button';
import getFinePrice from 'utils/getFinePrice';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import pixelImage from 'assets/img/token/common/pixel.png';

const StyledPaper = styled(Paper)(() => ({
  padding: 8,
}));

function ChestSales(props: PaperProps) {
  const account = useAccount();

  const { t } = useTranslation('chest-sale');

  // ========== BALANCE ==========
  const tradeToken = useTradeToken();
  const coreToken = useCoreToken();
  const tradeBalance = useBalance({
    address: account.accountAddress,
    token: tradeToken?.address,
    skip: !tradeToken?.address,
  });
  const coreBalance = useBalance({
    address: account.accountAddress,
    token: coreToken.data?.address,
    skip: !coreToken.data.address,
  });
  const gasBalance = useGasBalance({
    address: account.accountAddress,
    skip: !account.isConnected,
  });

  const tradeBalanceAmount = tradeBalance.data?.formatted || 0;
  const coreBalanceAmount = coreBalance.data?.formatted || 0;
  const gasBalanceAmount = gasBalance.data?.formatted || 0;
  // ========== END BALANCE ==========

  // ========== CHEST SALE ==========
  const chestSaleBuy = useChestSaleBuy();
  const chestSalesResult = useChestSales();
  const chestSales = (chestSalesResult.data || []) as IChestSale[];
  const chestSaleDrop = useChestSaleDrop({ receipt: chestSaleBuy.receipt.data });
  // ========== END CHEST SALE ==========

  // ========== STATE ==========
  const [sale, setSale] = React.useState<IChestSale | null>(null);
  const [buyLoading, setBuyLoading] = useState(false);
  // ========== END STATE ==========

  // ========== HANDLERS ==========
  const handleBuy = async (sale: IChestSale) => {
    Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
    setBuyLoading(true);

    try {
      await chestSaleBuy.buy(sale.chain!);

      await tradeBalance.refetch();
      await gasBalance.refetch();
      await coreBalance.refetch();
      await chestSalesResult.refetch();

      // setSale(null);

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      toaster.captureException(e);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    } finally {
      setBuyLoading(false);
    }
  };
  // ========== END HANDLERS ==========

  if (!chestSales.length) {
    return null;
  }

  return (
    <StyledPaper elevation={2} {...props}>
      <Typography fontWeight='bold' align='center'>
        {t('NFT Sale')}
      </Typography>

      <Stack direction='column' gap={0.5} divider={<Divider />}>
        {chestSales.map((sale, i) => (
          <ChestSaleRow
            key={`sale-${sale.id}`}
            sale={sale}
            loading={chestSalesResult.loading}
            disabled={!sale.chain?.amount}
            onBuy={() => {
              setSale(sale);
            }}
          />
        ))}
      </Stack>

      {!!sale && (
        <ChestSaleBuyDialog
          isOpen={!!sale}
          sale={sale}
          onClose={() => setSale(null)}
          onBuy={handleBuy}
          loading={buyLoading || tradeBalance.isLoading}
          actionLoading={buyLoading}
          gasBalanceAmount={gasBalanceAmount}
          receipt={{
            isConfirming: chestSaleBuy.receipt.isConfirming,
            isConfirmed: chestSaleBuy.receipt.isConfirmed,
            isFailed: chestSaleBuy.receipt.isFailed,
          }}
          tx={{
            isPending: chestSaleBuy.tx.isPending,
            isSuccess: chestSaleBuy.tx.isSuccess,
            isError: chestSaleBuy.tx.isError,
            isDelayed: chestSaleBuy.tx.isDelayed,
          }}
          tradeBalanceAmount={sale.chain?.isShardCurrency ? coreBalanceAmount : tradeBalanceAmount}
        />
      )}
      {!!chestSaleDrop?.tokens && (
        <BottomDialog isOpen onClose={() => chestSaleDrop.clear()}>
          <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <Typography align='center' variant='h2' fontWeight='bold'>
              {t('You have received')}{' '}
            </Typography>

            <img
              src={pixelImage as string}
              alt=''
              style={{ marginTop: 16, width: 85, height: 85 }}
            />

            <Typography variant='h2' fontWeight='bold' align='center' sx={{ mt: 2 }}>
              {chestSaleDrop.tokens && getFinePrice(chestSaleDrop.tokens)} {coreToken.data?.symbol}
            </Typography>
          </div>

          <Button
            size='large'
            fullWidth
            variant='contained'
            color='primary'
            onClick={() => chestSaleDrop.clear()}
            sx={{ mt: 3 }}
          >
            OK
          </Button>
        </BottomDialog>
      )}
      {!!chestSaleDrop.nft && (
        <ChestDropDialog nftItem={chestSaleDrop.nft} onClose={() => chestSaleDrop.clear()} />
      )}
    </StyledPaper>
  );
}

export default ChestSales;
