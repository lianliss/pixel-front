import React, { useMemo, useState } from 'react';
import styles from './ChestSaleRow.module.scss';
import { useAccount } from '@chain/hooks/useAccount.ts';
import Paper, { PaperProps } from '@mui/material/Paper';
import { Button } from '@ui-kit/Button/Button.tsx';
import clsx from 'clsx';
import { useGasBalance } from '@chain/hooks/useGasBalance.ts';
import { useTranslation } from 'react-i18next';
import { Timer } from '@ui-kit/Timer/Timer.tsx';
import { IChestSaleBox } from 'lib/Chests/utils/constants.ts';
import moment from 'moment';
import Skeleton from '@mui/material/Skeleton';
import { IChestSale } from 'lib/Chests/hooks/sale/useChestSales.ts';

const getBtnText = ({ chestCount, t }) => {
  if (typeof chestCount === 'undefined') {
    return 'Not available';
  }

  return chestCount ? t('OPEN') : t('SOLD OUT');
};

type Props = PaperProps & {
  sale: IChestSale;
  //
  onBuy?: (box: IChestSaleBox) => void;
  onStart?: () => void;
  disabled?: boolean;
  loading?: boolean;
};

function ChestSaleRow(props: Props) {
  const { sale, onBuy, onStart, disabled, loading, ...other } = props;

  const { t } = useTranslation('mining');

  const [started, updateStarted] = useState(false);

  const isCountDown = useMemo(() => {
    if (!sale.countdown) {
      return false;
    }

    const inverseOffset = moment().utcOffset();
    const now = moment();
    const target = moment(sale.countdown).add(inverseOffset / 60, 'hour');

    return target >= now;
  }, [sale.countdown, started]);

  const disabledBox = disabled || !sale.chain?.amount;
  const btnText = getBtnText({
    t,
    chestCount: sale.chain?.amount || 0,
  });

  return (
    <Paper elevation={1} className={clsx(styles.token, { disabled: disabledBox })} {...other}>
      <div className={styles.tokenInfo}>
        <div className={styles.tokenInfoIcon}>
          <img src={sale.image} className={styles.tokenInfoIconBox} alt={''} />
        </div>
        <div className={styles.tokenInfoTitle}>
          <div className={styles.tokenInfoTitleSymbol}>{sale.title}</div>
          <div className={styles.tokenInfoTitleName}>
            {loading ? (
              <Skeleton sx={{ width: 70 }} />
            ) : (
              `${!isCountDown ? sale.chain?.amount || 0 : sale.total} / ${sale.total}`
            )}
          </div>
        </div>
      </div>
      <div className={styles.tokenBalance}>
        <div className={styles.tokenBalanceAction}>
          {isCountDown ? (
            <Button disabled color='primary' variant='contained'>
              <Timer
                date={sale.countdown}
                onStart={() => {
                  updateStarted(true);
                  onStart?.();
                }}
              />
            </Button>
          ) : (
            <Button
              variant='contained'
              color='primary'
              loading={loading}
              disabled={disabledBox}
              onClick={() => {
                onBuy?.(sale);
              }}
            >
              {btnText}
            </Button>
          )}
        </div>
      </div>
    </Paper>
  );
}

export default ChestSaleRow;
