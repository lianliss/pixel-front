import React, { useEffect } from 'react';
import styles from './WeeklyBox.module.scss';
import toaster from 'services/toaster.tsx';
import Drop from 'lib/Chests/components/ChestDropDialog/ChestDropDialog.tsx';
import Countdown from 'ui/Countdown/Countdown';
import Paper from '@mui/material/Paper';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useFreeChestApi } from 'lib/Chests/hooks/deprecated/useOldUserChests.ts';
import { useFreeChestDropFromTx } from 'lib/Chests/hooks/drop/useFreeChestDropFromTx.ts';
import { useMiningStorage } from 'lib/Mining/hooks/useMiningStorage.ts';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { darken, styled } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';
import Box from '@mui/material/Box';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

const StyledBar = styled(`div`)(({ theme }) => ({
  backgroundColor: darken(theme.palette.background.paper, 0.3),

  [`&.filled`]: {
    backgroundColor: theme.palette.success.main,
  },
}));

const DAY = 1000 * 3600 * 24;
const WEEK = DAY * 7;

function WeeklyBox({ noGas = false }: { noGas?: boolean }) {
  const account = useAccount();
  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;

  //
  const minerAddress = MINER_CONTRACT_ID[account.chainId].core;

  const { t } = useTranslation('chest');

  const miningStorage = useMiningStorage({
    minerAddress,
    telegramId: telegramId,
  });
  const { claimTimestamp, extra: { activity: userActivity } = {} } = miningStorage.data || {};

  const [_timestamp, setTimestamp] = React.useState(Date.now());

  const activity = userActivity * 1000;
  const fromLastClaim = Date.now() - claimTimestamp * 1000;
  const isChestAvailable = activity >= WEEK;

  const activeDays = isChestAvailable ? 7 : fromLastClaim > DAY ? 0 : Math.floor(activity / DAY);

  const toNextFill = fromLastClaim % DAY;
  const nextFillDate = Date.now() + DAY - toNextFill;

  const days = [];
  for (let i = 0; i < 7; i++) {
    days.push(activeDays >= i + 1);
  }

  const freeChestApi = useFreeChestApi();
  const freeChestDrop = useFreeChestDropFromTx();

  const [isLoad, setIsLoad] = React.useState(false);

  const handleClaim = async () => {
    setIsLoad(true);

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      const receipt = await freeChestApi.claimWeekly();
      await freeChestDrop.load(receipt);

      await freeChestApi.reload();
      await miningStorage.refetch();

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (error) {
      toaster.logError(error);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }

    setIsLoad(false);
  };

  const daysToHide = (new Date('2024-11-20') - new Date()) / 1000 / 60 / 60 / 24;

  if (7 - activeDays >= daysToHide) {
    return null;
  }

  return (
    <Paper elevation={2} className={styles.weekly} sx={{ padding: 1.5 }}>
      <Box className={styles.token}>
        <div className={styles.tokenInfo}>
          <div
            className={styles.tokenInfoIcon}
            style={{
              borderColor: 'rgba(236, 25, 195, 1)',
            }}
          >
            <img
              src='https://storage.hellopixel.network/assets/chests/free.png'
              className={styles.tokenInfoIconBox}
              alt={''}
            />
          </div>
          <div className={styles.tokenInfoTitle}>
            <div className={styles.tokenInfoTitleSymbol}>{t('Free Box')}</div>
            <div className={styles.tokenInfoTitleName}>{t('Collect every week')}</div>
          </div>
        </div>
        <div>
          <Button
            variant='contained'
            color='primary'
            loading={isLoad}
            disabled={isLoad || !isChestAvailable || noGas}
            onClick={() => {
              if (isChestAvailable) handleClaim();
            }}
          >
            {noGas && 'No Gas'}
            {!noGas && (isChestAvailable ? t('OPEN') : t('PENDING'))}
          </Button>
        </div>
      </Box>

      <div className={styles.columns}>
        <div>{t('For seven day claim activity')}</div>
        <div>
          {isChestAvailable ? (
            t('Ready to Open')
          ) : (
            <>
              <Countdown time={nextFillDate} onComplete={() => setTimestamp(Date.now())} />
            </>
          )}
        </div>
      </div>

      <div className={styles.bars}>
        {days.map((value, index) => {
          return (
            <StyledBar key={index} className={[styles.item, value ? 'filled' : ''].join(' ')} />
          );
        })}
      </div>
      {!!freeChestDrop.nft && (
        <Drop nftItem={freeChestDrop.nft} onClose={() => freeChestDrop.clear()} />
      )}
    </Paper>
  );
}

export default WeeklyBox;
