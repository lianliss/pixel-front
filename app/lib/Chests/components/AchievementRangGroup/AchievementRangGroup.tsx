import { alpha, styled } from '@mui/material/styles';
import React from 'react';
import Box, { BoxProps } from '@mui/material/Box';
import { IconButton } from '@ui-kit/IconButton/IconButton.tsx';

const Root = styled(Box)(() => ({
  display: 'flex',
  justifyContent: 'space-between',
  gap: 4,
}));
const Item = styled(IconButton)(({ theme, selected, current }) => ({
  padding: '10px',
  display: 'flex',
  justifyContent: 'space-between',
  border: `solid 4px ${theme.palette.background.back1}`,
  background: alpha('#7EA1CA', 0.3),

  ...(selected && {
    background: '#7EA1CA',
    outline: `solid 1px ${alpha('#7EA1CA', 0.2)}`,

    '&:hover': {
      background: '#7EA1CA',
    },
  }),
  ...(current && {
    background: theme.palette.success.main,
    outline: `solid 1px ${alpha(theme.palette.success.main, 0.2)}`,

    '&:hover': {
      background: theme.palette.success.main,
    },
  }),
  ...(current &&
    !selected && {
      background: alpha(theme.palette.success.main, 0.4),

      '&:hover': {
        background: alpha(theme.palette.success.main, 0.4),
      },
    }),
}));

type Props = BoxProps & {
  current: number;
  level?: number;
  onChangeLevel?: (level: number) => void;
};

const list = Array.from({ length: 10 }, (_, i) => i + 1);

function AchievementRangGroup(props: Props) {
  const { level, current, onChangeLevel, ...other } = props;

  return (
    <Root {...other}>
      {list.map((el) => (
        <Item
          key={`level-${el}`}
          selected={el === level}
          current={el === current}
          onClick={() => onChangeLevel?.(el)}
        />
      ))}
    </Root>
  );
}

export default AchievementRangGroup;
