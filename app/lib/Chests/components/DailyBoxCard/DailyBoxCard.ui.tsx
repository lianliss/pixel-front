import React from 'react';
import { useTranslation } from 'react-i18next';
import { Button } from '@ui-kit/Button/Button.tsx';
import Paper, { PaperProps } from '@mui/material/Paper';
import { alpha, styled } from '@mui/material/styles';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
// import misteryBox from 'assets/img/chests/mystery-box.png';
// import misteryBoxOpen from 'assets/img/chests/mystery-box-open.png';
import misteryBox from 'assets/img/chests/newyear.png';
import misteryBoxOpen from 'assets/img/chests/newyear.png';
import { ProgressBar } from '@ui-kit/ProgressBar/ProgressBar.tsx';
import Box from '@mui/material/Box';
import { Timer } from '@ui-kit/Timer/Timer.tsx';
import HelpBadgePopover from '@ui-kit/HelpBadge/HelpBadgePopover.tsx';
import Alert from '@mui/material/Alert';
import HelpBadge from '@ui-kit/HelpBadge/HelpBadge.tsx';

const PREFIX = 'MysteryBox';
const classes = {
  img: `${PREFIX}_img`,
  box: `${PREFIX}_box`,
  info: `${PREFIX}_info`,
  content: `${PREFIX}_content`,
  progress: `${PREFIX}_progress`,
};

const StyledRoot = styled(Paper)(({ theme }) => ({
  padding: 20,
  paddingRight: 16,
  paddingTop: 12,
  paddingBottom: 12,
  // backgroundColor: alpha(theme.palette.primary.main, 0.1),
  display: 'flex',
  alignItems: 'center',
  gap: 10,
  border: `solid 1px ${alpha(theme.palette.primary.main, 1)}`,

  [`& .${classes.img}`]: {
    width: 94,
    height: 94,
    marginLeft: -8,
    marginRight: 8,
  },
  [`& .${classes.box}`]: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    gap: 4,
    flex: 1,
  },
  [`& .${classes.content}`]: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    gap: 16,
  },
  [`& .${classes.info}`]: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  [`& .${classes.progress}`]: {
    display: 'flex',
    flexDirection: 'column',
    gap: 4,
  },
}));

export type IMysteryBoxUIProps = {
  current?: number;
  total?: number;
  onOpen?: () => void;
  btnText?: string;
  disabled?: boolean;
  loading?: boolean;
  soon?: boolean;
  rewarded?: boolean;
  claimed?: boolean;
  nextRewardAt?: Date;
  onOpenRewards?: () => void;
} & PaperProps;

function DailyBoxCardUi(props: IMysteryBoxUIProps) {
  const {
    disabled,
    onOpen,
    current = 0,
    total = 0,
    loading = false,
    soon,
    btnText = 'Open',
    rewarded = false,
    claimed = false,
    nextRewardAt,
    onOpenRewards,
    ...other
  } = props;

  const { t } = useTranslation('chest-daily');

  return (
    <StyledRoot variant='elevation' {...other}>
      <HelpBadge onClick={onOpenRewards}>
        <img
          src={(rewarded ? misteryBoxOpen : misteryBox) as string}
          alt=''
          className={classes.img}
        />
      </HelpBadge>

      <div className={classes.box}>
        <Typography variant='subtitle2' fontWeight='bold' color='text.primary'>
          {t('Mystery Box')}
        </Typography>

        <div className={classes.content}>
          <div className={classes.info}>
            <Typography
              variant='caption'
              color='text.secondary'
              sx={{ fontSize: 10, maxWidth: 120 }}
            >
              {rewarded || claimed
                ? `${t('Next tasks for claim the Box in')}:`
                : t('Complete all tasks for claim the Box')}
            </Typography>
          </div>
          {!soon && claimed && !rewarded ? (
            <Typography variant='caption' fontWeight='bold' sx={{ whiteSpace: 'nowrap' }}>
              Wait for reward
            </Typography>
          ) : (!soon && rewarded) || claimed ? (
            <Typography
              variant='subtitle2'
              fontWeight='bold'
              color='text.primary'
              sx={{ whiteSpace: 'nowrap' }}
            >
              {nextRewardAt ? <Timer short date={nextRewardAt} /> : '-'}
            </Typography>
          ) : (
            <Button
              sx={{ maxWidth: 80 }}
              fullWidth
              onClick={onOpen}
              variant='contained'
              color='primary'
              disabled={disabled || soon}
              loading={loading}
            >
              {soon ? t('Soon') : t(btnText)}
            </Button>
          )}
        </div>

        <div className={classes.progress} style={{ opacity: rewarded ? 0.4 : 1 }}>
          <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
            <Typography variant='caption' color='text.primary' sx={{ fontSize: 10 }}>
              {t('Tasks')}
            </Typography>
            <Typography variant='caption' color='success.main' sx={{ fontSize: 10 }}>
              {current}/{total}
            </Typography>
          </Box>
          <ProgressBar total={total} value={current} size='medium' style={{ marginLeft: 0 }} />
        </div>
      </div>
    </StyledRoot>
  );
}

export default DailyBoxCardUi;
