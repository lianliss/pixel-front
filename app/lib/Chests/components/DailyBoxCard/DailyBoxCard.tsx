import React, { useEffect, useRef, useState } from 'react';
import DailyBoxCardUi, { IMysteryBoxUIProps } from './DailyBoxCard.ui.tsx';
import { useUserDailyBox } from 'lib/Chests/hooks/useUserDailyBox.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import ChestDailyDialog from 'lib/Chests/components/ChestDailyDialog/ChestDailyDialog.tsx';
import { useTranslation } from 'react-i18next';
import DailyBoxRewardsDialog from 'lib/Chests/components/DailyBoxRewardsDialog/DailyBoxRewardsDialog.tsx';
import { useUserAchievementRang } from 'lib/Achievement/hooks/useUserAchievementRang.ts';
import { getStartOfNextDay } from 'lib/Chests/utils/date.ts';
import { Chain } from 'services/multichain/chains';
import { AchievementGroup } from 'lib/Achievement/api-server/achievement.types.ts';

function DailyBoxCard(props: Omit<IMysteryBoxUIProps, 'total' | 'current' | 'onOpen' | 'loading'>) {
  const { soon, ...other } = props;

  const timeoutRef = useRef<any | null>();
  const { t } = useTranslation('chest-daily');
  const account = useAccount();
  const daily = useUserDailyBox({
    userAddress: account.accountAddress,
    chainId: account.chainId,
    skip: soon,
  });
  const achievementUserRang = useUserAchievementRang(account.accountAddress, {
    group: AchievementGroup.Default,
  });

  const [open, setOpen] = useState(false);
  const [openRewards, setOpenRewards] = useState(false);

  const rewardedAt = daily.data?.rewardedAt;
  const totalTasks = account.chainId === Chain.SKALE ? 2 : 3;
  const completedTasks = (
    account.chainId === Chain.SKALE
      ? [daily.data?.isZarClaimed, daily.data?.isSgbClaimed]
      : [
          daily.data?.isNewsRead,
          daily.data?.isSkaleClaimed,
          daily.data?.isQuestCompleted || daily.data?.isPartner1,
        ]
  ).filter(Boolean).length;
  const nextRewardAt = rewardedAt ? getStartOfNextDay(new Date(rewardedAt)) : undefined;

  useEffect(() => {
    return () => {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
      }
    };
  }, []);

  return (
    <>
      <DailyBoxRewardsDialog open={openRewards} onClose={() => setOpenRewards(false)} />

      <DailyBoxCardUi
        current={completedTasks}
        total={totalTasks}
        onOpen={() => setOpen(true)}
        btnText={t('More')}
        rewarded={!!rewardedAt}
        soon={soon}
        nextRewardAt={nextRewardAt}
        onOpenRewards={() => setOpenRewards(true)}
        loading={daily.loading}
        {...other}
      />
      <ChestDailyDialog
        dailyChest={daily.data}
        dailyChestLoading={daily.loading}
        open={open}
        achievementRang={achievementUserRang.data?.level || 0}
        achievementRangLoading={achievementUserRang.loading}
        onClose={() => setOpen(false)}
        onOpenRewards={() => setOpenRewards(true)}
        onClaim={() => {
          daily.refetch();

          timeoutRef.current = setTimeout(() => {
            daily.refetch();
          }, 5000);
        }}
      />
    </>
  );
}

export default DailyBoxCard;
