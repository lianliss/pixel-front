import React, { useEffect, useMemo, useState } from 'react';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { useUserAchievementRang } from 'lib/Achievement/hooks/useUserAchievementRang.ts';
import { useUserAchievementRangList } from 'lib/Achievement/hooks/useUserAchievementRangList.ts';
import { useChestDaily } from 'lib/Chests/hooks/useChestDaily.ts';
import RangChestRewardsDialogUi from 'lib/Chests/components/RangChestRewardsDialog/RangChestRewardsDialog.ui.tsx';
import { Chain } from 'services/multichain/chains';
import { AchievementGroup } from 'lib/Achievement/api-server/achievement.types.ts';

const Rewards = styled(Box)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  margin: '0 -32px',

  '& > *:nth-child(2n + 1)': {
    backgroundColor: theme.palette.background.back1,
  },
}));

type Props = {
  open: boolean;
  onClose?: () => void;
};

function DailyBoxRewardsDialog(props: Props) {
  const { open, onClose } = props;

  const account = useAccount();
  const tradeToken = useTradeToken();

  const achievementUserRang = useUserAchievementRang(account.accountAddress);
  const achievementsRangList = useUserAchievementRangList({
    skip: !open,
    group: account.chainId === Chain.UNITS ? AchievementGroup.Units : AchievementGroup.Default,
  });

  const [selectedLevel, setSelectedLevel] = useState<number | undefined>(undefined);
  const chest = useChestDaily({ rang: selectedLevel });

  const currentLevel = achievementUserRang.data?.level || 0;

  const selectedRang = useMemo(() => {
    const findLevel = selectedLevel || currentLevel;
    const result = achievementsRangList.data.find((el) => el.level === findLevel);

    return result;
  }, [selectedLevel, currentLevel, achievementsRangList.data]);

  useEffect(() => {
    if (!currentLevel) {
      setSelectedLevel(1);
    } else {
      setSelectedLevel(currentLevel);
    }
  }, [currentLevel]);

  return (
    <RangChestRewardsDialogUi
      open={open}
      onClose={onClose}
      level={selectedLevel}
      onChangeLevel={(v) => setSelectedLevel(v)}
      userLevel={currentLevel}
      chest={chest.data}
      symbol={tradeToken.symbol}
      rangName={selectedRang?.name}
    />
  );
}

export default DailyBoxRewardsDialog;
