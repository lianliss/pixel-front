import { Typography } from '@ui-kit/Typography/Typography.tsx';
import React, { useEffect } from 'react';
import { styled } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';
import UserChestCard from 'lib/Chests/components/UserChestCard/UserChestCard.tsx';
import Grid from '@mui/material/Grid';
import { IUserChestDto } from 'lib/Chests/api-server/chest.types.ts';

const StyledItems = styled('div')(({ theme }) => ({
  backgroundColor: theme.palette.background.default,
  padding: '8px 16px 16px',
}));

type Props = {
  // v1
  chests: IUserChestDto[];
  chestsLoading?: boolean;
  chestActionLoading?: boolean;
  onClaimChest?: (chest: IUserChestDto) => void;

  // v2
  userChests: IUserChestDto[];
  userChestsLoading: boolean;
  onClaimUserChest?: (chest: IUserChestDto) => void;
};

function UserChestsGrid(props: Props) {
  const { t } = useTranslation('inventory');

  const {
    chests,
    chestsLoading,
    chestActionLoading,
    onClaimChest,
    userChestsLoading,
    userChests = [],
    onClaimUserChest,
  } = props;

  return (
    <StyledItems>
      <div>
        <Typography
          variant='caption'
          component='p'
          color='text.secondary'
          sx={{ mb: 1.5, mt: 0.5 }}
        >
          {t('Here you can find all your boxes')}
        </Typography>
      </div>

      <Grid container spacing={1} id='#tarInvClickElement1'>
        {chests.map((chest, i) => (
          <Grid item xs={3} key={`chest2-${i}`}>
            <UserChestCard
              key={`chest-${i}`}
              userChest={chest}
              onClick={() => {
                if (!chestsLoading && !chestActionLoading) {
                  onClaimChest?.(chest);
                }
              }}
            />
          </Grid>
        ))}
        {userChests.map((userChest, i) => (
          <Grid item xs={3} key={`chest2-${i}`}>
            <UserChestCard
              userChest={userChest}
              onClick={() => {
                if (!userChestsLoading && !chestActionLoading) {
                  onClaimUserChest?.(userChest);
                }
              }}
            />
          </Grid>
        ))}
      </Grid>
    </StyledItems>
  );
}

export default UserChestsGrid;
