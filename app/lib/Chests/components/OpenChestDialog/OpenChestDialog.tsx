import React from 'react';
import styles from './OpenChestDialog.module.scss';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useTranslation } from 'react-i18next';
import { IUserChestDto } from 'lib/Chests/api-server/chest.types.ts';
import { Img } from '@ui-kit/Img/Img.tsx';
import BuyGasAlertLazy from 'lib/TelegramStars/components/BuyGasAlert/BuyGasAlert.lazy.tsx';
import { IWaitConfirmationsResult } from '@chain/hooks/useTransactionConfirmation.ts';
import Alert from '@mui/material/Alert';

const defaultImage = require('assets/img/chest.png') as string;

type Props = {
  open?: boolean;
  onClose?: () => void;
  onClaim?: () => void;
  disabled?: boolean;
  loading?: boolean;
  userChest?: IUserChestDto;
  noGas?: boolean;
  confirmation?: IWaitConfirmationsResult;
};

function OpenChestDialog(props: Props) {
  const { open, onClose, onClaim, disabled, loading, userChest, noGas, confirmation } = props;

  const { t } = useTranslation('chest');

  const chest = userChest?.chest;
  const image = chest?.image;

  return (
    <BottomDialog
      open={open}
      onClose={onClose}
      title={t('Stop. We found something strange...')}
      subTitle={`Chest ${chest?.title}`}
    >
      <div className={styles.chest} style={{ marginTop: 16 }}>
        <Img
          sx={{ borderRadius: '12px', maxWidth: 250 }}
          src={image || defaultImage}
          alt={'Strange chest'}
          onError={(e) => {
            (e.target as HTMLImageElement).src = defaultImage;
          }}
        />

        {noGas && <BuyGasAlertLazy sx={{ mb: 2 }} />}

        <Button
          id='#tarInvClickElement2'
          sx={{ mt: 2 }}
          variant='contained'
          size='large'
          fullWidth
          color='primary'
          onClick={onClaim}
          disabled={disabled || noGas || confirmation?.isPending}
          loading={loading}
        >
          {noGas
            ? t('No Gas')
            : confirmation?.isPending
            ? `Wait for Confirmations ${confirmation?.confirmations || ''}`
            : t("Let's look")}
        </Button>
      </div>
    </BottomDialog>
  );
}

export default React.memo(OpenChestDialog);
