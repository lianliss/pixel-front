import React from 'react';
import styles from './DailyBoxTask.module.scss';
import clsx from 'clsx';
import ChevronRight from 'lib/Quests/components/icons/ChevronRight';
import { useTranslation } from 'react-i18next';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import Box, { BoxProps } from '@mui/material/Box';
import Skeleton from '@mui/material/Skeleton';
import { Button } from '@ui-kit/Button/Button.tsx';

type Props = {
  title: string;
  image?: string;
  icon?: React.ReactNode;
  completed?: boolean;
  onClick?: () => void;
  onCheck?: () => void;
  unAvailable?: boolean;
  loading?: boolean;
} & BoxProps;

function DailyBoxTask(props: Props) {
  const {
    completed,
    title,
    image,
    icon,
    onClick,
    unAvailable = false,
    loading = false,
    onCheck,
    ...other
  } = props;

  const { t } = useTranslation('daily-chest');

  return (
    <Box
      className={clsx(styles.root, { [styles.unAvailable]: unAvailable })}
      onClick={completed || loading ? undefined : onClick}
      {...other}
    >
      <div className={styles.box}>
        <Avatar src={image} className={styles.image} variantStyle='outlined'>
          {icon}
        </Avatar>

        <div className={styles.content}>
          <span className={styles.title}>{title}</span>
          <div className={styles.prizeBox}>
            <span className={clsx(styles.completedText, { [styles.completed]: completed })}>
              {loading ? (
                <Skeleton sx={{ width: 70 }} />
              ) : completed ? (
                t('Completed')
              ) : (
                t('Incompleted')
              )}
            </span>
          </div>
        </div>

        {true && <ChevronRight sx={{ color: 'text.primary', opacity: loading ? 0.35 : 1 }} />}
      </div>

      {onCheck && !completed && (
        <Button
          variant='outlined'
          color='primary'
          fullWidth
          sx={{ mt: 1 }}
          loading={loading}
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            onCheck?.();
          }}
        >
          Check Completed
        </Button>
      )}
    </Box>
  );
}

export default DailyBoxTask;
