import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { CHEST_V2_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { CHEST_V2_ABI } from 'lib/Chests/api-contract/abi/chest-v2.abi.ts';
import { IChainChest } from 'lib/Chests/hooks/useChests.ts';
import { parseChestV2 } from 'lib/Chests/utils/parse-chest-v2.ts';

export function useChest(chestId?: number) {
  const account = useAccount();
  const chestAddress = CHEST_V2_CONTRACT_ID[account.chainId];

  const chestData = useReadContract({
    initData: null,
    abi: CHEST_V2_ABI,
    address: chestAddress,
    args: [chestId],
    skip: !chestAddress || !account.isConnected || typeof chestId === 'undefined',
    functionName: 'getChest',
    select: (el): IChainChest => {
      return el ? parseChestV2(el) : el;
    },
  });

  return chestData;
}
