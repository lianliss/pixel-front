import { useUnit } from 'effector-react';
import { chestDailyStore, getChestDaily } from 'lib/Chests/api-server/chest-daily.store.ts';
import { useCallback, useEffect } from 'react';
import { CHEST_V2_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { chestWeeklyStore, getChestWeekly } from 'lib/Chests/api-server/chest-weekly.store.ts';

export function useChestWeekly(payload: { rang?: number; skip?: boolean }) {
  const state = useUnit(chestWeeklyStore);

  const account = useAccount();
  const chestAddress = CHEST_V2_CONTRACT_ID[account.chainId];

  const refetch = useCallback(
    () => getChestWeekly({ chestAddress, rangLevel: payload.rang }).then(),
    [chestAddress, payload.rang]
  );

  useEffect(() => {
    if (payload.rang && !payload.skip) {
      refetch();
    }
  }, [refetch, payload.skip]);

  return {
    data: state.data,
    loading: state.loading,
    error: state.error,
    refetch,
  };
}
