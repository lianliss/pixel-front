import ChestABI from 'const/ABI/V5/chest';
import { useEffect, useState } from 'react';
import { wei } from 'utils/index';
import { CHEST_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { parseLogs } from 'services/web3Provider/methods';
import { loadNftType } from 'lib/NFT/api-server/nft.store.ts';
import { getNftTypeId } from 'lib/NFT/utils/ids.ts';
import { createNftFromType } from 'lib/NFT/utils/create-nft-from-type.ts';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { TransactionReceipt } from 'viem';

import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

export function useChestSaleBoxDrop({ receipt }: { receipt?: TransactionReceipt }) {
  const account = useAccount();
  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;
  const chestAddress = CHEST_CONTRACT_ID[account.chainId]?.toLowerCase();

  const [item, setItem] = useState<INftItemDto | null>(null);
  const [tokens, setTokens] = useState<number | null>(null);
  const [loading, setLoading] = useState(false);

  const load = async (receipt: TransactionReceipt) => {
    setLoading(true);

    try {
      const logs = await parseLogs(
        ChestABI.find((e) => e.name === 'ItemDrop')?.inputs,
        receipt.logs.filter((l) => l.address === chestAddress)
      );
      const dropLogs = await parseLogs(
        ChestABI.find((e) => e.name === 'BonusDrop')?.inputs,
        receipt.logs.filter((l) => l.address === chestAddress)
      );

      if (!logs.length) throw new Error('No drop');

      if (Number(logs[0].typeId) === telegramId) {
        console.log('[chest] drop tokens');

        const amount = wei.from(dropLogs[0].amount);
        setTokens(amount);
        return amount;
      } else {
        const tokenId = Number(logs[0].tokenId);
        const typeId = Number(logs[0].typeId);

        const nftType = await loadNftType({ typeId: getNftTypeId(typeId, account.chainId) }); // const itemType = itemTypes.find((item) => item.typeId === typeId);

        if (!nftType) {
          console.warn(`not found nft type: ${typeId}`);
          return null;
        }

        const item = createNftFromType(nftType, { tokenId });

        setItem(item);
      }
    } catch (error) {
      console.error('[processDrop]', error);
    }
    setLoading(false);

    return null;
  };

  useEffect(() => {
    if (receipt) {
      load(receipt);
    }
  }, [receipt]);

  return {
    load,
    nft: item,
    tokens,
    loading,
    clear: () => {
      setTokens(null);
      setItem(null);
    },
  };
}
