import React from 'react';
import { Chain, SKALE_TEST, SONGBIRD, SWISSTEST } from 'services/multichain/chains';
import { IChestPriceDto } from 'lib/Chests/hooks/sale-old/useChestSaleBoxes.ts';
import { IS_DEVELOP } from '@cfg/config.ts';

export type IChestBox = {
  title: string;
  chestPrice: IChestPriceDto;
  borderColor: string;
  background: string;
  image: string;
  countdown?: Date;
  total: number;
};

export function useSaleBoxes({
  chainId,
  pixelChest,
  sgbChest,
  testChest,
}: {
  chainId: number;
  pixelChest: any;
  sgbChest: any;
  testChest?: any;
}): IChestBox[] {
  const boxes = React.useMemo(() => {
    switch (chainId) {
      case Chain.SONGBIRD:
        return [
          null,
          {
            title: 'Pixel Box',
            chestPrice: pixelChest,
            borderColor: 'rgba(236, 25, 195, 1)',
            background: require('assets/mining/boxes/pxl.svg'),
            image: require('assets/mining/boxes/pixel2.png'),
            total: 100000,
          },
          {
            title: 'SGB Box',
            chestPrice: sgbChest,
            borderColor: 'rgba(243, 111, 5, 1)',
            background: require('assets/mining/boxes/sgb.svg'),
            image: require('assets/mining/boxes/sgb2.png'),
            down: 1719669600000,
            total: 20000,
            onClick: () => {},
          },
        ].filter((el) => Boolean(el.chestPrice));
      case Chain.SKALE_TEST:
        return [
          null,
          {
            title: 'Pixel Box',
            chestPrice: pixelChest,
            borderColor: 'rgba(236, 25, 195, 1)',
            background: require('assets/mining/boxes/pxl.svg'),
            image: require('assets/mining/boxes/pixel2.png'),
            total: 50000,
          },
          {
            title: 'SKL Box',
            chestPrice: sgbChest,
            borderColor: 'rgba(243, 111, 5, 1)',
            background: require('assets/mining/boxes/sgb.svg'),
            image: require('assets/mining/boxes/sgb2.png'),
            total: 50000,
          },
        ].filter((el) => Boolean(el.chestPrice));
      case Chain.SKALE:
        return [
          IS_DEVELOP
            ? {
                title: 'Test Box',
                chestPrice: testChest,
                borderColor: 'rgb(25,29,236)',
                background: require('assets/mining/boxes/pxl.svg'),
                image: require('assets/mining/boxes/pixel2.png'),
                total: 30,
              }
            : null,
          {
            title: 'Pixel Box',
            chestPrice: pixelChest,
            borderColor: 'rgba(236, 25, 195, 1)',
            background: require('assets/mining/boxes/pxl.svg'),
            image: require('assets/mining/boxes/pixel2.png'),
            countdown: new Date('2024-11-01T14:00:00'),
            total: 50000,
          },
          {
            title: 'SKL Box',
            chestPrice: sgbChest,
            borderColor: 'rgba(243, 111, 5, 1)',
            background: require('assets/mining/boxes/sgb.svg'),
            image: require('assets/mining/boxes/sgb2.png'),
            countdown: new Date('2024-11-02T14:00:00'),
            total: 50000,
          },
        ]; // .filter((el) => Boolean(el.chestPrice));
      case Chain.SWISSTEST:
        return [
          null,
          {
            title: 'NFT Box',
            chestPrice: pixelChest,
            borderColor: 'rgba(236, 25, 195, 1)',
            background: require('assets/mining/boxes/pxl.svg'),
            image: require('assets/mining/boxes/swiss.png'),
            total: 150000,
            onClick: () => {},
          },
          {
            title: 'NFT Box',
            chestPrice: sgbChest,
            borderColor: 'rgba(243, 111, 5, 1)',
            background: require('assets/mining/boxes/sgb.svg'),
            image: require('assets/mining/boxes/swtrBox.png'),
            total: 250000,
            onClick: () => {},
          },
        ].filter((el) => Boolean(el.chestPrice));
      default:
        return [];
    }
  }, [chainId, pixelChest, sgbChest, testChest]);

  return boxes;
}
