import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { wei } from 'utils/index';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import CHESTS_ABI from 'lib/Chests/api-contract/abi/chest.abi.ts';
import { CHEST_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { IS_DEVELOP, IS_STAGE } from '@cfg/config.ts';
import { IChestSaleBox } from 'lib/Chests/utils/constants.ts';
import { useInterval } from 'utils/hooks/useInterval';
import { isTester } from '@cfg/testers.ts';
import { getTransactionCount } from '@wagmi/core';
import { useConfig, useWaitForTransactionReceipt } from 'wagmi';
import wait from 'utils/wait';
import { useChestSaleBoxDrop } from 'lib/Chests/hooks/sale-old/useChestSaleBoxDrop.ts';

const ERC_20_ABI = require('const/ABI/Erc20Token');

export type IChestPriceDto = {
  chestId: number;
  count: number;
  pixelPrice: bigint;
  sgbPrice: bigint;
};

export function useChestSaleBoxes({
  testId,
  pixelId,
  coinId,
}: {
  testId?: number;
  pixelId?: number;
  coinId?: number;
}) {
  const config = useConfig();
  const account = useAccount();
  const tradeToken = useTradeToken();
  const isSecondCoin =
    !tradeToken.data.address ||
    tradeToken.data.address === '0x0000000000000000000000000000000000000000';

  const chestAddress = CHEST_CONTRACT_ID[account.chainId];

  const allowanceState = useReadContract<bigint>({
    initData: 0 as bigint,
    abi: ERC_20_ABI,
    address: tradeToken.data.address,
    functionName: 'allowance',
    args: [chestAddress, account.accountAddress],
    skip: !tradeToken.data.address || isSecondCoin,
  });

  const allowanceUpdate = useWriteContract({
    abi: ERC_20_ABI,
    address: tradeToken.data.address,
  });

  const testChestData = useReadContract<IChestPriceDto>({
    abi: CHESTS_ABI,
    address: chestAddress,
    skip:
      !chestAddress ||
      !account.isConnected ||
      !(IS_DEVELOP || IS_STAGE) ||
      !isTester(account.accountAddress) ||
      typeof testId === 'undefined',
    functionName: 'getMarketItem',
    args: [testId],
    select: (res) => {
      const rawChestId = res.chestId;
      const rawCount = res.count;
      const rawPixelPrice = res.pixelPrice;
      const rawSgbPrice = res.sgbPrice;

      return {
        chestId: Number(rawChestId),
        count: Number(rawCount),
        pixelPrice: wei.from(rawPixelPrice) as bigint,
        sgbPrice: wei.from(rawSgbPrice, 18) as bigint,
      };
    },
  });
  const pixelChestData = useReadContract<IChestPriceDto>({
    abi: CHESTS_ABI,
    address: chestAddress,
    skip: !chestAddress || !account.isConnected || typeof pixelId === 'undefined',
    functionName: 'getMarketItem',
    args: [pixelId],
    select: (res) => {
      const rawChestId = res.chestId;
      const rawCount = res.count;
      const rawPixelPrice = res.pixelPrice;
      const rawSgbPrice = res.sgbPrice;

      return {
        chestId: Number(rawChestId),
        count: Number(rawCount),
        pixelPrice: wei.from(rawPixelPrice) as bigint,
        sgbPrice: wei.from(rawSgbPrice, 18) as bigint,
      };
    },
  });
  const coinChestData = useReadContract<IChestPriceDto>({
    abi: CHESTS_ABI,
    address: chestAddress,
    skip: !chestAddress || !account.isConnected || typeof coinId === 'undefined',
    functionName: 'getMarketItem',
    args: [coinId],
    select: (res) => {
      const rawChestId = res.chestId;
      const rawCount = res.count;
      const rawPixelPrice = res.pixelPrice;
      const rawSgbPrice = res.sgbPrice;

      return {
        chestId: Number(rawChestId),
        count: Number(rawCount),
        pixelPrice: wei.from(rawPixelPrice) as bigint,
        sgbPrice: wei.from(rawSgbPrice, 18) as bigint,
      };
    },
  });

  const buyPixelChest = useWriteContract({
    abi: CHESTS_ABI,
    address: chestAddress,
  });

  const waitForTransactionReceipt = useWaitForTransactionReceipt({
    hash: buyPixelChest.data,
    confirmations: 1,
    query: {
      enabled: !!buyPixelChest.data,
    },
  });
  const {
    isLoading: isConfirming,
    isSuccess: isConfirmed,
    isError: isFailed,
    data: receipt,
  } = waitForTransactionReceipt;

  const saleDrop = useChestSaleBoxDrop({
    receipt,
  });

  const handleBuy = async (box: IChestSaleBox, price: IChestPriceDto) => {
    let nonce = await getTransactionCount(config, {
      address: account.accountAddress,
    });

    if (box.type === 'pixel') {
      return buyPixelChest.writeContractAsync({
        functionName: 'buyChest',
        args: [box.id],
        nonce: nonce++,
      });
    }

    const secondPriceWei = wei.to(price.sgbPrice) as bigint;

    if (box.type === 'erc20') {
      if (wei.from(allowanceState.data) < price.sgbPrice) {
        await allowanceUpdate.writeContractAsync({
          functionName: 'approve',
          args: [chestAddress, secondPriceWei],
          nonce: nonce++,
        });

        await wait(300);
      }

      return buyPixelChest.writeContractAsync({
        functionName: 'buyChest',
        args: [box.id],
        nonce: nonce++,
      });
    }

    return buyPixelChest.writeContractAsync({
      functionName: 'buyChest',
      args: [box.id],
      nonce: nonce++,
      overrides: {
        value: secondPriceWei,
      },
    });
  };
  const handleReload = async () => {
    if (pixelChestData.data?.count) {
      pixelChestData.refetch().catch();
    }
    if (coinChestData.data?.count) {
      coinChestData.refetch().catch();
    }
    if (testChestData.data?.count) {
      testChestData.refetch().catch();
    }
  };

  useInterval(() => handleReload(), 5000, [
    pixelChestData.data?.count,
    coinChestData.data?.count,
    testChestData.data?.count,
  ]);

  return {
    testChest: testChestData,
    pixelChest: pixelChestData,
    coinChest: coinChestData,
    buy: handleBuy,
    reload: handleReload,
    drop: {
      nft: saleDrop.nft,
      tokens: saleDrop.tokens,
      isLoading: saleDrop.loading,
      clear: () => {
        saleDrop.clear();
      },
    },
    receipt: {
      isConfirming,
      isConfirmed,
      isFailed: isFailed || !!waitForTransactionReceipt.failureReason,
    },
    tx: {
      isPending: buyPixelChest.isPending,
      isSuccess: buyPixelChest.isSuccess,
      isError: buyPixelChest.isError,
    },
  };
}
