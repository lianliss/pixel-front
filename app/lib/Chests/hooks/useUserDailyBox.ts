import { useUnit } from 'effector-react';
import { loadChestDaily, userChestDailyStore } from 'lib/Chests/api-server/chest-daily.store.ts';
import { useCallback, useEffect } from 'react';

export function useUserDailyBox(payload: { userAddress: string; chainId: number; skip?: boolean }) {
  const daily = useUnit(userChestDailyStore);

  const refetch = useCallback(
    () => loadChestDaily({ userAddress: payload.userAddress, chainId: payload.chainId }).then(),
    [payload.userAddress, payload.chainId]
  );

  useEffect(() => {
    if (payload.userAddress && !payload.skip) {
      refetch();
    }
  }, [refetch, payload.skip, payload.chainId]);

  return {
    data: daily.state ? daily.state : null,
    loading: daily.loading,
    error: daily.error,
    refetch,
  };
}
