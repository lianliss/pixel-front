import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { CHEST_V2_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { CHEST_V2_ABI } from 'lib/Chests/api-contract/abi/chest-v2.abi.ts';
import { parseChestV2 } from 'lib/Chests/utils/parse-chest-v2.ts';

export type IChainChest = {
  chestId: number;
  title: string;
  imageURI: string;
  drop: IChestV2DropDto[];
};
export type IChestV2DropDto = {
  ratio: number;
  isToken: boolean;
  isShard: boolean;
  items: number[];
  tokenAddress: string;
  amount: number;
  maxAmount: number;
  isNew?: boolean;
};

export function useChests() {
  const account = useAccount();
  const chestAddress = CHEST_V2_CONTRACT_ID[account.chainId];

  const chestsData = useReadContract<IChainChest[]>({
    initData: [],
    abi: CHEST_V2_ABI,
    address: chestAddress,
    skip: !chestAddress || !account.isConnected,
    functionName: 'getChests',
    select: (arr) => {
      return arr.map(parseChestV2);
    },
  });

  return chestsData;
}
