import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { CHEST_V2_ABI } from 'lib/Chests/api-contract/abi/chest-v2.abi.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { CHEST_V2_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { parseGwei } from 'viem';
import { Chain } from 'services/multichain/chains';

const GAS = {
  [Chain.UNITS]: parseGwei('0.005'),
  [Chain.SONGBIRD]: parseGwei('0.005'),
  [Chain.SKALE]: parseGwei('0.0005'),
};

function useUserChestApi() {
  const account = useAccount();
  const chestAddress = CHEST_V2_CONTRACT_ID[account.chainId];

  const write = useWriteContract({
    abi: CHEST_V2_ABI,
    address: chestAddress,
    ignoreRecipe: true,
  });

  return {
    write,
    claim: (chestId: number) => {
      return write.writeContractAsync({
        gas: GAS[account.chainId],
        functionName: 'claimFreeChest',
        args: [Number(chestId)],
      });
    },
  };
}

export default useUserChestApi;
