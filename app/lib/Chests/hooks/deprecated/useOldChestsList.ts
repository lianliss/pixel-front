import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { wei } from 'utils/index';
import { useAccount } from '@chain/hooks/useAccount.ts';
import CHESTS_ABI from 'lib/Chests/api-contract/abi/chest.abi.ts';
import { CHEST_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { IChainChest } from 'lib/Chests/hooks/deprecated/useOldUserChests.ts';

/**
 * @deprecated
 */
export function useOldChestsList() {
  const account = useAccount();
  const chestAddress = CHEST_CONTRACT_ID[account.chainId];

  const chestsData = useReadContract<IChainChest[]>({
    initData: [],
    abi: CHESTS_ABI,
    address: chestAddress,
    skip: !chestAddress || !account.isConnected,
    functionName: 'getChests',
    select: (res) => {
      const arr = res || [];

      return arr.map((d, chestId) => ({
        chestId: Number(d.chestId),
        title: d.title,
        ratios: d.ratios.map((r) => wei.from(r, 4)),
        items: d.items.map((slot) => slot.map((rarity) => rarity.map((item) => Number(item)))),
        minPixels: wei.from(d.minPixels),
        maxPixels: wei.from(d.maxPixels),
      }));
    },
  });

  return {
    chests: chestsData,
  };
}
