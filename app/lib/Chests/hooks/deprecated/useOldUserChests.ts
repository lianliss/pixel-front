import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { wei } from 'utils/index';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import React from 'react';

import CHESTS_ABI from 'lib/Chests/api-contract/abi/chest.abi.ts';
import FREE_CHEST_ABI from 'lib/Chests/api-contract/abi/free-chest.abi.ts';
import {
  CHEST_CONTRACT_ID,
  FREE_CHEST_CONTRACT_ID,
} from 'lib/Chests/api-contract/contract.constants.ts';
import { IUserChestDto } from 'lib/Chests/api-server/chest.types.ts';
import { freeChestToUserChest } from 'lib/Chests/utils/convert/free-chest-to-user-chest.ts';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

/**
 * @deprecated
 */
export function useReadFreeChests({
  freeChestAddress,
  telegramId,
  skip,
}: {
  freeChestAddress: string;
  telegramId: number;
  skip?: boolean;
}) {
  const freeChestsData = useReadContract<number[]>({
    initData: [],
    abi: FREE_CHEST_ABI,
    address: freeChestAddress,
    skip: !freeChestAddress || !telegramId || skip,
    functionName: 'getUserFreeChests',
    args: [telegramId],
  });

  return freeChestsData;
}

/**
 * @deprecated
 */
export type IChainChest = {
  chestId: number;
  title: string;
  ratios: number[];
  items: number[];
  minPixels: bigint;
  maxPixels: bigint;
  isFree?: boolean;
};

/**
 * @deprecated
 */
export function useFreeChestApi() {
  const account = useAccount();
  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;

  const chestAddress = CHEST_CONTRACT_ID[account.chainId];
  const freeChestAddress = FREE_CHEST_CONTRACT_ID[account.chainId];

  const writeChest = useWriteContract({
    abi: FREE_CHEST_ABI,
    address: freeChestAddress,
  });
  const chestsData = useReadContract<IChainChest[]>({
    initData: [],
    abi: CHESTS_ABI,
    address: chestAddress,
    skip: !chestAddress || !account.isConnected,
    functionName: 'getChests',
    select: (res) =>
      res?.map(
        (d, chestId): IChainChest => ({
          chestId: Number(d.chestId),
          title: d.title,
          ratios: d.ratios.map((r) => wei.from(r, 4)),
          items: d.items.map((slot) => slot.map((rarity) => rarity.map((item) => Number(item)))),
          minPixels: wei.from(d.minPixels),
          maxPixels: wei.from(d.maxPixels),
        })
      ) || [],
  });
  const freeChestsData = useReadFreeChests({
    freeChestAddress,
    telegramId,
    skip: !account.isConnected,
  });

  const freeChests = React.useMemo((): IUserChestDto[] => {
    if (!chestsData.data?.length) {
      return [];
    }

    const result: IUserChestDto[] = [];

    for (let i = 0; i < freeChestsData.data.length; i++) {
      if (!Number(freeChestsData.data[i])) {
        continue;
      }

      const rawchest = chestsData.data[i];

      result.push(freeChestToUserChest(rawchest, chestAddress));
    }

    return result;
  }, [chestsData.data, freeChestsData.data]);

  const handleReload = async () => {
    return freeChestsData.refetch();
  };

  const handleClaim = async (chestId: number) => {
    console.log('[claimFreeChest]', { address: freeChestAddress, args: [chestId] });
    return writeChest.writeContractAsync({ functionName: 'claimFreeChest', args: [chestId] });
  };
  const handleClaimWeekly = async () => {
    console.log('[claimWeeklyChest]', { address: freeChestAddress, args: [] });
    return writeChest.writeContractAsync({ functionName: 'claimWeeklyChest', args: [] });
  };

  return {
    chests: freeChests,
    reload: handleReload,
    claim: handleClaim,
    claimWeekly: handleClaimWeekly,
    loading: chestsData.loading || freeChestsData.loading,
  };
}
