import { useUnit } from 'effector-react';
import { loadChestWeekly, userChestWeeklyStore } from 'lib/Chests/api-server/chest-weekly.store.ts';
import { useCallback, useEffect } from 'react';

export function useUserWeeklyBox(payload: {
  userAddress: string;
  chestAddress: string;
  skip?: boolean;
}) {
  const Weekly = useUnit(userChestWeeklyStore);

  const refetch = useCallback(
    () =>
      loadChestWeekly({
        userAddress: payload.userAddress.toLowerCase(),
        chestAddress: payload.chestAddress.toLowerCase(),
      }).then(),
    [payload.userAddress, payload.chestAddress]
  );

  useEffect(() => {
    if (payload.userAddress && !payload.skip) {
      refetch();
    }
  }, [refetch, payload.skip, payload.chestAddress]);

  return {
    data: Weekly.state ? Weekly.state : null,
    loading: Weekly.loading,
    error: Weekly.error,
    refetch,
  };
}
