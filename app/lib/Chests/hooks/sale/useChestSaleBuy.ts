import { CHEST_SALE_ABI } from 'lib/Chests/api-contract/abi/chest-sale.abi.ts';
import { CHEST_SALE_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { useWaitForTransactionReceipt } from 'wagmi';
import { IChainChestSale } from 'lib/Chests/hooks/sale/useChestSales.ts';
import { useState } from 'react';

export function useChestSaleBuy() {
  const account = useAccount();

  const address = CHEST_SALE_CONTRACT_ID[account.chainId];

  const [delayed, setDelayed] = useState(false);

  const write = useWriteContract({
    abi: CHEST_SALE_ABI,
    address: address,
  });

  // 0x40c3277c6904b5932ce52539c9406d2754afa180d4ee31499fec3f9c20545bea
  const waitForTransactionReceipt = useWaitForTransactionReceipt({
    hash: write.data,
    confirmations: 1,
    query: {
      enabled: !!write.data,
    },
  });

  const {
    isLoading: isConfirming,
    isSuccess: isConfirmed,
    isError: isFailed,
    data: receipt,
  } = waitForTransactionReceipt;

  return {
    write,
    buy: async (sale: IChainChestSale) => {
      try {
        const tx = await write.writeContractAsync({
          functionName: 'buy',
          args: [sale.saleId],
          overrides: { value: sale.priceWei },
        });

        setDelayed(true);

        setTimeout(() => {
          setDelayed(false);
        }, sale.additionalDelaySeconds * 1000);

        return tx;
      } catch (e) {
        throw e;
      }
    },
    receipt: {
      data: receipt,
      isConfirming,
      isConfirmed,
      isFailed: isFailed || !!waitForTransactionReceipt.failureReason,
    },
    tx: {
      isDelayed: delayed,
      isPending: write.isPending,
      isSuccess: write.isSuccess,
      isError: write.isError,
    },
  };
}
