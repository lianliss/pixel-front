import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { CHEST_SALE_ABI } from 'lib/Chests/api-contract/abi/chest-sale.abi.ts';
import { CHEST_SALE_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import wei from 'utils/wei';
import { CHEST_SALE_BOXES, IChestSaleBoxChance } from 'lib/Chests/utils/constants.ts';

export type IChainChestSale = {
  saleId: number;
  chestId: number;
  price: number;
  priceWei: string;
  erc20Currency: string;
  isShardCurrency: boolean;
  amount: number;
  amountPerHandsLimit: number;
  additionalDelaySeconds: number;
};

export type IChestSale = {
  id: number;
  title: string;
  symbol: string;
  image: string;
  countdown?: Date;
  total: number;
  type: 'pixel' | 'coin' | 'erc20';
  ratios: IChestSaleBoxChance[];
  chain: IChainChestSale | null;
};

export function useChainChestSales() {
  const account = useAccount();

  const address = CHEST_SALE_CONTRACT_ID[account.chainId];

  const result = useReadContract({
    abi: CHEST_SALE_ABI,
    functionName: 'getSales',
    args: [account.accountAddress, 0, 10],
    address: address,
    skip: !account.isConnected,
    select: (arr) =>
      arr.map(
        (el): IChainChestSale => ({
          price: +wei.from(el.price),
          priceWei: el.price,
          chestId: Number(el.chestId),
          saleId: Number(el.saleId),
          amount: Number(el.amount),
          erc20Currency: el.erc20Currency,
          isShardCurrency: Boolean(el.isShardCurrency),
          additionalDelaySeconds: Number(el.additionalDelaySeconds),
          amountPerHandsLimit: Number(el.amountPerHandsLimit),
        })
      ),
  });

  return result;
}

export function useChestSales() {
  const account = useAccount();

  const boxes = CHEST_SALE_BOXES[account.chainId] || [];

  const result = useChainChestSales();
  const resultMap = (result.data || []).reduce((acc, el) => ({ ...acc, [el.saleId]: el }), {});

  return {
    ...result,
    data: boxes.map((box) => ({ ...box, chain: resultMap[box.id] })),
  };
}
