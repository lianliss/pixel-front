import { useAccount } from '@chain/hooks/useAccount.ts';
import { CHEST_V2_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { useUnit } from 'effector-react';
import { loadUserChests, userChestsStore } from 'lib/Chests/api-server/chest.store.ts';
import { useCallback, useEffect } from 'react';

export function useUserChests() {
  const account = useAccount();
  const chestAddress = CHEST_V2_CONTRACT_ID[account.chainId];

  // const chestsData = useReadContract({
  //   initData: [],
  //   abi: CHEST_V2_ABI,
  //   address: chestAddress,
  //   args: [userAddress || account.accountAddress],
  //   skip: !chestAddress || !(userAddress || account.isConnected),
  //   functionName: 'getUserChests',
  //   select: (rows): IUserChestV2[] => {
  //     return rows
  //       ? rows.map((el) => ({ chestId: Number(el.chestId), amount: Number(el.amount) }))
  //       : [];
  //   },
  // });

  const chests = useUnit(userChestsStore);

  const refetch = useCallback(
    () =>
      loadUserChests({
        contractId: chestAddress,
        userAddress: account.accountAddress?.toLowerCase(),
        limit: 100,
        offset: 0,
      }),
    [chestAddress, account.accountAddress]
  );

  useEffect(() => {
    if (account.isConnected && chestAddress) {
      refetch();
    }
  }, [refetch]);

  return {
    data: chests.state,
    loading: chests.loading,
    refetch,
  };
}
