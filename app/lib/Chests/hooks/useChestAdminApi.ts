import { useAccount } from '@chain/hooks/useAccount.ts';
import { CHEST_V2_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { CHEST_V2_ABI } from 'lib/Chests/api-contract/abi/chest-v2.abi.ts';
import { IChestV2DropDto } from 'lib/Chests/hooks/useChests.ts';
import { wei } from 'utils/index';
import { getTransactionCount, waitForTransactionReceipt } from '@wagmi/core';
import { useConfig } from 'wagmi';
import { useTransactionLogs } from '@chain/hooks/useTransactionLogs.ts';
import { useState } from 'react';

const convertToChainDrop = (el: IChestV2DropDto) => ({
  ...el,
  ratio: wei.to(el.ratio, 4),
  amount: wei.to(el.amount),
  maxAmount: wei.to(el.maxAmount),
});

export function useChestAdminApi() {
  const config = useConfig();
  const account = useAccount();
  const chestAddress = CHEST_V2_CONTRACT_ID[account.chainId];

  const [loading, setLoading] = useState(false);

  const transactionLogs = useTransactionLogs({
    abi: CHEST_V2_ABI,
  });

  const write = useWriteContract({
    address: chestAddress,
    abi: CHEST_V2_ABI,
    ignoreRecipe: true,
  });

  const addDrop = async (payload: { drop: IChestV2DropDto; chestId: number }) => {
    return write.writeContractAsync({
      functionName: 'addChestDrop',
      args: [payload.chestId, convertToChainDrop(payload.drop)],
      nonce: await getTransactionCount(config, { address: account.accountAddress }),
    });
  };

  const removeDrop = async (payload: { dropIndex: number; chestId: number }) => {
    return write.writeContractAsync({
      functionName: 'deleteChestDrop',
      args: [payload.chestId, payload.dropIndex],
      nonce: await getTransactionCount(config, { address: account.accountAddress }),
    });
  };

  const clearDrop = async (payload: { chestId: number }) => {
    return write.writeContractAsync({
      functionName: 'deleteChestDrop',
      args: [payload.chestId],
      nonce: await getTransactionCount(config, { address: account.accountAddress }),
    });
  };
  const updateChestDrop = async (payload: {
    chestId: number;
    dropIndex: number;
    drop: IChestV2DropDto;
  }) => {
    return write.writeContractAsync({
      functionName: 'updateChestDrop',
      args: [payload.chestId, payload.dropIndex, convertToChainDrop(payload.drop)],
      nonce: await getTransactionCount(config, { address: account.accountAddress }),
    });
  };

  const create = async (payload: { title: string; image: string; drop: IChestV2DropDto[] }) => {
    setLoading(true);

    try {
      const txHash = await write.writeContractAsync({
        functionName: 'createChest',
        args: [payload.title, payload.image],
        nonce: await getTransactionCount(config, { address: account.accountAddress }),
      });

      const receipt = await waitForTransactionReceipt(config, { hash: txHash, confirmations: 1 });
      const logs = transactionLogs.parse(receipt);
      const creationLog = logs.find((el) => el.name === 'CreateChest');
      const chestId = Number(creationLog.args[0]);

      for (const drop of payload.drop) {
        const dropTxHash = await addDrop({ chestId, drop });

        await waitForTransactionReceipt(config, { hash: dropTxHash, confirmations: 1 });
      }
    } catch (e) {
      throw e;
    } finally {
      setLoading(false);
    }
  };
  const updateInfo = async (payload: { chestId: number; title: string; image: string }) => {
    setLoading(true);

    try {
      const txHash = await write.writeContractAsync({
        functionName: 'updateChest',
        args: [payload.chestId, payload.title, payload.image],
        nonce: await getTransactionCount(config, { address: account.accountAddress }),
      });
      await waitForTransactionReceipt(config, { hash: txHash, confirmations: 1 });
    } catch (e) {
      throw e;
    } finally {
      setLoading(false);
    }
  };
  const update = async (payload: {
    chestId: number;
    title: string;
    image: string;
    drop: IChestV2DropDto[];
  }) => {
    setLoading(true);

    try {
      const txHash = await write.writeContractAsync({
        functionName: 'updateChest',
        args: [payload.chestId, payload.title, payload.image],
        nonce: await getTransactionCount(config, { address: account.accountAddress }),
      });
      await waitForTransactionReceipt(config, { hash: txHash, confirmations: 1 });

      const clearTxHash = await clearDrop({ chestId: payload.chestId });
      await waitForTransactionReceipt(config, { hash: clearTxHash, confirmations: 1 });

      for (const drop of payload.drop) {
        const dropTxHash = await addDrop({ chestId: payload.chestId, drop });

        await waitForTransactionReceipt(config, { hash: dropTxHash, confirmations: 1 });
      }
    } catch (e) {
      throw e;
    } finally {
      setLoading(false);
    }
  };

  const updateDrop = async (payload: {
    chestId: number;
    dropIndex: number;
    drop: IChestV2DropDto;
  }) => {
    setLoading(true);

    try {
      const txHash = await updateChestDrop(payload);
      await waitForTransactionReceipt(config, { hash: txHash, confirmations: 1 });
    } catch (e) {
      throw e;
    } finally {
      setLoading(false);
    }
  };

  const give = async (payload: { chestId: number; userAddresses: string[] }) => {
    return write.writeContractAsync({
      functionName: 'giveTheChest',
      args: [payload.chestId, payload.userAddresses],
      nonce: await getTransactionCount(config, { address: account.accountAddress }),
    });
  };
  const send = async (payload: { chestId: number; userAddress: string }) => {
    return write.writeContractAsync({
      functionName: 'sendChest',
      args: [payload.chestId, payload.userAddress],
      nonce: await getTransactionCount(config, { address: account.accountAddress }),
    });
  };

  return {
    write,
    loading: loading || write.isPending,
    create,
    update,
    updateInfo,
    send,
    give,
    addDrop,
    removeDrop,
    clearDrop,
    updateDrop,
  };
}
