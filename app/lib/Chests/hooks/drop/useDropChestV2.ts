import React, { useEffect, useState } from 'react';
import { CHEST_V2_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import toaster from 'services/toaster.tsx';
import { loadNftType } from 'lib/NFT/api-server/nft.store.ts';
import { getNftTypeId } from 'lib/NFT/utils/ids.ts';
import { createNftFromType } from 'lib/NFT/utils/create-nft-from-type.ts';
import { TransactionReceipt } from 'viem';
import { parseChestDropLog } from 'lib/Chests/utils/parse-drop-log.ts';
import { useTransactionReceipt } from 'wagmi';

import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

export type IChestDropToken = { address: string; amount: number };

export function useDropChestV2() {
  const account = useAccount();
  const userInfo = useSelfUserInfo();

  const chestAddress = CHEST_V2_CONTRACT_ID[account.chainId]?.toLowerCase();
  const telegramId = userInfo.data?.telegramId;

  // const [hash, setHash] = useState<string | null>(null);
  const [item, setItem] = useState<any | null>(null);
  const [token, setToken] = useState<IChestDropToken | null>(null);
  const [loading, setLoading] = useState(false);

  // const receipt = useTransactionReceipt({
  //   hash: hash,
  //   query: {
  //     enabled: !!hash,
  //   },
  // })

  const load = async (receipt: TransactionReceipt) => {
    setLoading(true);

    try {
      const { token, nft } = await parseChestDropLog({
        receipt,
        chestAddress,
        telegramId: telegramId!,
      });

      if (!token && !nft) {
        setLoading(false);
        toaster.error('failed to find log');
        return null;
      }

      if (token) {
        setToken({ amount: token.amount, address: token.address });
        return;
      }

      const nftType = await loadNftType({ typeId: getNftTypeId(nft.typeId, account.chainId) });

      if (!nftType) {
        setLoading(false);
        toaster.error('failed to find item type');
        return null;
      }

      const item = createNftFromType(nftType, { tokenId: nft.tokenId });

      setItem(item);
    } catch (error) {
      console.error('[processDrop]', error);
    }
    setLoading(false);

    return null;
  };

  // const result = useTransactionReceipt({
  //   hash: "0x90143bff91d7041d6251a10c623976d950590b778369beca02f7b3769e3d317d"
  // })

  // useEffect(() => {
  //   if (result.data) {
  //     load(result.data);
  //   }
  // }, [account.isConnected, result.data]);

  return {
    load,
    nft: item,
    token,
    loading,
    clear: () => {
      setToken(null);
      setItem(null);
    },
  };
}
