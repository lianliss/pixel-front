import React, { useEffect, useState } from 'react';
import { CHEST_CONTRACT_ID } from 'lib/Chests/api-contract/contract.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { parseLogs } from 'services/web3Provider/methods';
import toaster from 'services/toaster.tsx';
import CHESTS_ABI from 'lib/Chests/api-contract/abi/chest.abi.ts';
import { loadNftType } from 'lib/NFT/api-server/nft.store.ts';
import { getNftTypeId } from 'lib/NFT/utils/ids.ts';
import { createNftFromType } from 'lib/NFT/utils/create-nft-from-type.ts';
import { useTransactionReceipt } from 'wagmi';
import { TransactionReceipt } from 'viem';

export function useFreeChestDropFromTx() {
  const account = useAccount();

  const chestAddress = CHEST_CONTRACT_ID[account.chainId]?.toLowerCase();

  const [item, setItem] = useState<any | null>(null);
  const [tokens, setTokens] = useState<number | null>(null);
  const [loading, setLoading] = useState(false);

  const load = async (receipt: TransactionReceipt) => {
    setLoading(true);

    try {
      const abiInput = CHESTS_ABI.find((e) => e.name === 'ItemDrop')?.inputs;
      const receiptLog = receipt.logs?.filter((l) => l.address === chestAddress);

      let logs: any[] = await parseLogs(abiInput, receiptLog);

      // comment for test
      logs = logs.filter(
        (log) => log.account.toLowerCase() === account.accountAddress.toLowerCase()
      );

      if (!logs.length) {
        setLoading(false);
        toaster.error('failed to find log');
        return null;
      }

      const tokenId = Number(logs[0].tokenId);
      const typeId = Number(logs[0].typeId);

      const nftType = await loadNftType({ typeId: getNftTypeId(typeId, account.chainId) });

      if (!nftType) {
        setLoading(false);
        toaster.error('failed to find item type');
        return null;
      }

      const item = createNftFromType(nftType, { tokenId });

      setItem(item);
    } catch (error) {
      console.error('[processDrop]', error);
    }
    setLoading(false);

    return null;
  };

  // const result = useTransactionReceipt({
  //   hash: '0x87e55fc1ec64010738ebbcc4847514023887270b59fe61e4a7eb0e6b26910e03',
  // })
  //
  // useEffect(() => {
  //   if (result.data) {
  //     load(result.data);
  //   }
  // }, [account.isConnected, result.data]);

  return {
    load,
    nft: item,
    tokens,
    loading,
    clear: () => {
      setTokens(null);
      setItem(null);
    },
  };
}
