export function getChestChances(params: number[]) {
  const chances: number[] = [];
  let isValidChances = true;
  for (let i = 0; i < params.length; i++) {
    const param = params[i];
    if (!param) {
      chances.push(0);
      continue;
    }
    let sum = 0;
    for (let j = 0; j < i; j++) {
      if (chances[j]) {
        sum += chances[j];
      }
    }
    const value = (100 - sum) / param;
    if (value < 0) {
      isValidChances = false;
    }
    chances.push(value);
  }

  return chances;
}
