import { Chain } from 'services/multichain/chains';
import { SKALE_CHEST_SALE_BOXES } from 'lib/Chests/utils/boxes/skale-boxes.ts';
import { SKALE_TESTNET_BOXES } from 'lib/Chests/utils/boxes/skale-testnet-boxes.ts';
import { UNITS_CHEST_SALE_BOXES } from 'lib/Chests/utils/boxes/units-boxes.ts';

export type IChestSaleBox = {
  id: number;
  title: string;
  symbol: string;
  image: string;
  countdown?: Date;
  total: number;
  type: 'pixel' | 'coin' | 'erc20';
  ratios: IChestSaleBoxChance[];
};
export type IChestSaleBoxChance = {
  percent: number;
  image: string;
};

export const CHEST_SALE_BOXES: Record<number, Array<IChestSaleBox | null>> = {
  [Chain.SKALE]: SKALE_CHEST_SALE_BOXES,
  [Chain.SKALE_TEST]: SKALE_TESTNET_BOXES,
  [Chain.UNITS]: UNITS_CHEST_SALE_BOXES,
};
