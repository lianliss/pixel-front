import { Chain } from 'services/multichain/chains';
import {
  SGB_JADE_TOKEN,
  SGB_PIXEL,
  SGB_TOKEN,
  SKALE_GAS_TOKEN,
  SKALE_PXL_TOKEN,
  SKALE_TOKEN,
  UNITS_PIXEL_DUST_TOKEN,
  UNITS_PIXEL_TOKEN,
  UNITS_TOKEN,
} from 'services/multichain/token.constants.ts';
import { IChestV2DropDto } from 'lib/Chests/hooks/useChests.ts';

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

export const CHEST_TOKENS_BY_CHAIN = {
  [Chain.SONGBIRD]: [SGB_TOKEN, SGB_PIXEL, SGB_JADE_TOKEN].map((el) => {
    if (!el.address) {
      el.address = ZERO_ADDRESS;
    }

    return el;
  }),
  [Chain.SKALE]: [SKALE_TOKEN, SKALE_PXL_TOKEN, SKALE_GAS_TOKEN].map((el) => {
    if (!el.address) {
      el.address = ZERO_ADDRESS;
    }

    return el;
  }),
  [Chain.UNITS]: [UNITS_TOKEN, UNITS_PIXEL_TOKEN, UNITS_PIXEL_DUST_TOKEN].map((el) => {
    if (!el.address) {
      el.address = ZERO_ADDRESS;
    }

    return el;
  }),
};

export const getDefaultChestDrop = (part: Partial<IChestV2DropDto> = {}): IChestV2DropDto => {
  return {
    ratio: 0,
    isToken: true,
    amount: 0,
    isShard: false,
    items: [],
    maxAmount: 0,
    tokenAddress: ZERO_ADDRESS,
    ...part,
  };
};
