import { parseLogs } from 'services/web3Provider/methods';
import { CHEST_V2_ABI } from 'lib/Chests/api-contract/abi/chest-v2.abi.ts';
import { TransactionReceipt } from 'viem';
import wei from 'utils/wei';

export async function parseChestDropLog({
  chestAddress,
  receipt,
  telegramId,
}: {
  receipt: TransactionReceipt;
  chestAddress: string;
  telegramId: number;
}): { nft?: { tokenId: number; typeId: number }; token?: { amount: number; address: string } } {
  const logs = receipt.logs.filter((l) => l.address === chestAddress);

  const itemLog = (
    (await parseLogs(CHEST_V2_ABI.find((e) => e.name === 'ItemDrop')?.inputs, logs)) as any[]
  ).filter((log) => Number(log[1]) !== Number(telegramId))[0];
  const tokenLog = (
    (await parseLogs(CHEST_V2_ABI.find((e) => e.name === 'TokenDrop')?.inputs, logs)) as any[]
  ).filter((log) => Number(log[1]) === Number(telegramId))[0];

  if (itemLog) {
    const tokenId = Number(itemLog.tokenId);
    const typeId = Number(itemLog.typeId);

    return {
      nft: {
        tokenId,
        typeId,
      },
    };
  } else {
    const amount = Number(wei.from(tokenLog[3]));
    const token = tokenLog[2] as string;

    return {
      token: { amount, address: token },
    };
  }
}
