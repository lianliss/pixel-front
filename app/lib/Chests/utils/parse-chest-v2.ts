import { IChainChest } from 'lib/Chests/hooks/useChests.ts';
import wei from 'utils/wei';

export function parseChestV2(chest: any): IChainChest {
  return {
    ...chest,
    chestId: Number(chest.chestId),
    drop: chest.drop.map((el) => ({
      ...el,
      ratio: +wei.from(el.ratio, 4),
      items: el.items.map(Number),
      amount: +wei.from(el.amount),
      maxAmount: +wei.from(el.maxAmount),
    })),
  };
}
