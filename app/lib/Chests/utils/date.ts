import moment from 'moment/moment';

export function getStartOfNextDay(date?: Date) {
  return moment(date).add(1, 'day').startOf('day').toDate();
}

export function getStartOfNextWeek(date?: Date) {
  return moment(date).add(1, 'week').startOf('week').toDate();
}
