import { IChestDrop } from 'lib/Chests/api-server/chest.types.ts';
import { IChestDailyReward } from 'lib/Chests/utils/chest-daily-rewards.ts';
import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';

export function convertChestDropsToRewards(drop: IChestDrop[]): IChestDailyReward[] {
  if (!drop.length) {
    return [];
  }

  const sorted = drop.sort((a, b) => b.ratio - a.ratio);

  const result = sorted.map(
    (el): IChestDailyReward => ({
      type: el.isShard ? 'pixel' : el.isToken ? 'coin' : 'nft',
      chance: (el.ratio / 10_000) * 100,
      amount: el.maxAmount || el.amount,
    })
  );

  const nft = result.filter((el) => el.type === 'nft');
  const other = result.filter((el) => el.type !== 'nft');
  const nftByRarity = {
    [NftItemRarity.Common]: nft[0]?.chance || 0,
    [NftItemRarity.UnCommon]: nft[1]?.chance || 0 + nft[2]?.chance || 0,
    [NftItemRarity.Rare]: nft[3]?.chance || 0 + nft[4]?.chance || 0,
    [NftItemRarity.Epic]: nft[5]?.chance || 0 + nft[6]?.chance || 0,
    [NftItemRarity.Legendary]: nft[7]?.chance || 0 + nft[8]?.chance || 0,
  };

  return [
    ...other,
    ...Object.keys(nftByRarity).map(
      (k): IChestDailyReward => ({
        rarity: k as NftItemRarity,
        chance: nftByRarity[k],
        type: 'nft',
      })
    ),
  ].sort((a, b) => b.chance - a.chance);
}
