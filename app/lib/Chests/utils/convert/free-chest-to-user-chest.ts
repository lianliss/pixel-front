import { IUserChestDto } from 'lib/Chests/api-server/chest.types.ts';
import { IChainChest } from 'lib/Chests/hooks/deprecated/useOldUserChests.ts';

const defaultImage = require('assets/img/chest.png') as string;

export function freeChestToUserChest(chainChest: IChainChest, chestAddress: string): IUserChestDto {
  return {
    chestId: chainChest.chestId,
    count: 1,
    id: chainChest.chestId.toString(),
    contractId: chestAddress,
    userAddress: '',
    createdAt: new Date().getTime(),
    updatedAt: new Date().getTime(),
    chest: {
      chestId: chainChest.chestId,
      image: defaultImage,
      createdAt: new Date().getTime(),
      contractId: chestAddress,
      id: chainChest.chestId.toString(),
      title: 'Free Box',
      updatedAt: new Date().getTime(),
      drop: undefined,
    },
  };
}
