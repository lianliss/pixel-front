import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';

export type IChestDailyReward = {
  chance: number;
  type: 'coin' | 'pixel' | 'nft';
  amount?: number;
  rarity?: NftItemRarity;
};

const BASE_VALUE = 100;
const MULTIPLIER = 1.25;
const DEL = 2.7;

export const getChestDailyReward = (level: number): IChestDailyReward[] => {
  const chance1 = BASE_VALUE / MULTIPLIER ** level;
  const chance2 = (BASE_VALUE - chance1) / DEL;
  const chance3 = (BASE_VALUE - (chance1 + chance2)) / DEL;
  const chance4 = (BASE_VALUE - (chance1 + chance2 + chance3)) / DEL;
  const chance5 = (BASE_VALUE - (chance1 + chance2 + chance3 + chance4)) / DEL;
  const chance6 = (BASE_VALUE - (chance1 + chance2 + chance3 + chance4 + chance5)) / DEL;
  const chance7 = (BASE_VALUE - (chance1 + chance2 + chance3 + chance4 + chance5 + chance6)) / DEL;
  const chance8 =
    (BASE_VALUE - (chance1 + chance2 + chance3 + chance4 + chance5 + chance6 + chance7)) / DEL;
  const chance9 =
    (BASE_VALUE - (chance1 + chance2 + chance3 + chance4 + chance5 + chance6 + chance7 + chance8)) /
    DEL;
  const chance10 =
    (BASE_VALUE -
      (chance1 + chance2 + chance3 + chance4 + chance5 + chance6 + chance7 + chance8 + chance9)) /
    DEL;
  const chance11 =
    (BASE_VALUE -
      (chance1 +
        chance2 +
        chance3 +
        chance4 +
        chance5 +
        chance6 +
        chance7 +
        chance8 +
        chance9 +
        chance10)) /
    DEL;
  const chance12 =
    (BASE_VALUE -
      (chance1 +
        chance2 +
        chance3 +
        chance4 +
        chance5 +
        chance6 +
        chance7 +
        chance8 +
        chance9 +
        chance10 +
        chance11)) /
    DEL;
  const chance13 =
    (BASE_VALUE -
      (chance1 +
        chance2 +
        chance3 +
        chance4 +
        chance5 +
        chance6 +
        chance7 +
        chance8 +
        chance9 +
        chance10 +
        chance11 +
        chance12)) /
    DEL;
  const chance14 =
    (BASE_VALUE -
      (chance1 +
        chance2 +
        chance3 +
        chance4 +
        chance5 +
        chance6 +
        chance7 +
        chance8 +
        chance9 +
        chance10 +
        chance11 +
        chance12 +
        chance13)) /
    DEL;
  const chance15 =
    BASE_VALUE -
    (chance1 +
      chance2 +
      chance3 +
      chance4 +
      chance5 +
      chance6 +
      chance7 +
      chance8 +
      chance9 +
      chance10 +
      chance11 +
      chance12 +
      chance13 +
      chance14);

  return [
    {
      type: 'pixel',
      amount: 0.01,
      chance: chance1,
      rarity: NftItemRarity.Common,
    },
    {
      type: 'nft',
      chance: chance2,
      rarity: NftItemRarity.Common,
    },
    {
      type: 'coin',
      amount: 0.01,
      chance: chance3,
    },
    {
      type: 'pixel',
      amount: 0.1,
      chance: chance4,
    },
    {
      type: 'nft',
      chance: chance5,
      rarity: NftItemRarity.UnCommon,
    },
    {
      type: 'coin',
      amount: 0.1,
      chance: chance6,
    },
    {
      type: 'pixel',
      amount: 0.25,
      chance: chance7,
    },
    {
      type: 'nft',
      chance: chance8,
      rarity: NftItemRarity.Rare,
    },
    {
      type: 'coin',
      amount: 0.25,
      chance: chance9,
    },
    {
      type: 'pixel',
      amount: 0.5,
      chance: chance10,
    },
    {
      type: 'nft',
      chance: chance11,
      rarity: NftItemRarity.Epic,
    },
    {
      type: 'coin',
      amount: 0.5,
      chance: chance12,
    },
    {
      type: 'pixel',
      amount: 1,
      chance: chance13,
    },
    {
      type: 'nft',
      chance: chance14,
      rarity: NftItemRarity.Legendary,
    },
    {
      type: 'coin',
      amount: 1,
      chance: chance15,
    },
  ];
};
