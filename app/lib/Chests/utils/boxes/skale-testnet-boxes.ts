import { IChestSaleBox } from 'lib/Chests/utils/constants.ts';

export const SKALE_TESTNET_BOXES: Array<IChestSaleBox | null> = [
  null,
  {
    id: 0,
    title: 'Pixel Box',
    borderColor: 'rgba(236, 25, 195, 1)',
    symbol: 'PXLs',
    // countdown: new Date('2024-11-01T11:19:30'),
    image: 'https://storage.hellopixel.network/assets/chests/pixel.png',
    total: 50000,
    ratios: [
      {
        percent: 50,
        image: require('assets/mining/mining.png') as string,
      },
      {
        percent: 27.7778,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00500.jpg',
      },
      {
        percent: 13.8889,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00585.jpg',
      },
      {
        percent: 5.9524,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00557.jpg',
      },
      {
        percent: 1.9841,
        image: 'https://storage.hellopixel.network/nft-skale/backpack/00619.jpg',
      },
      {
        percent: 0.3968,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00540.jpg',
      },
    ],
    type: 'pixel',
  },
  {
    id: 1,
    title: 'SKL Box',
    symbol: 'SKL',
    borderColor: 'rgba(243, 111, 5, 1)',
    image: 'https://storage.hellopixel.network/assets/chests/skl.png',
    total: 50000,
    ratios: [
      {
        percent: 0,
        image: '',
      },
      {
        percent: 33.3333,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00500.jpg',
      },
      {
        percent: 29.6296,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00585.jpg',
      },
      {
        percent: 26.455,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00557.jpg',
      },
      {
        percent: 8.8183,
        image: 'https://storage.hellopixel.network/nft-skale/backpack/00619.jpg',
      },
      {
        percent: 1.7637,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00540.jpg',
      },
    ],
    type: 'erc20',
  },
];
