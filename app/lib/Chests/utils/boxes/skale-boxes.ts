import { IS_DEVELOP, IS_STAGE } from '@cfg/config.ts';
import { IChestSaleBox } from 'lib/Chests/utils/constants.ts';

export const SKALE_CHEST_SALE_BOXES: Array<IChestSaleBox | null> = [
  IS_DEVELOP || IS_STAGE
    ? {
        id: 0,
        title: 'Test Box',
        image: 'https://storage.hellopixel.network/assets/chests/skl.png',
        total: 30,
        type: 'erc20',
        symbol: 'SKL',
        // https://storage.hellopixel.network/nft-skale/pocket/00500.jpg
        // https://storage.hellopixel.network/nft-skale/pocket/00585.jpg
        // https://storage.hellopixel.network/nft-skale/pocket/00557.jpg
        // https://storage.hellopixel.network/nft-skale/backpack/00619.jpg
        // https://storage.hellopixel.network/nft-skale/pocket/00540.jpg

        ratios: [
          {
            percent: 0,
            image: '',
          },
          {
            percent: 50,
            image: 'https://storage.hellopixel.network/nft-skale/pocket/00500.jpg',
          },
          {
            percent: 28.5714,
            image: 'https://storage.hellopixel.network/nft-skale/pocket/00585.jpg',
          },
          {
            percent: 14.2857,
            image: 'https://storage.hellopixel.network/nft-skale/pocket/00557.jpg',
          },
          {
            percent: 5.7143,
            image: 'https://storage.hellopixel.network/nft-skale/backpack/00619.jpg',
          },
          {
            percent: 1.4286,
            image: 'https://storage.hellopixel.network/nft-skale/pocket/00540.jpg',
          },
        ],
      }
    : null,
  // typeId 550
  {
    id: 1,
    title: 'Pixel Box',
    image: 'https://storage.hellopixel.network/assets/chests/pixel.png',
    countdown: new Date('2024-11-01T14:00:00'),
    total: 20000,
    type: 'pixel',
    symbol: 'PXLs',
    ratios: [
      {
        percent: 50,
        image: require('assets/mining/mining.png') as string,
      },
      {
        percent: 27.7778,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00500.jpg',
      },
      {
        percent: 13.8889,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00585.jpg',
      },
      {
        percent: 5.9524,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00557.jpg',
      },
      {
        percent: 1.9841,
        image: 'https://storage.hellopixel.network/nft-skale/backpack/00619.jpg',
      },
      {
        percent: 0.3968,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00540.jpg',
      },
    ],
  },

  // typeId 551
  {
    id: 2,
    title: 'SKL Box',
    image: 'https://storage.hellopixel.network/assets/chests/skl.png',
    countdown: new Date('2024-11-02T14:00:00'),
    total: 10_000,
    type: 'erc20',
    symbol: 'SKL',
    // 50 28,5714 14,2857 5.7143 1.4286
    ratios: [
      {
        percent: 0,
        image: '',
      },
      {
        percent: 33.3333,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00500.jpg',
      },
      {
        percent: 29.6296,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00585.jpg',
      },
      {
        percent: 26.455,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00557.jpg',
      },
      {
        percent: 8.8183,
        image: 'https://storage.hellopixel.network/nft-skale/backpack/00619.jpg',
      },
      {
        percent: 1.7637,
        image: 'https://storage.hellopixel.network/nft-skale/pocket/00540.jpg',
      },
    ],
  },
];
