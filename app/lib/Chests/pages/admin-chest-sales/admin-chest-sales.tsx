import React from 'react';
import { AdminLayout } from 'lib/AdminV5/components/AdminLayout/AdminLayout.tsx';
import { ChestSalesTable } from 'lib/Chests/components/_admin/ChestSalesTable.tsx';

function AdminChestSales() {
  return (
    <AdminLayout>
      <ChestSalesTable />
    </AdminLayout>
  );
}

export default AdminChestSales;
