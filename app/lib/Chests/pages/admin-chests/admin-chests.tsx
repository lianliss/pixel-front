import React from 'react';
import { AdminLayout } from 'lib/AdminV5/components/AdminLayout/AdminLayout.tsx';
import ChestsGrid from 'lib/Chests/components/_admin/ChestsGrid/ChestsGrid.tsx';

function EditorWrap() {
  return (
    <AdminLayout>
      <ChestsGrid />
    </AdminLayout>
  );
}

export default EditorWrap;
