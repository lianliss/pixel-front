'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  ICheckPartnerChestDailyDto,
  ICheckPartnerChestDailyResponse,
  ICheckQuestChestDailyDto,
  ICheckQuestChestDailyResponse,
  IClaimChestDailyDto,
  IClaimChestDailyResponse,
  IGetChestDailyDto,
  IGetChestDailyResponse,
  IGetUserChestDailyDto,
  IGetUserChestDailyResponse,
  IReadNewsChestDailyDto,
  IReadNewsChestDailyResponse,
} from 'lib/Chests/api-server/chest-daily.types.ts';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';
import {
  pixelMessageKey,
  pixelSignKey,
  telegramAuthKey,
} from '../../../shared/const/localStorage.ts';
import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';

import { axiosInstance } from 'utils/libs/axios.ts';

export class ChestDailyApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async get(payload: IGetChestDailyDto): Promise<IGetChestDailyResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetChestDailyResponse },
      AxiosResponse<{ data: IGetChestDailyResponse }>,
      IGetChestDailyDto
    >(`/api/v1/chest-daily/get/test`, {
      baseURL: this.url,
      params: {
        rang: payload.rang,
        contractId: payload.contractId,
      },
    });

    return data.data;
  }

  async getLatest(payload: IGetUserChestDailyDto): Promise<IGetUserChestDailyResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetUserChestDailyResponse },
      AxiosResponse<{ data: IGetUserChestDailyResponse }>,
      IGetUserChestDailyDto
    >(`/api/v1/chest-daily/current`, {
      baseURL: this.url,
      params: {
        userAddress: payload.userAddress,
        chainId: payload.chainId,
      },
    });

    return data.data;
  }

  async claim(payload: IClaimChestDailyDto): Promise<IClaimChestDailyResponse> {
    const { data } = await axiosInstance.post<
      { data: IClaimChestDailyResponse },
      AxiosResponse<{ data: IClaimChestDailyResponse }>,
      Omit<IClaimChestDailyDto, 'userAddress'>
    >(
      `/api/v1/chest-daily/claim`,
      {},
      {
        baseURL: this.url,
        params: {
          chainId: payload.chainId,
        },
      }
    );

    return data.data;
  }

  async readNews(payload: IReadNewsChestDailyDto): Promise<IReadNewsChestDailyResponse> {
    const { data } = await axiosInstance.post<
      { data: IReadNewsChestDailyResponse },
      AxiosResponse<{ data: IReadNewsChestDailyResponse }>,
      Omit<IReadNewsChestDailyDto, 'userAddress'>
    >(
      `/api/v1/chest-daily/read-news`,
      { chainId: payload.chainId },
      {
        baseURL: this.url,
        params: {
          chainId: payload.chainId,
        },
      }
    );

    return data.data;
  }

  async checkQuest(payload: ICheckQuestChestDailyDto): Promise<ICheckQuestChestDailyResponse> {
    const { data } = await axiosInstance.post<
      { data: ICheckQuestChestDailyResponse },
      AxiosResponse<{ data: ICheckQuestChestDailyResponse }>,
      Omit<ICheckQuestChestDailyDto, 'userAddress'>
    >(
      `/api/v1/chest-daily/check/quest`,
      { chainId: payload.chainId },
      {
        baseURL: this.url,
        params: {
          chainId: payload.chainId,
        },
      }
    );

    return data.data;
  }

  async checkPartner(
    payload: ICheckPartnerChestDailyDto
  ): Promise<ICheckPartnerChestDailyResponse> {
    const { data } = await axiosInstance.post<
      { data: ICheckPartnerChestDailyResponse },
      AxiosResponse<{ data: ICheckPartnerChestDailyResponse }>,
      Omit<ICheckPartnerChestDailyDto, 'userAddress'>
    >(
      `/api/v1/chest-daily/check/partner`,
      { chainId: payload.chainId },
      {
        baseURL: this.url,
        params: {
          chainId: payload.chainId,
        },
      }
    );

    return data.data;
  }
}

export const chestDailyApi = new ChestDailyApi({ url: 'https://api.hellopixel.network' });
