// request
import { IChestDto } from 'lib/Chests/api-server/chest.types.ts';

export type IGetUserChestWeeklyDto = { chestAddress: string; userAddress: string };

export type IClaimUserChestWeeklyDto = {
  chestAddress: string;
} & { userAddress: string };

export type IGetChestWeeklyDto = { chestAddress: string; rangLevel: number };

// response

export type IGetUserChestWeeklyResponse = IUserChestWeeklyDto | null;

export type IClaimUserChestWeeklyResponse = IUserChestWeeklyDto;

export type IGetChestWeeklyResponse = IChestDto;

// entity

export type IUserChestWeekly<TDate = Date> = {
  id: string;

  //
  userAddress: string;
  chestAddress: string;

  //
  rangLevel: number;
  chestsCount: number;
  weekIndex: string;

  //
  rewardedChestsCount: number;
  rewardedAt: TDate | null;
  claimedAt: TDate | null;
  createdAt: TDate;
};

// dto

export type IUserChestWeeklyDto<TDate = number> = IUserChestWeekly<TDate>;
