import { createEffect, createStore } from 'effector';
import { AxiosError } from 'axios';
import {
  ICheckPartnerChestDailyDto,
  ICheckQuestChestDailyDto,
  IClaimChestDailyDto,
  IGetChestDailyDto,
  IGetUserChestDailyDto,
  IGetUserChestDailyResponse,
  IReadNewsChestDailyDto,
} from 'lib/Chests/api-server/chest-daily.types.ts';
import { chestDailyApi } from 'lib/Chests/api-server/chest-daily.api.ts';
import { createStoreState } from 'utils/store/createStoreState.ts';

// current

export const loadChestDaily = createEffect(async (opts: IGetUserChestDailyDto) => {
  const res = await chestDailyApi.getLatest(opts);
  return res;
});

export const claimChestDaily = createEffect(async (opts: IClaimChestDailyDto) => {
  const res = await chestDailyApi.claim(opts);
  return res;
});

export const readNewsChestDaily = createEffect(async (opts: IReadNewsChestDailyDto) => {
  const res = await chestDailyApi.readNews(opts);
  return res;
});

export const checQuestChestDaily = createEffect(async (opts: ICheckQuestChestDailyDto) => {
  const res = await chestDailyApi.checkQuest(opts);
  return res;
});

export const checkPartnerChestDaily = createEffect(async (opts: ICheckPartnerChestDailyDto) => {
  const res = await chestDailyApi.checkPartner(opts);
  return res;
});

export const userChestDailyStore = createStore({
  loading: true,
  error: null as Error | null,
  state: null as IGetUserChestDailyResponse | null,
})
  .on(claimChestDaily.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(readNewsChestDaily.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(checQuestChestDaily.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(checkPartnerChestDaily.doneData, (_prev, next) => ({
    loading: false,
    error: null,
    state: next,
  }))
  .on(loadChestDaily.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadChestDaily.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadChestDaily.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// get

export const getChestDaily = createEffect(async (opts: IGetChestDailyDto) => {
  const res = await chestDailyApi.get(opts);
  return res;
});

export const chestDailyStore = createStoreState(getChestDaily);
