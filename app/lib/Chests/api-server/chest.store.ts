import { createEffect, createEvent, createStore } from 'effector';
import { AxiosError } from 'axios';
import {
  IFindUserChestsDto,
  IFindUserChestsResponse,
  IGetChestDto,
  IGetChestResponse,
  IGetChestUserDto,
  IGetChestUserResponse,
} from 'lib/Chests/api-server/chest.types.ts';
import { chestApi } from 'lib/Chests/api-server/chest.api.ts';

// chest user stats

export const loadChestUser = createEffect(async (opts: IGetChestUserDto) => {
  const res = await chestApi.getChestUser(opts);
  return res;
});
export const clearChestUser = createEvent();

export const chestUserStore = createStore({
  loading: true,
  error: null as Error | null,
  state: null as IGetChestUserResponse | null,
})
  .on(clearChestUser, () => ({ state: null, error: null, loading: false }))
  .on(loadChestUser.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadChestUser.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadChestUser.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// chest get

export const loadChest = createEffect(async (opts: IGetChestDto) => {
  const res = await chestApi.getChest(opts);
  return res;
});

export const chestStore = createStore({
  loading: true,
  error: null as Error | null,
  state: null as IGetChestResponse | null,
})
  .on(loadChest.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadChest.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadChest.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// chest user find

export const loadUserChests = createEffect(async (opts: IFindUserChestsDto) => {
  const res = await chestApi.findUserChests(opts);
  return res;
});

export const userChestsStore = createStore({
  loading: true,
  error: null as Error | null,
  state: { rows: [], count: 0 } as IFindUserChestsResponse,
})
  .on(loadUserChests.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadUserChests.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: { rows: [], count: 0 },
  }))
  .on(loadUserChests.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));
