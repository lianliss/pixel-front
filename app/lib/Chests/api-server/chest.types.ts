import { IPaginatedQuery } from 'lib/Leaderboard/api-server/leaderboard.types.ts';
import { IPaginatedResult } from 'lib/Quests/api-server/quests.types.ts';

export type IGetChestUserDto = {
  userAddress: string;
  contract: string;
};

// response

export type IGetChestUserResponse = {
  coinChests: number;
  pixelChests: number;
  coinSpent: number;
  pixelSpent: number;
};

// entities

export type IChest<TDate = Date> = {
  id: string;

  chestId: number;
  contractId: string;
  title: string;
  image: string;

  updatedAt: TDate;
  createdAt: TDate;
};

export type IUserChest<TDate = Date> = {
  id: string;

  userAddress: string;
  count: number;

  chestId: number;
  contractId: string;

  updatedAt: TDate;
  createdAt: TDate;

  chest?: IChest<TDate>;
};

export type IChestDrop<TDate = Date> = {
  id: string;

  chestId: string;

  ratio: number;
  isToken: boolean;
  isShard: boolean;
  items: number[];
  tokenAddress: string;
  amount: number;
  maxAmount: number;
};

// dto

export type IUserChestDto<TDate = number> = Omit<IUserChest<TDate>, 'chest'> & {
  chest: IChestDto<TDate> | null;
};

export type IChestDto<TDate = number> = Omit<IChest<TDate>, 'drop'> & {
  drops?: IChestDropDto<TDate>[];
};

export type IChestDropDto<TDate = number> = IChestDrop<TDate>;

// request dto

export type IGetChestDto = { chestId: number; contractId: string };

export type IFindUserChestsDto = { userAddress: string; contractId: string } & IPaginatedQuery;

// response dto

export type IGetChestResponse = IChestDto;

export type IFindUserChestsResponse = IPaginatedResult<IUserChestDto>;
