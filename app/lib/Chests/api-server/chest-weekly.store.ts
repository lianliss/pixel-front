import { createEffect, createStore } from 'effector';
import { AxiosError } from 'axios';
import {
  IClaimUserChestWeeklyDto,
  IGetChestWeeklyDto,
  IGetUserChestWeeklyDto,
  IGetUserChestWeeklyResponse,
} from 'lib/Chests/api-server/chest-weekly.types.ts';
import { chestWeeklyApi } from 'lib/Chests/api-server/chest-weekly.api.ts';
import { createStoreState } from 'utils/store/createStoreState.ts';

// current

export const loadChestWeekly = createEffect(async (opts: IGetUserChestWeeklyDto) => {
  const res = await chestWeeklyApi.getLatest(opts);
  return res;
});

export const claimChestWeekly = createEffect(async (opts: IClaimUserChestWeeklyDto) => {
  const res = await chestWeeklyApi.claim(opts);
  return res;
});

export const userChestWeeklyStore = createStore({
  loading: true,
  error: null as Error | null,
  state: null as IGetUserChestWeeklyResponse | null,
})
  .on(claimChestWeekly.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadChestWeekly.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadChestWeekly.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadChestWeekly.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// get

export const getChestWeekly = createEffect(async (opts: IGetChestWeeklyDto) => {
  const res = await chestWeeklyApi.get(opts);
  return res;
});

export const chestWeeklyStore = createStoreState(getChestWeekly);
