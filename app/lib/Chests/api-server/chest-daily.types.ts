// request
import { IUserChestCounterDto } from 'lib/Quests/api-server/quests.types.ts';
import { IChestDto } from 'lib/Chests/api-server/chest.types.ts';

// request

export type IGetUserChestDailyDto = { userAddress: string; chainId: number };

export type IReadNewsChestDailyDto = { userAddress: string; chainId: number };

export type ICheckQuestChestDailyDto = { userAddress: string; chainId: number };

export type ICheckPartnerChestDailyDto = { userAddress: string; chainId: number };

// eslint-disable-next-line
export type IClaimChestDailyDto = { userAddress: string; chainId: number };

export type IGetChestDailyDto = { contractId: string; rang: number };

// response

export type IClaimChestDailyResponse = IUserChestDailyDto;

export type IReadNewsChestDailyResponse = IUserChestDailyDto;

export type IGetUserChestDailyResponse = IUserChestDailyDto | null;

export type ICheckQuestChestDailyResponse = IUserChestDailyDto;

export type ICheckPartnerChestDailyResponse = IUserChestDailyDto;

export type IGetChestDailyResponse = IChestDto;

// dto

export type IUserChestDailyDto<TDate = number> = Omit<
  IUserChestDaily<TDate>,
  'completedAt' | 'rewardedAt' | 'completedRangLevel'
> & {
  completedAt: TDate | null;
  rewardedAt: TDate | null;
  completedRangLevel: number | null;
  userQuestCounter: IUserChestCounterDto | null;
};

// entity

export type IUserChestDaily<TDate = Date> = {
  id: string;
  userAddress: string;

  isNewsRead: boolean;
  isQuestCompleted: boolean;
  isSkaleClaimed: boolean;
  isSgbClaimed: boolean;
  isZarClaimed: boolean;
  isPartner1: boolean;

  completedRangLevel?: number;
  rewardedAt?: TDate | null;
  completedAt?: TDate | null;
  claimedAt: TDate | null;
  createdAt: TDate;
};
