'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  IClaimUserChestWeeklyDto,
  IClaimUserChestWeeklyResponse,
  IGetChestWeeklyDto,
  IGetChestWeeklyResponse,
  IGetUserChestWeeklyDto,
  IGetUserChestWeeklyResponse,
} from 'lib/Chests/api-server/chest-weekly.types.ts';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';
import {
  pixelMessageKey,
  pixelSignKey,
  telegramAuthKey,
} from '../../../shared/const/localStorage.ts';
import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';

import { axiosInstance } from 'utils/libs/axios.ts';

export class ChestWeeklyApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async get(payload: IGetChestWeeklyDto): Promise<IGetChestWeeklyResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetChestWeeklyResponse },
      AxiosResponse<{ data: IGetChestWeeklyResponse }>,
      IGetChestWeeklyDto
    >(`/api/v1/chest/weekly/get`, {
      baseURL: this.url,
      params: {
        rangLevel: payload.rangLevel,
        chestAddress: payload.chestAddress,
      },
    });

    return data.data;
  }

  async getLatest(payload: IGetUserChestWeeklyDto): Promise<IGetUserChestWeeklyResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetUserChestWeeklyResponse },
      AxiosResponse<{ data: IGetUserChestWeeklyResponse }>,
      IGetUserChestWeeklyDto
    >(`/api/v1/chest/weekly/user`, {
      baseURL: this.url,
      params: {
        userAddress: payload.userAddress,
        chestAddress: payload.chestAddress,
      },
    });

    return data.data;
  }

  async claim({
    userAddress,
    chestAddress,
  }: IClaimUserChestWeeklyDto): Promise<IClaimUserChestWeeklyResponse> {
    const { data } = await axiosInstance.post<
      { data: IClaimUserChestWeeklyResponse },
      AxiosResponse<{ data: IClaimUserChestWeeklyResponse }>,
      Omit<IClaimUserChestWeeklyDto, 'userAddress'>
    >(
      `/api/v1/chest/weekly/claim`,
      { chestAddress },
      {
        baseURL: this.url,
      }
    );

    return data.data;
  }
}

export const chestWeeklyApi = new ChestWeeklyApi({ url: 'https://api.hellopixel.network' });
