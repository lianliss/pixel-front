'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  IFindUserChestsDto,
  IFindUserChestsResponse,
  IGetChestDto,
  IGetChestResponse,
  IGetChestUserDto,
  IGetChestUserResponse,
} from 'lib/Chests/api-server/chest.types.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class ChestApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async getChestUser(payload: IGetChestUserDto): Promise<IGetChestUserResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetChestUserResponse },
      AxiosResponse<{ data: IGetChestUserResponse }>,
      IGetChestUserDto
    >(`/api/v1/chest/user`, {
      baseURL: this.url,
      params: {
        userAddress: payload.userAddress,
        contract: payload.contract,
      },
    });

    return data.data;
  }

  async getChest(params: IGetChestDto): Promise<IGetChestResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetChestResponse },
      AxiosResponse<{ data: IGetChestResponse }>,
      IGetChestDto
    >(`/api/v1/chest/get`, {
      baseURL: this.url,
      params,
    });

    return data.data;
  }

  async findUserChests(params: IFindUserChestsDto): Promise<IFindUserChestsResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindUserChestsResponse },
      AxiosResponse<{ data: IFindUserChestsResponse }>,
      IFindUserChestsDto
    >(`/api/v1/chest/find/user`, {
      baseURL: this.url,
      params,
    });

    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const chestApi = new ChestApi({ url: 'https://api.hellopixel.network' });
