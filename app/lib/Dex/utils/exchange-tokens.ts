import { TokenInfo } from '@sunmoon11100/uniswap-widgets';
import { Chain } from 'services/multichain/chains';
import { EXCHANGE_SKALE_TESTNET_TOKENS } from 'lib/Dex/utils/chains/skale-testnet.tokens.ts';
import { EXCHANGE_POLYGON_TOKENS } from 'lib/Dex/utils/chains/polygon.tokens.ts';
import { EXCHANGE_SKALE_TOKENS } from 'lib/Dex/utils/chains/skale.tokens.ts';
import { EXCHANGE_COSTON2_TOKENS } from 'lib/Dex/utils/chains/coston2.tokens.ts';

export const EXCHANGE_TOKENS: Partial<Record<Chain, TokenInfo[]>> = {
  [Chain.COSTON2]: EXCHANGE_COSTON2_TOKENS,
  [Chain.SKALE_TEST]: EXCHANGE_SKALE_TESTNET_TOKENS,
  [Chain.SKALE]: EXCHANGE_SKALE_TOKENS,
  [Chain.POLYGON_MAINNET]: EXCHANGE_POLYGON_TOKENS,
};
