import { TokenInfo } from '@sunmoon11100/uniswap-widgets';
import { POLYGON_USDC } from '@chain/tokens/chains/polygon.tokens.ts';

export const EXCHANGE_POLYGON_TOKENS: TokenInfo[] = [POLYGON_USDC];
