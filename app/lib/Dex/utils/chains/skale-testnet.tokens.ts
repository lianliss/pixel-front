import { TokenInfo } from '@sunmoon11100/uniswap-widgets';
import { SKALETEST_PXL, SKALETEST_SKL } from '@chain/tokens/chains/skale-test.tokens.ts';

export const EXCHANGE_SKALE_TESTNET_TOKENS: TokenInfo[] = [SKALETEST_SKL, SKALETEST_PXL];
