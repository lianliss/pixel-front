import { TokenInfo } from '@sunmoon11100/uniswap-widgets';
import {
  COSTON2_PXL_SHARD,
  COSTON2_USDC,
  COSTON2_USDT,
  COSTON2_WRAPPED_FLARE,
} from '@chain/tokens/chains/coston2.tokens.ts';

export const EXCHANGE_COSTON2_TOKENS: TokenInfo[] = [
  COSTON2_WRAPPED_FLARE,
  COSTON2_PXL_SHARD,
  COSTON2_USDC,
  COSTON2_USDT,
];
