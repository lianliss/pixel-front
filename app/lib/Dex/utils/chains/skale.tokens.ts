import { TokenInfo } from '@sunmoon11100/uniswap-widgets';
import { SKALE_PXL, SKALE_SKL } from '@chain/tokens/chains/skale.tokens.ts';

export const EXCHANGE_SKALE_TOKENS: TokenInfo[] = [SKALE_SKL, SKALE_PXL];
