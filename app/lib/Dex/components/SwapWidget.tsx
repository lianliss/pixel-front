import React from 'react';
import { SwapWidget as BaseSwapWidget, SwapWidgetSkeleton } from '@sunmoon11100/uniswap-widgets';

import { IS_WAGMI } from '@cfg/config.ts';
import { EXCHANGE_TOKENS } from 'lib/Dex/utils/exchange-tokens.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useEthersSigner } from '@chain/utils/useEthersSigner.ts';
import { ErrorBoundary } from 'react-error-boundary';

function SwapWidget() {
  if (!IS_WAGMI) {
    return 'wagmi disabled';
  }

  const account = useAccount();
  const signer = useEthersSigner({ chainId: account.chainId });
  console.log(signer);
  const exchangeTokens = EXCHANGE_TOKENS[account.chainId] || [];
  const defaultInput = exchangeTokens[0];
  const defaultOutput = exchangeTokens[1];

  return (
    <div>
      {(!signer || !account.isConnected) && !!exchangeTokens.length && <SwapWidgetSkeleton />}
      {!!exchangeTokens.length && signer && account.isConnected && (
        <ErrorBoundary fallback={<div>Failed to load swap widget</div>}>
          <BaseSwapWidget
            // defaultChainId={account.chainId}
            hideConnectionUI
            defaultInputTokenAddress={defaultInput?.address}
            defaultOutputTokenAddress={defaultOutput?.address}
            tokenList={exchangeTokens}
            brandedFooter={false}
            provider={signer.provider}
          />
        </ErrorBoundary>
      )}
    </div>
  );
}

export default SwapWidget;
