import React from 'react';
import styles from './ExchangePage.module.scss';
// import SwapWidget from 'lib/Dex/components/SwapWidget.tsx';
// import { ErrorBoundary } from 'react-error-boundary';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import { Chain } from 'services/multichain/chains';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import NotAvailableChain from '../../../../shared/blocks/NotAvailableChain/NotAvailableChain.tsx';
import { EXCHANGE_TOKENS } from 'lib/Dex/utils/exchange-tokens.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTranslation } from 'react-i18next';

function ExchangePage() {
  const account = useAccount();
  const exchangeTokens = EXCHANGE_TOKENS[account.chainId] || [];
  const { t } = useTranslation('exchanger');

  return (
    <div className={styles.root}>
      <ChainSwitcher chains={[Chain.COSTON2, Chain.POLYGON_MAINNET]} />

      <Typography variant='h2' align='center' fontWeight='bold' sx={{ mb: 1, mt: 1 }}>
        {t('Exchange')}
      </Typography>

      {/*{!!exchangeTokens.length && <SwapWidget />}*/}

      {!exchangeTokens.length && (
        <NotAvailableChain
          title={t('No available')}
          summary={t('Exchange not available on current network')}
        />
      )}
    </div>
  );
}

export default ExchangePage;
