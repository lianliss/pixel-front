'use strict';

import axios, { AxiosResponse } from 'axios';
import { IFindFeatureResponse } from 'lib/ab/api-server/ab.types.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class AbApi {
  url: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async fetch(): Promise<IFindFeatureResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindFeatureResponse },
      AxiosResponse<{ data: IFindFeatureResponse }>,
      {}
    >(`/api/v1/feature`, {
      baseURL: this.url,
    });
    return data.data;
  }
}

export const abApi = new AbApi({ url: 'https://api.hellopixel.network' });
