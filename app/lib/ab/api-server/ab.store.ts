import { createEffect, createStore } from 'effector';
import { abApi } from 'lib/ab/api-server/ab.api.ts';
import { IAbKey } from 'lib/ab/context/ab.context.ts';

// list

export const loadAb = createEffect(async (opts: {}) => {
  const res = await abApi.fetch();
  return res.rows.reduce((acc, el) => ({ ...acc, [el.id]: el.enabled }), {}) as {
    [k in IAbKey]?: boolean;
  };
});

export const abStore = createStore<{
  data: {
    [k in IAbKey]?: boolean;
  };
  loaded: boolean;
}>({
  data: {},
  loaded: false,
}).on(loadAb.doneData, (_, next) => ({ data: next, loaded: true }));
