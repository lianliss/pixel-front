import { IPaginatedResult } from 'lib/Marketplace/api-server/market.types';

export type IGetFeatureDto = { featureId: string };

// eslint-disable-next-line
export type IFindFeatureDto = {};

export type IGetFeatureResponse = IFeatureDto;

export type IFindFeatureResponse = Omit<IPaginatedResult<IFeatureDto>, 'count'>;

//
export type IFeature<TDate = Date> = {
  id: string;

  enabled: boolean;
  createdAt: TDate;
};

export type IFeatureDto<TDate = number> = Omit<IFeature<TDate>, 'createdAt'>;
