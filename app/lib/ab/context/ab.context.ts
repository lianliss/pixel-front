import React from 'react';

export type IAbKey = 'market_tech_works' | 'leaderboard_tech_works' | 'budda_miner_enabled';

export type IAbContext = {
  isEnabled(key: IAbKey): boolean;
  loaded: boolean;
};

export const AbContext = React.createContext<IAbContext>(null);

export function useAb(): IAbContext {
  const ctx = React.useContext(AbContext);

  if (!ctx) {
    throw new Error('no ab ctx');
  }

  return ctx;
}
