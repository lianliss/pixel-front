import React from 'react';
import { useUnit } from 'effector-react';
import { abStore, loadAb } from 'lib/ab/api-server/ab.store.ts';
import { AbContext } from 'lib/ab/context/ab.context.ts';

function AbProvider(props: React.PropsWithChildren) {
  const state = useUnit(abStore);

  React.useEffect(() => {
    loadAb({})
      .then()
      .catch(() => console.error('failed to load ab'));
  }, []);

  return (
    <AbContext.Provider
      value={{
        isEnabled(key: string): boolean {
          return key && !!state.data[key];
        },
        loaded: state.loaded,
      }}
    >
      {props.children}
    </AbContext.Provider>
  );
}

export default AbProvider;
