import analytics from '../../../shared/analytics';

class MiningAnalytics {
  public tractClaim(payload: { chainId: number; userAddress: string; minerAddress: string }) {
    analytics.track(
      'claim',
      { userAddress: payload.userAddress },
      { minerAddress: payload.minerAddress, chainId: payload.chainId }
    );
  }

  public tractAccess(payload: { chainId: number; userAddress: string; minerAddress: string }) {
    analytics.track(
      'access',
      { userAddress: payload.userAddress },
      { minerAddress: payload.minerAddress, chainId: payload.chainId }
    );
  }

  public tractFailedAccess(payload: {
    chainId: number;
    userAddress: string;
    minerAddress: string;
  }) {
    analytics.track(
      'accessFailed',
      { userAddress: payload.userAddress },
      { minerAddress: payload.minerAddress, chainId: payload.chainId }
    );
  }
}

const miningAnalytics = new MiningAnalytics();

export default miningAnalytics;
