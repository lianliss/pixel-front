import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import PXLsABI from '../../../const/ABI/V5/core';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTransactionReceipt } from 'wagmi';
import { PAYABLE_ABI } from 'lib/Mining/api-contract/abi/payable.abi.ts';
import { PAYABLE_CONTRACT_ID } from 'lib/Wallet/api-contract/wallet.contracts.ts';
import { IS_PAYABLE } from '@cfg/app.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import ERC_20_ABI from 'lib/Wallet/api-contract/abi/erc-20.abi.ts';

export function useMiningUpgrade({
  payablePrice,
  onUpgrade,
}?: {
  payablePrice?: bigint;
  onUpgrade?: () => Promise<void>;
}) {
  const account = useAccount();
  const tradeToken = useTradeToken();

  const minerAddress = MINER_CONTRACT_ID[account.chainId]?.core;
  const payableAddress = PAYABLE_CONTRACT_ID[account.chainId];

  const write = useWriteContract({
    abi: PXLsABI,
    address: minerAddress,
    ignoreRecipe: true,
  });
  const writePaylable = useWriteContract({
    abi: PAYABLE_ABI,
    address: payableAddress,
  });
  const writeTradeToken = useWriteContract({
    abi: ERC_20_ABI,
    address: tradeToken.data?.address,
  });

  const recipe = useTransactionReceipt({
    hash: write.data,
    query: {
      enabled: !!write.data,
    },
  });

  const upgradeDrill = async (telegramId: number) => {
    if (payableAddress && IS_PAYABLE) {
      if (!tradeToken.data?.isCoin) {
        await writeTradeToken.writeContractAsync({
          functionName: 'approve',
          args: [payableAddress, payablePrice],
        });
      }

      return writePaylable.writeContractAsync({
        functionName: 'buySpeedLevel',
        args: [telegramId],
        overrides: payablePrice && tradeToken.data?.isCoin ? { value: payablePrice } : undefined,
      });
    } else {
      return write.writeContractAsync({
        functionName: 'buySpeedLevel',
        args: [telegramId],
      });
    }
  };
  const upgradeStorage = async (telegramId: number) => {
    if (payableAddress && IS_PAYABLE) {
      if (!tradeToken.data?.isCoin) {
        await writeTradeToken.writeContractAsync({
          functionName: 'approve',
          args: [payableAddress, payablePrice],
        });
      }

      return writePaylable.writeContractAsync({
        functionName: 'buySizeLevel',
        args: [telegramId],
        overrides: payablePrice && tradeToken.data?.isCoin ? { value: payablePrice } : undefined,
      });
    } else {
      return write.writeContractAsync({
        functionName: 'buySizeLevel',
        args: [telegramId],
      });
    }
  };
  const upgradeRefStorage = async (telegramId: number) => {
    if (payableAddress && IS_PAYABLE) {
      if (!tradeToken.data?.isCoin) {
        await writeTradeToken.writeContractAsync({
          functionName: 'approve',
          args: [payableAddress, payablePrice],
        });
      }

      return writePaylable.writeContractAsync({
        functionName: 'buyReferralLevel',
        args: [telegramId],
        overrides: payablePrice && tradeToken.data?.isCoin ? { value: payablePrice } : undefined,
      });
    } else {
      return write.writeContractAsync({
        functionName: 'buyReferralLevel',
        args: [telegramId],
      });
    }
  };

  return {
    write,
    recipe,
    loading: write.isPending || writePaylable.isPending || writeTradeToken.isPending,
    upgradeDrill,
    upgradeStorage,
    upgradeRefStorage,
  };
}
