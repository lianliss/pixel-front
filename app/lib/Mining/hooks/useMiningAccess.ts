import React, { useEffect } from 'react';
import miningAnalytics from 'lib/Mining/api-analytics';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useMiningStorage } from 'lib/Mining/hooks/useMiningStorage.ts';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';

import { useWeb3 } from 'services/web3Provider';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';
import { ZERO_ADDRESS } from '@cfg/config.ts';

export function useMiningAccess() {
  const account = useAccount();
  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;
  const { apiGetAccessForce } = useWeb3() || {};

  const minerAddress = MINER_CONTRACT_ID[account.chainId]?.core;

  // ========== Mining ==========
  const mining = useMiningStorage({
    minerAddress: minerAddress,
    telegramId,
    // type,
  });
  const noAccess = !mining.data?.account || mining.data.account?.toLowerCase() === ZERO_ADDRESS;

  // ========== State ==========
  const [isAccessLoading, setIsAccessLoading] = React.useState(false);

  // ========== Effect ==========
  useEffect(() => {
    if (account.isConnected && noAccess && mining.data) {
      // Grant access to a new address
      setIsAccessLoading(true);

      apiGetAccessForce?.(userInfo.data.parentId)
        .then(() => {
          mining.refetch();
          miningAnalytics.tractAccess({
            userAddress: account.accountAddress,
            minerAddress,
            chainId: account.chainId,
          });
        })
        .catch(() => {
          miningAnalytics.tractFailedAccess({
            userAddress: account.accountAddress,
            minerAddress,
            chainId: account.chainId,
          });
        })
        .finally(() => {
          setIsAccessLoading(false);
        });
    }
  }, [account.chainId, account.isConnected, minerAddress, noAccess, mining.data]);

  return {
    loadingData: mining.loading,
    loadingAccess: isAccessLoading,
    access: !noAccess,
  };
}
