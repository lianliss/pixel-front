import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { MINER_MATH_ABI } from 'lib/Mining/api-contract/abi/math.abi.ts';
import { MINER_MATH_ID } from 'lib/Wallet/api-contract/wallet.contracts.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';

import wei from 'utils/wei';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

export type IPayableCost = {
  formatted: number;
  value: bigint;
};

export function usePayable() {
  const account = useAccount();
  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;

  const address = MINER_MATH_ID[account.chainId];

  const costResult = useReadContract<IPayableCost, [number]>({
    functionName: 'getNextTransactionPrice',
    args: [telegramId],
    abi: MINER_MATH_ABI,
    address: address,
    skip: !account.isConnected || !address || !telegramId,
    select: (res) => {
      return {
        formatted: +wei.from(res),
        value: res,
      };
    },
  });
  const countResult = useReadContract<number, [number]>({
    functionName: 'getNextTransactionIndex',
    args: [telegramId],
    abi: MINER_MATH_ABI,
    address: address,
    skip: !account.isConnected || !address || !telegramId,
    select: (res) => Number(res),
  });
  const botModResult = useReadContract<number, [number]>({
    functionName: 'userBotMod',
    args: [telegramId],
    abi: MINER_MATH_ABI,
    address: address,
    skip: !account.isConnected || !address || !telegramId,
    select: (res) => {
      return res ? wei.from(res, 4) : 1;
    },
  });
  const extraModResult = useReadContract<number, [number]>({
    functionName: 'userExtraMod',
    args: [telegramId],
    abi: MINER_MATH_ABI,
    address: address,
    skip: !account.isConnected || !address || !telegramId,
    select: (res) => (res ? wei.from(res, 4) : 1),
  });
  const slotsModResult = useReadContract<number, [number]>({
    functionName: 'userSlotsMod',
    args: [telegramId],
    abi: MINER_MATH_ABI,
    address: address,
    skip: !account.isConnected || !address || !telegramId,
    select: (res) => (res ? wei.from(res, 4) : 1),
  });

  return {
    count: countResult,
    cost: costResult,
    costExtra: extraModResult.data,
    costBot: botModResult.data,
    costSlot: slotsModResult.data,
    rate: {
      data: botModResult.data * extraModResult.data * slotsModResult.data,
      loading: extraModResult.loading || botModResult.loading || slotsModResult.loading,
      refetch: () => {
        botModResult.refetch();
        extraModResult.refetch();
        slotsModResult.refetch();
      },
    },
    refetch: () => {
      countResult.refetch();
      costResult.refetch();
    },
  };
}
