import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { wei } from 'utils/index';
import { useMemo } from 'react';
import { MINER_ABI } from 'lib/Mining/api-contract/abi/miner.abi.ts';
import { SECOND_MINER_ABI } from 'lib/Mining/api-contract/abi/second-miner.ts';
import { FLOAT_MINER_ABI } from 'lib/Mining/api-contract/abi/float-miner.abi.ts';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { Chain } from 'services/multichain/chains';
import { MINER_UNITS_ABI } from 'lib/Mining/api-contract/abi/miner-units.abi.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { MINER_FLOAT_UNITS_ABI } from 'lib/Mining/api-contract/abi/miner-float-units.abi.ts';

export type IMiningDto = {
  balance: number;
  mined: number;
  rewardPerSecond: number;
  sizeLimit: number;
  claimTimestamp: number;
  updatedAt: number;
  account?: string;

  extra?: {
    refLimit: number;
    sizeLevel: number;
    speedLevel: number;
    referralLevel: number;
    refStorage: number;
    activity: number;
    parentId: number;
  };
};

export function useMiningStorage({
  minerAddress,
  telegramId,
  type = 'root',
}: {
  minerAddress: string;
  telegramId?: number;
  type?: 'root' | 'float' | 'second';
}) {
  const account = useAccount();
  const isUnits = account.chainId === Chain.UNITS;

  const state = useReadContract<IMiningDto | undefined>({
    // initData: {
    //   balance: 0,
    //   mined: 0,
    //   rewardPerSecond: 0,
    //   claimTimestamp: Date.now(),
    //   sizeLimit: 0,
    //   updatedAt: Date.now(),
    //   account: undefined,
    //   extra: {
    //     speedLevel: 0,
    //     refStorage: 0,
    //     refLimit: 0,
    //     sizeLevel: 0,
    //     referralLevel: 0,
    //     parentId: undefined,
    //     activity: 0,
    //   },
    // },
    abi:
      type === 'root'
        ? isUnits
          ? MINER_UNITS_ABI
          : MINER_ABI
        : type === 'second'
        ? SECOND_MINER_ABI
        : isUnits
        ? MINER_FLOAT_UNITS_ABI
        : FLOAT_MINER_ABI,
    address: minerAddress,
    functionName: 'getStorage',
    args: [telegramId],
    skip: !minerAddress || !telegramId,
  });

  const formattedData: any = useMemo(() => {
    if (state.data) {
      const data = state.data;

      // TODO cache state error
      if (data.extra) {
        return data;
      }

      if (type === 'second') {
        try {
          const rawUser = data[0];
          const rawUserClaimDate = rawUser.claimTimestamp;
          const rawMined = data[1];
          const rawSizeLimit = data[2];
          const rawRewardPerSecond = data[3];
          const rawBalance = data[4];

          return {
            balance: wei.from(rawBalance),
            mined: wei.from(rawMined),
            rewardPerSecond: wei.from(rawRewardPerSecond),
            claimTimestamp: Number(rawUserClaimDate),
            sizeLimit: wei.from(rawSizeLimit),
            updatedAt: Date.now(),
            account: '',
          };
        } catch (e) {
          console.error(e);
          throw e;
        }
      }
      if (type === 'float') {
        const rawUser = data[0];
        const rawUserClaimDate = rawUser.claimTimestamp;
        const rawMined = data[1];
        const rawSizeLimit = data[2];
        const rawRewardPerSecond = data[3];
        const rawBalance = data[4];

        return {
          balance: wei.from(rawBalance),
          mined: wei.from(rawMined),
          rewardPerSecond: wei.from(rawRewardPerSecond),
          claimTimestamp: Number(rawUserClaimDate),
          sizeLimit: wei.from(rawSizeLimit),
          updatedAt: Date.now(),
          account: '',
        };
      }

      try {
        const rawUser = data[0];
        const rawUserAccount = rawUser.account;
        const rawUserClaimDate = rawUser.claimTimestamp;
        const rawUserSizeLevel = rawUser.sizeLevel;
        const rawUserSpeedLevel = rawUser.speedLevel;
        const rawUserRefLevel = rawUser.referralLevel;
        const rawUserRefStorage = rawUser.refStorage;
        const rawUserActivity = rawUser.activity;
        const rawUserParent = rawUser.parent;
        const rawMined = data[3];
        const rawSizeLimit = data[4];
        const rawRewardPerSecond = data[5];
        const rawRefLimit = data[6];
        const rawBalance = data[7];

        return {
          balance: wei.from(rawBalance),
          mined: wei.from(rawMined),
          rewardPerSecond: wei.from(rawRewardPerSecond),
          claimTimestamp: Number(rawUserClaimDate),
          sizeLimit: wei.from(rawSizeLimit),
          updatedAt: Date.now(),
          account: rawUserAccount,
          extra: {
            refLimit: wei.from(rawRefLimit),
            sizeLevel: Number(rawUserSizeLevel),
            speedLevel: Number(rawUserSpeedLevel),
            referralLevel: Number(rawUserRefLevel),
            refStorage: wei.from(rawUserRefStorage),
            activity: Number(rawUserActivity),
            parentId: Number(rawUserParent),
          },
        };
      } catch (e) {
        console.error(e);
        throw e;
      }

      return undefined;
    }
  }, [state.data]);

  return {
    ...state,
    data: formattedData,
  };
}
