import PXLs4ABI from 'const/ABI/PXLs';
import { useWeb3 } from 'services/web3Provider';

import useMigrationToV5 from '../../../hooks/useMigrationToV5';
import React, { useEffect } from 'react';
import { useAccount } from '@chain/hooks/useAccount.ts';
import toaster from 'services/toaster.tsx';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { IS_DEVELOP, ZERO_ADDRESS } from '@cfg/config.ts';
import {
  gasLessStore,
  loadMinerUserGasLess,
  updateGasLess,
} from 'lib/Mining/api-server/miner.store.ts';
import { useUnit } from 'effector-react';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { CONTRACT_ADDRESSES } from 'services/multichain/contracts';
import { wagmiConfig } from '@chain/wagmi.ts';
import { readContract, estimateGas } from '@wagmi/core';
import miningAnalytics from 'lib/Mining/api-analytics';
import { PAYABLE_ABI } from '../api-contract/abi/payable.abi.ts';
import { IS_PAYABLE } from '@cfg/app.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import ERC_20_ABI from 'lib/Wallet/api-contract/abi/erc-20.abi.ts';
import { MINER_ABI } from 'lib/Mining/api-contract/abi/miner.abi.ts';
import { useMiningStorage } from 'lib/Mining/hooks/useMiningStorage.ts';
import { useMiningAccess } from 'lib/Mining/hooks/useMiningAccess.ts';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

export function useMining({
  minerAddress,
  payableAddress,
  payablePrice,
  afterClaim,
  type,
}: {
  payableAddress: string;
  minerAddress: string;
  payablePrice?: number;
  afterClaim?: () => void;
  type?: 'root' | 'second' | 'float';
}) {
  const userInfo = useSelfUserInfo();
  const account = useAccount();
  const { apiClaim } = useWeb3() || {};

  // payable

  const writePaylable = useWriteContract({
    abi: PAYABLE_ABI,
    address: payableAddress,
  });
  const tradeToken = useTradeToken();
  const writeTradeToken = useWriteContract({
    abi: ERC_20_ABI,
    address: tradeToken.data?.address,
  });

  //

  const { migrateToV5, checkOldInventory } = useMigrationToV5();

  const gasLess = useUnit(gasLessStore);

  const writeCore = useWriteContract({
    abi: MINER_ABI,
    address: minerAddress,
  });

  const miningAddress = MINER_CONTRACT_ID[account.chainId]?.core;
  const miningAddressV4 = CONTRACT_ADDRESSES[account.chainId]?.pixel?.core;
  const isRootMiner = minerAddress === miningAddress;

  const [isClaiming, setIsClaiming] = React.useState(false);
  const [isRefClaiming, setIsRefClaiming] = React.useState(false);
  const [isMigrating, setIsMigrating] = React.useState(false);
  const [isAccessLoading, setIsAccessLoading] = React.useState(false);

  const fetchV4 = async () => {
    const result = await readContract(wagmiConfig, {
      abi: PXLs4ABI,
      address: miningAddressV4,
      functionName: 'getStorage',
      args: [userInfo.data.telegramId],
    });

    if (!result) {
      return null;
    }

    const rawUser = result[0];
    return {
      haveOldAccount: rawUser.account !== ZERO_ADDRESS,
    };
  };
  const mining = useMiningStorage({
    minerAddress: minerAddress,
    telegramId: userInfo.data?.telegramId,
    type,
  });

  const noAccess =
    mining.data?.account &&
    mining.data.account?.toLowerCase() !== account.accountAddress.toLowerCase();

  const migrate = async () => {
    console.log('[mining]', 'migrate');
    setIsMigrating(true);

    try {
      const v4 = await fetchV4();
      const v5 = mining.data;

      if (v5.account === ZERO_ADDRESS && v4 && v4.haveOldAccount) {
        // Migrate account from old contract
        await migrateToV5(false);
        await checkOldInventory();
        await mining.refetch();
      } else if (isRootMiner && v4 && v4.haveOldAccount) {
        // Check inventory in other cases
        await checkOldInventory();
      }

      localStorage.setItem('pixel-v5-migration', 'yes');
    } catch (e) {
      toaster.logError(e, '[onUpgrade]');
      console.error(e);
    }

    setIsMigrating(false);
  };

  const onClaim = async () => {
    miningAnalytics.tractClaim({
      chainId: account.chainId,
      minerAddress,
      userAddress: account.accountAddress,
    });

    setIsClaiming(true);
    Telegram.WebApp.HapticFeedback.impactOccurred('light');

    try {
      if (gasLess.amount && gasLess.use) {
        await apiClaim();
        await loadMinerUserGasLess({
          chainId: account.chainId,
          accountAddress: account.accountAddress,
        });
      } else {
        if (IS_PAYABLE && payableAddress) {
          if (!tradeToken.data?.isCoin) {
            await writeTradeToken.writeContractAsync({
              functionName: 'approve',
              args: [payableAddress, payablePrice],
            });
          }

          await writePaylable.writeContractAsync({
            functionName: 'claimReward',
            args: [userInfo.data?.telegramId],
            overrides:
              payablePrice && tradeToken.data?.isCoin ? { value: payablePrice } : undefined,
          });
        } else {
          await writeCore.writeContractAsync({
            functionName: 'claimReward',
            args: [userInfo.data?.telegramId],
          });
        }

        toaster.success('Tokens claimed');

        await mining.refetch();
        afterClaim?.();

        Telegram.WebApp.HapticFeedback.notificationOccurred('success');
      }
    } catch (error) {
      toaster.captureException(error);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }
    setIsClaiming(false);
  };

  const onClaimRef = async () => {
    setIsRefClaiming(true);
    Telegram.WebApp.HapticFeedback.impactOccurred('light');

    try {
      if (IS_PAYABLE && payableAddress) {
        if (!tradeToken.data?.isCoin) {
          await writeTradeToken.writeContractAsync({
            functionName: 'approve',
            args: [payableAddress, payablePrice],
          });
        }

        await writePaylable.writeContractAsync({
          functionName: 'claimReferral',
          args: [userInfo.data?.telegramId],
          overrides: payablePrice && tradeToken.data?.isCoin ? { value: payablePrice } : undefined,
        });
      } else {
        await writeCore.writeContractAsync({
          functionName: 'claimReferral',
          args: [userInfo.data?.telegramId],
        });
      }

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
      toaster.success('Referral Storage claimed');

      await mining.refetch();
      afterClaim?.();
    } catch (error) {
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
      toaster.captureException(error);
    }
    setIsRefClaiming(false);
  };

  useEffect(() => {
    if (account.isConnected && minerAddress) {
      if (!IS_DEVELOP) {
        loadMinerUserGasLess({ accountAddress: account.accountAddress, chainId: account.chainId })
          .then()
          .catch();
      }
    }
  }, [account.chainId, account.isConnected, minerAddress]);

  useEffect(() => {
    if (account.isConnected && minerAddress && mining.data && !noAccess) {
      if (isRootMiner) {
        if (localStorage.getItem('pixel-v5-migration') !== 'yes' && miningAddressV4) {
          migrate().then();
        }
      }
    }
  }, [
    account.chainId,
    account.isConnected,
    account.accountAddress,
    minerAddress,
    mining.data,
    miningAddressV4,
    isRootMiner,
    noAccess,
  ]);
  useMiningAccess();
  useEffect(() => {
    if (account.isConnected) {
      loadMinerUserGasLess({
        chainId: account.chainId,
        accountAddress: account.accountAddress,
      }).then();
    }
  }, [account.chainId]);

  return {
    isClaiming,
    isRefClaiming,
    isMigrating,
    isMiningLoading: mining.loading || (!mining.data && mining.isFetching),
    isMiningDisabled: !miningAddress,
    data: mining.data,
    loading: mining.loading,
    refresh: mining.refetch,
    claim: onClaim,
    claimRef: onClaimRef,
    updateGasLess: (v: boolean) => updateGasLess(v),
    gasLess: gasLess.amount,
    useGasLess: gasLess.use,
    // tx
    txHash: writePaylable.data || writeCore.data,
    // access
    noAccess,
    isAccessLoading,
  };
}
