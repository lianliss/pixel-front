export function getCurrentMined({
  mined,
  sizeLimit,
  rewardPerSecond,
  lastGetTimestamp,
}: {
  mined: number;
  sizeLimit: number;
  rewardPerSecond: number;
  lastGetTimestamp: number;
}): number {
  const minedVirtualSeconds = (Date.now() - lastGetTimestamp) / 1000;
  const minedVirtual = mined + minedVirtualSeconds * rewardPerSecond;

  const minedValue = minedVirtual > sizeLimit ? sizeLimit : minedVirtual;

  return +minedValue.toFixed(4) || 0;
}
