const UPGRADE_STORAGE_IMAGES = [
  require('assets/mining/referral/0.png') as string,
  require('assets/mining/referral/1.png') as string,
  require('assets/mining/referral/2.png') as string,
  require('assets/mining/referral/3.png') as string,
  require('assets/mining/referral/4.png') as string,
  require('assets/mining/referral/5.png') as string,
  require('assets/mining/referral/6.png') as string,
  require('assets/mining/referral/7.png') as string,
  require('assets/mining/referral/8.png') as string,
  require('assets/mining/referral/9.png') as string,
];

const UPGRADE_DRILL_IMAGES = [
  require('assets/mining/speed/0.jpg') as string,
  require('assets/mining/speed/1.jpg') as string,
  require('assets/mining/speed/2.jpg') as string,
  require('assets/mining/speed/3.jpg') as string,
  require('assets/mining/speed/4.jpg') as string,
  require('assets/mining/speed/5.jpg') as string,
  require('assets/mining/speed/6.jpg') as string,
  require('assets/mining/speed/7.jpg') as string,
  require('assets/mining/speed/8.jpg') as string,
];

const UPGRADE_REF_IMAGES = [
  require('assets/mining/size/0.png') as string,
  require('assets/mining/size/1.png') as string,
  require('assets/mining/size/2.png') as string,
  require('assets/mining/size/3.png') as string,
  require('assets/mining/size/4.png') as string,
  require('assets/mining/size/5.png') as string,
  require('assets/mining/size/6.png') as string,
  require('assets/mining/size/7.png') as string,
  require('assets/mining/size/8.png') as string,
  require('assets/mining/size/9.png') as string,
  require('assets/mining/size/10.png') as string,
];

const UPGRADE_DRONE_IMAGES = [
  require('assets/mining/size/0.png') as string,
  require('assets/mining/size/1.png') as string,
  require('assets/mining/size/2.png') as string,
  require('assets/mining/size/3.png') as string,
  require('assets/mining/size/4.png') as string,
  require('assets/mining/size/5.png') as string,
  require('assets/mining/size/6.png') as string,
  require('assets/mining/size/7.png') as string,
  require('assets/mining/size/8.png') as string,
  require('assets/mining/size/9.png') as string,
  require('assets/mining/size/10.png') as string,
];

export { UPGRADE_DRILL_IMAGES, UPGRADE_DRONE_IMAGES, UPGRADE_REF_IMAGES, UPGRADE_STORAGE_IMAGES };
