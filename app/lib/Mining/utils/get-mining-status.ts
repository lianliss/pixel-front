export function getMiningStatus({
  notReady,
  claimTimestamp,
  mined,
  sizeLimit,
  rewardPerSecond,
  lastGetTimestamp,
  access,
  accessText,
}: {
  notReady: boolean;
  claimTimestamp: number;
  mined: number;
  sizeLimit: number;
  rewardPerSecond: number;
  lastGetTimestamp: number;
  access?: boolean;
  accessText?: string;
}): string {
  if (access === false) {
    return accessText || 'No Access';
  }
  if (notReady) {
    return 'Calculating';
  }
  if (claimTimestamp == 0) {
    return 'Press to start mining';
  }

  if (mined === sizeLimit) {
    return 'Storage is full';
  }

  const minedVirtualSeconds = (Date.now() - lastGetTimestamp) / 1000;
  const minedVirtual = mined + minedVirtualSeconds * rewardPerSecond;

  const minedValue = minedVirtual > sizeLimit ? sizeLimit : minedVirtual;
  const isFull = minedValue === sizeLimit;

  if (isFull) {
    return 'Storage is full';
  }

  const spaceLeft = sizeLimit - minedVirtual;
  let secondsLeft = spaceLeft / rewardPerSecond;
  const hours = Math.floor(secondsLeft / 3600);

  secondsLeft %= 3600;

  const minutes = Math.floor(secondsLeft / 60);
  const seconds = Math.floor(secondsLeft % 60);

  return `${hours}h ${minutes}m ${seconds}s ${'to fill'}`;
}
