export function getLevelSpeed(level, mul = 1, add = 0, config) {
  return (level + 1) * config.baseSpeed * mul + add;
}

export function getLevelSize(level, mul = 1, add = 0, config) {
  return (level + 1) * config.baseSpeed * config.baseSize * mul + add;
}

export function getLevelRef(level, mul = 1, add = 0, config) {
  const curLevel = level + 1;
  return (
    (curLevel * config.baseSpeed * config.baseSize +
      2 +
      config.refPercent1 * curLevel +
      config.refPercent2 * curLevel) *
      mul +
    add
  );
}

const DRONE_DIFFICULTY = 25;
const DRONE_MINING_BASE = 0.01;

export function getDroneLevelSize(currentLevel: number) {
  const top1 = (currentLevel + 1) * DRONE_DIFFICULTY;
  const top2 = currentLevel * DRONE_MINING_BASE;

  return top1 * top2;
}
