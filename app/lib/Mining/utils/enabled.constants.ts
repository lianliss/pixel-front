import { Chain } from 'services/multichain/chains';
import { IS_DEVELOP, IS_STAGE } from '@cfg/config.ts';

export const MINING_TESTERS = {
  ['0x4B9Ac70305761c8f1c4e88AD6C54499b15674a07'.toLowerCase()]: true,
};

export const MINING_IN_TEST = {
  // [Chain.SKALE]: true,
};

export function isMiningEnabled(chainId: number, account: string): boolean {
  return (
    (IS_DEVELOP || IS_STAGE) &&
    (MINING_IN_TEST[chainId] ? MINING_TESTERS[account.toLowerCase()] : true)
  );
}
