import { DEFAULT_CHAIN, MINING_CONFIG } from 'services/multichain/chains';

const DEFAULT_CONFIG = MINING_CONFIG[DEFAULT_CHAIN];

export function getUpgradeSpeedPrice(level, mod = 1, config) {
  const nextLevel = level + 2;
  return nextLevel ** 2 * config.difficulty * config.baseSpeed * mod;
}

export function getUpgradeStoragePrice(level, mod = 1, config) {
  const nextLevel = level + 2;
  return nextLevel ** 2 * config.difficulty * config.baseSpeed * config.baseSize * mod;
}

export function getUpgradeRefPrice(level, mod = 1, config) {
  const nextLevel = level + 2;

  // return nextLevel * config.difficulty * (config.baseSize * config.baseSpeed * nextLevel) * (1 + config.refPercent2 * nextLevel)

  return (
    nextLevel ** 2 *
    config.difficulty *
    config.baseSpeed *
    config.baseSize *
    (1 + config.refPercent1 * nextLevel) *
    (1 + config.refPercent2 * nextLevel) *
    mod
  );
}
