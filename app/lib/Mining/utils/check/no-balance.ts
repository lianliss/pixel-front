import { usePayable } from 'lib/Mining/hooks/usePayable.ts';
import { useTradeBalance } from '@chain/hooks/useTradeBalance.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';

export function isNoTradeTokenBalance(
  payable: ReturnType<typeof usePayable>,
  trade: ReturnType<typeof useTradeBalance>
) {
  const result =
    payable.cost.isFetched &&
    trade.isFetched &&
    (!trade.data?.formatted || trade.data.formatted < payable.cost.data.formatted);

  return result || false;
}

export function isNoGasTokenBalance(
  payable: ReturnType<typeof usePayable>,
  gasBalance: ReturnType<typeof useTradeBalance>,
  trade: ReturnType<typeof useTradeToken>
) {
  const result =
    payable.cost.isFetched &&
    gasBalance.isFetched &&
    (!gasBalance.data?.formatted ||
      Boolean(trade.data?.isCoin && gasBalance.data.formatted < payable.cost.data.formatted));

  return result || false;
}
