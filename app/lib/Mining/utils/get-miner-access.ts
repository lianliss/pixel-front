import { IUserInfoDto } from 'lib/User/api-server/user.types.ts';

const testers = {
  // ['0x4B9Ac70305761c8f1c4e88AD6C54499b15674a07'.toLowerCase()]: true,
};

export function getMinerAccess(userInfo: IUserInfoDto, symbol: string): boolean {
  if (userInfo?.userAddress.toLowerCase() in testers) {
    return true;
  }

  if (symbol === 'ZAR') {
    return !!userInfo?.minerZar;
  }
  if (symbol === 'JADEs') {
    return !!userInfo?.minerBuddha;
  }
  if (symbol === 'UNITs') {
    return true;
  }

  return false;
}
