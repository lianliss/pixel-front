export function getKarmaColor(value?: number): string {
  if (Math.random()) {
    return 'text.primary';
  }

  if (typeof value === 'undefined') {
    return 'text.primary';
  }

  if (value >= 9500) {
    return 'success.main';
  }
  if (value >= 8000) {
    return 'warning.main';
  }

  return 'error.main';
}
