export function getMiningPercent({
  mined,
  sizeLimit,
  rewardPerSecond,
  lastGetTimestamp,
}: {
  mined: number;
  sizeLimit: number;
  rewardPerSecond: number;
  lastGetTimestamp: number;
}): number {
  const minedVirtualSeconds = (Date.now() - lastGetTimestamp) / 1000;
  const minedVirtual = mined + minedVirtualSeconds * rewardPerSecond;

  const minedValue = minedVirtual > sizeLimit ? sizeLimit : minedVirtual;
  const minedPercents = sizeLimit ? (minedValue / sizeLimit) * 100 : 0;

  return minedPercents;
}

export function getMiningRefPercent({
  refStorage,
  refLimit,
}: {
  refStorage: number;
  refLimit: number;
}): number {
  return (refStorage / refLimit) * 100;
}
