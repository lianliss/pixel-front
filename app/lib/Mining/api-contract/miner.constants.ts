import { Chain } from 'services/multichain/chains';
import { IS_MINER_ZAR } from '@cfg/app.ts';
import { CheckAccessType } from 'lib/User/api-server/user.types.ts';
import { zerGatesTheme } from '../../../shared/theme/zar.theme.ts';
import { buddaTheme } from '../../../shared/theme/buddha.theme.ts';
import { PAYABLE_CONTRACT_ID } from 'lib/Wallet/api-contract/wallet.contracts.ts';

export const MINER_CONTRACT_ID = {
  [Chain.SONGBIRD]: {
    core: '0x7B3D7cfEe6BD2B3042B8F04D90b137D9Ed06Ea74',
    second: ['0xf4e4697Cc38f73B334daE3D41cAcAC489eFF3439'],
  },
  [Chain.SWISSTEST]: {
    // core: '0x8831d1AD3FA11c2A9DBdC88235eAd2b58c9FCA93',
  },
  [Chain.SKALE_TEST]: {
    core: '0xF064c145bD903DFa7BEB0B79482263509Dc2F6d3',
  },
  // [Chain.FLARE]: {
  //   core: '0x033691A4Bef2A0e5638aFA98d527ec13D4BfDd4C',
  // },
  [Chain.COSTON2]: {
    core: '0xA40F6AF5A9b43E4E713A4e3f4d71C918933916a3',
  },
  [Chain.SKALE]: {
    core: '0x049eEE4028Fb4fF8591Ea921F15843Cd2d9e9f94',
    second: ['0xbedcCEbB051AD2b8ED8bBEF2C87499278Aeb0345'],
  },
  [Chain.UNITS]: {
    core: '0x10b641D9fa73f1564e8d52641aE72DEC5B13d7Bc'.toLowerCase(),
    second: ['0xF1f63cbed6396A69112333B725Cc55612ab136a1'],
  },
};

export type IMinerInfo = {
  title: string;
  symbol: string;
  icon?: string;
  themeConfig?: any;
  contractAddress?: string;
  migrationAddress?: string;
  payableAddress?: string;
  referral?: boolean;
  requirements?: {
    speedLevel: number;
  };
  disabled?: boolean;
  soon?: boolean;

  accessText?: string;
  accessHref?: string;
  accessType?: CheckAccessType;
  info?: {
    twitter?: string;
    telegram?: string;
    web?: string;
  };
  type: 'root' | 'second' | 'float';
};

export const MINER_INFO: Record<string, IMinerInfo> = {
  '0xf4e4697Cc38f73B334daE3D41cAcAC489eFF3439': {
    title: 'Buddha Storage',
    symbol: 'JADEs',
    icon: require('assets/img/token/sgb/buddha.png') as string,
    themeConfig: buddaTheme,
    contractAddress: '0xf4e4697Cc38f73B334daE3D41cAcAC489eFF3439',
    migrationAddress: '0xcc59224C9Fb08986398325c82f2BEF2130B0c0BD',
    payableAddress: '0x8B1BF252Ec089a5216F52F3489B43d8f29A2dBfa',
    requirements: {
      speedLevel: 9,
    },
    referral: false,
    accessText: 'Upgrade drill to level 10',
    accessType: CheckAccessType.Buddha,
    info: {
      telegram: 'https://t.me/jadebuddha',
      web: 'https://jadebuddha.io',
    },
    type: 'second',
  },
  '0xbedcCEbB051AD2b8ED8bBEF2C87499278Aeb0345': {
    title: 'ZarGates Storage',
    symbol: 'ZAR',
    themeConfig: zerGatesTheme,
    icon: require('assets/img/token/skale/second-1.svg') as string,
    payableAddress: '0xFe6A5A97dCa00cED5F111db2d1591A007B623FF2',
    contractAddress: '0xbedcCEbB051AD2b8ED8bBEF2C87499278Aeb0345',
    referral: false,
    disabled: !IS_MINER_ZAR,
    soon: !IS_MINER_ZAR,
    accessText: 'Complete ZarGates Quest',
    accessHref: 'https://t.me/ZARGatesBot/?start=1956003243',
    accessType: CheckAccessType.Zar,
    info: {
      telegram: 'https://t.me/ZarGatesChannel',
      twitter: 'https://x.com/ZARGATES',
    },
    type: 'float',
  },
  // '0xF1f63cbed6396A69112333B725Cc55612ab136a1': {
  //   title: 'UNIT0 Storage',
  //   symbol: 'UNITs',
  //   themeConfig: zerGatesTheme,
  //   icon: require('assets/img/token/unit/native.png') as string,
  //   payableAddress: '0xC99169B8654D9f18d41512695AAC73d9EC0834EE',
  //   contractAddress: '0xF1f63cbed6396A69112333B725Cc55612ab136a1',
  //   referral: false,
  //   // disabled: !IS_MINER_ZAR,
  //   // soon: !IS_MINER_ZAR,
  //   // accessText: 'Complete ZarGates Quest',
  //   // accessHref: 'https://t.me/ZARGatesBot/?start=1956003243',
  //   // accessType: CheckAccessType.Zar,
  //   //     info: {
  //   //   telegram: 'https://t.me/ZarGatesChannel',
  //   //       twitter: 'https://x.com/ZARGATES',
  //   // },
  //   type: 'float',
  // },
};
