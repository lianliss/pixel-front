import { createEffect, createEvent, createStore } from 'effector';
import { minerApi } from 'lib/Mining/api-server/miner.api.ts';
import {
  IAnalyticsResponse,
  IFindTopReferralsDto,
  IFindTopReferralsResponse,
  IGetMinerUserDto,
  IGetMinerUserResponse,
  IGetReferralsCountDto,
  IGetReferralsCountResponse,
  IGetReferralsRewardsDto,
  IGetReferralsRewardsResponse,
  ITopReferral,
} from 'lib/Mining/api-server/miner.types.ts';
import { AxiosError } from 'axios';
import wait from 'utils/wait';

export type IAnalyticsStoreState = {
  state: IAnalyticsResponse | undefined;
  loading: boolean;
  error: Error | null;
};

export const loadMinerStats = createEffect(async (payload: { chainId: number }) => {
  const res = await minerApi.getStats(payload);
  return res;
});

export const minerStatsStore = createStore<IAnalyticsStoreState>({
  loading: true,
  error: null,
  state: undefined as IAnalyticsResponse | undefined,
})
  .on(loadMinerStats.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadMinerStats.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: undefined,
  }))
  .on(loadMinerStats.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// miner user

export const loadMinerUser = createEffect(async (payload: IGetMinerUserDto) => {
  const res = await minerApi.getMinerUser(payload);
  return res;
});

export const clearMinerUser = createEvent();

export const minerUserStore = createStore({
  loading: true,
  error: null as Error | null,
  state: null as IGetMinerUserResponse | null,
})
  .on(clearMinerUser, () => ({ state: null, error: null, loading: false }))
  .on(loadMinerUser.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadMinerUser.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadMinerUser.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

export const loadMinerUserSwtr = createEffect(async (payload: IGetMinerUserDto) => {
  const res = await minerApi.getMinerUser(payload);
  return res;
});

export const minerUserSwtrStore = createStore({
  loading: true,
  error: null as Error | null,
  state: null as IGetMinerUserResponse | null,
})
  .on(loadMinerUserSwtr.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadMinerUserSwtr.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadMinerUserSwtr.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// gasless

export const loadMinerUserGasLess = createEffect(
  async (payload: { chainId: number; accountAddress: string }) => {
    const res = await minerApi.getGasLess(payload);
    return res;
  }
);

export const updateGasLess = createEvent<boolean>();

export const gasLessStore = createStore({
  use: false,
  amount: 0,
})
  .on(updateGasLess, (prev, next) => ({ ...prev, use: next }))
  .on(loadMinerUserGasLess.doneData, (prev, next) => ({ ...prev, amount: next }));

// referrals count

export const loadUserReferralsCount = createEffect(async (payload: IGetReferralsCountDto) => {
  const res = await minerApi.getReferralsCount(payload);
  // res.parentAddress = '0x4B9Ac70305761c8f1c4e88AD6C54499b15674a07';
  // res.parentAssignedAt = new Date().getTime() - 1000 * 60 * 60;
  return res;
});

export const userReferralsCountStore = createStore({
  loading: false,
  error: null as Error | null,
  state: {
    total: 0,
    lastWeekActive: 0,
    parentAddress: null,
    parentAssignedAt: null,
  } as IGetReferralsCountResponse,
})
  .on(loadUserReferralsCount.doneData, (_prev, next) => ({
    loading: false,
    error: null,
    state: next,
  }))
  .on(loadUserReferralsCount.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadUserReferralsCount.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// referrals rewards

export const loadUserReferralsRewards = createEffect(async (payload: IGetReferralsRewardsDto) => {
  const res = await minerApi.getReferralsRewards(payload);
  return res;
});

export const userReferralsRewardsStore = createStore({
  loading: false,
  error: null as Error | null,
  state: null as IGetReferralsRewardsResponse | null,
})
  .on(loadUserReferralsRewards.doneData, (_prev, next) => ({
    loading: false,
    error: null,
    state: next,
  }))
  .on(loadUserReferralsRewards.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadUserReferralsRewards.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// referrals top

export const loadUserTopReferrals = createEffect(async (payload: IFindTopReferralsDto) => {
  const res = await minerApi.findTopReferrals(payload);

  return res.rows;
});

export const userTopReferralsStore = createStore({
  loading: false,
  error: null as Error | null,
  state: [] as ITopReferral[],
  loaded: false,
})
  .on(loadUserTopReferrals.doneData, (_prev, next) => ({
    loading: false,
    error: null,
    state: next,
    loaded: true,
  }))
  .on(loadUserTopReferrals.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: [],
    loaded: false,
  }))
  .on(loadUserTopReferrals.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
    loaded: prev.loaded,
  }));
