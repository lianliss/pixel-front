// request

export type IGetMinerUserDto = {
  userAddress: string;
  contract: string;
};

export type IGetReferralsCountDto = { contract: string; address: string };

export type IGetReferralsRewardsDto = { contract: string; address: string };

export type IFindTopReferralsDto = { contract: string; address: string };

// response

export type IGetMinerUserResponse = {
  claimedTokens: number;
  claimsCount: number;
  burnedTokens: number;
  referralsCount: number;
  referralTokens: number;
  drillLevel: number;
  storageLevel: number;
  referralsLastWeekActive: number;
  balance: number;
};

export type IAnalyticsResponse = {
  lastDayTransactions: number;
  notMintedSupply: number;
  onlineUsers: number;
  totalBurned: number;
  totalClaimed: number;
  totalCreatedUsers: number;
  totalHolders: number;
  totalSupply: number;
  totalTransactions: number;
};

export type IGetReferralsRewardsResponse = {
  total: number;
  lastDay: number;
};

export type IGetReferralsCountResponse = {
  total: number;
  lastWeekActive: number;
  parentAddress: string | null;
  parentAssignedAt: number | null;
};

export type ITopReferral = { address: string; amount: number; name?: string; image?: string };
export type IFindTopReferralsResponse = {
  rows: ITopReferral[];
};
