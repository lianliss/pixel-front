import axios, { AxiosResponse } from 'axios';
import {
  IAnalyticsResponse,
  IFindTopReferralsDto,
  IFindTopReferralsResponse,
  IGetMinerUserDto,
  IGetMinerUserResponse,
  IGetReferralsCountDto,
  IGetReferralsCountResponse,
  IGetReferralsRewardsDto,
  IGetReferralsRewardsResponse,
} from 'lib/Mining/api-server/miner.types.ts';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';
import {
  pixelMessageKey,
  pixelSignKey,
  telegramAuthKey,
} from '../../../shared/const/localStorage.ts';
import { IDENTITY_TOKEN, IS_DEVELOP, IS_STAGE } from '@cfg/config.ts';

import { axiosInstance } from 'utils/libs/axios.ts';

export class MinerApi {
  url: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async getReferralsCount(payload: IGetReferralsCountDto): Promise<IGetReferralsCountResponse> {
    const res = await axiosInstance.get<
      { data: IGetReferralsCountResponse },
      AxiosResponse<{ data: IGetReferralsCountResponse }>
    >('/api/v1/ft/referrals/count', {
      baseURL: this.url,
      params: { address: payload.address, contract: payload.contract },
    });

    return res.data.data;
  }

  async getReferralsRewards(
    payload: IGetReferralsRewardsDto
  ): Promise<IGetReferralsRewardsResponse> {
    const res = await axiosInstance.get<
      { data: IGetReferralsRewardsResponse },
      AxiosResponse<{ data: IGetReferralsRewardsResponse }>
    >('/api/v1/ft/referrals/rewards', {
      baseURL: this.url,
      params: { address: payload.address, contract: payload.contract },
    });

    return res.data.data;
  }

  async findTopReferrals(payload: IFindTopReferralsDto): Promise<IFindTopReferralsResponse> {
    const res = await axiosInstance.get<
      { data: IFindTopReferralsResponse },
      AxiosResponse<{ data: IFindTopReferralsResponse }>
    >('/api/v1/ft/referrals/top', {
      baseURL: this.url,
      params: { address: payload.address, contract: payload.contract },
    });

    return res.data.data;
  }

  async getStats(payload: { chainId: number }): Promise<IAnalyticsResponse> {
    const res = await axiosInstance.get('/api/v1/ft/stats', {
      baseURL: this.url,
      params: { chainId: payload.chainId },
    });

    return res.data.data;
  }

  async getMinerUser(payload: IGetMinerUserDto): Promise<IGetMinerUserResponse> {
    const res = await axiosInstance.get<
      { data: IGetMinerUserResponse },
      AxiosResponse<{ data: IGetMinerUserResponse }>
    >('/api/v1/ft/user', {
      baseURL: this.url,
      params: { userAddress: payload.userAddress, contract: payload.contract },
    });

    return res.data.data;
  }

  async getGasLess(payload: { chainId: number; accountAddress: string }) {
    const res = await axiosInstance.get<{ gasless: number }, AxiosResponse<{ gasless: number }>>(
      '/mining/gasless',
      {
        baseURL: 'https://hellopixel.network/api',
        params: { chainId: payload.chainId },
      }
    );

    return res.data.gasless;
  }

  async getTelegramId() {
    const res = await axiosInstance.get<{ gasless: number }, AxiosResponse<{ gasless: number }>>(
      '/auth/id',
      {
        baseURL: 'https://hellopixel.network/api',
        params: {},
      }
    );

    return Number(res.data.id) || null;
  }
}

export const minerApi = new MinerApi({ url: 'https://api.hellopixel.network' });
