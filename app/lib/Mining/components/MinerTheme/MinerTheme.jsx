import React from 'react';
import styles from './MinerTheme.module.scss';

function MinerTheme({ theme, className, children }) {
  const classNames = [styles.theme];
  if (className) {
    classNames.push(className);
  }
  if (theme === 'green') {
    classNames.push(styles.green);
  }
  if (theme === 'yellow') {
    classNames.push(styles.yellow);
  }

  return <div className={classNames.join(' ')}>{children}</div>;
}

export default MinerTheme;
