import styles from './MinerInfo.module.scss';
import getFinePrice from 'utils/getFinePrice';
import React from 'react';
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

type IMinerInfoProps = {
  id?: string;
  balance: number;
  icon: string;
};

export function MinerInfo(props: IMinerInfoProps) {
  const { icon, balance, id } = props;

  return (
    <div id={id} className={styles.miningHeader}>
      <div className={styles.miningHeaderLogo}>
        {!!icon ? <img src={icon} alt={'icon'} /> : <QuestionMarkIcon />}
      </div>

      <Typography color='text.primary' fontWeight='bold' sx={{ fontSize: 40 }}>
        {getFinePrice(balance)}
      </Typography>
    </div>
  );
}
