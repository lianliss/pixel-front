import React from 'react';
import styles from './MinersSlider.module.scss';
import MiningStorage from 'lib/Mining/components/MiningStorage/MiningStorage.tsx';

import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper/modules';

import 'swiper/css';
import 'swiper/css/pagination';
import { useAb } from 'lib/ab/context/ab.context.ts';
import { IS_DEVELOP, IS_STAGE } from '@cfg/config.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import {
  IMinerInfo,
  MINER_CONTRACT_ID,
  MINER_INFO,
} from 'lib/Mining/api-contract/miner.constants.ts';
import { useToken } from '@chain/hooks/useToken.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';
import { useCoreToken } from '@chain/hooks/useCoreToken.ts';
import { useTranslation } from 'react-i18next';
import { usePayable } from 'lib/Mining/hooks/usePayable.ts';
import { IUserKarmaDto } from 'lib/User/api-server/user.types.ts';
import { PAYABLE_CONTRACT_ID } from 'lib/Wallet/api-contract/wallet.contracts.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { useUserInfo } from 'lib/User/hooks/useUserInfo.ts';
import { getMinerAccess } from 'lib/Mining/utils/get-miner-access.ts';
import { useFriendsCount } from 'lib/Friends/hooks/useFriends.ts';
import { isNoGasTokenBalance, isNoTradeTokenBalance } from 'lib/Mining/utils/check/no-balance.ts';
import { IS_DRONE } from '@cfg/app.ts';
import MiningDroneStorage from 'lib/MinerDrone/components/MinerDroneStorage/MiningDroneStorage.tsx';
import { MINER_DRONE_CONTRACT_ID } from 'lib/MinerDrone/api-contract/miner-drone.constants.ts';

type Props = {
  minerDisabled?: boolean;
  karma?: IUserKarmaDto;
};

function MinersSlider({ minerDisabled, karma }: Props) {
  const { t } = useTranslation('mining');
  const ab = useAb();
  const account = useAccount();
  const { chainId } = account;
  const userInfo = useUserInfo(account.accountAddress);

  const tradeToken = useTradeToken();
  const coreToken = useCoreToken();
  const gasToken = useToken({});
  const gasBalance = useBalance({
    address: account.accountAddress,
    skip: !account.isConnected,
  });
  const tradeBalance = useBalance({
    address: account.accountAddress,
    token: tradeToken.data?.address,
    skip: !account.isConnected || !tradeToken.data,
  });
  const payableInfo = usePayable();
  const friends = useFriendsCount();

  const payableAddress = PAYABLE_CONTRACT_ID[account.chainId];
  const coreMinerAddress = MINER_CONTRACT_ID[chainId]?.core;
  const secondMiners = MINER_CONTRACT_ID[chainId]?.second;
  const noGas = isNoGasTokenBalance(payableInfo, gasBalance, tradeToken);
  const noTradeToken = isNoTradeTokenBalance(payableInfo, tradeBalance);
  const isDrone = IS_DRONE && !!MINER_DRONE_CONTRACT_ID[account.chainId];

  const secondMinersFiltered: IMinerInfo[] = React.useMemo(() => {
    return (
      secondMiners
        ?.filter((minerAddress) => minerAddress in MINER_INFO)
        .map((minerAddress) => MINER_INFO[minerAddress])
        .filter((miner) => {
          if (miner.abKey && !IS_DEVELOP && !IS_STAGE) {
            return ab.isEnabled(miner.abKey);
          } else {
            return true;
          }
        }) || []
    );
  }, [chainId, ab.loaded]);

  return (
    <div className={styles.wrapper} id='mining'>
      <Swiper
        id='#ClaimStorageScreen'
        className={styles.miners}
        slidesPerView={1}
        pagination={{
          dynamicBullets: false,
        }}
        modules={[Pagination]}
      >
        <SwiperSlide>
          <MiningStorage
            symbol={coreToken.data?.symbol}
            symbolLoading={coreToken.isLoading}
            icon={coreToken.data?.logoURI}
            status
            title={t('Pixel Storage')}
            minerAddress={coreMinerAddress}
            payableAddress={payableAddress}
            referral={false}
            gasBalance={gasBalance.data?.formatted || 0}
            gasBalanceLoading={gasBalance.isLoading}
            gasBalanceReload={() => gasBalance.refetch()}
            gasSymbol={gasToken.data?.symbol}
            minerDisabled={minerDisabled}
            noGas={noGas}
            noTradeToken={noTradeToken}
            // payable
            txCost={payableInfo.cost.data}
            txCostSymbol={tradeToken.data?.symbol}
            txCostBot={payableInfo.costBot}
            txCostSlot={payableInfo.costBot}
            txCostExtra={payableInfo.costExtra}
            txCostLoading={payableInfo.cost.loading}
            txCostReload={() => {
              payableInfo.refetch();
            }}
            txCostAddress={tradeToken.data?.address}
            txCountLoading={payableInfo.count.loading}
            txCount={payableInfo.count.data}
            txRate={payableInfo.rate.data}
            txRateLoading={payableInfo.rate.loading}
            karma={karma}
            access
            tutorialKey='mining'
          />
        </SwiperSlide>

        <SwiperSlide>
          <MiningStorage
            symbol={coreToken.data?.symbol}
            symbolLoading={coreToken.isLoading}
            icon={coreToken.data?.logoURI}
            status
            title={t('Referral Storage')}
            minerAddress={coreMinerAddress}
            payableAddress={payableAddress}
            referral
            gasBalance={gasBalance.data?.formatted || 0}
            gasBalanceLoading={gasBalance.isLoading}
            gasBalanceReload={() => gasBalance.refetch()}
            gasSymbol={gasToken.data?.symbol}
            minerDisabled={minerDisabled}
            noGas={noGas}
            noTradeToken={noTradeToken}
            friendsCount={friends.state?.total}
            // payable
            txCost={payableInfo.cost.data}
            txCostSymbol={tradeToken.data?.symbol}
            txCostBot={payableInfo.costBot}
            txCostExtra={payableInfo.costExtra}
            txCostLoading={payableInfo.cost.loading}
            txCostReload={() => payableInfo.refetch()}
            txCostAddress={tradeToken.data?.address}
            txCountLoading={payableInfo.count.loading}
            txCount={payableInfo.count.data}
            txRate={payableInfo.rate.data}
            txRateLoading={payableInfo.rate.loading}
            karma={karma}
            access
          />
        </SwiperSlide>

        {isDrone && (
          <SwiperSlide>
            <MiningDroneStorage
              symbol={coreToken.data?.symbol}
              symbolLoading={coreToken.isLoading}
              icon={coreToken.data?.logoURI}
              minerAddress={coreMinerAddress}
              payableAddress={payableAddress}
              minerDisabled={minerDisabled}
              // gas
              gasBalance={gasBalance.data?.formatted || 0}
              gasBalanceLoading={gasBalance.isLoading}
              gasBalanceReload={() => gasBalance.refetch()}
              gasSymbol={gasToken.data?.symbol}
              noGas={noGas}
              noTradeToken={noTradeToken}
              // payable
              txCost={payableInfo.cost.data}
              txCostSymbol={tradeToken.data?.symbol}
              txCostBot={payableInfo.costBot}
              txCostSlot={payableInfo.costBot}
              txCostExtra={payableInfo.costExtra}
              txCostLoading={payableInfo.cost.loading}
              txCostReload={() => payableInfo.refetch()}
              txCostAddress={tradeToken.data?.address}
              txCountLoading={payableInfo.count.loading}
              txCount={payableInfo.count.data}
              txRate={payableInfo.rate.data}
              txRateLoading={payableInfo.rate.loading}
              // other
              karma={karma}
              access
              tutorialKey='mining'
            />
          </SwiperSlide>
        )}

        {!!secondMinersFiltered &&
          secondMinersFiltered.map((miner, index) => {
            return (
              <SwiperSlide key={index}>
                <MiningStorage
                  secondIndex={index}
                  status
                  minerAddress={miner.contractAddress}
                  minerType={miner.type}
                  payableAddress={miner.payableAddress}
                  requirements={miner.requirements}
                  theme={miner.themeConfig}
                  referral={miner.referral}
                  icon={miner.icon}
                  title={miner.title}
                  info={miner.info}
                  gasBalance={gasBalance.data?.formatted || 0}
                  gasSymbol={gasToken.data?.symbol}
                  gasBalanceReload={() => gasBalance.refetch()}
                  minerDisabled={minerDisabled}
                  noGas={noGas}
                  noTradeToken={noTradeToken}
                  symbol={miner.symbol}
                  disabled={miner.disabled}
                  soon={miner.soon}
                  accessText={miner.accessText}
                  accessHref={miner.accessHref}
                  accessType={miner.accessType}
                  accessLoading={userInfo.loading}
                  // payable
                  txCost={payableInfo.cost.data}
                  txCostSymbol={tradeToken.data?.symbol}
                  txCostLoading={payableInfo.cost.loading}
                  txCostReload={() => payableInfo.refetch()}
                  txCostAddress={tradeToken.data?.address}
                  txCountLoading={payableInfo.count.loading}
                  txCount={payableInfo.count.data}
                  txCostBot={payableInfo.costBot}
                  txCostExtra={payableInfo.costExtra}
                  txRate={payableInfo.rate.data}
                  txRateLoading={payableInfo.rate.loading}
                  access={getMinerAccess(userInfo.data, miner.symbol)}
                />
              </SwiperSlide>
            );
          })}
      </Swiper>
    </div>
  );
}

export default MinersSlider;
