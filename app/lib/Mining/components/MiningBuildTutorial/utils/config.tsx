import { ITutorialConfig } from '../../../../../widget/tutorial/config/config.tsx';

export const MINING_BUILD_TUTORIAL_CONFIG: ITutorialConfig = {
  steps: [
    {
      id: 1,
      description:
        'Вы можете увеличить добычу или склад, чтобы реже собирать ваши доходы, но в больше количестве. Для этого давайте перейдём к строительству',
      hintPosition: 'center',
      isFooter: true,
    },

    {
      id: 2,
      description: 'Здесь вы можете улучшить ваш склад, чтобы собирать доход больше, но реже',
      hintPosition: 'end',
      targetElement: 'mining-upgrade-storage',
    },

    {
      id: 3,
      description: 'Здесь можно улучшить дрель, чтобы увеличить ваш доход',
      hintPosition: 'end',
      targetElement: 'mining-upgrade-drill',
    },
    {
      id: 4,
      description: 'Здесь вы можете улучшить реферальный склад',
      hintPosition: 'start',
      targetElement: 'mining-upgrade-ref',
    },
    {
      id: 5,
      description: 'Здесь указана стоимость улучшения',
      hintPosition: 'end',
      targetElement: 'mining-upgrade-storage-price',
    },

    {
      id: 6,
      description: 'Здесь указан текущий уровень постройки',
      hintPosition: 'end',
      targetElement: 'mining-upgrade-storage-level',
    },
    {
      id: 7,
      description: 'Давайте улучшим постройку',
      hintPosition: 'end',
      targetElement: 'mining-upgrade-storage',
      targetClickElement: 'mining-upgrade-storage',
      isFooter: false,
    },
    {
      id: 8,
      description: 'Здесь указана ваша текущая вместимость склада',
      hintPosition: 'start',
      targetElement: 'mining-upgrade-storage-dialog-current',
    },
    {
      id: 9,
      description: 'Здесь указана вместимость, которая будет после улучшения склада',
      hintPosition: 'start',
      targetElement: 'mining-upgrade-storage-dialog-next',
    },
    {
      id: 10,
      description: 'Давайте улучшим ваш склад',
      hintPosition: 'start',
      targetElement: 'mining-upgrade-storage-dialog-btn',
      targetClickElement: 'mining-upgrade-storage-dialog-btn',
      isFooter: false,
    },
  ],
};
