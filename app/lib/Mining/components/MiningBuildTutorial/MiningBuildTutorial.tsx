import { Tutorial } from '../../../../widget/tutorial';
import { useTutorial } from '../../../../widget/tutorial/useTutorial.tsx';
import { Tutorials } from '../../../../widget/tutorial/config/config.tsx';
import React, { useEffect } from 'react';
import { TUTORIAL_MINING_BUILD_ENABLED } from '@cfg/app.ts';
import { MINING_BUILD_TUTORIAL_CONFIG } from 'lib/Mining/components/MiningBuildTutorial/utils/config.tsx';

export function MiningBuildTutorial(props: { start?: boolean }) {
  const { startTutorial, checkRunTutorial, tutorial, completeTutorial } = useTutorial();
  const [stepTutorial, setStepTutorial] = React.useState(1);

  useEffect(() => {
    if (props.start && TUTORIAL_MINING_BUILD_ENABLED) {
      const dataTutorial = checkRunTutorial(Tutorials.miningBuild);

      if (dataTutorial.isStart) {
        startTutorial(Tutorials.miningBuild, dataTutorial.data);
      }
    }
  }, [props.start]);

  return (
    <Tutorial
      value={tutorial}
      step={stepTutorial}
      onChangeStep={(v) => {
        setStepTutorial(v);
      }}
      onComplete={completeTutorial}
      config={MINING_BUILD_TUTORIAL_CONFIG}
    />
  );
}
