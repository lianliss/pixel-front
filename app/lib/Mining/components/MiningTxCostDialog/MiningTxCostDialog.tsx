import BottomDialog, { IBottomDialogProps } from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { IconFire } from '@ui-kit/icon/common/IconFire.tsx';
import React from 'react';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import { useTokenPrice } from '@chain/hooks/useTokenPrice.ts';
import { IS_DEVELOP, IS_STAGE, ZERO_ADDRESS } from '@cfg/config.ts';
import Skeleton from '@mui/material/Skeleton';
import { useTranslation } from 'react-i18next';
import Alert from '@mui/material/Alert';
import { IUserKarmaDto } from 'lib/User/api-server/user.types.ts';
import { getKarmaColor } from 'lib/Mining/utils/get-karma-color.ts';

const Row = styled(Box)(() => ({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
}));

type Props = Omit<IBottomDialogProps, 'children'> & {
  txCost?: number;
  txCostLoading?: boolean;
  txCostExtra?: number;
  txCostBot?: number;
  txCostSlot?: number;
  txCostAddress?: string;
  txRate?: number;
  txCount?: number;
  txRateLoading?: boolean;
  txCountLoading?: boolean;
  soon?: boolean;
  karma?: IUserKarmaDto;
};

function MiningTxCostDialog(props: Props) {
  const {
    txCost,
    onClose,
    txRate,
    txCostBot,
    txCostExtra,
    txCostSlot,
    txCount,
    txCostAddress,
    txCostLoading,
    txCountLoading,
    txRateLoading,
    soon,
    karma,
    ...other
  } = props;

  const karmaColor = getKarmaColor(karma?.value || 10_000);
  const isRedRate = txRate > 1;

  const { t } = useTranslation('mining');

  const tokenPrice = useTokenPrice({
    address: txCostAddress || ZERO_ADDRESS,
  });

  return (
    <BottomDialog
      onClose={onClose}
      title={t('The next Transaction cost')}
      footer={
        <Box sx={{ p: 2, pt: 0, pb: 4 }}>
          <Button variant='contained' color='primary' fullWidth size='large' onClick={onClose}>
            OK
          </Button>
        </Box>
      }
      {...other}
    >
      <Typography
        sx={{
          fontSize: 24,
          mt: 2,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          gap: 0.5,
        }}
        align='center'
        fontWeight='bold'
        color={karmaColor}
      >
        {txCostLoading ? (
          <Skeleton sx={{ width: 90 }} />
        ) : (
          <>
            <IconFire sx={{ fontSize: 20 }} />
            {soon ? 'Soon' : txCost?.toFixed(6)}
          </>
        )}
      </Typography>

      <Typography
        sx={{
          fontSize: 14,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          gap: 0.5,
        }}
        align='center'
        color={karmaColor}
      >
        {txCountLoading || typeof tokenPrice.data === 'undefined' ? (
          <Skeleton sx={{ width: 90 }} />
        ) : soon ? (
          'Soon'
        ) : (
          `$${(txCost * tokenPrice.data).toFixed(6)}`
        )}
      </Typography>

      <Typography
        variant='caption'
        color='text.secondary'
        component='p'
        align='center'
        sx={{ mt: 1.25 }}
      >
        {t(
          'With each new transaction, its value increases during the day, and the update occurs every day at 00:00 GMT(0)'
        )}
      </Typography>

      <Paper sx={{ bgcolor: 'background.back1', p: 2, mt: 2, mb: 2 }}>
        <Stack direction='column' gap={2} divider={<Divider />}>
          <Row>
            <Typography variant='subtitle2' color={karmaColor}>
              {t('Personal modificator')}
            </Typography>
            <Typography variant='subtitle2' fontWeight='bold' color={karmaColor}>
              {txRateLoading ? <Skeleton sx={{ width: 30 }} /> : `x${txRate?.toFixed(4) || 1}`}
            </Typography>
          </Row>
          <Row>
            <Typography variant='subtitle2' color='text.secondary'>
              {t('Today you’ve done transaction') + ':'}
            </Typography>
            <Typography variant='subtitle2' fontWeight='bold'>
              {txCountLoading ? <Skeleton sx={{ width: 30 }} /> : txCount}
            </Typography>
          </Row>

          <Row>
            <Typography variant='subtitle2' color='text.secondary'>
              {t('Slot Modificator') + ':'}
            </Typography>
            <Typography variant='subtitle2' fontWeight='bold'>
              {txCostLoading ? <Skeleton sx={{ width: 30 }} /> : `x${txCostSlot || 1}`}
            </Typography>
          </Row>

          {(IS_STAGE || IS_DEVELOP) && (
            <Row>
              <Typography variant='subtitle2' color='text.secondary'>
                {t('Karma') + ':'}
              </Typography>
              <Typography variant='subtitle2' fontWeight='bold' color={karmaColor}>
                {!karma ? <Skeleton sx={{ width: 30 }} /> : karma?.value}
              </Typography>
            </Row>
          )}
        </Stack>
      </Paper>

      {txCostBot !== 1 && (
        <Alert severity={txCostBot < 1 ? 'success' : 'warning'} variant='outlined'>
          Harvester bot {txCostBot < 1 ? 'discount' : 'markup'}: {txCostBot < 1 ? '-' : ''}
          {+((1 - txCostBot) * 100).toFixed(4)}%
        </Alert>
      )}
      {txCostExtra !== 1 && (
        <Alert
          severity={txCostExtra < 1 ? 'success' : txCostExtra > 1.05 ? 'error' : 'warning'}
          variant='outlined'
          sx={{ mt: 1 }}
        >
          User modificator {txCostExtra < 1 ? 'discount' : 'markup'}: {txCostExtra < 1 ? '-' : '+'}
          {+(Math.abs(1 - txCostExtra) * 100).toFixed(4)}%
        </Alert>
      )}
    </BottomDialog>
  );
}

export default MiningTxCostDialog;
