import styles from './MiningNavigation.module.scss';
import routes from 'const/routes.tsx';
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useHaptic } from 'shared/telegram/useHaptic';
import { $sidebarState } from 'app/widget/Sidebar/sidebar.store';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import { Typography } from '@ui-kit/Typography/Typography';
import Divider from '@mui/material/Divider';
import Stack from '@mui/material/Stack';

const StyledRoot = styled(Paper)(({ theme }) => ({
  // borderTopLeftRadius: 16,
  // borderTopRightRadius: 16,
  borderBottom: 'bone',
  borderLeft: 'none',
  borderRight: 'none',
}));

export function MiningNavigation() {
  const { t } = useTranslation('mining');
  const navigate = useNavigate();

  const [state, setState] = React.useState($sidebarState.getState());
  const { openState: open, mode } = state;
  let collapsed = mode === 'collapsed';
  let hidden = mode === 'hidden';

  const onNavigate = async (route) => {
    Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
    navigate(route.path);
  };

  useEffect(() => {
    const unsubscribe = $sidebarState.watch(setState);
    return () => unsubscribe();
  }, []);

  return (
    <StyledRoot
      variant='outlined'
      square
      className={styles.root}
      id='miningButtons_id'
      style={{ right: collapsed ? 54 : 0 }}
    >
      <Stack
        direction='row'
        divider={<Divider flexItem orientation='vertical' />}
        id='miningButtons_list_id'
        className={styles.miningButtons}
      >
        <div
          className={[styles.miningButton].join(' ')}
          onClick={() => onNavigate(routes.walletQuests)}
        >
          <div className={styles.miningButtonIcon}>
            <img src={require('assets/mining/mining1.png')} alt={''} />
          </div>
          <Typography
            fontWeight='bold'
            color='text.primary'
            variant='caption'
            className={styles.miningButtonText}
          >
            {t('Quests')}
          </Typography>
        </div>

        <div
          id='targetMiningButton'
          className={[styles.miningButton].join(' ')}
          onClick={() => {
            onNavigate(routes.walletBuild);
          }}
        >
          <div className={styles.miningButtonIcon}>
            <img src={require('assets/mining/mining2.png')} alt={''} />
          </div>
          <Typography
            fontWeight='bold'
            color='text.primary'
            variant='caption'
            className={styles.miningButtonText}
          >
            {t('Build')}
          </Typography>
        </div>

        <div
          className={[styles.miningButton].join(' ')}
          onClick={() => onNavigate(routes.walletFriends)}
        >
          <div className={styles.miningButtonIcon}>
            <img src={require('assets/mining/mining3.png')} alt={''} />
          </div>
          <Typography
            fontWeight='bold'
            color='text.primary'
            variant='caption'
            className={styles.miningButtonText}
          >
            {t('Friends')}
          </Typography>
        </div>

        <div
          className={[styles.miningButton, ''].join(' ')}
          onClick={() => onNavigate(routes.inventory)}
        >
          <div className={styles.miningButtonIcon}>
            <img src={require('assets/mining/mining4.png')} alt={''} />
          </div>
          <Typography
            fontWeight='bold'
            color='text.primary'
            variant='caption'
            className={styles.miningButtonText}
          >
            {t('Inventory')}
          </Typography>
        </div>
      </Stack>
    </StyledRoot>
  );
}
