import React from 'react';
import Box, { BoxProps } from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import MinerRateParam from 'lib/Forge/components/MinerRateParam/MinerRateParam.tsx';
import { styled } from '@mui/material/styles';
import Divider from '@mui/material/Divider';
import { IconFire } from '@ui-kit/icon/common/IconFire.tsx';
import ToggleArrows from '../../../../shared/icons/ToggleArrows.tsx';
import MinerSpeedIcon from '../../../../shared/icons/MinerSpeed.tsx';
import { useDialog } from 'utils/hooks/useDialog.ts';
import MiningTxCostDialog from 'lib/Mining/components/MiningTxCostDialog/MiningTxCostDialog.tsx';
import { useTranslation } from 'react-i18next';
import { IS_PAYABLE } from '@cfg/app.ts';
import { IUserKarmaDto } from 'lib/User/api-server/user.types.ts';
import { getKarmaColor } from 'lib/Mining/utils/get-karma-color.ts';
import { IS_DEVELOP, IS_STAGE } from '@cfg/config.ts';

const Root = styled(Box)(() => ({
  // width: '100%',
}));

type Props = BoxProps & {
  // gas
  gasBalance?: number;
  gasBalanceLoading?: boolean;
  gasSymbol?: string;
  // miner
  minerSymbol?: string;
  minerSymbolLoading?: boolean;
  minerSpeed?: number;
  minerSpeedLoading?: boolean;
  minerSpeedIcon?: React.ReactNode;
  // tx cost
  txCost?: number;
  txCostSymbol?: string;
  txCostLoading?: boolean;
  txCostExtra?: number;
  txCostBot?: number;
  txCostSlot?: number;
  txRate?: number;
  txRateLoading?: boolean;
  txCount?: number;
  txCountLoading?: boolean;
  // gasless
  onToggleGasless?: () => void;
  gasless?: number;
  useGasless?: boolean;
  // karma
  karma?: IUserKarmaDto;
  tutorialKey?: string;
};

function MiningRateBlock(props: Props) {
  const {
    gasBalance,
    gasBalanceLoading = false,
    gasSymbol,
    minerSymbol,
    minerSpeed,
    txCost,
    txCostLoading = false,
    txCostBot,
    txCostExtra,
    txCostSlot,
    minerSymbolLoading = false,
    minerSpeedLoading = false,
    onToggleGasless,
    useGasless = false,
    gasless,
    txRate,
    txRateLoading,
    txCount,
    txCountLoading,
    txRateEnabled = false,
    karma,
    txCostSymbol,
    txCostAddress,
    tutorialKey,
    minerSpeedIcon,
    ...other
  } = props;

  const { t } = useTranslation('mining');
  const dialog = useDialog();

  const txCostColor = getKarmaColor(karma?.value);

  return (
    <Root {...other}>
      <Stack
        direction='row'
        gap={0.5}
        sx={{ width: '100%', justifyContent: 'space-between' }}
        divider={<Divider flexItem orientation='vertical' sx={{ my: 1 }} />}
      >
        <MinerRateParam
          id={`${tutorialKey}-tx`}
          color={txCostColor}
          title={t('Next txn cost') + ':'}
          subTitle={txCostSymbol || gasSymbol}
          value={typeof txCost === 'number' && IS_PAYABLE ? txCost?.toFixed(4) : `Soon`}
          startIcon={<IconFire />}
          loadingValue={txCostLoading}
          onInfo={typeof txCost === 'number' ? () => dialog.show() : undefined}
        />
        <MinerRateParam
          id={`${tutorialKey}-speed`}
          title={t('Mining speed') + ':'}
          subTitle={`${minerSymbol || '?'}/${t('hour')}`}
          value={minerSpeed?.toFixed(4) || '0'}
          startIcon={minerSpeedIcon || <MinerSpeedIcon color='primary' />}
          loadingValue={minerSpeedLoading}
          loadingSubtitle={minerSymbolLoading}
        />
        <MinerRateParam
          id={`${tutorialKey}-gas`}
          title={t('GAS') + ':'}
          subTitle={useGasless ? 'Claims' : gasSymbol}
          value={useGasless ? gasless.toString() : gasBalance?.toFixed(2) || '0'}
          startIcon={<IconFire color='primary' />}
          endIcon={
            !!(onToggleGasless && gasless) && (
              <ToggleArrows
                sx={{ fontSize: 16, color: 'text.secondary' }}
                onClick={onToggleGasless}
              />
            )
          }
          loadingValue={gasBalanceLoading}
        />
      </Stack>

      <MiningTxCostDialog
        open={dialog.open}
        onClose={dialog.onClose}
        txCost={txCost}
        txCostBot={txCostBot}
        txCostExtra={txCostExtra}
        txCostSlot={txCostSlot}
        txRate={txRate}
        txCount={txCount}
        txCountLoading={txCountLoading}
        txCostLoading={txCostLoading}
        txRateLoading={txRateLoading}
        soon={!IS_PAYABLE}
        karma={karma}
        txCostAddress={txCostAddress}
      />
    </Root>
  );
}

export default MiningRateBlock;
