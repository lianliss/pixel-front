import React from 'react';
import styles from './MiningProgress.module.scss';
import getFinePrice from 'utils/getFinePrice';
import useMiningTicker from 'lib/Mining/components/MiningStorage/utils/useMiningTicker';
import useStatus from 'lib/Mining/components/MiningStorage/utils/useStatus';
import useRequirements from 'lib/Mining/components/MiningStorage/utils/useRequirements.tsx';
import { ReactSVG } from 'react-svg';
import { Icon, Tooltip } from '@blueprintjs/core';
import { styled } from '@mui/material/styles';
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import { ProgressBar } from '@ui-kit/ProgressBar/ProgressBar.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

const StyledIconBox = styled('div')(({ theme }) => ({
  border: `solid 1px ${theme.palette.primary.main}`,
}));

type IMiningProgressProps = {
  symbol?: string;
  icon?: string;
  title?: string;
  status?: boolean;
  referral?: boolean;

  requirementsData?: any;
  rewardPerSecond?: number;
  refStorage?: number;
  refLimit?: number;
  isMiningDisabled?: boolean;
  sizeLimit?: number;
  lastGetTimestamp?: number;
  isClaiming?: boolean;
  isRefClaiming?: boolean;
  isMiningLoading?: boolean;
  claimTimestamp?: number;
  mined?: number;
  sizeLevel?: number;
  speedLevel?: number;
};

function MiningProgress({
  symbol,
  icon,
  title,
  status,
  // secondIndex,
  referral,
  // minerAddress,
  requirementsData,
  rewardPerSecond,
  refStorage,
  refLimit,
  isMiningDisabled,
  sizeLimit,
  lastGetTimestamp,
  isClaiming,
  isRefClaiming,
  isMiningLoading,
  claimTimestamp,
  mined,
  sizeLevel,
  speedLevel,
  disabled,
}: IMiningProgressProps) {
  const limit = referral ? refLimit : sizeLimit;
  const { minedValue, minedPercents } = useMiningTicker({
    start: referral ? undefined : lastGetTimestamp,
    mined: referral ? refStorage : mined,
    limit,
    rewardPerSecond,
    claimTimestamp,
  });
  const requirements = useRequirements({ speedLevel, sizeLevel }, requirementsData);

  const notReady = !isMiningDisabled && (isClaiming || isRefClaiming || isMiningLoading);
  const statusText = useStatus({
    notReady,
    requirements,
    limit,
    mined: minedValue,
    rewardPerSecond,
    claimTimestamp,
  });

  const progressClasses = [referral ? styles.storageReferralProgress : styles.storageBarProgress];
  const iconClasses = [styles.storageBarIcon];
  if (requirements) {
    progressClasses.push(styles.disabled);
    iconClasses.push(styles.disabled);
  }

  return (
    <div className={styles.storage}>
      <div className={styles.storageText}>
        <div className={styles.storageTextTitle}>{title}</div>
        <div className={styles.storageTextStatus}>
          <Typography
            color='text.secondary'
            variant='caption'
            sx={{ fontSize: 11 }}
            className={styles.storageTextStatusCounts}
          >
            {getFinePrice(minedValue)} / {getFinePrice(limit)} {symbol}
          </Typography>
          {status && (
            <Typography
              color='text.secondary'
              variant='caption'
              sx={{ opacity: 0.5, fontSize: 10 }}
            >
              {requirements ? <>&nbsp;</> : statusText}
            </Typography>
          )}
        </div>
      </div>
      <div className={styles.storageBarWrap}>
        {(!!icon || disabled) && (
          <StyledIconBox className={iconClasses.join(' ')}>
            {icon ? <img src={icon} alt={symbol} /> : <QuestionMarkIcon />}
            {!!requirements && <ReactSVG src={require('assets/svg/lock.svg')} />}
          </StyledIconBox>
        )}
        <div className={referral ? styles.storageReferral : styles.storageBar}>
          <ProgressBar
            value={minedPercents}
            size='medium'
            total={100}
            // color={false ? 'success' : 'primary'}
          />
        </div>
        {!!requirements && (
          <Tooltip content={requirements}>
            <Icon icon={'small-info-sign'} />
          </Tooltip>
        )}
      </div>
    </div>
  );
}

export default MiningProgress;
