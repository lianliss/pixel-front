import { classNames } from 'shared/lib/classNames.ts';
import styles from './FlareMiner.module.scss';
import ChainSwitcher from 'app/widget/ChainSwitcher/ChainSwitcher.tsx';
import getFinePrice from 'utils/getFinePrice';
import WalletBlock from 'ui/WalletBlock/WalletBlock';
import Button from 'ui/deprecated/ButtonDeprecated';
import { Chain } from 'services/multichain/chains';
import React from 'react';

import { useCoreToken } from '@chain/hooks/useCoreToken.ts';
import { useSwitchChain } from '@chain/hooks/useSwitchChain';
import { useTranslation } from 'react-i18next';

export function FlareMiner() {
  const { t } = useTranslation('mining');

  const { switchChain } = useSwitchChain();
  const coreToken = useCoreToken();

  // React.useEffect(() => {
  //   getDevicesPublic().then((dev) => {
  //     console.log('getDevicesPublic', dev);
  //   });
  // }, []);

  return (
    <div className={classNames(styles.mining, {}, ['scrollable-element'])}>
      <div className={styles.miningHeader}>
        <div className={styles.miningHeaderLogo}>
          <img
            src={coreToken.data?.logoURI || require('../../../../../styles/svg/logo_icon.svg')}
            alt={''}
          />
          <ChainSwitcher />
        </div>
        <div className={styles.miningHeaderBalance}>{getFinePrice(0)}</div>
        <div className={styles.miningHeaderMined}>
          <span className={styles.miningHeaderMinedStorage}>
            {getFinePrice(0)} {coreToken.data?.symbol || '-'} / {t('hour')}
          </span>
        </div>
      </div>
      <WalletBlock frame fontWeight='bold'>
        <div className={styles.miningCountdownTitle}>{t('Mining is not available in Flare')}</div>
        <div className={styles.miningCountdownActions}>
          <Button
            large
            icon={'backlink'}
            onClick={() => {
              switchChain({ chainId: Chain.SONGBIRD });
            }}
          >
            {t('Switch to Songbird')}
          </Button>
        </div>
      </WalletBlock>
    </div>
  );
}
