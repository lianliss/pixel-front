import BottomDialog, { IBottomDialogProps } from '@ui-kit/BottomDialog/BottomDialog.tsx';
import styles from './UpgradeMinerDialog.module.scss';
import WalletBlock from 'ui/WalletBlock/WalletBlock';
import { Icon } from '@blueprintjs/core';
import getFinePrice from 'utils/getFinePrice';
import React from 'react';
import Box from '@mui/material/Box';
import { Button } from '@ui-kit/Button/Button.tsx';
import Paper from '@mui/material/Paper';
import { alpha, styled } from '@mui/material/styles';
import clsx from 'clsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

const StyledRow = styled(Paper)(({ theme }) => ({
  border: `solid 1px ${alpha(theme.palette.primary.main, 0.6)}`,

  '&.active': {
    boxShadow: `0 9px 24px 0 ${alpha(theme.palette.primary.main, 0.4)}`,
  },
}));

type Props = Omit<IBottomDialogProps, 'children'> & {
  current?: { level: number; image: string; info: React.ReactNode };
  next?: { level: number; image: string; info: React.ReactNode };
  price?: number;
  priceIcon?: string;
  onUpgrade?: () => void;
  disabledBtnText?: string;
  btnText?: string;
  loading?: boolean;
  tutorialKey?: string;
  upgradeSymbol?: string;
};

export function UpgradeMinerDialog(props: Props) {
  const {
    current,
    next,
    price,
    priceIcon,
    onUpgrade,
    disabled = false,
    loading = false,
    disabledBtnText,
    tutorialKey,
    upgradeSymbol,
    ...other
  } = props;

  return (
    <BottomDialog
      keepMounted={!!tutorialKey}
      {...other}
      footer={
        <Box sx={{ px: 2, width: '100%', pb: 6 }}>
          <Button
            variant='contained'
            color='primary'
            size='large'
            fullWidth
            disabled={!onUpgrade || disabled}
            loading={loading}
            onClick={onUpgrade}
            id={`${tutorialKey}-btn`}
          >
            {disabled ? disabledBtnText || 'Upgrade' : 'Upgrade'}
          </Button>
        </Box>
      }
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          width: '100%',
          mt: 2,
        }}
      >
        <div className={styles.upgradeTokens}>
          <StyledRow
            variant='outlined'
            className={clsx(styles.upgradeToken, 'active')}
            sx={{ mb: 2 }}
            id={`${tutorialKey}-next`}
          >
            <div className={styles.upgradeTokenInfo}>
              <div className={styles.upgradeTokenInfoIcon}>
                <img src={next?.image} alt={''} />
              </div>
              <div className={styles.upgradeTokenInfoTitle}>
                <div className={styles.upgradeTokenInfoTitleLevel}>{next?.level} level</div>
                <div className={styles.upgradeTokenInfoTitleText}>
                  {next?.info}
                  {/*<span className={styles.upgradeTokenInfoTitleText_bonus}>{text2(levelIndex)}</span>*/}
                </div>
              </div>
            </div>
          </StyledRow>

          <Icon icon={'double-chevron-up'} />

          <StyledRow
            variant='outlined'
            className={styles.upgradeToken}
            sx={{ mt: 2 }}
            id={`${tutorialKey}-current`}
          >
            <div className={styles.upgradeTokenInfo}>
              <div className={styles.upgradeTokenInfoIcon}>
                <img src={current?.image} alt={''} />
              </div>
              <div className={styles.upgradeTokenInfoTitle}>
                <div className={styles.upgradeTokenInfoTitleLevel}>{current?.level} level</div>
                <div className={styles.upgradeTokenInfoTitleText}>{current?.info}</div>
              </div>
            </div>
          </StyledRow>
        </div>

        <div className={styles.upgradeCost}>
          <img src={priceIcon} alt='' /> {getFinePrice(price)}
          <Typography color='text.primary' variant='subtitle1' fontWeight='bold'>
            {upgradeSymbol}
          </Typography>
        </div>
      </Box>
    </BottomDialog>
  );
}
