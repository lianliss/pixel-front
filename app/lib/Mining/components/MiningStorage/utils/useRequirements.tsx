import { IMinerRequirements } from 'services/multichain/miners/miners.types.ts';

function useRequirements(
  { speedLevel, sizeLevel }: { speedLevel: number; sizeLevel: number },
  requirements: IMinerRequirements | undefined
): string | undefined {
  let requirementsText: string | undefined;

  const isDrillRequired = !!requirements?.speedLevel && speedLevel < requirements.speedLevel;
  const isSizeRequired = !!requirements?.sizeLevel && sizeLevel < requirements.sizeLevel;

  if (isDrillRequired && isSizeRequired) {
    requirementsText = `Upgrade your drill to level ${
      requirements.speedLevel + 1
    } and storage to level ${requirements.sizeLevel + 1} to claim`;
  } else if (isDrillRequired) {
    requirementsText = `Upgrade your drill to level ${requirements.speedLevel + 1} to claim`;
  } else if (isSizeRequired) {
    requirementsText = `Upgrade your storage to level ${requirements.sizeLevel + 1} to claim`;
  }
  return requirementsText;
}

export default useRequirements;
