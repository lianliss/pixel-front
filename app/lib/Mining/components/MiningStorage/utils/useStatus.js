import { useTranslation } from 'react-i18next';

function useStatus({ notReady, requirements, limit, mined, rewardPerSecond, claimTimestamp }) {
  const { t } = useTranslation('wallet');

  const spaceLeft = limit - mined;
  let secondsLeft = spaceLeft / rewardPerSecond;
  const hours = Math.floor(secondsLeft / 3600);
  secondsLeft %= 3600;
  const minutes = Math.floor(secondsLeft / 60);
  const seconds = Math.floor(secondsLeft % 60);
  const isFull = mined === limit;

  if (notReady) {
    return t('Calculating');
  }
  if (requirements) {
    return requirements;
  }
  if (claimTimestamp == 0) {
    return t('Press to start mining');
  }
  if (isFull) {
    return t('Storage is full');
  }
  return `${hours}h ${minutes}m ${seconds}s ${t('to fill')}`;
}

export default useStatus;
