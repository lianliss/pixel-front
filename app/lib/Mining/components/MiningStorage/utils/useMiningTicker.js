import React from 'react';

const DEFAULT_REWARD_PER_SECOND = 0.00001;

function useMiningTicker({ start, mined, limit, rewardPerSecond, claimTimestamp }) {
  const [tickInterval, setTickInterval] = React.useState();
  const [minedVirtual, setMinedVirtual] = React.useState(mined);

  const increaseMined = () => {
    const seconds = (Date.now() - start) / 1000;
    setMinedVirtual(mined + seconds * rewardPerSecond);
  };

  React.useEffect(() => {
    if (!limit || !start) {
      setMinedVirtual(mined);
      return;
    }
    clearInterval(tickInterval);
    if (claimTimestamp) {
      setTickInterval(setInterval(increaseMined, 1000));
    }
    setMinedVirtual(mined);

    return () => {
      clearInterval(tickInterval);
    };
  }, [limit, mined, rewardPerSecond, start, claimTimestamp]);

  const minedValue = minedVirtual > limit ? limit : minedVirtual;
  const minedPercents = limit ? (minedValue / limit) * 100 : 0;
  const isFull = minedValue === limit;

  return {
    minedValue,
    minedPercents,
    isFull,
  };
}

export default useMiningTicker;
