import Paper, { PaperProps } from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import Box from '@mui/material/Box';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import React from 'react';
import Skeleton from '@mui/material/Skeleton';

const StyledRoot = styled(Paper)(({ theme }) => ({
  padding: '8px',
  paddingRight: 12,
  // backgroundColor: theme.palette.background.back3,

  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'flex-end',
}));
const StyledAvatar = styled(Avatar)(({ theme }) => ({
  width: 56,
  height: 56,
  padding: 8,
  boxSizing: 'border-box',

  '& > img': {
    objectFit: 'contain',
  },
  '& > .MuiSvgIcon-root': {
    fontSize: 40,
  },
}));

type Props = {
  title?: string;
  amount?: React.ReactNode;
  maxAmount?: number;
  fixedAmount?: boolean;
  image?: string;
  icon?: React.ReactNode;
  symbol?: string;
  isBadKarma?: boolean;
  status?: React.ReactNode;
  miningLoading?: boolean;
  miningClaiming?: boolean;
  tutorialKey?: string;
} & PaperProps;

export function MiningStorageToken(props: Props) {
  const {
    loading = false,
    isBadKarma = false,
    fixedAmount = false,
    title,
    icon,
    amount = 0,
    maxAmount = 0,
    image,
    symbol,
    status,
    tutorialKey,
    ...other
  } = props;

  return (
    <StyledRoot elevation={0} {...other}>
      <Box sx={{ display: 'flex', gap: 1, alignItems: 'center' }}>
        <StyledAvatar variant='rounded' variantStyle='outlined' color={'primary'} src={image}>
          {icon}
        </StyledAvatar>

        <Box>
          <Typography variant='subtitle1' color='text.primary' fontWeight='bold'>
            {title}
          </Typography>
          <Typography variant='caption' sx={{ display: 'flex', gap: '2px' }}>
            <Typography
              variant='inherit'
              component='span'
              id={`${tutorialKey}-miner-balance`}
              sx={{ border: 'solid 2px transparent' }}
            >
              {loading ? <Skeleton component='span' sx={{ width: 40 }} /> : amount || 0}{' '}
              {fixedAmount ? symbol : ''}
            </Typography>
            {!fixedAmount && (
              <Typography
                variant='inherit'
                component='span'
                color='text.secondary'
                sx={{ display: 'flex', gap: '2px', border: 'solid 2px transparent' }}
                id={`${tutorialKey}-miner-storage`}
              >
                /{' '}
                {loading ? (
                  <Skeleton component='span' sx={{ width: 40 }} />
                ) : (
                  `${+maxAmount?.toFixed(4) || 0} ${symbol}`
                )}
              </Typography>
            )}
          </Typography>
        </Box>
      </Box>

      {status && (
        <Typography
          color='text.secondary'
          variant='caption'
          sx={{ mb: 0.5, maxWidth: 120, textAlign: 'right' }}
          id={`${tutorialKey}-miner-status`}
        >
          {status}
        </Typography>
      )}
    </StyledRoot>
  );
}
