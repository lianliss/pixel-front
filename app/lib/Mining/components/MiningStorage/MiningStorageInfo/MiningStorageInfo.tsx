import React, { useEffect, useState } from 'react';
import Box, { BoxProps } from '@mui/material/Box';
import { useTranslation } from 'react-i18next';
import { Button } from '@ui-kit/Button/Button.tsx';
import { styled } from '@mui/material/styles';
import { IconButton } from '@ui-kit/IconButton/IconButton.tsx';
import Telegram from '@mui/icons-material/Telegram';
import Twitter from '@mui/icons-material/Twitter';
import Web from '@mui/icons-material/Web';
import { openLink } from 'utils/open-link.ts';
import { userCheckAccess } from 'lib/User/api-server/user.store.ts';
import { CheckAccessType } from 'lib/User/api-server/user.types.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUserAccessApi } from 'lib/User/hooks/useUserAccessApi.ts';
import toaster from 'services/toaster.tsx';
import { storage } from 'utils/storage.ts';

const StyledRoot = styled(Box)(() => ({
  display: 'flex',
  gap: '8px',
}));
const StyledBtn = styled(IconButton)(({ theme }) => ({
  border: `solid 1px ${theme.palette.primary.main}`,
}));

const getMinerKey = (type: CheckAccessType) => `miner-${type}-check`;

type Props = BoxProps & {
  telegramUrl?: string;
  webUrl?: string;
  twitterUrl?: string;
  access?: boolean;
  accessText?: string;
  accessHref?: string;
  accessType?: CheckAccessType;
  accessLoading?: boolean;
};

export function MiningStorageInfo(props: Props) {
  const {
    webUrl,
    telegramUrl,
    twitterUrl,
    access,
    accessText,
    accessHref,
    accessType,
    accessLoading = false,
    ...other
  } = props;

  const account = useAccount();
  const { t } = useTranslation('mining');
  const [isChecked, setIsChecked] = useState(false);

  const checkApi = useUserAccessApi();

  useEffect(() => {
    if (accessType) {
      if (!accessLoading) {
        if (access) {
          storage.removeItem(getMinerKey(accessType)).then();
        } else {
          storage.getItem(getMinerKey(accessType)).then((v) => {
            setIsChecked(v === 'true');
          });
        }
      }
    }
  }, [account.chainId, access, accessLoading]);
  useEffect(() => {
    if (!accessHref) {
      setIsChecked(true);
    }
  }, [accessHref]);

  return (
    <StyledRoot {...other}>
      <Button
        variant='outlined'
        size='small'
        color={'primary'}
        fullWidth
        sx={{ height: 40 }}
        loading={checkApi.loading || accessLoading}
        disabled={!accessType}
        onClick={() => {
          if (accessHref && !isChecked) {
            openLink(accessHref);

            storage.setItem(getMinerKey(accessType), 'true').then();
            setIsChecked(true);

            return;
          }

          checkApi
            .check(accessType)
            .then((r) => {
              if (accessType === CheckAccessType.Zar && !r.minerZar) {
                toaster.warning('Quest does not completed');
              }
            })
            .catch((e) => {
              toaster.captureException(e);
              setIsChecked(false);
            })
            .finally(() => {
              storage.removeItem(getMinerKey(accessType)).then();
              setIsChecked(false);
            });
        }}
      >
        {access
          ? t('Ready to claim')
          : isChecked
          ? t('Check Access')
          : t(accessText || 'No Access')}
      </Button>

      {webUrl && (
        <StyledBtn onClick={() => window.open(webUrl, '_blank')}>
          <Web />
        </StyledBtn>
      )}

      {twitterUrl && (
        <StyledBtn onClick={() => window.open(twitterUrl, '_blank')}>
          <Twitter />
        </StyledBtn>
      )}

      {telegramUrl && (
        <StyledBtn onClick={() => window.open(telegramUrl, '_blank')}>
          <Telegram />
        </StyledBtn>
      )}
    </StyledRoot>
  );
}
