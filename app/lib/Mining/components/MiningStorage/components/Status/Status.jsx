import React from 'react';
import styles from './Status.module.scss';
import useMiningTicker from 'lib/Mining/components/MiningStorage/utils/useMiningTicker';
import useStatus from 'lib/Mining/components/MiningStorage/utils/useStatus';
import useRequirements from 'lib/Mining/components/MiningStorage/utils/useRequirements.tsx';

function Status({
  requirementsData,
  rewardPerSecond,
  isMiningDisabled,
  sizeLimit,
  lastGetTimestamp,
  isClaiming,
  isRefClaiming,
  isMiningLoading,
  claimTimestamp,
  mined,
  sizeLevel,
  speedLevel,
  disabled,
}) {
  const { minedValue } = useMiningTicker({
    start: lastGetTimestamp,
    mined,
    limit: sizeLimit,
    rewardPerSecond,
    claimTimestamp,
  });

  const requirements = useRequirements({ sizeLevel, speedLevel }, requirementsData);
  const notReady = !isMiningDisabled && (isClaiming || isRefClaiming || isMiningLoading);

  const statusText = useStatus({
    notReady,
    requirements,
    limit: sizeLimit,
    mined: minedValue,
    rewardPerSecond,
    claimTimestamp,
  });

  return <div className={styles.status}>{disabled ? 'Empty storage' : statusText}</div>;
}

export default Status;
