import React from 'react';
import styles from './MiningStorage.module.scss';
import { Button } from '@ui-kit/Button/Button.tsx';
import { styled, ThemeProvider, useTheme } from '@mui/material/styles';
import { MinerInfo } from 'lib/Mining/components/MinersSlider/components/MinerInfo/MinerInfo.tsx';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import { useMining } from 'lib/Mining/hooks/useMining.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTranslation } from 'react-i18next';
import BuyTokenAlertLazy from 'lib/TelegramStars/components/BuyTokenAlert/BuyTokenAlert.lazy';
import {
  IS_DECOR_ENABLED,
  IS_MINER_DRONE,
  IS_PAYABLE,
  SHOW_NO_GAS_TOKEN_ALERT,
  SHOW_NO_TRADE_TOKEN_ALERT,
} from '@cfg/app';
import { Chain } from 'services/multichain/chains';
import MinerClaimerStatus from 'lib/MinerClaimer/components/MinerClaimerStatus/MinerClaimerStatus.tsx';
import MiningRateBlock from 'lib/Mining/components/MiningRateBlock/MiningRateBlock';
import { MINER_CLAIMER_CONTRACT_ID } from 'lib/MinerClaimer/api-contract/miner-claimer.constants.ts';
import { MiningStorageToken } from 'lib/Mining/components/MiningStorage/MiningStorageToken/MiningStorageToken.tsx';
import { getMiningStatus } from 'lib/Mining/utils/get-mining-status.ts';
import { IntervalRender } from '@ui-kit/IntervalRender/IntervalRender.tsx';
import { getCurrentMined } from 'lib/Mining/utils/get-current-mined.ts';
import { ProgressBar } from '@ui-kit/ProgressBar/ProgressBar.tsx';
import { getMiningPercent, getMiningRefPercent } from 'lib/Mining/utils/get-mining-percent.ts';
import { MiningStorageInfo } from 'lib/Mining/components/MiningStorage/MiningStorageInfo/MiningStorageInfo.tsx';
import { getMiningStorageBtnText } from 'lib/Mining/components/MiningStorage/utils.ts';
import BuyGasAlertLazy from 'lib/TelegramStars/components/BuyGasAlert/BuyGasAlert.lazy.tsx';
import Divider from '@mui/material/Divider';
import { Img } from '@ui-kit/Img/Img.tsx';
import hatImage from 'assets/img/decor/new-year/hat.png';
import { CornerDecor } from '../../../../shared/decor/CornerDecor.tsx';
import { useTransactionConfirmation } from '@chain/hooks/useTransactionConfirmation.ts';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

const StyledBox = styled(Paper)(({ theme }) => ({
  padding: '3px',
  border: 'solid 1px',
  borderColor: theme.palette.primary.main,
}));

function MiningStorage({
  tutorialKey,
  title,
  symbol,
  symbolLoading,
  icon,
  minerAddress,
  minerType,
  payableAddress,
  requirements: requirementsData,
  theme,
  referral = false,
  info,
  gasSymbol,
  gasBalanceReload,
  gasBalance = 0,
  gasBalanceLoading = false,
  minerDisabled = false,
  noGas = false,
  noTradeToken = false,
  soon = false,
  accessText,
  accessHref,
  accessType,
  accessLoading = false,
  txCost,
  txCostSymbol,
  txCostBot,
  txCostSlot,
  txCostExtra,
  txCostLoading = false,
  txCostReload,
  txRate,
  txRateLoading,
  txCount,
  txCountLoading,
  txCostAddress,
  karma,
  access,
  friendsCount,
}) {
  const { t } = useTranslation('mining');
  const account = useAccount();
  const currentTheme = useTheme();
  const userInfo = useSelfUserInfo();

  const miningApi = useMining({
    minerAddress,
    payableAddress,
    type: minerType,
    payablePrice: txCost?.value,
    afterClaim: () => {
      txCostReload?.();
      gasBalanceReload?.();
    },
  });

  const confirmation = useTransactionConfirmation({
    txHash: miningApi.txHash,
    cacheKey: `miner-claim-${symbol}-${referral ? 'ref' : 'base'}`,
  });

  const { isRefClaiming, isClaiming, isMiningDisabled, isMiningLoading, isMigrating } = miningApi;

  const {
    mined,
    sizeLimit,
    rewardPerSecond,
    claimTimestamp,
    updatedAt: lastGetTimestamp,
    balance,
    extra: { refStorage, refLimit, sizeLevel, speedLevel } = {},
  } = miningApi.data || {};

  const isBadKarma = karma?.value < 5000;
  const notReady = !isMiningDisabled && (isClaiming || isRefClaiming || isMiningLoading);
  const isStart = !notReady && !claimTimestamp && !isMiningDisabled && !soon && access;

  const isDisabled =
    isClaiming ||
    isRefClaiming ||
    isMiningDisabled ||
    isMigrating ||
    minerDisabled ||
    !access ||
    soon ||
    noGas ||
    noTradeToken ||
    confirmation.isPending ||
    !userInfo.data;
  const loading =
    isClaiming || isMiningLoading || isRefClaiming || isMigrating || (IS_PAYABLE && txCostLoading);
  const miningLoading = isMiningLoading || isRefClaiming || isMigrating;

  const iconValue = icon;
  const titleText = title;
  const symbolText = symbol;

  const btnText = t(
    getMiningStorageBtnText({
      soon,
      disabled: isMiningDisabled,
      access,
      noGas,
      noTradeToken,
      isStart,
      referral,
      isConfirmationPending: confirmation.isPending,
      confirmations: confirmation.confirmations,
    })
  );

  return (
    <ThemeProvider theme={theme || currentTheme}>
      <MinerInfo
        id={`${tutorialKey}-balance`}
        rewardPerSecond={rewardPerSecond || 0}
        symbol={symbol || 'PXLs'}
        icon={icon || ''}
        balance={balance}
        gas={gasBalance}
        gasSymbol={gasSymbol || ''}
        gasless={miningApi.gasLess || 0}
        useGasless={miningApi.useGasLess || false}
        onToggleGasless={() => miningApi.updateGasLess(!miningApi.useGasLess)}
      />

      <MiningRateBlock
        tutorialKey={tutorialKey}
        sx={{ px: 1, mb: 2 }}
        // miner
        minerSymbol={symbol}
        minerSymbolLoading={symbolLoading}
        minerSpeed={(rewardPerSecond || 0) * 3600}
        minerSpeedLoading={miningApi.loading}
        // tx cost
        txCost={txCost?.formatted}
        txCostSymbol={txCostSymbol}
        txCostLoading={txCostLoading}
        txRate={txRate}
        txRateLoading={txRateLoading}
        txCount={txCount}
        txCountLoading={txCountLoading}
        txCostExtra={txCostExtra}
        txCostBot={txCostBot}
        txCostSlot={txCostSlot}
        txCostAddress={txCostAddress}
        // gas
        gasSymbol={gasSymbol}
        gasBalance={gasBalance}
        gasBalanceLoading={gasBalanceLoading}
        // gasless
        gasless={miningApi.gasLess || 0}
        useGasless={miningApi.useGasLess || false}
        onToggleGasless={() => miningApi.updateGasLess(!miningApi.useGasLess)}
        // karma
        karma={karma}
      />

      <StyledBox
        id={`${tutorialKey}-miner`}
        elevation={0}
        // variant='outlined'
        sx={{ position: 'relative' }}
      >
        {IS_DECOR_ENABLED && title === 'Pixel Storage' && <CornerDecor />}
        <Box>
          <MiningStorageToken
            tutorialKey={tutorialKey}
            loading={miningLoading}
            image={iconValue}
            title={titleText}
            amount={
              <IntervalRender
                render={() =>
                  referral
                    ? refStorage?.toFixed(4)
                    : getCurrentMined({
                        sizeLimit,
                        mined,
                        rewardPerSecond,
                        lastGetTimestamp,
                      })
                }
              />
            }
            maxAmount={referral ? refLimit : sizeLimit}
            symbol={symbolText}
            isBadKarma={isBadKarma}
            status={
              referral ? (
                t(`Friends`) + ' ' + (friendsCount || 0)
              ) : (
                <IntervalRender
                  render={() =>
                    getMiningStatus({
                      notReady:
                        !isMiningDisabled && (isClaiming || isRefClaiming || isMiningLoading),
                      sizeLimit,
                      mined,
                      claimTimestamp,
                      rewardPerSecond,
                      lastGetTimestamp,
                      access,
                      accessText,
                    })
                  }
                />
              )
            }
          />

          <Divider />

          <Box sx={{ px: 1, pb: 1, mt: 1 }}>
            <IntervalRender
              render={() => (
                <ProgressBar
                  id={`${tutorialKey}-miner-progress`}
                  value={
                    referral
                      ? getMiningRefPercent({
                          refStorage,
                          refLimit,
                        })
                      : getMiningPercent({
                          mined,
                          sizeLimit,
                          rewardPerSecond,
                          lastGetTimestamp,
                        })
                  }
                  size='medium'
                  total={100}
                  color={isBadKarma ? 'error' : referral ? 'success' : 'primary'}
                />
              )}
            />

            <div className={styles.storageActions}>
              <div className={styles.storageActionsButtons}>
                <Button
                  id={`${tutorialKey}-miner-claim`}
                  size='large'
                  variant='contained'
                  color={isBadKarma ? 'error' : referral ? 'success' : 'primary'}
                  disabled={isDisabled}
                  loading={loading}
                  onClick={referral ? miningApi.claimRef : miningApi.claim}
                  fullWidth
                >
                  {btnText}
                </Button>
              </div>
            </div>
          </Box>

          {info && (
            <MiningStorageInfo
              telegramUrl={info?.telegram}
              webUrl={info.web}
              twitterUrl={info?.twitter}
              access={access}
              accessText={accessText}
              accessHref={accessHref}
              accessType={accessType}
              accessLoading={accessLoading}
              sx={{ p: 2, pt: 1 }}
            />
          )}
        </Box>

        {MINER_CLAIMER_CONTRACT_ID[account.chainId] && IS_MINER_DRONE && (
          <MinerClaimerStatus sx={{ mt: 2, px: 1, mb: 2 }} />
        )}

        {[Chain.SONGBIRD, Chain.SKALE].includes(account.chainId) &&
          (noGas || SHOW_NO_GAS_TOKEN_ALERT) && (
            <BuyGasAlertLazy sx={{ mt: 2, px: 1, mb: 2 }} symbol={gasSymbol} />
          )}
        {[Chain.SKALE].includes(account.chainId) && (noTradeToken || SHOW_NO_TRADE_TOKEN_ALERT) && (
          <BuyTokenAlertLazy sx={{ mt: 2, px: 1, mb: 2 }} symbol={txCostSymbol} />
        )}
      </StyledBox>
    </ThemeProvider>
  );
}

export default MiningStorage;
