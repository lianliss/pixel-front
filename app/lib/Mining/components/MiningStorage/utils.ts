export function getMiningStorageBtnText({
  soon,
  isStart,
  access,
  noGas,
  disabled,
  referral,
  noTradeToken,
  isConfirmationPending = false,
  confirmations,
}: {
  referral?: boolean;
  soon: boolean;
  noGas: boolean;
  disabled: boolean;
  access: boolean;
  isStart: boolean;
  noTradeToken?: boolean;
  isConfirmationPending?: boolean;
  confirmations?: number;
}) {
  if (soon) {
    return 'Soon';
  }

  if (noGas) {
    return 'No enough Gas';
  }
  if (noTradeToken) {
    return 'No enough Tokens';
  }

  if (disabled) {
    return 'NOT AVAILABLE';
  }

  if (!access) {
    return 'No Access';
  }

  if (isConfirmationPending) {
    return `Wait for confirmations ${confirmations || ''}`;
  }

  if (isStart) {
    return 'START MINING';
  }

  if (referral) {
    return 'CLAIM REFERS';
  }

  return 'CLAIM Storage';
}
