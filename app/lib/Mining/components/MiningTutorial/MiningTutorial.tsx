import { Tutorial } from '../../../../widget/tutorial';
import { useTutorial } from '../../../../widget/tutorial/useTutorial.tsx';
import { Tutorials } from '../../../../widget/tutorial/config/config.tsx';
import React, { useEffect } from 'react';
import { MINING_TUTORIAL_CONFIG } from 'lib/Mining/components/MiningTutorial/utils/config.tsx';
import { TUTORIAL_MINING_ENABLED } from '@cfg/app.ts';

export function MiningTutorial(props: { start?: boolean }) {
  const { startTutorial, checkRunTutorial, tutorial, completeTutorial } = useTutorial();
  const [stepTutorial, setStepTutorial] = React.useState(1);

  useEffect(() => {
    if (props.start && TUTORIAL_MINING_ENABLED) {
      const dataTutorial = checkRunTutorial(Tutorials.mining);

      if (dataTutorial.isStart) {
        startTutorial(Tutorials.mining, dataTutorial.data);
      }
    }
  }, [props.start]);

  return (
    <Tutorial
      value={tutorial}
      step={stepTutorial}
      onChangeStep={(v) => {
        setStepTutorial(v);
      }}
      onComplete={completeTutorial}
      config={MINING_TUTORIAL_CONFIG}
    />
  );
}
