import { useGasBalance } from '@chain/hooks/useGasBalance.ts';

export function isMiningTutorialStart(gasBalance: ReturnType<typeof useGasBalance>): boolean {
  const sgbBalance = gasBalance?.data?.formatted;

  if (!sgbBalance) return false; // Early return if balance is not available

  const formattedBalance = parseFloat(sgbBalance.toFixed(2));

  if (formattedBalance < 0.2) return false;

  return true;
}
