import { ITutorialConfig } from '../../../../../widget/tutorial/config/config.tsx';

export const MINING_TUTORIAL_CONFIG: ITutorialConfig = {
  steps: [
    {
      id: 1,
      description:
        'Это страница майнинга. Тут я вам расскажу как вы сможете добывать себе криптовалюту и зарабатывать.',
      hintPosition: 'center',
      isFooter: true,
    },

    // top info
    {
      id: 2,
      description:
        'Здесь указан баланс основной криптовалюты нашего кошелька PXLs, которая добыта на текущий момент',
      hintPosition: 'center',
      targetElement: 'mining-balance',
      // targetContainerElement: 'mining',
    },

    // miner info
    {
      id: 3,
      description:
        'Здесь указана стоимость транзакции, то есть сколько гас токена спишется за действие в приложении',
      hintPosition: 'center',
      targetElement: 'mining-tx',
      // targetContainerElement: 'mining',
    },
    {
      id: 4,
      description: 'Это текущая добыча токена в час',
      hintPosition: 'center',
      targetElement: 'mining-speed',
      // targetContainerElement: 'mining',
    },
    {
      id: 5,
      description:
        'Здесь указан баланс гас токена. Гас токен необходим для совершения любых действий в нашем приложении. Наше приложение является ончейн, значит логика всех действий находится в смарт контрактах в открытом доступе',
      hintPosition: 'center',
      targetElement: 'mining-gas',
      // targetContainerElement: 'mining',
    },

    // miner
    {
      id: 6,
      description: 'Это окно майнинга криптовалюты, давайте я расскажу вам что в нём есть',
      hintPosition: 'end',
      targetElement: 'mining-miner',
      // targetContainerElement: 'mining',
    },
    {
      id: 7,
      description:
        'Это прогресс бар заполнения криптовалюты в складе. Если склад переполнится, она перестанет добываться',
      hintPosition: 'end',
      targetElement: 'mining-miner-progress',
      // targetContainerElement: 'mining',
    },
    {
      id: 8,
      description: 'Это значение того сколько добыто криптовалюты на склад',
      hintPosition: 'end',
      targetElement: 'mining-miner-balance',
      // targetContainerElement: 'mining',
    },
    {
      id: 9,
      description: 'Это вместимость склада',
      hintPosition: 'end',
      targetElement: 'mining-miner-storage',
      // targetContainerElement: 'mining',
    },
    {
      id: 10,
      description:
        'Похоже, что ваш склад заполнился и добыча остановилась, давайте соберём ваш первый доход',
      hintPosition: 'end',
      targetElement: 'mining-miner-claim',
      targetClickElement: 'mining-miner-claim',
      // targetContainerElement: 'mining',
      targetClickType: 'complete',
      isFooter: true,
    },
  ],
};

// "Здесь указан баланс основной криптовалюты нашего кошелька PXLs, которая добыта на текущий момент",
// 'Здесь указан баланс гас токена. Гас токен необходим для совершения любых действий в нашем приложении. Наше приложение является ончейн, значит логика всех действий находится в смарт контрактах в открытом доступе',
// 'Здесь указана стоимость транзакции, то есть сколько гас токена спишется за действие в приложении',
// 'Здесь указан список токенов, который у вас добывается в данный момент. Выбор из токенов может быть очень большой, но одновременно вы можете добывать не больше трёх'
// Давайте рассмотрим майнинг более подробно
// Это окно майнинга криптовалюты, давайте я расскажу вам что в нём есть
// Это прогресс бар заполнения криптовалюты в складе. Если склад переполнится, она перестанет добываться
// Это значение того сколько добыто криптовалюты на склад
// Это вместимость склада
// Здесь указан текущий баланс у вас на кошельке добываемого токена
// Это прогресс бар заполнения реферального склада. Если вы пригласите своих друзей в игру, в зависимости от того, сколько они добывают, вы будете получать бонусы, которые попадут на этот склад
// Похоже, что ваш склад заполнился и добыча остановилась, давайте соберём ваш первый доход
// Если ваш реферальный склад заполнится, для этого предусмотрена отдельная кнопка
// Пролистай вправо, чтобы посмотреть какую ещё криптовалюту вы можете добывать. Максимально можно добывать три криптовалюты одновременно, при выполненнии определённых требований
