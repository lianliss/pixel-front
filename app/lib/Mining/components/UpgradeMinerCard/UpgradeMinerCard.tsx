import WalletBlock from 'ui/WalletBlock/WalletBlock';
import styles from 'lib/Mining/components/UpgradeMinerCard/UpgradeMinerCard.module.scss';
import { Icon } from '@blueprintjs/core';
import getFinePrice from 'utils/getFinePrice';
import React from 'react';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import { CornerDecor } from '../../../../shared/decor/CornerDecor.tsx';

const StyledRoot = styled(Paper)(() => ({
  padding: '8px 12px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  position: 'relative',
}));

const calcDiscountPercent = (v) => (-(1 - v) * 100).toFixed(2);

type Props = {
  image: string;
  onClick?: () => void;
  level: number;
  symbol: string;
  title: string;
  summary: React.ReactNode;
  price: number;
  discountPercent: number;
  disableUpgrade?: boolean;
  decor?: boolean;
  tutorialKey?: string;
  noBalance?: boolean;
};

export function UpgradeMinerCard(props: Props) {
  const {
    onClick,
    image,
    level = 0,
    disableUpgrade = false,
    symbol,
    title,
    summary,
    price,
    discountPercent,
    decor = false,
    tutorialKey,
    noBalance = false,
  } = props;

  return (
    <StyledRoot
      variant='outlined'
      onClick={onClick}
      id={tutorialKey}
      sx={{ opacity: noBalance ? 0.7 : 1 }}
    >
      {decor && <CornerDecor />}
      <div className={styles.tokenInfo}>
        <div className={styles.tokenInfoIcon}>
          <img src={image} alt={''} />
        </div>
        <div className={styles.tokenInfoTitle}>
          <div className={styles.tokenInfoTitleSymbol}>{title}</div>
          <div className={styles.tokenInfoTitleName}>{summary}</div>
          <div className={styles.tokenInfoTitleParams}>
            <div className={styles.tokenInfoTitleParamsItem}>
              <Icon icon={'layers'} />
              <span
                id={`${tutorialKey}-level`}
                style={{ border: 'solid 2px transparent', boxSizing: 'border-box' }}
              >
                {level}
              </span>
            </div>
            <div className={styles.tokenInfoTitleParamsItem}>
              {!disableUpgrade ? (
                <>
                  <Icon icon={'double-chevron-up'} />
                  <span
                    id={`${tutorialKey}-price`}
                    style={{ border: 'solid 2px transparent', boxSizing: 'border-box' }}
                  >
                    {getFinePrice(price)}
                    <span className={styles.tokenInfoTitleParamsItemPrice}>
                      <span>{symbol}</span>
                      {discountPercent !== 1 && (
                        <span>({calcDiscountPercent(discountPercent)}%)</span>
                      )}
                    </span>
                  </span>
                </>
              ) : (
                'Completed'
              )}
            </div>
          </div>
        </div>
      </div>
      <div className={styles.tokenBalance}>
        <div className={styles.tokenBalanceAction}>
          <Icon icon={'chevron-right'} />
        </div>
      </div>
    </StyledRoot>
  );
}
