import React, { useState } from 'react';
import styles from './Build.module.scss';
import getFinePrice from 'utils/getFinePrice';

import last from 'lodash-es/last';
import { Chain, MINING_CONFIG } from 'services/multichain/chains';
import { useSumNftStats } from 'lib/Inventory/components/StatsDialog/useSumNftStats.ts';
import { useAccount } from '@chain/hooks/useAccount';
import { UpgradeMinerCard } from 'lib/Mining/components/UpgradeMinerCard/UpgradeMinerCard.tsx';
import { useCoreToken } from '@chain/hooks/useCoreToken.ts';
import { useMining } from 'lib/Mining/hooks/useMining.ts';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { useGasBalance } from '@chain/hooks/useGasBalance';
import ChainSwitcher from 'app/widget/ChainSwitcher/ChainSwitcher';
import { useTranslation } from 'react-i18next';
import { useMiningUpgrade } from 'lib/Mining/hooks/useMiningUpgrade';
import { UpgradeMinerDialog } from 'lib/Mining/components/UpgradeMinerDialog/UpgradeMinerDialog.tsx';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import NotAvailableChain from '../../../../shared/blocks/NotAvailableChain/NotAvailableChain.tsx';
import { waitForTransactionReceipt } from '@wagmi/core';
import { useConfig } from 'wagmi';
import { usePayable } from 'lib/Mining/hooks/usePayable.ts';
import { TransactionReceipt } from 'viem';
import toaster from 'services/toaster.tsx';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { useTradeBalance } from '@chain/hooks/useTradeBalance.ts';
import { isNoGasTokenBalance, isNoTradeTokenBalance } from 'lib/Mining/utils/check/no-balance.ts';
import { IS_DECOR_ENABLED, SHOW_NO_GAS_TOKEN_ALERT, SHOW_NO_TRADE_TOKEN_ALERT } from '@cfg/app.ts';
import BuyGasAlertLazy from 'lib/TelegramStars/components/BuyGasAlert/BuyGasAlert.lazy.tsx';
import { useToken } from '@chain/hooks/useToken.ts';
import BuyTokenAlertLazy from 'lib/TelegramStars/components/BuyTokenAlert/BuyTokenAlert.lazy.tsx';
import MiningRateBlock from 'lib/Mining/components/MiningRateBlock/MiningRateBlock.tsx';
import { getDefaultChainImage } from '@cfg/image.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Stack from '@mui/material/Stack';
import { MiningBuildTutorial } from 'lib/Mining/components/MiningBuildTutorial/MiningBuildTutorial.tsx';
import {
  getUpgradeRefPrice,
  getUpgradeSpeedPrice,
  getUpgradeStoragePrice,
} from 'lib/Mining/utils/upgrade-helpers.ts';
import {
  getLevelRef,
  getLevelSpeed,
  getLevelSize,
  getDroneLevelSize,
} from 'lib/Mining/utils/build-target-value.ts';
import Container from '@mui/material/Container';
import { useDustToken } from 'lib/MinerDrone/hooks/useDustToken.ts';
import { MINER_DRONE_CONTRACT_ID } from 'lib/MinerDrone/api-contract/miner-drone.constants.ts';
import {
  UPGRADE_DRILL_IMAGES,
  UPGRADE_DRONE_IMAGES,
  UPGRADE_REF_IMAGES,
  UPGRADE_STORAGE_IMAGES,
} from 'lib/Mining/utils/upgrade-images.ts';
import { useMiningUpgradeDronePrice } from 'lib/MinerDrone/hooks/useMiningUpgradeDronePrice.ts';
import { useMiningDroneLevel } from 'lib/MinerDrone/hooks/useMiningDroneLevel.ts';
import { useMinerDroneUpgrade } from 'lib/MinerDrone/hooks/useMinerDroneUpgrade.ts';
import { useCoreBalance } from '@chain/hooks/useCoreBalance.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';
import Box from '@mui/material/Box';
import { Img } from '@ui-kit/Img/Img.tsx';
import { useDustBalance } from 'lib/MinerDrone/hooks/useDustBalance.ts';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

const RenderBalance = ({ symbol, image, balance }) => (
  <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', gap: 0 }}>
    <Box sx={{ display: 'flex', alignItems: 'center', gap: 1 }}>
      <Img sx={{ width: 32 }} src={image} alt={''} />
      <Typography variant='subtitle2' fontWeight='bold' sx={{ fontSize: 20 }}>
        {symbol}
      </Typography>
    </Box>

    <Typography color='text.primary' className={styles.buildHeaderBalance}>
      {getFinePrice(balance)}
    </Typography>
  </Box>
);

const RenderInfo = ({ storage, speed, refStorage, droneStorage, isDrone, symbol, dustSymbol }) => (
  <div>
    <Typography variant='caption' component='p' color='text.secondary' align='center'>
      Storage size:{' '}
      <Typography component='span' variant='inherit' color='text.primary'>
        {getFinePrice(storage)}
      </Typography>{' '}
      {symbol}
    </Typography>
    <Typography variant='caption' component='p' color='text.secondary' align='center'>
      Mining speed:{' '}
      <Typography component='span' variant='inherit' color='text.primary'>
        {getFinePrice(speed)}
      </Typography>{' '}
      {symbol} / hour
    </Typography>
    <Typography variant='caption' component='p' color='text.secondary' align='center'>
      Referral size:{' '}
      <Typography component='span' variant='inherit' color='text.primary'>
        {getFinePrice(refStorage)}
      </Typography>{' '}
      {symbol}
    </Typography>
    {isDrone && (
      <Typography variant='caption' component='p' color='text.secondary' align='center'>
        Drone size:{' '}
        <Typography component='span' variant='inherit' color='text.primary'>
          {getFinePrice(droneStorage)}
        </Typography>{' '}
        {dustSymbol}
      </Typography>
    )}
  </div>
);

function Build() {
  // basic
  const account = useAccount();
  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;
  const sumNftStats = useSumNftStats({ autoLoad: true });

  const wagmiConfig = useConfig();

  const config = MINING_CONFIG[account.chainId];

  const { t } = useTranslation('build');

  // ========== TOKENS ==========
  const gasToken = useToken({});
  const tradeToken = useTradeToken();
  const coreToken = useCoreToken();
  const dustToken = useDustToken();

  const coreSymbol = coreToken.data?.symbol;
  const dustSymbol = dustToken.data?.symbol;
  const upgradeSymbol = dustSymbol || coreSymbol;
  // ========== TOKENS END ==========

  // ========== BALANCE ==========
  const gasBalance = useGasBalance({
    address: account.accountAddress,
    skip: !account.isConnected,
  });
  const tradeBalance = useTradeBalance({});
  const coreBalance = useCoreBalance({});
  const dustBalance = useDustBalance({
    skip: !dustToken.data?.address || !account.isConnected,
  });
  const upgradeBalance = dustToken.data ? dustBalance : coreBalance;
  const dustBalanceAmount = dustBalance.data?.formatted || 0;
  const coreBalanceAmount = coreBalance.data?.formatted || 0;

  const payableInfo = usePayable();
  const noGasBalance = isNoGasTokenBalance(payableInfo, gasBalance, tradeToken);
  const noTradeBalance = isNoTradeTokenBalance(payableInfo, tradeBalance);

  // ========== BALANCE END ==========

  // ========== DRONE ==========
  const minerDroneAddress = MINER_DRONE_CONTRACT_ID[account.chainId];
  const isDrone = !!minerDroneAddress;

  const miningDroneUpgradePrice = useMiningUpgradeDronePrice({
    minerDroneAddress,
    userAddress: account.accountAddress,
  });
  const miningDroneLevel = useMiningDroneLevel({
    minerDroneAddress,
    userAddress: account.accountAddress,
  });
  const minerDroneUpgrade = useMinerDroneUpgrade({
    minerDroneAddress,
  });

  const droneLevel = miningDroneLevel.data || 0;
  const droneSize = getDroneLevelSize(droneLevel);

  const droneImage = UPGRADE_DRONE_IMAGES[droneLevel] || last(UPGRADE_DRONE_IMAGES);
  const droneNextImage = UPGRADE_DRONE_IMAGES[droneLevel + 1] || last(UPGRADE_DRONE_IMAGES);
  const dronePrice = miningDroneUpgradePrice.data;

  const upgradeDroneInfo = `${t('Holds')} ${getFinePrice(
    getDroneLevelSize(droneLevel)
  )} ${dustSymbol}`;
  const upgradeDroneNextInfo = `${t('Holds')} ${getFinePrice(
    getDroneLevelSize(droneLevel + 1)
  )} ${dustSymbol}`;

  const disabledUpgradeDrone = dronePrice > coreBalanceAmount;
  // =========== DRONE END ===========

  // ========== MINER ==========
  const minerAddress = MINER_CONTRACT_ID[account.chainId]?.core;

  const miningApi = useMining({
    minerAddress,
  });
  const {
    rewardPerSecond,
    sizeLimit,
    balance: claimed,
    extra: { speedLevel = 0, referralLevel = 0, sizeLevel = 0, refLimit, refStorage } = {},
  } = miningApi.data || {};

  const miningUpgrade = useMiningUpgrade({
    payablePrice: payableInfo.cost.data?.value,
  });

  const rewardPerHour = rewardPerSecond * 3600;
  const size = UPGRADE_STORAGE_IMAGES[sizeLevel] || last(UPGRADE_STORAGE_IMAGES);
  const nextSize = UPGRADE_STORAGE_IMAGES[sizeLevel + 1] || last(UPGRADE_STORAGE_IMAGES);
  const speed = UPGRADE_DRILL_IMAGES[speedLevel] || last(UPGRADE_DRILL_IMAGES);
  const nextSpeed = UPGRADE_DRILL_IMAGES[speedLevel + 1] || last(UPGRADE_DRILL_IMAGES);
  const ref = UPGRADE_REF_IMAGES[referralLevel] || last(UPGRADE_REF_IMAGES);
  const nextRef = UPGRADE_REF_IMAGES[referralLevel + 1] || last(UPGRADE_REF_IMAGES);

  const storagePrice =
    getUpgradeStoragePrice(sizeLevel, undefined, config) * sumNftStats.discount.storage;
  const speedPrice =
    getUpgradeSpeedPrice(speedLevel, undefined, config) * sumNftStats.discount.drill;
  const refPrice =
    getUpgradeRefPrice(referralLevel, undefined, config) * sumNftStats.discount.referralStorage;
  const isStorageUpgrade = claimed > storagePrice;

  const upgradeDrillInfo = t('Mine hour', {
    count: `${getFinePrice(getLevelSpeed(speedLevel, undefined, undefined, config))} ${coreSymbol}`,
  });
  const upgradeDrillNextInfo = t('Mine hour', {
    count: `${getFinePrice(
      getLevelSpeed(speedLevel + 1, undefined, undefined, config)
    )} ${coreSymbol}`,
  });
  const upgradeStorageInfo = `${t('Holds')} ${getFinePrice(
    getLevelSize(sizeLevel, undefined, undefined, config)
  )} ${coreSymbol}`;
  const upgradeStorageNextInfo = `${t('Holds')} ${getFinePrice(
    getLevelSize(sizeLevel + 1, undefined, undefined, config)
  )} ${coreSymbol}`;
  const upgradeRefInfo = `${t('Holds')} ${getFinePrice(
    getLevelRef(referralLevel + 0, undefined, undefined, config)
  )} ${coreSymbol}`;
  const upgradeRefNextInfo = `${t('Holds')} ${getFinePrice(
    getLevelRef(referralLevel + 1, undefined, undefined, config)
  )} ${coreSymbol}`;

  const upgradeBalanceAmount = isDrone ? dustBalanceAmount : coreBalanceAmount;
  const disabledUpgradeDrill = speedPrice > upgradeBalanceAmount;
  const disabledUpgradeStorage = storagePrice > upgradeBalanceAmount;
  const disabledUpgradeRef = refPrice > upgradeBalanceAmount;
  // ========== MINER END ==========

  // =========== STATE END ===========
  const [loading, setLoading] = useState(false);

  const [upgradeDrill, setUpgradeDrill] = useState(false);
  const [upgradeStorage, setUpgradeStorage] = useState(false);
  const [upgradeRefStorage, setUpgradeRefStorage] = useState(false);
  const [upgradeDrone, setUpgradeDrone] = useState(false);
  // =========== STATE END ===========

  // =========== HANDLERS END ===========
  const handleUpgradeRef = () => {
    setLoading(true);

    miningUpgrade
      .upgradeRefStorage(telegramId)
      .then(async (receipt: TransactionReceipt) => {
        await waitForTransactionReceipt(wagmiConfig, { hash: receipt.transactionHash });

        setUpgradeRefStorage(false);
        await miningApi.refresh();
        await payableInfo.refetch();
        await upgradeBalance.refetch();
      })
      .catch((e) => toaster.captureException(e))
      .finally(() => setLoading(false));
  };
  const handleUpgradeStorage = async () => {
    try {
      setLoading(true);

      const receipt = await miningUpgrade.upgradeStorage(telegramId);
      await waitForTransactionReceipt(wagmiConfig, { hash: receipt.transactionHash });

      setUpgradeStorage(false);

      await miningApi.refresh();
      await payableInfo.refetch();
      await upgradeBalance.refetch();
    } catch (e) {
      toaster.captureException(e);
    } finally {
      setLoading(false);
    }
  };
  const handleUpgradeDrill = () => {
    setLoading(true);

    miningUpgrade
      .upgradeDrill(telegramId)
      .then(async (receipt: TransactionReceipt) => {
        await waitForTransactionReceipt(wagmiConfig, { hash: receipt.transactionHash });

        setUpgradeDrill(false);
        await miningApi.refresh();
        await payableInfo.refetch();
        await upgradeBalance.refetch();
      })
      .catch((e) => toaster.captureException(e))
      .finally(() => setLoading(false));
  };
  const handleUpgradeDrone = async () => {
    setLoading(true);

    try {
      const receipt = await minerDroneUpgrade.upgrade();
      await waitForTransactionReceipt(wagmiConfig, { hash: receipt.transactionHash });

      setUpgradeDrone(false);
      await miningDroneUpgradePrice.refetch();
      await coreBalance.refetch();
      await payableInfo.refetch();
    } catch (e) {
      toaster.captureException(e);
    } finally {
      setLoading(false);
    }
  };
  // =========== HANDLERS END ===========

  useBackAction('/wallet/mining');

  if (!MINER_CONTRACT_ID[account.chainId]?.core) {
    return (
      <>
        <ChainSwitcher chains={[Chain.SONGBIRD, Chain.SKALE, Chain.UNITS]} />
        <NotAvailableChain title='Miner not available' chainId={Chain.SONGBIRD} />
      </>
    );
  }

  return (
    <div
      className={styles.build}
      style={{ backgroundImage: `url(${getDefaultChainImage(account.chainId)})` }}
    >
      <Container maxWidth='xs'>
        <Typography variant='h2' fontWeight='bold' align='center' sx={{ mt: 2, mb: 2.5 }}>
          {t('Upgrade Miner')}
        </Typography>

        <ChainSwitcher chains={[Chain.SONGBIRD, Chain.SKALE, Chain.UNITS]} />

        <div className={styles.buildHeader}>
          <Box sx={{ display: 'flex', gap: 2, justifyContent: 'center' }}>
            <RenderBalance
              symbol={coreSymbol}
              balance={coreBalanceAmount}
              image={coreToken.data?.image}
            />

            {isDrone && (
              <RenderBalance
                symbol={dustSymbol}
                balance={dustBalanceAmount}
                image={dustToken.data?.image}
              />
            )}
          </Box>

          <RenderInfo
            storage={sizeLimit}
            refStorage={refLimit}
            speed={rewardPerHour}
            droneStorage={droneSize}
            symbol={coreSymbol}
            dustSymbol={dustSymbol}
            isDrone={isDrone}
          />
        </div>

        {[Chain.SONGBIRD, Chain.SKALE].includes(account.chainId) &&
          (noGasBalance || SHOW_NO_GAS_TOKEN_ALERT) && (
            <BuyGasAlertLazy sx={{ mt: 2, px: 1, mb: 2 }} symbol={gasToken.data?.symbol} />
          )}
        {[Chain.SKALE].includes(account.chainId) &&
          (noTradeBalance || SHOW_NO_TRADE_TOKEN_ALERT) && (
            <BuyTokenAlertLazy sx={{ mt: 2, px: 1, mb: 2 }} symbol={tradeToken.data?.symbol} />
          )}

        <Stack direction='column' gap={1}>
          <UpgradeMinerCard
            tutorialKey='mining-upgrade-storage'
            decor={IS_DECOR_ENABLED}
            title='Storage'
            summary={
              <>
                Increase the storage capacity
                <br />
                of the mined
              </>
            }
            level={sizeLevel + 1}
            disableUpgrade={!nextSize}
            discountPercent={sumNftStats.discount.storage}
            price={storagePrice}
            symbol={upgradeSymbol}
            image={size}
            noBalance={disabledUpgradeStorage}
            onClick={() => setUpgradeStorage(true)}
          />

          <UpgradeMinerCard
            tutorialKey='mining-upgrade-drill'
            title='Drill'
            summary='Increase mining speed'
            level={speedLevel + 1}
            disableUpgrade={!nextSpeed}
            discountPercent={sumNftStats.discount.drill}
            price={speedPrice}
            symbol={upgradeSymbol}
            image={speed}
            noBalance={disabledUpgradeDrill}
            onClick={() => setUpgradeDrill(true)}
          />

          <UpgradeMinerCard
            tutorialKey='mining-upgrade-ref'
            title='Referral Storage'
            summary='Increase referral storage capacity'
            level={referralLevel + 1}
            disableUpgrade={!nextRef}
            discountPercent={sumNftStats.discount.referralStorage}
            price={refPrice}
            symbol={upgradeSymbol}
            image={ref}
            noBalance={disabledUpgradeRef}
            onClick={() => setUpgradeRefStorage(true)}
          />

          {isDrone && (
            <UpgradeMinerCard
              tutorialKey='mining-drone'
              title='Miner Drone'
              summary='Increase drone storage capacity'
              level={droneLevel + 1}
              disableUpgrade={!droneNextImage}
              discountPercent={1}
              price={dronePrice}
              symbol={coreSymbol}
              image={droneImage}
              noBalance={disabledUpgradeDrone}
              onClick={() => setUpgradeDrone(true)}
            />
          )}
        </Stack>

        <UpgradeMinerDialog
          open={upgradeDrill}
          onClose={() => setUpgradeDrill(false)}
          title={t('Drill')}
          subTitle={t('Improved drill allows you to get more', { coreSymbol })}
          price={speedPrice}
          priceIcon={coreToken.data?.image}
          upgradeSymbol={upgradeSymbol}
          current={{
            level: speedLevel + 1,
            image: speed,
            info: upgradeDrillInfo,
          }}
          next={{
            level: speedLevel + 2,
            image: nextSpeed,
            info: upgradeDrillNextInfo,
          }}
          onUpgrade={handleUpgradeDrill}
          loading={
            miningUpgrade.loading ||
            miningUpgrade.recipe.isLoading ||
            payableInfo.cost.loading ||
            loading
          }
          disabled={noGasBalance || noTradeBalance || disabledUpgradeDrill}
          disabledBtnText={noGasBalance || noTradeBalance ? 'No Enough tokens' : undefined}
        />

        <UpgradeMinerDialog
          tutorialKey={`mining-upgrade-storage-dialog`}
          open={upgradeStorage}
          onClose={() => setUpgradeStorage(false)}
          title={t('Storage')}
          subTitle={t('Increased storage allows you to hold a larger amount of mined', {
            coreSymbol,
          })}
          price={storagePrice}
          priceIcon={coreToken.data?.image}
          upgradeSymbol={upgradeSymbol}
          current={{
            level: sizeLevel + 1,
            image: size,
            info: upgradeStorageInfo,
          }}
          next={{
            level: sizeLevel + 2,
            image: nextSize,
            info: upgradeStorageNextInfo,
          }}
          onUpgrade={handleUpgradeStorage}
          loading={
            miningUpgrade.loading ||
            miningUpgrade.recipe.isLoading ||
            payableInfo.cost.loading ||
            loading
          }
          disabled={noGasBalance || noTradeBalance || disabledUpgradeStorage}
          disabledBtnText={noGasBalance || noTradeBalance ? 'No Enough tokens' : undefined}
        />

        <UpgradeMinerDialog
          open={upgradeRefStorage}
          onClose={() => setUpgradeRefStorage(false)}
          title={t('Referral Storage')}
          subTitle={t(
            'Increased referral storage allows you to hold a larger amount of referral rewards'
          )}
          price={refPrice}
          priceIcon={coreToken.data?.image}
          upgradeSymbol={upgradeSymbol}
          current={{
            level: referralLevel + 1,
            image: ref,
            info: upgradeRefInfo,
          }}
          next={{
            level: referralLevel + 2,
            image: nextRef,
            info: upgradeRefNextInfo,
          }}
          onUpgrade={handleUpgradeRef}
          loading={
            miningUpgrade.loading ||
            miningUpgrade.recipe.isLoading ||
            payableInfo.cost.loading ||
            loading
          }
          disabled={noGasBalance || noTradeBalance || disabledUpgradeRef}
          disabledBtnText={noGasBalance || noTradeBalance ? 'No Enough tokens' : undefined}
        />

        <UpgradeMinerDialog
          open={upgradeDrone}
          onClose={() => setUpgradeDrone(false)}
          title={t('Drone Storage')}
          subTitle={t(
            'Increased drone storage allows you to hold a larger amount of drone rewards'
          )}
          price={dronePrice}
          priceIcon={coreToken.data?.image}
          upgradeSymbol={coreSymbol}
          current={{
            level: droneLevel + 1,
            image: droneImage,
            info: upgradeDroneInfo,
          }}
          next={{
            level: droneLevel + 2,
            image: droneNextImage,
            info: upgradeDroneNextInfo,
          }}
          onUpgrade={handleUpgradeDrone}
          loading={
            miningUpgrade.loading ||
            miningUpgrade.recipe.isLoading ||
            payableInfo.cost.loading ||
            loading
          }
          disabled={noGasBalance || noTradeBalance || disabledUpgradeDrone}
          disabledBtnText={noGasBalance || noTradeBalance ? 'No Enough tokens' : undefined}
        />

        <MiningBuildTutorial start={!noGasBalance && !noTradeBalance && isStorageUpgrade} />
      </Container>
    </div>
  );
}

export default Build;
