import Page from './Build';
import React from 'react';
import SlotsProvider from 'lib/Inventory/context/slots/Slots.provider.tsx';

const WrappedPage = () => {
  return (
    <SlotsProvider>
      <Page />
    </SlotsProvider>
  );
};

export default WrappedPage;
