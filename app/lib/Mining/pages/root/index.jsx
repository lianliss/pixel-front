import React from 'react';
import NftProvider from 'lib/NFT/context/Nft.provider.tsx';
import Mining from './Mining.tsx';
import AbProvider from 'lib/ab/context/ab.provider.tsx';

function MiningWrap() {
  return (
    <AbProvider>
      <NftProvider>
        {/*<TutorialForClaimStorageProvider started>*/}
        <Mining />
        {/*</TutorialForClaimStorageProvider>*/}
      </NftProvider>
    </AbProvider>
  );
}

export default MiningWrap;
