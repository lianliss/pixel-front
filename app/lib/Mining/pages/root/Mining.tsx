import React from 'react';
import styles from './Mining.module.scss';
import { Chain, FLARE } from 'services/multichain/chains';
import MinersSlider from 'lib/Mining/components/MinersSlider/MinersSlider.tsx';
import { FlareMiner } from 'lib/Mining/components/FlareMiner/FlareMiner';
import { MiningNavigation } from 'lib/Mining/components/MiningNavigation/MiningNavigation';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import NotAvailableChain from '../../../../shared/blocks/NotAvailableChain/NotAvailableChain.tsx';
import { classNames } from '../../../../shared/lib/classNames.ts';
import {
  BOXES_CHEST_CHAINS,
  DAILY_CHEST_CHAINS,
  DRONE_CHAINS,
  IS_DAILY_CHEST_ENABLED,
  IS_DECOR_ENABLED,
  IS_SHOP,
  IS_WEEKLY_CHEST_ENABLED,
  WEEKLY_CHEST_CHAINS,
} from '@cfg/app.ts';
import DailyBoxCard from '../../../Chests/components/DailyBoxCard/DailyBoxCard.tsx';
import { useUserInfo } from 'lib/User/hooks/useUserInfo.ts';
import { getDefaultChainImage } from '@cfg/image.ts';
import { usePayable } from 'lib/Mining/hooks/usePayable.ts';
import { MiningTutorial } from 'lib/Mining/components/MiningTutorial/MiningTutorial.tsx';
import { DynamicDecor } from '../../../../shared/decor/DynamicDecor.tsx';
import Box from '@mui/material/Box';
import { MinerDroneCardUi } from 'lib/MinerDrone/components/MinerDroneCard/MinerDroneCard.ui.tsx';
import { MinerDroneCard } from 'lib/MinerDrone/components/MinerDroneCard/MinerDroneCard.tsx';
import WeeklyBoxCard from 'lib/Chests/components/WeeklyBoxCard/WeeklyBoxCard.tsx';
import Container from '@mui/material/Container';
import {
  CHEST_SALE_CONTRACT_ID,
  isChestSaleEnabled,
} from 'lib/Chests/api-contract/contract.constants.ts';
import ChestSales from 'lib/Chests/components/ChestSales/ChestSales.tsx';
import { ShopStarters } from 'lib/Shop/components/ShopStarters/ShopStarters.tsx';

function Mining() {
  const account = useAccount();
  const { chainId } = account;

  const userInfo = useUserInfo({
    address: account.accountAddress,
  });

  const isWeekly = WEEKLY_CHEST_CHAINS.includes(chainId);
  const isDaily = DAILY_CHEST_CHAINS.includes(chainId);
  const isDrone = DRONE_CHAINS.includes(chainId);
  const isChestSale = isChestSaleEnabled(chainId);

  useBackAction('/wallet');

  // React.useEffect(() => {
  //   void getDevicesPublic();
  // }, []);

  if (chainId === FLARE) {
    return <FlareMiner />;
  }

  if (!MINER_CONTRACT_ID[chainId]?.core) {
    return (
      <>
        <ChainSwitcher chains={[Chain.SONGBIRD, Chain.SKALE, Chain.UNITS]} />
        <NotAvailableChain title='Miner not available' chainId={Chain.SONGBIRD} />
      </>
    );
  }

  return (
    <div
      className={classNames(styles.mining, {}, ['scrollable-element'])}
      style={{ backgroundImage: `url(${getDefaultChainImage(account.chainId)})` }}
    >
      <Container maxWidth='xs'>
        <ChainSwitcher />

        <MinersSlider karma={userInfo.data?.karma} />

        {isDrone && (
          <Box sx={{ mt: 1 }}>
            <MinerDroneCard />
          </Box>
        )}

        {/*{isWeekly && <WeeklyBox noGas={noGas} />}*/}
        {isDaily && (
          <DailyBoxCard sx={{ mt: 1 }} soon={!IS_DAILY_CHEST_ENABLED.includes(account.chainId)} />
        )}
        {isWeekly && (
          <WeeklyBoxCard sx={{ mt: 1 }} soon={!IS_WEEKLY_CHEST_ENABLED.includes(account.chainId)} />
        )}
        {isChestSale && <ChestSales sx={{ mt: 2 }} />}

        <MiningNavigation />

        <MiningTutorial start />
      </Container>
    </div>
  );
}

export default Mining;
