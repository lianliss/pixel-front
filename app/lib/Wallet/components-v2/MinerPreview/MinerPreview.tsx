import React from 'react';
import styles from './MinerPreview.module.scss';
import logo from 'styles/svg/logo_icon.svg';

import { useNavigate } from 'react-router-dom';
import routes from 'const/routes.tsx';
import clsx from 'clsx';
import { useAb } from 'lib/ab/context/ab.context.ts';
import { IS_DEVELOP, IS_STAGE } from '@cfg/config.ts';
import MiningProgress from 'lib/Mining/components/MiningProgress/MiningProgress.tsx';
import get from 'lodash/get';
import { Button } from '@ui-kit/Button/Button.tsx';
import Paper, { PaperProps } from '@mui/material/Paper';
import { useAccount } from '@chain/hooks/useAccount.ts';
import {
  IMinerInfo,
  MINER_CONTRACT_ID,
  MINER_INFO,
} from 'lib/Mining/api-contract/miner.constants.ts';
import { useMining } from 'lib/Mining/hooks/useMining.ts';
import { useCoreToken } from '@chain/hooks/useCoreToken.ts';
import { ThemeProvider } from '../../../../shared/theme/theme.ts';
import { useTranslation } from 'react-i18next';
import { Timer } from '@ui-kit/Timer/Timer.tsx';
import { PAYABLE_CONTRACT_ID } from 'lib/Wallet/api-contract/wallet.contracts.ts';
import { styled } from '@mui/material/styles';
import { CornerDecor } from '../../../../shared/decor/CornerDecor.tsx';
import { IS_DECOR_ENABLED } from '@cfg/app.ts';
import MinerDroneProgressUi from 'lib/MinerDrone/components/MinerDroneProgress/MinerDroneProgress.ui.tsx';
import MinerDroneProgress from 'lib/MinerDrone/components/MinerDroneProgress/MinerDroneProgress.tsx';
import { MINER_DRONE_CONTRACT_ID } from 'lib/MinerDrone/api-contract/miner-drone.constants.ts';

const StyledBox = styled(Paper)(({ theme }) => ({
  border: 'solid 1px',
  borderColor: theme.palette.primary.main,
  position: 'relative',
}));

type Props = { disabled?: boolean; startDate?: Date } & PaperProps;

function MinerProgressWrapper(props: {
  minerAddress: string;
  symbol: string;
  icon: any;
  title: string;
  status?: boolean;
  sizeLevel?: number;
  speedLevel?: number;
  requirements?: { speedLevel?: number };
  type?: 'root' | 'second' | 'float';
}) {
  const { minerAddress, status, requirements, title, symbol, icon, sizeLevel, speedLevel } = props;

  const miningApi = useMining({
    minerAddress,
    type: props.type,
  });

  const { isRefClaiming, isClaiming, isMiningDisabled, isMiningLoading } = miningApi;
  const {
    mined,
    sizeLimit,
    rewardPerSecond,
    claimTimestamp,
    lastGetTimestamp,
    extra: { refStorage, refLimit } = {},
  } = miningApi.data || {};

  return (
    <MiningProgress
      minerAddress={minerAddress}
      status={status}
      requirements={requirements}
      title={title}
      symbol={symbol}
      icon={icon}
      rewardPerSecond={rewardPerSecond}
      refStorage={refStorage}
      referral={false}
      refLimit={refLimit}
      requirementsData={requirements}
      isRefClaiming={isRefClaiming}
      isMiningDisabled={isMiningDisabled}
      isClaiming={isClaiming}
      isMiningLoading={isMiningLoading}
      sizeLimit={sizeLimit}
      claimTimestamp={claimTimestamp}
      lastGetTimestamp={lastGetTimestamp}
      mined={mined}
      sizeLevel={sizeLevel}
      speedLevel={speedLevel}
    />
  );
}

function MinerPreview(props: Props) {
  const { disabled, className, ...other } = props;

  const { t } = useTranslation('wallet');

  const ab = useAb();
  const account = useAccount();
  const { chainId } = account;
  const coreMinerAddress = MINER_CONTRACT_ID[chainId]?.core;
  const secondMiners = MINER_CONTRACT_ID[chainId]?.second || [];

  const isMinerDrone = !!MINER_DRONE_CONTRACT_ID[chainId];

  const miningApi = useMining({
    minerAddress: coreMinerAddress,
  });

  const { isRefClaiming, isClaiming, isMiningDisabled, isMiningLoading } = miningApi;
  const {
    mined,
    sizeLimit,
    rewardPerSecond,
    claimTimestamp,
    updatedAt: lastGetTimestamp,
    extra: { refStorage, refLimit } = {},
  } = miningApi.data || {};

  const navigate = useNavigate();
  const coreToken = useCoreToken();

  const secondMinersFiltered: IMinerInfo[] = React.useMemo(() => {
    return secondMiners
      .filter((minerAddress) => minerAddress in MINER_INFO)
      .map((minerAddress) => MINER_INFO[minerAddress])
      .filter((miner) => {
        if (miner.abKey && !IS_DEVELOP && !IS_STAGE) {
          return ab.isEnabled(miner.abKey);
        } else {
          return true;
        }
      });
  }, [chainId, ab.loaded]);

  const onMining = async () => {
    Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
    navigate(routes.walletMining.path);
  };

  return (
    <StyledBox elevation={0} variant='outlined' className={clsx(styles.root)} {...other}>
      {IS_DECOR_ENABLED && <CornerDecor position='right' />}

      <div className={styles.portfolio}>
        <MiningProgress
          symbol={get(coreToken.data?.symbol, 'symbol', 'PXLs')}
          icon={get(coreToken.data?.logoURI, 'logoURI', logo)}
          title={get(coreToken.data?.name, 'name', t('Pixel Storage'))}
          status
          rewardPerSecond={rewardPerSecond}
          refStorage={refStorage}
          referral={false}
          refLimit={refLimit}
          isRefClaiming={isRefClaiming}
          isMiningDisabled={isMiningDisabled}
          isClaiming={isClaiming}
          isMiningLoading={isMiningLoading}
          sizeLimit={sizeLimit}
          claimTimestamp={claimTimestamp}
          lastGetTimestamp={lastGetTimestamp}
          mined={mined}
        />
        {secondMinersFiltered.map((miner, index) => (
          <ThemeProvider key={`miner-${miner.contractAddress}`} theme={miner.themeConfig}>
            <MinerProgressWrapper
              symbol={miner.symbol}
              icon={miner.icon}
              title={miner.title}
              status
              secondIndex={index}
              minerAddress={miner.contractAddress}
              requirements={miner.requirements}
              type={miner.type}
            />
          </ThemeProvider>
        ))}

        {isMinerDrone && <MinerDroneProgress />}

        <div className={styles.portfolioActions}>
          <Button
            size='large'
            variant='contained'
            color='primary'
            fullWidth
            onClick={onMining}
            disabled={miningApi.noAccess}
            loading={miningApi.isAccessLoading}
          >
            {miningApi.noAccess ? 'No Access' : t('MINING')}
          </Button>
        </div>
      </div>
    </StyledBox>
  );
}

export default MinerPreview;
