import Dialog, { DialogProps } from '@mui/material/Dialog';
import RecoverWalletForm from 'app/lib/Wallet/RecoverWalletForm';

export function WalletRecoverDialog(props: Omit<DialogProps, 'children'>) {
  return (
    <Dialog {...props}>
      <RecoverWalletForm />
    </Dialog>
  );
}
