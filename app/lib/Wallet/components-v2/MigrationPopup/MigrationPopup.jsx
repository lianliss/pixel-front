import React from 'react';
import styles from './MigrationPopup.module.scss';
import { Spinner } from '@blueprintjs/core';
import WalletPopup from 'ui/WalletPopup/WalletPopup';

function MigrationPopup({ status }) {
  return (
    <WalletPopup className={styles.block}>
      <h2>Migrating to V5 contracts</h2>
      <p>{status}</p>
      <Spinner />
      <h3>PLEASE DO NOT CLOSE THE APP</h3>
      <p>Wait for the process to complete</p>
    </WalletPopup>
  );
}

export default MigrationPopup;
