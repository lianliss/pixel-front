import styles from './DelegateBlock.module.scss';
import toaster from 'services/toaster.tsx';
import React from 'react';

import { useTranslation } from 'react-i18next';
import { Button } from '@ui-kit/Button/Button.tsx';

export function DelegateBlock() {
  const { t } = useTranslation('wallet');

  return (
    <div className={styles.root}>
      <div className={styles.wrapper}>
        <div className={styles.portfolio}>
          <div className={styles.portfolioFlare}>
            <h2>{t('Delegate yor Flare to get reward')}</h2>
            <p>8% {t('APY + Reward Pool')}</p>
            <Button
              size='large'
              startIcon={
                <img
                  src={require('assets/svg/flare.svg')}
                  alt={'FLR'}
                  style={{ width: 24, height: 24 }}
                />
              }
              variant='contained'
              onClick={() => {
                Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
                toaster.warning('Coming soon...');
              }}
            >
              <span>{t('Delegate your FLARE')}</span>
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
