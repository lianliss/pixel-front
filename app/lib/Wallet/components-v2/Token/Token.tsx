import routes from '../../../../const/routes.tsx';
import { useNavigate } from 'react-router-dom';
import { useTokenPrice } from '@chain/hooks/useTokenPrice.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';
import WalletBlock from 'ui/WalletBlock/WalletBlock';
import styles from './Token.module.scss';
import getFinePrice from 'utils/getFinePrice';
import { Icon } from '@blueprintjs/core';
import React from 'react';
import Skeleton from '@mui/material/Skeleton';
import Paper from '@mui/material/Paper';

function Token({ address, logoURI, name, symbol, accountAddress }) {
  const route = routes.walletToken.path.replace(':symbol', address);
  const navigate = useNavigate();

  const price = useTokenPrice({
    address,
  });
  const balance = useBalance({
    address: accountAddress,
    token: address,
    skip: !accountAddress,
  });
  const priceRate = price.data;
  const balanceValue = balance.data?.formatted;

  return (
    <Paper
      variant='outlined'
      elevation={0}
      className={styles.token}
      sx={{ padding: '8px 16px' }}
      onClick={() => {
        navigate(route);
      }}
    >
      <div className={styles.tokenInfo}>
        <div className={styles.tokenInfoIcon}>
          <img src={logoURI} alt={symbol} />
        </div>
        <div className={styles.tokenInfoTitle}>
          <div className={styles.tokenInfoTitleSymbol}>{symbol}</div>
          <div className={styles.tokenInfoTitleName}>{name}</div>
        </div>
      </div>
      <div className={styles.tokenBalance}>
        <div className={styles.tokenBalanceText}>
          <div className={styles.tokenBalanceTextAmount}>
            {balance.isLoading && <Skeleton sx={{ width: 60 }} />}
            {!balance.isLoading && typeof balanceValue === 'number'
              ? getFinePrice(balanceValue)
              : ''}
          </div>
          <div className={styles.tokenBalanceTextValue}>
            {price.loading && <Skeleton sx={{ width: 40 }} />}
            {!balance.isLoading && !price.loading && typeof priceRate === 'number'
              ? `$${getFinePrice(priceRate)}`
              : balance.error
              ? 'failed..'
              : ''}
          </div>
        </div>
        <div className={styles.tokenBalanceAction}>
          <Icon icon={'chevron-right'} />
        </div>
      </div>
    </Paper>
  );
}

export default Token;
