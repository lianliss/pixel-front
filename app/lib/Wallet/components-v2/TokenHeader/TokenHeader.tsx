import React from 'react';
import styles from './TokenHeader.module.scss';
import getFinePrice from 'utils/getFinePrice';
import { ITokenData } from '@chain/hooks/useToken.ts';
import { Icon } from '@blueprintjs/core';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import Divider from '@mui/material/Divider';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

const StyledRoot = styled(Box)(({ theme }) => ({
  // backgroundColor: theme.palette.background.back2,
}));

type Props = { token: ITokenData | null; showBalance?: boolean; balance?: number; price?: number };

function TokenHeader({ token, balance, showBalance, price }: Props) {
  return (
    <StyledRoot className={styles.tokenHeader}>
      <div className={styles.tokenHeaderTitle}>
        <div className={styles.tokenHeaderTitleIcon}>
          <img src={token?.image} alt={token?.symbol} />
        </div>
        <Typography color='text.primary' className={styles.tokenHeaderTitleSymbol}>
          {token && (showBalance ? `${getFinePrice(balance)} ${token.name}` : token.name)}
        </Typography>
      </div>

      <Typography color='text.secondary' className={styles.tokenHeaderPrice}>
        1 {token?.symbol} ≈ ${getFinePrice(Number(price) || 0)}
      </Typography>
    </StyledRoot>
  );
}

export default TokenHeader;
