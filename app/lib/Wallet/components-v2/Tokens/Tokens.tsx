import React, { useEffect } from 'react';
import styles from './Tokens.module.scss';
import { useNavigate } from 'react-router-dom';
import orderBy from 'lodash-es/orderBy';
import slice from 'lodash-es/slice';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { DISPLAY_TOKENS, WRAP_TOKENS } from 'services/multichain/initialTokens';
import Token from 'lib/Wallet/components-v2/Token/Token.tsx';
import Alert from '@mui/material/Alert';
import { Chain, NETWORKS_DATA } from 'services/multichain/chains';
import { useTranslation } from 'react-i18next';
import Stack from '@mui/material/Stack';

function Tokens() {
  const account = useAccount();
  const { t } = useTranslation('wallet');

  const network = NETWORKS_DATA[account.chainId];
  const tokens = DISPLAY_TOKENS[account.chainId] || [];

  const sorted = React.useMemo(() => {
    return slice(orderBy(tokens, ['balance'], ['desc']), 0, 5);
  }, [tokens]);

  return (
    <Stack direction='column' gap={0.5} className={styles.tokens}>
      {account.chainId === Chain.SKALE && (
        <Alert severity='warning' variant='outlined' sx={{ mt: 2 }}>
          {t(
            `Make sure you only deposit Tokens via required network, If not, you risk losing your funds`,
            { network: network?.title || 'Current' }
          )}
        </Alert>
      )}

      {sorted.map((token, i) => {
        if (token.symbol === WRAP_TOKENS[account.chainId]?.symbol) {
          return <React.Fragment key={`token-${i}`} />;
        }

        return (
          <Token
            key={`token-${token.name}`}
            accountAddress={account.accountAddress}
            name={token.name}
            decimals={token.decimals}
            address={token.address || '0x0000000000000000000000000000000000000000'}
            balance={token.balance}
            logoURI={token.logoURI}
            symbol={token.symbol}
          />
        );
      })}
    </Stack>
  );
}

export default Tokens;
