import React from 'react';
import styles from './FreeGas.module.scss';
import useFreeGas from '../../hooks/useFreeGas.ts';
import WalletBlock from 'ui/WalletBlock/WalletBlock';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useToken } from '@chain/hooks/useToken.ts';
import { useTranslation } from 'react-i18next';

function FreeGas() {
  const { t } = useTranslation('wallet');
  const { claimLoading, claim, isFetched, data } = useFreeGas();

  const token = useToken({});

  return (
    <WalletBlock className={styles.token}>
      <div className={styles.tokenInfo}>
        <div className={styles.tokenInfoIcon}>
          <img src={token.data?.image} alt={token.data?.symbol} />
        </div>
        <div className={styles.tokenInfoTitle}>
          <div className={styles.tokenInfoTitleSymbol}>{t('Free gas')}</div>
          <div className={styles.tokenInfoTitleName}>
            {t('Claim free')} {token.data?.symbol}
          </div>
        </div>
      </div>
      <div className={styles.tokenBalance}>
        <Button
          onClick={claim}
          size='large'
          variant='contained'
          color='primary'
          loading={claimLoading || !isFetched}
          disabled={claimLoading || !isFetched || !data}
        >
          {t('Claim')}
        </Button>
      </div>
    </WalletBlock>
  );
}

export default FreeGas;
