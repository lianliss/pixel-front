'use strict';

import axios, { AxiosResponse } from 'axios';
import { IFindOraclePricesResponse } from 'lib/Wallet/api-server/oracle.types.ts';

export class OracleApi {
  url: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async find(): Promise<IFindOraclePricesResponse> {
    const { data } = await axios.get<
      { data: IFindOraclePricesResponse },
      AxiosResponse<{ data: IFindOraclePricesResponse }>
    >(`/api/oracle/prices`, {
      baseURL: this.url,
      params: { symbols: ['sgb', 'skl', 'pol', 'flr', 'unit0'].join(',') },
    });
    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const oracleApi = new OracleApi({ url: 'https://api.hellopixel.network' });
