export type IOraclePriceDto = {
  symbol: string;
  usd: number;
};

export type IFindOraclePricesDto = {
  arr: string[];
};

export type IFindOraclePricesResponse = IOraclePriceDto[];
