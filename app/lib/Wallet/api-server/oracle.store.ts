import { createEffect, createStore } from 'effector';
import { oracleApi } from 'lib/Wallet/api-server/oracle.api.ts';
import { useUnit } from 'effector-react';
import { Chain } from 'services/multichain/chains';
import { DEFAULT_TOKEN_PRICES, ORACLE_MAP_TOKENS } from 'lib/Wallet/api-server/oracle.constants.ts';
import { useMemo } from 'react';

// oracle prices

export const loadOraclePrices = createEffect(async (_: object) => {
  const res = await oracleApi.find();
  const result = res.reduce((acc, el) => ({ ...acc, [el.symbol.toLowerCase()]: el.usd }), {});

  return result;
});

export const tokenPricesStore = createStore<Record<Chain, Record<string, number>>>(
  DEFAULT_TOKEN_PRICES
).on(loadOraclePrices.doneData, (prev, prices) => {
  for (const symbol in prices) {
    const symbolEntries = ORACLE_MAP_TOKENS[symbol];

    for (const entry of symbolEntries) {
      if (!prev[entry.chain]) {
        prev[entry.chain] = {};
      }

      prev[entry.chain][entry.address] = prices[symbol];
    }
  }

  return prev;
});

export const useOracleTokenPrice = (payload: {
  chainId: number;
  address: string;
}): number | undefined => {
  const prices = useUnit(tokenPricesStore);

  return useMemo(() => {
    return prices[payload.chainId]?.[payload.address.toLowerCase()];
  }, [payload.chainId, payload.address, prices?.[payload.chainId]]);
};
