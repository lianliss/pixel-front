import { Chain } from 'services/multichain/chains';

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

export const ORACLE_MAP_TOKENS = {
  sgb: [{ chain: Chain.SONGBIRD, address: ZERO_ADDRESS }],
  flr: [{ chain: Chain.FLARE, address: ZERO_ADDRESS }],
  skl: [
    { chain: Chain.SKALE, address: '0xE0595a049d02b7674572b0d59cd4880Db60EDC50'.toLowerCase() },
    {
      chain: Chain.SKALE_TEST,
      address: '0x6c71319b1F910Cf989AD386CcD4f8CC8573027aB'.toLowerCase(),
    },
  ],
  pol: [{ chain: Chain.POLYGON_MAINNET, address: ZERO_ADDRESS }],
  unit0: [{ chain: Chain.UNITS, address: ZERO_ADDRESS }],
};

export const DEFAULT_TOKEN_PRICES = {
  [Chain.POLYGON_MAINNET]: {
    ['0xc2132D05D31c914a87C6611C10748AEb04B58e8F'.toLowerCase()]: 1,
  },
  [Chain.COSTON2]: {
    [ZERO_ADDRESS]: 0.01,
  },
  [Chain.SKALE_TEST]: {
    [ZERO_ADDRESS]: 0,
  },
  [Chain.SKALE]: {
    // [ZERO_ADDRESS]: 0,
    ['0x5f795bb52dac3085f578f4877d450e2929d2f13d']: 1,
  },
  [Chain.POLYGON_MAINNET]: {
    '0x3c499c542cef5e3811e1192ce70d8cc03d5c3359': 1,
    '0xc2132d05d31c914a87c6611c10748aeb04b58e8f': 1,
  },
};
