import React from 'react';
import MigrationPopup from 'lib/Wallet/components-v2/MigrationPopup/MigrationPopup';
import { useUnit } from 'effector-react';
import { isMigrationStore } from 'app/hooks/useMigrationToV5';

export const WalletContext = React.createContext();

function WalletProvider(props) {
  const migrationInfo = useUnit(isMigrationStore);

  return (
    <>
      {props.children}
      {!!migrationInfo.progress && <MigrationPopup status={migrationInfo.status} />}
    </>
  );
}

export default WalletProvider;
