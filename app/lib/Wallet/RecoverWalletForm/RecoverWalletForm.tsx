import React from 'react';
import styles from './Recover.module.scss';
import WalletBlock from 'ui/WalletBlock/WalletBlock';
import { Button as BPButton } from '@blueprintjs/core/lib/esm/components/button/buttons';
import { Input } from 'ui';

import { ethers } from 'ethers6';
import { useTranslation } from 'react-i18next';
import Container from '@mui/material/Container';
import { useConnect } from 'wagmi';
import { Button } from '@ui-kit/Button/Button';
import { pixelStorage } from '@chain/connectors/pixel.provider';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Paper from '@mui/material/Paper';
import { readFromClipboard } from 'utils/actions/readFromClipboard.ts';

function RecoverWalletForm() {
  const { connect, connectors } = useConnect();

  const [seed, setSeed] = React.useState('');
  const [privateKey, _setPrivateKey] = React.useState('');
  const [isSeed, setIsSeed] = React.useState(false);
  const [isPrivate, setIsPrivate] = React.useState(false);

  const { t } = useTranslation('wallet');

  const onRecover = () => {
    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
      const wallet = !!seed ? ethers.Wallet.fromPhrase(seed) : new ethers.Wallet(privateKey);

      pixelStorage.savePrivateKey(wallet.privateKey.toString());
      connect({ connector: connectors.find((el) => el.type === 'pixel') });
    } catch (error) {
      console.error('[onRecover]', error);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }
  };

  const onSeedPasteClick = async () => {
    Telegram.WebApp.HapticFeedback.impactOccurred('light');
    setSeed(await readFromClipboard());
  };

  const onPrivateKeyPasteClick = async () => {
    Telegram.WebApp.HapticFeedback.impactOccurred('light');
    pixelStorage.savePrivateKey(await readFromClipboard());
  };

  return (
    <div className={styles.recover} style={{ paddingBottom: 150 }}>
      <Typography variant='h1' align='center' fontWeight='bold'>
        Recover wallet
      </Typography>
      {!isPrivate && (
        <Paper variant='outlined' sx={{ py: 3, px: 2 }}>
          <Typography align='left' color='text.primary' sx={{ mb: 1 }}>
            {t('Seed phrase')}
          </Typography>
          <Input
            value={seed}
            placeholder={'words...'}
            onClick={() => {
              Telegram.WebApp.HapticFeedback.impactOccurred('light');
            }}
            indicator={
              <BPButton
                icon={t('clipboard')}
                onClick={onSeedPasteClick}
                minimal
                className={styles.recoverPaste}
              />
            }
            onTextChange={setSeed}
          />
        </Paper>
      )}
      {!isSeed && !isPrivate && (
        <Typography variant='h2' fontWeight='bold' align='center' sx={{ my: 3 }}>
          or
        </Typography>
      )}
      {!isSeed && (
        <Paper variant='outlined' sx={{ py: 3, px: 2 }}>
          <Typography align='left' color='textPrimary' sx={{ mb: 1 }}>
            {t('Private Key')}
          </Typography>
          <Input
            value={privateKey}
            placeholder={'0xA29b9...'}
            onClick={() => {
              Telegram.WebApp.HapticFeedback.impactOccurred('light');
            }}
            indicator={
              <BPButton
                icon={'clipboard'}
                onClick={onPrivateKeyPasteClick}
                minimal
                className={styles.recoverPaste}
              />
            }
            onTextChange={_setPrivateKey}
          />

          <Button
            variant='contained'
            color='primary'
            size='large'
            style={{ padding: 12, marginBlock: 12, width: '100%' }}
            onClick={() => {
              onRecover(false);
            }}
          >
            {t('Connect Wallet')}
          </Button>
        </Paper>
      )}
    </div>
  );
}

export default RecoverWalletForm;
