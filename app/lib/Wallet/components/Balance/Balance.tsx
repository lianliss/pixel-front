import React from 'react';
import styles from './Balance.module.scss';
import getFinePrice from 'utils/getFinePrice';
import { Icon } from '@blueprintjs/core';
import WalletBlock from 'ui/WalletBlock/WalletBlock';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useGasBalance } from '@chain/hooks/useGasBalance.ts';
import { useToken } from '@chain/hooks/useToken.ts';
import { useSumBalance } from '@chain/hooks/useSumBalance';
import { DISPLAY_TOKENS } from 'services/multichain/initialTokens';
import { useTranslation } from 'react-i18next';
import { Typography } from '@ui-kit/Typography/Typography';
import Paper, { PaperProps } from '@mui/material/Paper';
import { styled } from '@mui/material/styles';

const Root = styled(Paper)(() => ({
  padding: '8px 16px',
}));

function Balance(props: PaperProps) {
  const account = useAccount();
  const tokens = DISPLAY_TOKENS[account.chainId] || [];

  const { t } = useTranslation('wallet');

  const balanceValue = useSumBalance({
    address: account.accountAddress,
    skip: !account.isConnected,
    tokens: tokens
      .filter((token) => !!token.address)
      .map((token) => ({ address: token.address, decimals: token.decimals })),
  });
  const gasToken = useToken({
    skip: !account.isConnected,
  });
  const gasBalance = useGasBalance({
    address: account.accountAddress,
    skip: !account.isConnected,
  });

  return (
    <Root variant='outlined' elevation={0} {...props}>
      <div className={styles.balance}>
        <div className={styles.balanceLeft}>
          <Typography variant='subtitle2' sx={{ fontSize: 12 }} color='text.secondary'>
            {t('Total Balance')}
          </Typography>
          <Typography
            variant='subtitle1'
            color='text.primary'
            fontWeight='bold'
            sx={{ fontSize: 24 }}
          >
            ${getFinePrice(balanceValue.data.value)}
          </Typography>
        </div>
        <div className={styles.balanceRight}>
          <Typography variant='caption' sx={{ fontSize: 14 }} className={styles.balanceGas}>
            {gasBalance.data ? getFinePrice(gasBalance.data.formatted) : ''}{' '}
            {gasToken.data?.symbol || ''}
          </Typography>
          <Icon icon={'flame'} />
        </div>
      </div>
    </Root>
  );
}

export default Balance;
