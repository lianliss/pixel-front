import React from 'react';
import styles from './WalletNavigation.module.scss';
import routes from 'const/routes.tsx';
import { useNavigate } from 'react-router-dom';

import clsx from 'clsx';

import MarketIcon from './components/market';
import ExchangeIcon from './components/exchange';
import NftIcon from './components/nft';
import SoulIcon from '../../../../shared/ui/icon/common/digital-soul';
import { IconButton } from '@ui-kit/IconButton/IconButton.tsx';
import Paper, { PaperProps } from '@mui/material/Paper';
import { isMarketAvailable } from 'lib/Marketplace/api-contract/contracts.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain } from 'services/multichain/chains';
import { useTranslation } from 'react-i18next';
import { isSlotsAvailable } from 'lib/Inventory/api-contract/slots.constants.ts';
import { BRIDGE_CHAINS, EXCHANGE_CHAINS } from '@cfg/app.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import useIsTonChain from 'services/ton/hooks/useIsTonChain.tsx';

function WalletNavigation(props: PaperProps) {
  const { className, ...other } = props;

  const { t } = useTranslation('wallet');

  const account = useAccount();
  const { isTonChain } = useIsTonChain();
  const chainId = isTonChain ? Chain.TON : account.chainId;

  const navigate = useNavigate();

  const icons = [
    {
      text: t('Marketplace'),
      Icon: MarketIcon,
      route: routes.nftMarket,
      disabled: !isMarketAvailable(chainId),
    },
    {
      text: chainId === Chain.POLYGON_MAINNET ? 'Bridge' : t('Exchange'),
      Icon: ExchangeIcon,
      route: chainId === Chain.POLYGON_MAINNET ? routes.bridge : routes.exchange,
      disabled: ![...EXCHANGE_CHAINS, ...BRIDGE_CHAINS].includes(chainId),
    },
    {
      text: t('Inventory'),
      Icon: NftIcon,
      route: routes.inventory,
      disabled: !isSlotsAvailable(chainId),
    },
    {
      text: t('Digital Soul'),
      Icon: SoulIcon,
      route: routes.digitalSoul,
    },
  ];

  return (
    <Paper variant='outlined' className={clsx(styles.root, className)} {...other}>
      {icons.map(({ text, Icon, route, disabled: iconDisabled }, index) => {
        const id = `wallet-icon-${index}`;
        const { path } = route;
        let disabled = route.disabled || iconDisabled;

        const handleClick = () => {
          if (!disabled) {
            Telegram.WebApp.HapticFeedback.impactOccurred('medium');
            navigate(path);
          }
        };

        return (
          <div className={clsx(styles.item, { [styles.disabled]: disabled })} key={id}>
            <IconButton
              variant='outlined'
              color='primary'
              disabled={disabled}
              size='large'
              onClick={handleClick}
            >
              <Icon />
            </IconButton>
            <Typography
              color='text.primary'
              variant='caption'
              sx={{ fontSize: 11 }}
              className={styles.label}
            >
              {text}
            </Typography>
          </div>
        );
      })}
    </Paper>
  );
}

export default WalletNavigation;
