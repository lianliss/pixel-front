import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

export default function Icon() {
  return (
    <SvgIcon
      width='24'
      height='24'
      viewBox='0 0 44 48'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M21.9995 9.8457L34.2571 16.9226V31.0765L21.9995 38.1534L9.74196 31.0765V16.9226L21.9995 9.8457Z'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M13 29V19L17.5 31.5V16.5'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M21 33V14L24 15.5'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M21 24L24 24'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M27 17.5L31 20'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M29 19V30'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M37.0379 15.1543L33.9417 16.9756M6.96094 32.8466C8.00603 32.2318 9.00558 31.6439 9.96863 31.0774'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M6.96019 15.1543L9.96788 16.9235M37.0371 32.8466C35.9922 32.2319 34.9926 31.6439 34.0294 31.0774'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M22 6.30762C22 7.57702 22 8.74395 22 9.84608M22 41.6922V38.1538'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <circle
        cx='21.9995'
        cy='3.65385'
        r='2.65385'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <circle
        cx='39.6204'
        cy='13.8274'
        r='2.65385'
        transform='rotate(60 39.6204 13.8274)'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <circle
        cx='39.6189'
        cy='34.1735'
        r='2.65385'
        transform='rotate(120 39.6189 34.1735)'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <circle
        cx='21.9995'
        cy='44.3462'
        r='2.65385'
        stroke='currentcolor'
        strokeWidth='2'
        fill='none'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <circle
        cx='4.37823'
        cy='34.1731'
        r='2.65385'
        transform='rotate(60 4.37823 34.1731)'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <circle
        cx='4.37868'
        cy='13.8268'
        r='2.65385'
        transform='rotate(120 4.37868 13.8268)'
        fill='none'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </SvgIcon>
  );
}
