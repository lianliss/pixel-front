import React from 'react';
import styles from './CopyAddress.module.scss';
import toaster from 'services/toaster.tsx';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { Icon } from '@blueprintjs/core';

import ChainSwitcher from 'app/widget/ChainSwitcher/ChainSwitcher.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import ProjectGuideIcon from '../../../../shared/ui/icon/common/porject-guide';
import { Typography } from '@ui-kit/Typography/Typography';

function CopyAddress() {
  const { accountAddress, chainId, isConnected } = useAccount();
  // const navigate = useNavigate();

  // const questUserStats = useUnit(questUserStatsStore);

  // const uncompletedQuests = React.useMemo(() => {
  //   return questUserStats?.options.reduce((acc, el) => acc + el.amount, 0) || 0;
  // }, [questUserStats]);
  //
  // React.useEffect(() => {
  //   if (chainId && isConnected) {
  //     loadQuestUserStats({ address: accountAddress, chainId })
  //       .then()
  //       .catch(() => {});
  //   }
  // }, [chainId, isConnected]);

  // const redirectToQuests = () => navigate('/wallet/quests');

  return (
    <div className={styles.copyAddress}>
      {/*<div id='#GoToQuestsScreenTarget' className={styles.quests} onClick={redirectToQuests}>*/}
      {/*  <img src={questImage} alt='' />*/}
      {/*  {uncompletedQuests > 0 && <div>{uncompletedQuests}</div>}*/}
      {/*</div>*/}

      <CopyToClipboard
        text={accountAddress}
        className={styles.copyAddressContent}
        onCopy={() => {
          Telegram.WebApp.HapticFeedback.impactOccurred('medium');

          typeof toaster.show === 'function' &&
            toaster.show({
              intent: 'warning',
              message: <>Address copied to clipboard</>,
              icon: 'clipboard',
            });
        }}
      >
        <div>
          <Typography color='text.primary' className={styles.copyAddressText}>
            {accountAddress
              ? accountAddress.slice(0, 6) + '...' + accountAddress.slice(accountAddress.length - 4)
              : ''}
          </Typography>
          <Icon icon={'duplicate'} size={14} />
        </div>
      </CopyToClipboard>

      <div style={{ display: 'flex', gap: 16, alignItems: 'center' }}>
        <div
          onClick={() =>
            Telegram.WebApp.openLink(
              'https://telegra.ph/Navigation-and-Guide-to-Hello-Pixel-08-10',
              {}
            )
          }
          style={{ display: 'block' }}
        >
          <ProjectGuideIcon style={{ fontSize: 30 }} />
        </div>

        <ChainSwitcher isRelative />
      </div>
    </div>
  );
}

export default CopyAddress;
