import React from 'react';
import Wallet from './Wallet.tsx';
import AbProvider from 'lib/ab/context/ab.provider.tsx';
import useIsTonChain from 'services/ton/hooks/useIsTonChain.tsx';
import TonWallet from 'lib/Wallet/pages/TonWallet/TonWallet.tsx';

function WalletWrap() {
  const { isTonChain } = useIsTonChain();

  return (
    <AbProvider>
      {/*<TutorialForGettingGasProvider started>*/}
      {isTonChain ? <TonWallet /> : <Wallet />}
      {/*</TutorialForGettingGasProvider>*/}
    </AbProvider>
  );
}

export default WalletWrap;
