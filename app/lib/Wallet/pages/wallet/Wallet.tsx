import React from 'react';
import styles from './Wallet.module.scss';
import { Loading } from 'utils/async/load-module';
import CopyAddress from 'lib/Wallet/components/CopyAddress/CopyAddress';
import Balance from 'lib/Wallet/components/Balance/Balance.tsx';
import WalletNavigation from 'lib/Wallet/components/WalletNavigation/WalletNavigation.tsx';
import Tokens from 'lib/Wallet/components-v2/Tokens/Tokens.tsx';
import { WalletContext } from 'lib/Wallet/context/WalletProvider';
import MinerPreview from 'lib/Wallet/components-v2/MinerPreview/MinerPreview.tsx';
import { Chain } from 'services/multichain/chains';
import { DelegateBlock } from 'lib/Wallet/components-v2/DelegateBlock/DelegateBlock.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { BuyTokenForStars } from 'lib/TelegramStars/components/ByTokenForStars/BuyTokenForStars.tsx';
import { IS_DEVELOP, IS_STAGE, IS_TELEGRAM } from '@cfg/config.ts';
import FreeGas from 'lib/Wallet/components-v2/FreeGas/FreeGas.tsx';

import { useTranslation } from 'react-i18next';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { isMiningEnabled } from 'lib/Mining/utils/enabled.constants.ts';
import { getDefaultChainImage } from '@cfg/image.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { IS_DECOR_ENABLED, IS_SHOP } from '@cfg/app.ts';
import { DynamicDecor } from '../../../../shared/decor/DynamicDecor.tsx';
import { QuestsCard } from 'lib/Quests/components/QuestsCard/QuestsCard.tsx';
import Container from '@mui/material/Container';
import { ShopStarters } from 'lib/Shop/components/ShopStarters/ShopStarters.tsx';
import { isTester } from '@cfg/testers.ts';

const LazyCreateWallet = React.lazy(() => import('lib/Wallet/pages/signup/SignUp.tsx'));

function Wallet() {
  const { isConnected, chainId, accountAddress } = useAccount();

  // const { isKeyLoading } = React.useContext(WalletContext) || {};
  const { t, i18n } = useTranslation('wallet');

  // if (isKeyLoading) {
  //   return (
  //     <div className={styles.loading}>
  //       <Loading text={t('Initializing wallet')} />
  //     </div>
  //   );
  // }

  // if (!isConnected) {
  //   return <div className={styles.loading}>{!isConnected && <LazyCreateWallet />}</div>;
  // }

  // const isShopTest = isTester(accountAddress);

  const isMiner = !!MINER_CONTRACT_ID[chainId]?.core;
  const isFreeGas = [Chain.SKALE_TEST].includes(chainId);
  const isDelegate = chainId === Chain.FLARE;
  const isStartsAvailable = IS_TELEGRAM && [Chain.SONGBIRD, Chain.SKALE].includes(chainId);

  return (
    <>
      <div
        className={styles.wallet}
        style={{ backgroundImage: `url(${getDefaultChainImage(chainId)})`, position: 'relative' }}
      >
        <Container maxWidth='xs'>
          <CopyAddress />

          <Balance sx={{ mb: 1 }} />

          <WalletNavigation sx={{ mb: 1, mt: 0 }} />

          {/*{chainId === 19 && <Alert severity='warning' variant='filled' className={styles.walletPortfolioHelp}>The marketplace will be opened in <Timer date={new Date('2024-08-31T15:00:00')}/></Alert>}*/}

          {isMiner && (
            <div>
              <Typography color='text.primary' align='center' fontWeight='bold' sx={{ mb: 1 }}>
                {t('Miner')}
              </Typography>
              {/*{chainId === 1291 && <Alert severity='warning' variant='filled' className={styles.walletPortfolioHelp}>A snapshot of the artifact*/}
              {/*  parameters will be made via <Timer date={new Date('2024-07-22T18:00:00')}/></Alert>}*/}
              <MinerPreview />
            </div>
          )}
          {isDelegate && <DelegateBlock />}

          {isFreeGas && <FreeGas />}

          {isStartsAvailable && <BuyTokenForStars />}


          <div>
            <Typography
              color='text.primary'
              align='center'
              fontWeight='bold'
              sx={{ mb: 0.5, mt: 1 }}
            >
              {t('Unpassed quests')}
            </Typography>

            <QuestsCard />
          </div>

          {IS_SHOP && <ShopStarters sx={{ mt: 2 }} />}

          <div>
            <Typography
              color='text.primary'
              align='center'
              fontWeight='bold'
              sx={{ mb: 0.5, mt: 1 }}
            >
              {t('Tokens')}
            </Typography>

            <Tokens />
          </div>
        </Container>
      </div>
    </>
  );
}

export default Wallet;
