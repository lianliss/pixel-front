import React from 'react';
import styles from './Token.module.scss';
import { useMatch, useNavigate } from 'react-router-dom';

import getFinePrice from 'utils/getFinePrice';

import TokenHeader from 'lib/Wallet/components-v2/TokenHeader/TokenHeader.tsx';

import { useToken } from '@chain/hooks/useToken.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import Paper from '@mui/material/Paper';
import { useTokenPrice } from '@chain/hooks/useTokenPrice.ts';
import { Chain } from 'services/multichain/chains';
import ExchangeIcon from 'lib/Marketplace/components/icons/ExchangeIcon';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import KeyboardDoubleArrowUpIcon from '@mui/icons-material/KeyboardDoubleArrowUp';
import routes from '../../../../const/routes.tsx';
import { useTranslation } from 'react-i18next';
import { getDefaultChainImage } from '@cfg/image.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Divider from '@mui/material/Divider';
import Container from '@mui/material/Container';

function Token() {
  const account = useAccount();

  const { t } = useTranslation('common');

  const navigate = useNavigate();
  const routePath = routes.walletToken.path;
  const match = useMatch(routePath);
  const { symbol: address } = match.params;

  const tokenInfo = useToken({
    address,
  });
  const balanceData = useBalance({
    address: account.accountAddress,
    token: address,
    skip: !account.isConnected,
  });

  const tokenPrice = useTokenPrice({
    address,
  });

  const icons = [
    // {
    //   text: 'P2P',
    //   Icon: P2pIcon,
    //   route: routes.p2p,
    // },
    ...(account.chainId === Chain.FLARE
      ? [
          {
            text: 'Exchange',
            Icon: ExchangeIcon,
            route: routes.exchange,
          },
        ]
      : []),
    {
      text: t('Transfer'),
      Icon: KeyboardDoubleArrowUpIcon,
      route: routes.walletTransfer,
    },
  ];

  useBackAction('/wallet');

  return (
    <div
      className={styles.token}
      style={{ backgroundImage: `url(${getDefaultChainImage(account.chainId)})` }}
    >
      <Container maxWidth='xs'>
        <TokenHeader
          token={tokenInfo.data}
          amount={balanceData.data?.formatted}
          price={tokenPrice.data}
        />

        <Divider />

        <div className={styles.tokenContainer}>
          <div className={styles.tokenBalance}>
            <Typography color='text.primary' fontWeight='bold' sx={{ fontSize: 40 }}>
              {getFinePrice(balanceData.data?.formatted || 0)}
            </Typography>
            <Typography color='text.secondary'>
              ≈ $
              {tokenPrice.data && balanceData.isFetched
                ? getFinePrice(tokenPrice.data * balanceData.data?.formatted)
                : 0}
            </Typography>
          </div>
          <div className={styles.tokenIcons}>
            {icons.map(({ text, Icon, route }, index) => {
              const { path, disabled } = route;
              const classNames = [styles.tokenIcon];
              if (disabled) {
                classNames.push('disabled');
              }
              return (
                <Paper
                  variant='outlined'
                  className={classNames.join(' ')}
                  onClick={() => {
                    if (!disabled) {
                      Telegram.WebApp.HapticFeedback.impactOccurred('medium');
                      navigate(path.replace(':symbol', address));
                    }
                  }}
                  key={index}
                >
                  <div className={styles.tokenIconImageWrap}>
                    <Icon />
                  </div>
                  <div className={styles.tokenIconTitle}>{text}</div>
                </Paper>
              );
            })}
          </div>
        </div>
      </Container>
    </div>
  );
}

export default Token;
