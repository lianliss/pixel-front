import React from 'react';
import styles from './TonWallet.module.scss';
import { getDefaultChainImage } from '@cfg/image.ts';
import { Chain } from 'services/multichain/chains.js';
import Container from '@mui/material/Container';
import CopyTONAddress from 'lib/Wallet/pages/TonWallet/components/CopyTONAddress/CopyTONAddress.jsx';
import TONBalance from 'lib/Wallet/pages/TonWallet/components/TONBalance/TONBalance.tsx';
import WalletNavigation from 'lib/Wallet/components/WalletNavigation/WalletNavigation.tsx';
import BuyTonForStars from 'lib/Wallet/pages/TonWallet/components/BuyTonForStars/BuyTonForStars.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useTranslation } from 'react-i18next';
import TonTokens from 'lib/Wallet/pages/TonWallet/components/TonTokens/TonTokens.tsx';

export function TonWallet() {
  const chainId = Chain.TON;
  const { t, i18n } = useTranslation('wallet');

  return (
    <>
      <div
        className={styles.wallet}
        style={{ backgroundImage: `url(${getDefaultChainImage(chainId)})`, position: 'relative' }}
      >
        <Container maxWidth='xs'>
          <CopyTONAddress />
          <TONBalance sx={{ mb: 1 }} />
          <WalletNavigation sx={{ mb: 1, mt: 0 }} />
          <BuyTonForStars />
          <div>
            <Typography
              color='text.primary'
              align='center'
              fontWeight='bold'
              sx={{ mb: 0.5, mt: 1 }}
            >
              {t('Tokens')}
            </Typography>

            <TonTokens />
          </div>
        </Container>
      </div>
    </>
  );
}

export default TonWallet;
