import React from 'react';
import styles from './TONBalance.module.scss';
import getFinePrice from 'utils/getFinePrice';
import { Icon } from '@blueprintjs/core';
import { Typography } from '@ui-kit/Typography/Typography';
import Paper, { PaperProps } from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import useTonBalance from 'services/ton/hooks/useTonBalance.tsx';
import useTonPrice from 'services/ton/hooks/useTonPrice.tsx';
import { useTranslation } from 'react-i18next';

const Root = styled(Paper)(() => ({
  padding: '8px 16px',
}));

function TONBalance(props: PaperProps) {
  const balance = useTonBalance();
  const price = useTonPrice();

  const balanceValue = balance.data * price.data;

  const { t } = useTranslation('wallet');

  return (
    <Root variant='outlined' elevation={0} {...props}>
      <div className={styles.balance}>
        <div className={styles.balanceLeft}>
          <Typography variant='subtitle2' sx={{ fontSize: 12 }} color='text.secondary'>
            {t('Total Balance')}
          </Typography>
          <Typography
            variant='subtitle1'
            color='text.primary'
            fontWeight='bold'
            sx={{ fontSize: 24 }}
          >
            ${getFinePrice(balanceValue)}
          </Typography>
        </div>
        <div className={styles.balanceRight}>
          <Typography variant='caption' sx={{ fontSize: 14 }} className={styles.balanceGas}>
            {getFinePrice(balance.data)} TON
          </Typography>
          <Icon icon={'flame'} />
        </div>
      </div>
    </Root>
  );
}

export default TONBalance;
