import React, { useEffect } from 'react';
import styles from './TonTokens.module.scss';
import orderBy from 'lodash-es/orderBy';
import slice from 'lodash-es/slice';
import { useTranslation } from 'react-i18next';
import Stack from '@mui/material/Stack';
import useTonBalance from 'services/ton/hooks/useTonBalance.tsx';
import useTonPrice from 'services/ton/hooks/useTonPrice.tsx';
import TONImage from 'assets/svg/ton_symbol.svg';
import TONToken from 'lib/Wallet/pages/TonWallet/components/TONToken/TONToken.tsx';

function TonTokens() {
  const { t } = useTranslation('wallet');
  const balance = useTonBalance();
  const price = useTonPrice();

  const tokens = [
    {
      name: 'Toncoin',
      symbol: 'TON',
      balance: balance,
      logoURI: TONImage,
      decimals: 9,
      price: price,
      disabled: true,
    },
  ];

  const sorted = React.useMemo(() => {
    return slice(orderBy(tokens, ['balance'], ['desc']), 0, 5);
  }, [tokens]);

  return (
    <Stack direction='column' gap={0.5} className={styles.tokens}>
      {sorted.map((token, i) => {
        return (
          <TONToken
            name={token.name}
            decimals={token.decimals}
            balance={token.balance}
            logoURI={token.logoURI}
            symbol={token.symbol}
            price={token.price}
            disabled={token.disabled}
          />
        );
      })}
    </Stack>
  );
}

export default TonTokens;
