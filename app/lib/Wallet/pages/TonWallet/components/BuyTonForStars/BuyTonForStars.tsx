import React from 'react';
import styles from './styles.module.scss';
import { BuyTokenDialog } from 'lib/TelegramStars/components/BuyTokenDialog/BuyTokenDialog.tsx';
import { BuyTokenCard } from 'lib/TelegramStars/components/BuyTokenCard/BuyTokenCard.tsx';

type Props = {};

export const BuyTonForStars = (props: Props) => {
  return (
    <div className={styles.cards__wrapper}>
      <BuyTokenCard
        handler={() => {}}
        image={require('assets/svg/ton_symbol.svg')}
        symbol={'TON'}
        disabled={true}
      />
      <BuyTokenDialog
        onClose={() => {}}
        isOpen={false}
        onBuy={() => {}}
        disabled={true}
        symbol={'TON'}
        options={[]}
      />
    </div>
  );
};

export default BuyTonForStars;
