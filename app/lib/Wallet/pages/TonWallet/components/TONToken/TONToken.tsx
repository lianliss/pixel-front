import styles from './TONToken.module.scss';
import getFinePrice from 'utils/getFinePrice';
import { Icon } from '@blueprintjs/core';
import React from 'react';
import Skeleton from '@mui/material/Skeleton';
import Paper from '@mui/material/Paper';

function TONToken({ logoURI, name, symbol, price, balance, disabled }) {
  const balancePrice = price.data * balance.data || 0;
  const balanceValue = balance.data;
  const isLoading = balance.isLoading || price.isLoading;

  return (
    <Paper
      variant='outlined'
      elevation={0}
      className={styles.token}
      sx={{ padding: '8px 16px' }}
      onClick={() => {}}
    >
      <div className={styles.tokenInfo}>
        <div className={styles.tokenInfoIcon}>
          <img src={logoURI} alt={symbol} />
        </div>
        <div className={styles.tokenInfoTitle}>
          <div className={styles.tokenInfoTitleSymbol}>{symbol}</div>
          <div className={styles.tokenInfoTitleName}>{name}</div>
        </div>
      </div>
      <div className={styles.tokenBalance}>
        <div className={styles.tokenBalanceText}>
          <div className={styles.tokenBalanceTextAmount}>
            {isLoading && <Skeleton sx={{ width: 60 }} />}
            {!isLoading && typeof balanceValue === 'number' ? getFinePrice(balance.data) : ''}
          </div>
          <div className={styles.tokenBalanceTextValue}>
            {isLoading && <Skeleton sx={{ width: 40 }} />}
            {!isLoading && `$${getFinePrice(balancePrice)}`}
          </div>
        </div>
        <div className={styles.tokenBalanceAction}>
          <Icon icon={'chevron-right'} />
        </div>
      </div>
    </Paper>
  );
}

export default TONToken;
