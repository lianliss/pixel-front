import React from 'react';
import styles from './CopyTONAddress.module.scss';
import toaster from 'services/toaster.tsx';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { Icon } from '@blueprintjs/core';

import ChainSwitcher from 'app/widget/ChainSwitcher/ChainSwitcher.tsx';
import { Typography } from '@ui-kit/Typography/Typography';
import { useTonAddress, useTonWallet } from '@tonconnect/ui-react';
import ProjectGuideIcon from '@ui-kit/icon/common/porject-guide.jsx';
import { useTonBalance } from 'services/ton/hooks/useTonBalance';

function CopyTONAddress() {
  const accountAddress = useTonAddress();

  return (
    <div className={styles.copyAddress}>
      <CopyToClipboard
        text={accountAddress}
        className={styles.copyAddressContent}
        onCopy={() => {
          Telegram.WebApp.HapticFeedback.impactOccurred('medium');

          typeof toaster.show === 'function' &&
            toaster.show({
              intent: 'warning',
              message: <>Address copied to clipboard</>,
              icon: 'clipboard',
            });
        }}
      >
        <div>
          <Typography color='text.primary' className={styles.copyAddressText}>
            {accountAddress
              ? accountAddress.slice(0, 6) + '...' + accountAddress.slice(accountAddress.length - 4)
              : ''}
          </Typography>
          <Icon icon={'duplicate'} size={14} />
        </div>
      </CopyToClipboard>

      <div style={{ display: 'flex', gap: 16, alignItems: 'center' }}>
        <div
          onClick={() =>
            Telegram.WebApp.openLink(
              'https://telegra.ph/Navigation-and-Guide-to-Hello-Pixel-08-10',
              {}
            )
          }
          style={{ display: 'block' }}
        >
          <ProjectGuideIcon style={{ fontSize: 30 }} />
        </div>

        <ChainSwitcher isRelative />
      </div>
    </div>
  );
}

export default CopyTONAddress;
