import React from 'react';
import styles from './components/CreateWallet.module.scss';
import logo from 'styles/svg/logo_icon.svg';
import CreateWalletForm from './components/CreateWalletForm.tsx';

import RecoverWalletForm from 'app/lib/Wallet/RecoverWalletForm';
import { useTranslation } from 'react-i18next';
import { Button } from '@ui-kit/Button/Button';
import { IS_TELEGRAM } from '@cfg/config.ts';
import Dialog from '@mui/material/Dialog';
import Container from '@mui/material/Container';
import { showConnectWallet } from '../../../../widget/Connect/ConnectWallet.store.ts';

function SignUp() {
  const [isCreate, setIsCreate] = React.useState(false);
  const [isRecover, setIsRecover] = React.useState(false);
  const { t } = useTranslation('wallet');

  const handleCreateWallet = () => {
    Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
    setIsCreate(true);

    Telegram.WebApp.BackButton.onClick(() => {
      setIsCreate(false);
      Telegram.WebApp.BackButton.hide();
    });
    Telegram.WebApp.BackButton.show();
  };

  const onRecover = () => {
    Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
    setIsRecover(true);

    Telegram.WebApp.BackButton.onClick(() => {
      setIsRecover(false);
      Telegram.WebApp.BackButton.hide();
    });
    Telegram.WebApp.BackButton.show();
  };

  const renderConnect = () => (
    <>
      <div className={styles.createWalletCenter}>
        <img className={styles.createWalletLogo} src={logo} alt={''} />
        <h1>{t('PIXEL Wallet')}</h1>
        <p>
          {t('A seamless multi-chain Wallet from')}
          <br />
          {t('Gamified SocialFi Pixel Ecosystem')}
        </p>
      </div>
      <div className={styles.createWalletButtons}>
        <Button onClick={onRecover} variant='contained' color='primary' size='large'>
          {t('Import Private Key')}
        </Button>
        {!IS_TELEGRAM && (
          <Button
            onClick={() => showConnectWallet()}
            variant='contained'
            color='primary'
            size='large'
            sx={{ mt: 2 }}
          >
            {t('Connect Wallet')}
          </Button>
        )}
        <Button
          onClick={handleCreateWallet}
          variant='contained'
          color='success'
          size='large'
          sx={{ mt: 2 }}
        >
          {t('Create new Wallet')}
        </Button>
      </div>
    </>
  );

  return (
    <Dialog fullScreen open className={styles.createWallet}>
      <Container
        maxWidth='xs'
        sx={{ height: '100%', display: 'flex', flexDirection: 'column', justifyContent: 'center' }}
      >
        {isCreate && <CreateWalletForm />}
        {isRecover && <RecoverWalletForm />}
        {!isCreate && !isRecover && renderConnect()}
      </Container>
    </Dialog>
  );
}

export default SignUp;
