import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Paper, { PaperProps } from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import toaster from 'services/toaster.tsx';
import React from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { Button } from '@ui-kit/Button/Button.tsx';

const StyledRoot = styled(Paper)(() => ({
  padding: 16,
  display: 'flex',
  flexWrap: 'wrap',
  gap: 8,
}));
const StyledItem = styled(Paper)(() => ({
  padding: '4px 8px',
  width: 'calc(50% - 4px)',
  flexBasis: 'calc(50% - 4px)',
}));

type Props = PaperProps & { phrase: string; onNext?: () => void };

export function SeedPhrase(props: Props) {
  const { phrase, onNext, ...other } = props;

  const words = phrase.split(' ');

  return (
    <StyledRoot variant='outlined' {...other}>
      {words.map((word, i) => (
        <StyledItem key={`word-${word}`} variant='outlined'>
          <Typography>
            {i + 1}. {word}
          </Typography>
        </StyledItem>
      ))}

      <CopyToClipboard
        text={phrase}
        onCopy={() => {
          Telegram.WebApp.HapticFeedback.impactOccurred('medium');

          toaster.success('Secret phrase copied to clipboard');
        }}
      >
        <Button variant='outlined' fullWidth sx={{ mt: 1 }}>
          Copy
        </Button>
      </CopyToClipboard>

      <Button variant='contained' fullWidth sx={{ mt: 1 }} onClick={onNext}>
        Continue
      </Button>
    </StyledRoot>
  );
}
