import Paper, { PaperProps } from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import React, { useMemo, useState } from 'react';
import { Button } from '@ui-kit/Button/Button.tsx';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import { getRandomInt } from 'utils/format/random.ts';

const StyledRoot = styled(Paper)(() => ({
  padding: 16,
  display: 'flex',
  flexWrap: 'wrap',
  gap: 8,
}));

type Props = PaperProps & { phrase: string; onConfirm?: () => void };

export function SeedPhraseConfirm(props: Props) {
  const { phrase, onConfirm, ...other } = props;

  const words = phrase.split(' ');

  const [first, setFirst] = useState('');
  const [second, setSecond] = useState('');
  const [third, setThird] = useState('');

  const indices = useMemo(
    () => ({
      first: getRandomInt(4, 0),
      second: getRandomInt(5, 8),
      third: getRandomInt(9, 11),
    }),
    [phrase]
  );

  const validFirst = words[indices.first] === first;
  const validSecond = words[indices.second] === second;
  const validThird = words[indices.third] === third;

  return (
    <StyledRoot variant='outlined' {...other}>
      <TextField
        variant='outlined'
        InputProps={{ startAdornment: `${indices.first + 1}.` }}
        fullWidth
        size='small'
        value={first}
        onChange={(e) => setFirst(e.target.value)}
        error={first && !validFirst}
      />
      <TextField
        variant='outlined'
        InputProps={{ startAdornment: `${indices.second + 1}.` }}
        fullWidth
        size='small'
        value={second}
        onChange={(e) => setSecond(e.target.value)}
        error={second && !validSecond}
      />
      <TextField
        variant='outlined'
        InputProps={{ startAdornment: `${indices.third + 1}.` }}
        fullWidth
        size='small'
        value={third}
        onChange={(e) => setThird(e.target.value)}
        error={third && !validThird}
      />

      <Button
        variant='contained'
        fullWidth
        sx={{ mt: 1 }}
        onClick={onConfirm}
        disabled={!validFirst || !validSecond || !validThird}
      >
        Create Wallet
      </Button>
    </StyledRoot>
  );
}
