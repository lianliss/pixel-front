import React, { useEffect } from 'react';
import styles from './CreateWallet.module.scss';
import { ethers } from 'ethers6';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import toaster from 'services/toaster.tsx';
import { Icon } from '@blueprintjs/core';

import { useConnect } from '@chain/hooks/useConnect.ts';
import { Button } from '@ui-kit/Button/Button.tsx';
import { pixelConnectorState } from '@chain/wagmi.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { pixelStorage } from '@chain/connectors/pixel.provider.ts';
import { SeedPhrase } from 'lib/Wallet/pages/signup/components/SeedPhrase.tsx';
import { SeedPhraseConfirm } from 'lib/Wallet/pages/signup/components/SeedPhraseConfirm.tsx';

type Props = { onCreate?: () => void };

function CreateWalletForm(props: Props) {
  const { onCreate } = props;

  const connect = useConnect();

  const [wallet, setWallet] = React.useState(null);
  const [confirm, setConfirm] = React.useState(false);

  const handleConfirm = () => setConfirm(true);
  const handleConnect = () => {
    Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

    pixelStorage.savePrivateKey(wallet.privateKey);

    connect({ connector: pixelConnectorState });

    onCreate?.();
  };

  useEffect(() => {
    const wallet = ethers.Wallet.createRandom();

    setWallet(wallet);
    setConfirm(false);
  }, []);

  return (
    <div className={styles.createWalletMnemonic}>
      <div />
      <div>
        <Typography
          align='center'
          fontWeight='bold'
          variant='h1'
          color='text.primary'
          sx={{ mb: 2 }}
        >
          Create Wallet
        </Typography>

        <Typography variant='caption' align='center' component='p'>
          Save this phrase in a safe place. With it you can restore your wallet at any time
        </Typography>

        {wallet && !confirm && (
          <SeedPhrase phrase={wallet.mnemonic.phrase} sx={{ mt: 1 }} onNext={handleConfirm} />
        )}
        {wallet && confirm && (
          <SeedPhraseConfirm
            phrase={wallet.mnemonic.phrase}
            sx={{ mt: 1 }}
            onConfirm={handleConnect}
          />
        )}
      </div>
      <div />
    </div>
  );
}

export default CreateWalletForm;
