import React from 'react';
import styles from './Transfer.module.scss';
import { useMatch } from 'react-router-dom';
import routes from 'const/routes.tsx';
import getFinePrice from 'utils/getFinePrice';

import { isAddress as web3IsAddress } from 'web3-utils';
import toaster from 'services/toaster';
import { useToken } from '@chain/hooks/useToken.ts';
import TokenHeader from 'lib/Wallet/components-v2/TokenHeader/TokenHeader.tsx';
import { useBalance } from '@chain/hooks/useBalance.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTokenPrice } from '@chain/hooks/useTokenPrice.ts';
import { useTokenApi } from '@chain/hooks/useTokenApi.ts';
import Paper from '@mui/material/Paper';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import { IS_TELEGRAM } from '@cfg/config.ts';
import QrCodeIcon from '@mui/icons-material/QrCode';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useGasBalance } from '@chain/hooks/useGasBalance.ts';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import Alert from '@mui/material/Alert';
import { useTranslation } from 'react-i18next';
import BuyGasAlertLazy from 'lib/TelegramStars/components/BuyGasAlert/BuyGasAlert.lazy.tsx';
import { getDefaultChainImage } from '@cfg/image.ts';
import Divider from '@mui/material/Divider';
import Container from '@mui/material/Container';

function Transfer() {
  const account = useAccount();
  const routePath = routes.walletTransfer.path;
  const match = useMatch(routePath);
  const tokenAddress = match.params.symbol;

  const { t } = useTranslation('wallet');

  const tokenInfo = useToken({
    address: tokenAddress,
  });
  const tokenApi = useTokenApi({
    address: tokenAddress,
  });
  const gasBalance = useGasBalance({
    address: account.accountAddress,
    skip: !account.isConnected,
  });
  const balanceData = useBalance({
    address: account.accountAddress,
    token: tokenAddress,
    skip: !account.isConnected,
  });
  const tokenPrice = useTokenPrice({
    address: tokenAddress,
  });

  const [address, setAddress] = React.useState('');
  const [value, setValue] = React.useState('');

  const valueUsd = +(Number(value) * tokenPrice.data || 0).toFixed(2);

  const onQrClick = (e) => {
    e.stopPropagation();
    e.preventDefault();

    Telegram.WebApp.HapticFeedback.impactOccurred('light');
    // scanQR((code) => {
    //   if (web3IsAddress(code)) {
    //     setAddress(code);
    //     return true;
    //   } else {
    //     return false;
    //   }
    // }, 'Scan recipient address by QR-code');
  };

  const handleTransfer = async () => {
    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      await tokenApi.send({ amount: Number(value), receiver: address });
      setAddress('');
      setValue('');

      const shortAddress = address.slice(0, 6) + '...' + address.slice(address.length - 4);
      toaster.success(
        <>
          {getFinePrice(+value)} {tokenInfo.data?.symbol} successfully
          <br />
          sent to {shortAddress}
        </>
      );

      await balanceData.refetch();

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
      toaster.logError(e);
    }
  };

  useBackAction(`/wallet/token/${tokenAddress}`);

  const noGasBalance = !gasBalance.data?.formatted;
  const invalidAddress = !address || !web3IsAddress(address);

  return (
    <div
      className={styles.transfer}
      style={{ backgroundImage: `url(${getDefaultChainImage(account.chainId)})` }}
    >
      <Container maxWidth='xs'>
        <TokenHeader
          token={tokenInfo.data}
          balance={balanceData.data?.formatted}
          price={tokenPrice.data}
          showBalance
        />

        <Divider />

        <div className={styles.transferContainer}>
          <Typography
            variant='h2'
            fontWeight='bold'
            align='center'
            color='text.primary'
            sx={{ mb: 2 }}
          >
            Transfer
          </Typography>

          <Paper variant='outlined' sx={{ p: 2 }}>
            <Typography variant='h4' color='text.primary' sx={{ mb: 1 }}>
              Recipient address
            </Typography>

            <TextField
              placeholder={'0xa38d84...'}
              value={address}
              onClick={() => {
                Telegram.WebApp.HapticFeedback.impactOccurred('light');
              }}
              onChange={(e) => setAddress(e.target.value)}
              disabled={tokenApi.loading}
              // InputProps={{
              //   endAdornment: IS_TELEGRAM && (
              //     <Button size='small' variant='outlined' onClick={onQrClick}>
              //       <QrCodeIcon />
              //     </Button>
              //   ),
              // }}
              fullWidth
              error={Boolean(address && invalidAddress)}
              helperText={Boolean(address && invalidAddress) ? t('Invalid address') : undefined}
            />
          </Paper>
          <Paper variant='outlined' sx={{ p: 2, mt: 3 }}>
            <Typography variant='h4' color='text.primary' sx={{ mb: 1 }}>
              Amount
            </Typography>
            <TextField
              placeholder={'0'}
              type={'number'}
              value={value}
              onClick={() => {
                Telegram.WebApp.HapticFeedback.impactOccurred('light');
              }}
              onChange={(e) => setValue(e.target.value)}
              disabled={tokenApi.loading}
              InputProps={{
                endAdornment: (
                  <Button
                    size='small'
                    variant='contained'
                    disabled={tokenApi.loading}
                    onClick={() => {
                      setValue((balanceData.data?.formatted || 0).toString());
                    }}
                  >
                    Max
                  </Button>
                ),
              }}
              fullWidth
            />
          </Paper>

          {noGasBalance && (
            <BuyGasAlertLazy variant='outlined' sx={{ mt: 2 }} text={t('Not enough gas')} />
          )}

          <Button
            sx={{ mt: 2 }}
            variant='contained'
            size='large'
            onClick={handleTransfer}
            fullWidth
            loading={tokenApi.loading}
            disabled={!value || Number.isNaN(+value) || invalidAddress || noGasBalance}
          >
            Transfer {valueUsd ? `${valueUsd}$` : ''}
          </Button>
        </div>
      </Container>
    </div>
  );
}

export default Transfer;
