import React from 'react';
import styles from './Settings.module.scss';
import { Icon } from '@blueprintjs/core';
import { WalletBlock } from 'ui/index.jsx';
import { useTranslation } from 'react-i18next';
import { Typography } from '@ui-kit/Typography/Typography';
import SecretKeys from 'lib/Wallet/pages/settings/components/SecretKeys/SecretKeys';
import Logout from 'lib/Wallet/pages/settings/components/Logout/Logout';

import { useAccount } from '@chain/hooks/useAccount';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import Container from '@mui/material/Container';
import TONDisconnect from 'lib/Wallet/pages/settings/components/TONDisconnect/TONDisconnect.tsx';
import useIsTonChain from 'services/ton/hooks/useIsTonChain.tsx';

const ICON_SIZE = 24;

function Settings() {
  const account = useAccount();
  const { t } = useTranslation('settings');

  useBackAction('/wallet');

  const isPixel = account.connector?.type === 'pixel';
  const { isTonChain } = useIsTonChain();

  const [isKeys, setIsKeys] = React.useState(false);
  const [isLogout, setIsLogout] = React.useState(false);
  const [isTonDisconnect, setIsTonDisconnect] = React.useState(false);

  return (
    <div className={styles.settings}>
      <Container maxWidth='xs'>
        <Typography
          color='text.primary'
          variant='h5'
          align='center'
          fontWeight='bold'
          sx={{ mt: 2 }}
        >
          {t('Settings')}
        </Typography>
        {isPixel && (
          <WalletBlock
            className={styles.settingsButton}
            onClick={() => {
              Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
              setIsKeys(true);
            }}
          >
            <div className={styles.settingsButtonLeft}>
              <div className={styles.settingsButtonLeftIcon}>
                <Icon icon={'key'} size={ICON_SIZE} />
              </div>
              <div className={styles.settingsButtonLeftTitle}>
                <Typography color='text.primary' className={styles.settingsButtonLeftTitleText}>
                  {t('Show Private Key')}
                </Typography>
                <Typography
                  color='text.secondary'
                  variant='caption'
                  className={styles.settingsButtonLeftTitleHint}
                >
                  {t('Get your private key')}
                </Typography>
              </div>
            </div>
            <div className={styles.settingsButtonRight}>
              <div className={styles.settingsButtonRightAction}>
                <Icon icon={'chevron-right'} />
              </div>
            </div>
          </WalletBlock>
        )}
        <WalletBlock
          className={styles.settingsButton}
          onClick={() => {
            Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
            setIsLogout(true);
          }}
        >
          <div className={styles.settingsButtonLeft}>
            <div className={styles.settingsButtonLeftIcon}>
              <Icon icon={'log-out'} size={ICON_SIZE} />
            </div>
            <div className={styles.settingsButtonLeftTitle}>
              <Typography color='text.primary' className={styles.settingsButtonLeftTitleText}>
                {t('Logout')}
              </Typography>
              <Typography
                color='text.secondary'
                variant='caption'
                className={styles.settingsButtonLeftTitleHint}
              >
                {t('Log out of your account and forget your private key')}
              </Typography>
            </div>
          </div>
          <div className={styles.settingsButtonRight}>
            <div className={styles.settingsButtonRightAction}>
              <Icon icon={'chevron-right'} />
            </div>
          </div>
        </WalletBlock>
        {isTonChain && (
          <WalletBlock
            className={styles.settingsButton}
            onClick={() => {
              Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
              setIsTonDisconnect(true);
            }}
          >
            <div className={styles.settingsButtonLeft}>
              <div className={styles.settingsButtonLeftIcon}>
                <Icon icon={'unlink'} size={ICON_SIZE} />
              </div>
              <div className={styles.settingsButtonLeftTitle}>
                <Typography color='text.primary' className={styles.settingsButtonLeftTitleText}>
                  {t('Disconnect TON Wallet')}
                </Typography>
                <Typography
                  color='text.secondary'
                  variant='caption'
                  className={styles.settingsButtonLeftTitleHint}
                >
                  {t('Disable current TON Wallet connection')}
                </Typography>
              </div>
            </div>
            <div className={styles.settingsButtonRight}>
              <div className={styles.settingsButtonRightAction}>
                <Icon icon={'chevron-right'} />
              </div>
            </div>
          </WalletBlock>
        )}
        {isKeys && <SecretKeys onClose={() => setIsKeys(false)} />}
        {isLogout && <Logout onClose={() => setIsLogout(false)} />}
        {isTonDisconnect && isTonChain && (
          <TONDisconnect onClose={() => setIsTonDisconnect(false)} />
        )}

        {account.connector && (
          <Typography variant='caption' align='center' color='textSecondary' component='p'>
            Connector: {account.connector?.type || 'unknown'}
          </Typography>
        )}
      </Container>
    </div>
  );
}

export default Settings;
