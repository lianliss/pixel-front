import React from 'react';
import styles from './Logout.module.scss';

import { useNavigate } from 'react-router-dom';
import routes from 'const/routes.tsx';
import { useDisconnect } from '@chain/hooks/useDisconnect';
import { useTranslation } from 'react-i18next';
import { Button } from '@ui-kit/Button/Button';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog';
import { Typography } from '@ui-kit/Typography/Typography';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { clearEntryScreenHidden } from '../../../../../../shared/blocks/EntryScreen/utils.ts';
import { IS_TELEGRAM } from '@cfg/config.ts';
import { pixelStorage } from '@chain/connectors/pixel.provider.ts';
import { clearSign } from '@chain/auth/create-sign.ts';

function Logout({ onClose }) {
  const account = useAccount();
  const { disconnect } = useDisconnect();
  const { t } = useTranslation('settings');

  const navigate = useNavigate();
  const walletPath = routes.wallet.path;

  const isPixel = account.connector?.type === 'pixel';
  const isInjected = account.connector?.type === 'injected';
  const isConnector = !!account.connector?.getChainId;

  const handleLogout = () => {
    disconnect();
    navigate(walletPath);

    clearEntryScreenHidden();
    clearSign();

    if (IS_TELEGRAM && (!pixelStorage.getPrivateKey() || !isConnector)) {
      localStorage.clear();
    }
    if (isInjected) {
      localStorage.clear();
    }
  };

  const onThisClose = () => {
    onClose();
  };

  return (
    <BottomDialog open maxWidth='xs' fullWidth onClose={onThisClose} className={styles.keys}>
      <Typography align='center' variant='caption' component='p'>
        Logging out will erase your private key <br /> from the application
      </Typography>
      <Typography fontWeight='bold' align='center' variant='caption'>
        <b>{t('Please make sure you have saved your private key')}</b>
      </Typography>

      <Button variant='contained' size='large' fullWidth onClick={handleLogout} sx={{ mt: 2 }}>
        {isPixel ? t('Clear Private Key') : t('Disconnect Wallet')}
      </Button>
    </BottomDialog>
  );
}

export default Logout;
