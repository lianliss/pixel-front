import React from 'react';
import styles from './SecretKeys.module.scss';
import { WalletPopup } from 'ui/index.jsx';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import toaster from 'services/toaster';
import { Icon } from '@blueprintjs/core';
import { useTranslation } from 'react-i18next';
import { pixelStorage } from '@chain/connectors/pixel.provider';

function SecretKeys({ onClose }) {
  const privateKey = pixelStorage.getPrivateKey();

  const { t } = useTranslation('settings');

  const showToaster = () => {
    typeof toaster.show === 'function' &&
      toaster.show({
        intent: 'warning',
        message: <>{t('Private key copied to clipboard')}</>,
        icon: 'clipboard',
      });
  };

  const onThisClose = () => {
    onClose();
    Telegram.WebApp.HapticFeedback.impactOccurred('light');
  };

  return (
    <WalletPopup onClose={onThisClose} className={styles.keys}>
      <p dangerouslySetInnerHTML={{ __html: t('This private key') }}></p>
      <CopyToClipboard
        text={privateKey}
        onCopy={() => {
          Telegram.WebApp.HapticFeedback.impactOccurred('medium');
          showToaster();
        }}
      >
        <div className={styles.keysCopy}>
          <div className={styles.keysCopyText}>{privateKey}</div>
          <Icon className={styles.keysCopyIcon} icon={'clipboard'} />
        </div>
      </CopyToClipboard>
      <p dangerouslySetInnerHTML={{ __html: t('Do not share') }}></p>
    </WalletPopup>
  );
}

export default SecretKeys;
