import React from 'react';
import styles from './TONDisconnect.module.scss';

import { Button } from '@ui-kit/Button/Button';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog';
import { Typography } from '@ui-kit/Typography/Typography';
import useTonDisconnect from 'services/ton/hooks/useTonDisconnect.tsx';

function TONDisconnect({ onClose }) {
  const onDisconnect = useTonDisconnect();

  const onThisClose = () => {
    onClose();
  };

  return (
    <BottomDialog open maxWidth='xs' fullWidth onClose={onThisClose} className={styles.keys}>
      <Typography align='center' variant='caption' component='p'>
        Disconnect your TON Connection
      </Typography>

      <Button variant='contained' size='large' fullWidth onClick={onDisconnect} sx={{ mt: 2 }}>
        Disconnect TON Wallet
      </Button>
    </BottomDialog>
  );
}

export default TONDisconnect;
