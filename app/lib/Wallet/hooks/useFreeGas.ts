import React from 'react';
import { useWeb3 } from 'services/web3Provider';
import toaster from 'services/toaster';
import { wei } from 'utils/index';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { FREE_GAS_CONTRACT_ID } from 'lib/Wallet/api-contract/wallet.contracts.ts';
import { useAccount } from '../../../shared/chain/hooks/useAccount.ts';
import FREE_GAS_ABI from 'lib/Wallet/api-contract/abi/free-gas.abi.ts';

function useFreeGas() {
  const account = useAccount();
  const { apiClaimSFuel } = useWeb3() || {};

  const address = FREE_GAS_CONTRACT_ID[account.chainId];

  const [claimLoading, setClaimLoading] = React.useState(false);

  const freeGas = useReadContract({
    functionName: 'getData',
    abi: FREE_GAS_ABI,
    address,
    args: [account.accountAddress],
    skip: !account.isConnected || !address,
    select: (data) => ({
      balance: wei.from(data.balance),
      gasAmount: wei.from(data.gasAmount),
      minGasLimit: wei.from(data.minGasLimit),
      nextAddressOpportunity: Number(data.nextAddressOpportunity) * 1000,
      nextUserOpportunity: Number(data.nextUserOpportunity) * 1000,
      isAvailable: data.isAvailable,
    }),
  });

  const handleClaim = async () => {
    try {
      setClaimLoading(true);
      await apiClaimSFuel();
      toaster.success(`${freeGas.data?.gasAmount || 0.05} gas claimed`);
    } catch (error) {
      toaster.error(error?.data?.message, 'Unable to claim');
    }
    setClaimLoading(false);
  };

  return {
    isFetched: freeGas.isFetched,
    claim: handleClaim,
    claimLoading,
    data: freeGas.data,
    enabled: !!address,
    loading: freeGas.loading,
    available: !!freeGas.data?.isAvailable,
  };
}

export default useFreeGas;
