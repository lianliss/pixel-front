const BLOCKED_TELEGRAM_IDS = new Set([7125488705, 434206432]);

const BLOCKED_ADDRESSES = new Set([
  '0x080007e52C28b61A51350505C5FF00c3a6A0fCb9'.toLowerCase(),
  '0x22B655DCD63858bA128F41a0C03Ee1AfF6F50016'.toLowerCase(),
  // '0x4B9Ac70305761c8f1c4e88AD6C54499b15674a07'.toLowerCase(),
]);

export function isBlockedTelegram(telegramId: number): boolean {
  return BLOCKED_TELEGRAM_IDS.has(telegramId);
}

export function isBlockedAddress(address: string): boolean {
  return address && BLOCKED_ADDRESSES.has(address.toLowerCase());
}
