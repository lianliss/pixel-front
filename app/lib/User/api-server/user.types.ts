export type IUser<TDate = Date> = {
  userId: number;
  telegramId: number;
  address: string;
  telegramUserName: string;
  telegramFirstName: string;
  telegramLastName: string;
  created: TDate;
  isDeleted: boolean;
  gasless: number;
};
export type IUserDto = IUser;

export type IUserInfo<TDate = Date> = {
  id: string;
  userAddress: string;
  image?: string;
  minerZar: boolean;
  minerBuddha: boolean;
  telegramId?: number;
};

export type IUserInfoDto = IUserInfo<number> & { karma: IUserKarmaDto | null };

// request

export type IGetUserDto = {};
export type ICreateUserDto = {};
export type IGetUserInfoDto = { userAddress: string };

// response

export type IGetUserResponse = IUserDto;
export type ICreateUserResponse = IUserDto;
export type IGetUserInfoResponse = IUserInfoDto;

// karma

export enum UserKarmaStatus {
  Blocked = 'Blocked',
  Bot = 'Bot',
}

export type IUserKarmaDto<TDate = number> = {
  id: string;

  userAddress: string;
  userTelegram: string;

  status: UserKarmaStatus;
  value: number;

  createdAt: TDate;
};

// access

export enum CheckAccessType {
  Buddha = 'buddha',
  Zar = 'zar',
}

export type ICheckUserAccessDto = {
  type: CheckAccessType;
} & { userAddress: string };

export type ICheckUserAccessResponse = IUserInfoDto;
