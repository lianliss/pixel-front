import { createEffect, createStore } from 'effector';
import {
  ICheckUserAccessDto,
  ICreateUserDto,
  IGetUserDto,
  IGetUserInfoDto,
  IUserDto,
  IUserInfoDto,
} from 'lib/User/api-server/user.types.ts';
import { userApi } from 'lib/User/api-server/user.api.ts';
import { AxiosError } from 'axios';

export const createUserEffect = createEffect(
  async (params: ICreateUserDto & { address: string }) => {
    const res = await userApi.create(params);
    return res;
  }
);

export const getUserEffect = createEffect(async (params: IGetUserDto & { address: string }) => {
  const res = await userApi.get(params);
  return res;
});

export const userStore = createStore<IUserDto | null>(null)
  .on(getUserEffect.doneData, (prev, next) => next)
  .on(createUserEffect.doneData, (prev, next) => next);

// user info

export const getSelfUserInfo = createEffect(async (params: IGetUserInfoDto) => {
  const res = await userApi.getUserInfo(params);

  return res;
});

export const userSelfInfoStore = createStore({
  loading: true,
  error: null as Error | null,
  state: null as IUserInfoDto | null,
})
  .on(getSelfUserInfo.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(getSelfUserInfo.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(getSelfUserInfo.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

export const getUserInfo = createEffect(async (params: IGetUserInfoDto) => {
  const res = await userApi.getUserInfo(params);

  return res;
});

export const userCheckAccess = createEffect(async (params: ICheckUserAccessDto) => {
  const res = await userApi.checkAccess(params);

  return res;
});

export const userInfoStore = createStore({
  loading: true,
  error: null as Error | null,
  state: null as IUserInfoDto | null,
  isFetched: false,
})
  .on(userCheckAccess.doneData, (_prev, next) => ({
    loading: false,
    error: null,
    state: next,
    isFetched: true,
  }))
  .on(getUserInfo.doneData, (_prev, next) => ({
    loading: false,
    error: null,
    state: next,
    isFetched: true,
  }))
  .on(getUserInfo.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
    isFetched: true,
  }))
  .on(getUserInfo.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
    isFetched: prev.isFetched,
  }));
