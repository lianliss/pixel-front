'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  ICheckUserAccessDto,
  ICheckUserAccessResponse,
  ICreateUserDto,
  ICreateUserResponse,
  IGetUserInfoDto,
  IGetUserInfoResponse,
  IGetUserResponse,
} from 'lib/User/api-server/user.types.ts';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';
import {
  pixelMessageKey,
  pixelSignKey,
  telegramAuthKey,
} from '../../../shared/const/localStorage.ts';
import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';

import { axiosInstance } from 'utils/libs/axios.ts';

export class UserApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async get({ address }: { address: string }): Promise<IGetUserResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetUserResponse },
      AxiosResponse<{ data: IGetUserResponse }>
    >(`/api/v1/user`, { baseURL: this.url });
    return data.data;
  }

  async create({ address }: { address: string }): Promise<ICreateUserResponse> {
    const { data } = await axiosInstance.post<
      { data: ICreateUserResponse },
      AxiosResponse<{ data: ICreateUserResponse }>,
      ICreateUserDto
    >(`/api/v1/user`, {}, { baseURL: this.url });
    return data.data;
  }

  async getUserInfo({ userAddress }: IGetUserInfoDto): Promise<IGetUserInfoResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetUserInfoResponse },
      AxiosResponse<{ data: IGetUserInfoResponse }>
    >(`/api/v1/user/info`, {
      baseURL: this.url,
      params: { userAddress: userAddress.toLowerCase() },
    });

    return data.data || (data as any);
  }

  async checkAccess({
    userAddress,
    ...params
  }: ICheckUserAccessDto): Promise<ICheckUserAccessResponse> {
    const { data } = await axiosInstance.post<
      { data: ICheckUserAccessResponse },
      AxiosResponse<{ data: ICheckUserAccessResponse }>,
      Omit<ICheckUserAccessDto, 'userAddress'>
    >(`/api/v1/user/access/check`, params, {
      baseURL: this.url,
      params: { userAddress: userAddress.toLowerCase() },
    });

    return data.data || (data as any);
  }
}

// 'http://127.0.0.1:4000' ||
export const userApi = new UserApi({ url: 'https://api.hellopixel.network' });
