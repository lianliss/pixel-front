import React from 'react';
import { InvalidAddressDialogUi } from './InvalidAddressDialog.ui.tsx';

export function InvalidAddressDialog() {
  return <InvalidAddressDialogUi />;
}
