import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import React from 'react';

import styles from './styles.module.scss';

import { Button } from '@ui-kit/Button/Button.tsx';

import { Typography } from '@ui-kit/Typography/Typography.tsx';

export type IMinerDroneDialogUiProps = {};

export function InvalidAddressDialogUi(props: IMinerDroneDialogUiProps = {}) {
  const [isOpen, setIsOpen] = React.useState(true);

  const onClose = () => setIsOpen(false);

  return (
    <BottomDialog
      isOpen={isOpen}
      onClose={onClose}
      footer={
        <>
          <div className={styles.btns}>
            <Button
              size='large'
              variant='contained'
              fullWidth
              onClick={() => window.location.reload()}
            >
              Reload
            </Button>
          </div>
        </>
      }
    >
      <div className={styles.root}>
        <div className={styles.heading}>
          <Typography fontWeight='bold'>Oops! Error!</Typography>
          <Typography variant='caption' className={styles.description}>
            Try reloading the page
          </Typography>
        </div>

        <div className={styles.leveling}>
          <Typography sx={{ color: '#E62058' }} variant='caption' medium>
            Invalid Address
          </Typography>
        </div>
      </div>
    </BottomDialog>
  );
}
