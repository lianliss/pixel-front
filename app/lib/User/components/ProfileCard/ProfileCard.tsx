import React from 'react';
import styles from './styles.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import { useTranslation } from 'react-i18next';
import userImage from 'assets/img/lion-avatar.png';
import { darken, styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';

const StyledRoot = styled(Paper)(() => ({}));

const StyledInfo = styled(Paper)(({ theme }) => ({
  backgroundColor: darken(theme.palette.background.paper, 0.2),
}));

type Props = { speedLevel: number; sizeLevel: number; image?: string; loadingUser?: boolean };

const ProfileCard = (props: Props) => {
  const { t } = useTranslation('profile');
  const { speedLevel, sizeLevel, image, loadingUser = false } = props;

  return (
    <StyledRoot className={styles.profile__card}>
      <StyledInfo className={styles.profile__inner}>
        <div className={styles.info__sections}>
          <div className={styles.section}>
            <div className={styles.section__content}>
              <Typography sx={{ fontSize: 14 }} color='text.primary' fontWeight='bold'>
                {t('Title')}
              </Typography>
              <Typography sx={{ fontSize: 10 }} color='text.primary'>
                {t('Coming soon')}
              </Typography>
            </div>
          </div>
          <div className={styles.section}>
            <div className={styles.section__content}>
              <Typography sx={{ fontSize: 14 }} fontWeight='bold' color='text.primary'>
                {t('Drill')}
              </Typography>
              <div className={styles.bottom__content}>
                <Typography sx={{ fontSize: 12 }} color='text.secondary'>
                  <Typography variant='inherit' component='span' color='success.main'>
                    {typeof speedLevel === 'number' ? speedLevel : 0}{' '}
                  </Typography>
                  {t('Level')}
                </Typography>
                {/*<a>*/}
                {/*  <Typography sx={{ fontSize: 12 }} medium>*/}
                {/*    Upgrade*/}
                {/*  </Typography>*/}
                {/*</a>*/}
              </div>
            </div>
          </div>
          <div className={styles.section}>
            <div className={styles.section__content}>
              <Typography sx={{ fontSize: 14 }} fontWeight='bold' color='text.primary'>
                {t('Storage')}
              </Typography>
              <div className={styles.bottom__content}>
                <Typography sx={{ fontSize: 12 }} color='text.secondary'>
                  <Typography variant='inherit' component='span' color='success.main'>
                    {typeof sizeLevel === 'number' ? sizeLevel : 0}{' '}
                  </Typography>
                  {t('Level')}
                </Typography>
                {/*<a>*/}
                {/*  <Typography sx={{ fontSize: 12 }} medium>*/}
                {/*    Upgrade*/}
                {/*  </Typography>*/}
                {/*</a>*/}
              </div>
            </div>
          </div>
        </div>

        <div className={styles.profile__image}>
          <Avatar
            variantStyle='outlined'
            sx={{ width: 156, height: 156 }}
            src={image || (userImage as string)}
            loading={loadingUser}
          />
          {/*<img*/}
          {/*  className={styles.image}*/}
          {/*  src='https://hellopixel.network/dist/32109f2c8e7341815367.png'*/}
          {/*/>*/}
        </div>
      </StyledInfo>
    </StyledRoot>
  );
};

export default ProfileCard;
