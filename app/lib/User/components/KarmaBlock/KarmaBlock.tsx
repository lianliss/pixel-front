import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Paper from '@mui/material/Paper';
import styles from 'lib/User/pages/profile/user.module.scss';
import ProgressLine from '@ui-kit/ProgressLine/ProgressLine.tsx';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { IUserKarmaDto } from 'lib/User/api-server/user.types.ts';

function KarmaBlock({ karma }: { karma?: IUserKarmaDto }) {
  const { t } = useTranslation('profile');

  return (
    <div>
      <Typography align='center' fontWeight='bold' color='text.primary' sx={{ mb: 1, mt: 1 }}>
        {t('Karma')}
      </Typography>

      <Paper
        variant='outlined'
        sx={{
          display: 'flex',
          flexDirection: 'column',
          gap: '12px',
          padding: '16px',
        }}
      >
        <ProgressLine
          name={`${t('Karma value')}`}
          description={`${karma.value} / ${10_000}`}
          value={karma.value}
          total={10_000}
        />
      </Paper>
    </div>
  );
}

export default KarmaBlock;
