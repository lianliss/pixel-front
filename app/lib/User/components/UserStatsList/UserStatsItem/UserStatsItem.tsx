import styles from './styles.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import React, { ReactElement } from 'react';
import SvgIcon from '@mui/material/SvgIcon';

export type IUserStatItem = {
  label: string;
  value: number;
  suffix?: string;
  icon?: ReactElement;
  colorIcon?: string;
};

type Props = {
  data: IUserStatItem;
};

const UserStatsItem = ({ data: { label, value, suffix, icon, colorIcon } = {} }: Props) => {
  return (
    <div className={styles.item}>
      <div className={styles.left__side}>
        <div className={styles.icon} style={{ borderColor: colorIcon }}>
          <SvgIcon style={{ color: colorIcon }}>{icon}</SvgIcon>
        </div>
        <Typography sx={{ fontSize: 16 }} color='text.secondary' fontWeight='bold'>
          {label}
        </Typography>
      </div>
      <div className={styles.stats__value}>
        <Typography sx={{ fontSize: 18 }} color='text.primary' fontWeight='bold' noWrap>
          {value} {suffix}
        </Typography>
      </div>
    </div>
  );
};

export default UserStatsItem;
