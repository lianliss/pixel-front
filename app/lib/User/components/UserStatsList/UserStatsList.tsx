import React from 'react';
import styles from './styles.module.scss';
import UserStatsItem, { IUserStatItem } from './UserStatsItem/UserStatsItem.tsx';
import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';

type Props = {
  options: IUserStatItem[];
};

const UserStatsList = ({ options = [] }: Props) => {
  return (
    <Stack direction='column' divider={<Divider />} className={styles.items__list}>
      {options.map((option) => (
        <UserStatsItem key={`option-${option.label}`} data={option} />
      ))}
    </Stack>
  );
};

export default UserStatsList;
