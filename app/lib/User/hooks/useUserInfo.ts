import { useUnit } from 'effector-react';
import { getUserInfo, userInfoStore, userSelfInfoStore } from 'lib/User/api-server/user.store.ts';
import React, { useEffect } from 'react';
import userImage from 'assets/img/lion-avatar.png';

import { telegramIdKey } from '../../../shared/const/localStorage.ts';
import { IS_TELEGRAM } from '@cfg/config.ts';
import isNumber from 'lodash/isNumber';

export function useSelfUserInfo() {
  const userInfo = useUnit(userSelfInfoStore);

  // TODO fix
  const telegramId = IS_TELEGRAM
    ? Telegram.WebApp.initDataUnsafe.user?.id
    : +localStorage.getItem(telegramIdKey);
  const startParamRaw = Number(Telegram.WebApp.initDataUnsafe.start_param);
  const parentId = IS_TELEGRAM && !Number.isNaN(startParamRaw) ? startParamRaw : 0;

  return {
    loading: userInfo.loading,
    data: userInfo.state
      ? {
          telegramId:
            (userInfo.state?.telegramId && Number(userInfo.state?.telegramId)) || telegramId,
          parentId,
          image: userInfo.state?.image || userImage,
        }
      : null,
  };
}

export function useUserInfo({ address }: { address: string }) {
  const userInfo = useUnit(userInfoStore);

  useEffect(() => {
    if (address && !userInfo.state) {
      getUserInfo({ userAddress: address }).then();
    }
  }, [address, userInfo.state]);

  return {
    loading: userInfo.loading,
    data: userInfo.state,
  };
}
