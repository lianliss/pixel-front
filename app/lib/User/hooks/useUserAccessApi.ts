import { useUnit } from 'effector-react';
import { userCheckAccess } from 'lib/User/api-server/user.store.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { CheckAccessType } from 'lib/User/api-server/user.types.ts';

export function useUserAccessApi() {
  const account = useAccount();
  const loading = useUnit(userCheckAccess.pending);

  const check = (type: CheckAccessType) => {
    return userCheckAccess({ type: type, userAddress: account.accountAddress }).then();
  };

  return {
    loading,
    check,
  };
}
