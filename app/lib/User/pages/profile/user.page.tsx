import React, { useMemo } from 'react';
import styles from './user.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Button from '@mui/material/Button';
import CopyText from '../../../../features/CopyText/CopyText.tsx';
import { IconCopy } from '@ui-kit/icon/common/IconCopy.tsx';
import { Tabs } from '@ui-kit/Tabs/Tabs.tsx';
import { Tab } from '@ui-kit/Tab/Tab.tsx';
import UserStatsList from 'lib/User/components/UserStatsList/UserStatsList.tsx';
import ProfileCard from 'lib/User/components/ProfileCard/ProfileCard.tsx';
import { IGetMinerUserResponse } from 'lib/Mining/api-server/miner.types.ts';

import { IGetMarketUserResponse } from 'lib/Marketplace/api-server/market.types.ts';
import { IGetNftUserResponse } from 'lib/NFT/api-server/nft.types.ts';
import { InvalidAddressDialog } from 'lib/User/components/InvalidAddressDialog/InvalidAddressDialog.container.tsx';
import { useMarketplaceAcceptedToken } from 'lib/Marketplace/context/Marketplace.provider.tsx';
import { MARKET_CONTRACTS } from 'lib/Marketplace/api-contract/contracts.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useToken } from '@chain/hooks/useToken.ts';
import { Chain } from 'services/multichain/chains';
import Paper from '@mui/material/Paper';
import AchievementImage from 'lib/Achievement/components/AchievementsItem/AchievementImage/AchievementImage.tsx';
import ProgressLine from '@ui-kit/ProgressLine/ProgressLine.tsx';
import { IAchievementRangDto } from 'lib/Achievement/api-server/achievement.types.ts';
import { IUserInfoDto } from 'lib/User/api-server/user.types.ts';
import { useTranslation } from 'react-i18next';
import {
  getUserChestOptions,
  getUserMarketOptions,
  getUserMinerOptions,
  getUserNftOptions,
} from 'lib/User/pages/profile/utils.tsx';
import { TabProfileStatsEnum } from 'lib/User/pages/profile/user.container.tsx';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import { IS_PROFILE_CHEST, IS_PROFILE_KARMA, IS_PROFILE_NFT } from '@cfg/app.ts';
import CircularProgress from '@mui/material/CircularProgress';
import { IGetChestUserResponse } from 'lib/Chests/api-server/chest.types.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { IS_STAGE } from '@cfg/config.ts';
import KarmaBlock from 'lib/User/components/KarmaBlock/KarmaBlock.tsx';
import Divider from '@mui/material/Divider';
import Container from '@mui/material/Container';

type Props = {
  minerData: IGetMinerUserResponse | null;
  minerDataLoading?: boolean;
  minerSwtrData: IGetMinerUserResponse | null;
  minerSwtrDataLoading?: boolean;
  marketData: IGetMarketUserResponse | null;
  marketDataLoading?: boolean;
  nftData: IGetNftUserResponse | null;
  chestData: IGetChestUserResponse | null;
  nftDataLoading?: boolean;
  chestDataLoading?: boolean;
  userAddress?: string;
  invalidAddress: boolean;
  achievementRang?: IAchievementRangDto;
  achievementNextRang?: IAchievementRangDto;
  loadingUser?: boolean;
  userInfo?: IUserInfoDto;
  tab: TabProfileStatsEnum;
  onChangeTab?: (t: TabProfileStatsEnum) => void;
};

const UserPage = (props: Props) => {
  const {
    minerData,
    minerSwtrData,
    marketData,
    nftData,
    chestData,
    userAddress = '',
    invalidAddress,
    achievementRang: userRank,
    achievementNextRang: nextUserRank,
    loadingUser = false,
    userInfo,
    tab,
    onChangeTab,
    nftDataLoading = false,
    marketDataLoading = false,
    minerDataLoading = false,
    minerSwtrDataLoading = false,
    chestDataLoading = false,
  } = props;
  const { t } = useTranslation('profile');

  const account = useAccount();
  const acceptedToken = useMarketplaceAcceptedToken({
    marketAddress: MARKET_CONTRACTS[account.chainId],
    skip: !MARKET_CONTRACTS[account.chainId] || tab !== TabProfileStatsEnum.market,
  });
  const token = useToken({
    address: acceptedToken.data,
    skip: !acceptedToken.data || tab !== TabProfileStatsEnum.market,
  });
  const trade = useTradeToken();

  const isMarket = [
    Chain.SONGBIRD,
    Chain.SKALE,
    Chain.COSTON2,
    Chain.SKALE_TEST,
    Chain.UNITS,
  ].includes(account.chainId);
  const isLoading = useMemo(() => {
    return {
      [TabProfileStatsEnum.miner]: minerDataLoading && !minerData,
      [TabProfileStatsEnum.swtr]: minerSwtrDataLoading && !minerSwtrData,
      [TabProfileStatsEnum.market]: marketDataLoading && !marketData,
      [TabProfileStatsEnum.nft]: nftDataLoading && !nftData,
    }[tab];
  }, [
    tab,
    minerDataLoading,
    minerSwtrDataLoading,
    marketDataLoading,
    nftDataLoading,
    minerData,
    minerSwtrData,
    marketData,
    nftData,
  ]);

  return (
    <div className={styles.screen}>
      <Container maxWidth='xs'>
        {invalidAddress ? (
          <InvalidAddressDialog />
        ) : (
          <>
            <div className={styles.toolbar}>
              {/*<HintPage />*/}

              <Typography variant='h2' fontWeight='bold' align='center'>
                {t('Profile')}
              </Typography>

              <div className={styles.username}>
                <CopyText
                  text={`https://t.me/pixel_wallet_bot/wallet?startapp=profile-${userAddress}`}
                >
                  {({ copy }) => (
                    <Button className={styles.copy__button} onClick={copy}>
                      <Typography sx={{ fontSize: 16 }}>
                        {userAddress.slice(0, 5) + '...' + userAddress.slice(-3)}
                      </Typography>
                      <IconCopy />
                    </Button>
                  )}
                </CopyText>

                <ChainSwitcher isRelative />
              </div>
            </div>

            <ProfileCard
              speedLevel={minerData?.drillLevel}
              sizeLevel={minerData?.storageLevel}
              image={userInfo?.image}
              loadingUser={loadingUser}
            />

            <div className={styles.achi__area}>
              <Typography align='center' fontWeight='bold' color='text.primary'>
                {t('Achievements')}
              </Typography>

              <Paper variant='outlined' className={styles.card} sx={{ mt: 1 }}>
                {false && (
                  <div className={styles.achi__list}>
                    <AchievementImage />
                    <AchievementImage />
                    <AchievementImage />
                  </div>
                )}
                <div>
                  {/*<ProgressLine name={`Level ${userRank?.level}`} description='1 000 / 5 000' value={userRank?.level || 0} total={achievementsRangList.length} />*/}
                  <ProgressLine
                    name={`${t('Rank')} ${userRank?.level || 0}: ${userRank?.name || '0'}`}
                    description={`${userRank?.points || 0} / ${nextUserRank?.points || 0}`}
                    value={userRank?.points || 0}
                    total={nextUserRank?.points || userRank?.points || 0}
                  />
                </div>
              </Paper>
            </div>

            {IS_PROFILE_KARMA && userInfo?.karma && <KarmaBlock karma={userInfo.karma} />}

            <Paper variant='outlined' className={styles.stats__area}>
              <div className={styles.items__tabs}>
                <Tabs
                  onChange={(_, v) => onChangeTab?.(v as TabProfileStatsEnum)}
                  value={tab}
                  scrollButtons='auto'
                  variant='scrollable'
                >
                  <Tab
                    id={TabProfileStatsEnum.miner}
                    label={t('Miner')}
                    value={TabProfileStatsEnum.miner}
                  />
                  {isMarket && (
                    <Tab
                      id={TabProfileStatsEnum.market}
                      label={t('Market')}
                      value={TabProfileStatsEnum.market}
                    />
                  )}
                  {IS_PROFILE_NFT && (
                    <Tab id={TabProfileStatsEnum.nft} label='Nft' value={TabProfileStatsEnum.nft} />
                  )}
                  {IS_PROFILE_CHEST && (
                    <Tab
                      id={TabProfileStatsEnum.chest}
                      label='Chest'
                      value={TabProfileStatsEnum.chest}
                    />
                  )}
                  {account.chainId !== Chain.SWISSTEST && (
                    <Tab
                      id={TabProfileStatsEnum.swtr}
                      label={t('Swisstronik')}
                      value={TabProfileStatsEnum.swtr}
                    />
                  )}
                </Tabs>
              </div>

              <Divider />

              {isLoading && <CircularProgress sx={{ mx: 'auto', my: '100px', display: 'flex' }} />}

              {tab === TabProfileStatsEnum.miner && (
                <>
                  {minerData && (
                    <UserStatsList
                      options={getUserMinerOptions(minerData, { t, symbol: 'PXLs' })}
                    />
                  )}
                  {!minerData && !minerDataLoading && (
                    <Typography sx={{ pl: 2 }}>{t('No data')}</Typography>
                  )}
                </>
              )}

              {tab === TabProfileStatsEnum.market && (
                <>
                  {marketData && (
                    <UserStatsList
                      options={getUserMarketOptions(marketData, { t, symbol: token.data?.symbol })}
                    />
                  )}
                  {!marketData && !marketDataLoading && (
                    <Typography sx={{ pl: 2 }}>{t('No data')}</Typography>
                  )}
                </>
              )}

              {tab === TabProfileStatsEnum.nft && (
                <>
                  {nftData && <UserStatsList options={getUserNftOptions(nftData, { t })} />}
                  {!nftData && !nftDataLoading && (
                    <Typography sx={{ pl: 2 }}>{t('No data')}</Typography>
                  )}
                </>
              )}

              {tab === TabProfileStatsEnum.chest && (
                <>
                  {chestData && (
                    <UserStatsList
                      options={getUserChestOptions(chestData, {
                        t,
                        coreSymbol: 'PXLs',
                        coinSymbol: trade?.symbol,
                      })}
                    />
                  )}
                  {!chestData && !chestDataLoading && (
                    <Typography sx={{ pl: 2 }}>{t('No data')}</Typography>
                  )}
                </>
              )}

              {tab === TabProfileStatsEnum.swtr && (
                <>
                  {minerSwtrData && (
                    <UserStatsList
                      options={getUserMinerOptions(minerSwtrData, {
                        t,
                        symbol: 'SWTRs',
                        balance: true,
                      })}
                    />
                  )}
                  {!minerSwtrData && !minerSwtrDataLoading && (
                    <Typography sx={{ pl: 2 }}>{t('No data')}</Typography>
                  )}
                </>
              )}
            </Paper>
          </>
        )}
      </Container>
    </div>
  );
};

export default UserPage;
