import TokenIcon from '@mui/icons-material/Token';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import CallToActionIcon from '@mui/icons-material/CallToAction';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import { getPercentage } from 'utils/format/number.ts';
import React from 'react';
import { IGetMinerUserResponse } from 'lib/Mining/api-server/miner.types.ts';
import type { TFunction } from 'i18next';
import { IGetMarketUserResponse } from 'lib/Marketplace/api-server/market.types.ts';
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';
import LocalGroceryStoreIcon from '@mui/icons-material/LocalGroceryStore';
import EqualizerIcon from '@mui/icons-material/Equalizer';
import { IGetNftUserResponse, NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import { IGetChestUserResponse } from 'lib/Chests/api-server/chest.types.ts';

export function getUserMinerOptions(
  minerData: IGetMinerUserResponse,
  { t, symbol, balance }: { t: TFunction; symbol: string; balance?: true }
) {
  return [
    ...(balance
      ? [
          {
            label: t('Balance'),
            value: minerData.balance,
            suffix: symbol,
            icon: <TokenIcon />,
            colorIcon: '#A62CFF',
          },
        ]
      : []),
    {
      label: t('Claimed tokens'),
      value: minerData.claimedTokens,
      suffix: symbol,
      icon: <TokenIcon />,
      colorIcon: '#A62CFF',
    },
    {
      label: t('Burned tokens'),
      value: minerData.burnedTokens,
      suffix: symbol,
      icon: <WhatshotIcon />,
      colorIcon: '#E62058',
    },
    {
      label: t('Claim actions'),
      value: minerData.claimsCount,
      icon: <CallToActionIcon />,
      colorIcon: '#07AA34',
    },
    {
      label: t('Referrals'),
      value: minerData.referralsCount,
      icon: <PeopleAltIcon />,
      colorIcon: '#A62CFF',
    },
    {
      label: t('Active Referrals'),
      value: minerData.referralsLastWeekActive,
      suffix: `(${
        getPercentage(minerData.referralsLastWeekActive, minerData.referralsCount).toFixed(2) + '%'
      })`,
      icon: <PeopleAltIcon />,
      colorIcon: '#A62CFF',
    },
    {
      label: t('Referral Rewards'),
      value: minerData.referralTokens,
      suffix: symbol,
      icon: <TokenIcon />,
      colorIcon: '#07AA34',
    },
  ];
}

export function getUserMarketOptions(
  marketData: IGetMarketUserResponse,
  { t, symbol }: { t: TFunction; symbol?: string }
) {
  return [
    {
      label: t('Created orders'),
      value: marketData.sellerOrders,
      icon: <TokenIcon />,
      colorIcon: '#2c76ff',
    },
    {
      label: t('Current orders'),
      value: marketData.currentOrders,
      icon: <WhatshotIcon />,
      colorIcon: '#07AA34',
    },
    {
      label: t('Received amount'),
      value: marketData.receivedAmount,
      icon: <AccountBalanceWalletIcon />,
      suffix: symbol,
      colorIcon: '#07AA34',
    },
    {
      label: t('Paid amount'),
      value: marketData.paidAmount,
      suffix: symbol,
      icon: <AccountBalanceWalletIcon />,
      colorIcon: '#E62058',
    },
    {
      label: t('Paid orders'),
      value: marketData.buyerOrders,
      icon: <LocalGroceryStoreIcon />,
      colorIcon: '#E62058',
    },
    {
      label: t('Trade volume'),
      value: marketData.tradeVolume,
      suffix: symbol,
      icon: <EqualizerIcon />,
      colorIcon: '#A62CFF',
    },
  ];
}

export function getUserNftOptions(nftData: IGetNftUserResponse, { t }: { t: TFunction }) {
  return [
    {
      label: t('Total'),
      value: nftData.total,
      icon: <TokenIcon />,
      colorIcon: '#A62CFF',
    },

    {
      label: getNftRarityLabel(NftItemRarity.Legendary),
      value: nftData.rarity5,
      icon: <TokenIcon />,
      colorIcon: '#FF7A00',
    },
    {
      label: getNftRarityLabel(NftItemRarity.Epic),
      value: nftData.rarity4,
      icon: <TokenIcon />,
      colorIcon: '#8527D4',
    },

    {
      label: getNftRarityLabel(NftItemRarity.Rare),
      value: nftData.rarity3,
      icon: <TokenIcon />,
      colorIcon: '#0094FF',
    },

    {
      label: getNftRarityLabel(NftItemRarity.UnCommon),
      value: nftData.rarity2,
      icon: <TokenIcon />,
      colorIcon: '#05C618',
    },

    {
      label: getNftRarityLabel(NftItemRarity.Common),
      value: nftData.rarity1,
      icon: <TokenIcon />,
      colorIcon: '#7c7c7c',
    },
  ];
}

export function getUserChestOptions(
  chestData: IGetChestUserResponse,
  { t, coreSymbol, coinSymbol }: { t: TFunction; coinSymbol: string; coreSymbol: string }
) {
  return [
    {
      label: t('Coin Chests'),
      value: chestData.coinChests,
      icon: <TokenIcon />,
      colorIcon: '#A62CFF',
    },
    {
      label: t('Pixel Chests'),
      value: chestData.pixelChests,
      icon: <TokenIcon />,
      colorIcon: '#07AA34',
    },
    {
      label: t('Spent Coin'),
      value: chestData.coinSpent,
      suffix: coinSymbol,
      icon: <AccountBalanceWalletIcon />,
      colorIcon: '#E62058',
    },
    {
      label: t('Spent Pixel'),
      value: chestData.pixelSpent,
      suffix: coreSymbol,
      icon: <AccountBalanceWalletIcon />,
      colorIcon: '#07AA34',
    },
  ];
}
