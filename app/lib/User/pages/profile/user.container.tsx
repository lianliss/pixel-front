import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react';
import {
  clearMinerUser,
  loadMinerUser,
  loadMinerUserSwtr,
  minerUserStore,
  minerUserSwtrStore,
} from 'lib/Mining/api-server/miner.store.ts';
import {
  clearMarketUser,
  loadMarketUser,
  marketUserStore,
} from 'lib/Marketplace/api-server/market.store.ts';
import { Chain } from 'services/multichain/chains';
import { isAddress } from 'web3-utils';
import {
  chestUserStore,
  clearChestUser,
  loadChestUser,
} from 'lib/Chests/api-server/chest.store.ts';
import { clearNftUser, loadNftUser, nftUserStore } from 'lib/NFT/api-server/nft.store.ts';
import UserPage from 'lib/User/pages/profile/user.page.tsx';
import { HIDDEN_PROFILE_USER } from 'lib/User/utils/user.utils.ts';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import {
  achievementsRangListStore,
  achievementUserRangStore,
  loadAchievementRangList,
  loadAchievementUserRang,
} from 'lib/Achievement/api-server/achievement.store.ts';
import { useUserInfo } from 'lib/User/hooks/useUserInfo.ts';
import { AchievementGroup } from 'lib/Achievement/api-server/achievement.types.ts';

export enum TabProfileStatsEnum {
  miner = 'miner',
  market = 'market',
  nft = 'nft',
  swtr = 'swtr',
  chest = 'chest',
}

const MINER_CONTRACTS = {
  [Chain.SONGBIRD]: 'sgb-pixel',
  [Chain.SKALE]: 'skale-pixel',
  [Chain.SWISSTEST]: 'swtr-testnet-pixel',
  [Chain.SKALE_TEST]: 'skale-testnet-pixel',
  [Chain.UNITS]: 'units-pixel',
};
const MARKET_CONTRACT = {
  [Chain.SONGBIRD]: 'sgb-market',
  [Chain.SKALE]: 'skale-market',
  [Chain.SKALE_TEST]: 'skale-testnet-market',
  [Chain.UNITS]: 'units-market',
};
const NFT_CONTRACT = {
  [Chain.SONGBIRD]: 'sgb-pixel',
  [Chain.SKALE]: 'skale-pixel',
  [Chain.SWISSTEST]: 'swtr-testnet-pixel',
  [Chain.SKALE_TEST]: 'skale-testnet-pixel',
  [Chain.UNITS]: 'units-pixel',
};
const CHEST_CONTRACT = {
  [Chain.SONGBIRD]: 'sgb-pixel',
  [Chain.SKALE]: 'skale-pixel',
  [Chain.SKALE_TEST]: 'skale-testnet-pixel',
  [Chain.UNITS]: 'units-pixel',
};

function UserContainer() {
  const params = useParams<{ address: string | undefined }>();
  const account = useAccount();
  const { chainId } = account;

  const [tab, setTab] = React.useState<TabProfileStatsEnum>(TabProfileStatsEnum.miner);

  const accountAddress = params.address || account.accountAddress;
  const isHidden = HIDDEN_PROFILE_USER[accountAddress];
  const isExternalUser = !!params.address;
  const invalidAddress =
    (params.address && !isAddress(params.address)) || (isExternalUser && isHidden);

  const minerData = useUnit(minerUserStore);
  const minerSwtrData = useUnit(minerUserSwtrStore);
  const marketData = useUnit(marketUserStore);
  const chestData = useUnit(chestUserStore);
  const nftData = useUnit(nftUserStore);

  const achievementUserRang = useUnit(achievementUserRangStore);
  const achievementsRangList = useUnit(achievementsRangListStore);

  const userInfo = useUserInfo({
    address: accountAddress,
  });

  useEffect(() => {
    if (!invalidAddress && accountAddress && tab === TabProfileStatsEnum.miner) {
      if (chainId in MINER_CONTRACTS) {
        void loadMinerUser({ userAddress: accountAddress, contract: MINER_CONTRACTS[chainId] });
      } else {
        clearMinerUser();
      }
    }
  }, [invalidAddress, accountAddress, chainId, tab]);
  useEffect(() => {
    if (!invalidAddress && accountAddress && tab === TabProfileStatsEnum.market) {
      if (chainId in MARKET_CONTRACT) {
        void loadMarketUser({
          userAddress: accountAddress,
          contract: MARKET_CONTRACT[chainId],
        });
      } else {
        clearMarketUser();
      }
    }
  }, [invalidAddress, accountAddress, chainId, tab]);
  useEffect(() => {
    if (!invalidAddress && accountAddress && tab === TabProfileStatsEnum.nft) {
      if (chainId in NFT_CONTRACT) {
        void loadNftUser({
          userAddress: accountAddress.toLowerCase(),
          contract: NFT_CONTRACT[chainId],
        });
      } else {
        clearNftUser();
      }
    }
  }, [invalidAddress, accountAddress, chainId, tab]);
  useEffect(() => {
    if (!invalidAddress && accountAddress && tab === TabProfileStatsEnum.chest) {
      if (chainId in CHEST_CONTRACT) {
        void loadChestUser({
          userAddress: accountAddress,
          contract: CHEST_CONTRACT[chainId],
        });
      } else {
        clearChestUser();
      }
    }
  }, [invalidAddress, accountAddress, chainId, tab]);

  useEffect(() => {
    if (!invalidAddress && accountAddress && chainId !== Chain.SWISSTEST) {
      void loadMinerUserSwtr({
        userAddress: accountAddress,
        contract: MINER_CONTRACTS[Chain.SWISSTEST],
      });
    }
  }, [invalidAddress, accountAddress]);

  useEffect(() => {
    if (!invalidAddress && accountAddress) {
      void loadAchievementRangList({
        userAddress: accountAddress,
        group: AchievementGroup.Default,
      });
      void loadAchievementUserRang({
        userAddress: accountAddress,
        group: AchievementGroup.Default,
      });
    }
  }, [accountAddress, invalidAddress]);
  useBackAction('/wallet');

  const userRank = achievementsRangList?.find(
    (rank) => rank.level === achievementUserRang.data?.level
  );
  const userNextRank = achievementsRangList?.find(
    (rank) => rank.level === (achievementUserRang.data?.level || 0) + 1
  );

  return (
    <UserPage
      invalidAddress={invalidAddress}
      userAddress={accountAddress || ''}
      // miner
      minerData={minerData.state}
      minerDataLoading={minerData.loading}
      minerSwtrData={minerSwtrData.state}
      minerSwtrDataLoading={minerSwtrData.loading}
      // nft
      nftData={nftData.state}
      nftDataLoading={nftData.loading}
      // market
      marketData={marketData.state}
      marketDataLoading={marketData.loading}
      // chest
      chestData={chestData.state}
      chestDataLoading={chestData.loading}
      // achievements
      achievementRang={userRank}
      achievementNextRang={userNextRank}
      // user
      loadingUser={userInfo.loading}
      userInfo={userInfo.data}
      // utils
      tab={tab}
      onChangeTab={(t) => setTab(t)}
    />
  );
}

export default UserContainer;
