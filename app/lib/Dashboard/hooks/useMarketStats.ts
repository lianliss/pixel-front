import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react/effector-react.umd';
import { loadMarketStats, marketStatsStore } from 'lib/Marketplace/api-server/market.store.ts';
import React from 'react';
import { Chain } from 'services/multichain/chains';
import { IMarketStatusResponse } from 'lib/Marketplace/api-server/market.types.ts';

const MAP_MARKET_NAME = {
  [Chain.SONGBIRD]: 'sgb-market',
  [Chain.COSTON2]: 'flare-testnet-market',
  [Chain.SKALE_TEST]: 'skale-testnet-market',
  [Chain.SKALE]: 'skale-market',
  [Chain.UNITS]: 'units-market',
};

export function useMarketStats(): {
  data: IMarketStatusResponse | null;
  error: Error | null;
  loading: boolean;
  enabled: boolean;
} {
  const { chainId } = useAccount();

  const { state: data, error, loading } = useUnit(marketStatsStore);

  const marketKey = MAP_MARKET_NAME[chainId];

  React.useEffect(() => {
    if (chainId && marketKey) {
      void loadMarketStats({ marketContract: marketKey });
    }
  }, [chainId]);

  return {
    data,
    error,
    loading,
    enabled: !!marketKey,
  };
}
