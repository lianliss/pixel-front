import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react/effector-react.umd';
import React from 'react';
import { Chain } from 'services/multichain/chains';
import { INftStatusResponse } from 'lib/NFT/api-server/nft.types.ts';
import { loadNftStats, nftStatsStore } from 'lib/NFT/api-server/nft.store.ts';

const MAP_NFT_NAME = {
  [Chain.SONGBIRD]: 'sgb-pixel',
  [Chain.COSTON2]: 'flare-testnet-pixel',
  [Chain.SWISSTEST]: 'swtr-testnet-pixel',
  [Chain.SKALE_TEST]: 'skale-testnet-pixel',
  [Chain.SKALE]: 'skale-pixel',
  [Chain.UNITS]: 'units-pixel',
};

export function useNftStats(): {
  data: INftStatusResponse | null;
  error: Error | null;
  loading: boolean;
  enabled: boolean;
} {
  const { chainId } = useAccount();

  const { state: data, error, loading } = useUnit(nftStatsStore);

  const key = MAP_NFT_NAME[chainId];

  React.useEffect(() => {
    if (chainId && key) {
      void loadNftStats({ nftContract: key });
    }
  }, [chainId]);

  return {
    data,
    error,
    loading,
    enabled: !!key,
  };
}
