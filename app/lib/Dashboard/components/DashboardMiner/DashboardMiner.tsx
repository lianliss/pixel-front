'use strict';
import React from 'react';
import styles from './DashboardMiner.module.scss';
import { formatNumber, formatNumberSpacing } from 'utils/number';
import { useUnit } from 'effector-react/effector-react.umd';
import { minerStatsStore, loadMinerStats } from 'lib/Mining/api-server/miner.store.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import DashboardItem from 'lib/Dashboard/components/DashboardItem/DashboardItem.tsx';
import DashboardMainItem from 'lib/Dashboard/components/DashboardMainItem/DashboardMainItem.tsx';
import { useCoreToken } from '@chain/hooks/useCoreToken.ts';
import { useTranslation } from 'react-i18next';
import { Chain } from 'services/multichain/chains';
import DashboardSingleItem from 'lib/Dashboard/components/DashboardSingleItem/DashboardItem.tsx';
import { useBalance } from '@chain/hooks/useBalance.ts';

function DashboardMiner() {
  const { chainId } = useAccount();
  const coreToken = useCoreToken();

  const { t } = useTranslation('dashboard');
  const { t: n } = useTranslation('notifications');

  const balanceState = useBalance({
    address: undefined,
  });
  const balance = balanceState.data?.formatted;

  const { state: data, error, loading } = useUnit(minerStatsStore);

  React.useEffect(() => {
    if (chainId) {
      void loadMinerStats({ chainId });
    }
  }, [chainId]);

  if (error) {
    return `${n('Failed to fetch')}..`;
  }

  return (
    <div className={styles.root}>
      <DashboardMainItem
        loading={!data && loading}
        top={{
          label: t('Total wallet created'),
          value: data ? formatNumberSpacing(data.totalCreatedUsers) : undefined,
        }}
        secondTop={
          chainId !== Chain.SONGBIRD
            ? {
                label: t('Total network holders'),
                value: data ? formatNumberSpacing(data.totalHolders) : undefined,
              }
            : undefined
        }
        left={{
          label: t('Current miner supply'),
          value: data ? formatNumber(data.notMintedSupply) : undefined,
        }}
        right={{
          label: t('Total transaction'),
          value: data ? formatNumber(data.totalTransactions) : undefined,
        }}
      />

      <DashboardItem
        loading={!data && loading}
        left={{ label: t('Online 24h'), value: data ? formatNumber(data.onlineUsers) : undefined }}
        right={{
          label: t('Transaction 24h'),
          value: data ? formatNumber(data.lastDayTransactions) : undefined,
        }}
      />

      <DashboardItem
        loading={!data && loading}
        left={{
          label: `${t('Total claimed')} ${coreToken.data?.symbol}`,
          value: data ? formatNumber(data.totalClaimed) : undefined,
        }}
        right={{
          label: `${t('Total burned')} ${coreToken.data?.symbol}`,
          value: data ? formatNumber(data.totalBurned) : undefined,
        }}
      />
    </div>
  );
}

export default DashboardMiner;
