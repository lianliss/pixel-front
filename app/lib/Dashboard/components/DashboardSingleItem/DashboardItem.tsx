'use strict';
import React from 'react';
import styles from './DashboardItem.module.scss';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import Skeleton from '@mui/material/Skeleton';

type Props = {
  loading?: boolean;
  label: string;
  value: React.ReactNode;
};

function DashboardSingleItem(props: Props) {
  const { label, value, loading = false } = props;

  return (
    <Paper className={styles.dashboard__row}>
      <div className={styles.dashboard__item}>
        <p className={styles.dashboard__itemLabel}>{label}</p>
        <p className={styles.dashboard__itemValue}>
          {loading ? <Skeleton variant='text' sx={{ fontSize: '1rem', width: '100px' }} /> : value}
        </p>
      </div>
    </Paper>
  );
}

export default DashboardSingleItem;
