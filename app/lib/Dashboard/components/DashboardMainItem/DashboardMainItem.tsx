'use strict';
import React from 'react';
import styles from './DashboardMainItem.module.scss';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import Skeleton from '@mui/material/Skeleton';

type Props = {
  loading?: boolean;
  left: { label: string; value: React.ReactNode };
  right: { label: string; value: React.ReactNode };
  top: { label: string; value: React.ReactNode };
  secondTop?: { label: string; value: React.ReactNode };
};

function DashboardMainItem(props: Props) {
  const { left, right, top, loading, secondTop } = props;

  return (
    <Paper elevation={0} variant='outlined' className={styles.dashboard__mainRow}>
      <div className={styles.dashboard__singleItem}>
        <p className={styles.dashboard__itemLabel}>{top.label}</p>
        <p className={styles.dashboard__itemValue}>
          {loading ? (
            <Skeleton variant='text' sx={{ fontSize: '1rem', width: '100px' }} />
          ) : (
            top.value
          )}
        </p>
      </div>

      {secondTop && (
        <>
          <Divider />
          <div className={styles.dashboard__singleItem}>
            <p className={styles.dashboard__itemLabel}>{secondTop.label}</p>
            <p className={styles.dashboard__itemValue}>
              {loading ? (
                <Skeleton variant='text' sx={{ fontSize: '1rem', width: '100px' }} />
              ) : (
                secondTop.value
              )}
            </p>
          </div>
        </>
      )}

      <Divider />

      <div>
        <div className={styles.dashboard__item}>
          <p className={styles.dashboard__itemLabel}>{left.label}</p>
          <p className={styles.dashboard__itemValue}>
            {loading ? (
              <Skeleton variant='text' sx={{ fontSize: '1rem', width: '100px' }} />
            ) : (
              left.value
            )}
          </p>
        </div>

        <Divider orientation='vertical' flexItem />

        <div className={styles.dashboard__item}>
          <p className={styles.dashboard__itemLabel}>{right.label}</p>
          <p className={styles.dashboard__itemValue}>
            {loading ? (
              <Skeleton variant='text' sx={{ fontSize: '1rem', width: '100px' }} />
            ) : (
              right.value
            )}
          </p>
        </div>
      </div>
    </Paper>
  );
}

export default DashboardMainItem;
