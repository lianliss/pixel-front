'use strict';
import React from 'react';
import styles from './DashboardMarket.module.scss';
import { formatNumber } from 'utils/number';
import DashboardItem from 'lib/Dashboard/components/DashboardItem/DashboardItem.tsx';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import DashboardMainItem from 'lib/Dashboard/components/DashboardMainItem/DashboardMainItem.tsx';
import { useTokenPrice } from '@chain/hooks/useTokenPrice.ts';
import { useMarketStats } from 'lib/Dashboard/hooks/useMarketStats.ts';
import { useTranslation } from 'react-i18next';

function DashboardMarket() {
  const { t } = useTranslation('dashboard');

  const tradeToken = useTradeToken();
  const tokenPrice = useTokenPrice({
    address: tradeToken.data.address,
  });
  const coinRate = tokenPrice.data;

  const { data, error, loading } = useMarketStats();
  const tokenSymbol = tradeToken?.symbol;

  if (error) {
    return 'Failed to fetch..';
  }

  return (
    <div className={styles.root}>
      <DashboardMainItem
        loading={!data && loading}
        top={{
          label: t('Total Volume $'),
          value:
            typeof coinRate === 'number' && data
              ? formatNumber(+(data.tradeVolume * coinRate).toFixed(2))
              : t('Loading..'),
        }}
        left={{
          label: `${t('Total Volume')} ${tokenSymbol}`,
          value: data ? formatNumber(data.tradeVolume) : undefined,
        }}
        right={{
          label: `${t('24h Volume')} ${tokenSymbol}`,
          value: data ? formatNumber(data.lastDayTradeVolume) : undefined,
        }}
      />

      <DashboardItem
        loading={!data && loading}
        left={{
          label: t('Unique sellers'),
          value: data ? formatNumber(data.uniqueSellers) : undefined,
        }}
        right={{
          label: t('Unique sellers 7d'),
          value: data ? formatNumber(data.lastWeekUniqueSellers) : undefined,
        }}
      />

      <DashboardItem
        loading={!data && loading}
        left={{
          label: t('Completed orders'),
          value: data ? formatNumber(data.completedOrders) : undefined,
        }}
        right={{
          label: t('Completed orders 24h'),
          value: data ? formatNumber(data.lastDayCompletedOrders) : undefined,
        }}
      />

      <DashboardItem
        loading={!data && loading}
        left={{
          label: t('Current orders'),
          value: data ? formatNumber(data.currentOrders) : undefined,
        }}
        right={{
          label: t('New orders 24h'),
          value: data ? formatNumber(data.lastDayNewOrders) : undefined,
        }}
      />
    </div>
  );
}

export default DashboardMarket;
