'use strict';
import React from 'react';
import styles from './DashboardDroneMiner.module.scss';
import { formatNumber } from 'utils/number';
import DashboardMainItem from 'lib/Dashboard/components/DashboardMainItem/DashboardMainItem.tsx';
import { useUnit } from 'effector-react';
import {
  loadMinerClaimerStats,
  minerClaimerStatsStore,
} from 'lib/MinerClaimer/api-server/miner-claimer.store.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTranslation } from 'react-i18next';
import { MINER_CLAIMER_CONTRACT_ID } from 'lib/MinerClaimer/api-contract/miner-claimer.constants.ts';

function DashboardDroneMiner() {
  const { chainId } = useAccount();
  const claimerAddress = MINER_CLAIMER_CONTRACT_ID[chainId];

  const { data, isFetched, loading } = useUnit(minerClaimerStatsStore);

  const { t } = useTranslation('dashboard');

  React.useEffect(() => {
    if (claimerAddress) {
      loadMinerClaimerStats({ contractId: claimerAddress }).then();
    }
  }, [claimerAddress]);

  if (!data && isFetched) {
    return 'Failed to fetch..';
  }

  return (
    <div className={styles.root}>
      <DashboardMainItem
        loading={!data && loading}
        top={{
          label: t('Total claimers'),
          value: data ? formatNumber(data.total) : undefined,
        }}
        left={{
          label: t('Active claimers'),
          value: data ? formatNumber(data.started) : undefined,
        }}
        right={{
          label: t('Inactive claimers'),
          value: data ? formatNumber(data.stopped) : undefined,
        }}
      />
    </div>
  );
}

export default DashboardDroneMiner;
