'use strict';
import React from 'react';
import styles from './DashboardNft.module.scss';
import { useUnit } from 'effector-react/effector-react.umd';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain, COSTON2, SKALE_TEST, SONGBIRD, SWISSTEST } from 'services/multichain/chains';
import { loadNftStats, nftStatsStore } from 'lib/NFT/api-server/nft.store.ts';
import DashboardMainItem from 'lib/Dashboard/components/DashboardMainItem/DashboardMainItem.tsx';
import { formatNumber } from 'utils/number';
import DashboardItem from 'lib/Dashboard/components/DashboardItem/DashboardItem.tsx';
import { getPercentageText } from 'utils/format/number.ts';
import { getNftSlotLabel } from 'lib/NFT/utils/getNftSlotLabel.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { getNftCollectionLabel } from 'lib/NFT/utils/getNftCollectionLabel.ts';
import { useTranslation } from 'react-i18next';

const MAP_NFT_NAME = {
  [Chain.SONGBIRD]: 'sgb-pixel',
  [Chain.COSTON2]: 'flare-testnet-pixel',
  [Chain.SWISSTEST]: 'swtr-testnet-pixel',
  [Chain.SKALE_TEST]: 'skale-testnet-pixel',
  [Chain.SKALE]: 'skale-pixel',
  [Chain.UNITS]: 'units-pixel',
};

function DashboardNft() {
  const { chainId } = useAccount();

  const { t } = useTranslation('dashboard');

  const { state: data, error, loading } = useUnit(nftStatsStore);

  React.useEffect(() => {
    const nftKey = MAP_NFT_NAME[chainId];

    if (chainId && nftKey) {
      void loadNftStats({ nftContract: nftKey });
    }
  }, [chainId]);

  if (error) {
    return 'Failed to fetch..';
  }

  const pocketSlots = data ? data.slot0 + data.slot1 + data.slot3 + data.slot5 : 0;

  const slotData = [
    ...[8, 7, 2, 4, 9, 10, 11, 12].map((el) => ({
      index: el,
      value: data?.[`slot${el}`] || 0,
    })),
    {
      index: 0,
      value: pocketSlots,
    },
  ].sort((a, b) => b.value - a.value);
  const collectionData = (chainId === Chain.SONGBIRD ? [2, 4, 7, 6, 1, 3, 5, 8] : [1, 2, 3, 4])
    .map((el) => ({
      index: el,
      value: data?.[`collection${el}`] || 0,
    }))
    .sort((a, b) => b.value - a.value);

  return (
    <div className={styles.root}>
      <DashboardMainItem
        loading={!data && loading}
        top={{ label: t('Total NFT'), value: data ? formatNumber(data.total) : undefined }}
        left={{ label: t('Legendary count'), value: data ? formatNumber(data.rarity5) : undefined }}
        right={{
          label: t('Legendary of total'),
          value: data ? getPercentageText(data.rarity5, data.total) : undefined,
        }}
      />

      <DashboardItem
        loading={!data && loading}
        left={{ label: t('Epic count'), value: data ? formatNumber(data.rarity4) : undefined }}
        right={{
          label: t('Epic of total'),
          value: data ? getPercentageText(data.rarity4, data.total) : undefined,
        }}
      />
      <DashboardItem
        loading={!data && loading}
        left={{ label: t('Rare count'), value: data ? formatNumber(data.rarity3) : undefined }}
        right={{
          label: t('Rare of total'),
          value: data ? getPercentageText(data.rarity3, data.total) : undefined,
        }}
      />
      <DashboardItem
        loading={!data && loading}
        left={{ label: t('Uncommon count'), value: data ? formatNumber(data.rarity2) : undefined }}
        right={{
          label: t('Uncommon of total'),
          value: data ? getPercentageText(data.rarity2, data.total) : undefined,
        }}
      />
      <DashboardItem
        loading={!data && loading}
        left={{ label: t('Common count'), value: data ? formatNumber(data.rarity1) : undefined }}
        right={{
          label: t('Common of total'),
          value: data ? getPercentageText(data.rarity1, data.total) : undefined,
        }}
      />

      <Typography variant='h2' fontWeight='bold' align='center'>
        {t('Count by slots')}
      </Typography>

      {slotData.map((el) => (
        <DashboardItem
          loading={!data && loading}
          left={{
            label: `${t('Slot')} ${getNftSlotLabel(el.index)}`,
            value: data ? formatNumber(el.value) || 0 : undefined,
          }}
          right={{
            label: t('Percent of total'),
            value: data ? getPercentageText(el.value, data.total) : undefined,
          }}
        />
      ))}

      <Typography variant='h2' fontWeight='bold' align='center'>
        {t('Count by collections')}
      </Typography>

      {collectionData.map((el) => (
        <DashboardItem
          loading={!data && loading}
          left={{
            label: getNftCollectionLabel(chainId, el.index),
            value: data ? formatNumber(el.value) : undefined,
          }}
          right={{
            label: t('Percent of total'),
            value: data ? getPercentageText(el.value, data.total) : undefined,
          }}
        />
      ))}
    </div>
  );
}

export default DashboardNft;
