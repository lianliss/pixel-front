'use strict';
import React from 'react';
import styles from './DashboardPool.module.scss';
import DashboardItem from 'lib/Dashboard/components/DashboardItem/DashboardItem.tsx';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import DashboardMainItem from 'lib/Dashboard/components/DashboardMainItem/DashboardMainItem.tsx';
import { useTranslation } from 'react-i18next';

function DashboardPool() {
  const { t } = useTranslation('dashboard');

  const tradeToken = useTradeToken();
  const tokenSymbol = tradeToken?.symbol;

  return (
    <div className={styles.root}>
      <DashboardMainItem
        top={{
          label: t(`Total ${tokenSymbol}`),
          value: 'Soon',
        }}
        left={{
          label: t('Transaction pays'),
          value: 'Soon',
        }}
        right={{
          label: t('Karma system penalty'),
          value: 'Soon',
        }}
      />

      <DashboardItem
        left={{
          label: t('Market commission'),
          value: 'Soon',
        }}
        right={{
          label: t('Collection sales'),
          value: 'Soon',
        }}
      />
    </div>
  );
}

export default DashboardPool;
