'use strict';
import React from 'react';
import styles from './DashboardItem.module.scss';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import Skeleton from '@mui/material/Skeleton';

type Props = {
  loading?: boolean;
  left: { label: string; value: React.ReactNode };
  right?: { label: string; value: React.ReactNode };
};

function DashboardItem(props: Props) {
  const { left, right, loading = false } = props;

  return (
    <Paper elevation={0} variant='outlined' className={styles.dashboard__row}>
      <div className={styles.dashboard__item}>
        <p className={styles.dashboard__itemLabel}>{left.label}</p>
        <p className={styles.dashboard__itemValue}>
          {loading ? (
            <Skeleton variant='text' sx={{ fontSize: '1rem', width: '100px' }} />
          ) : (
            left.value
          )}
        </p>
      </div>
      {right && (
        <>
          <Divider orientation='vertical' flexItem />

          <div className={styles.dashboard__item}>
            <p className={styles.dashboard__itemLabel}>{right.label}</p>
            <p className={styles.dashboard__itemValue}>
              {loading ? (
                <Skeleton variant='text' sx={{ fontSize: '1rem', width: '100px' }} />
              ) : (
                right.value
              )}
            </p>
          </div>
        </>
      )}
    </Paper>
  );
}

export default DashboardItem;
