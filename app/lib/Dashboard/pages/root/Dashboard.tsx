'use strict';
import React, { useState } from 'react';
import styles from './Dashboard.module.scss';
import { useNavigate } from 'react-router-dom';
import { Button } from '@ui-kit/Button/Button.tsx';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import { Chain, COSTON2, SKALE_TEST, SONGBIRD, SWISSTEST } from 'services/multichain/chains';
import DashboardMiner from 'lib/Dashboard/components/DashboardMiner/DashboardMiner.tsx';
import { Tab } from '@ui-kit/Tab/Tab.tsx';
import { Tabs } from '@ui-kit/Tabs/Tabs.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import DashboardMarket from 'lib/Dashboard/components/DashboardMarket/DashboardMarket.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import DashboardNft from 'lib/Dashboard/components/DashboardNft/DashboardNft.tsx';
import { IS_DEVELOP, IS_STAGE } from '@cfg/config.ts';
import DashboardDroneMiner from 'lib/Dashboard/components/DashboardDroneMiner/DashboardDroneMiner.tsx';
import { useTranslation } from 'react-i18next';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';
import DashboardPool from 'lib/Dashboard/components/DashboardPool/DashboardPool.tsx';
import Container from '@mui/material/Container';

function Dashboard() {
  const { chainId, isConnected } = useAccount();
  const navigate = useNavigate();
  const [tab, setTab] = useState('miner');

  const { t } = useTranslation('dashboard');
  useBackAction('/wallet');

  const minerAvailable =
    [Chain.SONGBIRD, Chain.SWISSTEST, Chain.SKALE_TEST, Chain.SKALE, Chain.UNITS].includes(
      chainId
    ) || !isConnected;
  const marketAvailable = [
    Chain.SONGBIRD,
    Chain.COSTON2,
    Chain.SKALE_TEST,
    Chain.SKALE,
    Chain.UNITS,
  ].includes(chainId);
  const nftAvailable = [
    Chain.SONGBIRD,
    Chain.SWISSTEST,
    Chain.COSTON2,
    Chain.SKALE_TEST,
    Chain.SKALE,
    Chain.UNITS,
  ].includes(chainId);
  const smartClaimerAvailable = [Chain.SONGBIRD, Chain.SKALE].includes(chainId);

  const handleBack = () => {
    navigate('/wallet');
  };

  return (
    <div className={styles.dashboard}>
      <Container maxWidth='xs'>
        <ChainSwitcher
          chains={[
            Chain.SONGBIRD,
            Chain.SWISSTEST,
            Chain.COSTON2,
            Chain.SKALE_TEST,
            Chain.SKALE,
            Chain.UNITS,
          ]}
        />

        <h1 className={styles.dashboard__title}>{t('Dashboard')}</h1>

        <Tabs
          className={styles.dashboard__tabs}
          onChange={(_, nextTab) => setTab(nextTab)}
          value={tab}
          variant='scrollable'
          scrollButtons='auto'
        >
          <Tab value='miner' label={t('Miner')} disabled={!minerAvailable} />
          <Tab value='market' label={t('Market')} disabled={!marketAvailable} />
          <Tab value='nft' label='NFT' disabled={!nftAvailable} />
          <Tab value='pool' label='Ecosystem Pool' />

          {(IS_STAGE || IS_DEVELOP) && (
            <Tab value='miner-drone' label={t('Smart Claimer')} disabled={!smartClaimerAvailable} />
          )}
        </Tabs>

        <div>
          {tab === 'miner' && minerAvailable && <DashboardMiner />}
          {tab === 'miner' && isConnected && !minerAvailable && (
            <Typography variant='h3' sx={{ mt: 1, mb: 3 }}>
              {t('Dashboard not available')}
            </Typography>
          )}
          {tab === 'market' && marketAvailable && <DashboardMarket />}
          {tab === 'market' && isConnected && !marketAvailable && (
            <Typography variant='h3' sx={{ mt: 1, mb: 3 }}>
              {t('Dashboard not available')}
            </Typography>
          )}
          {tab === 'nft' && nftAvailable && <DashboardNft />}
          {tab === 'nft' && isConnected && !nftAvailable && (
            <Typography variant='h3' sx={{ mt: 1, mb: 3 }}>
              {t('Dashboard not available')}
            </Typography>
          )}

          {tab === 'pool' && <DashboardPool />}

          {tab === 'miner-drone' && minerAvailable && <DashboardDroneMiner />}
          {tab === 'miner-drone' && isConnected && !minerAvailable && (
            <Typography variant='h3' sx={{ mt: 1, mb: 3 }}>
              {t('Dashboard not available')}
            </Typography>
          )}
        </div>

        <Button
          variant='contained'
          size='large'
          fullWidth
          color='primary'
          className={styles.dashboard__btn}
          onClick={handleBack}
        >
          {t('Back to wallet')}
        </Button>
      </Container>
    </div>
  );
}

export default Dashboard;
