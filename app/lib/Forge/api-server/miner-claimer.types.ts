// entities
import { IPaginatedResult } from 'lib/Quests/api-server/quests.types.ts';
import { INftItemDto, INftTypeDto } from 'lib/NFT/api-server/nft.types.ts';

export type INftRecipe<TDate = Date> = {
  id: string;

  recipeTypeId: string;
  resultTypeId: string;
  requirements: number[];
  slots: number[];

  price: number;
  priceWei: string;
  tokenAddress: string;
  tokenShard: boolean;

  removedAt?: TDate;
  createdAt: TDate;

  recipeType: INftTypeDto<TDate> | null;
  resultType: INftTypeDto<TDate> | null;
};

// dto

export type INftRecipeDto<TDate = number> = Omit<
  INftRecipe<TDate>,
  'removedAt' | 'resultType' | 'recipeType'
> & {
  removedAt: TDate | null;
  resultType: INftTypeDto<TDate> | null;
  recipeType: INftTypeDto<TDate> | null;
};

export type INftRecipesDto<TDate = number> = IPaginatedResult<INftRecipeDto<TDate>>;

// request

export type IGetNftRecipeDto = { recipeId: string };

export type IFindUserNftRecipesDto = { userAddress: string; contractId: string };

export type IFindNftRecipesDto = { contractId: string };

// response

export type IGetNftRecipeResponse = INftRecipeDto;

export type IFindUserNftRecipesResponse = IPaginatedResult<INftItemDto>;

export type IFindNftRecipesResponse = IPaginatedResult<INftTypeDto>;
