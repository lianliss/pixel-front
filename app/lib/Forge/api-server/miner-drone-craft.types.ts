import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import { IPaginatedResult } from 'lib/Quests/api-server/quests.types.ts';
import { IPaginatedQuery } from 'lib/Marketplace/api-server/market.types.ts';

export type IFindMinerDroneCraftEntryDto = IPaginatedQuery & { minerDroneCraftAddress: string };

// response

export type IFindMinerDroneCraftEntriesResponse = IPaginatedResult<IMinerDroneCraftEntryDto>;

// entity

export type IMinerDroneCraftEntry<TDate = Date> = {
  id: string;

  minerDroneCraftAddress: string;
  nftAddress: string;
  typeId: string;
  rarity: NftItemRarity;

  createdAt: TDate;
};

// dto

export type IMinerDroneCraftEntryDto<TDate = number> = IMinerDroneCraftEntry<TDate>;
