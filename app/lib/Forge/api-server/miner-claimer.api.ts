'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  IFindNftRecipesDto,
  IFindNftRecipesResponse,
  IFindUserNftRecipesDto,
  IFindUserNftRecipesResponse,
} from 'lib/Forge/api-server/miner-claimer.types.ts';
import { getMockNftDroneRecipe } from 'lib/NFT/utils/mock.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class MinerClaimerApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async findUser(params: IFindUserNftRecipesDto): Promise<IFindUserNftRecipesResponse> {
    // if (Math.random()) {
    //   return {
    //     rows: [getMockNftDroneRecipe(), getMockNftDroneRecipe()],
    //     count: 0,
    //   };
    // }

    const { data } = await axiosInstance.get<
      { data: IFindUserNftRecipesResponse },
      AxiosResponse<{ data: IFindUserNftRecipesResponse }>,
      IFindUserNftRecipesDto
    >(`/api/v1/miner/claimer/craft/find/user`, { baseURL: this.url, params });
    return data.data;
  }

  async find(params: IFindNftRecipesDto): Promise<IFindNftRecipesResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindNftRecipesResponse },
      AxiosResponse<{ data: IFindNftRecipesResponse }>,
      IFindNftRecipesDto
    >(`/api/v1/miner/claimer/craft/find/type`, { baseURL: this.url, params });
    return data.data;
  }
}

export const forgeApi = new MinerClaimerApi({ url: 'https://api.hellopixel.network' });
