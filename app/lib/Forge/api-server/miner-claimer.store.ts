import { createEffect, createStore } from 'effector';
import {
  IFindNftRecipesDto,
  IFindUserNftRecipesDto,
} from 'lib/Forge/api-server/miner-claimer.types.ts';
import { forgeApi } from 'lib/Forge/api-server/miner-claimer.api.ts';
import { INftItemDto, INftTypeDto } from 'lib/NFT/api-server/nft.types.ts';
import { AxiosError } from 'axios';

export const loadUserNftRecipes = createEffect(async (params: IFindUserNftRecipesDto) => {
  const res = await forgeApi.findUser(params);
  return res;
});

export const userNftRecipesStore = createStore<{
  state: { rows: INftItemDto[]; count: number };
  error: Error | null;
  loading: boolean;
  isFetched: boolean;
}>({
  state: { rows: [], count: 0 },
  error: null,
  loading: false,
  isFetched: false,
})
  .on(loadUserNftRecipes.doneData, (prev, next) => ({
    error: null,
    state: next,
    loading: false,
    isFetched: true,
  }))
  .on(loadUserNftRecipes.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: { rows: [], count: 0 },
    isFetched: true,
  }))
  .on(loadUserNftRecipes.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
    isFetched: prev.isFetched,
  }));

// find

export const loadNftRecipes = createEffect(async (params: IFindNftRecipesDto) => {
  const res = await forgeApi.find(params);
  return res;
});

export const nftRecipesStore = createStore<{
  state: { rows: INftTypeDto[]; count: number };
  error: Error | null;
  loading: boolean;
  isFetched: boolean;
}>({
  state: { rows: [], count: 0 },
  error: null,
  loading: false,
  isFetched: false,
})
  .on(loadNftRecipes.doneData, (prev, next) => ({
    error: null,
    state: next,
    loading: false,
    isFetched: true,
  }))
  .on(loadNftRecipes.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: { rows: [], count: 0 },
    isFetched: true,
  }))
  .on(loadNftRecipes.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
    isFetched: prev.isFetched,
  }));
