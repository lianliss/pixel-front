import { createEffect } from 'effector';
import { minerDroneCraftApi } from 'lib/Forge/api-server/miner-drone-craft.api.ts';
import { IFindMinerDroneCraftEntryDto } from 'lib/Forge/api-server/miner-drone-craft.types.ts';
import { createStoreState } from 'utils/store/createStoreState.ts';

export const loadMinerDroneEntries = createEffect(async (params: IFindMinerDroneCraftEntryDto) => {
  const res = await minerDroneCraftApi.findEntries(params);
  return res;
});

export const minerDroneEntriesStore = createStoreState(loadMinerDroneEntries);
