'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  IFindMinerDroneCraftEntriesResponse,
  IFindMinerDroneCraftEntryDto,
} from 'lib/Forge/api-server/miner-drone-craft.types.ts';

export class MinerDroneCraftApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async findEntries({
    minerDroneCraftAddress,
    ...params
  }: IFindMinerDroneCraftEntryDto): Promise<IFindMinerDroneCraftEntriesResponse> {
    const { data } = await axios.get<
      { data: IFindMinerDroneCraftEntriesResponse },
      AxiosResponse<{ data: IFindMinerDroneCraftEntriesResponse }>,
      IFindMinerDroneCraftEntryDto
    >(`/api/v1/miner/drone/craft/entry/find`, {
      baseURL: this.url,
      params: { ...params, minerDroneCraftAddress: minerDroneCraftAddress.toLowerCase() },
    });
    return data.data;
  }
}

export const minerDroneCraftApi = new MinerDroneCraftApi({ url: 'https://api.hellopixel.network' });
