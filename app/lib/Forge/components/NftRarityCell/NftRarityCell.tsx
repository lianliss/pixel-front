'use strict';
import React from 'react';
import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import clsx from 'clsx';
import styles from './styles.module.scss';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import PlusIcon from 'lib/Marketplace/components/icons/PlusIcon';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import Badge from '@mui/material/Badge';
import EditIcon from '@mui/icons-material/Edit';
import AddIcon from '@mui/icons-material/Add';
import BadgeEdit from '@ui-kit/Badge/BadgeEdit.tsx';

type Props = {
  rarity?: NftItemRarity;
  selected?: boolean;
  handleSelectNft: (rarity: NftItemRarity) => void;
  selectedCount: number;
  allCount: number;
  disabled?: boolean;
  image?: string;
} & React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

function NftRarityCell(props: Props) {
  const {
    rarity,
    className,
    selected = false,
    selectedCount,
    allCount,
    handleSelectNft,
    disabled = false,
    image,
  } = props;

  const rarityColor = getNftRarityColor(rarity);
  const rarityLabel = getNftRarityLabel(rarity);

  const onselect = () => {
    handleSelectNft(rarity);
  };

  return (
    <button
      className={clsx(styles.root, { [styles.disabled]: disabled || allCount === 0 }, className)}
      onClick={onselect}
      disabled={disabled}
    >
      <div className={styles.slot} style={{ borderColor: rarityColor }}>
        <BadgeEdit show={selectedCount > 0}>
          <Avatar
            src={image}
            variant='rounded'
            sx={{ width: 55, height: 55, fontSize: 30, background: '#011324' }}
          >
            {selected ? '' : <AddIcon sx={{ color: rarityColor, fontSize: 24 }} />}
          </Avatar>
        </BadgeEdit>
      </div>
      <Typography className={styles.text} align='center' sx={{ fontSize: 10, mt: 0.5 }}>
        {rarityLabel}
      </Typography>
      <Typography className={styles.text} align='center' sx={{ fontSize: 10 }}>
        {selectedCount}
        {typeof allCount === 'number' ? `/${allCount}` : ''}
      </Typography>
    </button>
  );
}

export default NftRarityCell;
