'use strict';
import React from 'react';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import clsx from 'clsx';
import styles from './NftCell.module.scss';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import uriTransform from 'utils/uriTransform';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import Badge from '@mui/material/Badge';

type Props = {
  data?: INftItemDto;
  selected?: boolean;
  disabled?: boolean;
  index?: number;
} & React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

function NftCell(props: Props) {
  const { data, className, selected = false, disabled = false, index, ...other } = props;

  const tokenURI = data?.type.uri;
  const avatar = (
    <Avatar
      src={tokenURI ? uriTransform(tokenURI) : undefined}
      variant='rounded'
      variantStyle='outlined'
      style={{
        borderColor: data ? getNftRarityColor(data.type.rarity) : undefined,
      }}
      className={styles.avatar}
      selected={selected}
      disabled={disabled}
      sx={{ width: 75, height: 75 }}
    />
  );

  return (
    <div {...other} className={clsx(styles.root, className)}>
      {selected ? (
        <Badge
          sx={{ height: 'fit-content' }}
          badgeContent={index}
          color='success'
          componentsProps={{
            badge: { sx: { transform: 'scale(1) translate(4px, -4px)' } },
          }}
        >
          {avatar}
        </Badge>
      ) : (
        avatar
      )}
    </div>
  );
}

export default NftCell;
