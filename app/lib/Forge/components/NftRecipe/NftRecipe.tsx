'use strict';
import React from 'react';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import uriTransform from 'utils/uriTransform';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { styled } from '@mui/material/styles';
import SlotDroneCraft from 'lib/NFT/components/icons/SlotDroneCraft';

const StyledRoot = styled('div')(() => ({
  position: 'relative',
  height: 'fit-content',
}));
const StyledAvatar = styled(Avatar)(() => ({
  backgroundSize: 'cover',
  backgroundPosition: 'center',
  width: 75,
  height: 75,
  fontSize: 12,
  padding: 1,
  '& > img': {
    borderRadius: '12px',
  },
}));
const StyledSlot = styled('div')(({ theme }) => ({
  position: 'absolute',
  bottom: 3,
  right: 3,
  background: theme.palette.background.paper,
  borderRadius: '16px',
  padding: 2,
  width: 20,
  height: 20,
  border: 'solid 1px',

  '& > svg': {
    fontSize: 14,
  },
}));

type Props = {
  data?: INftItemDto;
  selected?: boolean;
} & React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

function NftRecipe(props: Props) {
  const { data, selected = false, ...other } = props;

  const tokenURI = data?.type.uri;
  const rarityColor = data ? getNftRarityColor(data.type.rarity) : undefined;

  return (
    <StyledRoot {...other}>
      <StyledAvatar
        src={tokenURI ? uriTransform(tokenURI) : undefined}
        variant='rounded'
        variantStyle='outlined'
        style={{
          borderColor: rarityColor,
        }}
        selected={selected}
      />

      <StyledSlot style={{ color: rarityColor }}>
        <SlotDroneCraft />
      </StyledSlot>
    </StyledRoot>
  );
}

export default NftRecipe;
