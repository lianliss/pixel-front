import Autocomplete, { AutocompleteProps } from '@mui/material/Autocomplete';
import { INftTypeDto } from 'lib/NFT/api-server/nft.types.ts';
import MenuItem from '@mui/material/MenuItem';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import React from 'react';

type Props = Omit<
  AutocompleteProps<INftTypeDto, true, undefined, undefined>,
  'renderOption' | 'renderInput'
>;

function AutocompleteNftType(props: Props) {
  return (
    <Autocomplete<INftTypeDto, true>
      {...props}
      multiple
      getOptionLabel={(option) => option.name}
      renderOption={(p, option) => (
        <MenuItem {...p} sx={{ display: 'flex', gap: 1 }}>
          <Avatar variant='rounded' src={option.uri} />
          <Typography style={{ color: getNftRarityColor(option.rarity) }}>
            {option.name} #{option.id?.split('-').pop()!}
          </Typography>
        </MenuItem>
      )}
      limitTags={10_000}
      // getOptionDisabled={(el) => el.id.split('-').pop() in itemsMap}
      renderInput={(params) => (
        <TextField
          {...params}
          variant='outlined'
          label='Select nft Type'
          placeholder='Enter nft type name..'
        />
      )}
    />
  );
}

export default AutocompleteNftType;
