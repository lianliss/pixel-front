import Dialog, { DialogProps } from '@mui/material/Dialog';
import { DialogTitle } from '@mui/material';
import React from 'react';
import { INftTypeDto } from 'lib/NFT/api-server/nft.types.ts';
import NftRecipeForm from 'lib/Forge/components/_admin/CreateBotRecipe/NftRecipeForm.tsx';

type Props = DialogProps & { nftType?: INftTypeDto };

function EditNftRecipeDialog(props: Props) {
  const { nftType, ...other } = props;

  const typeId = nftType?.id.split('-').pop();

  return (
    <Dialog maxWidth='lg' fullWidth scroll='body' {...other}>
      <DialogTitle>
        {typeof typeId !== 'undefined' ? `Edit Recipe ${typeId}` : 'Create new Recipe'}
      </DialogTitle>
      <NftRecipeForm sx={{ p: 2 }} nftType={nftType} />
    </Dialog>
  );
}

export default EditNftRecipeDialog;
