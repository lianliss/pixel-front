import React, { useEffect, useState } from 'react';
import Pagination from '@mui/material/Pagination';
import { usePagination } from 'utils/hooks/usePagination.ts';
import Grid from '@mui/material/Grid';
import { useMinerClaimerRecipes } from 'lib/Forge/hooks/useMinerClaimerRecipes.ts';
import RecipeCard from 'lib/Forge/components/_admin/RecipeCard.tsx';
import { INftTypeDto } from 'lib/NFT/api-server/nft.types.ts';

type Props = { onEdit?: (type: INftTypeDto) => void };

function RecipesGrid(props: Props) {
  const { onEdit } = props;

  const recipes = useMinerClaimerRecipes({});

  const pagination = usePagination({ total: 0, step: 24 });

  // useEffect(() => {
  //   onEdit?.(recipes.state.rows[0]);
  // }, [recipes.state.rows]);

  return (
    <div>
      <div>
        <Grid container spacing={2} sx={{}}>
          {/*<Grid*/}
          {/*  item*/}
          {/*  xs={12}*/}
          {/*  md={4}*/}
          {/*  lg={3}*/}
          {/*  xl={2}*/}
          {/*  sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}*/}
          {/*>*/}
          {/*  <Button*/}
          {/*    variant='contained'*/}
          {/*    size='large'*/}
          {/*    color='primary'*/}
          {/*    onClick={() => {*/}
          {/*      // dialog.show();*/}
          {/*      // setSelectedChestId(undefined);*/}
          {/*    }}*/}
          {/*  >*/}
          {/*    Create New Chest*/}
          {/*  </Button>*/}
          {/*</Grid>*/}
          {recipes.state.rows?.map((nftType, i) => (
            <Grid key={`chest-${i}`} item xs={12} md={4} lg={3} xl={2}>
              <RecipeCard nftType={nftType} onEdit={onEdit} />
            </Grid>
          ))}
        </Grid>
      </div>

      {pagination.pages > 1 && (
        <Pagination
          sx={{ margin: '16px auto 0 auto' }}
          count={pagination.pages}
          color='primary'
          variant='outlined'
          shape='rounded'
          size='large'
          siblingCount={1}
          boundaryCount={0}
          page={pagination.page}
          onChange={(_, p) => {
            pagination.setPage(p);
          }}
        />
      )}
    </div>
  );
}

export default RecipesGrid;
