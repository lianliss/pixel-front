import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { IChainChest } from 'lib/Chests/hooks/useChests.ts';
import defaultImage from 'assets/img/chests/mystery-box.png';
import { INftItemDto, INftTypeDto } from 'lib/NFT/api-server/nft.types.ts';
import Box from '@mui/material/Box';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import { ButtonGroup } from '@ui-kit/ButtonGroup/ButtonGroup.tsx';
import { createNftFromType } from 'lib/NFT/utils/create-nft-from-type.ts';
import Paper from '@mui/material/Paper';

type Props = { nftType: INftTypeDto; onEdit?: (nft: INftTypeDto) => void };

function ChestCard(props: Props) {
  const { nftType, onEdit } = props;

  return (
    <Paper
      variant='outlined'
      sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', p: 1 }}
    >
      <NFTCard nft={createNftFromType(nftType)} size='small' />

      <div style={{ display: 'flex', flexDirection: 'column', gap: 4, marginTop: 12 }}>
        <ButtonGroup>
          <Button
            variant='contained'
            color='primary'
            fullWidth
            onClick={() => {
              onEdit?.(nftType);
            }}
          >
            Edit
          </Button>
          <Button variant='contained' color='primary' fullWidth disabled>
            Clone
          </Button>
        </ButtonGroup>
      </div>
    </Paper>
  );
}

export default ChestCard;
