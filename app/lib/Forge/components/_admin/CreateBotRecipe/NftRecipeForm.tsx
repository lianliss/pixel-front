import React from 'react';
import styles from './CreateBotRecipe.module.scss';
import { useAutoClaimerAdminApi } from 'lib/MinerClaimer/hooks/useAutoClaimerAdminApi.ts';
import { useAdminRecipeApi } from 'lib/Forge/hooks/useAdminRecipeApi.ts';
import Chip from '@mui/material/Chip';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { alpha } from '@mui/material/styles';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import { useNftTypes } from 'lib/NFT/hooks/useNftTypes.ts';
import NftSelector from 'lib/AdminV5/pages/nft-editor/components/EditVariants/components/NftSelector/NftSelector.jsx';
import { convertToNftItemDto } from 'lib/NFT/components/NFTCard/convertToNftItemDto.ts';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';
import { getNftSlotLabel } from 'lib/NFT/utils/getNftSlotLabel.ts';
import Box from '@mui/material/Box';
import NumberField from '@ui-kit/NumberField/NumberField.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { ButtonGroup } from '@ui-kit/ButtonGroup/ButtonGroup.tsx';
import { Switch } from '@ui-kit/Switch/Switch.tsx';
import FormControlLabel from '@mui/material/FormControlLabel';

const INITIAL_REQUIREMENTS = [0, 0, 0, 0, 0, 0];

function NftRecipeForm({ nftType, typeId: overrideId, ...other }) {
  const typeId = overrideId || nftType?.id.split('-').pop();

  const itemTypesData = useNftTypes();
  const autoClaimersData = useAutoClaimerAdminApi();
  const recipesHook = useAdminRecipeApi();

  const account = useAccount();
  const minerAddress = MINER_CONTRACT_ID[account.chainId]?.core;

  const bots = React.useMemo(() => {
    return itemTypesData.data.filter((t) => autoClaimersData.list.includes(t.typeId));
  }, [autoClaimersData.list, itemTypesData.data]);

  const currentRecipe = recipesHook.data[typeId];
  const currentBotTypeId = currentRecipe?.botTypeId;
  const currentRequirements = currentRecipe?.requirements || INITIAL_REQUIREMENTS;
  const currentPrice = currentRecipe?.price;
  const currentSlots = currentRecipe?.slots || [
    NftItemSlot.Pocket0,
    NftItemSlot.Pocket1,
    NftItemSlot.Pocket2,
    NftItemSlot.Pocket3,
  ];
  const isNew = !currentRecipe;

  const [botTypeId, setBotTypeId] = React.useState<number>();
  const [slots, setSlots] = React.useState<number[]>([]);
  const botType = React.useMemo(() => {
    return botTypeId ? bots.find((t) => t.typeId === botTypeId) : undefined;
  }, [botTypeId, bots]);
  const [requirements, setRequirements] = React.useState<number[]>(INITIAL_REQUIREMENTS);
  const [price, setPrice] = React.useState<number>(currentPrice || 0);

  React.useEffect(() => {
    setBotTypeId(currentBotTypeId);
    setRequirements(currentRequirements);
    setPrice(currentPrice);
    setSlots(currentSlots);
  }, [currentRecipe]);

  const rarities = ['PXLs Drop', 'Common', 'Uncommon', 'Rare', 'Epic', 'Legendary'];

  const isChanged =
    isNew ||
    currentBotTypeId !== botTypeId ||
    price !== currentPrice ||
    currentRequirements.join('-') !== requirements.join('-') ||
    currentSlots?.join('-') !== slots?.join('-');

  const handleUpdate = async () => {
    await recipesHook.unset(typeId);
    await recipesHook.set({
      recipeTypeId: typeId,
      botTypeId,
      rarities: requirements,
      price,
      tokenAddress: minerAddress,
      isMinerAddress: true,
      slots: slots,
    });
  };

  return (
    <Box className={styles.block} {...other}>
      {/*{nftType && <Box sx={{ mb: -5, mt: '-70px'}}>*/}
      {/*    <NFTCard nft={createNftFromType(nftType)} size='small'/>*/}
      {/*</Box>}*/}
      <div className={styles.columns}>
        <Box>
          <Typography sx={{ mb: 2 }}>Requirements</Typography>

          <FormControlLabel
            sx={{ mb: 1 }}
            label='Use Backpack + Belt'
            control={
              <Switch
                checked={
                  !!slots?.includes(NftItemSlot.Belt) && !!slots?.includes(NftItemSlot.Backpack)
                }
                onChange={(e) =>
                  setSlots(
                    e.target.checked
                      ? [NftItemSlot.Belt, NftItemSlot.Backpack]
                      : [
                          NftItemSlot.Pocket0,
                          NftItemSlot.Pocket1,
                          NftItemSlot.Pocket2,
                          NftItemSlot.Pocket3,
                        ]
                  )
                }
              />
            }
          />
          <Typography component='p' variant='caption' sx={{ mb: 2 }}>
            Require NFT: {slots.map((id) => getNftSlotLabel(id)).join(',')}
          </Typography>

          <NumberField
            size='small'
            type='number'
            min={0}
            label={`Craft Price`}
            placeholder={`0.00`}
            InputProps={{
              endAdornment: <Chip label={'PXLs'} />,
            }}
            onChange={(e) => {
              setPrice(Number(e.target.value));
            }}
            value={price}
            fullWidth
          />
          {requirements.map((value, index) => {
            if (!index) return;
            const rarity = rarities[index];
            return (
              <div className={styles.item} key={index}>
                <TextField
                  size='small'
                  type='number'
                  min={0}
                  label={`${rarity} NFTs`}
                  placeholder={`${rarity} NFTs`}
                  InputProps={{
                    endAdornment: (
                      <Chip
                        label={rarity}
                        style={{
                          color: getNftRarityColor(index),
                          backgroundColor: rarity
                            ? alpha(getNftRarityColor(index), 0.4)
                            : undefined,
                        }}
                      />
                    ),
                  }}
                  onChange={(e) => {
                    const newRequirements = [...requirements];
                    newRequirements[index] = Number(e.target.value);
                    setRequirements(newRequirements);
                  }}
                  value={value}
                  fullWidth
                />
              </div>
            );
          })}

          <p>Token: {currentRecipe?.tokenAddress}</p>
        </Box>

        <Box className={styles.right}>
          <Typography sx={{ mb: 2 }}>Result</Typography>
          <NftSelector typeId={botTypeId} setTypeId={setBotTypeId} itemTypes={bots} />
          <div className={styles.bot}>
            {!!botType && <NFTCard nft={convertToNftItemDto(botType)} size='small' {...botType} />}
          </div>
        </Box>
      </div>

      <ButtonGroup>
        {!isNew && (
          <Button
            loading={recipesHook.loading}
            disabled={recipesHook.loading}
            color='error'
            variant='contained'
            onClick={() => recipesHook.unset(typeId)}
          >
            Unset
          </Button>
        )}
        <Button
          loading={recipesHook.loading}
          disabled={recipesHook.loading || !isChanged}
          color='success'
          variant='contained'
          onClick={handleUpdate}
        >
          {isNew ? 'Set as AutoClaim recipe' : 'Update recipe'}
        </Button>
      </ButtonGroup>
    </Box>
  );
}

export default NftRecipeForm;
