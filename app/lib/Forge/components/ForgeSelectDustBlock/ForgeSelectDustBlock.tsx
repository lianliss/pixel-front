import React, { useEffect, useRef, useState } from 'react';
import { styled } from '@mui/material/styles';
import Box, { BoxProps } from '@mui/material/Box';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Slider from '@mui/material/Slider';

const marks = [
  { value: 1_000 },
  { value: 2_000 },
  { value: 4_000 },
  { value: 6_000 },
  { value: 8_000 },
  { value: 10_000 },
];
// const defaultValue = 1_000;
const maxValue = 10_000;
const step = 500;

const Root = styled(Box)(() => ({
  display: 'flex',
  flexDirection: 'column',
}));

type Props = {
  loading?: boolean;
  disabled?: boolean;
  value?: number;
  onChange?: (value: number) => void;
} & Omit<BoxProps, 'onChange'>;

function ForgeSelectDustBlock(props: Props) {
  const { loading = false, disabled, value, onChange, ...other } = props;

  const [amount, setAmount] = useState(value);
  const timeoutRef = useRef<any | null>(null);

  useEffect(() => {
    if (value) {
      setAmount(value);
    }
  }, [value]);

  useEffect(() => {
    return () => {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
      }
    };
  }, []);

  const handleChange = (_event: Event, value: number, _activeThumb: number) => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }

    timeoutRef.current = setTimeout(() => {
      setAmount(value);
      onChange?.(value);
    }, 500);
  };
  const preventDefault = (e) => {
    e.stopPropagation();
    e.preventDefault();
  };

  return (
    <Root {...other}>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography variant='subtitle2' fontWeight='medium'>
          Add Pixel dust
        </Typography>
        <Typography variant='caption' color='text.secondary'>
          {amount} / {maxValue}
        </Typography>
      </Box>

      <Slider
        disabled={disabled}
        defaultValue={value}
        valueLabelFormat={(value) => `${value} PXLd`}
        valueLabelDisplay='auto'
        shiftStep={step}
        step={step}
        marks={marks}
        min={step}
        disableSwap
        max={maxValue}
        onChange={handleChange}
        onMouseMove={preventDefault}
        sx={{ padding: '8px 0', '& .MuiSlider-valueLabel': { minWidth: 60 }, mt: 1 }}
      />
    </Root>
  );
}

export default ForgeSelectDustBlock;
