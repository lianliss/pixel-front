import { styled } from '@mui/material/styles';
import Box, { BoxProps } from '@mui/material/Box';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Stack from '@mui/material/Stack';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import Skeleton from '@mui/material/Skeleton';

const IMAGES = {
  [NftItemRarity.Common]: require('assets/img/forge/1.png'),
  [NftItemRarity.UnCommon]: require('assets/img/forge/2.png'),
  [NftItemRarity.Rare]: require('assets/img/forge/3.png'),
  [NftItemRarity.Epic]: require('assets/img/forge/4.png'),
  [NftItemRarity.Legendary]: require('assets/img/forge/5.png'),
};

const Root = styled(Box)(() => ({
  width: '100%',
}));

const Chance = styled(Box)(() => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
}));

type Props = BoxProps & { options?: number[]; loading?: boolean };

const rarityValues = Object.values(NftItemRarity).filter((value) => typeof value === 'number');

function ForgeNftChanceBlock(props: Props) {
  const { options = [] as number[], loading = false, ...other } = props;

  return (
    <Root {...other}>
      <Typography align='center' fontWeight='medium' sx={{ fontSize: 14 }}>
        Chances NFT rare
      </Typography>

      <Stack direction='row' gap={1} sx={{ width: '100%', justifyContent: 'space-between', mt: 1 }}>
        {rarityValues.map((rarity, i) => (
          <Chance key={`chance-${rarity}`}>
            <Avatar src={IMAGES[rarity]} sx={{ width: 48, height: 48 }} />
            <Typography color='text.secondary' variant='caption' sx={{ fontSize: 11, mt: '2px' }}>
              {getNftRarityLabel(rarity)}
            </Typography>
            <Typography
              variant='caption'
              fontWeight='bold'
              sx={{ fontSize: 11, color: getNftRarityColor(rarity) }}
            >
              {loading ? <Skeleton sx={{ width: 35 }} /> : `${options[i] || '0'}%`}
            </Typography>
          </Chance>
        ))}
      </Stack>
    </Root>
  );
}

export default ForgeNftChanceBlock;
