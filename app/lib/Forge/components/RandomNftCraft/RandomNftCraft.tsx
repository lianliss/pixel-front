'use strict';
import React from 'react';
import Paper from '@mui/material/Paper';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { INftItemDto, NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useTranslation } from 'react-i18next';
import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import ForgeSelectNftBlock from 'lib/Forge/components/ForgeSelectNftBlock/ForgeSelectNftBlock.tsx';
import ForgeNftChanceBlock from 'lib/Forge/components/ForgeNftChanceBlock/ForgeNftChanceBlock.tsx';
import ForgeSelectDustBlock from 'lib/Forge/components/ForgeSelectDustBlock/ForgeSelectDustBlock.tsx';
import BuyGasAlertLazy from 'lib/TelegramStars/components/BuyGasAlert/BuyGasAlert.lazy.tsx';

export type INftCreated = { txHash: string; txUrl?: string | null };

type Props = {
  // state
  amount?: number;
  selectedNft?: Partial<Record<NftItemRarity, INftItemDto[]>>;
  chances?: number[];
  chancesLoading?: boolean;
  // requirements?: number[];
  // handlers
  onSelectNft?: (rarity: NftItemRarity) => void;
  onCraft?: () => void;
  onChangeAmount?: (amount: number) => void;
  // status
  disabled?: boolean;
  loading?: boolean;
  disabledCraft?: boolean;
  // info
  created?: INftCreated;
  noGas?: boolean;
};

function RandomNftCraft(props: Props) {
  const {
    onSelectNft,
    onCraft,
    disabled = false,
    selectedNft = {} as Partial<Record<NftItemRarity, INftItemDto[]>>,
    loading = false,
    amount,
    onChangeAmount,
    chances,
    disabledCraft,
    // requirements = [],
    created,
    chancesLoading = false,
    noGas = false,
  } = props;
  const { t } = useTranslation('forge');

  const handleSelectNft = (rarity: NftItemRarity) => {
    onSelectNft?.(rarity);
  };

  const handleCraft = () => {
    onCraft?.();
  };

  return (
    <Paper style={{ padding: 16 }} variant='outlined'>
      <Typography align='center' fontWeight='bold' sx={{ fontSize: 14 }}>
        {t('NFT Blender')}
      </Typography>

      <ForgeSelectDustBlock
        sx={{ mt: 1 }}
        value={amount}
        onChange={onChangeAmount}
        disabled={disabled}
      />

      <ForgeNftChanceBlock sx={{ mt: 2 }} options={chances} loading={chancesLoading} />

      <Paper sx={{ mt: 2 }}>
        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', gap: 1 }}>
          <Typography align='center' fontWeight='medium' sx={{ fontSize: 14 }}>
            {t('Add NFT')}
          </Typography>
        </Box>

        <ForgeSelectNftBlock
          sx={{ mt: 1 }}
          options={Object.keys(selectedNft).map((r) => ({
            rarity: +r as NftItemRarity,
            arr: selectedNft[r] || [],
          }))}
          // requirements={requirements}
          onSelect={handleSelectNft}
          disabled={disabled}
        />

        {created && (
          <Alert
            severity='success'
            variant='outlined'
            sx={{ mt: 2 }}
            component='a'
            href={created.txUrl || '#'}
          >
            NFT successfully created
          </Alert>
        )}

        {noGas && <BuyGasAlertLazy sx={{ mt: 1 }} />}

        <Button
          onClick={handleCraft}
          disabled={disabled || disabledCraft || noGas}
          loading={loading}
          fullWidth
          variant='contained'
          sx={{ mt: 2 }}
        >
          {disabled ? t('Select nft') : t('Craft')}
        </Button>
      </Paper>
    </Paper>
  );
}

export default RandomNftCraft;
