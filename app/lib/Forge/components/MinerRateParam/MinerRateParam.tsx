import React from 'react';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { styled } from '@mui/material/styles';
import Box, { BoxProps } from '@mui/material/Box';
import { TypographyProps } from '@mui/material';
import Skeleton from '@mui/material/Skeleton';
import InfoIcon from '@mui/icons-material/Info';

const Root = styled(Box)(() => ({
  border: 'solid 2px transparent',
  boxSizing: 'border-box',
  minWidth: 100,

  '& svg': {
    fontSize: 16,
  },
}));

const Value = styled(Typography)(() => ({
  fontSize: 16,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  gap: 4,
  minWidth: 60,
}));

const Subtitle = styled(Typography)(() => ({
  fontSize: 14,
  opacity: 0.5,
  display: 'flex',
  justifyContent: 'center',
}));

type Props = BoxProps & {
  title: string;
  subTitle: string;
  value: string;
  startIcon?: React.ReactNode;
  endIcon?: React.ReactNode;
  color?: TypographyProps['color'];
  loadingValue?: boolean;
  loadingSubtitle?: boolean;
  onInfo?: () => void;
};

const MinerRateParam = (props: Props) => {
  const {
    title,
    subTitle,
    value,
    startIcon,
    endIcon,
    color,
    loadingValue = false,
    loadingSubtitle = false,
    onInfo,
    ...other
  } = props;

  return (
    <Root {...other} onClick={onInfo}>
      <Typography
        color='text.secondary'
        sx={{
          fontSize: 12,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          gap: '4px',
        }}
        fontWeight='normal'
        align='center'
        noWrap
      >
        {title} {onInfo && <InfoIcon sx={{ fontSize: 14 }} onClick={onInfo} />}
      </Typography>
      <Value fontWeight='bold' align='center' noWrap color={color || 'text.primary'}>
        {loadingValue ? (
          <Skeleton sx={{ width: 50 }} />
        ) : (
          <>
            {startIcon} {value} {endIcon}
          </>
        )}
      </Value>
      <Subtitle align='center' color='text.primary'>
        {loadingSubtitle ? <Skeleton sx={{ width: 50 }} /> : subTitle}
      </Subtitle>
    </Root>
  );
};

export default MinerRateParam;
