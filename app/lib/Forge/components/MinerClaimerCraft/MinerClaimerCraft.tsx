'use strict';
import React from 'react';
import Paper from '@mui/material/Paper';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { INftItemDto, NftItemRarity, NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';
import BreadwinnerImageMinerDrone from 'lib/MinerClaimer/components/MinerClaimerDialog/components/BreadwinnerImageMinerDrone/BreadwinnerImageMinerDrone.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import styles from './MinerClaimerCraft.module.scss';
import { IconDuration } from '@ui-kit/icon/common/IconDuration.tsx';
import { IconDurability } from '@ui-kit/icon/common/IconDurability.tsx';
import NftRarityCell from 'lib/Forge/components/NftRarityCell';
import { getDateDays } from 'utils/format/date';
import { useTranslation } from 'react-i18next';
import PixelIcon from '../../../../shared/icons/PixelIcon.tsx';
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import { getNftSlotIcon } from 'lib/NFT/utils/getNftSlotIcon.tsx';
import Box from '@mui/material/Box';
import ForgeSelectNftBlock from 'lib/Forge/components/ForgeSelectNftBlock/ForgeSelectNftBlock.tsx';
import BuyGasAlertLazy from 'lib/TelegramStars/components/BuyGasAlert/BuyGasAlert.lazy.tsx';

export type IDroneMinerCreated = { txHash: string; txUrl?: string | null };

type Props = {
  onSelectNft?: (rarity: NftItemRarity) => void;
  onCraft?: () => void;
  disabled?: boolean;
  selectedNft?: Partial<Record<NftItemRarity, INftItemDto[]>>;
  selectedRecipe?: INftItemDto;
  onSelectRecipe?: () => void;
  balance?: number;
  balanceLoading?: boolean;
  loadingCraft?: boolean;
  priceSymbol?: string;
  priceImage?: string;
  created?: IDroneMinerCreated;
  slots?: number[];
  noGas?: boolean;
};

function MinerClaimerCraft(props: Props) {
  const {
    onSelectNft,
    onCraft,
    disabled = false,
    selectedNft = {} as Partial<Record<NftItemRarity, INftItemDto[]>>,
    selectedRecipe,
    onSelectRecipe,
    balance,
    balanceLoading = false,
    loadingCraft = false,
    created,
    priceSymbol,
    priceImage,
    slots = [],
    noGas = false,
  } = props;
  const { t } = useTranslation('forge');

  const handleSelectNft = (rarity: NftItemRarity) => {
    onSelectNft?.(rarity);
  };

  const handleCraft = () => {
    onCraft?.();
  };

  const rarityValues = Object.values(NftItemRarity).filter((value) => typeof value === 'number');
  const recipeRequirements = (selectedRecipe?.type?.droneRecipe?.requirements || []).slice(1); // remove zero element (pixel)
  const droneType = selectedRecipe?.type.droneRecipe?.resultType;
  const droneMiner = droneType?.minerDroneType;
  const droneDuration = droneMiner?.duration ? getDateDays(droneMiner.duration) : undefined;
  const droneSessionClaims = droneMiner?.sessionClaims;
  const droneSessionDuration = Number(droneMiner?.sessionDuration);
  const price = selectedRecipe?.type?.droneRecipe?.price;
  const noBalance = price > balance;
  const isTimed = droneMiner?.isTimeSession;
  const filteredSlots = slots.filter(
    (el) => ![NftItemSlot.Pocket1, NftItemSlot.Pocket2, NftItemSlot.Pocket3].includes(el)
  );

  return (
    <Paper style={{ padding: 16 }} variant='outlined'>
      <Typography align='center' fontWeight='bold' sx={{ fontSize: 14 }}>
        {t('Create smart claimer')}
      </Typography>

      <div className={styles.breadwinner__inner}>
        <div className={styles.breadwinner__left}>
          <BreadwinnerImageMinerDrone
            showEdit={!!droneType}
            onClick={onSelectRecipe}
            image={selectedRecipe?.type?.uri}
            rarity={selectedRecipe?.type?.rarity}
          />
        </div>

        <div className={styles.breadwinner__right}>
          <Typography fontWeight='bold' sx={{ fontSize: 16 }}>
            {droneType?.name || 'Select Recipe'}
          </Typography>

          <div className={styles.name__line}>
            <IconDuration />
            <Typography sx={{ fontSize: 12 }} className={styles.value} fontWeight='normal'>
              {!isTimed && (
                <>
                  {t('Duration')} <span>{droneSessionClaims || '0'} claims</span>
                </>
              )}
              {isTimed && (
                <>
                  {t('Duration')}{' '}
                  <span>
                    {(Number(droneSessionDuration || '0') / 1000 / 60 / 60).toFixed(2)} h.
                  </span>
                </>
              )}
            </Typography>
          </div>
          <div className={styles.name__line}>
            <IconDurability />
            <Typography sx={{ fontSize: 12 }} className={styles.value} fontWeight='normal'>
              {t('Durability')}{' '}
              <span>
                {droneDuration || '0'} {t('days')}
              </span>
            </Typography>
          </div>
        </div>
      </div>

      <Paper sx={{ marginTop: 2 }} elevation={0}>
        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', gap: 1 }}>
          <Typography align='center' fontWeight='bold' sx={{ fontSize: 14 }}>
            {t('Add NFT')}
          </Typography>

          {!!filteredSlots?.length && (
            <Stack direction='row' stack={1} sx={{ '& svg': { width: 16, height: 16 } }}>
              {filteredSlots.map((slot) => (
                <div key={`slot-${slot}`}>{getNftSlotIcon(slot)}</div>
              ))}
            </Stack>
          )}
        </Box>

        <ForgeSelectNftBlock
          sx={{ mt: 1.5 }}
          options={Object.keys(selectedNft).map((r) => ({
            rarity: +r as NftItemRarity,
            arr: selectedNft[r] || [],
          }))}
          requirements={recipeRequirements}
          onSelect={handleSelectNft}
          loading={loadingCraft}
          disabled={!selectedRecipe}
        />

        {price && (
          <div className={styles.total}>
            <img src={priceImage} alt='' style={{ width: 24, height: 24 }} />
            <Typography align='center' fontWeight='bold' sx={{ fontSize: 22 }}>
              {price} {priceSymbol}
            </Typography>
          </div>
        )}

        {created && (
          <Alert
            severity='success'
            variant='outlined'
            sx={{ mt: 2 }}
            component='a'
            href={created.txUrl || '#'}
          >
            NFT successfully created
          </Alert>
        )}

        {noGas && <BuyGasAlertLazy sx={{ mt: 1 }} />}

        <Button
          onClick={handleCraft}
          disabled={disabled || noBalance || noGas}
          loading={balanceLoading || loadingCraft}
          fullWidth
          variant='contained'
          color='primary'
          sx={{ mt: 2 }}
        >
          {noBalance && 'No enough PXLs'}
          {!noBalance && (disabled ? t('Select nft') : t('Craft'))}
        </Button>
      </Paper>
    </Paper>
  );
}

export default MinerClaimerCraft;
