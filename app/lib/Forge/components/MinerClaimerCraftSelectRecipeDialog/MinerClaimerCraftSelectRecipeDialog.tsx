'use strict';
import React, { useEffect, useState } from 'react';
import { DialogProps } from '@mui/material/Dialog';
import { Button } from '@ui-kit/Button/Button.tsx';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { useUnit } from 'effector-react';
import styles from './MinerClaimerCraftSelectRecipeDialog.module.scss';
import Typography from '@mui/material/Typography';
import { userNftRecipesStore } from 'lib/Forge/api-server/miner-claimer.store.ts';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import NftCell from 'lib/Forge/components/NftCell';
import NftRecipe from 'lib/Forge/components/NftRecipe';
import CircularProgress from '@mui/material/CircularProgress';
import { useTranslation } from 'react-i18next';

type Props = DialogProps & {
  onApply?: (recipe: INftItemDto) => void;
  onCancel?: () => void;
  accountAddress?: string;
  nftAddress?: string;
  selectedRecipe?: INftItemDto;
};

function MinerClaimerCraftSelectRecipeDialog(props: Props) {
  const {
    onApply,
    rarity,
    open,
    accountAddress,
    nftAddress,
    selectedRecipe,
    onClose,
    onCancel,
    ...other
  } = props;
  const { t } = useTranslation('forge');

  const recipesState = useUnit(userNftRecipesStore);
  const [selected, setSelected] = useState<INftItemDto | null>(null);

  useEffect(() => {
    if (rarity && open && accountAddress && nftAddress) {
      // loadUserNftRecipes({
      //   limit: 20,
      //   offset: 0,
      //   holder: accountAddress,
      //   contractId: nftAddress.toLowerCase(),
      // }).then();
    }
  }, [rarity, open, accountAddress, nftAddress]);
  useEffect(() => {
    if (selectedRecipe) {
      setSelected(selectedRecipe);
    }
  }, [selectedRecipe]);

  useEffect(() => {
    if (recipesState.state?.rows.length) {
      setSelected(recipesState.state?.rows[0]);
      onApply?.(recipesState.state?.rows[0]);
    }
  }, [recipesState.state?.rows]);

  const handleApply = () => {
    onApply?.(selected);
    onClose?.();
  };
  const handleCancel = () => {
    onCancel?.();
    onClose?.();
  };
  const handleSelect = (r: INftItemDto) => {
    setSelected(r);
  };

  return (
    <BottomDialog
      open={open}
      {...other}
      onClose={onClose}
      footer={
        <div className={styles.btns}>
          <Button size='large' variant='outlined' onClick={handleCancel}>
            {t('Cancel')}
          </Button>
          <Button size='large' variant='contained' onClick={handleApply}>
            {t('Apply')}
          </Button>
        </div>
      }
    >
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Typography style={{ fontSize: 16 }} fontWeight='bold' align='center'>
          {t('Select Recipe')}
        </Typography>

        <Typography className={styles.desc} style={{ fontSize: 14 }} align='center'>
          {t('Select recipe for craft Smart Claimer')}
        </Typography>

        <div className={styles.content}>
          {!!recipesState.state?.rows.length && !recipesState.loading && (
            <div className={styles.grid}>
              {recipesState.state?.rows.map((nft) => {
                const isSelected = selected?.id === nft.id;

                return (
                  <div key={nft.id}>
                    <NftRecipe data={nft} onClick={() => handleSelect(nft)} selected={isSelected} />
                  </div>
                );
              })}
            </div>
          )}

          {recipesState.loading && <CircularProgress sx={{ mt: 10 }} />}

          {!recipesState.loading && !recipesState.state?.rows.length && (
            <Typography align='center' sx={{ mt: 1.5 }}>
              {t('No recipes')}
            </Typography>
          )}
        </div>
      </div>
    </BottomDialog>
  );
}

export default MinerClaimerCraftSelectRecipeDialog;
