'use strict';
import React, { useEffect, useState } from 'react';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import { Button } from '@ui-kit/Button/Button.tsx';
import { INftItemDto, NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { useUnit } from 'effector-react';
import { loadNftByRarityList, nftListByRarityStore } from 'lib/NFT/api-server/nft.store.ts';
import NftCell from 'lib/Forge/components/NftCell';
import styles from './ForgeSelectNftDialog.module.scss';
import Typography from '@mui/material/Typography';
import CircularProgress from '@mui/material/CircularProgress';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import Pagination from '@mui/material/Pagination';
import { usePagination } from 'utils/hooks/usePagination.ts';

type Props = DialogProps & {
  onApply?: (nfts: INftItemDto[]) => void;
  onCancel?: () => void;
  rarity?: NftItemRarity | null;
  accountAddress?: string;
  nftAddress?: string;
  selectedNft?: INftItemDto[];
  requireCount?: number;
  slots?: number[];
};

function DroneMinerCraftSelectDialog(props: Props) {
  const {
    onApply,
    rarity,
    open,
    accountAddress,
    nftAddress,
    selectedNft,
    onClose,
    onCancel,
    requireCount,
    slots,
    ...other
  } = props;

  const nftState = useUnit(nftListByRarityStore);
  const [selected, setSelected] = useState<INftItemDto[]>([]);
  const isAllSelected = typeof requireCount === 'number' && selected.length >= (requireCount || 0);
  const pagination = usePagination({ total: nftState.state.count, step: 20 });

  useEffect(() => {
    if (rarity && open && accountAddress && nftAddress) {
      loadNftByRarityList({
        limit: pagination.limit,
        offset: pagination.offset,
        rarity,
        holder: accountAddress,
        slots: slots?.length ? slots : undefined,
        contractId: nftAddress.toLowerCase(),
      }).then();
    }
  }, [rarity, open, accountAddress, nftAddress, pagination.offset]);
  useEffect(() => {
    if (selectedNft) {
      setSelected(selectedNft);
    }
  }, [selectedNft]);

  const handleApply = () => {
    onApply?.(selected);
    onClose?.();
  };
  const handleCancel = () => {
    onCancel?.();
    onClose?.();
  };
  const handleSelect = (nft: INftItemDto, swapWithFirst?: boolean) => {
    let next = [...selected];

    if (next.find((el) => el.id === nft.id)) {
      next = next.filter((el) => el.id !== nft.id);
    } else {
      next.push(nft);

      if (swapWithFirst) {
        next.shift();
      }
    }

    setSelected(next);
  };

  return (
    <BottomDialog
      open={open}
      {...other}
      onClose={onClose}
      footer={
        <div className={styles.btns}>
          <Button size='large' variant='outlined' onClick={handleCancel}>
            Cancel
          </Button>
          <Button size='large' variant='contained' onClick={handleApply}>
            Apply
          </Button>
        </div>
      }
    >
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Typography style={{ fontSize: 16 }} fontWeight='bold' align='center'>
          Select {getNftRarityLabel(rarity)} NFT
        </Typography>
        <Typography className={styles.desc} style={{ fontSize: 14 }} align='center'>
          Add items for the Craft
          {typeof requireCount === 'number' ? `${selected.length}/${requireCount}` : ''}
        </Typography>

        <div className={styles.content} style={{ position: 'relative' }}>
          {!!nftState.state.rows.length && (
            <div className={styles.grid}>
              {nftState.state.rows.map((nft) => {
                const index = selected.findIndex((el) => el.id === nft.id);
                const isSelected = index !== -1;
                const itemDisabled = !isSelected && isAllSelected;

                return (
                  <div key={nft.id} style={{ height: 'fit-content' }}>
                    <NftCell
                      data={nft}
                      index={index + 1}
                      onClick={() => handleSelect(nft, itemDisabled)}
                      selected={isSelected}
                      disabled={itemDisabled}
                    />
                  </div>
                );
              })}
            </div>
          )}

          {pagination.enabled && (
            <Pagination
              sx={{ margin: '16px auto 0 auto' }}
              count={pagination.pages}
              color='primary'
              variant='outlined'
              shape='rounded'
              size='large'
              siblingCount={1}
              boundaryCount={0}
              page={pagination.page}
              onChange={(_, p) => {
                pagination.setPage(p);
              }}
            />
          )}

          {nftState.loading && !nftState.state.count && <CircularProgress sx={{ mt: 10 }} />}
          {nftState.loading && !!nftState.state.count && (
            <CircularProgress
              size={60}
              sx={{ position: 'absolute', left: 'calc(50% - 30px)', top: 'calc(50% - 30px)' }}
            />
          )}
        </div>
      </div>
    </BottomDialog>
  );
}

export default DroneMinerCraftSelectDialog;
