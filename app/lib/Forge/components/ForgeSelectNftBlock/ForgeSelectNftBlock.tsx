import NftRarityCell from 'lib/Forge/components/NftRarityCell';
import { INftItemDto, NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import React from 'react';
import { styled } from '@mui/material/styles';
import Box, { BoxProps } from '@mui/material/Box';

const Root = styled(Box)(() => ({
  display: 'flex',
  alignItems: 'center',
  gap: '3px',
  justifyContent: 'space-between',
}));

const rarityValues = Object.values(NftItemRarity).filter((value) => typeof value === 'number');

type ForgeSelectNftOption = {
  arr: INftItemDto[];
  rarity: NftItemRarity;
};

type Props = {
  options: ForgeSelectNftOption[];
  onSelect?: (rarity: NftItemRarity) => void;
  loading?: boolean;
  disabled?: boolean;
  requirements?: number[];
} & BoxProps;

function ForgeSelectNftBlock(props: Props) {
  const {
    requirements,
    onSelect,
    options = [] as ForgeSelectNftOption[],
    loading = false,
    disabled,
    ...other
  } = props;

  return (
    <Root {...other}>
      {rarityValues.map((rarity, index) => {
        const requireCount = requirements?.[index] || 0;
        const option = options.find((el) => el.rarity === rarity);
        const firstNft = option?.arr?.[0];

        return (
          <NftRarityCell
            key={index}
            rarity={rarity}
            allCount={requirements ? requireCount : undefined}
            image={firstNft?.type.uri}
            selectedCount={option?.arr.length || 0}
            handleSelectNft={onSelect}
            disabled={disabled || (requirements && !requireCount) || loading}
          />
        );
      })}
    </Root>
  );
}

export default ForgeSelectNftBlock;
