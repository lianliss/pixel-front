'use strict';
import React from 'react';
import { IS_DEVELOP, IS_TELEGRAM } from '@cfg/config.ts';
import styles from './ForgeLayout.module.scss';
import IconPixelDust from '@ui-kit/icon/pixel/IconPixelDust.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import MinerRateParam from 'lib/Forge/components/MinerRateParam/MinerRateParam.tsx';
import { Tab } from '@ui-kit/Tab/Tab.tsx';
import { Tabs } from '@ui-kit/Tabs/Tabs.tsx';
import { useLocation, useNavigate } from 'react-router-dom';
import { IS_FORGE_CRAFT } from '@cfg/app.ts';
import Container from '@mui/material/Container';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { getDefaultChainImage } from '@cfg/image.ts';
import { MINER_DUST_CONTRACT_ID } from 'lib/MinerDrone/api-contract/miner-drone.constants.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';

function ForgeLayout(props: React.PropsWithChildren) {
  const { children } = props;

  const location = useLocation();
  const navigate = useNavigate();
  const account = useAccount();

  const dustAddress = MINER_DUST_CONTRACT_ID[account.chainId];
  const dustBalance = useBalance({ address: dustAddress });

  return (
    <div
      style={{
        padding: '24px 0',
        paddingTop: 16,
        backgroundImage: `url(${getDefaultChainImage(account.chainId)})`,
      }}
      className={styles.root}
    >
      <Container maxWidth='xs'>
        <div className={styles.drone__header}>
          <div className={styles.header__inner}>
            <div className={styles.balance}>
              <IconPixelDust sx={{ fontSize: 38 }} />
              <Typography color='text.primary' sx={{ fontSize: 38 }} fontWeight='bold'>
                {dustAddress ? dustBalance.data?.formatted || 0 : 'Soon'}
              </Typography>
            </div>
            {false && (
              <div className={styles.params}>
                <MinerRateParam />
                <MinerRateParam />
                <MinerRateParam />
              </div>
            )}
          </div>
        </div>

        <Tabs
          sx={{ mb: 1 }}
          onChange={(_, nextTab) => {
            navigate(nextTab);
          }}
          value={location.pathname}
          variant='scrollable'
          scrollButtons='auto'
        >
          <Tab value='/forge/smart-claimer' label='Smart Claimer' />
          <Tab value='/forge/random-nft' label='NFT Blender' disabled={!IS_FORGE_CRAFT} />
        </Tabs>

        {children}
      </Container>
    </div>
  );
}

export default ForgeLayout;
