import { Chain } from 'services/multichain/chains';
import { IS_SKALE_SMART_CLAIMER } from '@cfg/app.ts';

export const FORGE_MINER_CLAIMER_CONTRACT_ID = {
  [Chain.SONGBIRD]: '0x8B8e37E8A4cBFB58f0bb127B0F5493a91E50413D'.toLowerCase(),
  [Chain.SKALE]: IS_SKALE_SMART_CLAIMER
    ? '0x11a79984FE5BC0482B5800aDFBfCA78DCb33060d'.toLowerCase()
    : undefined,
  [Chain.UNITS]: '0x346A222a6B5fe17CEe63a443371073180006E7c3'.toLowerCase(),
};

export const FORGE_BLENDER_CONTRACT_ID = {
  [Chain.SONGBIRD]: '0xa10af4079d5E05eCF0aCa35ceAE8989c198CB51A'.toLowerCase(),
  [Chain.UNITS]: '0x17419092FcaeBe8B9800f5D38769e04b4Bb4e0E8'.toLowerCase(),
};
