export const FORGE_MINER_CLAIMER_ABI = [
  {
    inputs: [
      {
        internalType: 'address',
        name: 'coreAddress',
        type: 'address',
      },
      {
        internalType: 'address',
        name: 'nftAddress',
        type: 'address',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'constructor',
  },
  {
    inputs: [],
    name: 'AccessControlBadConfirmation',
    type: 'error',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        internalType: 'bytes32',
        name: 'neededRole',
        type: 'bytes32',
      },
    ],
    name: 'AccessControlUnauthorizedAccount',
    type: 'error',
  },
  {
    inputs: [],
    name: 'EnforcedPause',
    type: 'error',
  },
  {
    inputs: [],
    name: 'ExpectedPause',
    type: 'error',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'uint256',
        name: 'recipeTypeId',
        type: 'uint256',
      },
      {
        indexed: true,
        internalType: 'uint256',
        name: 'botTypeId',
        type: 'uint256',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        indexed: false,
        internalType: 'uint256[]',
        name: 'tokensBurned',
        type: 'uint256[]',
      },
      {
        indexed: false,
        internalType: 'uint256[]',
        name: 'typesBurned',
        type: 'uint256[]',
      },
      {
        indexed: false,
        internalType: 'uint8[6]',
        name: 'raritiesBurned',
        type: 'uint8[6]',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'payed',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'address',
        name: 'tokenAddress',
        type: 'address',
      },
      {
        indexed: false,
        internalType: 'bool',
        name: 'isMinerToken',
        type: 'bool',
      },
    ],
    name: 'BotCrafted',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'Paused',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'uint256',
        name: 'recipeTypeId',
        type: 'uint256',
      },
      {
        indexed: true,
        internalType: 'uint256',
        name: 'botTypeId',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint8[6]',
        name: 'requirements',
        type: 'uint8[6]',
      },
      {
        indexed: false,
        internalType: 'uint8[]',
        name: 'slots',
        type: 'uint8[]',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'price',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'address',
        name: 'tokenAddress',
        type: 'address',
      },
      {
        indexed: false,
        internalType: 'bool',
        name: 'isMinerToken',
        type: 'bool',
      },
    ],
    name: 'RecipeAdded',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'uint256',
        name: 'recipeTypeId',
        type: 'uint256',
      },
      {
        indexed: true,
        internalType: 'uint256',
        name: 'botTypeId',
        type: 'uint256',
      },
    ],
    name: 'RecipeDeleted',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'previousAdminRole',
        type: 'bytes32',
      },
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'newAdminRole',
        type: 'bytes32',
      },
    ],
    name: 'RoleAdminChanged',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'sender',
        type: 'address',
      },
    ],
    name: 'RoleGranted',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'sender',
        type: 'address',
      },
    ],
    name: 'RoleRevoked',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'Unpaused',
    type: 'event',
  },
  {
    inputs: [],
    name: 'DEFAULT_ADMIN_ROLE',
    outputs: [
      {
        internalType: 'bytes32',
        name: '',
        type: 'bytes32',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'EDITOR_ROLE',
    outputs: [
      {
        internalType: 'bytes32',
        name: '',
        type: 'bytes32',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'recipeTypeId',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'botTypeId',
        type: 'uint256',
      },
      {
        internalType: 'uint8[6]',
        name: 'requirements',
        type: 'uint8[6]',
      },
      {
        internalType: 'uint8[]',
        name: 'slots',
        type: 'uint8[]',
      },
      {
        internalType: 'uint256',
        name: 'price',
        type: 'uint256',
      },
      {
        internalType: 'address',
        name: 'tokenAddress',
        type: 'address',
      },
      {
        internalType: 'bool',
        name: 'isMinerToken',
        type: 'bool',
      },
    ],
    name: 'addRecipe',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'recipeTokenId',
        type: 'uint256',
      },
      {
        internalType: 'uint256[]',
        name: 'tokensIds',
        type: 'uint256[]',
      },
    ],
    name: 'craft',
    outputs: [],
    stateMutability: 'payable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'botTypeId',
        type: 'uint256',
      },
    ],
    name: 'deleteRecipeByBotTypeId',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'recipeTypeId',
        type: 'uint256',
      },
    ],
    name: 'deleteRecipeByRecipeTypeId',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'botTypeId',
        type: 'uint256',
      },
    ],
    name: 'getRecipeByBotTypeId',
    outputs: [
      {
        components: [
          {
            internalType: 'uint256',
            name: 'recipeTypeId',
            type: 'uint256',
          },
          {
            internalType: 'uint256',
            name: 'botTypeId',
            type: 'uint256',
          },
          {
            internalType: 'uint8[6]',
            name: 'requirements',
            type: 'uint8[6]',
          },
          {
            internalType: 'uint8[]',
            name: 'slots',
            type: 'uint8[]',
          },
          {
            internalType: 'uint256',
            name: 'price',
            type: 'uint256',
          },
          {
            internalType: 'address',
            name: 'tokenAddress',
            type: 'address',
          },
          {
            internalType: 'bool',
            name: 'isMinerToken',
            type: 'bool',
          },
        ],
        internalType: 'struct Recipe',
        name: '',
        type: 'tuple',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'recipeTypeId',
        type: 'uint256',
      },
    ],
    name: 'getRecipeByRecipeTypeId',
    outputs: [
      {
        components: [
          {
            internalType: 'uint256',
            name: 'recipeTypeId',
            type: 'uint256',
          },
          {
            internalType: 'uint256',
            name: 'botTypeId',
            type: 'uint256',
          },
          {
            internalType: 'uint8[6]',
            name: 'requirements',
            type: 'uint8[6]',
          },
          {
            internalType: 'uint8[]',
            name: 'slots',
            type: 'uint8[]',
          },
          {
            internalType: 'uint256',
            name: 'price',
            type: 'uint256',
          },
          {
            internalType: 'address',
            name: 'tokenAddress',
            type: 'address',
          },
          {
            internalType: 'bool',
            name: 'isMinerToken',
            type: 'bool',
          },
        ],
        internalType: 'struct Recipe',
        name: '',
        type: 'tuple',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'getRecipes',
    outputs: [
      {
        components: [
          {
            internalType: 'uint256',
            name: 'recipeTypeId',
            type: 'uint256',
          },
          {
            internalType: 'uint256',
            name: 'botTypeId',
            type: 'uint256',
          },
          {
            internalType: 'uint8[6]',
            name: 'requirements',
            type: 'uint8[6]',
          },
          {
            internalType: 'uint8[]',
            name: 'slots',
            type: 'uint8[]',
          },
          {
            internalType: 'uint256',
            name: 'price',
            type: 'uint256',
          },
          {
            internalType: 'address',
            name: 'tokenAddress',
            type: 'address',
          },
          {
            internalType: 'bool',
            name: 'isMinerToken',
            type: 'bool',
          },
        ],
        internalType: 'struct Recipe[]',
        name: '',
        type: 'tuple[]',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
    ],
    name: 'getRoleAdmin',
    outputs: [
      {
        internalType: 'bytes32',
        name: '',
        type: 'bytes32',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'grantRole',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'hasRole',
    outputs: [
      {
        internalType: 'bool',
        name: '',
        type: 'bool',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'paused',
    outputs: [
      {
        internalType: 'bool',
        name: '',
        type: 'bool',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        internalType: 'address',
        name: 'callerConfirmation',
        type: 'address',
      },
    ],
    name: 'renounceRole',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'revokeRole',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'receiverAddress',
        type: 'address',
      },
    ],
    name: 'setReceiver',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes4',
        name: 'interfaceId',
        type: 'bytes4',
      },
    ],
    name: 'supportsInterface',
    outputs: [
      {
        internalType: 'bool',
        name: '',
        type: 'bool',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
];
