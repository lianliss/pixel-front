export const FORGE_BLENDER_ABI = [
  {
    inputs: [
      {
        internalType: 'address',
        name: 'core',
        type: 'address',
      },
      {
        internalType: 'address',
        name: 'pixelDust',
        type: 'address',
      },
      {
        internalType: 'address',
        name: 'blendCollection',
        type: 'address',
      },
      {
        internalType: 'address',
        name: 'slotCollection',
        type: 'address',
      },
      {
        internalType: 'address',
        name: 'craftedCollection',
        type: 'address',
      },
    ],
    stateMutability: 'nonpayable',
    type: 'constructor',
  },
  {
    inputs: [],
    name: 'AccessControlBadConfirmation',
    type: 'error',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        internalType: 'bytes32',
        name: 'neededRole',
        type: 'bytes32',
      },
    ],
    name: 'AccessControlUnauthorizedAccount',
    type: 'error',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'x',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'y',
        type: 'uint256',
      },
    ],
    name: 'PRBMath_MulDiv18_Overflow',
    type: 'error',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'x',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'y',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'denominator',
        type: 'uint256',
      },
    ],
    name: 'PRBMath_MulDiv_Overflow',
    type: 'error',
  },
  {
    inputs: [
      {
        internalType: 'UD60x18',
        name: 'x',
        type: 'uint256',
      },
    ],
    name: 'PRBMath_UD60x18_Exp2_InputTooBig',
    type: 'error',
  },
  {
    inputs: [
      {
        internalType: 'UD60x18',
        name: 'x',
        type: 'uint256',
      },
    ],
    name: 'PRBMath_UD60x18_Log_InputTooSmall',
    type: 'error',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: 'address',
        name: 'crafter',
        type: 'address',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'craftedTokenId',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'typeId',
        type: 'uint256',
      },
      {
        indexed: false,
        internalType: 'enum IForge.Rarity',
        name: 'rarity',
        type: 'uint8',
      },
    ],
    name: 'CraftCompleted',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: 'string',
        name: 'parameterName',
        type: 'string',
      },
      {
        indexed: false,
        internalType: 'uint8',
        name: 'newParameterId',
        type: 'uint8',
      },
    ],
    name: 'ParameterIdUpdated',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'address',
        name: 'user',
        type: 'address',
      },
      {
        indexed: false,
        internalType: 'uint8',
        name: 'parameterId',
        type: 'uint8',
      },
      {
        indexed: false,
        internalType: 'uint24',
        name: 'multiplier',
        type: 'uint24',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'adder',
        type: 'uint256',
      },
    ],
    name: 'ParameterUpdated',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'enum IForge.Rarity',
        name: 'rarity',
        type: 'uint8',
      },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'typeId',
        type: 'uint256',
      },
    ],
    name: 'RandomTypeIdSelected',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'previousAdminRole',
        type: 'bytes32',
      },
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'newAdminRole',
        type: 'bytes32',
      },
    ],
    name: 'RoleAdminChanged',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'sender',
        type: 'address',
      },
    ],
    name: 'RoleGranted',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'sender',
        type: 'address',
      },
    ],
    name: 'RoleRevoked',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'enum IForge.Rarity',
        name: 'rarity',
        type: 'uint8',
      },
      {
        indexed: false,
        internalType: 'uint256[]',
        name: 'tokenTypeIds',
        type: 'uint256[]',
      },
    ],
    name: 'TypeIdsAdded',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'enum IForge.Rarity',
        name: 'rarity',
        type: 'uint8',
      },
      {
        indexed: false,
        internalType: 'uint256[]',
        name: 'tokenTypeIds',
        type: 'uint256[]',
      },
    ],
    name: 'TypeIdsRemoved',
    type: 'event',
  },
  {
    inputs: [],
    name: 'BLEND_COLLECTION',
    outputs: [
      {
        internalType: 'contract IPXLsNFTV5',
        name: '',
        type: 'address',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'CORE',
    outputs: [
      {
        internalType: 'contract ICoreV5Auth',
        name: '',
        type: 'address',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'CRAFTED_COLLECTION',
    outputs: [
      {
        internalType: 'contract IPXLsNFTV5',
        name: '',
        type: 'address',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'DEFAULT_ADMIN_ROLE',
    outputs: [
      {
        internalType: 'bytes32',
        name: '',
        type: 'bytes32',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'PIXEL_DUST',
    outputs: [
      {
        internalType: 'contract IPXLDust',
        name: '',
        type: 'address',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'SETTINGS_EDITOR_ROLE',
    outputs: [
      {
        internalType: 'bytes32',
        name: '',
        type: 'bytes32',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'SLOT_COLLECTION',
    outputs: [
      {
        internalType: 'contract IPXLsNFTV5',
        name: '',
        type: 'address',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'SLOT_EDITOR_ROLE',
    outputs: [
      {
        internalType: 'bytes32',
        name: '',
        type: 'bytes32',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256[]',
        name: 'tokenTypeIds',
        type: 'uint256[]',
      },
      {
        internalType: 'enum IForge.Rarity',
        name: 'rarity',
        type: 'uint8',
      },
    ],
    name: 'addTypeIdsByRarity',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'pixelDustAmount',
        type: 'uint256',
      },
      {
        internalType: 'uint256[]',
        name: 'blendTokenIds',
        type: 'uint256[]',
      },
      {
        internalType: 'address',
        name: 'user',
        type: 'address',
      },
    ],
    name: 'calculateRarityDistribution',
    outputs: [
      {
        internalType: 'UD60x18[5]',
        name: 'distribution',
        type: 'uint256[5]',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256',
        name: 'pixelDustAmount',
        type: 'uint256',
      },
      {
        internalType: 'uint256[]',
        name: 'blendTokenIds',
        type: 'uint256[]',
      },
    ],
    name: 'craft',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
    ],
    name: 'getRoleAdmin',
    outputs: [
      {
        internalType: 'bytes32',
        name: '',
        type: 'bytes32',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'enum IForge.Rarity',
        name: 'rarity',
        type: 'uint8',
      },
    ],
    name: 'getTotalTypeIdsByRarity',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'enum IForge.Rarity',
        name: 'rarity',
        type: 'uint8',
      },
      {
        internalType: 'uint256',
        name: 'offset',
        type: 'uint256',
      },
      {
        internalType: 'uint256',
        name: 'limit',
        type: 'uint256',
      },
    ],
    name: 'getTypeIdsByRarityPaginated',
    outputs: [
      {
        internalType: 'uint256[]',
        name: '',
        type: 'uint256[]',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'grantRole',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'hasRole',
    outputs: [
      {
        internalType: 'bool',
        name: '',
        type: 'bool',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint256[]',
        name: 'tokenTypeIds',
        type: 'uint256[]',
      },
      {
        internalType: 'enum IForge.Rarity',
        name: 'rarity',
        type: 'uint8',
      },
    ],
    name: 'removeTypeIdsByRarity',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        internalType: 'address',
        name: 'callerConfirmation',
        type: 'address',
      },
    ],
    name: 'renounceRole',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes32',
        name: 'role',
        type: 'bytes32',
      },
      {
        internalType: 'address',
        name: 'account',
        type: 'address',
      },
    ],
    name: 'revokeRole',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint8',
        name: 'parameterId',
        type: 'uint8',
      },
    ],
    name: 'setSlot0ParameterId',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint8',
        name: 'parameterId',
        type: 'uint8',
      },
    ],
    name: 'setSlot1ParameterId',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [],
    name: 'slot0ParameterId',
    outputs: [
      {
        internalType: 'uint8',
        name: '',
        type: 'uint8',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'slot1ParameterId',
    outputs: [
      {
        internalType: 'uint8',
        name: '',
        type: 'uint8',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'address',
        name: 'user',
        type: 'address',
      },
      {
        internalType: 'uint256',
        name: 'slotNumber',
        type: 'uint256',
      },
    ],
    name: 'slotMultiplier',
    outputs: [
      {
        internalType: 'UD60x18',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'bytes4',
        name: 'interfaceId',
        type: 'bytes4',
      },
    ],
    name: 'supportsInterface',
    outputs: [
      {
        internalType: 'bool',
        name: '',
        type: 'bool',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'uint64',
        name: 'userId',
        type: 'uint64',
      },
      {
        internalType: 'uint8',
        name: 'parameterId',
        type: 'uint8',
      },
      {
        internalType: 'uint24',
        name: 'multiplier',
        type: 'uint24',
      },
      {
        internalType: 'uint256',
        name: 'adder',
        type: 'uint256',
      },
    ],
    name: 'updateParameter',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
];
