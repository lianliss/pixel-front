'use strict';
import React from 'react';
import ForgeDroneMiner from 'lib/Forge/pages/miner-claimer/ForgeDroneMiner.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain } from 'services/multichain/chains';
import NotAvailableChain from '../../../../shared/blocks/NotAvailableChain/NotAvailableChain.tsx';
import { IS_DEVELOP, IS_TELEGRAM } from '@cfg/config.ts';
import NotAvailableBrowser from '../../../../shared/blocks/NotAvailableBrowser/NotAvailableBrowser.tsx';
import ForgeLayout from 'lib/Forge/components/ForgeLayout/ForgeLayout.tsx';
import { FORGE_MINER_CLAIMER_CONTRACT_ID } from 'lib/Forge/api-contract/contract.ts';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';

function ForgeDroneMinerContainer() {
  const account = useAccount();

  if (!FORGE_MINER_CLAIMER_CONTRACT_ID[account.chainId]) {
    return (
      <>
        <ChainSwitcher chains={[Chain.SKALE, Chain.SONGBIRD]} />
        <NotAvailableChain chainId={Chain.SONGBIRD} title='Not Available' />
      </>
    );
  }
  if (!IS_DEVELOP && !IS_TELEGRAM) {
    return <NotAvailableBrowser />;
  }

  return (
    <ForgeLayout>
      <ChainSwitcher chains={[Chain.SKALE, Chain.SONGBIRD]} />

      <ForgeDroneMiner />
    </ForgeLayout>
  );
}

export default ForgeDroneMinerContainer;
