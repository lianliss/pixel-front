'use strict';
import React, { useEffect, useMemo, useState } from 'react';
import MinerClaimerCraft from 'app/lib/Forge/components/MinerClaimerCraft';
import DroneMinerCraftSelectNftDialog from 'lib/Forge/components/ForgeSelectNftDialog/ForgeSelectNftDialog.tsx';
import { INftItemDto, NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import { useMinerClaimerCraft } from 'lib/Forge/hooks/useMinerClaimerCraft.ts';
import { loadUserNftRecipes } from 'lib/Forge/api-server/miner-claimer.store.ts';

import MinerClaimerCraftSelectRecipeDialog from 'app/lib/Forge/components/MinerClaimerCraftSelectRecipeDialog';
import { useBalance } from '@chain/hooks/useBalance.ts';
import toaster from 'services/toaster.tsx';
import { IDroneMinerCreated } from 'lib/Forge/components/MinerClaimerCraft/MinerClaimerCraft.tsx';
import { createTxUrl } from '@chain/utils/url.ts';
import DropNftDialog from 'lib/Chests/components/ChestDropDialog/ChestDropDialog.tsx';
import { createNftFromType } from 'lib/NFT/utils/create-nft-from-type.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useToken } from '@chain/hooks/useToken.ts';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import wait from 'utils/wait';
import { useGasBalance } from '@chain/hooks/useGasBalance.ts';

const defaultNfts: Record<NftItemRarity, INftItemDto[]> = {
  [NftItemRarity.Common]: [],
  [NftItemRarity.UnCommon]: [],
  [NftItemRarity.Rare]: [],
  [NftItemRarity.Epic]: [],
  [NftItemRarity.Legendary]: [],
};

function ForgeDroneMiner() {
  const account = useAccount();
  const gasBalance = useGasBalance({
    address: account.accountAddress,
  });

  const nftAddress = NFT_CONTRACT_ID[account.chainId];
  const minerAddress = MINER_CONTRACT_ID[account.chainId].core;
  const noGas = gasBalance.isFetched && !gasBalance.data?.formatted;

  const [created, setCreated] = useState<IDroneMinerCreated | null>(null);
  const [droppedNft, setDroppedNft] = useState<INftItemDto | null>(null);
  const [openSelectNft, setOpenSelectNft] = useState(false);
  const [openSelectRecipe, setOpenSelectRecipe] = useState(false);
  const [selectNftRarity, setSelectNftRarity] = useState<NftItemRarity | null>(null);
  const [selectedRecipe, setSelectedRecipe] = useState<INftItemDto | null>(null);
  const [selectedNft, setSelectedNft] = useState<Record<NftItemRarity, INftItemDto[]>>({
    [NftItemRarity.Common]: [],
    [NftItemRarity.UnCommon]: [],
    [NftItemRarity.Rare]: [],
    [NftItemRarity.Epic]: [],
    [NftItemRarity.Legendary]: [],
  });
  const recipe = selectedRecipe?.type?.droneRecipe;
  const recipeTokenAddress = recipe?.tokenShard ? minerAddress : recipe?.tokenAddress;
  const recipeTokenPriceWei = recipe?.tokenShard ? minerAddress : recipe?.priceWei;

  const forgeMiner = useMinerClaimerCraft();
  const recipeToken = useToken({
    address: recipeTokenAddress,
    skip: !recipeTokenAddress,
  });
  const recipeTokenBalance = useBalance({
    address: account.accountAddress,
    token: recipeToken.data?.address,
    skip: !recipeToken.data?.address || !account.isConnected,
  });

  const handleCraft = async () => {
    if (!selectedRecipe || !recipeTokenAddress || !recipeTokenPriceWei) {
      return;
    }

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      const nftAssetIds = Object.values(selectedNft)
        .flatMap((el) => el)
        .map((el) => el.assetId);

      const txHash = await forgeMiner.craft({
        recipeId: selectedRecipe.assetId,
        nftAssetIds: nftAssetIds,
        tokenAddress: recipeTokenAddress,
        priceInWei: recipeTokenPriceWei,
      });

      const createdNft = createNftFromType(
        selectedRecipe.type?.droneRecipe?.resultType || selectedRecipe.type,
        {}
      );
      setCreated({ txHash, txUrl: createTxUrl(account, txHash) });
      setDroppedNft(createdNft);

      setSelectedRecipe(null);
      setSelectedNft(defaultNfts);

      await wait(5_000);

      loadUserNftRecipes({
        userAddress: account.accountAddress,
        contractId: nftAddress.toLowerCase(),
      }).then();

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      toaster.logError(e, 'craft');
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }
  };

  const disabled = useMemo(() => {
    if (!selectedRecipe) {
      return true;
    }

    const allSelected = selectedRecipe.type.droneRecipe?.requirements.slice(1).every((count, i) => {
      return selectedNft[i + 1]?.length === count;
    });

    return !allSelected;
  }, [selectedNft, selectedRecipe]);

  const requiredCountByRarity = selectedRecipe
    ? selectedRecipe.type.droneRecipe?.requirements.slice(1)[selectNftRarity - 1]
    : 0;

  useEffect(() => {
    if (account.isConnected && nftAddress) {
      loadUserNftRecipes({
        userAddress: account.accountAddress,
        contractId: nftAddress.toLowerCase(),
      }).then();
    }
  }, [account.isConnected, nftAddress]);
  useBackAction('/wallet');

  return (
    <div>
      <MinerClaimerCraft
        selectedNft={selectedNft}
        onSelectNft={(rarity) => {
          setSelectNftRarity(rarity);
          setOpenSelectNft(true);
        }}
        selectedRecipe={selectedRecipe}
        onSelectRecipe={() => {
          setOpenSelectRecipe(true);
          setCreated(null);
        }}
        disabled={disabled}
        onCraft={handleCraft}
        loadingCraft={forgeMiner.loading}
        coreBalance={recipeTokenBalance.data?.formatted}
        coreBalanceLoading={recipeTokenBalance.isLoading}
        priceSymbol={recipeToken.data?.symbol}
        priceImage={recipeToken.data?.image}
        created={created}
        slots={recipe?.slots}
        noGas={noGas}
      />

      <DroneMinerCraftSelectNftDialog
        open={openSelectNft}
        onClose={() => setOpenSelectNft(false)}
        rarity={selectNftRarity}
        selectedNft={selectedNft[selectNftRarity] || []}
        onApply={(arr) => {
          setSelectedNft({ ...selectedNft, [selectNftRarity]: arr });
          setSelectNftRarity(null);
        }}
        onCancel={() => {
          setSelectedNft(null);
          setSelectNftRarity(null);
        }}
        accountAddress={account.accountAddress}
        nftAddress={nftAddress}
        requireCount={requiredCountByRarity}
        slots={recipe?.slots}
      />
      <MinerClaimerCraftSelectRecipeDialog
        open={openSelectRecipe}
        selectedRecipe={selectedRecipe}
        onApply={(r) => setSelectedRecipe(r)}
        onCancel={() => setSelectedRecipe(null)}
        onClose={() => setOpenSelectRecipe(false)}
      />

      {droppedNft && <DropNftDialog nftItem={droppedNft} onClose={() => setDroppedNft(null)} />}
    </div>
  );
}

export default ForgeDroneMiner;
