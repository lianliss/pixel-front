import { AdminLayout } from 'lib/AdminV5/components/AdminLayout/AdminLayout.tsx';
import { useAdminForgeBlenderTypesCount } from 'lib/Forge/hooks/useAdminForgeBlenderTypesCount.ts';
import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import { ButtonGroup } from '@ui-kit/ButtonGroup/ButtonGroup.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import Badge from '@mui/material/Badge';
import { useEffect, useState } from 'react';
import { useAdminForgeBlenderTypes } from 'lib/Forge/hooks/useAdminForgeBlenderTypes.ts';
import { useAdminForgeBlenderApi } from 'lib/Forge/hooks/useAdminForgeBlenderApi.ts';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import Box from '@mui/material/Box';
import toaster from 'services/toaster.tsx';

const rarities = [
  NftItemRarity.Common,
  NftItemRarity.UnCommon,
  NftItemRarity.Rare,
  NftItemRarity.Epic,
  NftItemRarity.Legendary,
];

function AdminBlenderPage() {
  const commonData = useAdminForgeBlenderTypesCount(NftItemRarity.Common);
  const uncommonData = useAdminForgeBlenderTypesCount(NftItemRarity.UnCommon);
  const rareData = useAdminForgeBlenderTypesCount(NftItemRarity.Rare);
  const epicData = useAdminForgeBlenderTypesCount(NftItemRarity.Epic);
  const legData = useAdminForgeBlenderTypesCount(NftItemRarity.Legendary);

  const countMap = {
    [NftItemRarity.Common]: commonData,
    [NftItemRarity.UnCommon]: uncommonData,
    [NftItemRarity.Rare]: rareData,
    [NftItemRarity.Epic]: epicData,
    [NftItemRarity.Legendary]: legData,
  };

  const [selectedRarity, setSelectedRarity] = useState(NftItemRarity.Common);
  const types = useAdminForgeBlenderTypes(selectedRarity);
  const adminApi = useAdminForgeBlenderApi();
  const [nextTypes, setNextTypes] = useState<string>('');

  const selectedRarityCount = countMap[selectedRarity];

  useEffect(() => {
    setNextTypes(types.data.join(','));
  }, [types.data]);

  const handleSave = async () => {
    try {
      const nextTypeIds = nextTypes.split(',').map(Number).filter(Boolean);
      const mapNext = Object.fromEntries(nextTypeIds.map((el) => [el, true]));

      const excludedTypes = types.data.filter((el) => !(el in mapNext));

      if (excludedTypes.length) {
        await adminApi.removeTypes({ types: excludedTypes, rarity: selectedRarity });
      }
      if (nextTypeIds.length) {
        await adminApi.addTypes({ types: nextTypeIds, rarity: selectedRarity });
      }

      await selectedRarityCount.refetch();
      await types.refetch();
    } catch (e) {
      toaster.captureException(e);
    }
  };

  const loading = adminApi.write.isPending || selectedRarityCount.loading || types.loading;

  return (
    <AdminLayout>
      <Box>
        <ButtonGroup variant='outlined'>
          {rarities.map((rarity) => (
            <Badge badgeContent={countMap[rarity].data || 0} color='primary' key={`rar-${rarity}`}>
              <Button
                variant={selectedRarity === rarity ? 'contained' : undefined}
                onClick={() => setSelectedRarity(rarity)}
              >
                {getNftRarityLabel(rarity)}
              </Button>
            </Badge>
          ))}
        </ButtonGroup>

        <TextField
          value={nextTypes}
          fullWidth
          label={`Types for ${getNftRarityLabel(selectedRarity)}`}
          sx={{ mt: 3 }}
          minRows={3}
          multiline
          onChange={(e) => setNextTypes(e.target.value)}
          disabled={loading}
        />
        <Button
          variant='contained'
          color='primary'
          sx={{ mt: 2 }}
          onClick={handleSave}
          loading={loading}
        >
          Save
        </Button>
      </Box>
    </AdminLayout>
  );
}

export default AdminBlenderPage;
