import { AdminLayout } from 'lib/AdminV5/components/AdminLayout/AdminLayout.tsx';
import RecipesGrid from 'lib/Forge/components/_admin/RecipesGrid.tsx';
import { useState } from 'react';
import { INftTypeDto } from 'lib/NFT/api-server/nft.types.ts';
import EditNftRecipeDialog from 'lib/Forge/components/_admin/EditNftRecipeDialog.tsx';

function AdminRecipesPage() {
  const [type, setType] = useState<INftTypeDto | null>(null);

  return (
    <AdminLayout>
      <RecipesGrid onEdit={(t) => setType(t)} />
      <EditNftRecipeDialog open={!!type} nftType={type} onClose={() => setType(null)} />
    </AdminLayout>
  );
}

export default AdminRecipesPage;
