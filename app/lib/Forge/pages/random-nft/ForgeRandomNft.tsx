'use strict';
import React, { useEffect, useMemo, useState } from 'react';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import RandomNftCraft from 'lib/Forge/components/RandomNftCraft';
import { INftItemDto, NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import DroneMinerCraftSelectNftDialog from 'lib/Forge/components/ForgeSelectNftDialog/ForgeSelectNftDialog.tsx';
import { useAdminForgeBlenderResult } from 'lib/Forge/hooks/useAdminForgeBlenderResult.ts';
import { useGasBalance } from '@chain/hooks/useGasBalance.ts';

function ForgeRandomNft() {
  const account = useAccount();
  const gasBalance = useGasBalance({
    address: account.accountAddress,
  });

  const nftAddress = NFT_CONTRACT_ID[account.chainId];
  const noGas = gasBalance.isFetched && !gasBalance.data?.formatted;

  const [dustAmount, setDustAmount] = useState(1000);

  const [openSelectNft, setOpenSelectNft] = useState(false);
  const [selectNftRarity, setSelectNftRarity] = useState<NftItemRarity | null>(null);
  const [selectedNft, setSelectedNft] = useState<Record<NftItemRarity, INftItemDto[]>>({
    [NftItemRarity.Common]: [],
    [NftItemRarity.UnCommon]: [],
    [NftItemRarity.Rare]: [],
    [NftItemRarity.Epic]: [],
    [NftItemRarity.Legendary]: [],
  });

  const nftAssetIds = Object.values(selectedNft)
    .flatMap((el) => el)
    .map((el) => Number(el.assetId));
  const forgeBlender = useAdminForgeBlenderResult(dustAmount, nftAssetIds);

  const handleCraft = () => {};

  useBackAction('/wallet');

  return (
    <div>
      <RandomNftCraft
        amount={dustAmount}
        onChangeAmount={setDustAmount}
        chances={forgeBlender.data}
        chancesLoading={forgeBlender.loading}
        // requirements={requirements}
        onSelectNft={(rarity) => {
          setSelectNftRarity(rarity);
          setOpenSelectNft(true);
        }}
        onCraft={handleCraft}
        selectedNft={selectedNft}
        disabledCraft
        noGas={noGas}
      />

      <DroneMinerCraftSelectNftDialog
        open={openSelectNft}
        onClose={() => setOpenSelectNft(false)}
        rarity={selectNftRarity}
        selectedNft={selectedNft[selectNftRarity] || []}
        onApply={(arr) => {
          setSelectedNft({ ...selectedNft, [selectNftRarity]: arr });
          setSelectNftRarity(null);
        }}
        onCancel={() => {
          setSelectedNft(null);
          setSelectNftRarity(null);
        }}
        accountAddress={account.accountAddress}
        nftAddress={nftAddress}
        // requireCount={requiredCountByRarity}
      />
    </div>
  );
}

export default ForgeRandomNft;
