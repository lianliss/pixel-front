'use strict';
import React, { useState } from 'react';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain } from 'services/multichain/chains';
import NotAvailableChain from '../../../../shared/blocks/NotAvailableChain/NotAvailableChain.tsx';
import { IS_DEVELOP, IS_TELEGRAM } from '@cfg/config.ts';
import NotAvailableBrowser from '../../../../shared/blocks/NotAvailableBrowser/NotAvailableBrowser.tsx';
import ForgeLayout from 'lib/Forge/components/ForgeLayout/ForgeLayout.tsx';
import ForgeRandomNft from 'lib/Forge/pages/random-nft/ForgeRandomNft.tsx';
import { useAdminForgeBlenderResult } from 'lib/Forge/hooks/useAdminForgeBlenderResult.ts';
import { FORGE_BLENDER_CONTRACT_ID } from 'lib/Forge/api-contract/contract.ts';

function ForgeRandomNftContainer() {
  const account = useAccount();

  if (!FORGE_BLENDER_CONTRACT_ID[account.chainId]) {
    return <NotAvailableChain chainId={Chain.SONGBIRD} title='Not Available' />;
  }
  if (!IS_DEVELOP && !IS_TELEGRAM) {
    return <NotAvailableBrowser />;
  }

  return (
    <ForgeLayout>
      <ForgeRandomNft />
    </ForgeLayout>
  );
}

export default ForgeRandomNftContainer;
