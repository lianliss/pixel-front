import { FORGE_MINER_CLAIMER_CONTRACT_ID } from 'lib/Forge/api-contract/contract.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { FORGE_MINER_CLAIMER_ABI } from 'lib/Forge/api-contract/abi/FORGE_MINER_DRONE.abi.ts';
import { useState } from 'react';
import { ZERO_ADDRESS } from '@cfg/config.ts';

export function useMinerClaimerCraft() {
  const account = useAccount();

  const contractId = FORGE_MINER_CLAIMER_CONTRACT_ID[account.chainId];

  const [loadingCraft, setLoadingCraft] = useState(false);

  const write = useWriteContract({
    abi: FORGE_MINER_CLAIMER_ABI,
    address: contractId,
  });

  const handleCraft = async (payload: {
    recipeId: string;
    nftAssetIds: string[];
    tokenAddress: string;
    priceInWei: string;
  }) => {
    setLoadingCraft(true);

    try {
      const recipe = await write.writeContractAsync({
        args: [payload.recipeId, payload.nftAssetIds],
        functionName: 'craft',
        overrides:
          payload.tokenAddress === ZERO_ADDRESS
            ? { value: payload.priceInWei as bigint }
            : undefined,
        waitTime: 5_000,
      });

      return recipe.transactionHash;
    } catch (e) {
      console.error('[craft]', payload, e);
      throw e;
    } finally {
      setLoadingCraft(false);
    }
  };

  return {
    craft: handleCraft,
    available: !!contractId,
    loading: loadingCraft,
  };
}
