import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { FORGE_BLENDER_CONTRACT_ID } from 'lib/Forge/api-contract/contract.ts';
import { FORGE_BLENDER_ABI } from 'lib/Forge/api-contract/abi/FORGE_BLENDER.abi.ts';
import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import wei from 'utils/wei';

export function useAdminForgeBlenderResult(dust: number, tokens: number[]) {
  const account = useAccount();
  const contractId = FORGE_BLENDER_CONTRACT_ID[account.chainId];

  const result = useReadContract({
    initData: [0, 0, 0, 0, 0],
    abi: FORGE_BLENDER_ABI,
    functionName: 'calculateRarityDistribution',
    address: contractId,
    args: [dust, tokens, account.accountAddress],
    skip: !contractId || !account.isConnected || !dust,
    select: (res) => res.map((el) => Number(wei.from(el, 3))),
  });

  return result;
}
