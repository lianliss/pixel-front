import React from 'react';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import toaster from 'services/toaster.tsx';
import { FORGE_MINER_CLAIMER_CONTRACT_ID } from 'lib/Forge/api-contract/contract.ts';
import { FORGE_MINER_CLAIMER_ABI } from 'lib/Forge/api-contract/abi/FORGE_MINER_DRONE.abi.ts';
import get from 'lodash/get.js';
import wei from 'utils/wei.jsx';
import { NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';

type AutoClaimRecipe = {
  botTypeId: number;
  recipeTypeId: number;
  requirements: number[6];
  price: number;
  tokenAddress: string;
  slots: number[];
};

export type AutoClaimRecipes = Record<number, AutoClaimRecipe>;

export function useAdminRecipeApi() {
  const { chainId, isConnected } = useAccount();

  const [isLoading, setIsLoading] = React.useState(true);
  const [recipes, setRecipes] = React.useState<AutoClaimRecipes>({});
  const [list, setList] = React.useState<number[]>([]);

  const forgeAddress = FORGE_MINER_CLAIMER_CONTRACT_ID[chainId];

  const recipesData = useReadContract({
    abi: FORGE_MINER_CLAIMER_ABI,
    address: forgeAddress,
    skip: !forgeAddress || !isConnected,
    functionName: 'getRecipes',
  });

  const write = useWriteContract({
    abi: FORGE_MINER_CLAIMER_ABI,
    address: forgeAddress,
  });

  const updateRecipes = React.useMemo(
    () => async () => {
      try {
        if (!isConnected || !chainId) return;
        setIsLoading(true);
        const data = await recipesData.refetch();
        const rawData = get(data, 'data', data);
        const recipes: AutoClaimRecipes = {};
        const list: number[] = [];
        rawData.map((r) => {
          const recipeTypeId = Number(r.recipeTypeId);
          recipes[recipeTypeId] = {
            recipeTypeId,
            botTypeId: Number(r.botTypeId),
            requirements: r.requirements.map((r) => Number(r)),
            price: wei.from(r.price),
            slots: r.slots.map(Number),
            tokenAddress: r.tokenAddress,
          };
          list.push(recipeTypeId);
        });
        setRecipes(recipes);
        setList(list);
      } catch (error) {
        console.error('[updateBots]', error);
      }
      setIsLoading(false);
    },
    [isConnected, chainId]
  );

  React.useEffect(() => {
    if (isConnected) {
      updateRecipes().then();
    }
  }, [updateRecipes]);

  const addRecipe = React.useCallback(
    async ({
      recipeTypeId,
      botTypeId,
      rarities,
      tokenAddress,
      price,
      isMinerAddress,
      slots,
    }: {
      recipeTypeId: number;
      botTypeId: number;
      rarities: number[];
      price: number;
      tokenAddress: string;
      isMinerAddress: boolean;
      slots: NftItemSlot[];
    }) => {
      try {
        setIsLoading(true);
        await write.writeContractAsync({
          functionName: 'addRecipe',
          args: [
            recipeTypeId,
            botTypeId,
            rarities.length <= 5 ? [0, ...rarities] : rarities,
            slots,
            wei.to(price),
            tokenAddress,
            isMinerAddress,
          ],
        });
        await updateRecipes();
      } catch (error) {
        toaster.logError(error, '[addRecipe]');
      }
      setIsLoading(false);
    },
    [updateRecipes]
  );

  const unsetRecipe = React.useMemo(
    () => async (typeId) => {
      try {
        setIsLoading(true);
        await write.writeContractAsync({
          functionName: 'deleteRecipeByRecipeTypeId',
          args: [typeId],
        });
        await updateRecipes();
      } catch (error) {
        toaster.logError(error, '[unsetRecipe]');
      }
      setIsLoading(false);
    },
    [updateRecipes]
  );

  return {
    list,
    data: recipes,
    loading: isLoading,
    refetch: updateRecipes,
    set: addRecipe,
    unset: unsetRecipe,
  };
}
