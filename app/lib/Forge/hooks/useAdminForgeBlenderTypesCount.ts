import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { FORGE_BLENDER_CONTRACT_ID } from 'lib/Forge/api-contract/contract.ts';
import { FORGE_BLENDER_ABI } from 'lib/Forge/api-contract/abi/FORGE_BLENDER.abi.ts';
import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';

export function useAdminForgeBlenderTypesCount(rarity: NftItemRarity) {
  const account = useAccount();
  const contractId = FORGE_BLENDER_CONTRACT_ID[account.chainId];

  const countResult = useReadContract({
    abi: FORGE_BLENDER_ABI,
    functionName: 'getTotalTypeIdsByRarity',
    address: contractId,
    args: [rarity],
    skip: !contractId || !account.isConnected,
    select: (res) => Number(res),
  });

  return countResult;
}
