import { FORGE_BLENDER_ABI } from 'lib/Forge/api-contract/abi/FORGE_BLENDER.abi.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { FORGE_BLENDER_CONTRACT_ID } from 'lib/Forge/api-contract/contract.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';

export function useForgeBlenderApi() {
  const account = useAccount();
  const contractId = FORGE_BLENDER_CONTRACT_ID[account.chainId];

  const write = useWriteContract({
    abi: FORGE_BLENDER_ABI,
    address: contractId,
  });

  const craft = (payload: { dustAmount: number; typeIds: number[] }) =>
    write.writeContractAsync({
      functionName: 'craft',
      args: [payload.dustAmount, payload.typeIds],
    });

  return {
    write,
    craft,
  };
}
