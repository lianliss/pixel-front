import { FORGE_BLENDER_ABI } from 'lib/Forge/api-contract/abi/FORGE_BLENDER.abi.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { FORGE_BLENDER_CONTRACT_ID } from 'lib/Forge/api-contract/contract.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';

export function useAdminForgeBlenderApi() {
  const account = useAccount();
  const contractId = FORGE_BLENDER_CONTRACT_ID[account.chainId];

  const write = useWriteContract({
    abi: FORGE_BLENDER_ABI,
    address: contractId,
  });

  const addTypes = (payload: { types: number[]; rarity: NftItemRarity }) =>
    write.writeContractAsync({
      functionName: 'addTypeIdsByRarity',
      args: [payload.types, payload.rarity],
    });
  const removeTypes = (payload: { types: number[]; rarity: NftItemRarity }) =>
    write.writeContractAsync({
      functionName: 'removeTypeIdsByRarity',
      args: [payload.types, payload.rarity],
    });

  return {
    write,
    addTypes,
    removeTypes,
  };
}
