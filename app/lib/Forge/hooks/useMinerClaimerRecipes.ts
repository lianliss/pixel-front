import { useEffect } from 'react';
import { loadNftRecipes, nftRecipesStore } from 'lib/Forge/api-server/miner-claimer.store.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react';
import { Chain } from 'services/multichain/chains';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';

export function useMinerClaimerRecipes({ skip }: { skip?: boolean }) {
  const account = useAccount();

  const state = useUnit(nftRecipesStore);
  const nftAddress = NFT_CONTRACT_ID[account.chainId];

  useEffect(() => {
    if (account.isConnected && !skip && nftAddress) {
      loadNftRecipes({ contractId: nftAddress.toLowerCase() }).then();
    }
  }, [account.isConnected, account.accountAddress, skip, nftAddress, account.chainId]);

  return {
    state: state.state,
    error: state.error,
    loading: state.loading,
    isFetched: state.isFetched,
  };
}
