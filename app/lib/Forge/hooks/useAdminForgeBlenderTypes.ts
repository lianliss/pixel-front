import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { FORGE_BLENDER_CONTRACT_ID } from 'lib/Forge/api-contract/contract.ts';
import { FORGE_BLENDER_ABI } from 'lib/Forge/api-contract/abi/FORGE_BLENDER.abi.ts';
import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import { IPaginationResult } from 'utils/hooks/usePagination.ts';

export function useAdminForgeBlenderTypes(rarity: NftItemRarity) {
  const account = useAccount();
  const contractId = FORGE_BLENDER_CONTRACT_ID[account.chainId];

  const result = useReadContract<number[]>({
    initData: [],
    abi: FORGE_BLENDER_ABI,
    functionName: 'getTypeIdsByRarityPaginated',
    address: contractId,
    args: [rarity, 0, 500],
    skip: !contractId || !account.isConnected,
    select: (res) => res.map(Number),
  });

  return result;
}
