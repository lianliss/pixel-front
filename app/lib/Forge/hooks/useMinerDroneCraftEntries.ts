import { useCallback, useEffect } from 'react';
import { useUnit } from 'effector-react';
import {
  minerDroneEntriesStore,
  loadMinerDroneEntries,
} from 'lib/Forge/api-server/miner-drone-craft.store.ts';

export function useMinerDroneCraftEntries({
  minerDroneCraftAddress,
  accountAddress,
}: {
  minerDroneAddress: string;
  accountAddress?: string;
}) {
  const entries = useUnit(minerDroneEntriesStore);

  const reloadMinerDrone = useCallback(() => {
    loadMinerDroneEntries({
      minerDroneCraftAddress: minerDroneCraftAddress,
      limit: 50,
      offset: 0,
    }).then();
  }, [accountAddress, minerDroneCraftAddress]);

  useEffect(() => {
    if (accountAddress) {
      reloadMinerDrone();
    }
  }, [reloadMinerDrone]);

  return {
    data: entries.data,
    loading: entries.loading,
    refetch: reloadMinerDrone,
    isFetched: entries.isFetched,
  };
}
