import { createEffect } from 'effector';
import { shopApi } from 'lib/Shop/api-server/shop.api.ts';
import {
  ICreateShopPaymentDto,
  IFindShopItemsDto,
  IFindShopItemsResponse,
  IFindShopPaymentsDto,
} from 'lib/Shop/api-server/shop.types.ts';
import { createStoreState } from 'utils/store/createStoreState.ts';

//

export const loadShopItems = createEffect(async (params: IFindShopItemsDto) => {
  const res = await shopApi.findItems(params);
  return res;
});

export const shopItemsStore = createStoreState(loadShopItems);

//

export const loadShopPayments = createEffect(async (params: IFindShopPaymentsDto) => {
  const res = await shopApi.findPayments(params);
  return res;
});

export const shopPaymentsStore = createStoreState(loadShopPayments);

//

export const createShopPayment = createEffect(async (params: ICreateShopPaymentDto) => {
  const res = await shopApi.createPayment(params);
  return res;
});

export const shopPaymentStore = createStoreState(createShopPayment);
