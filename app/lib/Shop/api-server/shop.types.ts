import { IPaginatedQuery } from 'lib/Marketplace/api-server/market.types.ts';
import { IPaginatedResult } from 'lib/Quests/api-server/quests.types.ts';

// enum

export enum ShopPaymentStatus {
  Open = 'open',
  Cancelled = 'cancelled',
  Complete = 'complete',
}

export enum ShopItemRewardType {
  Chest = 'Chest',
}

export enum ShopProvider {
  Crypto = 'Crypto',
  Telegram = 'Telegram',
}
// entity

export type IShopItem<TDate = Date> = {
  id: string;

  // payment service
  provider: ShopProvider;
  // providerContext: IShopItemProviderContext[ShopProvider];

  // info
  title: string;
  summary: string;
  image: string;

  price: number;
  currency: string;

  disabled: boolean;
  deleted: boolean;

  createdAt: TDate;

  // relations
  rewards?: IShopItemReward<ShopItemRewardType, TDate>[];
};

export type IShopPayment<TDate = Date> = {
  id: string;

  // checkout
  checkoutId: string;
  checkoutUrl: string;
  checkoutStatus: ShopCheckoutStatus;
  checkoutProvider: ShopProvider;
  checkoutExpireAt: TDate;

  userAddress: string;
  shopItemId: string;
  status: ShopStatus;

  rewardsTotal: number;
  rewardsCompleted: number;

  createdAt: TDate;

  shopItem?: IShopItem<TDate>;
};

export type IShopItemRewardContext = {
  [ShopItemRewardType.Chest]: { chainId: number; chestId: number };
};

export type IShopItemReward<T extends ShopItemRewardType = ShopItemRewardType, TDate = Date> = {
  id: string;

  shopItemId: string;

  // info
  title: string;
  image: string;
  quantity: number;

  //
  type: ShopItemRewardType;
  context: IShopItemRewardContext[T];

  createdAt: TDate;
};

// dto

export type IShopPaymentDto<TDate = number> = Omit<
  IShopPayment<TDate>,
  'checkoutProductId' | 'shopItem'
> & {
  shopItem: IShopItemDto<TDate> | null;
};

export type IShopItemDto<TDate = number> = Omit<IShopItem<TDate>, 'productId' | 'rewards'> & {
  rewards: IShopItemRewardDto<TDate>[] | null;
};

export type IShopItemRewardDto<TDate = number> = Omit<
  IShopItemReward<ShopItemRewardType, TDate>,
  'context'
>;

// request

export type IGetShopPaymentDto = { paymentId: string };

export type IFindShopItemsDto = IPaginatedQuery;

export type ICreateShopPaymentDto = {
  shopItemId: string;
};

export type IFindShopPaymentsDto = IPaginatedQuery;

// response

export type IGetShopPaymentResponse = IShopPaymentDto;

export type IFindShopItemsResponse = IPaginatedResult<IShopItemDto>;

export type ICreateShopPaymentResponse = IShopPaymentDto;

export type IFindShopPaymentsResponse = IPaginatedResult<IShopPaymentDto>;
