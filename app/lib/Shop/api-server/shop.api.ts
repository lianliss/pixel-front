'use strict';

import { AxiosResponse } from 'axios';
import { axiosInstance } from 'utils/libs/axios.ts';
import {
  ICreateShopPaymentDto,
  ICreateShopPaymentResponse,
  IFindShopItemsDto,
  IFindShopItemsResponse,
  IFindShopPaymentsDto,
  IFindShopPaymentsResponse,
} from 'lib/Shop/api-server/shop.types.ts';

export class ShopApi {
  url: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async findItems(params: IFindShopItemsDto): Promise<IFindShopItemsResponse> {
    const response = await axiosInstance.get<{ data: IFindShopItemsResponse }>(
      `/api/v1/shop/items`,
      {
        baseURL: this.url,
        params,
      }
    );

    return response.data.data;
  }

  async findPayments(params: IFindShopPaymentsDto): Promise<IFindShopPaymentsResponse> {
    const response = await axiosInstance.get<{ data: IFindShopPaymentsResponse }>(
      `/api/v1/shop/payment/find`,
      {
        baseURL: this.url,
        params,
      }
    );

    return response.data.data;
  }

  async createPayment(payload: ICreateShopPaymentDto): Promise<ICreateShopPaymentResponse> {
    const { data } = await axiosInstance.post<
      { data: ICreateShopPaymentResponse },
      AxiosResponse<{ data: ICreateShopPaymentResponse }>,
      ICreateShopPaymentDto
    >(`/api/v1/shop/payment/create`, payload, {
      baseURL: this.url,
    });

    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const shopApi = new ShopApi({ url: 'https://api.hellopixel.network' });
