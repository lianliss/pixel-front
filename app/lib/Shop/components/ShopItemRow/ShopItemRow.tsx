import React, { useMemo, useState } from 'react';
import styles from './ShopItemRow.module.scss';
import { useAccount } from '@chain/hooks/useAccount.ts';
import Paper, { PaperProps } from '@mui/material/Paper';
import { Button } from '@ui-kit/Button/Button.tsx';
import clsx from 'clsx';
import { useGasBalance } from '@chain/hooks/useGasBalance.ts';
import { useTranslation } from 'react-i18next';
import { Timer } from '@ui-kit/Timer/Timer.tsx';
import { IChestSaleBox } from 'lib/Chests/utils/constants.ts';
import moment from 'moment';
import Skeleton from '@mui/material/Skeleton';
import { IChestSale } from 'lib/Chests/hooks/sale/useChestSales.ts';
import { IShopItemDto } from 'lib/Shop/api-server/shop.types.ts';
import { Img } from '@ui-kit/Img/Img.tsx';
import { styled } from '@mui/material/styles';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import wei from 'utils/wei';
import Box from '@mui/material/Box';
import BuyStarsIcon from '../../../../shared/icons/BuyStars.tsx';
import IconTgStar from '@ui-kit/icon/common/IconTgStar.tsx';

const defaultImage = 'https://storage.hellopixel.network/assets/chests/pixel.png';

const StyledRoot = styled(Paper)(() => ({
  padding: '6px 8px',
}));

type Props = PaperProps & {
  shopItem: IShopItemDto;
  //
  onBuy?: (box: IShopItemDto) => void;
  disabled?: boolean;
  loading?: boolean;
};

function ShopItemRow(props: Props) {
  const { shopItem, onBuy, disabled, loading, ...other } = props;

  const { t } = useTranslation('shop');

  return (
    <StyledRoot
      elevation={1}
      variant='outlined'
      className={clsx(styles.token, { disabled: disabled })}
      style={{ opacity: shopItem.disabled ? 0.6 : 1 }}
      {...other}
    >
      <div className={styles.tokenInfo}>
        <div className={styles.tokenInfoIcon}>
          <Img src={shopItem.image || defaultImage} className={styles.tokenInfoIconBox} alt={''} />
        </div>
        <Box sx={{ ml: 2 }}>
          <Typography fontWeight='bold'>{shopItem.title || '-'}</Typography>
          <Typography
            variant='caption'
            fontWeight='bold'
            sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}
          >
            Price: {shopItem.price}{' '}
            {shopItem.currency === 'STARS' ? (
              <IconTgStar sx={{ fontSize: 12 }} />
            ) : (
              shopItem.currency
            )}
          </Typography>
        </Box>
      </div>

      <div className={styles.tokenBalance}>
        <div>
          <Button
            variant='contained'
            color='primary'
            loading={loading}
            disabled={disabled}
            onClick={() => {
              onBuy?.(shopItem);
            }}
          >
            {t('Buy')}
          </Button>
        </div>
      </div>
    </StyledRoot>
  );
}

export default ShopItemRow;
