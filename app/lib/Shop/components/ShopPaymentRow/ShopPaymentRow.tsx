import React, { useMemo, useState } from 'react';
import styles from './ShopPaymentRow.module.scss';
import Paper, { PaperProps } from '@mui/material/Paper';
import { Button } from '@ui-kit/Button/Button.tsx';
import clsx from 'clsx';
import { useTranslation } from 'react-i18next';
import { IShopPaymentDto, ShopPaymentStatus } from 'lib/Shop/api-server/shop.types.ts';
import { Img } from '@ui-kit/Img/Img.tsx';
import { styled } from '@mui/material/styles';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { getShopPaymentStatusColor } from 'lib/Shop/utils/getShopPaymentStatusColor.ts';
import Box from '@mui/material/Box';

const defaultImage = 'https://storage.hellopixel.network/assets/chests/pixel.png';

const StyledRoot = styled(Paper)(() => ({
  padding: '6px 8px',
}));

type Props = PaperProps & {
  shopPayment: IShopPaymentDto;
  //
  onOpen?: (box: IShopPaymentDto) => void;
  disabled?: boolean;
  loading?: boolean;
};

function ShopPaymentRow(props: Props) {
  const { shopPayment, onOpen, disabled, loading, ...other } = props;

  const shopItem = shopPayment.shopItem;

  const { t } = useTranslation('shop');

  return (
    <StyledRoot
      elevation={1}
      variant='outlined'
      className={clsx(styles.token, { disabled: disabled })}
      {...other}
    >
      <div className={styles.tokenInfo}>
        <div className={styles.tokenInfoIcon}>
          <Img src={shopItem?.image || defaultImage} className={styles.tokenInfoIconBox} alt={''} />
        </div>
        <Box sx={{ ml: 2 }}>
          <Typography fontWeight='bold'>{shopItem?.title || '-'}</Typography>
          <Typography
            variant='caption'
            fontWeight='bold'
            color={getShopPaymentStatusColor(shopPayment)}
          >
            Status: {shopPayment.checkoutStatus}
          </Typography>
        </Box>
      </div>
      {shopPayment.checkoutStatus === ShopPaymentStatus.Open && (
        <div>
          <Button
            variant='contained'
            color='primary'
            loading={loading}
            disabled={disabled}
            onClick={() => {
              onOpen?.(shopPayment);
            }}
          >
            {t('Open')}
          </Button>
        </div>
      )}
    </StyledRoot>
  );
}

export default ShopPaymentRow;
