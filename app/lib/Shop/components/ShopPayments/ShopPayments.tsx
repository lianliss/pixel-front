import { Button } from '@ui-kit/Button/Button.tsx';
import { IShopPaymentDto, ShopPaymentStatus } from 'lib/Shop/api-server/shop.types.ts';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { useDialog } from 'utils/hooks/useDialog.ts';
import ShopPaymentRow from 'lib/Shop/components/ShopPaymentRow/ShopPaymentRow.tsx';
import Stack from '@mui/material/Stack';

type Props = { payments?: IShopPaymentDto[]; onOpen?: (payment: IShopPaymentDto) => void };

const defaultPayments: IShopPaymentDto[] = [];

export function ShopPayments(props: Props) {
  const { payments = defaultPayments, onOpen } = props;

  const dialog = useDialog();

  const pending = payments.filter((el) => el.checkoutStatus === ShopPaymentStatus.Open).length;

  return (
    <>
      <Button
        variant='contained'
        size='large'
        color='primary'
        fullWidth
        onClick={dialog.show}
        endIcon={pending ? `(pending: ${pending})` : undefined}
      >
        Payments
      </Button>

      <BottomDialog open={dialog.open} onClose={dialog.onClose} title='Payments'>
        <Stack direction='column' gap={1} sx={{ mt: 2 }}>
          {payments.map((row) => (
            <ShopPaymentRow key={`shop-payment-${row.id}`} shopPayment={row} onOpen={onOpen} />
          ))}
        </Stack>
      </BottomDialog>
    </>
  );
}
