import React from 'react';
import styles from './ShopBoughtDialog.module.scss';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useTranslation } from 'react-i18next';
import { IUserChestDto } from 'lib/Chests/api-server/chest.types.ts';
import { Img } from '@ui-kit/Img/Img.tsx';
import BuyGasAlertLazy from 'lib/TelegramStars/components/BuyGasAlert/BuyGasAlert.lazy.tsx';
import { IWaitConfirmationsResult } from '@chain/hooks/useTransactionConfirmation.ts';
import Alert from '@mui/material/Alert';
import { IShopItemDto } from 'lib/Shop/api-server/shop.types.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useNavigate } from 'react-router-dom';

const defaultImage = require('assets/img/chest.png') as string;

type Props = {
  open?: boolean;
  onClose?: () => void;
  shopItem: IShopItemDto;
};

function ShopBoughtDialog(props: Props) {
  const { open, onClose, shopItem } = props;

  const { t } = useTranslation('shop');

  const image = shopItem?.image;

  const navigate = useNavigate();
  const redirectToInventory = () => {
    navigate('/inventory');
  };

  return (
    <BottomDialog open={open} onClose={onClose} title={t('Congratulations')}>
      <Typography color='text.primary' variant='subtitle2' align='center' sx={{ mt: 1 }}>
        {t('You bought an Pack')}
      </Typography>
      <Typography color='text.primary' variant='subtitle2' align='center'>
        {t('You can find it in Inventory')}
      </Typography>

      <div style={{ marginTop: 16, display: 'flex', justifyContent: 'center' }}>
        <Img
          sx={{ borderRadius: '12px', maxWidth: 250 }}
          src={image || defaultImage}
          onError={(e) => {
            (e.target as HTMLImageElement).src = defaultImage;
          }}
        />
      </div>

      <Button
        size='large'
        variant='contained'
        style={{ marginTop: 10 }}
        onClick={redirectToInventory}
        fullWidth
      >
        {t('Open inventory')}
      </Button>
    </BottomDialog>
  );
}

export default React.memo(ShopBoughtDialog);
