import ShopItemRow from 'lib/Shop/components/ShopItemRow/ShopItemRow.tsx';
import Box, { BoxProps } from '@mui/material/Box';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import { useShopItems } from 'lib/Shop/hooks/useShopItems.ts';
import { IShopItemDto, IShopPaymentDto, ShopProvider } from 'lib/Shop/api-server/shop.types.ts';
import toaster from 'services/toaster.tsx';
import { useShopPayment } from 'lib/Shop/hooks/useShopPayment.ts';
import { useShopPayments } from 'lib/Shop/hooks/useShopPayments.ts';
import Stack from '@mui/material/Stack';
import ShopPaymentRow from 'lib/Shop/components/ShopPaymentRow/ShopPaymentRow.tsx';
import { openLink } from 'utils/open-link.ts';
import { useActiveTab } from '../../../../shared/hooks/useActiveTab.ts';
import {useEffect, useState} from 'react';
import { ShopItemDialog } from 'lib/Shop/components/ShopItemDialog/ShopItemDialog.tsx';
import { IS_TELEGRAM } from '@cfg/config.ts';
import { isTester } from '@cfg/testers.ts';
import { useInterval } from 'utils/hooks/useInterval';
import { ShopPayments } from 'lib/Shop/components/ShopPayments/ShopPayments.tsx';
import ShopBoughtDialog from 'lib/Shop/components/ShopBoughtDialog/ShopBoughtDialog.tsx';
import {useParams, useSearchParams} from "react-router-dom";

const StyledRoot = styled(Box)(() => ({}));

type Props = BoxProps;

export function ShopStarters(props: Props) {
  const [searchParams] = useSearchParams();

  const isInitialOpen = searchParams.get('shop') === '1';

  const shopItems = useShopItems();
  const shopPayment = useShopPayment();
  const shopPayments = useShopPayments();

  const shopItemsData = (shopItems.data?.rows || [])
  .filter((el) =>
    IS_TELEGRAM ? el.provider === ShopProvider.Telegram : el.provider === ShopProvider.Crypto
  );

  const [shopItem, setShopItem] = useState<IShopItemDto | null>(null);
  const [boughtItem, setBoughtItem] = useState<IShopItemDto | null>(null);

  const handleBuy = async (shopItem: IShopItemDto) => {
    try {
      const payment = await shopPayment.create(shopItem);
      await shopPayments.refetch();

      if (payment.checkoutProvider === ShopProvider.Telegram) {
        await new Promise(async (resolve, reject) => {
          Telegram.WebApp.openInvoice(payment.checkoutUrl, (status) => {
            if (status === 'failed') {
              setShopItem(null);
              reject(new Error('failed'));
            }
            if (status === 'cancelled') {
              setShopItem(null);
              resolve();
            }
            if (status === 'paid') {
              setBoughtItem(shopItem);
              setShopItem(null);
            }
          });
        });
      } else {
        openLink(payment.checkoutUrl);
        setShopItem(null);
      }
    } catch (e) {
      toaster.captureException(e);
    }
  };
  const handleOpen = async (shopPayment: IShopPaymentDto) => {
    openLink(shopPayment.checkoutUrl);
  };

  useInterval(() => shopPayments.refetch(), shopPayments.data?.count > 0 ? 10_000 : 60_000);
  useActiveTab(() => shopPayments.refetch(), []);

  useEffect(() => {
    if (isInitialOpen) {
      setShopItem(shopItemsData[0] || null);
    }
  }, [shopItems.data, isInitialOpen]);

  if (!shopItems.data?.count && !shopPayments.data?.count) {
    return null;
  }

  return (
    <StyledRoot {...props}>
      <Typography align='center' fontWeight='bold' sx={{ mb: 1 }}>
        Shop
      </Typography>

      <Paper elevation={1} variant='outlined' sx={{ p: 1 }}>
        {shopItemsData.length > 0 && (
          <Stack direction='column' gap={1} sx={{ mb: shopPayments.data?.rows.length ? 2 : 0 }}>
            {shopItemsData.map((row) => (
              <ShopItemRow
                key={`shop-item-${row.id}`}
                shopItem={row}
                onBuy={(v) => setShopItem(v)}
                loading={shopPayment.createLoading}
              />
            ))}
          </Stack>
        )}

        {shopPayments.data?.rows.length > 0 && (
          <>
            <ShopPayments onOpen={handleOpen} payments={shopPayments.data?.rows} />

            {/*<Stack direction='column' gap={1} sx={{}}>*/}
            {/*  {shopPayments.data?.rows.map((row) => (*/}
            {/*    <ShopPaymentRow*/}
            {/*      key={`shop-payment-${row.id}`}*/}
            {/*      shopPayment={row}*/}
            {/*      onOpen={handleOpen}*/}
            {/*    />*/}
            {/*  ))}*/}
            {/*</Stack>*/}
          </>
        )}
      </Paper>

      <ShopItemDialog
        open={!!shopItem}
        shopItem={shopItem}
        onClose={() => setShopItem(null)}
        onBuy={handleBuy}
        loading={shopPayment.createLoading}
      />
      <ShopBoughtDialog
        open={!!boughtItem}
        shopItem={boughtItem}
        onClose={() => setBoughtItem(null)}
      />
    </StyledRoot>
  );
}
