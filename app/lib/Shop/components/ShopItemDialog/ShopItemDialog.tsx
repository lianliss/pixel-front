import BottomDialog, { IBottomDialogProps } from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { IShopItemDto, IShopItemRewardDto } from 'lib/Shop/api-server/shop.types.ts';
import { Button } from '@ui-kit/Button/Button.tsx';
import React from 'react';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import { Img } from '@ui-kit/Img/Img.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Stack from '@mui/material/Stack';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import Paper from '@mui/material/Paper';
import wei from 'utils/wei';
import { styled } from '@mui/material/styles';
import IconTgStar from '@ui-kit/icon/common/IconTgStar.tsx';

const getDiscountValue = (value: number) => Math.ceil(value / 10) * 10 * 4;

const StyledRewards = styled(Paper)(({ theme }) => ({
  border: 'solid 1px',
  borderColor: theme.palette.primary.main,
  marginTop: 16,
  padding: 1,
  display: 'flex',
  justifyContent: 'center',
}));

function Reward({ reward }: { reward: IShopItemRewardDto }) {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        gap: 1,
        px: '6px',
        py: 2,
      }}
    >
      <Avatar variant='rounded' src={reward.image} sx={{ width: 56, height: 56 }} />
      <div>
        <Typography variant='subtitle2' color='text.primary' fontWeight='bold' align='center'>
          {reward.title}
        </Typography>

        {reward.quantity > 1 && (
          <Typography color='text.primary' sx={{ opacity: 0.5 }} component='p' align='center'>
            {reward.quantity} pcs
          </Typography>
        )}
      </div>
    </Box>
  );
}

type Props = Omit<IBottomDialogProps, 'children'> & {
  shopItem?: IShopItemDto;
  onBuy?: (shopItem: IShopItemDto) => void;
  loading?: boolean;
};

export function ShopItemDialog(props: Props) {
  const { shopItem, onBuy, loading = false, ...other } = props;

  if (!shopItem) {
    return null;
  }

  return (
    <BottomDialog {...other}>
      <Box sx={{ display: 'flex', gap: 2 }}>
        <Img sx={{ width: 180, height: 180, borderRadius: '8px' }} src={shopItem.image} />
        <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
          <Typography variant='h3' fontWeight='bold' sx={{ fontSize: 23, lineHeight: '22px' }}>
            {shopItem.title}
          </Typography>
          <Typography
            variant='subtitle1'
            fontWeight='bold'
            sx={{ mt: 0.5, display: 'flex', alignItems: 'center', gap: '6px' }}
          >
            <span style={{ opacity: 0.5 }}>{shopItem.price}</span>{' '}
            {shopItem.currency === 'STARS' ? (
              <IconTgStar sx={{ fontSize: 14 }} />
            ) : (
              shopItem.currency
            )}
          </Typography>
          <Typography variant='subtitle2' fontWeight='bold' sx={{ mt: 1.5 }}>
            {shopItem.summary}
          </Typography>
        </Box>
      </Box>
      <StyledRewards variant='outlined' elevation={0}>
        <Stack
          direction='row'
          spacing={1}
          divider={
            <Divider
              flexItem
              orientation='vertical'
              sx={{ height: '100px', alignSelf: 'center' }}
            />
          }
          sx={{ width: '100%', display: 'flex', justifyContent: 'center' }}
        >
          {shopItem.rewards?.map((reward) => (
            <Reward reward={reward} key={`reward-${reward.id}`} />
          ))}
        </Stack>
      </StyledRewards>

      <Typography
        variant='caption'
        component='p'
        color='text.secondary'
        align='center'
        sx={{ mt: 2 }}
      >
        All items from the pack cannot be sold!
      </Typography>

        <Typography align='center' component='p' fontWeight='medium' color='error' sx={{ mt: 1, textDecoration: 'line-through' }}>
            {getDiscountValue(shopItem.price)} {shopItem.currency === 'STARS' ?  <IconTgStar sx={{ fontSize: 16, ml: 1, mb: '-2px' }} /> : shopItem.currency}
        </Typography>

      <Box sx={{ mt: 1 }}>
        <Button
          variant='contained'
          color='primary'
          fullWidth
          onClick={() => onBuy?.(shopItem)}
          disabled={!shopItem}
          loading={loading}
          size='large'
        >
          Buy for {shopItem.price}{' '}
          {shopItem.currency === 'STARS' ? (
            <IconTgStar sx={{ fontSize: 16, ml: 1 }} />
          ) : (
            shopItem.currency
          )}
        </Button>
      </Box>
    </BottomDialog>
  );
}
