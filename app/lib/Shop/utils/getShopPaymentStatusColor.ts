import { IShopPaymentDto, ShopPaymentStatus } from 'lib/Shop/api-server/shop.types.ts';

export function getShopPaymentStatusColor(payment: IShopPaymentDto): string {
  switch (payment.checkoutStatus) {
    case ShopPaymentStatus.Open:
      return 'warning.main';
    case ShopPaymentStatus.Cancelled:
      return 'error.main';
    case ShopPaymentStatus.Complete:
      return 'success.main';
    default:
      return 'text.primary';
  }
}
