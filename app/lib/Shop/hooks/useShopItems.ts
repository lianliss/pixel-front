import { useUnit } from 'effector-react';
import { useEffect } from 'react';
import toaster from 'services/toaster.tsx';
import { loadShopItems, shopItemsStore } from 'lib/Shop/api-server/shop.store.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { IFindShopItemsResponse } from 'lib/Shop/api-server/shop.types.ts';
import { IStoreState } from 'utils/store/createStoreState.ts';

export function useShopItems() {
  const account = useAccount();
  const state = useUnit(shopItemsStore);

  useEffect(() => {
    if (account.isConnected) {
      void loadShopItems({ limit: 10, offset: 0 }).catch((e) => toaster.captureException(e));
    }
  }, [account.isConnected]);

  return {
    ...state,
    data: state.data as IFindShopItemsResponse | null,
  };
}
