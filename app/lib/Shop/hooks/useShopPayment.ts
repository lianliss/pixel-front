import { useUnit } from 'effector-react';
import { useEffect } from 'react';
import toaster from 'services/toaster.tsx';
import {
  createShopPayment,
  loadShopItems,
  shopItemsStore,
  shopPaymentStore,
} from 'lib/Shop/api-server/shop.store.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { IShopItemDto } from 'lib/Shop/api-server/shop.types.ts';

export function useShopPayment() {
  const state = useUnit(shopPaymentStore);
  const createPaymentPending = useUnit(createShopPayment.pending);

  const create = (shopItem: IShopItemDto) => createShopPayment({ shopItemId: shopItem.id });

  return {
    ...state,
    create,
    createLoading: createPaymentPending,
  };
}
