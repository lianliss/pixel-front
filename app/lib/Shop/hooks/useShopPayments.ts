import { useUnit } from 'effector-react';
import { useCallback, useEffect } from 'react';
import toaster from 'services/toaster.tsx';
import {
  loadShopItems,
  loadShopPayments,
  shopItemsStore,
  shopPaymentsStore,
  shopPaymentStore,
} from 'lib/Shop/api-server/shop.store.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import {
  IFindShopItemsResponse,
  IFindShopPaymentsResponse,
} from 'lib/Shop/api-server/shop.types.ts';

export function useShopPayments() {
  const account = useAccount();
  const state = useUnit(shopPaymentsStore);

  const refetch = useCallback(() => {
    if (account.isConnected) {
      void loadShopPayments({ limit: 10, offset: 0 }).catch((e) => toaster.captureException(e));
    }
  }, [account.isConnected]);

  useEffect(() => {
    refetch();
  }, [refetch]);

  return {
    ...state,
    data: state.data as IFindShopPaymentsResponse | null,
    refetch,
  };
}
