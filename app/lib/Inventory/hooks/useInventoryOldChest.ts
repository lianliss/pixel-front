import React from 'react';
import wait from 'utils/wait';
import toaster from 'services/toaster.tsx';
import { useFreeChestApi } from 'lib/Chests/hooks/deprecated/useOldUserChests.ts';
import { useFreeChestDropFromTx } from 'lib/Chests/hooks/drop/useFreeChestDropFromTx.ts';
import { IUserChestDto } from 'lib/Chests/api-server/chest.types.ts';

/**
 * @deprecated
 */
export function useInventoryOldChests({ onClaimed }: { onClaimed?: () => void } = {}) {
  const [selected, setSelected] = React.useState<IUserChestDto | null>(null);
  const [loading, setLoading] = React.useState(false);

  const freeChestsApi = useFreeChestApi();
  const droppedFreeChest = useFreeChestDropFromTx();

  const handleClaimFreeChest = async () => {
    if (!selected || !selected.chest) {
      return;
    }

    setLoading(true);

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      const receipt = await freeChestsApi.claim(selected.chest.chestId);
      await droppedFreeChest.load(receipt);

      Telegram.WebApp.HapticFeedback.impactOccurred('light');

      setSelected(null);
      await freeChestsApi.reload();

      await wait(5000);

      onClaimed?.();
      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      toaster.logError(e, '[claimFreeChest]');
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }

    setLoading(false);
  };

  return {
    chests: freeChestsApi.chests,
    droppedNft: droppedFreeChest.nft,
    droppedLoading: droppedFreeChest.loading,
    clearDropped: () => droppedFreeChest.clear(),
    //
    selected: selected,
    select: (c: IUserChestDto) => setSelected(c),
    clearSelected: () => setSelected(null),
    //
    loading: freeChestsApi.loading,
    actionLoading: freeChestsApi.loading || loading || droppedFreeChest.loading,
    handleClaim: handleClaimFreeChest,
  };
}
