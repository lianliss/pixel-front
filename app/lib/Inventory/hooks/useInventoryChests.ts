import React from 'react';
import { useUserChests } from 'lib/Chests/hooks/useUserChests.ts';
import useUserChestApi from 'lib/Chests/hooks/useUserChestApi.ts';
import wait from 'utils/wait';
import toaster from 'services/toaster.tsx';
import { useDropChestV2 } from 'lib/Chests/hooks/drop/useDropChestV2.ts';
import { waitForTransactionReceipt } from '@wagmi/core';
import { useConfig } from 'wagmi';
import { IUserChestDto } from 'lib/Chests/api-server/chest.types.ts';
import { useTransactionConfirmation } from '@chain/hooks/useTransactionConfirmation.ts';

export function useInventoryChests({ onClaimed }: { onClaimed?: () => void } = {}) {
  const [selectedUserChest, setSelectedUserChest] = React.useState<IUserChestDto | null>(null);
  const [loading, setLoading] = React.useState(false);

  const config = useConfig();
  const userChests = useUserChests();
  const userChestsApi = useUserChestApi();
  const drop = useDropChestV2();

  const confirmation = useTransactionConfirmation({
    txHash: userChestsApi.write.data,
    cacheKey: `chest`,
  });

  const handleClaimUserChest = async () => {
    if (!selectedUserChest || !selectedUserChest.chest) {
      return;
    }

    setLoading(true);

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      const txHash = await userChestsApi.claim(selectedUserChest.chest.chestId);
      const receipt = await waitForTransactionReceipt(config, { hash: txHash, confirmations: 1 });

      await wait(3000);

      await drop.load(receipt);
      setSelectedUserChest(null);

      Telegram.WebApp.HapticFeedback.impactOccurred('light');

      await userChests.refetch();
      onClaimed?.();
      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      console.error('[claimFreeChest]', e);
      toaster.logError(e, '[claimFreeChest]');
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }

    setLoading(false);
  };

  return {
    confirmation,
    userChests: userChests.data,
    // drop
    droppedNft: drop.nft,
    droppedToken: drop.token,
    droppedLoading: drop.loading,
    clearDropped: () => drop.clear(),
    //
    selected: selectedUserChest,
    select: (c: IUserChestDto) => setSelectedUserChest(c),
    clearSelected: () => setSelectedUserChest(null),
    //
    loading: userChests.loading,
    actionLoading: userChestsApi.write.loading || loading,
    handleClaim: handleClaimUserChest,
  };
}
