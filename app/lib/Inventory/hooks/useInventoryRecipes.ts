import { useEffect } from 'react';
import { loadUserNftRecipes } from 'lib/Forge/api-server/miner-claimer.store.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import { useUserMinerClaimerRecipes } from 'lib/Forge/hooks/useUserMinerClaimerRecipes.ts';

export function useInventoryRecipes() {
  const account = useAccount();
  const nftAddress = NFT_CONTRACT_ID[account.chainId];

  const recipes = useUserMinerClaimerRecipes({ skip: !account.isConnected });

  useEffect(() => {
    if (account.isConnected && nftAddress) {
      loadUserNftRecipes({
        userAddress: account.accountAddress,
        contractId: nftAddress.toLowerCase(),
      }).then();
    }
  }, [account.accountAddress, account.isConnected, nftAddress]);

  return {
    data: recipes.state,
    loading: recipes.loading,
    isFetched: recipes.isFetched,
  };
}
