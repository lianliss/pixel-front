import { useChainUserSlots } from 'lib/Inventory/hooks/useChainUserSlots.ts';

export function useChainUserSlotsDrone({ telegramId }: { telegramId?: number }) {
  const userSlots = useChainUserSlots({ telegramId });
  const droneSlot = userSlots.data?.find((el) => el.slotId === 13);

  return {
    ...userSlots,
    data: droneSlot,
  };
}
