import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { wei } from 'utils/index';
import { useState } from 'react';
import SLOTS_ABI from 'lib/Inventory/api-contract/abi/SLOTS.abi.ts';

const ERC_20_ABI = require('const/ABI/Erc20Token');

export function useUnlockSlot({
  slotsAddress,
  token2Address,
  token2Price,
  accountAddress,
}: {
  slotsAddress: string;
  token2Address?: string;
  token2Price?: number;
  accountAddress: string;
}) {
  const isCoin2 = !token2Address || token2Address === '0x0000000000000000000000000000000000000000';
  const [loading, setLoading] = useState(false);

  const allowance2State = useReadContract<bigint>({
    initData: 0 as bigint,
    abi: ERC_20_ABI,
    address: token2Address,
    functionName: 'allowance',
    args: [slotsAddress, accountAddress],
    skip: !accountAddress || isCoin2,
  });

  const allowance2Update = useWriteContract({
    abi: ERC_20_ABI,
    address: token2Address,
  });
  const unlockSlot = useWriteContract({
    abi: SLOTS_ABI,
    address: slotsAddress,
  });

  const onBuy = async (slotId: number) => {
    setLoading(true);

    try {
      await unlockSlot.writeContractAsync({
        functionName: 'unlockSlot',
        args: [slotId],
      });
    } catch (e) {
      throw e;
    } finally {
      setLoading(false);
    }
  };

  const onBuy2 = async (slotId: number) => {
    setLoading(true);

    try {
      const weiPrice = wei.to(token2Price) as bigint;

      if (!isCoin2) {
        if (wei.from(allowance2State.data) < token2Price) {
          console.log(`[unlockSlot] have allowance ${allowance2State.data} required ${weiPrice}`);

          await allowance2Update.writeContractAsync({
            functionName: 'approve',
            args: [slotsAddress, weiPrice],
          });
        } else {
          console.log(
            `[unlockSlot] already have allowance ${allowance2State.data} required ${weiPrice}`
          );
        }
      }

      await unlockSlot.writeContractAsync({
        functionName: 'unlockSlotSGB',
        args: [slotId],
        overrides: {
          value: isCoin2 ? weiPrice : undefined,
        },
      });
    } catch (e) {
      throw e;
    } finally {
      setLoading(false);
    }
  };

  return {
    buy2Enabled: Boolean(token2Price),
    loading,
    onBuy,
    onBuy2,
  };
}
