import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { useState } from 'react';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { PAYABLE_CONTRACT_ID } from 'lib/Wallet/api-contract/wallet.contracts.ts';
import { PAYABLE_ABI } from 'lib/Mining/api-contract/abi/payable.abi.ts';
import { IS_DEVELOP } from '@cfg/config.ts';
import { IS_PAYABLE } from '@cfg/app.ts';
import ERC_20_ABI from 'lib/Wallet/api-contract/abi/erc-20.abi.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { useTransactionConfirmation } from '@chain/hooks/useTransactionConfirmation.ts';

const SLOTS_ABI = require('const/ABI/V5/slots');

export function useSlotsApi({
  slotsAddress,
  payablePrice,
  onEquip,
  onUnEquip,
}: {
  slotsAddress: string;
  payablePrice?: bigint;
  onEquip?: () => Promise<void>;
  onUnEquip?: () => Promise<void>;
}) {
  const account = useAccount();
  const tradeToken = useTradeToken();

  const payableAddress = PAYABLE_CONTRACT_ID[account.chainId];

  const write = useWriteContract({
    abi: SLOTS_ABI,
    address: slotsAddress,
  });
  const writePaylable = useWriteContract({
    abi: PAYABLE_ABI,
    address: payableAddress,
  });
  const writeTradeToken = useWriteContract({
    abi: ERC_20_ABI,
    address: tradeToken.data?.address,
  });

  const [loading, setLoading] = useState(false);

  const handleEquipItem = async (payload: { assetId: number; slot: number }) => {
    setLoading(true);

    try {
      if (payableAddress && IS_PAYABLE) {
        if (!tradeToken.data?.isCoin) {
          await writeTradeToken.writeContractAsync({
            functionName: 'approve',
            args: [payableAddress, payablePrice],
          });
        }

        await writePaylable.writeContractAsync({
          functionName: 'equip',
          args: [payload.assetId, payload.slot, 0],
          overrides: payablePrice && tradeToken.data?.isCoin ? { value: payablePrice } : undefined,
        });
      } else {
        await write.writeContractAsync({
          functionName: 'equip',
          args: [payload.assetId, payload.slot, 0],
        });
      }

      await onEquip?.();
    } catch (e) {
      throw e;
    } finally {
      setLoading(false);
    }
  };
  const handleUnEquipItem = async (payload: { slot: number }) => {
    setLoading(true);

    try {
      if (payableAddress && IS_PAYABLE) {
        if (!tradeToken.data?.isCoin) {
          await writeTradeToken.writeContractAsync({
            functionName: 'approve',
            args: [payableAddress, payablePrice],
          });
        }

        await writePaylable.writeContractAsync({
          functionName: 'unequip',
          args: [payload.slot],
          overrides: payablePrice && tradeToken.data?.isCoin ? { value: payablePrice } : undefined,
        });
      } else {
        await write.writeContractAsync({ functionName: 'unequip', args: [payload.slot] });
      }

      await onUnEquip?.();
    } catch (e) {
      throw e;
    } finally {
      setLoading(false);
    }
  };

  const confirmation = useTransactionConfirmation({
    txHash: writePaylable.data,
    cacheKey: `inventory-slot`,
  });

  return {
    loading,
    equipItem: handleEquipItem,
    unEquipItem: handleUnEquipItem,
    confirmation,
  };
}
