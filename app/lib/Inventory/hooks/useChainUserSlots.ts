import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { ISlotDto, IUserSlotDto } from 'lib/Inventory/context/slots/Slots.context.ts';
import SLOTS_ABI from 'lib/Inventory/api-contract/abi/SLOTS.abi.ts';
import { SLOTS_CONTRACT_ID } from 'lib/Inventory/api-contract/slots.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { createNftFromType } from 'lib/NFT/utils/create-nft-from-type.ts';
import { createNftTypeFromChain } from 'lib/NFT/utils/create-nft-type-from-chain.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';

export function useChainUserSlots({ telegramId }: { telegramId?: number }) {
  const account = useAccount();

  const { chainId } = account;
  const slotsAddress = SLOTS_CONTRACT_ID[chainId];
  const nftAddress = NFT_CONTRACT_ID[chainId];

  const userSlots = useReadContract<IUserSlotDto[]>({
    // initData: [[]],
    abi: SLOTS_ABI,
    address: slotsAddress,
    skip: !telegramId || !slotsAddress || !account.isConnected,
    args: [+telegramId],
    functionName: 'getUserSlots',
    select: (rawSlots) => {
      return rawSlots[0].map(
        (slot, slotId): IUserSlotDto => ({
          slotId,
          isEnabled: slot.isEnabled,
          variantId: Number(slot.variantId),
          tokenId: Number(slot.tokenId),
          itemType: Number(slot.tokenId)
            ? createNftFromType(createNftTypeFromChain(rawSlots[1][slotId]), {
                tokenId: slot.tokenId,
                contractId: nftAddress,
              })
            : null,
        })
      );
    },
  });

  return userSlots;
}
