const mapPocket = {
  0: 1,
  1: 2,
  3: 3,
  5: 4,
};

export default function getSlotName(slotId) {
  let title = `Pocket ${mapPocket[slotId] || ''}`;

  switch (slotId) {
    case 6:
      title = 'Artifact';
      break;
    case 2:
      title = 'Backpack';
      break;
    case 4:
      title = 'Belt';
      break;
    case 7:
      title = 'Drill';
      break;
    case 8:
      title = 'Back';
      break;
    case 9:
      title = 'Loyalty Card';
      break;
    case 10:
      title = 'Smart Claimer';
      break;
    default:
  }

  return title;
}
