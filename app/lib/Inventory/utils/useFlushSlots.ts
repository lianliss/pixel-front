import React from 'react';
import { useSlots } from 'lib/Inventory/context/slots/Slots.context.ts';
import { Chain } from 'services/multichain/chains.js';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import SLOTS_FIXER_ABI from 'lib/Inventory/api-contract/abi/SLOTS.FIXER.abi.ts';
import { SLOTS_FIXER_CONTRACT_ID } from 'lib/Inventory/api-contract/slots.constants.ts';

const marketItemsFixedKey = 'market-items-fixed';

const ITEMS_TO_FIX = {
  [Chain.SONGBIRD]: [
    481, // Drill
  ],
};

const COLLECTIONS_TO_FIX = {
  [Chain.SONGBIRD]: [
    5, // Marketplace
  ],
};

const FIX_STORAGE_MARK = '2024-10-15';

function useFlushSlots() {
  const { chainId } = useAccount();
  const slots = useSlots();
  const userSlots = slots?.userSlots || [];
  const [fixing, setFixing] = React.useState<boolean>(false);

  const fixerAddress = SLOTS_FIXER_CONTRACT_ID[chainId];
  const write = useWriteContract({
    abi: SLOTS_FIXER_ABI,
    address: fixerAddress,
  });

  const flush = React.useMemo(
    () => async () => {
      try {
        setFixing(true);
        await write.writeContractAsync({
          functionName: 'flushSlotsSelf',
          args: [],
        });
        localStorage.setItem(marketItemsFixedKey, FIX_STORAGE_MARK);
      } catch (error) {
        console.error('[useFlushSlots] flush', error);
      }
      setFixing(false);
    },
    [chainId]
  );

  React.useEffect(() => {
    if (fixing) return;
    if (localStorage.getItem(marketItemsFixedKey) === FIX_STORAGE_MARK) return;
    if (!userSlots.length) return;

    const items = ITEMS_TO_FIX[chainId] || [];
    const collections = COLLECTIONS_TO_FIX[chainId] || [];
    if (!items.length && !collections.length) return;

    const isFound = !!userSlots.find((slot) => {
      const { typeId, collectionId } = slot.itemType || {};
      return items.includes(typeId) || collections.includes(collectionId);
    });

    if (isFound) {
      flush().then();
    }
  }, [userSlots, chainId]);

  return {
    flush,
    fixing,
  };
}

export default useFlushSlots;
