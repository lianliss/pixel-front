import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

export const useInventoryChangeTabStageAction = (
  handleNextTutorialStep: (step: number) => void
) => {
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const activeTab = queryParams.get('tab');

  useEffect(() => {
    const handleTabChange = () => {
      if (activeTab === 'all') {
        handleNextTutorialStep(2);
      }
      if (activeTab === 'recipes') {
        handleNextTutorialStep(3);
      }
      if (activeTab === 'boxes') {
        handleNextTutorialStep(4);
      }
    };

    handleTabChange();
  }, [activeTab]);
};
