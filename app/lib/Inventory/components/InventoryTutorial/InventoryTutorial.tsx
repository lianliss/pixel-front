import { Tutorial } from '../../../../widget/tutorial';
import { useTutorial } from '../../../../widget/tutorial/useTutorial.tsx';
import { Tutorials } from '../../../../widget/tutorial/config/config.tsx';
import React, { useEffect } from 'react';
import { useInventoryChangeTabStageAction } from 'lib/Inventory/components/InventoryTutorial/hooks/useInventoryChangeTabStageAction.tsx';
import { INVENTORY_TUTORIAL_CONFIG } from 'lib/Inventory/components/InventoryTutorial/config/config.tsx';
import { TUTORIAL_INVENTORY_ENABLED } from '@cfg/app.ts';

export function InventoryTutorial(props: { start?: boolean }) {
  const { startTutorial, checkRunTutorial, tutorial, completeTutorial } = useTutorial();
  const [stepTutorial, setStepTutorial] = React.useState(1);

  useInventoryChangeTabStageAction((v) => setStepTutorial(v));

  useEffect(() => {
    if (props.start && TUTORIAL_INVENTORY_ENABLED) {
      const dataTutorial = checkRunTutorial(Tutorials.inventoryBox);

      if (dataTutorial.isStart) {
        startTutorial(Tutorials.inventoryBox, dataTutorial.data);
      }
    }
  }, [props.start]);

  return (
    <Tutorial
      value={tutorial}
      step={stepTutorial}
      onChangeStep={(v) => setStepTutorial(v)}
      onComplete={completeTutorial}
      config={INVENTORY_TUTORIAL_CONFIG}
    />
  );
}
