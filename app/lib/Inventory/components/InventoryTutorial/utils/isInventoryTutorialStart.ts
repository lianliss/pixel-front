import { useGasBalance } from '@chain/hooks/useGasBalance.ts';
import { useInventoryChests } from 'lib/Inventory/hooks/useInventoryChests.ts';

export function isInventoryTutorialStart(
  gasBalance: ReturnType<typeof useGasBalance>,
  inventoryChests: ReturnType<typeof useInventoryChests>
): boolean {
  const sgbBalance = gasBalance?.data?.formatted;

  if (!sgbBalance) return false; // Early return if balance is not available

  const formattedBalance = parseFloat(sgbBalance.toFixed(2));

  if (formattedBalance < 0.2) return false;

  const hasChests = inventoryChests?.userChests?.rows.length > 0;

  if (hasChests) {
    return true;
  }

  return false;
}
