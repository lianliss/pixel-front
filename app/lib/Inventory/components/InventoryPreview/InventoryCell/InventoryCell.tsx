import React from 'react';
import styles from './InventoryCell.module.scss';
import Lock from 'assets/svg/Lock';
import get from 'lodash/get';
import clsx from 'clsx';
import EmptyPocketDialog from 'lib/Inventory/components/EmptyPocketDialog/EmptyPocketDialog.tsx';
import { Icon, Spinner } from '@blueprintjs/core';
import WalletPopup from 'ui/WalletPopup/WalletPopup';

import { RARITY_COLORS } from 'const/rarity';
import orderBy from 'lodash-es/orderBy';
import uriTransform from 'utils/uriTransform';
import { useSlots } from 'lib/Inventory/context/slots/Slots.context.ts';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'react-i18next';

const InventoryCell = (props) => {
  const {
    loadingSlots = false,
    // handlers
    onClick: onClickOverride,
    onEquip,
    onUnEquip,
    onReRequest,
    // data
    slotId,
    size,
    color,
    utils,
    icon,
    isLocked = true,
    slotDisabled = false,
    requiredEnabled = false,
    userSlot,
    slotType,
    data,
    userItems = [],
    noGas,
    onLockedClick,
    ...other
  } = props;

  const tokenId = get(userSlot, 'tokenId', undefined);

  const { t } = useTranslation('inventory');

  const [isEquip, setIsEquip] = React.useState(false);
  const item = data || (tokenId ? userSlot?.itemType : undefined);
  const itemType = item?.type;

  const isLoading = false;
  const tokenURI = itemType?.tokenURI;

  const onClick = () => {
    Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

    if (isLocked && requiredEnabled && !slotDisabled) {
      onLockedClick?.(slotType);
      // setIsEmptyDialog(true);
      return;
    } else if (!isLocked) {
      if (!tokenId) {
        if (userItems.length) {
          setIsEquip(true);
        }
      } else {
        onUnEquip?.(item, slotId);
      }
    }
  };

  const filteredItems = orderBy(userItems, ['type.rarity', 'type.name'], ['desc', 'asc']).filter(
    (userItem) => userItem.type.slots.map((el) => el.slot).includes(slotId)
  );

  const style = { ...(other?.style || {}) };
  if (itemType?.rarity) {
    style.borderColor = RARITY_COLORS[itemType.rarity];
  }

  return (
    <div
      className={clsx(
        styles.cell,
        size === 'large' ? styles.cell_large : '',
        color === 'blue' ? styles.cell_cellRobot : '',
        (requiredEnabled && isLocked) || (!isLocked && !tokenId && !onClickOverride)
          ? styles.cellEmpty
          : '',
        isLocked && !requiredEnabled ? styles.cellLocked : ''
      )}
      {...other}
      onClick={onClickOverride || onClick}
      style={style}
    >
      {isLocked ? (
        requiredEnabled ? (
          <Icon icon={'unlock'} />
        ) : loadingSlots || isLoading ? (
          <Spinner size={20} />
        ) : (
          <Lock />
        )
      ) : tokenURI ? (
        <img src={uriTransform(tokenURI)} alt='' />
      ) : (
        icon || '?'
      )}
      {isEquip && !item && (
        <WalletPopup
          onClose={() => {
            setIsEquip(false);
          }}
          noAnimate
        >
          <h2>{isLoading ? t('Equipping...') : t('Select Item')}</h2>
          {isLoading ? (
            <Spinner />
          ) : (
            <div className={styles.cellItems__wrapper}>
              {!filteredItems.length ? (
                <div className={styles.empty__items}>
                  <Typography fontWeight='bold'>{t('No Nft for this slot')}</Typography>
                </div>
              ) : (
                <div className={styles.elements__list}>
                  {filteredItems.map((item, index) => (
                    <div className={styles.item__wrapper}>
                      <NFTCard
                        nft={item}
                        key={index}
                        onClick={(e) => {
                          e.stopPropagation();
                          onEquip?.(item, slotId);
                        }}
                      />
                    </div>
                  ))}
                </div>
              )}
            </div>
          )}
        </WalletPopup>
      )}
    </div>
  );
};

export default InventoryCell;
