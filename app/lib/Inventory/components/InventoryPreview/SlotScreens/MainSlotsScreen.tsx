// @flow
import * as React from 'react';
import styles from './styles.module.scss';
import InventoryCell from 'lib/Inventory/components/InventoryPreview/InventoryCell/InventoryCell.tsx';
import { Icon } from '@blueprintjs/core';
import get from 'lodash/get';
import { getNftSlotIcon } from 'lib/NFT/utils/getNftSlotIcon.tsx';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { IS_DEVELOP, IS_STAGE } from '@cfg/config.ts';
import { MAIN_CELL_POSITIONS } from 'lib/Inventory/components/InventoryPreview/utils/render-slots.ts';
import { useTranslation } from 'react-i18next';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain } from 'services/multichain/chains';

const isDevelop = IS_STAGE || IS_DEVELOP;

type Props = {
  slots: any;
  userSlots: any;
  onOpenStats: any;
  onEquip: () => void;
  onUnEquip: () => void;
  onReRequest: () => void;
  loadingSlots: boolean;
  userItems?: INftItemDto[];
  onLockedClick?: (key: string) => void;
};
export const MainSlotsScreen = (props: Props) => {
  const {
    onOpenStats,
    slots = [],
    userSlots = [],
    onReRequest,
    onEquip,
    onUnEquip,
    loadingSlots,
    userItems = [],
    onLockedClick,
  } = props;
  const { t } = useTranslation('inventory');
  const account = useAccount();

  const positions = React.useMemo(() => {
    return MAIN_CELL_POSITIONS.map((pos) => {
      const slotType = slots[pos.slotId] ?? null;
      const userSlot = userSlots[pos.slotId];
      const isLocked = !userSlot || !userSlot.isEnabled;

      const requiredEnabled =
        slotType?.requireSlot === 0 ||
        get(userSlots, `[${slotType?.requireSlot}].isEnabled`, false);

      const slotDisabled = !userSlot;

      return {
        ...pos,
        locked: isLocked,
        disabled: slotDisabled,
        requiredEnabled,
        userSlot,
        slotType,
      };
    });
  }, [slots, userSlots]);

  return (
    <div className={styles.inventoryPreview__box}>
      <span className={styles.inventoryPreview__title}>{t('Inventory')}</span>

      {positions.map((pos, index) => {
        const slotId = pos.slotId;

        return (
          <InventoryCell
            key={`cell-${index}`}
            style={{ top: pos.top, left: pos.left, bottom: pos.bottom }}
            // data
            slotId={slotId === 13 ? (account.chainId === Chain.UNITS ? slotId : undefined) : slotId}
            size={pos.size}
            data={pos.data}
            color={pos.color}
            isLocked={pos.locked}
            requiredEnabled={pos.requiredEnabled}
            slotDisabled={pos.slotDisabled}
            userSlot={pos.userSlot}
            slotType={pos.slotType}
            icon={getNftSlotIcon(slotId)}
            // handlers
            onReRequest={onReRequest}
            onEquip={onEquip}
            onUnEquip={onUnEquip}
            onLockedClick={onLockedClick}
            // onClick={}
            // slots
            loadingSlots={loadingSlots}
            userItems={userItems}
          />
        );
      })}

      <InventoryCell
        style={{
          right: '16px',
          top: '16px',
          borderRadius: '100%',
        }}
        // data
        size='large'
        color='blue'
        icon={<Icon icon='vertical-bar-chart-asc' />}
        // handlers
        onClick={onOpenStats}
        isLocked={false}
      />
    </div>
  );
};
