import React, { useState } from 'react';
import styles from './styles.module.scss';
import background_monkey from 'assets/img/inventory/background_monkey.png';
import backgroundSkale from 'assets/img/inventory/background_skale.png';
import swissBg from 'assets/img/inventory-swiss.png';
import { Chain, SWISSTEST } from 'services/multichain/chains';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { MainSlotsScreen } from 'lib/Inventory/components/InventoryPreview/SlotScreens/MainSlotsScreen.tsx';
import { AdditionalSlotsScreen } from 'lib/Inventory/components/InventoryPreview/SlotScreens/AdditionalSlotsScreen.tsx';
import { IconArrowRight } from '@ui-kit/icon/common/IconArrowRight.tsx';
import { useSwipeable } from 'react-swipeable';
import { Button } from '@ui-kit/Button/Button.tsx';
import clsx from 'clsx';

const IMAGES = {
  [Chain.SONGBIRD]: background_monkey,
  [Chain.SWISSTEST]: swissBg,
  [Chain.SKALE_TEST]: backgroundSkale,
  [Chain.SKALE]: backgroundSkale,
};

const InventoryPreview = ({
  style = {},
  onReRequest,
  onLockedClick,
  onItemClick,
  onEmptyClick,
  onOpenStats,
  slots = [],
  userSlots = [],
  loadingSlots = false,
  onEquip,
  onUnEquip,
  additionalScreenEnabled = false,
  userItems = [],
  className,
  ...other
}) => {
  const account = useAccount();
  const { chainId } = account;
  const [additionalScreenVisible, setAdditionalScreenVisible] = useState(false);

  const handleAdditionalScreenClick = () => {
    setAdditionalScreenVisible((prev) => !prev);
  };

  const handlers = useSwipeable({
    onSwipedLeft: () => setAdditionalScreenVisible(true),
    onSwipedRight: () => setAdditionalScreenVisible(false),
    preventDefaultTouchmoveEvent: true,
    trackMouse: true,
  });

  const background = IMAGES[chainId] || IMAGES[Chain.SONGBIRD];

  return (
    <div {...handlers} {...other} className={clsx(styles.container, className)}>
      <div
        className={`${styles.background__scene} ${
          additionalScreenVisible ? styles.right : styles.left
        }`}
        style={{ ...style, backgroundImage: `url(${background})` }}
      />
      <div className={styles.main__scene}>
        {/* Main slots screen */}
        <div
          className={`${styles.slots__main} ${additionalScreenVisible ? styles.hide : ''}`}
          {...other}
        >
          <MainSlotsScreen
            slots={slots}
            onEquip={onEquip}
            loadingSlots={loadingSlots}
            onUnEquip={onUnEquip}
            onReRequest={onReRequest}
            userSlots={userSlots}
            onOpenStats={onOpenStats}
            userItems={userItems}
            onLockedClick={onLockedClick}
          />
        </div>
        {/* Additional slots screen */}
        {additionalScreenEnabled && (
          <div
            className={`${styles.slots__additional} ${additionalScreenVisible ? styles.show : ''}`}
            {...other}
          >
            <AdditionalSlotsScreen
              slots={slots}
              onEquip={onEquip}
              loadingSlots={loadingSlots}
              onUnEquip={onUnEquip}
              onReRequest={onReRequest}
              userSlots={userSlots}
              onOpenStats={onOpenStats}
              userItems={userItems}
              onLockedClick={onLockedClick}
            />
          </div>
        )}
      </div>

      {additionalScreenEnabled && (
        <Button
          sx={{ width: 56, height: 56, p: 1, minWidth: 56 }}
          className={`${styles.next__btn} ${additionalScreenVisible ? styles.rotate : ''}`}
          onClick={handleAdditionalScreenClick}
        >
          <IconArrowRight />
        </Button>
      )}
    </div>
  );
};

export default InventoryPreview;
