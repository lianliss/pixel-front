type ISlotPos = {
  left?: string;
  top?: string;
  bottom?: string;

  size?: 'large';
  color?: 'blue';
  slotId?: number;
  disabled?: boolean;
  locked?: boolean;
  slotDisabled?: boolean; // remove
  data?: any;
  key?: string; // dev helper
};

export const MAIN_CELL_POSITIONS: ISlotPos[] = [
  {
    left: '16px',
    bottom: '62px',
    slotId: 0,
  },
  {
    left: '88px',
    bottom: '62px',
    slotId: 1,
  },
  {
    left: '55%',
    top: '29%',
    slotId: 2,
  },
  {
    left: '160px',
    bottom: '62px',
    slotId: 3,
  },
  {
    left: '47%',
    top: '56%',
    slotId: 4,
  },
  {
    left: '232px',
    bottom: '62px',
    type: 'utils',
    slotId: 5,
  },
  {
    left: '14px',
    top: '10%',
    slotId: 6,
  },
  {
    left: '61%',
    top: '43%',
    slotId: 8,
  },
  {
    left: '112px',
    top: '31%',
  },

  { left: '16px', top: '60%', slotId: 7 },
  { left: '35px', top: '44%', slotId: 12 },
  { left: '110px', top: '54%' },
  { left: '52%', top: '60px' },

  { left: 'calc(100% - 70px)', top: '112px', size: 'large', color: 'blue', slotId: 13 },
  { left: 'calc(100% - 62px)', top: '190px', color: 'blue' },
  { left: 'calc(100% - 62px)', top: '255px', color: 'blue' },
];

export const ADDITIONAL_CELL_POSITIONS: ISlotPos[] = [
  { left: '16px', top: '112px', size: 'large', color: 'blue', slotId: undefined },
  { left: '16px', top: '190px', color: 'blue' },
  { left: '16px', top: '255px', color: 'blue' },

  {
    left: '15%',
    bottom: '62px',
    slotId: 9,
  },
  {
    left: '43%',
    top: '56%',
    slotId: 50,
  },
  {
    left: '50%',
    bottom: '62px',
    type: 'utils',
    slotId: 50,
  },
  // {
  //   left: '14px',
  //   top: '10%',
  //   slotId: 9,
  // },
  {
    left: '30%',
    top: '43%',
    slotId: 50,
  },
  {
    left: '50%',
    top: '29%',
  },

  { left: 'calc(100% - 70px)', bottom: '36%', slotId: 50 },
  { left: 'calc(100% - 70px)', top: '28%', slotId: 50 },
];
