import React from 'react';
import styles from './NftItemDialog.module.scss';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import toaster from 'services/toaster.tsx';
import getSlotName from 'lib/Inventory/utils/getSlotName';
import get from 'lodash/get';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import AutorenewIcon from '@mui/icons-material/Autorenew';
import LockOpen from '@mui/icons-material/LockOpen';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import { useSlots } from 'lib/Inventory/context/slots/Slots.context.ts';
import { INftItemDto, NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';
import { useUnlockSlot } from 'lib/Inventory/hooks/useUnlockSlot.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { useTranslation } from 'react-i18next';
import getFinePrice from 'utils/getFinePrice';
import { useNftItem } from 'lib/NFT/hooks/useNftItem.ts';
import Alert from '@mui/material/Alert';
import { IWaitConfirmationsResult } from '@chain/hooks/useTransactionConfirmation.ts';

type Props = {
  item: INftItemDto;
  slotId: number | null;
  isEquiped: boolean;
  onClose?: () => void;
  onSell?: (item: INftItemDto) => void;
  onEquip?: (item: INftItemDto, slot: number) => Promise<void>;
  onUnEquip?: (item: INftItemDto, slot: number) => Promise<void>;
  sellingDisabled?: boolean;
  loading?: boolean;
  tradeBalance?: number;
  coreBalance?: number;
  noGas?: boolean;
  confirmation?: IWaitConfirmationsResult;
};

const NftItemDialog: React.FC<Props> = (props) => {
  const {
    item,
    slotId,
    isEquiped,
    onClose,
    onSell,
    sellingDisabled = true,
    onEquip,
    onUnEquip,
    loading = false,
    noGas = false,
    coreBalance,
    tradeBalance,
    confirmation,
  } = props;
  const { accountAddress } = useAccount();

  const { t } = useTranslation('inventory');

  const { slots: slotTypes, userSlots: slots, address: slotsAddress } = useSlots();
  const [isAction, setIsAction] = React.useState(false);

  const tradeToken = useTradeToken();
  const nftItemState = useNftItem({
    assetId: item.assetId,
    contractId: item.contractId,
  });
  const nftItem = nftItemState.loading ? null : nftItemState.state;

  const getSlotAvailable = (slotId) => {
    const slot = slots[slotId];
    return slot && slot.isEnabled && !slot.tokenId;
  };

  const getSlotReleasable = (slotId) => {
    const slot = slots[slotId];
    return slot && slot.isEnabled && slot.tokenId;
  };

  const getSlotUnlockable = (slotId) => {
    const slot = slots[slotId];
    const require = slotTypes[slotId] && slotTypes[slotId].requireSlot;
    return slot && !slot.isEnabled && (require === 0 || slots[require].isEnabled);
  };

  const availableSlotId = item?.type?.slots
    .map((el) => el.slot)
    .find((slotId) => getSlotAvailable(slotId));
  const unlockableSlotId = item?.type?.slots
    .map((el) => el.slot)
    .find((slotId) => getSlotUnlockable(slotId));
  const releasableSlotId = item?.type?.slots
    .map((el) => el.slot)
    .find((slotId) => getSlotReleasable(slotId));

  const availableSlot = slotTypes[availableSlotId];
  const unlockableSlot = slotTypes[unlockableSlotId];
  const releasableSlot = slotTypes[releasableSlotId];

  const tradePrice = unlockableSlot?.sgbPrice;
  const pixelPrice = unlockableSlot?.pixelPrice;
  const noCoreBalance = coreBalance < pixelPrice;
  const noTradeBalance = tradeBalance < tradePrice;

  const tokenURI = get(item, 'tokenURI', '');
  const isVideo = tokenURI.includes('.mp4');
  const isAvailableToUnlock =
    !isVideo &&
    !!unlockableSlot &&
    !item?.type.droneMiner &&
    !item?.type.recipe &&
    unlockableSlotId !== NftItemSlot.Recipe;
  const isAvailableToEquip = !item?.type.droneMiner && !item?.type.recipe;
  const isDisabled = isAction || noGas || confirmation?.isPending;

  const unlockSlotApi = useUnlockSlot({
    slotsAddress,
    token2Address: tradeToken.data.address,
    token2Price: tradePrice,
    accountAddress,
  });

  const handleEquip = async (slot: any) => {
    setIsAction(true);
    await onEquip?.(item, slot.slotId);
    setIsAction(false);
  };

  const handleUnEquip = async (slot: number) => {
    setIsAction(true);
    await onUnEquip?.(item, slot);
    setIsAction(false);
  };

  const onUnlockSlot = async (isSgb = false) => {
    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      setIsAction(true);

      if (!isSgb) {
        await unlockSlotApi.onBuy(unlockableSlot.slotId);
      } else {
        await unlockSlotApi.onBuy2(unlockableSlot.slotId);
      }

      await onEquip?.(item, unlockableSlot.slotId);
    } catch (error) {
      toaster.logError(error, '[onUnlockSlot]');
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }
    setIsAction(false);
  };

  const onReplaceItem = async () => {
    if (!onUnEquip || !onEquip) {
      return;
    }

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      setIsAction(true);

      await onUnEquip(item, releasableSlot.slotId);
      await onEquip(item, releasableSlot.slotId);
    } catch (error) {
      toaster.logError(error, '[onReleaseSlot]');
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }
    setIsAction(false);
  };

  return (
    <BottomDialog
      open
      onClose={onClose}
      title={item.type?.name}
      subTitle={item.type?.collection?.name}
      data-typeid={item.typeId}
    >
      {/*{(!nftItem || nftItemState.loading) && <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: 150 }}><CircularProgress/></Box>}*/}

      {!!isVideo && !nftItemState.loading && (
        <video className={styles.itemVideo} autoPlay loop>
          <source src={tokenURI} type={'video/mp4'} />
        </video>
      )}

      <div className={styles.itemBox}>
        {!isVideo && <NFTCard nft={nftItem || item} loading={nftItemState.loading} />}

        {nftItem?.type?.disposable && (
          <Alert severity='warning' variant='outlined' sx={{ mb: 2 }}>
            {t('WARNING! If you remove NFT from slot, YOU WILL LOSE YOUR NFT FOREVER!')}
          </Alert>
        )}
        {confirmation?.isPending && (
          <Alert severity='warning' variant='outlined' sx={{ mb: 2 }}>
            Wait for confirmations {confirmation?.confirmations || ''}
          </Alert>
        )}

        <div style={{ display: 'flex', flexDirection: 'column', gap: 8, width: '100%' }}>
          {isAvailableToEquip && !isVideo && isEquiped && (
            <Button
              onClick={() => handleUnEquip(slotId)}
              className={styles.itemButton}
              size='large'
              variant='contained'
              color='primary'
              startIcon={<RemoveCircleOutlineIcon />}
              fullWidth
              disabled={isDisabled}
              loading={isAction || loading}
            >
              {noGas && 'No Gas'}
              {!noGas && t('Unequip')}
            </Button>
          )}

          {isAvailableToEquip && !isVideo && !isEquiped && availableSlot && (
            <Button
              onClick={() => handleEquip(availableSlot)}
              className={styles.itemButton}
              size='large'
              variant='contained'
              color='primary'
              startIcon={<AddCircleOutlineIcon />}
              disabled={isDisabled}
              fullWidth
              loading={isAction || loading}
            >
              {noGas && 'No Gas'}
              {!noGas && `${t('Equip to')} ${getSlotName(availableSlot.slotId)} Slot`}
            </Button>
          )}

          {isAvailableToUnlock && Boolean(unlockableSlot && pixelPrice) && (
            <Button
              onClick={() => onUnlockSlot(false)}
              className={styles.itemButton}
              size='large'
              variant='contained'
              color='primary'
              startIcon={<LockOpen />}
              disabled={isDisabled}
              fullWidth
              loading={isAction || loading}
            >
              {noGas && 'No Gas'}
              {!noGas &&
                noCoreBalance &&
                `Require ${getFinePrice(pixelPrice)} ${tradeToken.coreSymbol || ''}`}
              {!noGas &&
                !noCoreBalance &&
                `${t('Unlock')} ${getSlotName(unlockableSlot.slotId)} Slot for ${getFinePrice(
                  pixelPrice
                )} ${tradeToken.coreSymbol}`}
            </Button>
          )}
          {isAvailableToUnlock && Boolean(unlockableSlot && tradePrice) && (
            <Button
              onClick={() => onUnlockSlot(true)}
              className={styles.itemButton}
              size='large'
              variant='contained'
              color='primary'
              startIcon={<LockOpen />}
              disabled={isDisabled}
              fullWidth
              loading={isAction || loading}
            >
              {noGas && 'No Gas'}
              {!noGas &&
                `${t('Unlock')} ${getSlotName(unlockableSlot.slotId)} Slot for ${tradePrice} ${
                  tradeToken.symbol
                }`}
            </Button>
          )}

          {isAvailableToEquip && !isVideo && !!releasableSlot && !isEquiped && !availableSlot && (
            <Button
              onClick={onReplaceItem}
              className={styles.itemButton}
              size='large'
              variant='contained'
              color='primary'
              startIcon={<AutorenewIcon />}
              disabled={isDisabled}
              loading={isAction || loading}
            >
              {noGas && 'No Gas'}
              {!noGas && t('Replace with item in')} {getSlotName(releasableSlot.slotId)} {t('Slot')}
            </Button>
          )}

          {!isVideo && !sellingDisabled && !item?.type?.soulbound && (
            <Button
              size='large'
              variant='contained'
              color='primary'
              fullWidth
              disabled={isDisabled}
              style={{ width: '100%', marginBottom: 8 }}
              onClick={() => {
                if (onSell) {
                  onSell(item);
                }
              }}
            >
              {noGas && 'No Gas'}
              {!noGas && t('Sell')}
            </Button>
          )}

          <Button
            onClick={() => {
              Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
              onClose();
            }}
            className={styles.itemButton}
            size='large'
            variant='outlined'
            color='primary'
            fullWidth
            disabled={isAction}
            loading={isAction || loading}
          >
            {t('Close')}
          </Button>
        </div>
      </div>
    </BottomDialog>
  );
};

export default React.memo(NftItemDialog);
