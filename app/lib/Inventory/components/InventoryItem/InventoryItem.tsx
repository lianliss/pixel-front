import React from 'react';
import clsx from 'clsx';
import styles from './IntentoryItem.module.scss';
import { RARITY_COLORS } from 'const/rarity';
import uriTransform from 'utils/uriTransform';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { getNftSlotIcon } from 'lib/NFT/utils/getNftSlotIcon.tsx';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { useTranslation } from 'react-i18next';
import LockIcon from '@mui/icons-material/Lock';

type Props = {
  item: INftItemDto;
  onPreview?: (item: INftItemDto) => void;
  selected?: boolean;
} & React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

const InventoryItem: React.FC<Props> = (props) => {
  const { item, onPreview, selected, ...other } = props;
  const { type: { slots = [], uri: tokenURI, rarity, name } = {} } = item;

  const { t } = useTranslation('inventory');

  const slotValue = slots?.[0]?.slot;
  const rarityColor = getNftRarityColor(rarity);

  const SlotIcon = getNftSlotIcon(slotValue);
  const isVideo = (tokenURI || '').includes('.mp4');

  return (
    <div className={clsx(styles.inventoryItem, { [styles.selected]: selected })} {...other}>
      <div
        className={styles.inventoryItem__box}
        onClick={() => {
          onPreview?.(item);
        }}
        style={{
          backgroundImage: tokenURI ? `url(${uriTransform(tokenURI)})` : undefined,
          borderColor: RARITY_COLORS[rarity],
          borderStyle: item.type.recipe ? 'dashed' : 'solid',
          borderWidth: item.type.recipe ? 2 : 1,
        }}
      >
        {isVideo && (
          <video autoPlay muted loop>
            <source src={tokenURI} type={'video/mp4'} />
          </video>
        )}

        {item.type?.soulbound && (
          <div className={styles.card_soulbound}>
            <div className={styles.souldbound__inner} style={{ color: rarityColor }}>
              <LockIcon />
            </div>
          </div>
        )}

        <div className={styles.card_slot}>
          <div className={styles.slot__inner} style={{ color: rarityColor }}>
            {SlotIcon}
          </div>
        </div>
      </div>
      <div
        className={styles.inventoryItem__info}
        onClick={() => {
          onPreview?.(item);
        }}
      >
        {/*{count && <span className={`${PREFIX}__count`}>{count}/{total}</span>}*/}
        <Typography
          className={styles.inventoryItem__label}
          align='center'
          color={selected ? 'primary' : undefined}
        >
          {item ? name : t('Empty')}
        </Typography>
      </div>
    </div>
  );
};

export default InventoryItem;
