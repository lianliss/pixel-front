import React from 'react';
import './ItemEquipDialog.scss';
import clsx from 'clsx';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Button } from '@blueprintjs/core';
import { useTranslation } from 'react-i18next';

const PREFIX = 'itemEquipDialog';

const ItemEquipDialog = (props) => {
  const { children, className, items = [], onEquip, ...other } = props;

  const { t } = useTranslation('inventory');

  const [selected, setSelected] = React.useState(null);

  const handleEquip = () => {
    if (selected && onEquip) {
      onEquip(selected);
    }
  };

  return (
    <BottomDialog
      title='Pocket'
      subTitle={t('You can insert one of the following items into your pocket')}
      className={clsx(PREFIX, className)}
      {...other}
    >
      <div className={`${PREFIX}__list`}>
        {items.map((item) => (
          <div
            key={`item-${item.id}`}
            className={clsx(`${PREFIX}__item`, {
              [`${PREFIX}__itemSelected`]: selected && selected.id === item.id,
            })}
            onClick={() => setSelected(item)}
          >
            <img src={item.image} alt='' />
            <p>{item.title}</p>
          </div>
        ))}
      </div>
      <Button className={`${PREFIX}__btn`} onClick={handleEquip} disabled={!selected || !onEquip}>
        {t('Equip')}
      </Button>
    </BottomDialog>
  );
};

export default React.memo(ItemEquipDialog);
