import React, { useEffect } from 'react';
import styles from './InventoryGrid.module.scss';
import { Tabs } from '@ui-kit/Tabs/Tabs.tsx';
import { Tab } from '@ui-kit/Tab/Tab.tsx';
import Paper, { PaperProps } from '@mui/material/Paper';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import LinearProgress from '@mui/material/LinearProgress';
import InventoryRecipes from 'lib/Inventory/components/InventoryGrid/components/InventoryRecipes/InventoryRecipes.tsx';
import InventoryItems from 'lib/Inventory/components/InventoryGrid/components/InventoryItems/InventoryItems.tsx';
import { useTranslation } from 'react-i18next';
import { IS_MINER_DRONE } from '@cfg/app.ts';
import UserChestsGrid from 'lib/Chests/components/UserChestsGrid/UserChestsGrid.tsx';
import { IUserChestDto } from 'lib/Chests/api-server/chest.types.ts';
import clsx from 'clsx';

type Props = {
  // nft
  items: INftItemDto[];
  total: number;

  // recipes
  recipes?: {
    loading?: boolean;
    page?: number;
    onChangePage?: (page: number) => void;
    rows?: INftItemDto[];
    count?: number;
    isFetched?: boolean;
  };

  page?: number;
  onChangePage?: (page: number) => void;

  // chest
  chests: IUserChestDto[];
  chestActionLoading?: boolean;
  chestsLoading?: boolean;
  onClaimChest?: (chest: IUserChestDto) => void;

  // chest v2
  userChests: IUserChestDto[];
  userChestsLoading: boolean;
  onClaimUserChest?: (chest: IUserChestDto) => void;

  // common
  tab: string;
  onChangeTab?: (tab: string) => void;

  isActiveFilter: boolean;
  handleOpenFilter?: () => void;

  onPreview?: (item: INftItemDto) => void;
} & Omit<PaperProps, 'children'>;

const InventoryGrid: React.FC<Props> = (props) => {
  const {
    items = [] as INftItemDto[],
    onChangeTab,
    tab = 'all',
    total = 0,
    onPreview,
    page = 1,
    onChangePage,
    handleOpenFilter,
    isActiveFilter,
    //
    recipes,
    // chests
    chests = [],
    onClaimChest,
    chestActionLoading = false,
    chestsLoading = false,
    // chests v2
    userChests = [] as IUserChestDto[],
    userChestsLoading = false,
    onClaimUserChest,
    //
    className,
    ...other
  } = props;
  const { t } = useTranslation('inventory');
  const chestsCount = (chests.length || 0) + userChests.reduce((acc, el) => acc + el.count, 0);

  return (
    <Paper id='#tarInvElement1' {...other} className={clsx(styles.inventoryItems, className)}>
      <div className={styles.tabs__wrapper}>
        <Tabs onChange={(_, v) => onChangeTab?.(v)} value={tab}>
          <Tab value='all' label={t('All')} count={total || undefined} />
          {recipes && (
            <Tab value='recipes' label={t('Recipes')} count={recipes?.count || undefined} />
          )}
          <Tab value='boxes' label={t('Boxes')} count={chestsCount || undefined} />
        </Tabs>
      </div>

      {tab === 'boxes' && (chestsLoading || chestActionLoading) && <LinearProgress />}

      {tab === 'all' && (
        <InventoryItems
          page={page}
          onChangePage={onChangePage}
          onPreview={onPreview}
          total={total}
          items={items}
          isActiveFilter={isActiveFilter}
          handleOpenFilter={handleOpenFilter}
        />
      )}
      {tab === 'boxes' && (
        <UserChestsGrid
          chests={chests}
          chestActionLoading={chestActionLoading}
          chestsLoading={chestsLoading}
          onClaimChest={onClaimChest}
          userChests={userChests}
          userChestsLoading={userChestsLoading}
          onClaimUserChest={onClaimUserChest}
        />
      )}
      {tab === 'recipes' && recipes && (
        <InventoryRecipes
          onPreview={onPreview}
          page={recipes.page}
          onChangePage={recipes.onChangePage}
          loading={recipes.loading}
          rows={recipes.rows}
          count={recipes.count}
          isFetched={recipes.isFetched}
        />
      )}
    </Paper>
  );
};

export default InventoryGrid;
