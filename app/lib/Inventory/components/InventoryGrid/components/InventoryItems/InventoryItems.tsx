import styles from './InventoryItems.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import React from 'react';
import { styled } from '@mui/material/styles';
import IconSliders from '@ui-kit/icon/common/IconSliders.tsx';
import InventoryItem from 'lib/Inventory/components/InventoryItem/InventoryItem.tsx';
import Pagination from '@mui/material/Pagination';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { useTranslation } from 'react-i18next';

const StyledItems = styled('div')(({ theme }) => ({
  backgroundColor: theme.palette.background.default,
}));

type Props = {
  items: INftItemDto[];
  total: number;

  isActiveFilter: boolean;
  handleOpenFilter?: () => void;

  onPreview?: (item: INftItemDto) => void;

  page?: number;
  onChangePage?: (page: number) => void;
};

function InventoryItems(props: Props) {
  const { t } = useTranslation('inventory');

  const { isActiveFilter, handleOpenFilter, items, total, onPreview, page, onChangePage } = props;

  const pagesCount = Math.ceil(total / 24);

  return (
    <StyledItems className={styles.root}>
      <div className={styles.heading}>
        <Typography
          variant='caption'
          component='p'
          color='text.secondary'
          sx={{ mb: 1.5, mt: 0.5 }}
        >
          {t('Here you can find all your items')}
        </Typography>
        <button
          className={styles.btn__filter}
          onClick={handleOpenFilter}
          aria-selected={isActiveFilter}
        >
          <IconSliders />
        </button>
      </div>

      <div className={styles.root__box}>
        {items.map((item) => (
          <InventoryItem key={`item-${item.id}`} item={item} onPreview={onPreview} />
        ))}
      </div>

      {total > 24 && (
        <Pagination
          sx={{ margin: '16px auto 0 auto' }}
          count={pagesCount}
          color='primary'
          variant='outlined'
          shape='rounded'
          size='large'
          siblingCount={1}
          boundaryCount={0}
          page={page}
          onChange={(_, p) => {
            onChangePage?.(p);
          }}
        />
      )}
    </StyledItems>
  );
}

export default InventoryItems;
