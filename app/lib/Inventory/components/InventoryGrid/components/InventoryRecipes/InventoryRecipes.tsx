import styles from './InventoryRecipes.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import React from 'react';
import { styled } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';
import { useUnit } from 'effector-react';
import { userMinerClaimersStore } from 'lib/MinerClaimer/api-server/miner-claimer.store.ts';
import InventoryItem from 'lib/Inventory/components/InventoryItem/InventoryItem.tsx';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { userNftRecipesStore } from 'lib/Forge/api-server/miner-claimer.store.ts';
import Pagination from '@mui/material/Pagination';
import CircularProgress from '@mui/material/CircularProgress';

const StyledItems = styled('div')(({ theme }) => ({
  backgroundColor: theme.palette.background.default,
}));

type Props = {
  onPreview?: (nft: INftItemDto) => void;
  page?: number;
  onChangePage?: (page: number) => void;
  loading?: boolean;
  rows?: INftItemDto[];
  count?: number;
  isFetched?: boolean;
};

function InventoryRecipes(props: Props) {
  const { onPreview, page, onChangePage, loading = false, rows, count, isFetched } = props;
  const { t } = useTranslation('inventory');

  const pagesCount = Math.ceil(count / 24);

  return (
    <StyledItems className={styles.root}>
      <div>
        <Typography
          variant='caption'
          component='p'
          color='text.secondary'
          sx={{ mb: 1.5, mt: 0.5 }}
        >
          {t('Here you can find all your recipes')}
        </Typography>
      </div>

      {count > 0 && (
        <div className={styles.grid}>
          {rows?.map((item) => (
            <InventoryItem key={`item-${item.id}`} item={item} onPreview={onPreview} />
          ))}
        </div>
      )}
      {!count && loading && <CircularProgress sx={{ mx: 'auto', mt: 6, mb: 6 }} />}
      {isFetched && !loading && !count && <Typography variant='subtitle1'>No recipes</Typography>}

      {count > 24 && onChangePage && (
        <Pagination
          sx={{ margin: '16px auto 0 auto' }}
          count={pagesCount}
          color='primary'
          variant='outlined'
          shape='rounded'
          size='large'
          siblingCount={1}
          boundaryCount={0}
          page={page}
          onChange={(_, p) => {
            onChangePage?.(p);
          }}
        />
      )}
    </StyledItems>
  );
}

export default InventoryRecipes;
