import React from 'react';
import styles from './EmptyPocketDialog.module.scss';
import toaster from 'services/toaster.tsx';
import getFinePrice from 'utils/getFinePrice';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useUnlockSlot } from 'lib/Inventory/hooks/useUnlockSlot.ts';
import { SLOTS_CONTRACT_ID } from 'lib/Inventory/api-contract/slots.constants.ts';
import { useTranslation } from 'react-i18next';
import BuyTokenAlertLazy from 'lib/TelegramStars/components/BuyTokenAlert/BuyTokenAlert.lazy.tsx';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import BuyGasAlertLazy from 'lib/TelegramStars/components/BuyGasAlert/BuyGasAlert.lazy.tsx';
import { getNftSlotLabel } from 'lib/NFT/utils/getNftSlotLabel.ts';

type Props = {
  disabled?: boolean;
  slotType?: any;
  onClose?: () => void;
  onBuySlot?: () => void;
  noGas?: boolean;
  coreBalance?: number;
  coreSymbol?: string;
  tradeBalance?: number;
  tradeSymbol?: string;
  tradeAddress?: string;
};

const EmptyPocketDialog: React.FC<Props> = (props) => {
  const {
    disabled,
    slotType,
    onClose,
    onBuySlot,
    noGas,
    coreBalance,
    tradeBalance,
    coreSymbol,
    tradeSymbol,
    tradeAddress,
  } = props;

  const account = useAccount();
  const { accountAddress } = account;
  const slotId = slotType?.slotId;

  const { t } = useTranslation('inventory');
  const { t: tNft } = useTranslation('nft');

  const slotsAddress = SLOTS_CONTRACT_ID[account.chainId];
  const tradePrice = slotType?.sgbPrice;
  const pixelPrice = slotType?.pixelPrice;
  const noCoreBalance = coreBalance < pixelPrice;
  const noTradeBalance = tradeBalance < tradePrice;

  const unlockSlotApi = useUnlockSlot({
    slotsAddress,
    token2Address: tradeAddress,
    token2Price: tradePrice,
    accountAddress,
  });

  const [isPixelProcess, setIsPixelProcess] = React.useState(false);

  const title = tNft(getNftSlotLabel(slotId));

  const handleBuyCoin = async () => {
    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      await unlockSlotApi.onBuy2(slotId);

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
      toaster.success(`${title} slot unlocked`);

      await onBuySlot?.();
      onClose?.();
    } catch (e) {
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
      toaster.logError(e);
    }
  };

  const onBuyPixel = async () => {
    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      setIsPixelProcess(true);
      await unlockSlotApi.onBuy(slotId);

      toaster.success(`${title} slot unlocked`);
      Telegram.WebApp.HapticFeedback.notificationOccurred('success');

      await onBuySlot?.();
    } catch (error) {
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
      toaster.logError(error);
    }
    setIsPixelProcess(false);
  };

  return (
    <BottomDialog isOpen className={styles.dialog} onClose={onClose}>
      <Typography align='center' fontWeight='bold' variant='h6' sx={{ mb: 1 }}>
        {t('Empty')} {title}
      </Typography>
      <Typography align='center' variant='subtitle2' sx={{ mb: 2 }} color='textSecondary'>
        {t('You can buy an additional', { title })}
      </Typography>

      {noGas && (
        <BuyGasAlertLazy
          sx={{
            width: '100%',
            mb: 2,
          }}
          text='No enough gas'
        />
      )}

      {!noGas && noTradeBalance && (
        <BuyTokenAlertLazy
          sx={{
            width: '100%',
            mb: 2,
          }}
          text='No enough funds'
        />
      )}

      {!disabled && !(typeof tradePrice === 'undefined' && typeof pixelPrice === 'undefined') ? (
        <div className={styles.dialogColumns}>
          {!!pixelPrice && (
            <Button
              disabled={unlockSlotApi.loading || isPixelProcess || noGas || noCoreBalance}
              variant='contained'
              color='primary'
              fullWidth
              size='large'
              loading={isPixelProcess}
              onClick={onBuyPixel}
            >
              {noGas && 'No Gas'}
              {!noGas && noCoreBalance && `Require ${getFinePrice(pixelPrice)} ${coreSymbol || ''}`}
              {!noGas &&
                !noCoreBalance &&
                `${t('Buy for')} ${getFinePrice(pixelPrice)} ${coreSymbol || ''}`}
            </Button>
          )}
          {unlockSlotApi.buy2Enabled && (
            <Button
              disabled={isPixelProcess || noGas || noTradeBalance}
              variant='contained'
              color='primary'
              fullWidth
              size='large'
              loading={unlockSlotApi.loading}
              onClick={handleBuyCoin}
            >
              {noGas && 'No Gas'}
              {!noGas &&
                noTradeBalance &&
                `Require ${getFinePrice(tradePrice)} ${tradeSymbol || ''}`}
              {!noGas &&
                !noTradeBalance &&
                `${t('Buy for')} ${getFinePrice(tradePrice)} ${tradeSymbol || ''}`}
            </Button>
          )}
        </div>
      ) : (
        <Button variant='contained' color='primary' fullWidth size='large' disabled>
          {t('Slot is not available yet')}
        </Button>
      )}
    </BottomDialog>
  );
};

export default React.memo(EmptyPocketDialog);
