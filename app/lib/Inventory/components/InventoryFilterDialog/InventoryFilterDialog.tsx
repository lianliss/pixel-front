import React from 'react';
import styles from './InventoryFilterDialog.module.scss';
import clsx from 'clsx';
import NftTypeIcon from 'lib/NFT/components/NftTypeIcon/NftTypeIcon.tsx';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import {
  MODS_LIST,
  RARITY_LIST,
  SLOTS_LIST,
  STATUS_LIST,
} from 'lib/Marketplace/components/MarketToolbar/data.ts';
import Divider from 'ui/Divider';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { NftItemRarity, NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import { getNftSlotLabel } from 'lib/NFT/utils/getNftSlotLabel.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Checkbox } from '@ui-kit/Checkbox/Checkbox.tsx';
import { useTranslation } from 'react-i18next';

export type IInventoryFilterState = {
  status: string;
  rarities?: NftItemRarity[];
  slots?: NftItemSlot[];
  mods?: string[];
};

type Props = {
  onClose?: () => void;
  filters: IInventoryFilterState;
  handleChangeFilter: (field: string, item: any) => void;
  onChange?: (isActive: boolean) => void;
  reset?: () => void;
};

function InventoryFilterDialog(props: Props) {
  const { onClose, onChange, handleChangeFilter, filters, reset } = props;

  const { t } = useTranslation('inventory');

  const handleClear = () => {
    reset();
    onChange(false);
  };

  const handleDone = () => {
    if (onChange) {
      onChange(true);
      Telegram.WebApp.HapticFeedback.impactOccurred('medium');
    }
  };

  return (
    <BottomDialog
      isOpen
      onClose={onClose}
      PaperProps={{ className: styles.dialog }}
      footer={
        <div className={styles.btns}>
          <Button size='large' variant='outlined' onClick={handleClear}>
            {t('Clear All')}
          </Button>
          <Button size='large' variant='contained' onClick={handleDone}>
            {t('Done')}
          </Button>
        </div>
      }
    >
      <div className={styles.root}>
        <div>
          <Typography className={styles.title} variant='body1' fontWeight='bold'>
            {t('Slot')}
          </Typography>

          <div className={clsx(styles.checkboxes, styles.checkboxes_2cols)}>
            {SLOTS_LIST.map((slot) => (
              <div className={styles.checkbox__wrapper} key={`filter-slot-${slot}`}>
                <Checkbox
                  checked={filters.slots.includes(slot)}
                  className={styles.checkbox}
                  onChange={() => handleChangeFilter('slots', slot)}
                />
                <div className={styles.checkbox__label}>
                  <NftTypeIcon slot={slot} compact size='small' /> {getNftSlotLabel(slot)}
                </div>
              </div>
            ))}
          </div>
        </div>

        <Divider />

        <div>
          <Typography className={styles.title} variant='body1' fontWeight='bold'>
            {t('Rarity')}
          </Typography>

          <div className={clsx(styles.checkboxes, styles.checkboxes_2cols)}>
            {RARITY_LIST.map((rarity) => (
              <div className={styles.checkbox__wrapper} key={`filter-rarity-${rarity}`}>
                <Checkbox
                  style={{ color: getNftRarityColor(rarity) }}
                  onChange={() => handleChangeFilter('rarities', rarity)}
                  checked={filters.rarities.includes(rarity)}
                />
                <div className={styles.checkbox__label}>{getNftRarityLabel(rarity)}</div>
              </div>
            ))}
          </div>
        </div>

        <Divider />

        <div>
          <Typography className={styles.title} variant='body1' fontWeight='bold'>
            {t('Modifiers')}
          </Typography>

          <div className={styles.checkboxes}>
            {MODS_LIST.map((mod) => {
              return (
                <div className={styles.checkbox__wrapper} key={`filter-mod-${mod}`}>
                  <Checkbox
                    onChange={() => handleChangeFilter('mods', mod.value)}
                    checked={filters.mods.map((i) => String(i)).includes(String(mod.value))}
                  />
                  <div className={styles.checkbox__label}> {t(mod.label)} </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </BottomDialog>
  );
}

export default React.memo(InventoryFilterDialog);
