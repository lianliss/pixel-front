import React from 'react';
import styles from './StatsDialog.module.scss';
import StatValue from 'lib/NFT/components/StatValue/StatValue.tsx';
import { Spinner } from '@blueprintjs/core';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { getNftStatLabel } from 'lib/NFT/utils/getNftStatLabel';
import { getNftStatsValue, isNftStatPercent } from 'lib/NFT/utils/getNftStatsValue.ts';
import { useSumNftStats } from 'lib/Inventory/components/StatsDialog/useSumNftStats.ts';
import { isNftModBuff } from 'lib/NFT/components/NFTCard/convertToNftItemDto.ts';
import { useTranslation } from 'react-i18next';

type Props = { onClose?: () => void; userSlots: any[]; open: boolean };

const StatsDialog = (props: Props) => {
  const { onClose, userSlots = [], open } = props;

  const { t } = useTranslation('inventory');

  const sumStats = useSumNftStats({ autoLoad: false });

  React.useEffect(() => {
    if (open) {
      void sumStats.load();
    }
  }, [userSlots, open]);

  return (
    <BottomDialog open={open} onClose={onClose} title={t('Statistic')}>
      {sumStats.loading && <Spinner />}
      <div className={styles.dialogStats}>
        {!sumStats.loading && !Object.keys(sumStats.data).length && (
          <h3>{t('There is no bonuses yet')}</h3>
        )}
        {Object.keys(sumStats.data).map((_parameterId, index) => {
          const parameterId = Number(_parameterId);
          const title = getNftStatLabel(parameterId);
          const mod = {
            modId: parameterId.toString(),
            add: sumStats.data[parameterId].add,
            mult: sumStats.data[parameterId].mult,
            buff: isNftModBuff({
              modId: parameterId.toString(),
              add: sumStats.data[parameterId].add,
              mult: sumStats.data[parameterId].mult,
            }),
          };
          const value = getNftStatsValue(mod);
          const isPercent = isNftStatPercent(parameterId.toString());

          return (
            <React.Fragment key={index}>
              {index !== 0 && <hr />}
              <div className={styles.dialogItem}>
                <span>{title}</span>
                <StatValue value={value} percent={isPercent} buff={mod.buff} />
              </div>
            </React.Fragment>
          );
        })}
      </div>
      <Button
        sx={{ mt: 2 }}
        variant='contained'
        size='large'
        color='primary'
        fullWidth
        onClick={onClose}
      >
        OK
      </Button>
    </BottomDialog>
  );
};

export default React.memo(StatsDialog);
