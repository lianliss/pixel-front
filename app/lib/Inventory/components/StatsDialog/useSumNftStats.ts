import { sumNftStats } from 'lib/Inventory/components/StatsDialog/utils.ts';
import toaster from 'services/toaster.tsx';
import React from 'react';
import { useSlots } from 'lib/Inventory/context/slots/Slots.context.ts';
import { wei } from 'utils/index';

export function useSumNftStats({ autoLoad = true }: { autoLoad?: boolean } = {}) {
  const slots = useSlots();

  const [parameters, setParameters] = React.useState<Record<string, { add: number; mult: number }>>(
    {}
  );
  const [isLoading, setIsLoading] = React.useState(true);

  const loadItems = async () => {
    try {
      setIsLoading(true);

      const types = slots.userSlots.map((s) => s.itemType).filter((t) => !!t);
      const sum = sumNftStats(types);

      setParameters(sum);
    } catch (error) {
      console.error('failed to load nft stats', error);
      toaster.logError(error);
    }
    setIsLoading(false);
  };

  React.useEffect(() => {
    if (autoLoad) {
      void loadItems();
    }
  }, [autoLoad, slots.userSlots]);

  return {
    load: loadItems,
    loading: isLoading,
    data: parameters,
    stats: {
      drill: parameters[0] ? +wei.from(parameters[0]?.mult, 4) - 1 : 1,
      storage: parameters[1] ? +wei.from(parameters[1]?.mult, 4) - 1 : 1,
      referral: parameters[2] ? +wei.from(parameters[2]?.mult, 4) - 1 : 1,
    },
    discount: {
      drill: (parameters[6] && +wei.from(parameters[6]?.mult, 4)) || 1,
      storage: (parameters[7] && +wei.from(parameters[7]?.mult, 4)) || 1,
      referralStorage: (parameters[8] && +wei.from(parameters[8]?.mult, 4)) || 1,
    },
    marketDiscount: {
      sale: (parameters[17] && +wei.from(parameters[17]?.mult, 4)) || 1,
      publication: (parameters[18] && +wei.from(parameters[18]?.mult, 4)) || 1,
    },
  };
}
