import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';

export function sumNftStats(items: INftItemDto[]): Record<string, { add: number; mult: number }> {
  const parameters: Record<string, { add: number; mult: number }> = {};

  const arr = items.map((item) => item.type.mods).flatMap((el) => el);

  arr.forEach((p) => {
    const parameterId = p.modId;
    const mul = Number(p.mult);
    const add = Number(p.add);

    if (!parameters[parameterId]) {
      parameters[parameterId] = { add, mult: mul / 10_000 };
    } else {
      parameters[parameterId].add += add;
      parameters[parameterId].mult *= mul / 10_000;
    }
  });

  for (const k in parameters) {
    if (parameters[k].mult) {
      parameters[k].mult *= 10_000;
      parameters[k].mult = Math.round(parameters[k].mult);
    }
  }

  return parameters;
}
