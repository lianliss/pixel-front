import React from 'react';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';

export type ISlotsContext = {
  enabled: boolean;
  address: string;
  loadingSlots: boolean;
  loadSlots: () => Promise<void>;
  slots: ISlotDto[];
  userSlots: IUserSlotDto[];
};

export type ISlotDto = {
  slotId: number;
  title: string;
  pixelPrice: number;
  sgbPrice: number;
  requireSlot: number;
};

export type IUserSlotDto = {
  slotId: number;
  isEnabled: boolean;
  variantId: number;
  tokenId: number;
  itemType: INftItemDto;
};

export const SlotsContext = React.createContext<ISlotsContext>(null);

export function useSlots(): ISlotsContext {
  return React.useContext(SlotsContext);
}
