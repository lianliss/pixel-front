import React from 'react';
import { wei } from 'utils/index';
import { ISlotDto, IUserSlotDto, SlotsContext } from 'lib/Inventory/context/slots/Slots.context.ts';

import { useAccount } from '@chain/hooks/useAccount.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import SLOTS_ABI from 'lib/Inventory/api-contract/abi/SLOTS.abi.ts';
import { SLOTS_CONTRACT_ID } from 'lib/Inventory/api-contract/slots.constants.ts';
import { convertSlotItemType } from 'lib/NFT/utils/converters.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { createNftFromType } from 'lib/NFT/utils/create-nft-from-type.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import { createNftTypeFromChain } from 'lib/NFT/utils/create-nft-type-from-chain.ts';
import { useChainUserSlots } from 'lib/Inventory/hooks/useChainUserSlots.ts';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

function SlotsProvider(props: React.PropsWithChildren) {
  const account = useAccount();
  const userInfo = useSelfUserInfo();

  const { chainId } = account;
  const telegramId = userInfo.data?.telegramId;

  const slotsAddress = SLOTS_CONTRACT_ID[chainId];
  const nftAddress = NFT_CONTRACT_ID[chainId];
  const tradeToken = useTradeToken();

  const slots = useReadContract<ISlotDto[]>({
    // initData: [],
    abi: SLOTS_ABI,
    address: slotsAddress,
    skip: !slotsAddress || !account.isConnected,
    functionName: 'getSlotsTypes',
    select: (rawSlots) =>
      rawSlots.map(
        (slot, slotId): ISlotDto => ({
          slotId,
          title: slot.title,
          pixelPrice: wei.from(slot.pixelPrice),
          sgbPrice: wei.from(slot.sgbPrice, tradeToken.decimals),
          requireSlot: Number(slot.requireSlot),
        })
      ),
  });

  const userSlots = useChainUserSlots({ telegramId });

  return (
    <SlotsContext.Provider
      value={{
        enabled: !!slotsAddress,
        address: slotsAddress,
        loadingSlots: userSlots.loading || slots.loading,
        loadSlots: async () => void userSlots.refetch(),
        slots: slots.data || [],
        userSlots: userSlots.data || [],
      }}
    >
      {props.children}
    </SlotsContext.Provider>
  );
}

export default SlotsProvider;
