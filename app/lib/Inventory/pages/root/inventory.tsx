import React, { useEffect } from 'react';
import InventoryPreview from 'lib/Inventory/components/InventoryPreview/InventoryPreview.tsx';
import styles from './inventory.module.scss';
import EmptyPocketDialog from 'lib/Inventory/components/EmptyPocketDialog/EmptyPocketDialog.tsx';
import ItemDialog from 'lib/Inventory/components/NftItemDialog/NftItemDialog.tsx';
import StatsDialog from 'lib/Inventory/components/StatsDialog/StatsDialog.tsx';
import ItemEquipDialog from 'lib/Inventory/components/ItemEquipDialog/ItemEquipDialog';
import { Loading } from 'utils/async/load-module';
import { useNft } from 'lib/NFT/context/Nft.context.ts';
import { useMarketplace } from 'lib/Marketplace/context/Marketplace.context.ts';
import MarketSellDialog from 'lib/Marketplace/components/MarketSellDialog/MarketSellDialog.tsx';
import MarketSellConfirmDialog from 'lib/Marketplace/components/MarketSellConfirmDialog/MarketSellConfirmDialog.tsx';
import MarketSuccessSellDialog from 'lib/Marketplace/components/MarketSuccessSellDialog/MarketSuccessSellDialog.tsx';
import { useMarketplaceSell } from 'lib/Marketplace/hooks/useMarketplaceSell.ts';

import { useSlots } from 'lib/Inventory/context/slots/Slots.context.ts';
import ChestDropDialog from 'lib/Chests/components/ChestDropDialog/ChestDropDialog.tsx';
import InventoryGrid from 'lib/Inventory/components/InventoryGrid/InventoryGrid.tsx';
import { Chain } from 'services/multichain/chains';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useOwnedNfts } from 'lib/Marketplace/hooks/useOwnedNfts.ts';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { useSlotsApi } from 'lib/Inventory/hooks/useSlotsApi.ts';
import wait from 'utils/wait';
import toaster from 'services/toaster.tsx';
import InventoryFilterDialog, {
  IInventoryFilterState,
} from 'lib/Inventory/components/InventoryFilterDialog/InventoryFilterDialog.tsx';
import { STATUS_LIST } from 'lib/Marketplace/components/MarketToolbar/data.ts';
import OpenChestDialog from 'lib/Chests/components/OpenChestDialog/OpenChestDialog.tsx';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import { useGasBalance } from '@chain/hooks/useGasBalance.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { useCoreToken } from '@chain/hooks/useCoreToken.ts';
import { loadUserNftRecipes } from 'lib/Forge/api-server/miner-claimer.store.ts';
import { useInventoryChests } from 'lib/Inventory/hooks/useInventoryChests.ts';
import { useInventoryOldChests } from 'lib/Inventory/hooks/useInventoryOldChest.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import { useInventoryRecipes } from 'lib/Inventory/hooks/useInventoryRecipes.ts';
import { useLocation, useNavigate } from 'react-router-dom';
import { usePayable } from 'lib/Mining/hooks/usePayable.ts';
import { FORGE_MINER_CLAIMER_CONTRACT_ID } from 'lib/Forge/api-contract/contract.ts';
import { InventoryTutorial } from 'lib/Inventory/components/InventoryTutorial/InventoryTutorial.tsx';
import { isInventoryTutorialStart } from 'lib/Inventory/components/InventoryTutorial/utils/isInventoryTutorialStart.ts';
import Container from '@mui/material/Container';

const ERC_20_ABI = require('const/ABI/Erc20Token');

const initialFilters: IInventoryFilterState = {
  status: STATUS_LIST[0].value,
  rarities: [],
  slots: [],
  mods: [],
};

const Inventory = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const account = useAccount();
  const { chainId, accountAddress } = account;
  const coreTokenAddress = MINER_CONTRACT_ID[chainId]?.core;
  const nftAddress = NFT_CONTRACT_ID[chainId];
  const tradeToken = useTradeToken();
  const coreToken = useCoreToken();
  console.log();
  const nft = useNft();
  const marketplace = useMarketplace();
  const slots = useSlots();

  const [openFilters, setOpenFilters] = React.useState(false);
  const [isActiveFilter, setIsActiveFilter] = React.useState(false);
  const [filters, setFilters] = React.useState<IInventoryFilterState>(initialFilters);

  const ownedNftState = useOwnedNfts({
    nftAddress: nft.address,
    accountAddress,
    limit: 24,
    rarities: filters.rarities,
    slots: filters.slots,
    mods: filters.mods,
  });
  const payableInfo = usePayable();
  const slotsApi = useSlotsApi({
    slotsAddress: slots.address,
    payablePrice: payableInfo.cost.data?.value,
    onEquip: async () => payableInfo.refetch(),
    onUnEquip: async () => payableInfo.refetch(),
  });
  const gasBalance = useGasBalance({
    address: accountAddress,
  });
  const userBalance = useBalance({
    address: accountAddress,
  });
  const noGas = gasBalance.isFetched && !gasBalance.data?.formatted;
  const coreBalance = useBalance({
    address: accountAddress,
    token: coreToken.data?.address,
    skip: !coreToken.data?.address,
  });
  const tradeBalance = useBalance({
    address: accountAddress,
    token: tradeToken.data?.address,
    skip: !tradeToken.data?.address,
  });

  const [tab, setTab] = React.useState('all');

  const inventoryRecipes = useInventoryRecipes();
  const inventoryOldChests = useInventoryOldChests({
    onClaimed: () => ownedNftState.load(),
  });
  const inventoryChestsV2 = useInventoryChests({
    onClaimed: () => ownedNftState.load(),
  });

  const [loading, setLoading] = React.useState(false);
  const [selectedLockedCell, setSelectedLockedCell] = React.useState(null);
  const [selectedItem, setSelectedItem] = React.useState<INftItemDto | null>(null);
  const [isSlotSelected, setIsSlotSelected] = React.useState(null);
  const [selectedSlotId, setSelectedSlotId] = React.useState<number | null>(null);
  const [isEquiped, setIsEquipped] = React.useState(false);
  const [openStats, setOpenStats] = React.useState(false);
  const [selectedCell, setSelectedCell] = React.useState(null);

  const sellingDisabled =
    !nft.address || !marketplace.address || !marketplace.enabled || !isSlotSelected;

  // marketplace
  const allowanceState = useReadContract<bigint>({
    initData: 0 as bigint,
    abi: ERC_20_ABI,
    address: marketplace.tradeToken.address,
    functionName: 'allowance',
    args: [marketplace.tradeToken.address, accountAddress],
    skip: !accountAddress || marketplace.tradeToken.isGas || !marketplace.tradeToken.isFetched,
  });
  const sellState = useMarketplaceSell({
    nft,
    marketplace,
    allowanceState,
    onCompleted: async () => {
      setSelectedItem(null);
      ownedNftState.load().then();
    },
  });

  const handleUnEquip = async (_item: INftItemDto, slot: number) => {
    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      await slotsApi.unEquipItem({ slot });

      await wait(5000);

      await Promise.all([slots.loadSlots()]);
      await ownedNftState.load();

      setIsEquipped(false);
      setSelectedItem(null);
      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      toaster.captureException(e);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }
  };
  const handleEquip = async (item: INftItemDto, slot: number) => {
    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      await slotsApi.equipItem({ assetId: +item.assetId, slot });

      await wait(5000);

      await Promise.all([slots.loadSlots()]);
      await ownedNftState.load();

      setIsEquipped(false);
      setSelectedItem(null);
      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      toaster.captureException(e);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }
  };

  const handleOpenFilter = () => {
    setOpenFilters(!openFilters);
  };

  const handleChangeFilter = (field, item) => {
    setFilters((prevFilters) => {
      const isSelected = prevFilters[field].includes(item);
      return {
        ...prevFilters,
        [field]: isSelected
          ? prevFilters[field].filter((el) => el !== item)
          : [...prevFilters[field], item],
      };
    });
  };

  const handleChangeFilters = (isActive: boolean) => {
    setIsActiveFilter(isActive);
    ownedNftState.changePage(1);
    setOpenFilters(false);
  };

  const onReRequest = async () => {
    await slots.loadSlots();
  };

  useEffect(() => {
    ownedNftState.load();
  }, [ownedNftState.load]);
  useEffect(() => {
    if (account.isConnected && tab === 'recipes' && nftAddress) {
      loadUserNftRecipes({
        userAddress: account.accountAddress,
        contractId: nftAddress.toLowerCase(),
      }).then();
    }
  }, [tab, account.accountAddress, account.isConnected, nftAddress]);
  useBackAction('/wallet');

  // for test
  // useEffect(() => {
  //   setSelectedItem(ownedNftState.data.rows[0] || null);
  // }, [ownedNftState.data.rows]);

  useEffect(() => {
    const queryParams = new URLSearchParams(location.search);
    const currentTabQuery = queryParams.get('tab');

    if (currentTabQuery) {
      queryParams.set('tab', tab);
      navigate({ search: queryParams.toString() });
    }
  }, [tab]);

  const onChangeTab = (nextTab: string) => {
    setTab(nextTab);

    const queryParams = new URLSearchParams(location.search);

    queryParams.set('tab', tab);
    navigate({ search: queryParams.toString() });
  };

  return (
    <div className={styles.root}>
      <Container maxWidth='xs' disableGutters>
        {(loading || sellState.loading) && <Loading fullScreen />}

        {selectedItem && (
          <ItemDialog
            isEquiped={isEquiped && selectedSlotId !== null}
            item={selectedItem}
            slotId={selectedSlotId}
            onClose={() => setSelectedItem(null)}
            onSell={sellState.handleSelectItem}
            sellingDisabled={sellingDisabled}
            onEquip={handleEquip}
            onUnEquip={handleUnEquip}
            loading={slotsApi.loading}
            noGas={noGas}
            coreBalance={coreBalance.data?.formatted}
            tradeBalance={tradeBalance.data?.formatted}
            confirmation={slotsApi.confirmation}
          />
        )}

        {/* Inventory */}
        <StatsDialog
          open={openStats}
          onClose={() => setOpenStats(false)}
          userSlots={slots.userSlots}
        />
        {!!selectedLockedCell && (
          <EmptyPocketDialog
            slotType={selectedLockedCell}
            noGas={noGas}
            coreBalance={coreBalance.data?.formatted}
            tradeBalance={tradeBalance.data?.formatted}
            onClose={() => setSelectedLockedCell(null)}
            onBuySlot={() => {
              slots.loadSlots();
              setSelectedLockedCell(null);
            }}
            coreSymbol={coreToken.data?.symbol}
            tradeSymbol={tradeToken?.symbol}
            tradeAddress={tradeToken?.address}
          />
        )}
        <ItemEquipDialog
          isOpen={!!selectedCell}
          onClose={() => setSelectedCell(null)}
          onEquip={handleEquip}
          // items={forEquipItems}
        />

        {/* Inventory */}
        <StatsDialog
          open={openStats}
          onClose={() => setOpenStats(false)}
          userSlots={slots.userSlots}
        />
        {/* Слоты */}
        <InventoryPreview
          className={styles.inventoryPreview}
          onReRequest={onReRequest}
          onEmptyClick={(type) => setSelectedCell(type)}
          onLockedClick={(slotId) => setSelectedLockedCell(slotId)}
          onItemClick={(item) => {
            setSelectedItem(item);
            setIsSlotSelected(!!item);
          }}
          onOpenStats={() => setOpenStats(true)}
          userItems={ownedNftState.data.rows}
          // slots
          slots={slots.slots}
          userSlots={slots.userSlots}
          loadingSlots={slots.loadingSlots}
          // handlers
          onEquip={handleEquip}
          onUnEquip={(item, slotId) => {
            setSelectedItem(item);
            setIsEquipped(true);
            setSelectedSlotId(slotId);
          }}
          additionalScreenEnabled={[Chain.SONGBIRD, Chain.SKALE_TEST, Chain.SKALE].includes(
            chainId
          )}
        />
        {/* Мой инвентарь */}
        <InventoryGrid
          className={styles.inventoryItems}
          // data
          tab={tab}
          // items={userItems}
          // total={userItems.length}
          items={ownedNftState.data.rows}
          total={ownedNftState.data.count}
          // chests
          chests={inventoryOldChests.chests}
          chestsLoading={inventoryOldChests.loading}
          chestActionLoading={inventoryOldChests.droppedLoading}
          onClaimChest={inventoryOldChests.select}
          // chests v2
          userChests={inventoryChestsV2.userChests.rows}
          userChestsLoading={inventoryChestsV2.loading}
          onClaimUserChest={inventoryChestsV2.select}
          //
          page={ownedNftState.page}
          onChangePage={ownedNftState.changePage}
          // handlers
          onChangeTab={(nextTab) => onChangeTab(nextTab)}
          onPreview={(item) => {
            Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
            setSelectedItem(item);
            setIsEquipped(false);
          }}
          isActiveFilter={isActiveFilter}
          handleOpenFilter={handleOpenFilter}
          recipes={
            FORGE_MINER_CLAIMER_CONTRACT_ID[account.chainId]
              ? {
                  isFetched: inventoryRecipes.isFetched,
                  loading: inventoryRecipes.loading,
                  rows: inventoryRecipes.data.rows,
                  count: inventoryRecipes.data.count,
                }
              : undefined
          }
        />
        {/* MarketPlace */}
        {!sellState.success && sellState.sellingNft && (
          <MarketSellDialog
            item={sellState.sellingNft}
            onSell={sellState.handleSellConfirm}
            onClose={sellState.handleCloseSell}
            fee={marketplace.fee}
            publicationFee={marketplace.publicationFee}
          />
        )}
        {!sellState.success && sellState.sellingNft && sellState.price && (
          <MarketSellConfirmDialog
            onClose={sellState.handleCloseSellConfirm}
            price={sellState.price}
            item={sellState.sellingNft}
            onSell={sellState.handleSell}
            disabled={sellingDisabled}
          />
        )}
        {sellState.success && (
          <MarketSuccessSellDialog onClose={sellState.handleCloseSuccessSell} />
        )}

        {/** Inventory **/}
        {openFilters && (
          <InventoryFilterDialog
            filters={filters}
            handleChangeFilter={handleChangeFilter}
            onClose={() => setOpenFilters(false)}
            reset={() => setFilters(initialFilters)}
            onChange={handleChangeFilters}
          />
        )}

        {/** Chest **/}
        <OpenChestDialog
          open={!!inventoryOldChests.selected}
          userChest={inventoryOldChests.selected}
          onClose={inventoryOldChests.clearSelected}
          onClaim={inventoryOldChests.handleClaim}
          loading={inventoryOldChests.actionLoading}
          noGas={noGas}
        />
        <OpenChestDialog
          open={!!inventoryChestsV2.selected}
          userChest={inventoryChestsV2.selected}
          onClose={inventoryChestsV2.clearSelected}
          onClaim={inventoryChestsV2.handleClaim}
          loading={inventoryChestsV2.actionLoading}
          noGas={noGas}
          confirmation={inventoryChestsV2.confirmation}
        />

        {!!inventoryOldChests.droppedNft && (
          <ChestDropDialog
            nftItem={inventoryOldChests.droppedNft}
            onClose={inventoryOldChests.clearDropped}
          />
        )}
        {!!(inventoryChestsV2.droppedNft || inventoryChestsV2.droppedToken) && (
          <ChestDropDialog
            nftItem={inventoryChestsV2.droppedNft}
            token={inventoryChestsV2.droppedToken}
            onClose={inventoryChestsV2.clearDropped}
            confirmation={inventoryChestsV2.confirmation}
          />
        )}

        <InventoryTutorial start={isInventoryTutorialStart(gasBalance, inventoryChestsV2)} />
      </Container>
    </div>
  );
};

export default Inventory;
