import React from 'react';
import Inventory from './inventory.tsx';
import NftProvider from 'lib/NFT/context/Nft.provider.tsx';
import MarketplaceProvider from 'lib/Marketplace/context/Marketplace.provider.tsx';
import SlotsProvider from 'lib/Inventory/context/slots/Slots.provider.tsx';

function InventoryWrap() {
  return (
    <MarketplaceProvider>
      <NftProvider>
        <SlotsProvider>
          <Inventory />
        </SlotsProvider>
      </NftProvider>
    </MarketplaceProvider>
  );
}

export default InventoryWrap;
