import { IQuestRewardContextMap, QuestRewardType } from 'lib/Quests/api-server/quests.types.ts';

function formatChestId(chestId?: number | string): string | undefined {
  if (typeof chestId === 'number') {
    return 'NFT box';
  }
  if (chestId === 'mystery') {
    return 'Mystery Box';
  }
  if (chestId === 'pixel') {
    return 'Pixel Box';
  }
  if (chestId === 'pixel_mystery') {
    return 'Pixel & Myst. Box';
  }
}

export function questRewardToLocale<T extends QuestRewardType>(
  type: T,
  context: IQuestRewardContextMap[T]
) {
  switch (type) {
    case QuestRewardType.Pixel: {
      const pixelCtx = context as IQuestRewardContextMap[QuestRewardType.Pixel];
      return [
        pixelCtx.amount && `${pixelCtx.amount} ${pixelCtx.symbol || 'coins'}`,
        formatChestId(pixelCtx.chestId),
      ]
        .filter(Boolean)
        .join(', ');
    }
    case QuestRewardType.Gassless: {
      return `${(context as IQuestRewardContextMap[QuestRewardType.Gassless]).amount} Free claim`;
    }
    case QuestRewardType.Gas: {
      const gasCtx = context as IQuestRewardContextMap[QuestRewardType.Gas];

      return `${gasCtx.amount || '?'} ${gasCtx.symbol}`;
    }
    case QuestRewardType.Coin: {
      const coinCtx = context as IQuestRewardContextMap[QuestRewardType.Coin];
      return `${coinCtx.amountType === 'upTo' ? 'up to ' : ''}${coinCtx.amount} ${coinCtx.symbol}`;
    }
  }
}
