export function formatQuestTaskLink(link: string, opts: { address: string; telegramId?: number }) {
  if (link.startsWith('https://t.me/flarexgamebot')) {
    const str = `pixel_${opts.address}`;

    return `${link}?start=${str}`;
  }
  if (link.startsWith('https://invite.daopeople.io')) {
    return `${link}&tg_id=${opts.telegramId}`;
  }

  return link.replace(':address', opts.address);
}
