import { IQuestDto, IQuestTaskDto, QuestTaskType } from 'lib/Quests/api-server/quests.types.ts';

export function localQuestChecked(key: string): void {
  localStorage.setItem(`pixel-quest-${key}`, '1');
}

export function localQuestIsChecked(key: string): boolean {
  return localStorage.getItem(`pixel-quest-${key}`) === '1';
}

export function isAllLocalQuestTasks(quest: IQuestDto): boolean {
  return quest.tasks.every((task) => isLocalQuestTask(task));
}

export function isLocalQuestTask(task: IQuestTaskDto): boolean {
  return [
    QuestTaskType.TwitterChannel,
    QuestTaskType.SubscribeYoutube,
    QuestTaskType.SwitchNetwork,
    QuestTaskType.NoopLink,
  ].includes(task.type);
}

export function isQuestLocalTasksCompleted(quest: IQuestDto): boolean {
  const allLocalTasksCompleted = quest.tasks
    .filter(isLocalQuestTask)
    .every((task) => localQuestIsChecked(task.id));

  return allLocalTasksCompleted;
}
