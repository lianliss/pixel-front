import { default as QuestsPage } from './QuestsPage.tsx';
import { TutorialForGettingGasProvider } from 'app/widget/tutorials/TutorialForGettingGas/TutorialForGettingGas';

import React from 'react';

function QuestsPageWrap() {
  return (
    <TutorialForGettingGasProvider>
      <QuestsPage />
    </TutorialForGettingGasProvider>
  );
}

export default QuestsPageWrap;
