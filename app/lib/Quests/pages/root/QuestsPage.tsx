import React, { useMemo } from 'react';
import styles from './Quests.module.scss';
import { useUnit } from 'effector-react';
import {
  checkQuest,
  loadQuests,
  loadQuestUserStats,
  loadUserQuests,
  questUserStatsStore,
  userQuestsStore,
} from 'lib/Quests/api-server/quests.store.ts';
import QuestRow from '../../components/QuestRow/QuestRow.tsx';
import Divider from 'ui/Divider';
import Tabs from '@mui/material/Tabs';
import { useDelayedDisabled } from 'utils/hooks/useDelayedDisabled';
import QuestDialog from '../../components/QuestDialog/index.ts';
import { IQuestDto } from 'lib/Quests/api-server/quests.types.ts';
import {
  isAllLocalQuestTasks,
  isQuestLocalTasksCompleted,
} from 'lib/Quests/utils/local-quest-check.ts';
import Chain from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import toaster from 'services/toaster.tsx';
import { useLocation, useMatch, useNavigate } from 'react-router-dom';
import routes from 'const/routes.tsx';
import get from 'lodash/get';
import LinearProgress from '@mui/material/LinearProgress';
import { Tab } from '@ui-kit/Tab/Tab.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTranslation } from 'react-i18next';
import { tutorialQuestId } from '../../../../widget/tutorials/config.tsx';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import { $tutorialForGettingGasState } from '../../../../widget/tutorials/TutorialForGettingGas/tutorialForGettingGasState.ts';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';
import { IS_TELEGRAM } from '@cfg/config.ts';

type ICategory = 'public' | 'events' | 'partners' | 'influencers';

function QuestsPage() {
  const account = useAccount();
  const userInfo = useSelfUserInfo();

  const location = useLocation();
  const navigate = useNavigate();

  const { t } = useTranslation('quests');
  const { t: n } = useTranslation('notifications');

  const queryParams = new URLSearchParams(location.search);
  const onlyCompletedParam = queryParams.get('onlyCompleted') === 'true';
  const { chainId, isConnected, accountAddress } = account;

  const routePath = routes.walletQuestsTab.path;
  const match = useMatch(routePath);
  const routeTab = get(match, 'params.tab');

  const questsByCategory = useUnit(userQuestsStore);
  const userStats = useUnit(questUserStatsStore);
  const checkLoading = useUnit(checkQuest.pending);
  const loading = useUnit(loadQuests.pending);

  const delayedDisabled = useDelayedDisabled(5000);
  const [category, setCategory] = React.useState<ICategory>(routeTab || 'public');
  const [onlyCompleted, setOnlyCompleted] = React.useState(onlyCompletedParam);
  const [selectedQuest, setSelectedQuest] = React.useState<IQuestDto | null>(null);

  const state = questsByCategory[category] || { rows: [], count: 0 };

  const completedRows = useMemo(() => {
    return state.rows.filter((el) => el.completedAt);
  }, [state.rows]);

  const rows = React.useMemo(() => {
    if (onlyCompleted) {
      return state.rows.filter((el) => el.completedAt).filter((el) => el.id !== tutorialQuestId);
    } else {
      return state.rows.filter((el) => !el.completedAt).filter((el) => el.id !== tutorialQuestId);
    }
  }, [state.rows, onlyCompleted]);

  const allCompleted = React.useMemo(
    () => state.rows.every((el) => !!el.completedAt),
    [state.rows]
  );

  const uncompletedByTabs: Partial<Record<ICategory, number>> = React.useMemo(() => {
    return (
      userStats?.options.reduce(
        (acc, el) => ({
          ...acc,
          [el.category]: el.amount,
        }),
        { public: 0 }
      ) || {}
    );
  }, [userStats]);

  const callLoadQuests = () => {
    loadUserQuests({ chainId: chainId.toString(), address: accountAddress, category })
      .then()
      .catch((e) => {
        toaster.captureException(e, n('Quests loading failed'));
      });

    loadQuestUserStats({ chainId, address: accountAddress })
      .then()
      .catch((e) => {
        toaster.captureException(e, n('Quests stats loading failed'));
      });
  };

  React.useEffect(() => {
    setOnlyCompleted(onlyCompletedParam);
  }, [onlyCompletedParam]);

  React.useEffect(() => {
    if (chainId && accountAddress) {
      callLoadQuests();
    }
  }, [chainId, accountAddress, category]);

  React.useEffect(() => {
    if (selectedQuest) {
      const item = state.rows.find((q) => q.id === selectedQuest.id);

      if (item.completedAt !== selectedQuest.completedAt) {
        setSelectedQuest(item);
      }
    }
  }, [state.rows, selectedQuest]);

  useBackAction('/wallet');

  const setupQuest = (quest: IQuestDto) => {
    setSelectedQuest(quest);
  };

  const handleCheck = async (quest: IQuestDto, context: object) => {
    delayedDisabled.call();

    if (isAllLocalQuestTasks(quest) && !isQuestLocalTasksCompleted(quest)) {
      return;
    }

    await checkQuest({ questId: quest.id, address: accountAddress, context })
      .then(callLoadQuests)
      .catch((e) => {
        (toaster as any).captureException(e, n('Check quest failed'));
      });
  };

  const changeCompletedFilter = () => {
    const newValue = !onlyCompleted;
    setOnlyCompleted(newValue);
    queryParams.set('onlyCompleted', newValue.toString());
    navigate({ search: queryParams.toString() });
  };

  const disabledCheck = !accountAddress || delayedDisabled.disabled;

  const tutorialQuest = React.useMemo(() => {
    const completedTargetQuest = completedRows.find((row) => row.id === tutorialQuestId);
    const unCompletedTargetQuest = state.rows.find((row) => row.id === tutorialQuestId);
    const quest = completedTargetQuest || unCompletedTargetQuest;

    if (quest) {
      return (
        <QuestRow
          quest={quest}
          onSelected={setupQuest}
          disabled={quest.disabled}
          tutorialId='#CompleteQuestScreenTarget'
        />
      );
    }

    return null;
  }, [completedRows, tutorialQuestId, rows, setupQuest]);

  return (
    <div className={styles.root}>
      <Container maxWidth='xs'>
        <Chain />

        <h1 className={styles.title}>{t('Quests')}</h1>

        <p className={styles.summary}>
          {t('Complete quests and receive gasless transactions to claim Pixel Shards')}
        </p>

        {onlyCompleted && (
          <div className={styles.btn__wrapper}>
            <Button
              size='large'
              variant='contained'
              color='primary'
              fullWidth
              onClick={changeCompletedFilter}
            >
              {t('Back to Incompleted Quests')}
            </Button>
          </div>
        )}

        <Paper variant='outlined' elevation={0} className={styles.paper}>
          <Tabs
            onChange={(_, nextTab) => setCategory(nextTab as ICategory)}
            value={category}
            variant='scrollable'
            scrollButtons='auto'
          >
            <Tab
              value='public'
              label={t('General')}
              count={onlyCompleted ? undefined : uncompletedByTabs.public || undefined}
            />
            <Tab
              value='events'
              label={t('Events')}
              count={onlyCompleted ? undefined : uncompletedByTabs.events || undefined}
            />
            <Tab
              value='partners'
              label={t('Partners')}
              count={onlyCompleted ? undefined : uncompletedByTabs.partners || undefined}
            />
            <Tab
              value='influencers'
              label={t('Influencers')}
              count={onlyCompleted ? undefined : uncompletedByTabs.influencers || undefined}
            />
          </Tabs>

          <Divider />

          {loading ? <LinearProgress /> : <div className={styles.loaderPlaceholder} />}

          <div className={styles.list}>
            {$tutorialForGettingGasState.getState().isVisible && (
              <>
                {tutorialQuest}
                <Divider />
              </>
            )}

            {rows.map((row, i) => (
              <React.Fragment key={`quest-${row.id}`}>
                {i > 0 && <Divider />}
                <QuestRow quest={row} onSelected={setupQuest} disabled={row.disabled} />
              </React.Fragment>
            ))}

            {!loading && !rows.length && (
              <span style={{ padding: '32px 16px' }}>{t('No Quests')}</span>
            )}

            {completedRows.length > 0 && !onlyCompleted && (
              <div className={styles.btn__wrapper}>
                <Button
                  size='large'
                  variant='outlined'
                  color='primary'
                  fullWidth
                  onClick={changeCompletedFilter}
                >
                  {t('Completed Quests')}
                </Button>
              </div>
            )}
          </div>
        </Paper>

        <QuestDialog
          open={!!selectedQuest}
          quest={selectedQuest}
          address={accountAddress}
          onClose={() => {
            setSelectedQuest(null);
          }}
          onCheck={handleCheck}
          disabled={disabledCheck}
          loading={delayedDisabled.disabled}
          telegramId={userInfo.data?.telegramId}
        />

        {/*{(IS_DEVELOP || IS_STAGE) && <TutorialMining />}*/}
      </Container>
    </div>
  );
}

export default QuestsPage;
