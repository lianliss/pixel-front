import React, { useEffect, useState } from 'react';
import { AdminLayout } from 'lib/AdminV5/components/AdminLayout/AdminLayout.tsx';
import { usePagination } from 'utils/hooks/usePagination.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { QuestTable } from 'lib/Quests/components/_admin/QuestTable.tsx';
import { useQuests } from 'lib/Quests/hooks/useQuests.ts';
import QuestFormDialog from 'lib/Quests/components/_admin/QuestFormDialog.tsx';
import { IQuestDto } from 'lib/Quests/api-server/quests.types.ts';
import { useQuestApi } from 'lib/Quests/hooks/useQuestApi.ts';
import { IQuestFormState } from 'lib/Quests/components/_admin/QuestForm.tsx';

function QuestAdminPage() {
  const account = useAccount();

  const pagination = usePagination({ step: 10 });
  const quests = useQuests({
    pagination,
    chainId: account.chainId,
  });
  const questApi = useQuestApi({
    onCreate: () => quests.refetch(),
    onUpdate: () => quests.refetch(),
  });

  const [quest, setQuest] = useState<IQuestDto | null>(null);
  const [create, setCreate] = useState<boolean>(false);

  const handleSave = async (state: IQuestFormState) => {
    if (create) {
      await questApi.create({
        adminAddress: account.accountAddress,
        title: state.title,
        category: state.category,
        image: state.image || undefined,
        dailyChest: true,
        chainId: account.chainId,
        limitUsers: state.limitUsers,
        rewards: state.rewards,
        tasks: state.tasks,
        fromDate: state.fromDate?.getTime() || null,
        severity: state.severity,
        position: state.position,
      });
      setCreate(false);
    } else {
      await questApi.update({
        id: quest.id,
        adminAddress: account.accountAddress,
        title: state.title,
        category: state.category,
        image: state.image || undefined,
        dailyChest: true,
        limitUsers: state.limitUsers,
        chainId: account.chainId,
        fromDate: state.fromDate?.getTime() || null,
        rewards: state.rewards,
        tasks: state.tasks,
        severity: state.severity,
        position: state.position,
      });
    }

    await quests.refetch();
  };
  const handleDisable = (disable: boolean) => async (quest: IQuestDto) => {
    await questApi.disable({
      disabled: disable,
      questId: quest.id,
      adminAddress: account.accountAddress,
    });
    await quests.refetch();
  };
  const handleDelete = async (quest: IQuestDto) => {
    await questApi.delete({
      deleted: true,
      questId: quest.id,
      adminAddress: account.accountAddress,
    });
    await quests.refetch();
  };

  // useEffect(() => {
  //   setQuest(quests.data?.rows[0] || null);
  // }, [quests.data]);
  useEffect(() => {
    if (quests.data?.rows && quest) {
      const renewQuest = quests.data?.rows?.find((el) => el.id === quest.id);
      setQuest(renewQuest);
    }
  }, [quest, quests.data]);

  return (
    <AdminLayout>
      <QuestTable
        loading={quests.loading}
        rows={quests.data?.rows || []}
        count={quests.data?.count || 0}
        pagination={pagination}
        onSelect={(v) => setQuest(v)}
        onCreate={() => setCreate(true)}
      />
      <QuestFormDialog
        open={!!quest || create}
        quest={quest}
        loading={questApi.loading}
        onClose={() => {
          setQuest(null);
          setCreate(false);
        }}
        onStart={handleDisable(false)}
        onStop={handleDisable(true)}
        onSave={handleSave}
        onDelete={handleDelete}
      />
    </AdminLayout>
  );
}

export default QuestAdminPage;
