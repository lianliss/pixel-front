import React, { useEffect, useRef, useState } from 'react';
import styles from './QuestTaskRow.module.scss';
import {
  IQuestTaskContextMap,
  IQuestTaskDto,
  QuestTaskType,
} from 'lib/Quests/api-server/quests.types.ts';
import clsx from 'clsx';
import ChevronRight from 'lib/Quests/components/icons/ChevronRight';
import { formatQuestTaskLink } from 'lib/Quests/utils/formatQuestTaskLink.ts';
import {
  isLocalQuestTask,
  localQuestChecked,
  localQuestIsChecked,
} from 'lib/Quests/utils/local-quest-check.ts';
import { useSwitchChain } from '@chain/hooks/useSwitchChain.ts';
import { useTranslation } from 'react-i18next';
import { openLink } from 'utils/open-link.ts';
import toaster from 'services/toaster.tsx';

type Props = {
  task: IQuestTaskDto;
  address?: string;
  telegramId?: number;
  completed?: boolean;
};

function QuestTaskRow(props: Props) {
  const { task, address, telegramId, completed } = props;

  const { t } = useTranslation('quests');

  const { switchToChain } = useSwitchChain();
  const [localCompleted, setLocalCompleted] = useState(false);
  const isCompleted = !!task.completedAt || completed || localCompleted;
  const link = task.link ? formatQuestTaskLink(task.link, { address, telegramId }) : undefined;
  const isSwitchNetwork = task.type === QuestTaskType.SwitchNetwork;
  const isChevron = Boolean(link || isSwitchNetwork);

  const windowHiddenTime = useRef({});

  useEffect(() => {
    if (!task.completedAt && isLocalQuestTask(task)) {
      setLocalCompleted(localQuestIsChecked(task.id));

      const onFocus = function () {
        if (windowHiddenTime.current[task.id]) {
          const hiddenTime = Date.now() - windowHiddenTime.current[task.id];

          if (hiddenTime >= 5 * 1000) {
            localQuestChecked(task.id);
            toaster.success('Task completed');
            setLocalCompleted(true);
          } else {
            toaster.warning('Task does not completed');
          }
        }
      };
      window.onfocus = onFocus;

      const onBlur = function () {
        windowHiddenTime.current[task.id] = Date.now();
      };
      window.onblur = onBlur;

      return () => {
        window.onfocus = undefined;
        window.onblur = undefined;
        // delete windowHiddenTime.current[task.id];
      };
    }

    return () => {};
  }, [task.id]);

  const redirect = async () => {
    if (isSwitchNetwork) {
      const switchCtx = task.context as IQuestTaskContextMap[QuestTaskType.SwitchNetwork];
      if (switchCtx.chainId) {
        await switchToChain(switchCtx.chainId);
      }
      return;
    }

    if (!address || !link) {
      return;
    }

    if (isLocalQuestTask(task)) {
      localQuestChecked(task.id);
    }

    openLink(link);
  };

  return (
    <div
      className={clsx(styles.root)}
      onClick={redirect}
      data-link={link}
      data-switch={isSwitchNetwork + ''}
    >
      <img src={task.image} alt='' className={styles.image} />

      <div className={styles.content}>
        <span className={styles.title}>{task.title}</span>
        <div className={styles.prizeBox}>
          <span
            id={isCompleted ? 'completedQuest' : 'unCompletedQuest'}
            className={clsx(styles.completedText, { [styles.completed]: isCompleted })}
          >
            {isCompleted ? t('Completed') : t('Incompleted')}
          </span>
        </div>
      </div>

      {isChevron && <ChevronRight className={styles.icon} />}
    </div>
  );
}

export default QuestTaskRow;
