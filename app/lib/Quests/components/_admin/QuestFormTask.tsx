import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import {
  ICreateQuestTaskDto,
  IQuestTaskContextMap,
  QuestTaskType,
} from 'lib/Quests/api-server/quests.types.ts';
import { Button } from '@ui-kit/Button/Button.tsx';
import React from 'react';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Stack from '@mui/material/Stack';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import ImageUrlField from '@ui-kit/ImageUrlField/ImageUrlField.tsx';
import { isValidUrl } from 'utils/validate/url.ts';
import NumberField from '@ui-kit/NumberField/NumberField.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';

type Props = {
  task: ICreateQuestTaskDto;
  onRemove?: (task: ICreateQuestTaskDto) => void;
  onChange?: (task: ICreateQuestTaskDto) => void;
  disabled?: boolean;
};

export function QuestFormTask(props: Props) {
  const { task, onRemove, onChange, disabled } = props;

  const account = useAccount();

  const disableFields = disabled;
  const isInvalidImage = !isValidUrl(task.image);
  const isInvalidLink = task.link && !isValidUrl(task.link);
  const isInvalidTask = !!(
    task.type == QuestTaskType.DrillLevel &&
    +Number.isNaN(+(task.context as IQuestTaskContextMap[QuestTaskType.DrillLevel]).level)
  );

  return (
    <Accordion variant='outlined' disableGutters defaultExpanded>
      <AccordionSummary>
        <Typography>{task.title}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Stack direction='column' gap={2}>
          <Stack direction='row' gap={2}>
            <TextField
              fullWidth
              label='Title'
              placeholder='Enter title'
              value={task.title}
              onChange={(e) => onChange?.({ ...task, title: e.target.value })}
              required
              disabled={disableFields}
            />

            <ImageUrlField
              required
              fullWidth
              label='Image'
              placeholder='Enter url'
              value={task.image}
              error={isInvalidImage}
              onChange={(e) => onChange?.({ ...task, image: e.target.value })}
              disabled={disableFields}
            />

            <Box
              sx={{
                width: '100%',
                display: 'flex',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}
            >
              <Button
                variant='outlined'
                color='error'
                disabled={!onRemove}
                onClick={(e) => {
                  onRemove?.(task);
                }}
              >
                Delete
              </Button>
            </Box>
          </Stack>

          <Stack direction='row' gap={2}>
            <FormControl fullWidth required disabled={disableFields}>
              <InputLabel id='demo-simple-select-label'>Type</InputLabel>
              <Select
                label='Type'
                required
                value={task.type}
                onChange={(e) =>
                  onChange?.({ ...task, context: {}, type: e.target.value as QuestTaskType })
                }
              >
                {[
                  QuestTaskType.NoopLink,
                  QuestTaskType.TelegramChat,
                  QuestTaskType.DrillLevel,
                  QuestTaskType.StorageLevel,
                ].map((el) => (
                  <MenuItem key={el} value={el}>
                    {el}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            {[QuestTaskType.DrillLevel, QuestTaskType.StorageLevel].includes(task.type) ? (
              <div style={{ width: '100%' }} />
            ) : (
              <TextField
                fullWidth
                label='Link'
                placeholder='Enter link'
                value={task.link}
                error={isInvalidLink}
                onChange={(e) => onChange?.({ ...task, link: e.target.value })}
                // required
                disabled={disableFields}
              />
            )}
          </Stack>

          {task.type === QuestTaskType.TelegramChat && (
            <Stack direction='row' gap={2}>
              <TextField
                fullWidth
                label='Chat ID'
                placeholder='Enter chat ID'
                value={(task.context as any).chatId?.replace('@', '') || ''}
                InputProps={{ startAdornment: <Box sx={{ pr: 0.5 }}>@</Box> }}
                onChange={(e) => onChange?.({ ...task, context: { chatId: `@${e.target.value}` } })}
                required
                disabled={disableFields}
              />
              <div style={{ width: '100%' }} />
            </Stack>
          )}
          {[QuestTaskType.DrillLevel, QuestTaskType.StorageLevel].includes(task.type) && (
            <Stack direction='row' gap={2}>
              <NumberField
                fullWidth
                label='Drill Level'
                placeholder='Enter drill level'
                value={(task.context as any).level || ''}
                onChange={(e) =>
                  onChange?.({
                    ...task,
                    context: { level: Number(e.target.value), chainId: account.chainId },
                  })
                }
                required
                error={isInvalidTask}
                helperText={isInvalidTask ? `Invalid drill level` : undefined}
                disabled={disableFields}
              />
              <div style={{ width: '100%' }} />
            </Stack>
          )}
        </Stack>
      </AccordionDetails>
    </Accordion>
  );
}
