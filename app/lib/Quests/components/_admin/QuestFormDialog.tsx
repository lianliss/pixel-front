import React from 'react';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import { DialogTitle } from '@mui/material';
import { IQuestDto } from 'lib/Quests/api-server/quests.types.ts';
import DialogContent from '@mui/material/DialogContent';
import { IQuestFormState, QuestForm } from 'lib/Quests/components/_admin/QuestForm.tsx';

type Props = DialogProps & {
  quest?: IQuestDto;
  loading?: boolean;
  onSave?: (state: IQuestFormState) => void;
  onStart?: (quest: IQuestDto) => void;
  onStop?: (quest: IQuestDto) => void;
  onDelete?: (quest: IQuestDto) => void;
};

function QuestFormDialog(props: Props) {
  const { onStart, onStop, onDelete, quest, loading, onClose, open, onSave, ...other } = props;

  return (
    <Dialog open={open} onClose={onClose} {...other} fullWidth maxWidth='lg' scroll='body'>
      <DialogTitle sx={{ overflow: 'hidden' }}>
        {quest ? `Edit ${quest.id}` : 'Create Quest'}
      </DialogTitle>
      <DialogContent>
        <QuestForm
          onSave={onSave}
          onStart={quest && (() => onStart?.(quest))}
          onStop={quest && (() => onStop?.(quest))}
          onDelete={quest && (() => onDelete?.(quest))}
          initialData={quest}
          loading={loading}
        />
      </DialogContent>
    </Dialog>
  );
}

export default QuestFormDialog;
