import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import { ICreateQuestRewardDto, QuestRewardType } from 'lib/Quests/api-server/quests.types.ts';
import { Button } from '@ui-kit/Button/Button.tsx';
import React from 'react';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Stack from '@mui/material/Stack';
import NumberField from '@ui-kit/NumberField/NumberField.tsx';
import { SelectInputProps } from '@mui/material/Select/SelectInput';
import { useToken } from '@chain/hooks/useToken.ts';
import { OutlinedInputProps } from '@mui/material/OutlinedInput';
import { useAccount } from '@chain/hooks/useAccount.ts';

type Props = {
  reward: ICreateQuestRewardDto;
  onRemove?: (reward: ICreateQuestRewardDto) => void;
  onChange?: (reward: ICreateQuestRewardDto) => void;
  disabled?: boolean;
};

export function QuestFormReward(props: Props) {
  const { reward, onRemove, onChange, disabled } = props;

  const account = useAccount();
  const gasToken = useToken({});
  const gasSymbol = gasToken.data?.symbol;
  const rewardSymbol = reward.context?.symbol;

  const disableFields = disabled;

  const handleChangeType: SelectInputProps['onChange'] = (e) => {
    const nextType = e.target.value;

    switch (nextType) {
      case QuestRewardType.Pixel: {
        onChange?.({
          ...reward,
          type: nextType,
          execute: true,
          context: { ...reward.context, symbol: 'PXLs', chainId: account.chainId },
        });
        break;
      }
      case QuestRewardType.Gas: {
        onChange?.({
          ...reward,
          type: nextType,
          execute: true,
          context: { ...reward.context, symbol: gasSymbol, chainId: account.chainId },
        });
        break;
      }
      case QuestRewardType.Chest: {
        onChange?.({
          ...reward,
          type: nextType,
          execute: true,
          context: { chestId: 0, chainId: account.chainId },
        });
        break;
      }
    }
  };
  const handleChangeAmount: OutlinedInputProps['onChange'] = (e) => {
    onChange?.({
      ...reward,
      context: { ...reward.context, amount: +e.target.value },
    });
  };

  return (
    <Accordion variant='outlined' disableGutters defaultExpanded>
      <AccordionSummary>
        <Typography>{reward.type}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Stack direction='column' gap={2}>
          <Stack direction='row' gap={2}>
            <FormControl fullWidth required disabled={disableFields || !gasSymbol}>
              <InputLabel id='demo-simple-select-label'>Type</InputLabel>
              <Select label={`Type`} required value={reward.type} onChange={handleChangeType}>
                {[QuestRewardType.Pixel, QuestRewardType.Gas, QuestRewardType.Chest].map((el) => (
                  <MenuItem key={el} value={el}>
                    {el}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <Box
              sx={{
                width: '100%',
                display: 'flex',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}
            >
              <Button
                variant='outlined'
                color='error'
                disabled={!onRemove}
                onClick={(e) => {
                  onRemove?.(reward);
                }}
              >
                Delete
              </Button>
            </Box>
          </Stack>

          <Stack direction='row' gap={2}>
            {[QuestRewardType.Gas, QuestRewardType.Pixel].includes(reward.type) && (
              <NumberField
                fullWidth
                label='Tokens Amount'
                placeholder='Enter tokens amount'
                value={reward.context.amount}
                onChange={handleChangeAmount}
                required
                InputProps={{ endAdornment: rewardSymbol }}
                disabled={disableFields}
              />
            )}
            {/*{[QuestRewardType.Chest].includes(reward.type) && <NumberField*/}
            {/*    fullWidth*/}
            {/*    label='Chest ID'*/}
            {/*    placeholder='Enter chest ID'*/}
            {/*    value={reward.context.amount}*/}
            {/*    onChange={handleChangeAmount}*/}
            {/*    required*/}
            {/*    disabled={disableFields}*/}
            {/*/>}*/}
          </Stack>
        </Stack>
      </AccordionDetails>
    </Accordion>
  );
}
