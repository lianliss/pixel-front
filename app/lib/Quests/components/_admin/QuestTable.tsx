import Paper from '@mui/material/Paper';
import TableContainer from '@mui/material/TableContainer';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableCell from '@mui/material/TableCell';
import TableBody from '@mui/material/TableBody';
import React from 'react';
import { BountyEntryStatus, IBountyUserEntryDto } from 'lib/Bounty/api-server/bounty.types.ts';
import { TablePagination, TableRow } from '@mui/material';
import { IPaginationResult } from 'utils/hooks/usePagination.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Link } from 'react-router-dom';
import { Button } from '@ui-kit/Button/Button.tsx';
import { IQuestDto } from 'lib/Quests/api-server/quests.types.ts';
import { Img } from '@ui-kit/Img/Img.tsx';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import Box from '@mui/material/Box';

type Props = {
  rows: IQuestDto[];
  count: number;
  pagination?: IPaginationResult;
  onSelect?: (row: IQuestDto) => void;
  onCreate?: () => void;
  loading?: boolean;
};

export function QuestTable(props: Props) {
  const {
    rows = [] as IQuestDto[],
    count = 0,
    pagination,
    onSelect,
    onCreate,
    loading = false,
  } = props;

  return (
    <Paper variant='outlined' sx={{ overflow: 'hidden' }}>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell sx={{ width: '70px' }}>
                <Button variant='outlined' color='primary' disabled={!onCreate} onClick={onCreate}>
                  Create
                </Button>
              </TableCell>
              <TableCell sx={{ width: '60px' }}>ID</TableCell>
              <TableCell sx={{ width: '100px' }}>Title</TableCell>
              <TableCell sx={{ width: '100px' }}>Category</TableCell>
              <TableCell sx={{ width: '7px' }}>Daily</TableCell>
              <TableCell sx={{ width: '100px' }}>Limit</TableCell>
              <TableCell sx={{ width: '100px' }}>Status</TableCell>
              <TableCell sx={{ width: '60px' }}>Tasks</TableCell>
              <TableCell sx={{ width: '60px' }}>Rewards</TableCell>
              <TableCell sx={{ width: '100px' }}>From Date</TableCell>
              <TableCell sx={{ width: '100px' }}>Created</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={`row-${row.id}`}>
                <TableCell>
                  <Button
                    variant='outlined'
                    color='primary'
                    disabled={!onSelect}
                    onClick={() => onSelect?.(row)}
                  >
                    Edit
                  </Button>
                </TableCell>
                <TableCell>
                  <Box sx={{ display: 'flex', gap: 1, alignItems: 'center' }}>
                    <Avatar variant='circular' variantStyle='outlined' src={row.image} /> {row.id}
                  </Box>
                </TableCell>
                <TableCell>{row.title}</TableCell>
                <TableCell>{row.category}</TableCell>
                <TableCell>{row.dailyChest ? 'yes' : '-'}</TableCell>
                <TableCell>
                  <Typography
                    variant='subtitle2'
                    color={
                      row.limitUsers && row.currentUsers >= row.limitUsers
                        ? 'error.main'
                        : undefined
                    }
                  >
                    {row.limitUsers ? `${row.currentUsers}/${row.limitUsers}` : '-'}
                  </Typography>
                </TableCell>
                <TableCell>
                  {row.deleted ? (
                    <Typography color='error.main'>Deleted</Typography>
                  ) : row.disabled ? (
                    <Typography color='warning.main'>Disabled</Typography>
                  ) : (
                    <Typography color='success.main'>Active</Typography>
                  )}
                </TableCell>
                <TableCell>{row.tasks?.length}</TableCell>
                <TableCell>{row.rewards?.length}</TableCell>
                <TableCell>
                  <Typography variant='caption'>
                    {row.fromDate ? new Date(row.fromDate).toLocaleString('ru-RU') : '-'}
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography variant='caption'>
                    {new Date(row.createdAt).toLocaleString('ru-RU')}
                  </Typography>
                </TableCell>
              </TableRow>
            ))}
            {!loading && !rows.length && (
              <TableRow>
                <TableCell colSpan={11}>No data</TableCell>
              </TableRow>
            )}
            {loading && !rows.length && (
              <TableRow>
                <TableCell colSpan={11}>Loading...</TableCell>
              </TableRow>
            )}
          </TableBody>

          <TablePagination
            count={count}
            page={(pagination?.page || 1) - 1}
            rowsPerPage={pagination?.step || 20}
            onPageChange={(e, v) => {
              pagination?.setPage(v + 1);
            }}
            rowsPerPageOptions={[20]}
          />
        </Table>
      </TableContainer>
    </Paper>
  );
}
