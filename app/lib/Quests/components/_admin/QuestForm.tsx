import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import React, { useEffect, useState } from 'react';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import ImageUrlField from '@ui-kit/ImageUrlField/ImageUrlField.tsx';
import Stack from '@mui/material/Stack';
import { Button } from '@ui-kit/Button/Button.tsx';
import {
  ICreateQuestRewardDto,
  ICreateQuestTaskDto,
  IQuestDto,
  QuestRewardType,
  QuestSeverity,
  QuestTaskType,
} from 'lib/Quests/api-server/quests.types.ts';
import { ButtonGroup } from '@ui-kit/ButtonGroup/ButtonGroup.tsx';
import NumberField from '@ui-kit/NumberField/NumberField.tsx';
import { QuestFormTask } from 'lib/Quests/components/_admin/QuestFormTask.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Divider from '@mui/material/Divider';
import { QuestFormReward } from 'lib/Quests/components/_admin/QuestFormReward.tsx';
import { isValidUrl } from 'utils/validate/url.ts';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers';
import dayjs from 'dayjs';

export type IQuestFormState = {
  title: string;
  image: string;
  category: string;
  limitUsers?: number;
  position?: number;
  severity?: QuestSeverity;
  fromDate: Date | null;
  tasks: ICreateQuestTaskDto[];
  rewards: ICreateQuestRewardDto[];
};

type Props = {
  initialData?: IQuestDto;
  onSave?: (state: IQuestFormState) => void;
  loading?: boolean;
  disabled?: boolean;
  onStart?: () => void;
  onStop?: () => void;
  onDelete?: () => void;
};

export function QuestForm(props: Props) {
  const { onStart, onStop, onSave, onDelete, initialData, loading, disabled: disabledForm } = props;

  const [title, setTitle] = useState<string>('');
  const [position, setPosition] = useState<string>('');
  const [severity, setSeverity] = useState<QuestSeverity | '_'>('_');
  const [fromDate, setFromDate] = useState<any | null>(null);
  const [image, setImage] = useState<string>('https://storage.hellopixel.network/assets/quests/');
  const [limitUsers, setLimitUsers] = useState<string>('10000');
  const [category, setCategory] = useState<string>('public');
  const [tasks, setTasks] = useState<ICreateQuestTaskDto[]>([
    { title: 'Task 0', type: QuestTaskType.NoopLink, context: {} },
  ]);
  const [rewards, setRewards] = useState<ICreateQuestRewardDto[]>([
    {
      type: QuestRewardType.Pixel,
      context: { amount: 0.025, chainId: 19, symbol: 'PXLs' },
      execute: true,
    },
  ]);

  useEffect(() => {
    if (initialData) {
      setTitle(initialData.title);

      if (initialData.fromDate) {
        setFromDate(dayjs(initialData.fromDate));
      }

      setImage(initialData.image);
      setCategory(initialData.category);
      setPosition(initialData.position.toString());
      setSeverity(initialData.severity || '_');
      setImage(initialData.image);

      setLimitUsers(initialData.limitUsers?.toString() || '');
      setTasks(
        initialData.tasks.map((t) => ({
          title: t.title,
          context: t.context,
          type: t.type,
          image: t.image,
          link: t.link,
          summary: t.summary,
        }))
      );
      setRewards(
        initialData.rewards.map((t) => ({
          type: t.type,
          context: t.context,
          execute: t.execute,
        }))
      );
    } else {
      setTitle('');
      setSeverity('_');
      setPosition('');
      setImage('https://storage.hellopixel.network/assets/quests/');
      setCategory('public');
      setLimitUsers('10000');
      setTasks([{ title: 'Task 0', type: QuestTaskType.NoopLink, context: {} }]);
      setRewards([
        {
          type: QuestRewardType.Pixel,
          context: { amount: 0.025, chainId: 19, symbol: 'PXLs' },
          execute: true,
        },
      ]);
    }
  }, [initialData]);

  const handleSave = () => {
    onSave?.({
      title,
      category,
      image,
      limitUsers: +limitUsers || undefined,
      position: Number.isNaN(+position) ? undefined : +position,
      severity: severity === '_' ? undefined : severity,
      tasks,
      rewards,
      fromDate: fromDate?.isValid() ? fromDate.toDate() : null,
    });
  };

  const handleAddReward = () => {
    setRewards([
      ...rewards,
      {
        type: QuestRewardType.Pixel,
        context: { amount: 0.025, chainId: 19, symbol: 'PXLs' },
        execute: true,
      },
    ]);
  };
  const handleChangeReward = (i: number) => (reward: ICreateQuestRewardDto) => {
    const list = [...rewards];
    list[i] = reward;
    setRewards(list);
  };
  const handleRemoveReward = (i: number) => () => {
    const list = [...rewards];
    list.splice(i, 1);
    setRewards(list);
  };

  const handleAddTask = () => {
    setTasks([
      ...tasks,
      { title: `Task ${tasks.length}`, type: QuestTaskType.NoopLink, context: {} },
    ]);
  };
  const handleChangeTask = (i: number) => (task: ICreateQuestTaskDto) => {
    const list = [...tasks];
    list[i] = task;
    setTasks(list);
  };
  const handleRemoveTask = (i: number) => () => {
    const list = [...tasks];
    list.splice(i, 1);
    setTasks(list);
  };

  const disabled = !onSave || !title || loading || !tasks.length;
  const startDisabled = !title || loading || !tasks.length;
  const disableFields = loading || disabledForm;
  const isInvalidImage = !isValidUrl(image);

  return (
    <Box>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <Paper variant='outlined' sx={{ p: 2 }}>
          <Stack direction='column' gap={2}>
            <Stack direction='row' gap={2}>
              <TextField
                fullWidth
                label='Title'
                placeholder='Enter title'
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                required
                disabled={disableFields}
              />

              <ImageUrlField
                required
                fullWidth
                label='Image'
                placeholder='Enter url'
                value={image}
                error={isInvalidImage}
                onChange={(e) => setImage(e.target.value)}
                disabled={disableFields}
              />
            </Stack>

            <Stack direction='row' gap={2}>
              <FormControl fullWidth required disabled={disableFields}>
                <InputLabel id='demo-simple-select-label'>Category</InputLabel>
                <Select
                  value={category}
                  label='Category'
                  required
                  onChange={(e) => setCategory(e.target.value as string)}
                >
                  {['public', 'events', 'partners', 'influencers'].map((el) => (
                    <MenuItem key={el} value={el}>
                      {el}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <NumberField
                fullWidth
                label='Limit Users'
                placeholder='Enter limit value'
                value={limitUsers}
                onChange={(e) => setLimitUsers(e.target.value)}
                disabled={disableFields}
              />
            </Stack>

            <Stack direction='row' gap={2}>
              <FormControl fullWidth>
                <InputLabel>Severity</InputLabel>
                <Select
                  value={severity}
                  label='Severity'
                  required
                  onChange={(e) => setSeverity(e.target.value as QuestSeverity)}
                >
                  {[QuestSeverity.Best, QuestSeverity.Fresh, QuestSeverity.Premium].map((el) => (
                    <MenuItem key={el} value={el}>
                      {el}
                    </MenuItem>
                  ))}
                  <MenuItem value='_'>Default</MenuItem>
                </Select>
              </FormControl>
              <NumberField
                fullWidth
                label='Position'
                placeholder='Position'
                value={position || ''}
                onChange={(e) => setPosition(e.target.value)}
              />
            </Stack>

            <DateTimePicker
              label='Register From'
              value={fromDate}
              ampm={false}
              onChange={(e) => setFromDate(e)}
            />

            <Divider />

            <Box>
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  width: '100%',
                  alignItems: 'center',
                  mb: 2,
                }}
              >
                <Typography fontWeight='bold'>Tasks</Typography>
                {tasks.length < 5 && (
                  <Button
                    variant='outlined'
                    size='small'
                    onClick={handleAddTask}
                    disabled={disableFields}
                  >
                    Add Task
                  </Button>
                )}
              </Box>

              <div>
                {tasks.map((task, i) => (
                  <QuestFormTask
                    key={`task-${i}`}
                    task={task}
                    disabled={disableFields || loading}
                    onChange={handleChangeTask(i)}
                    onRemove={handleRemoveTask(i)}
                  />
                ))}
                {!tasks.length && <Typography color='text.secondary'>No tasks</Typography>}
              </div>
            </Box>

            <Divider />

            <Box>
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  width: '100%',
                  alignItems: 'center',
                  mb: 2,
                }}
              >
                <Typography fontWeight='bold'>Rewards</Typography>
                {rewards.length < 1 && (
                  <Button
                    variant='outlined'
                    size='small'
                    onClick={handleAddReward}
                    disabled={disableFields}
                  >
                    Add Reward
                  </Button>
                )}
              </Box>

              <div>
                {rewards.map((reward, i) => (
                  <QuestFormReward
                    key={`reward-${i}`}
                    reward={reward}
                    disabled={disableFields || loading}
                    onChange={handleChangeReward(i)}
                    onRemove={handleRemoveReward(i)}
                  />
                ))}
                {!rewards.length && <Typography color='text.secondary'>No rewards</Typography>}
              </div>
            </Box>

            <Divider />

            <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
              <ButtonGroup variant='outlined' sx={{ '& .MuiButton-root': { minWidth: 150 } }}>
                <Button
                  color='primary'
                  size='large'
                  fullWidth
                  onClick={handleSave}
                  loading={loading}
                  disabled={disabled}
                >
                  {!initialData ? 'Create' : 'Update'}
                </Button>
                {initialData?.disabled && (
                  <Button
                    color='success'
                    size='large'
                    fullWidth
                    onClick={onStart}
                    loading={loading}
                    disabled={startDisabled}
                  >
                    Start
                  </Button>
                )}
                {!!initialData && !initialData.disabled && (
                  <Button color='error' size='large' fullWidth onClick={onStop} loading={loading}>
                    Stop
                  </Button>
                )}
                {!!initialData && initialData.disabled && (
                  <Button color='error' size='large' fullWidth onClick={onDelete} loading={loading}>
                    Delete
                  </Button>
                )}
              </ButtonGroup>
            </Box>
          </Stack>
        </Paper>
      </LocalizationProvider>
    </Box>
  );
}
