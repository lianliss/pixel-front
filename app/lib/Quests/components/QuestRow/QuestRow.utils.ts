import { IQuestDto } from 'lib/Quests/api-server/quests.types.ts';
import { useTranslation } from 'react-i18next';

export function getQuestStatusText(quest: IQuestDto) {
  const { t } = useTranslation('quests');

  const isCompleted = !!quest.completedAt;
  const isLimited = typeof quest.limitUsers === 'number';
  const isLimitReached = isLimited && quest.currentUsers >= quest.limitUsers;
  const text = isCompleted
    ? t('Completed')
    : isLimitReached
    ? t('Ended')
    : isLimited
    ? t('Incompleted') // `${quest.currentUsers}/${quest.limitUsers}`
    : t('Incompleted');

  return {
    text,
    isCompleted,
    isLimited,
    isLimitReached,
  };
}
