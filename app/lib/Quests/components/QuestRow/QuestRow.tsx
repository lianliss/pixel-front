import React from 'react';
import styles from './QuestRow.module.scss';
import { IQuestDto, QuestRewardType, QuestSeverity } from 'lib/Quests/api-server/quests.types.ts';
import clsx from 'clsx';
import ChevronRight from 'lib/Quests/components/icons/ChevronRight';
import { questRewardToLocale } from 'lib/Quests/utils/questRewardToLocale.ts';
import { isLocalQuestTask, localQuestChecked } from 'lib/Quests/utils/local-quest-check.ts';
import { getQuestStatusText } from 'lib/Quests/components/QuestRow/QuestRow.utils.ts';

type Props = {
  quest: IQuestDto;
  onSelected?: (quest: IQuestDto) => void;
  disabled?: boolean;
  tutorialId?: string;
};

function QuestRow(props: Props) {
  const { quest, onSelected, disabled = false, tutorialId } = props;

  const { isCompleted, text: statusText } = getQuestStatusText(quest);
  const firstTask = quest.tasks?.[0];
  const rewards = quest.rewards;
  const link = quest.link;

  const redirect = () => {
    if (link && firstTask) {
      if (isLocalQuestTask(firstTask)) {
        localQuestChecked(firstTask.id);
      }

      window.open(link, '_blank');
    } else {
      onSelected?.(quest);
    }
  };

  return (
    <div
      id={tutorialId}
      className={clsx(styles.root, {
        [styles.disabled]: disabled,
        [styles[`custom_${quest?.severity?.toLowerCase()}`]]: !!quest?.severity,
      })}
      onClick={redirect}
    >
      <img src={quest.image} alt='' className={styles.image} />

      <div className={styles.content}>
        <span className={styles.title}>{quest.title}</span>
        <div className={styles.prizeBox}>
          {rewards?.length > 0 && (
            <span>
              {rewards.map((reward, i) => (
                <span className={styles.prize} key={`reward-${i}`}>
                  {i > 0 ? ', ' : ''}
                  {questRewardToLocale(reward.type, reward.context)}
                </span>
              ))}
            </span>
          )}
          <span className={clsx(styles.completedText, { [styles.completed]: isCompleted })}>
            {statusText}
          </span>
        </div>
      </div>

      <ChevronRight className={styles.icon} />
    </div>
  );
}

export default QuestRow;
