'use strict';
import React from 'react';
import styles from './QuestDialog.module.scss';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { IQuestDto } from 'lib/Quests/api-server/quests.types.ts';
import Divider from 'ui/Divider';
import QuestTaskRow from '../QuestTaskRow/QuestTaskRow.tsx';
import clsx from 'clsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import { Input } from '@ui-kit/Input/Input.tsx';
import Alert from '@mui/material/Alert';
import { useTranslation } from 'react-i18next';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

type Props = {
  open: boolean;
  onClose?: () => void;
  disabled?: boolean;
  quest?: IQuestDto;
  address?: string;
  onCheck?: (quest: IQuestDto, context: object) => void;
  telegramId?: number;
  loading?: boolean;
};

function QuestDialog(props: Props) {
  const { onClose, quest, open, disabled, onCheck, address, telegramId, loading } = props;

  const { t } = useTranslation('quests');

  const [uid, setUid] = React.useState('');
  const allCompleted = !!quest?.completedAt;
  const disabledByLimit =
    quest && typeof quest.limitUsers === 'number' && quest.currentUsers >= quest.limitUsers;

  const isOkx = quest && [101, 102, 202, 207].includes(quest.id);
  const invalid = isOkx && !uid;
  const disabledOkx = Number.isNaN(+uid);

  const handleCheck: React.MouseEventHandler = (e) => {
    e.stopPropagation();
    onCheck?.(quest, isOkx ? { uuid: String(uid) } : undefined);
  };

  return (
    <BottomDialog isOpen={open} onClose={onClose}>
      {quest && (
        <div className={clsx(styles.root, { [styles.isOkx]: isOkx })}>
          <h2 className={styles.title}>{quest.title}</h2>

          {(quest.summary || quest.summary3) && (
            <div className={styles.summary}>
              {quest.summary && <span>{quest.summary}</span>}
              {quest.summary3 && <span>{quest.summary3}</span>}
            </div>
          )}

          {isOkx && (
            <>
              <span className={styles.help}>{t('Find your UID')}</span>
              <Input
                fullWidth
                sx={{ marginTop: 1, marginBottom: 1 }}
                placeholder={t('OKX account UID')}
                value={uid}
                onChange={(e) => setUid(e.target.value)}
              />
            </>
          )}

          <div className={styles.list} style={{ marginTop: 10, marginBottom: 12 }}>
            {quest.tasks?.length > 0 && <Divider />}
            {quest?.tasks.map((task, i) => (
              <React.Fragment key={`quest-${quest.id}-${task.id}`}>
                {i > 0 && <Divider />}
                <QuestTaskRow
                  task={task}
                  address={address}
                  telegramId={telegramId}
                  completed={allCompleted}
                />
              </React.Fragment>
            ))}
            {quest.tasks?.length > 0 && <Divider />}

            {!quest.tasks?.length && <span style={{ padding: '32px 16px' }}>{t('No tasks')}</span>}
          </div>

          {(quest.warning || quest.help) && (
            <div className={styles.helpBox}>
              {quest.warning && (
                <Alert color='warning' variant='outlined'>
                  {quest.warning}
                </Alert>
              )}

              {quest.help && (
                <Alert color='info' variant='outlined'>
                  {quest.help}
                </Alert>
              )}
            </div>
          )}

          {quest.dailyChest && (
            <Typography variant='caption' color='info.main' fontWeight='bold'>
              Available for Daily Chest
            </Typography>
          )}

          {!allCompleted && (
            <Button
              id='CheckQuestScreenTarget'
              size='large'
              variant='contained'
              fullWidth
              className={styles.button}
              onClick={handleCheck}
              loading={loading}
              disabled={disabled || invalid || allCompleted || disabledOkx || disabledByLimit}
            >
              {disabledByLimit ? t('Limit reached') : t('Check Progress')}
            </Button>
          )}
        </div>
      )}
    </BottomDialog>
  );
}

export default QuestDialog;
