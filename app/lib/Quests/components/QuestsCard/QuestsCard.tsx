import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import QuestIcon from '../../../../shared/icons/QuestIcon.tsx';
import Box from '@mui/material/Box';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useUnit } from 'effector-react';
import { loadQuestUserStats, questUserStatsStore } from 'lib/Quests/api-server/quests.store.ts';
import React from 'react';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useNavigate } from 'react-router-dom';

const Root = styled(Paper)(() => ({
  padding: '16px',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  gap: 16,
}));

export function QuestsCard() {
  const account = useAccount();
  const navigate = useNavigate();

  const questUserStats = useUnit(questUserStatsStore);

  const uncompletedQuests = React.useMemo(() => {
    return questUserStats?.options.reduce((acc, el) => acc + el.amount, 0) || 0;
  }, [questUserStats]);

  React.useEffect(() => {
    if (account.chainId && account.isConnected) {
      loadQuestUserStats({ address: account.accountAddress, chainId: account.chainId })
        .then()
        .catch(() => {});
    }
  }, [account.chainId, account.isConnected, account.accountAddress]);

  const redirectToQuests = () => navigate('/wallet/quests');

  return (
    <Root>
      <Box
        sx={{
          display: 'flex',
          gap: 2,
        }}
      >
        <QuestIcon sx={{ fontSize: 36, color: 'text.secondary' }} />

        <Box>
          <Typography variant='subtitle2'>You have</Typography>
          <Typography variant='caption' color='text.secondary' sx={{ fontSize: 10 }}>
            {uncompletedQuests > 0
              ? `${uncompletedQuests} quests not completed`
              : 'All quests completed'}
          </Typography>
        </Box>
      </Box>

      <Button variant='contained' color='primary' onClick={redirectToQuests}>
        Check
      </Button>
    </Root>
  );
}
