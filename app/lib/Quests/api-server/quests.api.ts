'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  IFindUserQuestsDto,
  IFindUserQuestsResponse,
  ICheckQuestDto,
  ICheckQuestResponse,
  IGetUserQuestResponse,
  IGetUserQuestsUncompletedDto,
  IGetUserQuestsUncompletedResponse,
  IFindQuestsResponse,
  IFindQuestsDto,
  ICreateQuestDto,
  ICreateQuestResponse,
  IUpdateQuestDto,
  IUpdateQuestResponse,
  IDeleteQuestDto,
  IDisabledQuestDto,
  IDeleteQuestResponse,
  IDisableQuestResponse,
} from './quests.types.ts';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';

import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';
import {
  pixelMessageKey,
  pixelSignKey,
  telegramAuthKey,
} from '../../../shared/const/localStorage.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class QuestsApi {
  url: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async fetchQuest({
    address,
    questId,
  }: {
    address: string;
    questId: number;
  }): Promise<IGetUserQuestResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetUserQuestResponse },
      AxiosResponse<{ data: IGetUserQuestResponse }>,
      {}
    >(`/api/v1/quest/get/${questId}`, {
      baseURL: this.url,
    });
    return data.data;
  }

  async fetchQuests({
    userAddress,
    ...other
  }: IFindQuestsDto & { userAddress: string }): Promise<IFindQuestsResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindQuestsResponse },
      AxiosResponse<{ data: IFindQuestsResponse }>,
      IFindQuestsDto
    >(`/api/v1/quest/find`, {
      baseURL: this.url,
      params: other,
    });

    return data.data;
  }

  async fetchUserQuests({
    chainId,
    address,
    category,
  }: IFindUserQuestsDto & { address: string }): Promise<IFindUserQuestsResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindUserQuestsResponse },
      AxiosResponse<{ data: IFindUserQuestsResponse }>,
      IFindUserQuestsDto
    >(`/api/v1/quest/find/user`, {
      baseURL: this.url,
      params: { chainId, category },
    });

    return data.data;
  }

  async checkQuest({
    questId,
    address,
    context,
  }: ICheckQuestDto & { address: string }): Promise<ICheckQuestResponse> {
    const { data } = await axiosInstance.post<
      { data: ICheckQuestResponse },
      AxiosResponse<{ data: ICheckQuestResponse }>,
      ICheckQuestDto
    >(`/api/v1/quest/check`, { questId, context }, { baseURL: this.url });
    return data.data;
  }

  async createQuest({ adminAddress, ...payload }: ICreateQuestDto): Promise<ICreateQuestResponse> {
    const { data } = await axiosInstance.post<
      { data: ICreateQuestResponse },
      AxiosResponse<{ data: ICreateQuestResponse }>,
      Omit<ICreateQuestDto, 'adminAddress'>
    >(`/api/v1/quest/create`, payload, {
      baseURL: this.url,
    });
    return data.data;
  }

  async updateQuest({
    adminAddress,
    id,
    ...payload
  }: IUpdateQuestDto): Promise<IUpdateQuestResponse> {
    const { data } = await axiosInstance.patch<
      { data: IUpdateQuestResponse },
      AxiosResponse<{ data: IUpdateQuestResponse }>,
      Omit<IUpdateQuestDto, 'adminAddress' | 'id'>
    >(`/api/v1/quest/${id}`, payload, {
      baseURL: this.url,
    });
    return data.data;
  }

  async disableQuest({
    adminAddress,
    questId,
    ...payload
  }: IDisabledQuestDto): Promise<IDisableQuestResponse> {
    const { data } = await axiosInstance.patch<
      { data: IDisableQuestResponse },
      AxiosResponse<{ data: IDisableQuestResponse }>,
      Omit<IDisabledQuestDto, 'adminAddress' | 'questId'>
    >(`/api/v1/quest/${questId}/disable`, payload, {
      baseURL: this.url,
    });
    return data.data;
  }

  async deleteQuest({
    adminAddress,
    questId,
    ...payload
  }: IDeleteQuestDto): Promise<IDeleteQuestResponse> {
    const { data } = await axiosInstance.patch<
      { data: IDeleteQuestResponse },
      AxiosResponse<{ data: IDeleteQuestResponse }>,
      Omit<IDeleteQuestDto, 'adminAddress' | 'questId'>
    >(`/api/v1/quest/${questId}/delete`, payload, {
      baseURL: this.url,
    });
    return data.data;
  }

  async fetchUserStats({
    chainId,
    address,
  }: IGetUserQuestsUncompletedDto & {
    address: string;
  }): Promise<IGetUserQuestsUncompletedResponse> {
    const { data } = await axiosInstance.get<
      { data: ICheckQuestResponse },
      AxiosResponse<{ data: IGetUserQuestsUncompletedResponse }>,
      IGetUserQuestsUncompletedDto
    >(`/api/v1/quest/user/uncompleted`, {
      baseURL: this.url,
      params: { chainId },
    });
    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const questsApi = new QuestsApi({ url: 'https://api.hellopixel.network' });
