import { createEffect, createStore } from 'effector';
import {
  ICheckQuestDto,
  ICreateQuestDto,
  IDeleteQuestDto,
  IDisabledQuestDto,
  IFindQuestsDto,
  IFindUserQuestsDto,
  IGetUserQuestsUncompletedResponse,
  IQuestDto,
  IQuestsDto,
  IUpdateQuestDto,
} from 'lib/Quests/api-server/quests.types.ts';
import { questsApi } from 'lib/Quests/api-server/quests.api.ts';
import { createStoreState } from 'utils/store/createStoreState.ts';

// list

export const loadQuests = createEffect(async (params: IFindQuestsDto) => {
  const res = await questsApi.fetchQuests(params);
  return res;
});

export const questsStore = createStoreState(loadQuests);

// user quests

export const loadUserQuests = createEffect(
  async (params: IFindUserQuestsDto & { address: string }) => {
    const res = await questsApi.fetchUserQuests({ ...params });
    let nextRows = res.rows;

    if (nextRows.find((el) => el.id === 100)?.completedAt) {
      nextRows = nextRows.filter((el) => ![214].includes(el.id));
    }

    return { res: { rows: nextRows, count: nextRows.length }, category: params.category };
  }
);

export const userQuestsStore = createStore<{ [key: string]: IQuestsDto }>({}).on(
  loadUserQuests.doneData,
  (prev, next) => {
    return { ...prev, [next.category]: next.res };
  }
);

// get
export const loadQuest = createEffect(async (params: { address: string; questId: number }) => {
  const res = await questsApi.fetchQuest(params);
  return res;
});

export const questStore = createStore<IQuestDto | null>(null).on(
  loadQuest.doneData,
  (prev, next) => next
);

// check

export const checkQuest = createEffect(async (params: ICheckQuestDto & { address: string }) => {
  const res = await questsApi.checkQuest(params);
  return res;
});

// total
export const loadQuestUserStats = createEffect(
  async (params: { address: string; chainId: number }) => {
    const res = await questsApi.fetchUserStats({ ...params });
    return res;
  }
);

export const questUserStatsStore = createStore<IGetUserQuestsUncompletedResponse | null>(null).on(
  loadQuestUserStats.doneData,
  (prev, next) => {
    const result = next.options;

    const option = result.find((el) => el.category === 'partners');

    // fix FINU
    if (option?.amount) {
      option.amount--;
    }

    return next;
  }
);

// admin

export const createQuest = createEffect(async (params: ICreateQuestDto) => {
  const res = await questsApi.createQuest(params);
  return res;
});

export const updateQuest = createEffect(async (params: IUpdateQuestDto) => {
  const res = await questsApi.updateQuest(params);
  return res;
});

export const disableQuest = createEffect(async (params: IDisabledQuestDto) => {
  const res = await questsApi.disableQuest(params);
  return res;
});

export const deleteQuest = createEffect(async (params: IDeleteQuestDto) => {
  const res = await questsApi.deleteQuest(params);
  return res;
});
