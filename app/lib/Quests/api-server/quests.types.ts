//

import { IPaginatedQuery } from 'lib/Leaderboard/api-server/leaderboard.types.ts';

export type IPaginatedResult<Row> = {
  rows: Row[];
  count: number;
};

//
export enum QuestTaskType {
  TelegramChat = 'TelegramChat',
  ExternalService = 'ExternalService',
  TwitterChannel = 'TwitterChannel',
  SubscribeYoutube = 'SubscribeYoutube',
  DrillLevel = 'DrillLevel',
  StorageLevel = 'StorageLevel',
  Noop = 'Noop',
  NoopLink = 'NoopLink',
  SwitchNetwork = 'SwitchNetwork',
}

export enum QuestSeverity {
  Best = 'Best',
  Fresh = 'Fresh',
  Premium = 'Premium',
}

export type IQuest<TDate = Date> = {
  id: number;
  chainId?: string;

  title: string;
  position: number;
  category?: string;
  summary?: string;
  warning?: string;
  image?: string;
  help?: string;
  link?: string;

  limitUsers?: number;
  currentUsers?: number;

  dailyChest: boolean;
  severity?: QuestSeverity;

  deleted?: boolean;
  disabled?: boolean;
  // dates
  fromDate: TDate | null;
  createdAt: TDate;

  // relations
  tasks?: IQuestTask<QuestTaskType, TDate>[];
};

export type IQuestDto<TDate = number> = Omit<IQuest<TDate>, 'tasks' | 'rewards'> & {
  tasks?: IQuestTaskDto<TDate>[];
  rewards?: IQuestRewardDto<TDate>[];
  completedAt?: TDate | null;
};

export type IQuestsDto = IPaginatedResult<IQuestDto>;

export type IQuestRewardContextMap = {
  [QuestRewardType.Gassless]: { amount: number };
  [QuestRewardType.Gas]: { amount: number; symbol?: string };
  [QuestRewardType.Pixel]: { chestId?: number; pixels?: number; symbol?: string; chainId?: number };
  [QuestRewardType.Coin]: { amount: number; symbol: string; amountType?: 'upTo' };
};

export type IUserChestCounterDto = {
  id: string;
  completed: number;
};

// task

export type IQuestTaskDto<TDate = number> = IQuestTask<QuestTaskType, TDate>;

export type IQuestTaskContextMap = {
  [QuestTaskType.TelegramChat]: { chatId: string };
  [QuestTaskType.DrillLevel]: { level: number; chainId: number };
  [QuestTaskType.StorageLevel]: { level: number; chainId: number };
  [QuestTaskType.SubscribeYoutube]: { channelId: string };
  [QuestTaskType.TwitterChannel]: { link: string };
  [QuestTaskType.ExternalService]: { link: string };
  [QuestTaskType.SwitchNetwork]: { chainId: number };
  // eslint-disable-next-line
  [QuestTaskType.Noop]: {};
};

export type IQuestTask<Type extends QuestTaskType = QuestTaskType, TDate = Date> = {
  id: string;

  questId: number;
  title: string;
  image?: string;
  link?: string;
  summary?: string;

  type: Type;
  context: IQuestTaskContextMap[Type];

  completedAt: TDate | null;
  createdAt: TDate;
};

// reward

export enum QuestRewardType {
  Gassless = 'Gassless',
  Pixel = 'Pixel',
  Coin = 'Coin',
  Gas = 'Gas',
  Chest = 'Chest',
}

export type IQuestRewardContextMap = {
  [QuestRewardType.Gassless]: { amount: number };
  [QuestRewardType.Pixel]: { chestId?: number; amount?: number; symbol?: string };
  [QuestRewardType.Chest]: { chestId: number };
  [QuestRewardType.Gas]: { amount?: number; symbol?: string };
  [QuestRewardType.Coin]: { amount: number; symbol: string; amountType?: 'upTo' };
};

export type IQuestReward<Type extends QuestRewardType = QuestRewardType, TDate = Date> = {
  id: string;
  questId: number;

  type: QuestRewardType;
  context: IQuestRewardContextMap[Type];
  execute: boolean;

  createdAt: TDate;

  // relations
};

export type IQuestRewardDto<TDate = number> = IQuestReward<QuestRewardType, TDate>;

// dto

export type IFindQuestsDto = IPaginatedQuery & { userAddress: string } & { chainId?: number };

export type IFindUserQuestsDto = {
  chainId?: string;
  category: string;
};

export type ICheckQuestDto = {
  questId: number;
  context?: object;
};

export type IFindUserQuestsResponse = IQuestsDto;

export type IGetUserQuestResponse = IQuestDto;

export type IFindQuestsResponse = IPaginatedResult<IQuestDto>;

export type ICheckQuestResponse = boolean;

// total
export type IGetUserQuestsUncompletedDto = {
  chainId: number;
};

export type IGetUserQuestsUncompletedResponseOption = { category: string; amount: number };
export type IGetUserQuestsUncompletedResponse = {
  options: IGetUserQuestsUncompletedResponseOption[];
};

// request admin

export type ICreateQuestTaskDto = {
  title: string;
  link?: string;
  summary?: string;
  image?: string;
  type: QuestTaskType;
  severity?: QuestSeverity;
  position?: number;
  context: IQuestTaskContextMap[QuestTaskType];
};
export type ICreateQuestRewardDto = {
  type: QuestRewardType;
  context: IQuestRewardContextMap[QuestRewardType];
  execute?: boolean;
};
export type ICreateQuestDto = {
  title: string;
  image?: string;
  link?: string;
  locale?: string;
  category?: string;
  dailyChest: boolean;
  chainId: number;
  limitUsers?: number;
  fromDate: number | null;
  position?: number;
  severity?: QuestSeverity;
  tasks: ICreateQuestTaskDto[];
  rewards: ICreateQuestRewardDto[];
} & { adminAddress: string };

export type IUpdateQuestDto = {
  title: string;
  image?: string;
  link?: string;
  locale?: string;
  category?: string;
  chainId: number;
  dailyChest: boolean;
  limitUsers?: number;
  fromDate: number | null;
  severity?: QuestSeverity;
  position?: number;
  tasks: ICreateQuestTaskDto[];
  rewards: ICreateQuestRewardDto[];
} & { adminAddress: string } & { id: number };

export type IDisabledQuestDto = {
  disabled: boolean;
} & { questId: number } & { adminAddress: string };

export type IDeleteQuestDto = {
  deleted: boolean;
} & { questId: number } & { adminAddress: string };

// response admin

export type ICreateQuestResponse = IQuestDto;

export type IUpdateQuestResponse = IQuestDto;

export type IDisableQuestResponse = IQuestDto;

export type IDeleteQuestResponse = IQuestDto;
