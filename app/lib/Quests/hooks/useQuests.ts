import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react';
import { useCallback, useEffect } from 'react';
import toaster from 'services/toaster.tsx';
import { IPaginationResult } from 'utils/hooks/usePagination.ts';
import { loadQuests, questsStore } from 'lib/Quests/api-server/quests.store.ts';

export function useQuests({
  pagination,
  chainId,
}: {
  pagination?: IPaginationResult;
  chainId?: number;
}) {
  const account = useAccount();
  const entries = useUnit(questsStore);

  const refetch = useCallback(async () => {
    if (account.isConnected) {
      try {
        await loadQuests({
          limit: pagination?.limit || 10,
          offset: pagination?.offset || 0,
          userAddress: account.accountAddress,
          chainId,
        });
      } catch (e) {
        toaster.captureException(e);
      }
    }
  }, [account.accountAddress, account.isConnected, pagination?.limit, pagination?.offset]);

  useEffect(() => {
    refetch();
  }, [refetch]);
  useEffect(() => {
    pagination?.setTotal(entries.data?.count || 0);
  }, [entries.data?.count]);

  return {
    data: entries.data,
    error: entries.error,
    loading: entries.loading,
    refetch,
  };
}
