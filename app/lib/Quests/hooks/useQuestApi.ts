import {
  ICreateQuestDto,
  IDeleteQuestDto,
  IDisabledQuestDto,
  IUpdateQuestDto,
} from 'lib/Quests/api-server/quests.types.ts';
import toaster from 'services/toaster.tsx';
import {
  createQuest,
  deleteQuest,
  disableQuest,
  updateQuest,
} from 'lib/Quests/api-server/quests.store.ts';
import { useUnit } from 'effector-react';

export function useQuestApi({
  onCreate,
  onUpdate,
}: {
  onCreate?: () => void;
  onUpdate?: () => void;
}) {
  const loadingCreate = useUnit(createQuest.pending);
  const loadingUpdate = useUnit(updateQuest.pending);
  const loadingDisable = useUnit(disableQuest.pending);
  const loadingDelete = useUnit(deleteQuest.pending);

  const create = async (payload: ICreateQuestDto) => {
    try {
      await createQuest(payload);
      onCreate?.();
    } catch (e) {
      toaster.captureException(e);
    }
  };
  const update = async (payload: IUpdateQuestDto) => {
    try {
      await updateQuest(payload);
      onUpdate?.();
    } catch (e) {
      toaster.captureException(e);
    }
  };
  const disable = async (payload: IDisabledQuestDto) => {
    try {
      await disableQuest(payload);
      onCreate?.();
    } catch (e) {
      toaster.captureException(e);
    }
  };
  const deleteAction = async (payload: IDeleteQuestDto) => {
    try {
      await deleteQuest(payload);
      onCreate?.();
    } catch (e) {
      toaster.captureException(e);
    }
  };

  return {
    create,
    update,
    disable,
    delete: deleteAction,
    loading: loadingCreate || loadingUpdate || loadingDisable || loadingDelete,
  };
}
