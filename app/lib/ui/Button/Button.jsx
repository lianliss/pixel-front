'use strict';
import React from 'react';
import clsx from 'clsx';
import styles from './Button.module.scss';
import { Spinner } from '@blueprintjs/core';

function Button(props) {
  const {
    className,
    variant = 'contained',
    color,
    primary,
    fullWidth,
    children,
    loading,
    disabled,
    large,
    small,
    ...other
  } = props;

  return (
    <button
      {...other}
      className={clsx(
        styles.root,
        {
          [styles.primary]: color === 'primary' || primary,
          [styles.white]: color === 'white',
          [styles.contained]: variant === 'contained',
          [styles.outlined]: variant === 'outlined',
          [styles.fullWidth]: fullWidth,
          [styles.large]: large,
          [styles.small]: small,
        },
        className
      )}
      disabled={disabled || loading}
    >
      {loading ? <Spinner size={20} /> : children}
    </button>
  );
}

export default Button;
