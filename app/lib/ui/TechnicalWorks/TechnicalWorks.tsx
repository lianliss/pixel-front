import React from 'react';
import styles from './TechnicalWorks.module.scss';
import image from 'styles/svg/loading.svg';
import { useTranslation } from 'react-i18next';

export function TechnicalWorks(props = {}) {
  const { t } = useTranslation('achievements');

  return (
    <div className={styles.root}>
      <img className='loading' src={image} />
      <div>{t('Technical Works')}</div>
    </div>
  );
}
