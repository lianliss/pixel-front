import React from 'react';
import styles from './WalletBlock.module.scss';
import Paper from '@mui/material/Paper';

function WalletBlock({ children, className, frame, bold, title, footer, onClick = () => {} }) {
  const classNames = [styles.walletBlock];
  if (className) {
    classNames.push(className);
  }
  if (frame) {
    classNames.push(styles.framed);
    const frameClassNames = [styles.walletBlockFrame];
    if (bold) {
      frameClassNames.push(styles.bold);
      classNames.push(styles.bold);
    }
    return (
      <Paper variant='outlined' className={frameClassNames.join(' ')} onClick={onClick}>
        <div className={classNames.join(' ')}>{children}</div>
        {!!footer && <div className={styles.footer}>{footer}</div>}
      </Paper>
    );
  }

  return (
    <Paper variant='outlined' className={classNames.join(' ')} onClick={onClick}>
      {!!title && <div className={styles.walletBlockTitle}>{title}</div>}
      {children}
    </Paper>
  );
}

export default WalletBlock;
