import React from 'react';
import { default as ReactCountdown } from 'react-countdown';

const timeRenderer = ({ days, hours, minutes, seconds, completed }) => {
  if (completed) {
    // Render a completed state
    return '';
  } else {
    // Render a countdown
    const _hours = days * 24 + hours;
    return (
      <>
        {/*{!!days && `${days.toString()} days `}*/}
        {!!_hours && `${_hours.toString().padStart(2, '0')}:`}
        {minutes.toString().padStart(2, '0')}:{seconds.toString().padStart(2, '0')}
      </>
    );
  }
};

function Countdown({ time, onComplete = () => {} }) {
  return <ReactCountdown renderer={timeRenderer} onComplete={onComplete} date={time} />;
}

export default Countdown;
