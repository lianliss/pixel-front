import clsx from 'clsx';
import React from 'react';

import styles from './Divider.module.scss';

const Divider = (props) => {
  const { className, ...other } = props;

  return <hr className={clsx(styles.root, className)} {...other} />;
};

export default Divider;
