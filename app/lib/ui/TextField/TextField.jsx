import clsx from 'clsx';
import React from 'react';

import styles from './TextField.module.scss';
import { InputGroup } from '@blueprintjs/core';

const TextField = (props) => {
  const { className, type, fullWidth, value, onChange, placeholder, min, max, step, ...other } =
    props;

  return (
    <div
      className={clsx(styles.root, { [styles.root_fullwidth]: fullWidth }, className)}
      {...other}
    >
      <InputGroup
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        type={type}
        min={min}
        max={max}
        step={step}
      />
    </div>
  );
};

export default TextField;
