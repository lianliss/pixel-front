import clsx from 'clsx';
import React from 'react';

import styles from './Select.module.scss';

const Select = (props) => {
  const { className, options = [], fullWidth, value, onChange, ...other } = props;

  return (
    <div
      className={clsx(styles.root, { [styles.root_fullwidth]: fullWidth }, className)}
      {...other}
    >
      <select value={value} onChange={onChange}>
        {options.map((option) => (
          <option value={option.value} key={`option-${option.value}`}>
            {option.label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default Select;
