import React from 'react';
import styles from './WalletPopup.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { appSetIsHideMenuButton } from 'slices/App';
import { BottomSheet } from 'react-spring-bottom-sheet';
import 'react-spring-bottom-sheet/dist/style.css';
import { networkIDSelector } from 'app/store/selectors';

function WalletPopup({
  title,
  footer,
  children,
  className,
  focusRef,
  noAnimate = false,
  onClose = () => {},
  onOpen = () => {},
  onUnmount = () => {},
}) {
  const dispatch = useDispatch();
  const classes = [styles.walletPopupContainer];
  const networkID = useSelector(networkIDSelector);
  if (networkID) {
    classes.push(networkID);
    classes.push('wallet-popup');
  }
  if (className) {
    classes.push(className);
  }

  React.useEffect(() => {
    onOpen();
    dispatch(appSetIsHideMenuButton(true));
    return () => {
      dispatch(appSetIsHideMenuButton(false));
      onUnmount();
    };
  }, []);

  return (
    <BottomSheet
      open={true}
      header={title}
      footer={footer}
      skipInitialTransition={noAnimate}
      initialFocusRef={focusRef}
      onDismiss={onClose}
    >
      <div className={classes.join(' ')}>{children}</div>
    </BottomSheet>
  );
}

export default WalletPopup;
