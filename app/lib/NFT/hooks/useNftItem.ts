import { useUnit } from 'effector-react';
import { loadNftItem, nftItemStore } from 'lib/NFT/api-server/nft.store.ts';
import { useEffect } from 'react';

export function useNftItem(payload: { assetId: number | string; contractId: string }) {
  const state = useUnit(nftItemStore);

  useEffect(() => {
    loadNftItem({
      assetId: payload.assetId.toString(),
      contractId: payload.contractId,
    })
      .then()
      .catch();
  }, [payload.assetId, payload.contractId]);

  return {
    ...state,
  };
}
