import { useUnit } from 'effector-react';
import { loadNftCollections, nftCollectionsStore } from 'lib/NFT/api-server/nft.store.ts';
import { useEffect } from 'react';

export function useNftCollections(payload: { address: string; skip?: boolean }) {
  const res = useUnit(nftCollectionsStore);

  useEffect(() => {
    if (!payload.skip && payload.address) {
      loadNftCollections({
        limit: 100,
        offset: 0,
        contractId: payload.address?.toLowerCase(),
      }).then();
    }
    if (!payload.address) {
      nftCollectionsStore.reset();
    }
  }, [payload.address, payload.skip]);

  return {
    data: res.state,
    loading: res.loading,
    error: res.error,
  };
}
