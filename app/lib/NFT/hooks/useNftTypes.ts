import React from 'react';
import wait from 'utils/wait';
import flatten from 'lodash-es/flatten';
import pushPixel from 'utils/pixelGlobal';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { convertSlotItemType } from 'lib/NFT/utils/converters.ts';
import NFT_ABI from 'lib/NFT/api-contract/nft.abi.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import { useReadContracts } from 'wagmi';
import ERC_20_ABI from 'lib/Wallet/api-contract/abi/erc-20.abi.ts';
import { IS_WAGMI } from '@cfg/config.ts';

const LIMIT = 200;

export function useNftTypes() {
  const { chainId, isConnected } = useAccount();
  const [isTypesLoading, setIsTypesLoading] = React.useState(true);
  const [itemTypes, setItemTypes] = React.useState([]);

  const nftAddress = NFT_CONTRACT_ID[chainId];
  const typesCountData = useReadContract({
    initData: 0,
    abi: NFT_ABI,
    address: nftAddress,
    skip: !nftAddress || !isConnected,
    functionName: 'getTypesLength',
    select: (res) => Number(res),
  });
  const typesCount = typesCountData.data;
  const typesData = useReadContract({
    abi: NFT_ABI,
    address: nftAddress,
    skip: !nftAddress || !isConnected,
    functionName: 'getTypes',
  });

  const fetchContracts = [];
  const chunks = Math.floor(typesCount / LIMIT);
  const tail = typesCount % LIMIT;

  for (let i = 0; i < chunks; i++) {
    fetchContracts.push({
      address: nftAddress,
      abi: NFT_ABI,
      functionName: 'getTypes',
      args: [i * LIMIT, LIMIT],
    });
  }
  if (tail) {
    fetchContracts.push({
      address: nftAddress,
      abi: NFT_ABI,
      functionName: 'getTypes',
      args: [chunks * LIMIT, tail],
    });
  }

  const wagmiTypes =
    IS_WAGMI &&
    useReadContracts({
      allowFailure: false,
      contracts: fetchContracts,
      query: {
        enabled: !(!nftAddress || !isConnected) && IS_WAGMI && fetchContracts.length,
      },
    });

  const updateItemTypes = React.useMemo(
    () => async () => {
      try {
        if (!isConnected || !typesCount) return;

        setIsTypesLoading(true);
        // if (version) {
        //     await wait(3000);
        // }
        const LIMIT = 200;
        // const preload = await Promise.all([
        //   contracts.PXLsNFTV5.methods.getTypesLength().call(),
        //   // contracts.PXLsNFTV5.methods.getCollections().call(),
        // ]);
        const count = typesCount;
        // const collections = preload[1];
        const promises = [];
        const chunks = Math.floor(count / LIMIT);
        const tail = count % LIMIT;

        for (let i = 0; i < chunks; i++) {
          promises.push(typesData.refetch({ args: [i * LIMIT, LIMIT] }));
          // promises.push(contracts.PXLsNFTV5.methods.getTypes(i * LIMIT, LIMIT).call());
        }
        if (tail) {
          promises.push(typesData.refetch({ args: [chunks * LIMIT, tail] }));
          // promises.push(contracts.PXLsNFTV5.methods.getTypes(chunks * LIMIT, tail).call());
        }

        const result = await Promise.allSettled(promises);
        const types = [];

        result.map((chunk: any, index) => {
          if (chunk.error) {
            if (index < chunks) {
              console.error(
                '[updateItemTypes]',
                index,
                {
                  chunks,
                  offset: index * LIMIT,
                  limit: LIMIT,
                },
                chunk.error
              );
            } else {
              console.error(
                '[updateItemTypes]',
                'tail',
                {
                  chunks,
                  offset: chunks * LIMIT,
                  limit: tail,
                },
                chunk.error
              );
            }
          } else {
            types.push(chunk.value);
          }
        });

        const processed = flatten(types).map(convertSlotItemType);

        setItemTypes(processed);
        // setCollections(collections);
        // setVersion(Date.now());

        // pushPixel(
        //     {
        //         itemTypes: processed,
        //         collections,
        //     },
        //     'nft'
        // );
      } catch (error) {
        console.error('[updateNFTTypes]', error);
      }
      setIsTypesLoading(false);
    },
    [isConnected, typesCount, chainId]
  );

  React.useEffect(() => {
    if (isConnected && typesCount) {
      if (!IS_WAGMI) {
        updateItemTypes().then();
      }
    }
  }, [updateItemTypes]);

  const wagmiTypesResult = IS_WAGMI ? flatten(wagmiTypes.data || []).map(convertSlotItemType) : [];

  return {
    data: IS_WAGMI ? wagmiTypesResult : itemTypes,
    loading: isTypesLoading,
    refetch: updateItemTypes,
  };
}
