import { useUnit } from 'effector-react';
import { loadNftType, nftTypeStore } from 'lib/NFT/api-server/nft.store.ts';
import { useEffect } from 'react';

export function useNftType(payload: { typeId?: string }) {
  const state = useUnit(nftTypeStore);

  useEffect(() => {
    if (payload.typeId) {
      loadNftType({
        typeId: payload.typeId,
      })
        .then()
        .catch();
    }
  }, [payload.typeId]);

  return {
    ...state,
  };
}
