import styles from './NftCardParam.module.scss';
import React from 'react';
import StatValue from 'lib/NFT/components/StatValue/StatValue.tsx';

export type INftCardParamData = {
  label: string;
  value: string | number;
  isPercent?: boolean;
  isBuff?: boolean;
  isDuration?: boolean;
  isCount?: boolean;
  isDust?: boolean;
};

type Props = {
  label: string;
  value: string | number;
  isPercent?: boolean;
  isBuff?: boolean;
  isDuration?: boolean;
  isCount?: boolean;
  isDust?: boolean;
};

function NftCardParam({ label, value, isPercent, isBuff, isDuration, isCount, isDust }: Props) {
  return (
    <div className={styles.cardInfoParamsItem}>
      <div>
        {typeof value === 'number' && typeof isBuff === 'boolean' && !isDuration && !isCount ? (
          <StatValue
            value={value}
            percent={isPercent}
            buff={isBuff}
            prefix={value > 0 ? '+' : ''}
            postfix={isPercent ? '%' : isDust ? ' PXLd' : ' PXLs'}
          />
        ) : (
          <span>
            {value} {isDuration ? 'h' : ''}
          </span>
        )}
      </div>
      <div className={styles.cardInfoParamsItemTitle}>{label}</div>
    </div>
  );
}

export default NftCardParam;
