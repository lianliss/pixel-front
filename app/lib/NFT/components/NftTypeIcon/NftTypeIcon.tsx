'use strict';
import React from 'react';
import styles from './NftTypeIcon.module.scss';
import clsx from 'clsx';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { NftItemRarity, NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';
import { getNftSlotIcon } from 'lib/NFT/utils/getNftSlotIcon.tsx';

type Props = {
  rarity?: NftItemRarity;
  slot: NftItemSlot;
  compact?: boolean;
  size?: 'small';
} & React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

function NftTypeIcon(props: Props) {
  const { className, rarity, slot, compact, size, ...other } = props;

  const rarityColor = getNftRarityColor(rarity);
  const icon = getNftSlotIcon(slot);

  return (
    <div
      className={clsx(
        styles.nftTypeIcon,
        {
          [styles.nftTypeIcon_compact]: compact,
          [styles.nftTypeIcon_small]: size === 'small',
          [styles.nftTypeIcon_border]: !compact,
        },
        className
      )}
      {...other}
    >
      {compact ? icon : <div style={{ borderColor: rarityColor, color: rarityColor }}>{icon}</div>}
    </div>
  );
}

export default NftTypeIcon;
