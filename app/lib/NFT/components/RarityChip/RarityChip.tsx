'use strict';
import React from 'react';
import styles from './RarityChip.module.scss';
import clsx from 'clsx';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';

type Props = { rarity: NftItemRarity } & React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>;

function RarityChip(props: Props) {
  const { className, rarity, style = {}, ...other } = props;
  const backgroundColor = getNftRarityColor(rarity);
  const color = rarity?.toString() === '1' ? 'black' : 'white';

  return (
    <div
      className={clsx(styles.rarityChip, className)}
      {...other}
      style={{ ...style, backgroundColor, color }}
    >
      {getNftRarityLabel(rarity)}
    </div>
  );
}

export default RarityChip;
