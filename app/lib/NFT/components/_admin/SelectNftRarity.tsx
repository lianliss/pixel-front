import FormControl, { FormControlProps } from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import React from 'react';
import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';

type Props = Omit<FormControlProps, 'value' | 'onChange'> & {
  value?: NftItemRarity;
  onChange?: (rarity: NftItemRarity | null) => void;
};

function SelectNftRarity(props: Props) {
  const { value, onChange, ...other } = props;

  return (
    <FormControl {...other}>
      <InputLabel>Rarity</InputLabel>
      <Select
        value={value || 'none'}
        label='Rarity'
        onChange={(e) =>
          onChange?.(e.target.value === 'none' ? null : (e.target.value as NftItemRarity))
        }
      >
        <MenuItem value='none'>None</MenuItem>
        {[
          NftItemRarity.Common,
          NftItemRarity.UnCommon,
          NftItemRarity.Rare,
          NftItemRarity.Epic,
          NftItemRarity.Legendary,
        ].map((r) => (
          <MenuItem key={r} value={r} sx={{ color: getNftRarityColor(r) }}>
            {getNftRarityLabel(r)}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}

export default SelectNftRarity;
