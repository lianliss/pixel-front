import FormControl, { FormControlProps } from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import React from 'react';
import { NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';
import { getNftSlotLabel } from 'lib/NFT/utils/getNftSlotLabel.ts';
import { getNftSlotIcon } from 'lib/NFT/utils/getNftSlotIcon.tsx';
import SvgIcon from '@mui/material/SvgIcon';

type Props = Omit<FormControlProps, 'value' | 'onChange'> & {
  value?: NftItemSlot;
  onChange?: (Slot: NftItemSlot | null) => void;
};

function SelectNftSlot(props: Props) {
  const { value, onChange, ...other } = props;

  return (
    <FormControl {...other}>
      <InputLabel>Slot</InputLabel>
      <Select
        value={value || 'none'}
        label='Slot'
        onChange={(e) =>
          onChange?.(e.target.value === 'none' ? null : (e.target.value as NftItemSlot))
        }
      >
        <MenuItem value='none'>None</MenuItem>
        {[
          NftItemSlot.Pocket1,
          NftItemSlot.Back,
          NftItemSlot.Backpack,
          NftItemSlot.Belt,
          NftItemSlot.Artifact,
          NftItemSlot.Drill,
          NftItemSlot.LoyaltyCard,
          NftItemSlot.Ring,
        ].map((r) => (
          <MenuItem key={r} value={r}>
            {getNftSlotLabel(r)}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}

export default SelectNftSlot;
