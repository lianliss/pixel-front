import FormControl, { FormControlProps } from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import React, { useMemo } from 'react';
import { useNftCollections } from 'lib/NFT/hooks/useNftCollections.ts';
import { INftCollectionDto } from 'lib/NFT/api-server/nft.types.ts';

type Props = Omit<FormControlProps, 'value' | 'onChange'> & {
  contractId?: string;
  value?: string;
  onChange?: (collectionId: string | null, collectionIndex: number | null) => void;
};

function SelectNftCollection(props: Props) {
  const { contractId, value, onChange, ...other } = props;

  const { data } = useNftCollections({ address: contractId, skip: !contractId });
  const collections: Record<string, INftCollectionDto> = useMemo(() => {
    return data.rows.reduce((acc, el) => ({ ...acc, [el.id]: el }), {});
  }, [data]);

  return (
    <FormControl {...other}>
      <InputLabel>Collection</InputLabel>
      <Select
        value={value || 'none'}
        label='Collection'
        onChange={(e) =>
          onChange?.(
            e.target.value === 'none' ? null : (e.target.value as string),
            e.target.value === 'none' ? null : collections[e.target.value].index
          )
        }
      >
        <MenuItem value='none'>None</MenuItem>

        {data.rows.map((collection) => (
          <MenuItem key={collection.id} value={collection.id}>
            {collection.name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}

export default SelectNftCollection;
