'use strict';
import React from 'react';
import styles from './NftCollectionIcon.module.scss';
import clsx from 'clsx';
import SgbCollectionIcon from 'lib/NFT/components/NftCollectionIcon/SgbCollectionIcon';
import SkaleIcon from 'shared/icons/SkaleIcon';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';

const StyledRoot = styled(Box)(({ theme }) => ({
  border: `solid 1px ${theme.palette.divider}`,
  // color: theme.palette.
}));

const icons = {
  _: SgbCollectionIcon,
  'sgb-pixel-0': SgbCollectionIcon,
  'sgb-pixel-1': SgbCollectionIcon,
  'sgb-pixel-2': SgbCollectionIcon,
  'sgb-pixel-3': SgbCollectionIcon,
  'skale-pixel-0': SgbCollectionIcon,
  'skale-pixel-1': SgbCollectionIcon,
  'skale-pixel-2': SgbCollectionIcon,
  'skale-pixel-3': SgbCollectionIcon,
};

function NftCollectionIcon(props) {
  const { className, collectionId } = props;

  let Icon = icons[collectionId] || icons['_'];

  if (collectionId?.startsWith('skale')) {
    Icon = SkaleIcon;
  }

  return (
    <StyledRoot className={clsx(styles.nftCollectionIcon, className)}>
      <Icon color='text.secondary' sx={{ fontSize: 9 }} />
    </StyledRoot>
  );
}

export default NftCollectionIcon;
