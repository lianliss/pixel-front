import React from 'react';
import styles from './StatValue.module.scss';
import { formatNumber } from 'utils/number';
import clsx from 'clsx';

type Props = React.DetailedHTMLProps<React.HTMLAttributes<HTMLSpanElement>, HTMLSpanElement> & {
  value: number;
  percent?: boolean;
  buff?: boolean;
  prefix?: string;
  postfix?: string;
};

const StatValue: React.FC<Props> = (props) => {
  const { value, className, percent, buff, prefix, postfix, ...other } = props;

  return (
    <span
      className={clsx(styles.stat, { [styles.statUp]: buff, [styles.statDown]: !buff }, className)}
      {...other}
    >
      {prefix}
      {value > 1_000 ? formatNumber(value) : +value.toFixed(4)}
      {postfix}
    </span>
  );
};

export default StatValue;
