import React from 'react';

function SlotBattery() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='32' height='32' viewBox='0 0 32 32' fill='none'>
      <path
        d='M14.125 1C12.5669 1 11.3125 1.83625 11.3125 2.875V3.8125H8.5C6.65331 3.8125 4.75 5.71581 4.75 7.5625V27.25C4.75 29.0967 6.65331 31 8.5 31H23.5C25.3467 31 27.25 29.0967 27.25 27.25V7.5625C27.25 5.71581 25.3467 3.8125 23.5 3.8125H20.6875V2.875C20.6875 1.83625 19.4331 1 17.875 1H14.125ZM8.5 7.5625H23.5V27.25H8.5V7.5625ZM16 9.4375V16H11.3125L16 25.375V18.8125H20.6875L16 9.4375Z'
        fill='white'
      />
    </svg>
  );
}

export default SlotBattery;
