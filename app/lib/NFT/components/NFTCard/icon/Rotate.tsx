import React from 'react';

const RotateIcon = () => {
  return (
    <svg width='58' height='58' viewBox='0 0 58 58' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M27 14.0378C27.6604 14.0128 28.3273 14 29 14C43.9117 14 56 20.268 56 28C56 32.2701 52.3132 36.0937 46.5 38.6616'
        stroke='url(#paint0_linear_71_17)'
        strokeWidth='4'
        strokeLinecap='round'
      />
      <path
        d='M33 41.8474C31.6948 41.9479 30.3591 42 29 42C14.0883 42 2 35.732 2 28C2 23.7299 5.68685 19.9063 11.5 17.3384'
        stroke='url(#paint1_linear_71_17)'
        strokeWidth='4'
        strokeLinecap='round'
      />
      <path
        d='M26.4854 33L34.2635 40.7782C34.6541 41.1687 34.6541 41.8019 34.2635 42.1924L26.4854 49.9706'
        stroke='white'
        strokeWidth='4'
        strokeLinecap='round'
      />
      <path
        d='M31.364 8L25.7072 13.6569C25.3166 14.0474 25.3166 14.6805 25.7072 15.0711L31.364 20.7279'
        stroke='white'
        strokeWidth='4'
        strokeLinecap='round'
      />
      <defs>
        <linearGradient
          id='paint0_linear_71_17'
          x1='39.5'
          y1='12.5'
          x2='33'
          y2='38'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='white' />
          <stop offset='1' stopColor='white' stopOpacity='0' />
        </linearGradient>
        <linearGradient
          id='paint1_linear_71_17'
          x1='15.3621'
          y1='15.8384'
          x2='9.23474'
          y2='41.5342'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='white' stopOpacity='0' />
          <stop offset='1' stopColor='white' />
        </linearGradient>
      </defs>
    </svg>
  );
};

export default RotateIcon;
