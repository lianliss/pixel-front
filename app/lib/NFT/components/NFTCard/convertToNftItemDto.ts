import {
  INftItemDto,
  INftSlotDto,
  INftStatDto,
  INftTypeDto,
  NftItemRarity,
} from 'lib/NFT/api-server/nft.types.ts';

const discounts = {
  6: true,
  7: true,
  8: true,
  17: true,
  18: true,
};

function isNftStatDiscount(modId: string): boolean {
  return !!discounts[modId];
}

export function isNftModBuff(payload: { modId: string; mult: number; add: number }): boolean {
  if (!payload.mult) {
    return payload.add > 0;
  }

  if (isNftStatDiscount(payload.modId)) {
    return payload.mult <= 10_000;
  } else {
    return payload.mult >= 10_000;
  }
}

export function convertToNftItemDto(obj: any): INftItemDto {
  const variants = obj.variants.flatMap((el) => el);

  const slots: INftSlotDto[] = obj.slots.map((s): INftSlotDto => ({ slot: s }));
  const mods: INftStatDto[] = variants.map(
    (m): INftStatDto => ({
      add: m.add,
      mult: m.mul,
      modId: m.parameterId,
      buff: isNftModBuff({ modId: m.parameterId, mult: m.mul, add: m.add }),
    })
  );

  const nftType: INftTypeDto = {
    id: '',
    name: obj.name,
    contractId: '',
    uri: obj.tokenURI,
    rarity: obj.rarity as NftItemRarity,
    createdAt: new Date(),
    collectionId: obj.collectionId,
    slots,
    collection: {
      id: '',
      index: obj.collectionId,
    },
    mods,
    soulbound: obj.soulbound || false,
    droneMiner: false,
    minerDroneType: null,
  };
  const nftItemDto: INftItemDto = {
    id: 'null',
    holder: 'null',
    assetId: obj.tokenId?.toString(),
    typeId: obj.typeId?.toString(),
    type: nftType,
    burnedAt: undefined,
    createdAt: new Date(),
  };

  return nftItemDto as INftItemDto;
}
