import React from 'react';

function Card({ tokenURI, id }) {
  return (
    <svg width='842' height='1119' viewBox='0 0 842 1119' fill='none'>
      <g opacity='0.15' filter='url(#filter0_f_1_55)'>
        <path
          fill='currentColor'
          d='M250.439 188.18C256.311 182.913 263.92 180 271.808 180H630C647.673 180 662 194.327 662 212V907C662 924.673 647.673 939 630 939H212C194.327 939 180 924.807 180 907.134C180 693.153 180 339.844 180 265.403C180 256.313 183.866 247.901 190.632 241.831L250.439 188.18Z'
        />
      </g>
      <g filter='url(#filter1_f_1_55)'>
        <path
          d='M247.598 188.237C253.477 182.935 261.113 180 269.03 180H631.348C649.021 180 663.348 194.327 663.348 212V919C663.348 936.673 649.021 951 631.348 951H208.348C190.675 951 176.348 936.807 176.348 919.134C176.348 701.567 176.348 341.59 176.348 266.489C176.348 257.429 180.188 249.033 186.916 242.966L247.598 188.237Z'
          fill='black'
        />
      </g>
      <g opacity='0.1' filter='url(#filter2_i_1_55)'>
        <path
          fill='currentColor'
          d='M646 156C663.673 156 678 170.327 678 188V903C678 920.673 663.673 935 646 935H192C174.327 935 160 920.848 160 903.175C160 689.519 160 300.908 160 187.723C160 170.05 174.327 156 192 156H646Z'
        />
      </g>
      <g filter='url(#filter3_i_1_55)'>
        <path
          d='M236.514 163.699C242.313 158.731 249.697 156 257.333 156H646C663.673 156 678 170.327 678 188V903C678 920.673 663.673 935 646 935H192C174.327 935 160 920.807 160 903.133C160 683.657 160 320.346 160 243.714C160 234.37 164.084 225.75 171.181 219.671L236.514 163.699Z'
          fill='#081F3A'
        />
      </g>
      <mask id='mask0_1_55' maskUnits='userSpaceOnUse' x='160' y='156' width='518' height='779'>
        <path
          d='M236.514 163.699C242.313 158.731 249.697 156 257.333 156H646C663.673 156 678 170.327 678 188V903C678 920.673 663.673 935 646 935H192C174.327 935 160 920.807 160 903.133C160 683.657 160 320.346 160 243.714C160 234.37 164.084 225.75 171.181 219.671L236.514 163.699Z'
          fill='#081F3A'
        />
      </mask>
      <g mask='url(#mask0_1_55)'>
        <circle opacity='0.2' cx='556' cy='667' r='285' fill={`url(#paint0_radial_1_55${id})`} />
        <circle opacity='0.2' cx='271' cy='839' r='372' fill='url(#paint1_radial_1_55)' />
      </g>
      <g filter='url(#filter4_d_1_55)'>
        <mask id='mask1_1_55' maskUnits='userSpaceOnUse' x='176' y='172' width='486' height='486'>
          <path
            d='M244.083 179.469C249.843 174.644 257.118 172 264.632 172H638C651.255 172 662 182.797 662 196.052C662 360.709 662 463.621 662 626.412C662 643.84 647.872 658 630.445 658V658C624.283 658 618.257 656.196 613.108 652.811L592.503 639.262C587.282 635.829 581.17 634 574.922 634H527.177C521.495 634 515.915 635.513 511.011 638.384L484.989 653.616C480.085 656.487 474.506 658 468.824 658C444.755 658 420.071 658 405.793 658C398.589 658 392.359 653.022 390.75 646V646C389.141 638.978 382.893 634 375.689 634H208C190.327 634 176 619.65 176 601.977C176 461.312 176 383.921 176 251.451C176 241.982 180.193 232.988 187.451 226.908L244.083 179.469Z'
            fill='#081F3A'
          />
        </mask>
        <g mask='url(#mask1_1_55)'>
          <g filter='url(#filter5_dd_1_55)'>
            <rect
              x='174'
              y='170'
              width='490'
              height='490'
              rx='24'
              fill={`url(#pattern0_1_55${id})`}
              shapeRendering='crispEdges'
            />
            <rect
              x='174.5'
              y='170.5'
              width='489'
              height='489'
              rx='23.5'
              stroke='currentColor'
              shapeRendering='crispEdges'
            />
          </g>
          <g opacity='0.4' filter='url(#filter6_f_1_55)'>
            <path
              d='M197.5 233.5L252.065 187.528C257.835 182.666 265.138 180 272.683 180H630C643.255 180 654 190.829 654 204.084C654 375.135 654 436.475 654 618C654 632.5 645.5 643 637 646'
              stroke='url(#paint2_linear_1_55)'
            />
          </g>
          <path
            opacity='0.05'
            d='M244.083 179.469L187.451 226.908C180.193 232.988 176 241.979 176 251.447V481.5L499.5 172H264.637C257.123 172 249.843 174.644 244.083 179.469Z'
            fill='url(#paint3_linear_1_55)'
          />
        </g>
      </g>
      <mask id='mask2_1_55' maskUnits='userSpaceOnUse' x='160' y='156' width='518' height='356'>
        <path
          d='M236.514 163.699C242.313 158.731 249.697 156 257.333 156H646C663.673 156 678 170.327 678 188V480C678 497.673 663.673 512 646 512H192C174.327 512 160 497.535 160 479.862C160 332.983 160 269.151 160 243.921C160 234.576 164.084 225.75 171.181 219.671L236.514 163.699Z'
          fill='#081F3A'
        />
      </mask>
      <g mask='url(#mask2_1_55)'>
        <circle opacity='0.2' cx='662' cy='164' r='285' fill='url(#paint4_radial_1_55)' />
      </g>
      <g opacity='0.4' filter='url(#filter7_i_1_55)'>
        <path
          fill='url(#paint5_linear_1_55)'
          d='M236.514 163.699C242.313 158.731 249.697 156 257.333 156H646C663.673 156 678 170.327 678 188V903C678 920.673 663.673 935 646 935H192C174.327 935 160 920.807 160 903.133C160 683.657 160 320.346 160 243.714C160 234.37 164.084 225.75 171.181 219.671L236.514 163.699Z'
        />
      </g>
      <g filter='url(#filter8_d_1_55)'>
        <path
          stroke='currentColor'
          strokeWidth='2'
          shapeRendering='crispEdges'
          d='M673 190V901C673 917.016 660.016 930 644 930H194C177.979 930 165 917.125 165 901.113V899.832V898.546V897.258V895.967V894.672V893.374V892.074V890.77V889.463V888.153V886.84V885.524V884.205V882.883V881.558V880.23V878.899V877.566V876.229V874.89V873.548V872.203V870.855V869.504V868.151V866.795V865.436V864.074V862.71V861.344V859.974V858.602V857.227V855.85V854.47V853.088V851.703V850.315V848.925V847.533V846.138V844.741V843.341V841.939V840.534V839.128V837.718V836.307V834.893V833.477V832.058V830.638V829.215V827.79V826.363V824.933V823.502V822.068V820.632V819.194V817.754V816.312V814.868V813.422V811.974V810.524V809.072V807.618V806.162V804.704V803.244V801.783V800.319V798.854V797.387V795.918V794.447V792.975V791.501V790.025V788.547V787.068V785.587V784.105V782.621V781.135V779.647V778.159V776.668V775.176V773.683V772.188V770.691V769.193V767.694V766.193V764.691V763.188V761.683V760.176V758.669V757.16V755.65V754.138V752.626V751.112V749.597V748.081V746.563V745.045V743.525V742.004V740.482V738.959V737.435V735.91V734.384V732.857V731.329V729.8V728.27V726.739V725.207V723.674V722.141V720.607V719.071V717.535V715.998V714.461V712.922V711.383V709.844V708.303V706.762V705.22V703.678V702.134V700.591V699.047V697.502V695.956V694.41V692.864V691.317V689.77V688.222V686.674V685.125V683.576V682.026V680.477V678.927V677.376V675.825V674.274V672.723V671.171V669.62V668.068V666.516V664.963V663.411V661.858V660.306V658.753V657.2V655.647V654.094V652.541V650.988V649.435V647.882V646.329V644.777V643.224V641.672V640.119V638.567V637.015V635.463V633.911V632.36V630.809V629.258V627.707V626.157V624.607V623.057V621.508V619.959V618.41V616.862V615.315V613.767V612.221V610.674V609.129V607.584V606.039V604.495V602.951V601.409V599.866V598.325V596.784V595.244V593.704V592.165V590.627V589.09V587.553V586.018V584.483V582.949V581.415V579.883V578.352V576.821V575.291V573.763V572.235V570.708V569.183V567.658V566.134V564.612V563.09V561.57V560.051V558.533V557.016V555.5V553.985V552.472V550.96V549.449V547.939V546.431V544.924V543.418V541.914V540.411V538.909V537.409V535.91V534.413V532.917V531.422V529.929V528.438V526.948V525.46V523.973V522.488V521.004V519.522V518.042V516.564V515.087V513.612V512.138V510.666V509.196V507.728V506.262V504.797V503.334V501.874V500.415V498.958V497.502V496.049V494.598V493.148V491.701V490.256V488.812V487.371V485.932V484.495V483.06V481.627V480.196V478.768V477.341V475.917V474.495V473.075V471.658V470.242V468.83V467.419V466.011V464.605V463.201V461.8V460.401V459.005V457.611V456.219V454.83V453.444V452.06V450.678V449.299V447.923V446.549V445.178V443.81V442.444V441.081V439.721V438.363V437.008V435.655V434.306V432.959V431.615V430.274V428.936V427.601V426.268V424.938V423.612V422.288V420.967V419.649V418.334V417.023V415.714V414.408V413.105V411.806V410.509V409.216V407.925V406.638V405.354V404.074V402.796V401.522V400.251V398.983V397.719V396.458V395.2V393.946V392.694V391.447V390.203V388.962V387.724V386.49V385.26V384.033V382.81V381.59V380.373V379.161V377.951V376.746V375.544V374.346V373.151V371.96V370.773V369.59V368.41V367.234V366.062V364.893V363.729V362.568V361.411V360.259V359.109V357.964V356.823V355.686V354.553V353.423V352.298V351.177V350.06V348.947V347.838V346.733V345.632V344.535V343.443V342.355V341.271V340.191V339.115V338.044V336.977V335.914V334.855V333.801V332.751V331.706V330.665V329.628V328.596V327.568V326.545V325.526V324.512V323.502V322.497V321.496V320.5V319.509V318.522V317.54V316.562V315.589V314.621V313.658V312.699V311.745V310.796V309.851V308.911V307.977V307.047V306.121V305.201V304.286V303.376V302.47V301.57V300.674V299.783V298.898V298.017V297.142V296.272V295.406V294.546V293.691V292.841V291.996V291.157V290.323V289.493V288.67V287.851V287.038V286.23V285.427V284.629V283.837V283.051V282.269V281.494V280.723V279.958V279.199V278.445V277.696V276.953V276.216V275.484V274.758V274.037V273.322V272.612V271.909V271.211V270.518V269.831V269.15V268.475V267.806V267.142V266.484V265.832V265.186V264.546V263.911V263.283V262.66V262.044V261.433V260.828V260.23V259.637V259.05V258.47V257.895V257.327V256.764V256.208V255.658V255.114V254.576V254.045V253.52V253.001V252.488V251.982V251.481V250.987V250.5V250.019V249.544V249.076V248.614V248.158V247.709C165 238.666 168.95 230.325 175.831 224.43L241.164 168.458C246.782 163.645 253.936 161 261.333 161H644C660.016 161 673 173.984 673 190Z'
        />
      </g>
      <circle cx='551' cy='644' r='56' fill='#0A2E3B' />
      <circle cx='551' cy='644' r='49' fill='#0E1F3C' stroke='currentColor' strokeWidth='2' />
      <defs>
        <filter
          id='filter0_f_1_55'
          x='0'
          y='0'
          width='842'
          height='1119'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feBlend mode='normal' in='SourceGraphic' in2='BackgroundImageFix' result='shape' />
          <feGaussianBlur stdDeviation='90' result='effect1_foregroundBlur_1_55' />
        </filter>
        <filter
          id='filter1_f_1_55'
          x='116.348'
          y='120'
          width='607'
          height='891'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feBlend mode='normal' in='SourceGraphic' in2='BackgroundImageFix' result='shape' />
          <feGaussianBlur stdDeviation='30' result='effect1_foregroundBlur_1_55' />
        </filter>
        <filter
          id='filter2_i_1_55'
          x='160'
          y='156'
          width='518'
          height='780'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feBlend mode='normal' in='SourceGraphic' in2='BackgroundImageFix' result='shape' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
            result='hardAlpha'
          />
          <feOffset dy='1' />
          <feGaussianBlur stdDeviation='1' />
          <feComposite in2='hardAlpha' operator='arithmetic' k2='-1' k3='1' />
          <feColorMatrix
            type='matrix'
            values='0 0 0 0 0.789226 0 0 0 0 0.230893 0 0 0 0 0.837777 0 0 0 0.25 0'
          />
          <feBlend mode='normal' in2='shape' result='effect1_innerShadow_1_55' />
        </filter>
        <filter
          id='filter3_i_1_55'
          x='160'
          y='156'
          width='518'
          height='780'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feBlend mode='normal' in='SourceGraphic' in2='BackgroundImageFix' result='shape' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
            result='hardAlpha'
          />
          <feOffset dy='1' />
          <feGaussianBlur stdDeviation='1' />
          <feComposite in2='hardAlpha' operator='arithmetic' k2='-1' k3='1' />
          <feColorMatrix
            type='matrix'
            values='0 0 0 0 0.789226 0 0 0 0 0.230893 0 0 0 0 0.837777 0 0 0 0.25 0'
          />
          <feBlend mode='normal' in2='shape' result='effect1_innerShadow_1_55' />
        </filter>
        <filter
          id='filter4_d_1_55'
          x='175'
          y='172'
          width='488'
          height='488'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
            result='hardAlpha'
          />
          <feOffset dy='1' />
          <feGaussianBlur stdDeviation='0.5' />
          <feComposite in2='hardAlpha' operator='out' />
          <feColorMatrix
            type='matrix'
            values='0 0 0 0 0.254502 0 0 0 0 0.650846 0 0 0 0 0.603285 0 0 0 0.25 0'
          />
          <feBlend mode='normal' in2='BackgroundImageFix' result='effect1_dropShadow_1_55' />
          <feBlend mode='normal' in='SourceGraphic' in2='effect1_dropShadow_1_55' result='shape' />
        </filter>
        <filter
          id='filter5_dd_1_55'
          x='162'
          y='162'
          width='514'
          height='516'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
            result='hardAlpha'
          />
          <feOffset dy='6' />
          <feGaussianBlur stdDeviation='6' />
          <feComposite in2='hardAlpha' operator='out' />
          <feColorMatrix type='matrix' values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.55 0' />
          <feBlend mode='normal' in2='BackgroundImageFix' result='effect1_dropShadow_1_55' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
            result='hardAlpha'
          />
          <feOffset dy='2' />
          <feGaussianBlur stdDeviation='5' />
          <feComposite in2='hardAlpha' operator='out' />
          <feColorMatrix
            type='matrix'
            values='0 0 0 0 0.521569 0 0 0 0 0.152941 0 0 0 0 0.831373 0 0 0 0.4 0'
          />
          <feBlend mode='normal' in2='effect1_dropShadow_1_55' result='effect2_dropShadow_1_55' />
          <feBlend mode='normal' in='SourceGraphic' in2='effect2_dropShadow_1_55' result='shape' />
        </filter>
        <pattern
          id={`pattern0_1_55${id}`}
          patternContentUnits='objectBoundingBox'
          width='1'
          height='1'
        >
          <use href={`#image-${id}`} transform='scale(0.000976562)' />
        </pattern>
        <filter
          id='filter6_f_1_55'
          x='185.178'
          y='167.5'
          width='481.322'
          height='490.971'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feBlend mode='normal' in='SourceGraphic' in2='BackgroundImageFix' result='shape' />
          <feGaussianBlur stdDeviation='6' result='effect1_foregroundBlur_1_55' />
        </filter>
        <filter
          id='filter7_i_1_55'
          x='160'
          y='156'
          width='518'
          height='780'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feBlend mode='normal' in='SourceGraphic' in2='BackgroundImageFix' result='shape' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
            result='hardAlpha'
          />
          <feOffset dy='1' />
          <feGaussianBlur stdDeviation='1' />
          <feComposite in2='hardAlpha' operator='arithmetic' k2='-1' k3='1' />
          <feColorMatrix
            type='matrix'
            values='0 0 0 0 0.789226 0 0 0 0 0.230893 0 0 0 0 0.837777 0 0 0 0.25 0'
          />
          <feBlend mode='normal' in2='shape' result='effect1_innerShadow_1_55' />
        </filter>
        <filter
          id='filter8_d_1_55'
          x='158'
          y='158'
          width='522'
          height='783'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
            result='hardAlpha'
          />
          <feOffset dy='4' />
          <feGaussianBlur stdDeviation='3' />
          <feComposite in2='hardAlpha' operator='out' />
          <feColorMatrix
            type='matrix'
            values='0 0 0 0 0.521569 0 0 0 0 0.152941 0 0 0 0 0.831373 0 0 0 0.95 0'
          />
          <feBlend mode='normal' in2='BackgroundImageFix' result='effect1_dropShadow_1_55' />
          <feBlend mode='normal' in='SourceGraphic' in2='effect1_dropShadow_1_55' result='shape' />
        </filter>
        <radialGradient
          id={`paint0_radial_1_55${id}`}
          cx='0'
          cy='0'
          r='1'
          gradientUnits='userSpaceOnUse'
          gradientTransform='translate(556 667) rotate(90) scale(285)'
        >
          <stop stopColor='currentColor' />
          <stop offset='1' stopColor='#737373' stopOpacity='0' />
        </radialGradient>
        <radialGradient
          id='paint1_radial_1_55'
          cx='0'
          cy='0'
          r='1'
          gradientUnits='userSpaceOnUse'
          gradientTransform='translate(271 839) rotate(90) scale(372)'
        >
          <stop stopColor='#8527D4' />
          <stop offset='1' stopColor='#737373' stopOpacity='0' />
        </radialGradient>
        <linearGradient
          id='paint2_linear_1_55'
          x1='654'
          y1='180'
          x2='157.5'
          y2='554'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='white' />
          <stop offset='1' stopColor='#999999' stopOpacity='0' />
        </linearGradient>
        <linearGradient
          id='paint3_linear_1_55'
          x1='187'
          y1='185'
          x2='500'
          y2='172'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#101010' />
          <stop offset='1' stopColor='white' />
        </linearGradient>
        <radialGradient
          id='paint4_radial_1_55'
          cx='0'
          cy='0'
          r='1'
          gradientUnits='userSpaceOnUse'
          gradientTransform='translate(662 164) rotate(90) scale(285)'
        >
          <stop stopColor='#D9D9D9' />
          <stop offset='1' stopColor='#737373' stopOpacity='0' />
        </radialGradient>
        <linearGradient
          id='paint5_linear_1_55'
          x1='503'
          y1='59'
          x2='195'
          y2='917'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#083A74' stopOpacity='0' />
          <stop offset='1' stopColor='#01070E' />
        </linearGradient>
        <image id={`image-${id}`} width='1024' height='1024' href={tokenURI} />
      </defs>
    </svg>
  );
}

export default Card;
