import React, { useState } from 'react';
import styles from './NFTCard.module.scss';
import clsx from 'clsx';

import romanize from 'app/utils/romanize';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import { getNftSlotIcon } from 'lib/NFT/utils/getNftSlotIcon.tsx';
import RotateIcon from './icon/Rotate.tsx';
import NftCardParams from 'lib/NFT/components/NftCardParams/NftCardParams.tsx';
import NftBackSvg from './icon/NftBack.svg.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { alpha } from '@mui/material/styles';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import Box, { BoxProps } from '@mui/material/Box';
import NftCardBack from 'lib/NFT/components/NftCardBack/NftCardBack.tsx';
import NftCardBackRecipe from 'lib/NFT/components/NftCardBackRecipe/NftCardBackRecipe.tsx';
import NftFrontSvg from './icon/NftFront.svg.tsx';
import NftFrontDashed from 'lib/NFT/components/NFTCard/icon/NftFront.dashed.tsx';

export type NftItemProps = {
  className?: string;
  onClick?: () => void;
  nft?: INftItemDto;
  loading?: boolean;
  size?: 'small' | 'medium';
} & BoxProps;

function NFTCard(props: NftItemProps) {
  const { onClick, className = '', nft, loading = false, size, ...other } = props;
  const { type, mods: nftMods } = nft;
  const { name, rarity, slots = [], collection: collectionObj } = type;
  const [isFlipped, setIsFlipped] = useState(false);

  const isRecipe = !!nft?.type?.recipe;
  const slotValue = slots?.[0]?.slot;
  const rarityName = getNftRarityLabel(rarity);
  const SlotIcon = getNftSlotIcon(slotValue);
  const collectionLabel = collectionObj ? romanize(collectionObj.index) : 0;
  const droneRecipe = nft.type?.droneRecipe;
  const minerDroneType = nft.type.minerDroneType || droneRecipe?.resultType?.minerDroneType;

  const imageNft = nft.type.uri;
  const rarityColor = getNftRarityColor(rarity);

  const handleClick = () => {
    setIsFlipped(!isFlipped);
  };

  return (
    <Box className={clsx(styles.nft, { [styles.small]: size === 'small' })} {...other}>
      <div className={`${styles.card} ${isFlipped ? styles.isFlipped : ''}`} onClick={handleClick}>
        <Box
          className={clsx(styles.card__face, className)}
          style={
            isRecipe
              ? undefined
              : {
                  background: isRecipe ? undefined : rarityColor,
                  bgcolor: isRecipe ? 'background.paper' : undefined,
                  boxShadow: `0 0 89px 0 ${alpha(rarityColor, 0.2)}`,
                }
          }
          onClick={onClick}
        >
          {!isRecipe && <NftFrontSvg color={rarityColor} />}
          {isRecipe && (
            <NftFrontDashed
              sx={{ width: '100%', height: '100%', padding: '1px', color: rarityColor }}
            />
          )}

          <div className={styles.card__content}>
            <Typography className={styles.cardCollection} color='text.primary'>
              {collectionLabel}
            </Typography>
            <div className={styles.shine2} />
            <div className={styles.nft_image__wrapper}>
              <img
                className={clsx(styles.nft__image, styles.nft__image_mask)}
                src={imageNft}
                alt=''
              />
              <div className={styles.image_glow__wrapper}>
                <div className={styles.nft_image__glow} />
              </div>
            </div>
            <div className={styles.glow} />

            <div className={styles.card_slot}>
              <div className={styles.slot__inner} style={{ color: rarityColor }}>
                {SlotIcon}
              </div>
            </div>
            <div className={styles.cardInfo}>
              <div className={styles.cardInfoRarity}>
                <Typography variant='caption' style={{ color: rarityColor }}>
                  {rarityName}
                </Typography>
              </div>
              <div className={styles.cardInfoTitle}>
                <Typography align='left' variant='body1' sx={{ color: '#ffffff' }}>
                  {name}
                </Typography>
              </div>
              <NftCardParams
                mods={nft.type.mods || []}
                minerDroneType={minerDroneType}
                maxParams={2}
                loading={loading}
              />
            </div>
            <div className={styles.rotate__icon}>
              <RotateIcon />
            </div>
          </div>
        </Box>

        <Box
          className={styles.card__back}
          sx={{
            background: isRecipe ? undefined : rarityColor,
            bgcolor: isRecipe ? 'background.paper' : undefined,
            boxShadow: `0 0 89px 0 ${alpha(rarityColor, 0.2)}`,
          }}
        >
          {!isRecipe && <NftBackSvg />}
          {isRecipe && (
            <NftFrontDashed
              sx={{ width: '100%', height: '100%', padding: '1px', color: rarityColor }}
            />
          )}

          <div className={styles.card__content}>
            <Box
              className={styles.cardCollection}
              sx={{ color: isRecipe ? 'text.primary' : 'background.default' }}
            >
              {collectionLabel}
            </Box>
            <div className={styles.shine2} />

            {!droneRecipe && (
              <NftCardBack mods={nft.type.mods} loading={loading} minerDroneType={minerDroneType} />
            )}
            {droneRecipe && <NftCardBackRecipe recipe={droneRecipe} />}

            <div className={styles.rotate__icon}>
              <RotateIcon />
            </div>

            <Typography variant='caption' sx={{ position: 'absolute', bottom: 8, left: 12 }}>
              Token: #{nft?.assetId}
            </Typography>
          </div>
        </Box>
      </div>
    </Box>
  );
}

export default NFTCard;
