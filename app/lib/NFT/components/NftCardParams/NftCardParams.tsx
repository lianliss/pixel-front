import React from 'react';
import styles from './NftCardParams.module.scss';
import { INftStatDto } from 'lib/NFT/api-server/nft.types.ts';
import { useTranslation } from 'react-i18next';
import CircularProgress from '@mui/material/CircularProgress';
import { IMinerClaimerTypeDto } from 'lib/MinerClaimer/api-server/miner-claimer.types.ts';
import { getNftParams } from 'lib/NFT/utils/get-nft-params.ts';
import NftCardParamRender from 'lib/NFT/components/NftCardParam/NftCardParam.tsx';

interface NftInfoParamsProps {
  mods: INftStatDto[];
  maxParams?: number;
  minerDroneType?: IMinerClaimerTypeDto;
  loading?: boolean;
}

const NftCardParams = ({
  mods = [],
  maxParams,
  minerDroneType,
  loading = false,
}: NftInfoParamsProps) => {
  const params = React.useMemo(
    () => getNftParams({ type: { mods, minerDroneType } } as any),
    [mods, minerDroneType]
  );
  const showParams = params.sort((a, b) => Number(b.isBuff) - Number(a.isBuff)).slice(0, maxParams);
  let restParamsCount = maxParams ? mods.length - maxParams : 0;

  return (
    <div className={styles.cardInfoParams}>
      {loading && <CircularProgress size={30} sx={{ mx: 'auto' }} />}

      {!loading &&
        showParams.map((param, paramIndex) => {
          return <NftCardParamRender {...param} key={`param-${paramIndex}`} />;
        })}
      {!loading && restParamsCount > 0 && (
        <span className={styles.cardInfoParamsItemTitle}>And other {restParamsCount}</span>
      )}
    </div>
  );
};

export default NftCardParams;
