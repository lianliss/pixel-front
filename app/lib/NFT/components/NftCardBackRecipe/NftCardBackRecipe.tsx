import styles from 'lib/NFT/components/NFTCard/NFTCard.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import React, { useMemo } from 'react';
import Stack from '@mui/material/Stack';
import { INftRecipeDto } from 'lib/Forge/api-server/miner-claimer.types.ts';
import NftCardRecipeParam from 'lib/NFT/components/NftCardRecipeParam/NftCardRecipeParam.tsx';
import Divider from '@mui/material/Divider';

type Props = {
  recipe: INftRecipeDto;
  loading?: boolean;
};

function NftCardBackRecipe(props: Props) {
  const { recipe, loading = false } = props;

  const arr = useMemo(() => {
    return recipe.requirements.map((c, i) => ({ rarity: i, count: c })).filter((el) => !!el.count);
  }, [recipe]);
  const slots = recipe.slots;

  return (
    <div className={styles.cardFeatures}>
      <div className={styles.cardFeaturesTitle}>
        <Typography variant='body1' fontWeight='bold'>
          Required for Craft
        </Typography>
      </div>

      <Stack direction='column' divider={<Divider />} gap={1} sx={{ mt: 2 }}>
        {arr.map((el) => (
          <NftCardRecipeParam count={el.count} slots={slots} rarity={el.rarity} />
        ))}
      </Stack>
    </div>
  );
}

export default NftCardBackRecipe;
