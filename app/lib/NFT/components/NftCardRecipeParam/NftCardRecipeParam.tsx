import React from 'react';
import { NftItemRarity, NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';
import { styled } from '@mui/material/styles';
import { getNftSlotIcon } from 'lib/NFT/utils/getNftSlotIcon.tsx';
import { getNftSlotLabel } from 'lib/NFT/utils/getNftSlotLabel.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import Box from '@mui/material/Box';

const Root = styled('div')(() => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',

  '& > div': {
    display: 'flex',
    alignItems: 'center',
    gap: 6,
  },
}));

type Props = { slots?: NftItemSlot[]; count: number; rarity: NftItemRarity };

function NftCardRecipeParam({ slots = [], count, rarity }: Props) {
  const uniqueSlots = slots.filter(
    (el) => ![NftItemSlot.Pocket1, NftItemSlot.Pocket2, NftItemSlot.Pocket3].includes(el)
  );
  const firstIcon = getNftSlotIcon(uniqueSlots[0]);
  const secondIcon = uniqueSlots[1] ? getNftSlotIcon(uniqueSlots[1]) : undefined;

  return (
    <Root>
      <div>
        {!!firstIcon && (
          <Box sx={{ '& > svg': { width: 26, height: 26 } }}>
            {firstIcon}
            {secondIcon}
          </Box>
        )}
        <div>
          <Typography variant='subtitle2' fontWeight='bold' sx={{ lineHeight: '18px' }}>
            NFT
          </Typography>
          <Typography variant='caption' sx={{ fontSize: 11 }} color='text.secondary'>
            Rarity:{' '}
            <span style={{ color: getNftRarityColor(rarity) }}>{getNftRarityLabel(rarity)}</span>
          </Typography>
        </div>
      </div>

      <Typography fontWeight='bold' sx={{ fontSize: 20 }}>
        {count}
      </Typography>
    </Root>
  );
}

export default NftCardRecipeParam;
