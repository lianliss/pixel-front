import styles from 'lib/NFT/components/NFTCard/NFTCard.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import NftCardParams from 'lib/NFT/components/NftCardParams/NftCardParams.tsx';
import React from 'react';
import { INftStatDto } from 'lib/NFT/api-server/nft.types.ts';
import { IMinerClaimerTypeDto } from 'lib/MinerClaimer/api-server/miner-claimer.types.ts';

type Props = {
  mods: INftStatDto[];
  minerDroneType?: IMinerClaimerTypeDto;
  loading?: boolean;
};

function NftCardBack(props: Props) {
  const { mods = [], loading, minerDroneType } = props;

  return (
    <div className={styles.cardFeatures}>
      <div className={styles.cardFeaturesTitle}>
        <Typography variant='body1' fontWeight='bold'>
          Nft Features
        </Typography>
      </div>
      <NftCardParams mods={mods} loading={loading} minerDroneType={minerDroneType} />
    </div>
  );
}

export default NftCardBack;
