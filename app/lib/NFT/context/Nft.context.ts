import React from 'react';

export type INftContext = {
  itemTypes: any[];
  address: string;
};

export const NftContext = React.createContext<INftContext>(null);

export function useNft(): INftContext {
  return React.useContext(NftContext);
}
