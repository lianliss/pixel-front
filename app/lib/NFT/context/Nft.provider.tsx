import React from 'react';
import { NftContext } from 'lib/NFT/context/Nft.context.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useNftTypes } from 'lib/NFT/hooks/useNftTypes.ts';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';

function NftProvider(props: React.PropsWithChildren) {
  const account = useAccount();

  const nftAddress = NFT_CONTRACT_ID[account.chainId];

  const itemTypes = useNftTypes();

  return (
    <NftContext.Provider
      value={{
        itemTypes: itemTypes.data,
        address: nftAddress?.toLowerCase(),
      }}
    >
      {props.children}
    </NftContext.Provider>
  );
}

export default NftProvider;
