const map = {
  0: 'Drill speed',
  1: 'Storage capacity',
  2: 'Referral storage capacity',
  3: 'PXLs per hour',
  4: 'Storage fixed capacity',
  5: 'Referral storage capacity',
  6: 'Drill upgrade discount',
  7: 'Storage upgrade discount',
  8: 'Referral storage discount',
  16: 'Second miner speed',
  17: 'Market commission order discount',
  18: 'Market commission publication discount',
  19: 'Increased chance of rare drop',
  20: 'Transaction cost markup',
  21: 'Dust per hour',
  22: 'Mission duration',
  23: 'Missions count',
  24: 'Upgrade cost discount',
};
const markup = {
  6: 'Drill upgrade markup',
  7: 'Storage upgrade markup',
  8: 'Referral storage markup',
  17: 'Market commission order markup',
  18: 'Market commission publication markup',
  20: 'Transaction cost discount',
  24: 'Drone: upgrade cost markup',
};
const markupKeys = Object.keys(markup).map((k) => Number(k));

export function getNftStatLabel(parameterIndex, value = 0) {
  if (markupKeys.includes(parameterIndex) && value >= 100) {
    return markup[parameterIndex] || `Unknown bonus ${parameterIndex}`;
  }

  return map[parameterIndex] || `Unknown bonus ${parameterIndex}`;
}
