export const convertSlotItemType = (type) => {
  return {
    typeId: Number(type.typeId),
    collectionId: Number(type.collectionId),
    name: type.name,
    tokenURI: type.tokenURI,
    rarity: Number(type.rarity),
    slots: type.slots.map((slot) => Number(slot)),
    variants: type.variants.map((v) =>
      v.map((p) => ({
        add: p.add,
        mul: p.mul,
        parameterId: Number(p.parameterId),
      }))
    ),
    soulbound: type.soulbound,
    disposable: type.disposable,
  };
};
