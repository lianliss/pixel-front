import { Chain } from 'services/multichain/chains';

const map = {
  [Chain.UNITS]: `units-pixel`,
  [Chain.SKALE]: `skale-pixel`,
  [Chain.SONGBIRD]: `sgb-pixel`,
};

export function getNftTypeId(chainId: number, typeId: number): string {
  return map[chainId] + '-' + typeId;
}
