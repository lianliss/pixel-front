import { INftSlotDto, INftStatDto, INftTypeDto } from 'lib/NFT/api-server/nft.types.ts';
import { isNftModBuff } from 'lib/NFT/components/NFTCard/convertToNftItemDto.ts';

export function createNftTypeFromChain(type: any): INftTypeDto {
  const nftItemDto: INftTypeDto = {
    ...type,
    uri: type.tokenURI,
    name: type.name,
    mods:
      type.variants[0]?.map(
        (variant): INftStatDto => ({
          modId: String(variant.parameterId),
          add: variant.add,
          mult: variant.mul,
          buff: isNftModBuff({ add: variant.add, mult: variant.mul, modId: variant.parameterId }),
        })
      ) || [],
    slots: type.slots.map((id): INftSlotDto => ({ slot: id })),
  };

  return nftItemDto;
}
