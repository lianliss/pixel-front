import { INftStatDto } from 'lib/NFT/api-server/nft.types.ts';
import { wei } from 'utils/index';

export function isNftStatPercent(modId: string): boolean {
  return (+modId < 3 || +modId > 5) && !['22', '21'].includes(modId);
}

export function isNftStatDuration(modId: string) {
  return ['22'].includes(modId);
}

export function isNftStatCount(modId: string) {
  return ['23'].includes(modId);
}
export function isNftStatDust(modId: string) {
  return ['21'].includes(modId);
}

export function getNftStatsValue(mod: INftStatDto): number {
  if (isNftStatDuration(mod.modId)) {
    return Number(mod.add) / 60 / 60;
  }
  if (isNftStatCount(mod.modId)) {
    return mod.add;
  }

  let value = mod.add ? wei.from(mod.add) : wei.from(mod.mult, 4);

  const isPercent = isNftStatPercent(mod.modId);

  if (isPercent) {
    value -= 1;
    return +(value * 100).toFixed(2);
  }
  if (['3'].includes(mod.modId.toString())) {
    return +(value * 3600).toFixed(3);
  }

  return +value.toFixed(4);
}
