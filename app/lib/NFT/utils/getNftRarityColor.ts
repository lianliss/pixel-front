import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';

const map: Record<NftItemRarity, string> = {
  5: '#FF7A00',
  4: '#8527D4',
  3: '#0094FF',
  2: '#05C618',
  1: '#ffffff',
};

export function getNftRarityColor(rarity: NftItemRarity) {
  return map[rarity] || map['1'];
}
