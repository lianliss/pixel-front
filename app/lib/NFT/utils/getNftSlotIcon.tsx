import React from 'react';
import { NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';
import SlotArtifact from 'lib/NFT/components/icons/SlotArtifact';
import SlotBelt from 'lib/NFT/components/icons/SlotBelt';
import SlotBackpack from 'lib/NFT/components/icons/SlotBackpack';
import SlotPocket from 'lib/NFT/components/icons/SlotPocket';
import SlotDrill from 'lib/NFT/components/icons/SlotDrill.jsx';
import SlotEmpty from 'lib/NFT/components/icons/SlotEmpty';
import SlotBack from 'lib/NFT/components/icons/SlotBack';
import SlotLoyaltyCard from 'lib/NFT/components/icons/SlotLoyaltyCard.jsx';
import SlotDroneCraft from 'lib/NFT/components/icons/SlotDroneCraft';
import SlotDrone from 'lib/NFT/components/icons/SlotDrone';
import SlotSmartClaimer from 'lib/NFT/components/icons/SlotSmartClaimer.jsx';
import SlotRing from 'lib/NFT/components/icons/SlotRing';

const map: Record<NftItemSlot, JSX.Element> = {
  9: <SlotLoyaltyCard />,
  8: <SlotBack />,
  7: <SlotDrill />,
  6: <SlotArtifact />,
  2: <SlotBackpack />,
  4: <SlotBelt />,

  5: <SlotPocket />,
  3: <SlotPocket />,
  1: <SlotPocket />,
  0: <SlotPocket />,

  10: <SlotSmartClaimer />,
  11: <SlotDroneCraft />,
  12: <SlotRing />,
  13: <SlotDrone />,
  16: <SlotEmpty />,
  // 9: <SlotBattery />,
  // 11: <SlotDroneAccessories />,
  // 12: <SlotDroneCraft />,
  // 13: <SlotExtruder />,
  // 14: <SlotSynthesisBlock />,
  // 15: <SlotTraining />,
};

export function getNftSlotIcon(slot: NftItemSlot): React.ReactElement {
  return map[slot] || <></>;
}
