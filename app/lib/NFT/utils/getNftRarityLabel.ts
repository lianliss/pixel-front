import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';

const map: Record<NftItemRarity, string> = {
  [NftItemRarity.Common]: 'Common',
  [NftItemRarity.UnCommon]: 'Uncommon',
  [NftItemRarity.Rare]: 'Rare',
  [NftItemRarity.Epic]: 'Epic',
  [NftItemRarity.Legendary]: 'Legendary',
};

export function getNftRarityLabel(rarity: NftItemRarity) {
  return map[rarity] || 'unknown';
}
