import { Chain } from 'services/multichain/chains';

const prefixes = {
  [Chain.SKALE]: 'skale-pixel-',
  [Chain.SONGBIRD]: 'sgb-pixel-',
  [Chain.SKALE_TEST]: 'skale-testnet-pixel-',
  [Chain.FLARE]: 'flare-pixel-',
  [Chain.UNITS]: 'units-pixel-',
};
export function getNftTypeId(typeId: number, chainId: number): string {
  return prefixes[chainId] + typeId;
}
