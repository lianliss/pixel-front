import { INftItemDto, INftTypeDto } from 'lib/NFT/api-server/nft.types.ts';

export function createNftFromType(
  type: INftTypeDto,
  payload: { tokenId?: number; contractId?: string } = {}
): INftItemDto {
  const nftItemDto: INftItemDto = {
    contractId: (payload.contractId || '').toLowerCase(),
    id: 'null',
    holder: 'null',
    assetId: typeof payload.tokenId === 'undefined' ? '' : payload.tokenId.toString(),
    typeId: type.id,
    type: type,
    burnedAt: undefined,
    createdAt: new Date().getTime(),
  };

  return nftItemDto;
}
