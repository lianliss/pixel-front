import { NftItemCollection } from 'lib/NFT/api-server/nft.types.ts';

const map: Record<NftItemCollection, string> = {
  [NftItemCollection.Ogc]: 'OGC',
};

export function getNftCollection(collection: NftItemCollection): string {
  return map[collection];
}
