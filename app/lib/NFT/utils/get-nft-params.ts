import { INftCardParamData } from 'lib/NFT/components/NftCardParam/NftCardParam.tsx';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import {
  getNftStatsValue,
  isNftStatCount,
  isNftStatDuration,
  isNftStatDust,
  isNftStatPercent,
} from 'lib/NFT/utils/getNftStatsValue.ts';
import { getNftStatLabel } from 'lib/NFT/utils/getNftStatLabel';
import { getMinerClaimerSessionTImeStr } from 'lib/MinerClaimer/utils/getMinerClaimerSessionTImeStr.ts';
import { getLocaleTime } from 'utils/format/date';

export function getNftParams(nft: INftItemDto): INftCardParamData[] {
  const stats = nft.type.mods;
  const droneType = nft.type.minerDroneType;

  const result: INftCardParamData[] = [];

  for (const param of stats) {
    const value = getNftStatsValue({
      modId: param.modId,
      mult: param.mult,
      add: param.add,
      buff: param.buff,
    });

    const label = getNftStatLabel(param.modId, 100 + Number(value));
    const isPercent = isNftStatPercent(param.modId);
    const isDuration = isNftStatDuration(param.modId);
    const isCount = isNftStatCount(param.modId);
    const isDust = isNftStatDust(param.modId);
    const isBuff = param.buff;

    result.push({ label, isBuff, isCount, isDuration, isDust, value, isPercent });
  }

  if (droneType) {
    const isTimed = droneType.isTimeSession;

    if (!isTimed) {
      result.push({ label: 'Duration', value: `${droneType.sessionClaims} ${'claims'}` });
    } else {
      result.push({
        label: 'Duration',
        value: getMinerClaimerSessionTImeStr(droneType.sessionDuration),
      });
    }

    if (+droneType.duration > 0) {
      result.push({ label: 'Durability', value: `${getLocaleTime(+droneType.duration)}` });
    }
    if (1 - droneType.txPriceMod || 0) {
      result.push({
        label: 'Commission discount',
        value: `-${((1 - droneType.txPriceMod || 0) * 100).toFixed(2)}%`,
      });
    }
  }

  return result;
}
