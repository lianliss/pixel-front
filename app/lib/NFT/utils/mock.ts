import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import pixelImage from 'assets/img/digital-soul/pixel.png';

const getRandomName = () => `NFT ${Math.fround(Math.random() * 1000)}`;
let index = 0;

export const getMockNft = (): INftItemDto => {
  const id = (index++).toString();
  return {
    type: {
      createdAt: new Date().getTime(),
      id,
      uri: pixelImage as string,
      name: getRandomName(),
      rarity: 2,
      mods: [],
      slots: [],
      collectionId: 1,
      droneMiner: false,
      contractId: '',
      collection: { name: 'test', id: '1', createdAt: new Date().getTime(), index: '0' },
      minerDroneType: null,
      droneRecipe: null,
    },
    id,
    typeId: id,
    assetId: id,
    holder: '',
    createdAt: new Date().getTime(),
  };
};

export const getMockNftDroneRecipe = (): INftItemDto => {
  const nft = getMockNft();
  return {
    ...nft,
    type: {
      ...nft.type,
      droneRecipe: {
        id: '1',
        createdAt: new Date().getTime(),
        droneTypeId: '',
        recipeType: null,
        recipeTypeId: '',
        removedAt: null,
        requirements: [2, 1],
        droneType: {
          name: 'test',
          droneRecipe: null,
          droneMiner: true,
          collection: null,
          collectionId: 1,
          id: '1',
          contractId: '',
          uri: pixelImage as string,
          rarity: 2,
          createdAt: new Date().getTime(),
          minerDroneType: {
            createdAt: new Date().getTime(),
            typeId: '1',
            removedAt: null,
            duration: 60 * 60 * 24 * 15 * 1000,
            durability: 1000,
            id: '1',
          },
          mods: [],
          slots: [],
          soulbound: false,
        },
      },
    },
  };
};
