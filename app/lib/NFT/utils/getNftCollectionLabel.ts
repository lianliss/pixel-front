import { Chain } from 'services/multichain/chains';

const map: Record<number, Record<number, string>> = {
  [Chain.SONGBIRD]: {
    // 0: 'Developer\'s Edition',
    1: 'Ref Whale',
    2: 'Uniq Collection',
    3: 'OGCommunity',
    4: 'Web3 Epic Collection',
    5: 'Marketplace Collection',
    8: 'Event Collection',
    6: 'Blockchain Dominator',
    7: 'Smart Claimer',
  },
  [Chain.SKALE]: {
    // 0: 'Developer\'s Edition',
    1: 'Ref Whale',
    2: 'Uniq Collection',
    3: 'OGCommunity',
    4: 'Web3 Epic Collection',
  },
};

export function getNftCollectionLabel(chainId: number, collection: number) {
  const chainMap = map[chainId] || map[Chain.SONGBIRD];

  return chainMap[collection] || 'unknown';
}
