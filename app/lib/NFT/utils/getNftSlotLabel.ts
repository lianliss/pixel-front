import { NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';

const map: Record<NftItemSlot, string> = {
  9: 'Loyalty Card',
  8: 'Back',
  7: 'Drill',
  6: 'Artifact',
  4: 'Belt',
  2: 'Backpack',

  3: 'Pocket',
  5: 'Pocket',
  1: 'Pocket',
  0: 'Pocket',

  10: 'Smart Claimer',
  11: 'Recipe',
  12: 'Ring',
  13: 'Drone',
  16: 'unknown',
};

export function getNftSlotLabel(slot: NftItemSlot) {
  return map[slot] || 'unknown';
}
