import { IPaginatedQuery, IPaginatedResult } from 'lib/Marketplace/api-server/market.types.ts';
import { IMinerClaimerTypeDto } from 'lib/MinerClaimer/api-server/miner-claimer.types.ts';
import { INftRecipeDto } from 'lib/Forge/api-server/miner-claimer.types.ts';

export enum NftItemCollection {
  Ogc = 3,
}

export enum NftItemRarity {
  Common = 1,
  UnCommon = 2,
  Rare = 3,
  Epic = 4,
  Legendary = 5,
}

export enum NftItemSlot {
  Belt = 4,
  Artifact = 6,
  Backpack = 2,
  Pocket0 = 0,
  Pocket1 = 1,
  Pocket2 = 5,
  Pocket3 = 3,
  Drill = 7,
  Back = 8,
  LoyaltyCard = 9,
  MinerDrone = 10,
  Recipe = 11,
  Ring = 12,
  Drone = 13,

  Empty = 16,
}

export type INftStatDto<TDate = number> = {
  modId: string;

  mult: number;
  add: number;
  buff: boolean;
};

export type INftSlotDto<TDate = number> = {
  slot: number;
};

export type INftCollectionDto<TDate = number> = {
  id: string;

  name: string;
  index: string;

  createdAt: TDate;
};

export type INftTypeDto<TDate = number> = {
  id: string;

  contractId: string;

  name: string;
  uri: string;
  collectionId: number;
  rarity: number;
  soulbound?: boolean;
  disposable?: boolean;
  droneMiner: boolean;
  recipe: boolean;

  createdAt: TDate;

  // relations

  collection: INftCollectionDto | null;
  slots: INftSlotDto[] | null;
  mods: INftStatDto[] | null;
  minerDroneType: IMinerClaimerTypeDto | null;
  droneRecipe: INftRecipeDto<TDate> | null;
};

export type INftItemDto<TDate = number> = {
  id: string;

  holder: string;
  assetId: string;
  contractId: string;
  typeId: string;

  burnedAt?: TDate;
  createdAt: TDate;

  // relations
  type: INftTypeDto | null;
};

// request

export type IGetNftItemDto = { assetId: string; contractId: string };

export type IFindNftItemsDto = {
  holder: string;
  contractId: string;
  rarities?: NftItemRarity[];
  slots?: NftItemSlot[];
  mods?: string[];
  typeId?: string;
  soulbound?: boolean;
} & IPaginatedQuery;

export type IGetNftItemResponse = INftItemDto;

export type IFindNftItemsResponse = IPaginatedResult<INftItemDto>;

export type INftStatusResponse = {
  total: number;
  rarity1: number;
  rarity2: number;
  rarity3: number;
  rarity4: number;
  rarity5: number;

  slot0: number;
  slot1: number;
  slot2: number;
  slot3: number;
  slot4: number;
  slot5: number;
  slot6: number;
  slot7: number;
  slot8: number;

  collection0: number;
};

export type IGetNftUserDto = {
  userAddress: string;
  contract: string;
};

export type IFindNftTypesDto = {
  limit: number;
  offset: number;
  contractId?: string;
  collectionId?: string;
  rarity?: NftItemRarity;
};
export type IFindNftCollectionsDto = {
  limit: number;
  offset: number;
  contractId?: string;
};

export type IGetNftTypeDto = { typeId: string };

// response

export type IGetNftUserResponse = {
  total: number;
  rarity1: number;
  rarity2: number;
  rarity3: number;
  rarity4: number;
  rarity5: number;
};

export type IFindNftTypesResponse = IPaginatedResult<INftTypeDto>;

export type IFindNftCollectionsResponse = IPaginatedResult<INftCollectionDto>;

export type IGetNftTypeResponse = INftTypeDto;
