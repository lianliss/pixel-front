'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  IFindNftCollectionsDto,
  IFindNftCollectionsResponse,
  IFindNftItemsDto,
  IFindNftItemsResponse,
  IFindNftTypesDto,
  IFindNftTypesResponse,
  IGetNftItemDto,
  IGetNftItemResponse,
  IGetNftTypeDto,
  IGetNftTypeResponse,
  IGetNftUserDto,
  IGetNftUserResponse,
  INftStatusResponse,
  NftItemRarity,
  NftItemSlot,
} from 'lib/NFT/api-server/nft.types.ts';
import { IMarketStatusResponse } from 'lib/Marketplace/api-server/market.types.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class NftApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  public async get(payload: IGetNftItemDto): Promise<IGetNftItemResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetNftItemResponse },
      AxiosResponse<{ data: IGetNftItemResponse }>,
      IGetNftItemDto
    >(`/api/v1/nft/get`, { baseURL: this.url, params: payload });
    return data.data;
  }

  async find({
    limit = 10,
    offset = 0,
    contractId,
    holder,
    soulbound,
    rarities,
    slots,
    mods,
  }: IFindNftItemsDto): Promise<IFindNftItemsResponse> {
    const { data } = await axiosInstance.post<
      { data: IFindNftItemsResponse },
      AxiosResponse<{ data: IFindNftItemsResponse }>,
      IFindNftItemsDto
    >(
      `/api/v1/nft/find`,
      { limit, offset, contractId, holder: holder.toLowerCase(), soulbound, rarities, slots, mods },
      { baseURL: this.url }
    );
    return data.data;
  }

  async status(nftContract: string): Promise<INftStatusResponse> {
    const { data } = await axiosInstance.get<
      { data: INftStatusResponse },
      AxiosResponse<{ data: INftStatusResponse }>,
      { key: string }
    >(`/api/v1/nft/${nftContract}/stats`, {
      baseURL: this.url,
    });
    return data.data;
  }

  async getNftUser(payload: IGetNftUserDto): Promise<IGetNftUserResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetNftUserResponse },
      AxiosResponse<{ data: IGetNftUserResponse }>,
      IGetNftUserDto
    >(`/api/v1/nft/user`, {
      baseURL: this.url,
      params: {
        userAddress: payload.userAddress,
        contract: payload.contract,
      },
    });

    return data.data;
  }

  // nft type

  async getNftType(payload: IGetNftTypeDto): Promise<IGetNftTypeResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetNftTypeResponse },
      AxiosResponse<{ data: IGetNftTypeResponse }>,
      IGetNftTypeDto
    >(`/api/v1/nft/type/get`, {
      baseURL: this.url,
      params: payload,
    });

    return data.data;
  }

  async findNftTypes(payload: IFindNftTypesDto): Promise<IFindNftTypesResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindNftTypesResponse },
      AxiosResponse<{ data: IFindNftTypesResponse }>,
      IFindNftTypesDto
    >(`/api/v1/nft/type/find`, {
      baseURL: this.url,
      params: payload,
    });

    return data.data;
  }

  // collection

  async findNftCollections(payload: IFindNftCollectionsDto): Promise<IFindNftCollectionsResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindNftCollectionsResponse },
      AxiosResponse<{ data: IFindNftCollectionsResponse }>,
      IFindNftCollectionsDto
    >(`/api/v1/nft/collection/find`, {
      baseURL: this.url,
      params: payload,
    });

    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const nftApi = new NftApi({ url: 'https://api.hellopixel.network' });
