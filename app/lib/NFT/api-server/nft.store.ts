import { createEffect, createEvent, createStore } from 'effector';
import { nftApi } from 'lib/NFT/api-server/nft.api.ts';
import {
  IFindNftCollectionsDto,
  IFindNftCollectionsResponse,
  IFindNftItemsDto,
  IFindNftTypesDto,
  IFindNftTypesResponse,
  IGetNftItemDto,
  IGetNftTypeDto,
  IGetNftTypeResponse,
  IGetNftUserDto,
  IGetNftUserResponse,
  INftItemDto,
  INftStatusResponse,
  INftTypeDto,
  NftItemRarity,
} from 'lib/NFT/api-server/nft.types.ts';
import { AxiosError } from 'axios';
import wait from 'utils/wait';

// get nft

const CACHE: Record<string, INftItemDto> = {};

export const loadNftItem = createEffect(async (params: IGetNftItemDto) => {
  const key = `${params.contractId}-${params.assetId}`;

  if (key in CACHE) {
    return CACHE[key];
  }

  const res = await nftApi.get(params);

  CACHE[key] = res;

  return res;
});

export const nftItemStore = createStore<{
  error: Error | null;
  loading: boolean;
  state: INftItemDto | null;
}>({
  state: null,
  error: null,
  loading: false,
})
  .on(loadNftItem.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadNftItem.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadNftItem.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// nfts

export const loadNftList = createEffect(async (params: IFindNftItemsDto) => {
  const res = await nftApi.find(params);
  return res;
});

export const nftListStore = createStore<{ rows: INftItemDto[]; count: number }>({
  rows: [],
  count: 0,
}).on(loadNftList.doneData, (prev, next) => next);

export const loadNftByRarityList = createEffect(
  async ({ rarity, ...params }: Omit<IFindNftItemsDto, 'rarities' & { rarity: NftItemRarity }>) => {
    const res = await nftApi.find({ ...params, rarities: [rarity] });
    return res;
  }
);

export const nftListByRarityStore = createStore<{
  state: { rows: INftItemDto[]; count: number };
  loading: boolean;
  error: null | Error;
}>({
  state: {
    rows: [],
    count: 0,
  },
  error: null,
  loading: false,
})
  .on(loadNftByRarityList.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadNftByRarityList.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadNftByRarityList.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// stats

export const loadNftStats = createEffect(async (opts: { nftContract: string }) => {
  const res = await nftApi.status(opts.nftContract);
  return res;
});

export const nftStatsStore = createStore({
  loading: false,
  error: null as Error | null,
  state: null as INftStatusResponse | null,
})
  .on(loadNftStats.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadNftStats.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadNftStats.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// nft user

export const loadNftUser = createEffect(async (opts: IGetNftUserDto) => {
  const res = await nftApi.getNftUser(opts);
  return res;
});
export const clearNftUser = createEvent();

export const nftUserStore = createStore({
  loading: true,
  error: null as Error | null,
  state: null as IGetNftUserResponse | null,
})
  .on(clearNftUser, () => ({ state: null, error: null, loading: false }))
  .on(loadNftUser.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadNftUser.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadNftUser.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// nft types

export const loadNftTypes = createEffect(async (opts: IFindNftTypesDto) => {
  const res = await nftApi.findNftTypes(opts);
  return res;
});

export const nftTypesStore = createStore({
  loading: true,
  error: null as Error | null,
  state: { rows: [], count: 0 } as IFindNftTypesResponse,
})
  .on(loadNftTypes.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadNftTypes.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: { rows: [], count: 0 },
  }))
  .on(loadNftTypes.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// nft collections

export const loadNftCollections = createEffect(async (opts: IFindNftCollectionsDto) => {
  const res = await nftApi.findNftCollections(opts);
  return res;
});

export const nftCollectionsStore = createStore({
  loading: true,
  error: null as Error | null,
  state: { rows: [], count: 0 } as IFindNftCollectionsResponse,
})
  .on(loadNftCollections.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadNftCollections.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadNftCollections.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// nft type

const CACHE_TYPES: Record<string, INftTypeDto> = {};

export const loadNftType = createEffect(async (params: IGetNftTypeDto) => {
  const key = params.typeId;

  if (key in CACHE_TYPES) {
    return CACHE_TYPES[key];
  }

  const res = await nftApi.getNftType(params);

  CACHE_TYPES[key] = res;

  return res;
});

export const nftTypeStore = createStore<{
  error: Error | null;
  loading: boolean;
  state: INftTypeDto | null;
}>({
  state: null,
  error: null,
  loading: false,
})
  .on(loadNftType.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadNftType.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadNftType.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));
