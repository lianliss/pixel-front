export type IBridgeEncodeRequestBody = {
  from: string;
  to: string;
  amount: string;
  fromAddress: string;
  fromContract?: boolean;
  recipient: string;
  dataToContract?: string;
};

export type IBridgeFeeRequestBody = {
  from: string;
  to: string;
  amount: string;
};

type IBridgeFeeData = {
  serviceFee: string;
  lpFee: string;
  totalFee: string;
};

export type IBridgeEncodeResponseData = {
  encoded: string;
  fromContract: string;
  recipient: string;
  fee: IBridgeFeeData;
  initiator: string;
};

export type IBridgeSwapStatusResponseData = {
  isExpired: boolean;
  isDone: boolean;
  isFailure: boolean;
  isSuccess: boolean;
  txHash: string | undefined;
};

export type IBridgeSwapResult = {
  link: string;
  hash: string;
};
