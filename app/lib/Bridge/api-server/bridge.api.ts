'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  IBridgeEncodeRequestBody,
  IBridgeEncodeResponseData,
  IBridgeFeeRequestBody,
  IBridgeSwapStatusResponseData,
} from 'lib/Bridge/api-server/bridge.types.ts';

export class MesonBridgeApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  getUrl(path) {
    return `${this.url}/${path}`;
  }

  async encode(data: IBridgeEncodeRequestBody): Promise<IBridgeEncodeResponseData> {
    try {
      const result = await axios({
        method: 'post',
        maxBodyLength: Infinity,
        url: this.getUrl('swap'),
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        data: JSON.stringify(data),
      });
      console.log('[BridgeApi][encode]', result);
      return result.data.result;
    } catch (error) {
      console.error('[BridgeApi][encode]', data, error);
      throw error;
    }
  }

  async getFee(data: IBridgeFeeRequestBody): Promise<IBridgeFeeData> {
    try {
      const result = await axios({
        method: 'post',
        maxBodyLength: Infinity,
        url: this.getUrl('price'),
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        data,
      });
      console.log('[BridgeApi][getFee]', result);
      return result.data.result;
    } catch (error) {
      console.error('[BridgeApi][getFee]', data, error);
      throw error;
    }
  }

  async getStatus(hash: string): Promise<IBridgeSwapStatusResponseData> {
    try {
      const response = await axios({
        method: 'get',
        maxBodyLength: Infinity,
        url: this.getUrl('swap'),
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        params: {
          hash,
        },
      });
      const data = response.data.result;
      return {
        isExpired: data.expired,
        isDone: !!data.RELEASED || !!data.CANCELLED,
        isFailure: !!data.CANCELLED,
        isSuccess: !!data.RELEASED,
        txHash: data.RELEASED || data.CANCELLED,
      };
    } catch (error) {
      console.error('[BridgeApi][getStatus]', hash, error);
      throw error;
    }
  }
}

export const mesonBridgeApi = new MesonBridgeApi({ url: 'https://relayer.meson.fi/api/v1' });
