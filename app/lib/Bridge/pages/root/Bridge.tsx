'use strict';
import React from 'react';
import { useTranslation } from 'react-i18next';

import styles from './Bridge.module.scss';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain } from 'services/multichain/chains';
import NotAvailableChain from '../../../../shared/blocks/NotAvailableChain/NotAvailableChain.tsx';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import ExchangeLayout from 'lib/Exchange/components/ExchangeLayout/ExchangeLayout.tsx';
import BridgeWidget from 'lib/Bridge/components/BridgeWidget/BridgeWidget.tsx';
import { BRIDGE_CHAINS } from '@cfg/app.ts';

function BridgePage() {
  const { t } = useTranslation('bridge');

  const account = useAccount();
  const available = BRIDGE_CHAINS.includes(account.chainId);

  useBackAction('/wallet/');

  return (
    <ExchangeLayout>
      {available ? (
        <div className={styles.root}>
          <BridgeWidget />
        </div>
      ) : (
        <NotAvailableChain
          title={t('Not Available')}
          summary={t('Bridge not available on current network')}
          chainId={Chain.POLYGON_MAINNET}
        />
      )}
    </ExchangeLayout>
  );
}

export default BridgePage;
