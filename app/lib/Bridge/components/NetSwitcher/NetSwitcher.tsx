import React, { useMemo } from 'react';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import styles from './styles.module.scss';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import { IconArrowRight } from '@ui-kit/icon/common/IconArrowRight.tsx';
import Box from '@mui/material/Box';
import { Chain } from 'services/multichain/chains';
import { IChain } from 'services/multichain/types.ts';

interface Props {
  title: string;
  balance: string;
  chain?: IChain;
  disabled?: boolean;
  balanceDisabled?: boolean;
  gasToken?: IToken;
}
const NetSwitcher = ({
  title,
  balance,
  chain,
  disabled = false,
  balanceDisabled = false,
  gasToken,
}: Props) => {
  return (
    <div className={styles.net__switcher}>
      <div className={styles.left__side}>
        <Typography>{title}</Typography>
        <img style={{ width: 24, height: 24 }} src={chain?.icon} alt='' />

        <ChainSwitcher chains={[Chain.SKALE, Chain.POLYGON_MAINNET]} isRelative disabled={disabled}>
          <Box display='flex' align='center'>
            <Typography>{chain?.title}</Typography>
            {!disabled && <IconArrowRight style={{ transform: 'rotate(90deg)', marginTop: 4 }} />}
          </Box>
        </ChainSwitcher>
      </div>
      {!balanceDisabled && (
        <Typography className={styles.from__net} align='right' sx={{ fontSize: 14 }} noWrap>
          {balance} {gasToken?.symbol}
        </Typography>
      )}
    </div>
  );
};

export default NetSwitcher;
