import React from 'react';
import { useTranslation } from 'react-i18next';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

import styles from './styles.module.scss';

interface Props {
  options: IToken[];

  value: IToken | undefined;
  onChange: (value: IToken) => void;
  disabled?: boolean;
}

const CurrencySelect = ({ options, value, onChange, disabled }: Props) => {
  const handleChange = (event: SelectChangeEvent) => {
    const token = options.find((el) => el.address === event.target.value);
    onChange?.(token);
  };

  return (
    <Select
      size='small'
      value={value?.address || ''}
      fullWidth
      onChange={handleChange}
      sx={{
        '& .MuiSelect-select': {
          display: 'flex',
        },
      }}
      disabled={!options.length || disabled}
    >
      {options.map((option) => (
        <MenuItem key={`item-${option.symbol}`} value={option.address}>
          <Box className={styles.option__content}>
            <img src={option.logoURI} alt='' className={styles.img} />
            <Typography variant='caption'>{option.symbol}</Typography>
          </Box>
        </MenuItem>
      ))}
    </Select>
  );
};

export default CurrencySelect;
