import styles from 'lib/Bridge/components/BridgeWidget/styles.module.scss';
import NetSwitcher from 'lib/Bridge/components/NetSwitcher/NetSwitcher.tsx';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import CurrencySelect from 'lib/Bridge/components/CurrencySelect/CurrencySelect.tsx';
import React from 'react';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Button from '@mui/material/Button';
import { useTranslation } from 'react-i18next';
import NumberField from '@ui-kit/NumberField/NumberField.tsx';

const CurrencySection = ({
  title,
  amount,
  onTokenChange,
  onAmountChange,
  // onNetChange,
  chain,
  gasBalance,
  token,
  tokenBalance,
  balanceDisabled,
  loading = false,
  gasToken,
}) => {
  const { t } = useTranslation('bridge');
  return (
    <div className={styles[`${title.toLowerCase()}__currency`]}>
      <div className={styles.from__header}>
        <NetSwitcher
          title={t(title)}
          chain={chain}
          balance={gasBalance?.data?.formatted || 0}
          disabled
          balanceDisabled={balanceDisabled}
          gasToken={gasToken}
        />
      </div>
      <div className={styles[`${title.toLowerCase()}__select`]}>
        <div className={styles[`${title.toLowerCase()}__input`]}>
          <NumberField
            placeholder='0.0'
            size='small'
            value={amount}
            onChange={(e) => onAmountChange(e.target.value)}
            disabled={loading}
          />
          {tokenBalance && (
            <Button
              size='small'
              onClick={() =>
                onAmountChange(parseFloat(tokenBalance?.data?.formatted - 0.05).toFixed(2))
              }
            >
              <Typography sx={{ fontSize: 10, color: '#4E99DE' }}>
                {`${t('Balance')}: ${
                  parseFloat(tokenBalance?.data?.formatted || 0).toFixed(2) || 0
                } ${token?.symbol || ''}`}
              </Typography>
            </Button>
          )}
        </div>
        <CurrencySelect
          options={token ? [token] : []}
          onChange={onTokenChange}
          value={token}
          disabled={loading}
        />
      </div>
    </div>
  );
};

export default CurrencySection;
