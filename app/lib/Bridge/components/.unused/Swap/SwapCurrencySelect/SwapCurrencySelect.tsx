import React from 'react';
import { useTranslation } from 'react-i18next';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { Currency } from 'lib/Bridge/components/.unused/Swap/SwapCurrencySelect/const.tsx';

interface Props {
  options: {
    title: string;
    value: Currency;
    icon: JSX.Element;
  }[];

  value: Currency;
  setValue: (value: Currency) => void;
}

const SwapCurrencySelect = ({ options, value, setValue }: Props) => {
  const { t } = useTranslation('bridge');

  const handleChange = (event: SelectChangeEvent) => {
    setValue(event.target.value as Currency);
  };

  return (
    <Select
      size='medium'
      value={value}
      fullWidth
      onChange={handleChange}
      sx={{
        '& .MuiSelect-select': {
          display: 'flex',
        },
      }}
    >
      {options.map((option) => (
        <MenuItem value={option.value}>
          <Box display='flex' alignItems='center' justifyContent='space-between' width='100%'>
            <Box display='flex' alignItems='center' justifyContent='space-between' gap={2}>
              {option.icon}
              <Typography variant='body2' component='div'>
                32.839201
              </Typography>
            </Box>
            {option.value}
          </Box>
        </MenuItem>
      ))}
    </Select>
  );
};

export default SwapCurrencySelect;
