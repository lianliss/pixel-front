import React, { useState } from 'react';

import styles from './styles.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useTranslation } from 'react-i18next';
import IconRu from '@ui-kit/icon/flags/IconRu.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import {
  Currency,
  CURRENCY_OPTIONS,
} from 'lib/Bridge/components/.unused/Swap/SwapCurrencySelect/const.tsx';
import SwapCurrencySelect from 'lib/Bridge/components/.unused/Swap/SwapCurrencySelect/SwapCurrencySelect.tsx';

const Swap = () => {
  const { t } = useTranslation('bridge');
  const [fromCurrency, setFromCurrency] = useState<Currency>(Currency.exUSDT);
  const [toCurrency, setToCurrency] = useState<Currency>(Currency.sgb);

  const onChangeFromCurrency = (currency: Currency) => {
    setFromCurrency(currency);
  };

  const onChangeToCurrency = (currency: Currency) => {
    setToCurrency(currency);
  };

  return (
    <div className={styles.content}>
      <div className={styles.swap__container}>
        <form className={styles.swap__form} onSubmit={(e) => e.preventDefault()}>
          <Typography className={styles.user__balance} align='right' sx={{ fontSize: 10 }}>
            {t('Balance')}: <span>50,943.923817</span>
          </Typography>

          <div className={styles.form__content}>
            <div className={styles.from__currency}>
              <div className={styles.from__select}>
                <SwapCurrencySelect
                  options={CURRENCY_OPTIONS}
                  setValue={onChangeFromCurrency}
                  value={fromCurrency}
                />
              </div>
              <Button size='small'>Max</Button>
            </div>

            <div className={styles.separator}>
              <div className={styles.hr} />
              <IconRu />
              <div className={styles.hr} />
            </div>

            <div className={styles.to__currency}>
              <div className={styles.to__select}>
                <SwapCurrencySelect
                  options={CURRENCY_OPTIONS}
                  setValue={onChangeFromCurrency}
                  value={fromCurrency}
                />
              </div>
            </div>
          </div>
        </form>
      </div>

      <div className={styles.exchange__footer}>
        <Typography sx={{ fontSize: 14 }}>Rate:</Typography>
        <Typography sx={{ fontSize: 14 }}>1 exUSDT ≈ 0.38293 SGB</Typography>
      </div>
    </div>
  );
};

export default Swap;
