import React, { useMemo } from 'react';
import styles from './styles.module.scss';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { IconArrowRight } from '@ui-kit/icon/common/IconArrowRight.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import { Chain, NETWORKS_DATA } from 'services/multichain/chains';
import CopyText from '../../../../features/CopyText/CopyText.tsx';
import { useTranslation } from 'react-i18next';

type Props = {
  open?: boolean;
  onClose?: () => void;
  fromNet: Chain;
  toNet: Chain;
  fromAmount: string;
  toAmount: string;
  fromToken?: IToken;
  toToken?: IToken;
  recipient: string;
  onSwap?: () => void;
  loading?: boolean;
  statusMessage?: string;
  fee?: any;
};

const BridgeConfirmTransactionDialog: React.FC<Props> = (props) => {
  const {
    onClose,
    fromNet,
    open,
    fromAmount,
    fromToken,
    toNet,
    toAmount,
    toToken,
    recipient,
    fee,
    statusMessage,
    onSwap,
    loading = false,
  } = props;
  const { t } = useTranslation('bridge');

  const fromChain = useMemo(() => NETWORKS_DATA[fromNet], [fromNet]);
  const toChain = useMemo(() => NETWORKS_DATA[toNet], [toNet]);

  const onSubmit = async () => {
    onSwap?.();
  };

  return (
    <BottomDialog
      open={open}
      onClose={onClose}
      footer={
        <div className={styles.btns}>
          <Button size='large' variant='outlined' onClick={onClose} loading={loading}>
            Cancel
          </Button>
          <Button
            size='large'
            variant='contained'
            onClick={onSubmit}
            disabled={!onSwap}
            loading={loading}
          >
            {t('Confirm')}
          </Button>
        </div>
      }
    >
      <h2 align='center'> {t('Confirm Swap')}</h2>

      <div className={styles.transaction__content}>
        <div className={styles.currencies__section}>
          <div className={styles.from__aria}>
            <div className={styles.network}>
              <Typography>{fromChain?.title}</Typography>
            </div>
            <div className={styles.center__section}>
              <img style={{ width: 26, height: 26 }} src={fromChain?.icon} />
              <div className={styles.dots}>
                <div />
                <div />
                <div />
              </div>
            </div>
            <div className={styles.currency}>
              <Typography>
                {parseFloat(fromAmount).toFixed(2)} {fromToken?.symbol}
              </Typography>
            </div>
          </div>
          <Box display='flex' alignItems='center' justifyContent='center'>
            <IconArrowRight />
          </Box>
          <div className={styles.to__aria}>
            <div className={styles.network}>
              <Typography align='right'>{toChain?.title}</Typography>
            </div>
            <div className={styles.center__section} style={{ justifyContent: 'flex-end' }}>
              <div className={styles.dots}>
                <div />
                <div />
                <div />
              </div>
              <img style={{ width: 26, height: 26 }} src={toChain?.icon} />
            </div>
            <div className={styles.currency}>
              <Typography align='right'>
                {toAmount} {toToken?.symbol}
              </Typography>
            </div>
          </div>
        </div>
        <div className={styles.recipient}>
          <Typography className={styles.title} sx={{ fontSize: 14 }}>
            {t('Recipient')}
          </Typography>
          <CopyText text={recipient}>
            {({ copy }) => (
              <Button>
                <Typography sx={{ fontSize: 14 }} noWrap onClick={copy} color='white'>
                  {recipient ? recipient.slice(0, 12) + '...' + recipient.slice(-12) : '-'}
                </Typography>
              </Button>
            )}
          </CopyText>
        </div>

        {statusMessage && (
          <Alert severity='info'>
            <Typography sx={{ fontSize: 12 }}>{statusMessage}</Typography>
          </Alert>
        )}

        <div className={styles.fee__section}>
          <div className={styles.fee__row}>
            <Typography>{t('Total Fee')}</Typography>
            <Typography>{fee.totalFee} USDT</Typography>
          </div>
          <div className={styles.fee__row}>
            <Typography sx={{ fontSize: 14, color: 'gray' }}>{t('Protocol Fee')}</Typography>
            <Typography sx={{ fontSize: 14, color: 'gray' }}>{fee.protocolFee} USDT</Typography>
          </div>
          <div className={styles.fee__row}>
            <Typography sx={{ fontSize: 14, color: 'gray' }}>{t('Gas & LP Fees')}</Typography>
            <Typography sx={{ fontSize: 14, color: 'gray' }}>{fee.gasFee} POL</Typography>
          </div>
        </div>

        {/*<div className={styles.confirm__check}>*/}
        {/*  <Checkbox {...label} />*/}
        {/*  <Typography sx={{ fontSize: 11, color: 'gray' }}>*/}
        {/*    Sending from a hardware wallet (Ledger, Trezor, etc.)*/}
        {/*  </Typography>*/}
        {/*</div>*/}
      </div>
    </BottomDialog>
  );
};

export default React.memo(BridgeConfirmTransactionDialog);
