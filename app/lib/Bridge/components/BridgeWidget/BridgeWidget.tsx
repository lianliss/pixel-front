import React, { useEffect, useMemo, useState } from 'react';
import styles from './styles.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useTranslation } from 'react-i18next';
import { IconExchange } from '@ui-kit/icon/common/IconExchange.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import BridgeConfirmTransactionDialog from 'lib/Bridge/components/BridgeConfirmTransactionDialog/BridgeConfirmTransactionDialog.tsx';
import Paper from '@mui/material/Paper';
import { styled, useTheme } from '@mui/material/styles';
import { useCurrencyBridge } from 'lib/Bridge/hooks/useCurrencyBridge.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain, NETWORKS_DATA } from 'services/multichain/chains';
import { useBalance } from '@chain/hooks/useBalance.ts';
import useBridgeWrap from 'lib/Bridge/hooks/useBridgeWrap.tsx';
import toaster from 'services/toaster.tsx';
import CurrencySection from 'lib/Bridge/components/CurrencySection/CurrencySection.tsx';
import { useToken } from '@chain/hooks/useToken.ts';
import Alert from '@mui/material/Alert';
import bridgeAnalytics from 'lib/Bridge/api-analytics';
import Divider from '@mui/material/Divider';

const LightPaper = styled(Paper)(({ theme }) => ({
  // backgroundColor: theme.palette.background.back2,
}));

const BridgeWidget = () => {
  const { t } = useTranslation('bridge');
  const { t: n } = useTranslation('notifications');

  const account = useAccount();

  const {
    fee,
    fromNet,
    toNet,
    fromToken,
    toToken,
    fromAmount,
    toAmount,
    openConfirmTransaction,
    setOpenConfirmTransaction,
    handleCurrencyChange,
    handleFromAmountChange,
    handleToAmountChange,
    onSubmitBridge,
    onNetChange,
    onSetFromCurrency,
    onSetToCurrency,
  } = useCurrencyBridge();
  const [process, setProcess] = useState(false);
  const { isSwapping, swap, loadStatus, isSuccess, isDone, isFailed } = useBridgeWrap({
    fromToken,
    toToken,
    fromNet,
    toNet,
  });
  const gasToken = useToken({});

  const fromTokenBalance = useBalance({
    address: account.accountAddress,
    token: fromToken?.address,
    skip: !account.isConnected || !fromToken,
  });
  // const toTokenBalance = useBalance({
  //   address: account.accountAddress,
  //   token: fromToken?.address,
  //   skip: !account.isConnected || !fromToken,
  // });
  const gasBalance = useBalance({
    address: account.accountAddress,
    skip: !account.isConnected,
  });
  const noGas = !gasBalance.data?.formatted;

  const fromChain = useMemo(() => NETWORKS_DATA[fromNet], [fromNet]);
  const toChain = useMemo(() => NETWORKS_DATA[toNet], [toNet]);

  const disabled = React.useMemo(() => {
    if (fromTokenBalance && fromTokenBalance?.data?.formatted < fromAmount) {
      return true;
    }
    if (!fromToken || !toToken || !fromChain || !toChain || !fromAmount) {
      return true;
    }

    if (!toAmount) {
      return true;
    }

    return fromAmount < 2;
  }, [fromToken, toToken, fromChain, toChain, fromAmount, toAmount]);

  const alertMessage = React.useMemo(() => {
    if (fromTokenBalance && fromTokenBalance?.data?.formatted < fromAmount) {
      return n('Insufficient funds');
    }
    if (fromAmount && fromAmount < fee.totalFee) {
      return n('Swap amount cannot cover fees');
    }
    if (fromAmount && fromAmount > fee.totalFee && fromAmount < 2) {
      return n('Minimum amount 2 USDC');
    }
  }, [fromAmount]);

  const statusMessage = React.useMemo(() => {
    if (process) {
      if (!isDone) {
        return n('Swap in progress');
      }
      if (isSuccess) {
        return n('Successfully swap');
      }
      if (isFailed) {
        return n('Failed swap');
      }
    }

    return '';
  }, [isSuccess, isDone, isFailed]);

  useEffect(() => {
    if (!isDone) {
      setProcess(true);
    }

    if (isSuccess || isFailed) {
      const timer = setTimeout(() => {
        setProcess(false);
      }, 3000);

      return () => clearTimeout(timer);
    }
  }, [isDone, isSuccess, isFailed]);

  const handleSwap = async () => {
    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      const result = await swap(+fromAmount);

      handleFromAmountChange('0');
      handleToAmountChange('0');

      setOpenConfirmTransaction(false);

      loadStatus(result.hash)
        .then(() => {
          toaster.success(n('Successfully swapped'));
          setProcess(true);
        })
        .catch((e) => {
          toaster.logError(e);
        });

      bridgeAnalytics.trackSwap({
        chainId: account.chainId,
        userAddress: account.accountAddress,
        fromToken: fromToken.symbol,
        toToken: toToken.symbol,
        fromAmount: +fromAmount,
        toAmount: +toAmount,
      });

      toaster.success(n(`Successfully requested`, { fromAmount }));
      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      toaster.logError(e);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    }
  };

  return (
    <div className={styles.content}>
      <div className={styles.contentBox}>
        <div className={styles.bridge__container}>
          <div className={styles.bridge__content}>
            <div className={styles.form__content}>
              <CurrencySection
                title='From'
                amount={fromAmount}
                onNetChange={(network) => onNetChange(network, true)}
                onCurrencyChange={handleCurrencyChange(onSetFromCurrency)}
                onAmountChange={handleFromAmountChange}
                chain={fromChain}
                gasBalance={gasBalance}
                token={fromToken}
                tokenBalance={fromTokenBalance}
                loading={isSwapping}
                gasToken={gasToken.data}
              />
              <div className={styles.separator}>
                <Divider className={styles.hr} />
                <Button onClick={handleSwap} disabled>
                  <IconExchange />
                </Button>
                <Divider className={styles.hr} />
              </div>
              <CurrencySection
                title='To'
                amount={toAmount}
                token={toToken}
                chain={toChain}
                onNetChange={(network) => onNetChange(network, false)}
                onCurrencyChange={handleCurrencyChange(onSetToCurrency)}
                onAmountChange={handleToAmountChange}
                balanceDisabled
                loading={true}
              />
            </div>
          </div>
        </div>
      </div>

      {alertMessage && (
        <Alert severity='warning'>
          <Typography sx={{ fontSize: 12 }}>{alertMessage}</Typography>
        </Alert>
      )}

      <LightPaper elevation={0} variant='outlined' style={{ overflow: 'hidden' }}>
        <div className={styles.params__list}>
          <ParamItem label={t('Protocol Fee')} value={`${fee.totalFee} USDC`} />
          <Divider />
          <ParamItem label={t('Destination Gas (Est)')} value={`0.03 ${gasToken.data?.symbol}`} />
        </div>

        <BridgeConfirmTransactionDialog
          open={openConfirmTransaction}
          fromNet={fromNet}
          fromAmount={fromAmount}
          fromToken={fromToken}
          toNet={toNet}
          toAmount={toAmount}
          toToken={toToken}
          onClose={() => setOpenConfirmTransaction(false)}
          recipient={account.accountAddress}
          onSwap={handleSwap}
          loading={isSwapping}
          statusMessage={statusMessage}
          fee={fee}
        />
      </LightPaper>

      <Button
        size='large'
        fullWidth
        variant='contained'
        onClick={onSubmitBridge}
        disabled={disabled || noGas}
        loading={isSwapping}
      >
        {noGas && 'No Gas'}
        {!noGas && t('Bridge')}
      </Button>
    </div>
  );
};

const ParamItem = ({ label, value }) => (
  <div className={styles.param__item}>
    <Typography sx={{ fontSize: 14 }} light>
      {label}
    </Typography>
    <Typography sx={{ fontSize: 14 }} light>
      {value}
    </Typography>
  </div>
);

export default BridgeWidget;
