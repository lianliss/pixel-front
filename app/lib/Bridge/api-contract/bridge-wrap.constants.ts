import { Chain } from 'services/multichain/chains';

export const BRIDGE_WRAP_CONTRACT_ID = {
  [Chain.POLYGON_MAINNET]: '0x8936C90536704Ae3E84b6dFcA61B93df446b83da',
  [Chain.SKALE]: '0x05AcA9436d4D9C8060A71eB8BB40C3C5B3f3BBBd',
};

export const BRIDGE_WRAP_FEE = 0.49;
export const BRIDGE_MESON_FEE = {
  [Chain.POLYGON_MAINNET]: 0.5,
};

export const BRIDGE_NETWORKS = {
  [Chain.POLYGON_MAINNET]: 'polygon',
  [Chain.SKALE]: 'skale-europa',
};
