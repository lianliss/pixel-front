import React, { useEffect } from 'react';
import { useAccount } from '@chain/hooks/useAccount.ts';
import toaster from 'services/toaster.tsx';
import { mesonBridgeApi } from 'lib/Bridge/api-server/bridge.api.ts';
import {
  BRIDGE_MESON_FEE,
  BRIDGE_NETWORKS,
  BRIDGE_WRAP_CONTRACT_ID,
  BRIDGE_WRAP_FEE,
} from 'lib/Bridge/api-contract/bridge-wrap.constants.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import BRIDGE_WRAP_ABI from 'lib/Bridge/api-contract/bridge-wrap.abi.ts';
import ERC_20_ABI from 'lib/Wallet/api-contract/abi/erc-20.abi.ts';
import { TOKENS } from 'services/multichain/initialTokens.js';
import wei from 'utils/wei.jsx';
import {
  IBridgeSwapResult,
  IBridgeSwapStatusResponseData,
} from 'lib/Bridge/api-server/bridge.types.ts';

function useBridgeWrap({
  fromToken,
  toToken,
  fromNet,
  toNet,
}: {
  fromToken: IToken;
  toToken: IToken;
  fromNet: string;
  toNet: string;
}) {
  const { chainId, isConnected, accountAddress } = useAccount();
  const bridgeAddress = BRIDGE_WRAP_CONTRACT_ID[chainId];

  const [encoded, setEncoded] = React.useState<string | undefined>();
  const link = encoded ? `https://explorer.meson.fi/swap/${encoded}` : undefined;
  const [isSwapping, setIsSwapping] = React.useState<boolean>(false);
  const [isDone, setIsDone] = React.useState<boolean>(false);
  const [isSuccess, setIsSuccess] = React.useState<boolean>(false);
  const [isFailed, setIsFailed] = React.useState<boolean>(false);

  const intervalRef = React.useRef<any | null>(null);

  const write = useWriteContract({
    abi: BRIDGE_WRAP_ABI,
    address: bridgeAddress,
  });
  const allowanceUpdate = useWriteContract({
    abi: ERC_20_ABI,
    address: fromToken?.address,
  });

  const swap = async (amount: number = 2): Promise<IBridgeSwapResult> => {
    try {
      if (!chainId || !isConnected || !fromToken || !toToken) return;
      const totalFee = BRIDGE_WRAP_FEE + BRIDGE_MESON_FEE;
      if (amount <= totalFee)
        throw new Error(`Amount must be more than fee (${totalFee.toFixed(2)})`);

      setIsSwapping(true);

      const mesonAmount = amount - BRIDGE_WRAP_FEE;
      const approveWei = wei.to(amount, fromToken.decimals);

      console.log('[swap]', {
        amount,
        approveWei,
        mesonAmount,
      });
      await allowanceUpdate.writeContractAsync({
        functionName: 'approve',
        args: [bridgeAddress, approveWei],
      });
      console.log('[allowanceUpdate]', true);

      const encodeData = await mesonBridgeApi.encode({
        from: `${BRIDGE_NETWORKS[fromNet]}:${fromToken.symbol.toLowerCase()}`,
        to: `${BRIDGE_NETWORKS[toNet]}:${toToken.symbol.toLowerCase()}`,
        amount: mesonAmount.toFixed(2),
        fromAddress: bridgeAddress,
        fromContract: true,
        recipient: accountAddress,
      });

      console.log('[encodeData]', encodeData);
      const tx = await write.writeContractAsync({
        functionName: 'transferToMeson',
        args: [encodeData.encoded, encodeData.initiator],
      });
      const hash = tx.hash;

      setIsSwapping(false);

      return {
        link: `https://explorer.meson.fi/swap/${encodeData.encoded}`,
        hash,
      };
    } catch (error) {
      setIsSwapping(false);
      console.log('SWAP', error);
      throw error;
    }
  };

  const getStatus = React.useCallback(
    async (txHash: string): Promise<IBridgeSwapStatusResponseData> => {
      return await mesonBridgeApi.getStatus(txHash);
    },
    []
  );
  const loadStatus = React.useCallback(
    async (txHash: string): Promise<IBridgeSwapStatusResponseData> => {
      if (intervalRef.current) {
        clearInterval(intervalRef.current);
      }

      let i = 0;

      const interval = setInterval(async () => {
        if (i >= 20) {
          clearInterval(intervalRef.current);
        }

        const status = await getStatus(txHash);

        if (status.isDone) {
          if (status.isSuccess) {
            setIsSuccess(true);
            setIsDone(true);
            setIsFailed(false);
          } else {
            setIsFailed(true);
            setIsDone(true);
            setIsSuccess(false);
          }
        }

        i++;
      }, 2000);

      intervalRef.current = interval;
    },
    [getStatus]
  );

  useEffect(() => {
    return () => {
      if (intervalRef.current) {
        clearInterval(intervalRef.current);
      }
    };
  }, []);

  return {
    isSwapping,
    swap,
    loadStatus,
    isSuccess,
    isDone,
    isFailed,
  };
}

export default useBridgeWrap;
