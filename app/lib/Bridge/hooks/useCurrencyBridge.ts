import React, { useState } from 'react';
import { Chain } from 'services/multichain/chains';
import { TOKENS } from 'services/multichain/initialTokens';
import { useAccount } from '@chain/hooks/useAccount.ts';
import bridgeAnalytics from 'lib/Bridge/api-analytics';
import {
  BRIDGE_MESON_FEE,
  BRIDGE_NETWORKS,
  BRIDGE_WRAP_FEE,
} from 'lib/Bridge/api-contract/bridge-wrap.constants.ts';
import { mesonBridgeApi } from 'lib/Bridge/api-server/bridge.api.ts';

export const useCurrencyBridge = () => {
  const account = useAccount();
  const isFromPolygon = account.chainId === Chain.POLYGON_MAINNET;

  const [fromNet, setFromNet] = useState<Chain>(account.chainId);
  const [toNet, setToNet] = useState<Chain>(isFromPolygon ? Chain.SKALE : Chain.POLYGON_MAINNET);
  const [fromAmount, setFromAmount] = useState<string>('');
  const [toAmount, setToAmount] = useState<string>('');
  const [openConfirmTransaction, setOpenConfirmTransaction] = useState<boolean>(false);
  const [fromToken, setFromToken] = React.useState<IToken | null>(null);
  const [toToken, setToToken] = React.useState<IToken | null>();

  const wrapFee = BRIDGE_WRAP_FEE;
  const [mesonFee, setMesonFee] = React.useState(BRIDGE_MESON_FEE[account.chainId] || 0);
  const fee = {
    protocolFee: wrapFee + mesonFee,
    gasFee: 0.003,
    totalFee: wrapFee + mesonFee,
  };

  const updateMesonFee = async () => {
    try {
      if (BRIDGE_MESON_FEE[account.chainId] || !Number(fromAmount)) return;
      const data = await mesonBridgeApi.getFee({
        from: `${BRIDGE_NETWORKS[fromNet]}:${fromToken.symbol.toLowerCase()}`,
        to: `${BRIDGE_NETWORKS[toNet]}:${toToken.symbol.toLowerCase()}`,
        amount: fromAmount,
      });
      setMesonFee(Number(data.totalFee));
    } catch (error) {
      console.error('[updateMesonFee]', error);
    }
  };
  React.useEffect(() => {
    updateMesonFee().then();
  }, [fromNet, toNet, fromAmount]);

  React.useEffect(() => {
    setFromToken((TOKENS[fromNet]?.usdc as IToken) || null);
    setToToken((TOKENS[toNet]?.usdc as IToken) || null);
  }, [fromNet, toNet]);

  const handleCurrencyChange =
    (setter: React.Dispatch<React.SetStateAction<IToken>>) => (currency: IToken) => {
      setter(currency);
    };

  const handleFromAmountChange = (newValue: string) => {
    setFromAmount(newValue);
    const convertedAmount = convertCurrency((newValue || 0).toString(), fromToken, toToken);
    setToAmount(convertedAmount);
  };

  const handleToAmountChange = (newValue: string) => {
    setToAmount(newValue);
  };

  const convertCurrency = (amount: string, from: IToken, to: IToken): string => {
    const conversionRate = 1;
    const totalFee = fee.totalFee;
    const convertedAmount = parseFloat(amount) * conversionRate;
    const finalAmount = convertedAmount - totalFee;
    return finalAmount.toFixed(2);
  };

  const onSubmitBridge = async () => {
    setOpenConfirmTransaction(true);
  };

  const onNetChange = (network: Chain, isFromNet: boolean) => {
    if (isFromNet) {
      setFromNet(network);
    } else {
      setToNet(network);
    }
  };
  React.useEffect(() => {
    onNetChange(account.chainId, true);
    onNetChange(
      account.chainId === Chain.POLYGON_MAINNET ? Chain.SKALE : Chain.POLYGON_MAINNET,
      false
    );
  }, [account.chainId]);

  const onSetFromCurrency = (currency: IToken) => {
    setFromToken(currency);
  };
  const onSetToCurrency = (currency: IToken) => {
    setToToken(currency);
  };

  return {
    fee,
    fromNet,
    toNet,
    fromToken,
    toToken,
    fromAmount,
    toAmount,
    openConfirmTransaction,
    setOpenConfirmTransaction,
    handleCurrencyChange,
    handleFromAmountChange,
    handleToAmountChange,
    convertCurrency,
    onSubmitBridge,
    onNetChange,
    onSetFromCurrency,
    onSetToCurrency,
  };
};
