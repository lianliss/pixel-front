import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import DrillIcon from '../../../shared/icons/Drill.tsx';
import StorageIcon from '../../../shared/icons/Storage.tsx';
import LotteryTimeLeft from 'lib/Lottery/components/LotteryTimeLeft.tsx';
import { Img } from '@ui-kit/Img/Img.tsx';
import sgbAlt from 'assets/img/token/sgb/sgb-alt.svg';
import React from 'react';

const StyledBody = styled(Paper)(() => ({
  display: 'flex',
  flexDirection: 'column',
  gap: 8,
  padding: '8px 12px',
}));

type Props = {
  requireDrill?: number;
  drillLevel?: number;
  requireStorage?: number;
  storageLevel?: number;
  rewardAmount?: number;
  endedAt?: number;
  startedAt?: number;
};

function LotteryCardInfo(props: Props) {
  const {
    startedAt,
    endedAt,
    storageLevel = 0,
    requireStorage = 0,
    drillLevel = 0,
    requireDrill = 0,
    rewardAmount = 0,
    ...other
  } = props;

  return (
    <StyledBody elevation={0} variant='outlined' {...other}>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
          gap: 2,
          width: '100%',
        }}
      >
        <Box sx={{ maxWidth: 125 }}>
          <Typography variant='caption' color='text.secondary'>
            Requirements:
          </Typography>

          <Typography component='p' variant='caption' sx={{ fontSize: 10, opacity: 0.5 }}>
            To participate in the lottery you must have these indicators or higher
          </Typography>

          <Box sx={{ display: 'flex', gap: 1, mt: 1 }}>
            {!!requireDrill && (
              <Typography
                variant='subtitle2'
                color={requireDrill > drillLevel ? 'error.main' : 'success.main'}
                sx={{ display: 'flex', alignItems: 'center', gap: '3px' }}
              >
                <DrillIcon sx={{ fontSize: 16, color: 'text.secondary' }} />

                {requireDrill}
              </Typography>
            )}
            {!!requireStorage && (
              <Typography
                variant='subtitle2'
                color={requireStorage > storageLevel ? 'error.main' : 'success.main'}
                sx={{ display: 'flex', alignItems: 'center', gap: '3px' }}
              >
                <StorageIcon sx={{ fontSize: 16, color: 'text.secondary' }} />

                {requireStorage}
              </Typography>
            )}
            {!requireStorage && !requireDrill && '-'}
          </Box>
        </Box>

        <Box sx={{ maxWidth: 125 }}>
          <Typography component='p' variant='caption' color='text.secondary' align='right'>
            Prize Fund:
          </Typography>

          <Typography
            align='right'
            component='p'
            variant='caption'
            sx={{ fontSize: 10, opacity: 0.5 }}
          >
            To participate in the lottery you must have these indicators or higher
          </Typography>

          <Box sx={{ display: 'flex', gap: 1, justifyContent: 'flex-end', mt: 1 }}>
            <Img src={sgbAlt as string} sx={{ width: 18 }} />

            <Typography sx={{ fontSize: 18, lineHeight: '18px' }} fontWeight='bold'>
              <Typography variant='inherit' component='span'>
                {rewardAmount}{' '}
              </Typography>
              <Typography variant='inherit' component='span' sx={{ opacity: 0.5 }}>
                SGB
              </Typography>
            </Typography>
          </Box>
        </Box>
      </Box>

      {endedAt && <LotteryTimeLeft endedAt={endedAt} startedAt={startedAt} align='center' />}
    </StyledBody>
  );
}

export default LotteryCardInfo;
