import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { IntervalRender } from '@ui-kit/IntervalRender/IntervalRender.tsx';
import { diffTime, fromUtc, toUTC } from 'utils/format/date';
import { TypographyProps } from '@mui/material';

type Props = TypographyProps & { endedAt?: number; startedAt?: number };

function LotteryTimeLeft(props: Props) {
  const { endedAt, startedAt, ...other } = props;

  return (
    <Typography variant='caption' {...other}>
      <IntervalRender
        render={() => {
          const utcNow = Date.now();
          const isNotStarted = startedAt && startedAt > utcNow;

          return (
            <>
              <Typography
                variant='inherit'
                component='span'
                sx={{ opacity: 0.5 }}
                color='text.primary'
              >
                Time {isNotStarted ? 'to' : 'left'}:{' '}
              </Typography>

              <Typography
                variant='inherit'
                component='span'
                color={endedAt && endedAt - utcNow < 1000 * 60 * 60 * 1 ? 'error' : undefined}
              >
                {endedAt && startedAt
                  ? endedAt < utcNow
                    ? 'ended'
                    : isNotStarted
                    ? diffTime(toUTC(startedAt))
                    : diffTime(toUTC(endedAt))
                  : '-'}
              </Typography>
            </>
          );
        }}
      />
    </Typography>
  );
}

export default LotteryTimeLeft;
