import { alpha, styled } from '@mui/material/styles';
import Box, { BoxProps } from '@mui/material/Box';
import React from 'react';
import Paper from '@mui/material/Paper';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import QuestionMark from '@mui/icons-material/QuestionMark';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { NftItemRarity, NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { getNftSlotIcon } from 'lib/NFT/utils/getNftSlotIcon.tsx';
import romanize from 'utils/romanize';
import CircularProgress from '@mui/material/CircularProgress';

const StyledRoot = styled(Box)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  gap: 2,
}));
const StyledAvatar = styled(Avatar)(({ theme }) => ({
  width: 64,
  height: 64,
  maxWidth: 64,
}));
const StyledSlot = styled(Box)(({ theme }) => ({
  width: 24,
  height: 24,
  borderRadius: '100%',
  border: 'solid 1px black',
  position: 'absolute',
  bottom: 0,
  right: 0,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  background: theme.palette.background.paper,

  '& > svg': {
    width: 14,
    height: 14,
  },
}));
const StyledCollection = styled(Box)(({ theme }) => ({
  width: 20,
  height: 20,
  fontSize: 10,
  borderRadius: '100%',
  border: `solid 1px ${theme.palette.primary.main}`,
  position: 'absolute',
  top: 0,
  left: 0,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  background: theme.palette.background.paper,

  '& > svg': {
    width: 14,
    height: 14,
  },
}));

type Props = BoxProps & {
  count?: number;
  countRequired?: number;
  rarity?: NftItemRarity;
  slot?: NftItemSlot;
  nftTypeImage?: string;
  collectionIndex?: number;
  loading?: boolean;
  ended?: boolean;
};

function LotteryLuckyNft(props: Props) {
  const {
    count = 0,
    countRequired = 0,
    rarity,
    slot,
    collectionIndex,
    nftTypeImage,
    loading = false,
    ended = false,
    ...other
  } = props;

  const completed = count >= countRequired;
  const rarityColor = typeof rarity !== 'undefined' ? getNftRarityColor(rarity) : '#ffffff';
  const slotIcon = typeof slot !== 'undefined' && slot !== null ? getNftSlotIcon(slot) : undefined;

  return (
    <StyledRoot {...other}>
      <Box sx={{ position: 'relative' }}>
        <StyledAvatar
          variant='rounded'
          variantStyle='outlined'
          src={nftTypeImage}
          style={{ borderColor: rarityColor }}
          sx={{ '& > img': { opacity: completed ? 1 : 0.25 } }}
        >
          {loading ? (
            <CircularProgress style={{ width: 24, height: 24 }} />
          ) : completed ? (
            <CheckIcon color='success' />
          ) : ended ? (
            <CloseIcon color='error' />
          ) : (
            <QuestionMark />
          )}
        </StyledAvatar>
        {slotIcon && (
          <StyledSlot sx={{ borderColor: rarityColor, color: rarityColor }}>{slotIcon}</StyledSlot>
        )}
        {collectionIndex && (
          <StyledCollection sx={{ borderColor: rarityColor, color: rarityColor }}>
            {romanize(collectionIndex)}
          </StyledCollection>
        )}
      </Box>
      <Typography
        color={completed ? 'success' : undefined}
        variant='caption'
        component='p'
        sx={{ fontSize: 10, mt: '2px' }}
        align='center'
        fontWeight='bold'
      >
        <Typography color={!completed ? 'error' : undefined} variant='inherit' component='span'>
          {Math.min(count, countRequired)}
        </Typography>
        <Typography variant='inherit' component='span'>
          /{countRequired}
        </Typography>
      </Typography>
    </StyledRoot>
  );
}

export default LotteryLuckyNft;
