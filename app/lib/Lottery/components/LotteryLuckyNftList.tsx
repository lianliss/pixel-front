import { alpha, styled } from '@mui/material/styles';
import { BoxProps } from '@mui/material/Box';
import React, { useMemo } from 'react';
import Paper from '@mui/material/Paper';
import { ILotteryCheckDto, LotteryCheckType } from 'lib/Lottery/api-server/lottery.types.ts';
import LotteryLuckyNft from 'lib/Lottery/components/LotteryLuckyNft.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { getLotteryHaveNftList } from 'lib/Lottery/utils/getLotteryHaveNftList.ts';

const StyledRoot = styled(Paper)(({ theme }) => ({
  padding: 12,
  display: 'flex',
  flexWrap: 'wrap',
  gap: 16,
  justifyContent: 'center',

  '& > *': {
    flexBasis: '20%',
    width: '20%',
  },

  variants: [
    {
      props: {
        status: 'success',
      },
      style: {
        backgroundColor: alpha(theme.palette.success.main, 0.1),
        border: `1px solid ${theme.palette.success.main}`,
      },
    },
    {
      props: {
        status: 'failed',
      },
      style: {
        backgroundColor: alpha(theme.palette.error.main, 0.1),
        border: `1px solid ${theme.palette.error.main}`,
      },
    },
  ],
}));

type Props = BoxProps & {
  checks: ILotteryCheckDto<LotteryCheckType>[];
  nftList?: INftItemDto[];
  loading?: boolean;
  completed?: boolean;
  ended?: boolean;
};

function LotteryLuckyNftList(props: Props) {
  const {
    checks = [] as ILotteryCheckDto<LotteryCheckType>[],
    nftList = [] as INftItemDto[],
    loading = false,
    completed = false,
    ended = false,
    ...other
  } = props;

  const nftChecks = useMemo(() => {
    return getLotteryHaveNftList(checks, nftList);
  }, [checks, nftList]);
  const isWin = nftChecks.every((el) => el.count >= el.check.context.count);

  return (
    <StyledRoot
      variant='outlined'
      ownerState={{
        status: loading ? undefined : isWin || completed ? 'success' : 'failed',
      }}
      {...other}
    >
      {nftChecks &&
        nftChecks.map(({ check, count, image }) => (
          <LotteryLuckyNft
            key={`check-${check.id}`}
            count={completed ? check.context.count : count}
            countRequired={check.context.count}
            rarity={check.context.rarity}
            slot={check.context.slot}
            collectionIndex={check.context.collectionIndex}
            nftTypeImage={check.context.nftTypeImage || image}
            loading={loading}
            ended={ended}
          />
        ))}
      {!nftChecks.length && <Typography>No checks</Typography>}
    </StyledRoot>
  );
}

export default LotteryLuckyNftList;
