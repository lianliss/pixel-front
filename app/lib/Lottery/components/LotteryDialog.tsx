import BottomDialog, { IBottomDialogProps } from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { ILotteryDto, LotteryCheckType } from 'lib/Lottery/api-server/lottery.types.ts';
import { Button } from '@ui-kit/Button/Button.tsx';
import React, { useMemo } from 'react';
import { useLotteryStatus } from 'lib/Lottery/hooks/useLotteryStatus.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import LotteryCardInfo from 'lib/Lottery/components/LotteryCardInfo.tsx';
import LotteryLuckyNftList from 'lib/Lottery/components/LotteryLuckyNftList.tsx';
import { isLotteryChecksCompleted } from 'lib/Lottery/utils/getLotteryHaveNftList.ts';

type Props = {
  lottery: ILotteryDto | null;
  disabled?: boolean;
  loading?: boolean;
  onCheck?: (lottery: ILotteryDto) => void;
  drillLevel?: number;
  storageLevel?: number;
} & Omit<IBottomDialogProps, 'children'>;

function LotteryDialog(props: Props) {
  const {
    lottery,
    disabled = false,
    loading = false,
    onCheck,
    drillLevel = 0,
    storageLevel = 0,
    ...other
  } = props;

  const limit = lottery?.limitUsers;
  const current = lottery?.currentUsers;
  const started = lottery?.startedAt && lottery?.startedAt <= Date.now();
  const ended = lottery?.endedAt && lottery?.endedAt <= Date.now();

  const rewardAmount = lottery?.rewards?.[0]?.context?.amount || 0;
  const rewardPerUser = lottery?.isFixedReward ? rewardAmount / limit : rewardAmount / current;

  const requireDrill = lottery?.checks?.find((el) => el.type === LotteryCheckType.DrillLevel)
    ?.context?.level;
  const requireStorage = lottery?.checks?.find((el) => el.type === LotteryCheckType.StorageLevel)
    ?.context?.level;

  const lotteryStatus = useLotteryStatus({
    id: lottery?.id,
    skip: !!lottery?.lotteryUser?.userCompleteAt || ended,
  });
  const isWin = useMemo(
    () =>
      lottery &&
      lotteryStatus.data &&
      isLotteryChecksCompleted(lottery.checks, lotteryStatus.data?.nftList),
    [lotteryStatus.data, lottery]
  );
  const isCompleted = !!lottery?.lotteryUser?.userCompleteAt;

  const title = lotteryStatus.loading
    ? 'Loading...'
    : started
    ? isWin || isCompleted
      ? 'You Win!'
      : 'You Loose'
    : 'Soon';
  const subtitle =
    isWin || isCompleted
      ? 'Congratulations! You are lucky, you have all the NFTs from the requirements in the lottery. You can claim your prize.'
      : 'Be vigilant and do not miss the start of the lottery draw. The number of winning places is limited';

  return (
    <BottomDialog title={title} subTitle={subtitle} {...other}>
      <Typography align='center' component='p' variant='caption' sx={{ lineHeight: '21px' }}>
        <Typography variant='inherit' component='span' sx={{ opacity: 0.5 }}>
          Limit:{' '}
        </Typography>
        <Typography variant='inherit' component='span' color='success.main'>
          {current}
        </Typography>
        <Typography variant='inherit' component='span' sx={{ opacity: 0.5 }}>
          /{limit}
        </Typography>
      </Typography>

      <Typography
        variant='subtitle1'
        fontWeight='bold'
        align='center'
        color='text.primary'
        sx={{ mt: 2, mb: 1 }}
      >
        Lucky NFT
      </Typography>

      <LotteryLuckyNftList
        sx={{ mb: 2 }}
        checks={lottery?.checks}
        nftList={lotteryStatus.data?.nftList}
        loading={lotteryStatus.loading}
        completed={isCompleted}
        ended={ended}
      />

      <LotteryCardInfo
        endedAt={lottery?.endedAt}
        startedAt={lottery?.startedAt}
        requireDrill={requireDrill}
        requireStorage={requireStorage}
        drillLevel={drillLevel}
        storageLevel={storageLevel}
        rewardAmount={rewardAmount}
      />

      <Button
        sx={{ mt: 2 }}
        variant='contained'
        color='primary'
        fullWidth
        disabled={disabled || !isWin}
        loading={loading}
        size='large'
        onClick={() => onCheck?.(lottery)}
      >
        {lottery?.lotteryUser?.userCompleteAt
          ? lottery.lotteryUser.rewardedAt
            ? `Rewarded`
            : `Completed`
          : isWin
          ? `I Win (Request)`
          : 'You Loose'}
      </Button>

      <Typography
        component='p'
        variant='caption'
        color='text.secondary'
        align='center'
        sx={{ mt: 1 }}
      >
        Current Reward Per Winner {rewardPerUser} SGB
      </Typography>
    </BottomDialog>
  );
}

export default LotteryDialog;
