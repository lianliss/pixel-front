import Dialog, { DialogProps } from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import Stack from '@mui/material/Stack';
import { ILotteryDto } from 'lib/Lottery/api-server/lottery.types.ts';
import { useState } from 'react';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { Button } from '@ui-kit/Button/Button.tsx';

type Props = DialogProps & {
  lottery?: ILotteryDto | null;
  loading?: boolean;
  onStart?: (lottery: ILotteryDto, data: { start: Date; end: Date }) => void;
};

export function StartLotteryDialog(props: Props) {
  const { onStart, lottery, loading, ...other } = props;

  const [start, setStart] = useState<any | null>(null);
  const [end, setEnd] = useState<any | null>(null);

  const handleStart = () => {
    if (!start || !end || !lottery) {
      return;
    }

    onStart?.(lottery, { start: start.toDate(), end: end.toDate() });
  };

  return (
    <Dialog fullWidth maxWidth='sm' {...other}>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <DialogTitle>Start Lottery</DialogTitle>
        <DialogContent>
          <Stack direction='column' gap={2} sx={{ py: 1 }}>
            <DateTimePicker
              label='Start Date'
              value={start}
              ampm={false}
              onChange={(e) => setStart(e)}
            />
            <DateTimePicker label='End Date' value={end} ampm={false} onChange={(e) => setEnd(e)} />

            <Button
              loading={loading}
              variant='contained'
              color='primary'
              fullWidth
              disabled={!start || !end || !lottery}
              onClick={handleStart}
              size='large'
            >
              Start
            </Button>
          </Stack>
        </DialogContent>
      </LocalizationProvider>
    </Dialog>
  );
}
