import Paper from '@mui/material/Paper';
import TableContainer from '@mui/material/TableContainer';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableCell from '@mui/material/TableCell';
import TableBody from '@mui/material/TableBody';
import React from 'react';
import { TablePagination, TableRow } from '@mui/material';
import { IPaginationResult } from 'utils/hooks/usePagination.ts';
import {
  ILotteryCheckDto,
  ILotteryDto,
  LotteryCheckType,
} from 'lib/Lottery/api-server/lottery.types.ts';
import { Button } from '@ui-kit/Button/Button.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { ButtonGroup } from '@ui-kit/ButtonGroup/ButtonGroup.tsx';
import { fromUtc } from 'utils/format/date';

type Props = {
  rows: ILotteryDto[];
  count: number;
  pagination?: IPaginationResult;
  loading?: boolean;
  onEdit?: (l: ILotteryDto) => void;
  onStart?: (payload: ILotteryDto) => void;
  onStop?: (payload: { id: string }) => void;
  onComplete?: (payload: { id: string }) => void;
  onReward?: (payload: { id: string }) => void;
  onCreate?: () => void;
};

export function LotteryTable(props: Props) {
  const {
    rows = [] as ILotteryDto[],
    count = 0,
    loading,
    pagination,
    onEdit,
    onStart,
    onStop,
    onCreate,
    onComplete,
    onReward,
  } = props;

  return (
    <Paper variant='outlined' sx={{ overflow: 'hidden' }}>
      <TableContainer>
        <Table sx={{ minWidth: 800 }}>
          <TableHead>
            <TableRow>
              <TableCell sx={{ width: '70px' }}>
                <Button variant='outlined' color='primary' onClick={onCreate} disabled={!onCreate}>
                  New
                </Button>
              </TableCell>
              <TableCell sx={{ width: '150px' }}>Name</TableCell>
              <TableCell sx={{ width: '150px' }}>Status</TableCell>
              <TableCell sx={{ width: '100px' }}>Users</TableCell>
              <TableCell sx={{ width: '200px' }}>Require</TableCell>
              <TableCell sx={{ width: '100px' }}>Reward</TableCell>
              <TableCell sx={{ width: '200px' }} align='right'>
                Created
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => {
              const checkDrill = row.checks?.find(
                (el) => el.type === LotteryCheckType.DrillLevel
              ) as ILotteryCheckDto<LotteryCheckType.DrillLevel> | undefined;
              const checkStorage = row.checks?.find(
                (el) => el.type === LotteryCheckType.StorageLevel
              ) as ILotteryCheckDto<LotteryCheckType.StorageLevel> | undefined;
              const active =
                row.startedAt &&
                row.endedAt &&
                fromUtc(row.startedAt).getTime() < Date.now() &&
                fromUtc(row.endedAt).getTime() > Date.now();
              const notStarted = row.startedAt && fromUtc(row.startedAt).getTime() > Date.now();
              console.log(fromUtc(row.startedAt));
              return (
                <TableRow key={`row-${row.id}`}>
                  <TableCell>
                    <ButtonGroup>
                      <Button
                        variant='outlined'
                        color='primary'
                        disabled={
                          !onEdit || !!row.startedAt || !!row.completedAt || !!row.rewardedAt
                        }
                        onClick={() => onEdit?.(row)}
                      >
                        Edit
                      </Button>
                      <Button
                        variant='outlined'
                        color='primary'
                        disabled={
                          (active || notStarted ? !onStop : !onStart) ||
                          !!row.completedAt ||
                          !!row.rewardedAt
                        }
                        onClick={active || notStarted ? () => onStop?.(row) : () => onStart?.(row)}
                      >
                        {active || notStarted ? 'Stop' : 'Start'}
                      </Button>
                      <Button
                        variant='outlined'
                        color='primary'
                        disabled={
                          !onComplete ||
                          !row.startedAt ||
                          active ||
                          !!row.completedAt ||
                          !!row.rewardedAt ||
                          !row.rewards?.length
                        }
                        onClick={() => onComplete?.(row)}
                      >
                        Complete
                      </Button>
                      <Button
                        variant='outlined'
                        color='primary'
                        disabled={
                          !onReward ||
                          !row.startedAt ||
                          active ||
                          !row.completedAt ||
                          !!row.rewardedAt ||
                          !row.rewards?.length
                        }
                        onClick={() => onReward?.(row)}
                      >
                        Reward
                      </Button>
                    </ButtonGroup>
                  </TableCell>
                  <TableCell>
                    <Typography noWrap variant='inherit'>
                      {row.name}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography color={active ? 'success.main' : 'text.secondary'}>
                      {notStarted
                        ? `Start ${+(
                            (fromUtc(row.startedAt) - Date.now()) /
                            1000 /
                            60 /
                            60
                          ).toFixed(1)}h`
                        : active
                        ? `Active ${+((fromUtc(row.endedAt) - Date.now()) / 1000 / 60 / 60).toFixed(
                            1
                          )}h`
                        : 'Inactive'}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    {row.currentUsers}/{row.limitUsers}
                  </TableCell>
                  <TableCell>
                    Drill: {checkDrill?.context.level || 0}, Storage:{' '}
                    {checkStorage?.context.level || 0}
                  </TableCell>
                  <TableCell>{row.rewards?.[0]?.context.amount || '-'} SGB</TableCell>
                  <TableCell align='right'>
                    {new Date(row.createdAt).toLocaleString('ru-RU')}
                  </TableCell>
                </TableRow>
              );
            })}
            {!rows.length && (
              <TableRow>
                <TableCell colSpan={8}>{loading ? 'Loading...' : 'No data'}</TableCell>
              </TableRow>
            )}
          </TableBody>

          <TablePagination
            count={count}
            page={(pagination?.page || 1) - 1}
            rowsPerPage={pagination?.step || 20}
            onPageChange={(e, v) => pagination?.setPage(v)}
            rowsPerPageOptions={[20]}
          />
        </Table>
      </TableContainer>
    </Paper>
  );
}
