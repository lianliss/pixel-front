import React, { useEffect, useState } from 'react';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import {
  ICreateLotteryCheckDto,
  ICreateLotteryDto,
  ICreateLotteryRewardDto,
  ILotteryCheckDto,
  ILotteryDto,
  ILotteryReward,
  LotteryCheckType,
  LotteryRewardType,
} from 'lib/Lottery/api-server/lottery.types.ts';
import Stack from '@mui/material/Stack';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
// import DateRangePicker from "@mui/x-date-pickers/DateRangePicker";
import NumberField from '@ui-kit/NumberField/NumberField.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { NftItemRarity, NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import SelectNftCollection from 'lib/NFT/components/_admin/SelectNftCollection.tsx';
import { NFT_CONTRACT_ID } from 'lib/NFT/api-contract/nft.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import SelectNftRarity from 'lib/NFT/components/_admin/SelectNftRarity.tsx';
import SelectNftSlot from 'lib/NFT/components/_admin/SelectNftSlot.tsx';
import { ZERO_ADDRESS } from '@cfg/config.ts';
import Box from '@mui/material/Box';
import FormControlLabel from '@mui/material/FormControlLabel';
import { Switch } from '@ui-kit/Switch/Switch.tsx';
import { useToken } from '@chain/hooks/useToken.ts';

type Props = DialogProps & {
  lottery?: ILotteryDto | true;
  onSave?: (lottery: Omit<ICreateLotteryDto, 'userAddress'>, id: string | null) => void;
  loading?: boolean;
};

function EditLotteryDialog(props: Props) {
  const { onClose, lottery, onSave, loading = false, ...other } = props;

  const account = useAccount();
  const token = useToken({});
  const nftAddress = NFT_CONTRACT_ID[account.chainId];

  const [name, setName] = useState('');
  const [summary, setSummary] = useState('');
  const [limitUsers, setLimitUsers] = useState('100');
  const [requireDrill, setRequireDrill] = useState<string>('');
  const [requireStorage, setRequireStorage] = useState<string>('');
  const [nftRarity, setNftRarity] = useState<NftItemRarity | null>(null);
  const [nftCollection, setNftCollection] = useState<string | null>(null);
  const [nftCollectionIndex, setNftCollectionIndex] = useState<number | null>(null);
  const [tokenAmount, setTokenAmount] = useState<string | null>(null);
  const [isFixedReward, setIsFixedReward] = useState<boolean>(true);
  const [nftSlot, setNftSlot] = useState<NftItemSlot | null>(null);
  const [nftTypeId, setNftTypeId] = useState<string | null>(null);
  const [nftCount, setNftCount] = useState<string>('0');

  useEffect(() => {
    if (typeof lottery === 'object' && !!lottery) {
      setSummary(lottery.summary);
      setName(lottery.name);
      setLimitUsers(lottery.limitUsers.toString());
      setIsFixedReward(lottery.isFixedReward);

      const nftCountCheck = lottery.checks?.find((el) => el.type === LotteryCheckType.NftCount) as
        | ILotteryCheckDto<LotteryCheckType.NftCount>
        | undefined;
      const checkDrill = lottery.checks?.find((el) => el.type === LotteryCheckType.DrillLevel) as
        | ILotteryCheckDto<LotteryCheckType.DrillLevel>
        | undefined;
      const checkStorage = lottery.checks?.find(
        (el) => el.type === LotteryCheckType.StorageLevel
      ) as ILotteryCheckDto<LotteryCheckType.StorageLevel> | undefined;

      if (nftCountCheck) {
        setNftCount(nftCountCheck.context.count?.toString() || '');
      } else {
        setNftCount('');
      }
      if (checkDrill) {
        setRequireDrill(checkDrill.context.level.toString());
      } else {
        setRequireDrill('');
      }
      if (checkStorage) {
        setRequireStorage(checkStorage.context.level.toString());
      } else {
        setRequireStorage('');
      }
      if (typeof nftCountCheck.context.rarity !== 'undefined') {
        setNftRarity(nftCountCheck.context.rarity as NftItemRarity);
      } else {
        setNftRarity(null);
      }
      if (typeof nftCountCheck.context.collectionId !== 'undefined') {
        setNftCollection(nftCountCheck.context.collectionId);
        setNftCollectionIndex(nftCountCheck.context.collectionIndex);
      } else {
        setNftCollection(null);
        setNftCollectionIndex(null);
      }
      if (typeof nftCountCheck.context.slot !== 'undefined') {
        setNftSlot(nftCountCheck.context.slot);
      } else {
        setNftSlot(null);
      }

      const firstReward = lottery.rewards?.[0] as
        | ILotteryReward<LotteryRewardType.Token>
        | undefined;

      if (firstReward) {
        setTokenAmount(firstReward.context.amount.toString());
      } else {
        setTokenAmount('');
      }
    }
  }, [lottery]);

  const handleSave = () => {
    const checks: ICreateLotteryCheckDto[] = [];
    const rewards: ICreateLotteryRewardDto[] = [];

    if (!!nftCount) {
      checks.push({
        type: LotteryCheckType.NftCount,
        context: {
          count: +nftCount,
          rarity: nftRarity,
          collectionId: nftCollection,
          collectionIndex: nftCollectionIndex,
          slot: nftSlot,
          address: nftAddress?.toLowerCase(),
        },
      });
    }
    if (!!+requireDrill) {
      checks.push({
        type: LotteryCheckType.DrillLevel,
        context: { level: +requireDrill },
      });
    }
    if (!!+requireStorage) {
      checks.push({
        type: LotteryCheckType.StorageLevel,
        context: { level: +requireStorage },
      });
    }
    if (!!+tokenAmount) {
      rewards.push({
        type: LotteryRewardType.Token,
        context: { chainId: 19, amount: +tokenAmount, address: ZERO_ADDRESS },
      });
    }

    onSave?.(
      {
        name,
        summary,
        limitUsers: +limitUsers,
        checks: checks,
        rewards: rewards,
        isFixedReward,
      },
      (lottery as ILotteryDto)?.id || null
    );
  };

  const disabled = !name || !summary || !+limitUsers || !nftCount || !+tokenAmount;
  const disabledFields = loading;

  const content = (
    <Stack direction='column' gap={2} sx={{ flex: 1 }}>
      <TextField
        label='Title'
        fullWidth
        value={name}
        onChange={(e) => setName(e.target.value)}
        required
        disabled={disabledFields}
      />
      <TextField
        label='Summary'
        fullWidth
        multiline
        minRows={3}
        value={summary}
        onChange={(e) => setSummary(e.target.value)}
        disabled={disabledFields}
      />

      <Stack direction='row' gap={2} sx={{ width: '100%' }}>
        <NumberField
          label='Limit Users'
          fullWidth
          value={limitUsers}
          onChange={(e) => setLimitUsers(e.target.value)}
          required
          disabled={disabledFields}
        />
        {/*<DateRangePicker localeText={{ start: 'Check-in', end: 'Check-out' }} />*/}
      </Stack>

      <Typography>Miner requirements</Typography>

      <Stack direction='row' gap={2} sx={{ width: '100%' }}>
        <NumberField
          label='Require Drill'
          fullWidth
          value={requireDrill}
          onChange={(e) => setRequireDrill(e.target.value)}
          disabled={disabledFields}
        />
        <NumberField
          label='Require Storage'
          fullWidth
          value={requireStorage}
          onChange={(e) => setRequireStorage(e.target.value)}
          disabled={disabledFields}
        />
      </Stack>

      <Typography>NFT requirements</Typography>

      <NumberField
        label='NFT count'
        fullWidth
        value={nftCount}
        onChange={(e) => setNftCount(e.target.value)}
        disabled={disabledFields}
      />

      <Stack direction='row' gap={2} sx={{ width: '100%' }}>
        <SelectNftCollection
          contractId={nftAddress}
          fullWidth
          value={nftCollection}
          disabled={disabledFields}
          onChange={(v, v2) => {
            setNftCollection(v);
            setNftCollectionIndex(v2);
          }}
        />
        <SelectNftRarity
          fullWidth
          value={nftRarity}
          onChange={(v) => setNftRarity(v)}
          disabled={disabledFields}
        />
        <SelectNftSlot
          fullWidth
          value={nftSlot}
          onChange={(v) => setNftSlot(v)}
          disabled={disabledFields}
        />
      </Stack>

      <Typography>Rewards</Typography>

      <Box sx={{ display: 'flex', gap: 2, alignItems: 'center' }}>
        <NumberField
          label={`${token.data?.symbol} amount`}
          fullWidth
          value={tokenAmount}
          required
          disabled={disabledFields}
          onChange={(e) => setTokenAmount(e.target.value)}
        />
        <FormControlLabel
          sx={{ width: 250 }}
          label='Fixed Reward'
          control={
            <Switch checked={isFixedReward} onChange={(e) => setIsFixedReward(e.target.checked)} />
          }
        />

        <Typography sx={{ width: 300 }}>
          Per user:{' '}
          {isFixedReward
            ? tokenAmount / limitUsers
            : tokenAmount / 10_000 + ' - ' + tokenAmount / limitUsers}{' '}
          {token.data?.symbol}
        </Typography>
      </Box>

      <Button
        size='large'
        variant='contained'
        color='primary'
        onClick={handleSave}
        disabled={!onSave || disabled}
        loading={loading}
      >
        {lottery === true ? 'Create' : 'Update'}
      </Button>
    </Stack>
  );

  return (
    <Dialog onClose={onClose} {...other} fullWidth maxWidth='lg' scroll='body'>
      <DialogTitle>
        {lottery === true ? `Create Lottery` : `Edit Lottery ${lottery?.id || ''}`}
      </DialogTitle>
      <div style={{ padding: 24 }}>{content}</div>
    </Dialog>
  );
}

export default EditLotteryDialog;
