import Paper, { PaperProps } from '@mui/material/Paper';
import { alpha, styled } from '@mui/material/styles';
import Box, { BoxProps } from '@mui/material/Box';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import CheckOutlined from '../../../shared/icons/CheckOutlined.tsx';
import DrillIcon from '../../../shared/icons/Drill.tsx';
import StorageIcon from '../../../shared/icons/Storage.tsx';
import React from 'react';
import LotteryTimeLeft from 'lib/Lottery/components/LotteryTimeLeft.tsx';
import { Img } from '@ui-kit/Img/Img.tsx';
import sgbAlt from 'assets/img/token/sgb/sgb-alt.svg';

const StyledRoot = styled(Box)(({ ownerState, theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  gap: 8,
  padding: 8,
  borderRadius: 16,

  variants: [
    {
      props: {
        variant: 'success',
      },
      style: {
        backgroundColor: alpha(theme.palette.success.main, 0.1),
        border: `1px solid ${theme.palette.success.main}`,
      },
    },
    {
      props: {
        variant: 'failed',
      },
      style: {
        backgroundColor: alpha(theme.palette.error.main, 0.1),
        border: `1px solid ${theme.palette.error.main}`,
      },
    },
    {
      props: {
        variant: 'new',
      },
      style: {
        backgroundColor: alpha(theme.palette.warning.main, 0.1),
        border: `1px solid ${theme.palette.warning.main}`,
      },
    },
    {
      props: {
        variant: 'active',
      },
      style: {
        backgroundColor: alpha(theme.palette.info.main, 0.1),
        border: `1px solid ${theme.palette.info.main}`,
      },
    },
  ],
}));
const StyledBody = styled(Paper)(() => ({
  display: 'flex',
  justifyContent: 'space-between',
  gap: 16,
  padding: '8px 12px',
}));

type LotteryStatus = 'success' | 'failed' | 'new' | 'active';
type Props = BoxProps & {
  status?: LotteryStatus;
  current?: number;
  limit?: number;
  rewardAmount?: number;
  requireDrill?: number;
  requireStorage?: number;
  drillLevel?: number;
  storageLevel?: number;
  onDetails?: () => void;
  startedAt?: number;
  endedAt?: number;
  title?: string;
};

function LotteryCard(props: Props) {
  const {
    title,
    status = 'new',
    limit = 0,
    current = 0,
    rewardAmount = '-',

    requireDrill = 0,
    requireStorage = 0,
    onDetails,
    startedAt,
    endedAt,
    drillLevel = 0,
    storageLevel = 0,
    ...other
  } = props;

  const ownerState = { variant: status };
  const color =
    status === 'success'
      ? 'success'
      : status === 'failed'
      ? 'error'
      : status === 'active'
      ? 'info'
      : 'warning';
  const statusColor =
    status === 'success'
      ? 'success.main'
      : status === 'failed'
      ? 'error.main'
      : status === 'active'
      ? 'info.main'
      : 'warning.main';
  const statusText =
    status === 'success'
      ? 'You Win!'
      : status === 'failed'
      ? 'You Loose'
      : status === 'active'
      ? 'Active'
      : 'Soon';

  const utcNow = Date.now();
  const isActive = startedAt <= utcNow && endedAt > utcNow;

  const isEnded = endedAt <= utcNow;
  const isAvailable =
    endedAt && endedAt > utcNow && storageLevel >= requireStorage && drillLevel >= requireDrill;

  return (
    <StyledRoot ownerState={ownerState} {...other}>
      <Box sx={{ width: '100%', display: 'flex', justifyContent: 'space-between', pl: 1 }}>
        <Box sx={{ gap: '14px', display: 'flex', alignItems: 'center', width: '100%' }}>
          <div>
            <CheckOutlined sx={{ fontSize: 32 }} color={color} />
          </div>

          <Box sx={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
            <Box sx={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
              <Typography sx={{ lineHeight: '21px' }}>
                <Typography
                  variant='subtitle1'
                  fontWeight='bold'
                  component='span'
                  noWrap
                  color='text.primary'
                >
                  {title || 'Lottery'}{' '}
                </Typography>
                <Typography
                  variant='caption'
                  fontWeight='bold'
                  color='success.main'
                  component='span'
                >
                  {isActive ? 'in progress' : isEnded ? 'ended' : 'soon'}
                </Typography>
              </Typography>

              <Typography variant='subtitle1' fontWeight='bold' align='right' color={statusColor}>
                {statusText}
              </Typography>
            </Box>

            <Box sx={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
              <Typography variant='caption' sx={{ lineHeight: '21px' }}>
                <Typography
                  variant='inherit'
                  component='span'
                  sx={{ opacity: 0.5 }}
                  color='text.primary'
                >
                  Limit:{' '}
                </Typography>
                <Typography variant='inherit' component='span' color='success.main'>
                  {current}
                </Typography>
                <Typography
                  variant='inherit'
                  component='span'
                  color='text.primary'
                  sx={{ opacity: 0.5 }}
                >
                  /{limit}
                </Typography>
              </Typography>

              <LotteryTimeLeft endedAt={endedAt} startedAt={startedAt} align='right' />
            </Box>
          </Box>
        </Box>
      </Box>

      <StyledBody elevation={0}>
        <Box>
          <Typography variant='caption' color='text.secondary'>
            Requirements:
          </Typography>
          <Box sx={{ display: 'flex', gap: 1 }}>
            {!!requireDrill && (
              <Typography
                variant='subtitle2'
                color={requireDrill > drillLevel ? 'error.main' : 'success.main'}
                sx={{ display: 'flex', alignItems: 'center', gap: '3px' }}
              >
                <DrillIcon sx={{ fontSize: 16, color: 'text.secondary' }} />

                {requireDrill}
              </Typography>
            )}
            {!!requireStorage && (
              <Typography
                variant='subtitle2'
                color={requireStorage > storageLevel ? 'error.main' : 'success.main'}
                sx={{ display: 'flex', alignItems: 'center', gap: '3px' }}
              >
                <StorageIcon sx={{ fontSize: 16, color: 'text.secondary' }} />

                {requireStorage}
              </Typography>
            )}
            {!requireStorage && !requireDrill && '-'}
          </Box>
        </Box>

        <Box>
          <Typography variant='caption' color='text.secondary'>
            Prize Fund:
          </Typography>
          <Box sx={{ display: 'flex', alignItems: 'center', gap: 1 }}>
            <Img src={sgbAlt as string} sx={{ width: 18 }} />

            <Typography sx={{ fontSize: 18, lineHeight: '18px' }} fontWeight='bold'>
              <Typography variant='inherit' component='span'>
                {rewardAmount}{' '}
              </Typography>
              <Typography variant='inherit' component='span' sx={{ opacity: 0.5 }}>
                SGB
              </Typography>
            </Typography>
          </Box>
        </Box>
      </StyledBody>

      <Button
        variant='contained'
        size='large'
        color={color}
        fullWidth
        onClick={onDetails}
        disabled={!onDetails}
      >
        Details
      </Button>
    </StyledRoot>
  );
}

export default LotteryCard;
