import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react';
import { useCallback, useEffect } from 'react';
import toaster from 'services/toaster.tsx';
import { loadLotteryStatus, lotteryStatusStore } from 'lib/Lottery/api-server/lottery.store.ts';

export function useLotteryStatus({ id, skip }: { id?: string; skip?: boolean }) {
  const account = useAccount();
  const statusData = useUnit(lotteryStatusStore);

  const refetch = useCallback(async () => {
    if (account.isConnected && id) {
      try {
        await loadLotteryStatus({ lotteryId: id, userAddress: account.accountAddress });
      } catch (e) {
        toaster.captureException(e);
      }
    }
  }, [account.accountAddress, account.isConnected, id]);

  useEffect(() => {
    if (!skip) {
      refetch();
    }
  }, [refetch, skip]);

  return {
    data: statusData.data,
    error: statusData.error,
    loading: statusData.loading,
    refetch,
  };
}
