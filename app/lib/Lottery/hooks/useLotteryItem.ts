import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react';
import { useCallback, useEffect } from 'react';
import toaster from 'services/toaster.tsx';
import { loadLotteryItem, lotteryItemStore } from 'lib/Lottery/api-server/lottery.store.ts';

export function useLottery({ id }: { id?: string }) {
  const account = useAccount();
  const list = useUnit(lotteryItemStore);

  const refetch = useCallback(async () => {
    if (account.isConnected && id) {
      try {
        await loadLotteryItem({ lotteryId: id, userAddress: account.accountAddress });
      } catch (e) {
        toaster.captureException(e);
      }
    }
  }, [account.accountAddress, account.isConnected, id]);

  useEffect(() => {
    refetch();
  }, [refetch]);

  return {
    data: list.data,
    error: list.error,
    loading: list.loading,
    refetch,
  };
}
