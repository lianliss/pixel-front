import { checkLottery } from 'lib/Lottery/api-server/lottery.store.ts';
import { useUnit } from 'effector-react';

export function useLotteryApi({ userAddress }: { userAddress?: string }) {
  const checkLotteryLoading = useUnit(checkLottery.pending);

  const checkUserLottery = (lotteryId: string) => checkLottery({ userAddress, lotteryId });

  return {
    disabled: !userAddress,
    loading: checkLotteryLoading,
    checkLottery: checkUserLottery,
  };
}
