import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react';
import { useCallback, useEffect } from 'react';
import toaster from 'services/toaster.tsx';
import { IPaginationResult } from 'utils/hooks/usePagination.ts';
import {
  loadAdminLotteryList,
  lotteryAdminListStore,
} from 'lib/Lottery/api-server/lottery.store.ts';

export function useAdminLotteryList({ pagination }: { pagination?: IPaginationResult }) {
  const account = useAccount();
  const list = useUnit(lotteryAdminListStore);

  const refetch = useCallback(async () => {
    if (account.isConnected) {
      try {
        await loadAdminLotteryList({ limit: 100, offset: 0, userAddress: account.accountAddress });
      } catch (e) {
        toaster.captureException(e);
      }
    }
  }, [account.accountAddress, account.isConnected]);

  useEffect(() => {
    refetch();
  }, [refetch]);
  useEffect(() => {
    pagination.setTotal(list.data?.count || 0);
  }, [list.data?.count]);

  return {
    data: list.data,
    error: list.error,
    loading: list.loading,
    refetch,
  };
}
