import {
  completeLottery,
  createLottery,
  rewardLottery,
  startLottery,
  stopLottery,
  updateLottery,
} from 'lib/Lottery/api-server/lottery.store.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { ICreateLotteryDto } from 'lib/Lottery/api-server/lottery.types.ts';
import { useUnit } from 'effector-react';

export function useAdminLotteryApi() {
  const account = useAccount();

  const stop = async (payload: { id: string }) => {
    await stopLottery({
      lotteryId: payload.id,
      userAddress: account.accountAddress,
    });
  };

  const start = async (payload: { startedAt: number; endedAt: number; id: string }) => {
    await startLottery({
      lotteryId: payload.id,
      startedAt: payload.startedAt,
      endedAt: payload.endedAt,
      userAddress: account.accountAddress,
    });
  };

  const complete = async (payload: { id: string }) => {
    return completeLottery({
      lotteryId: payload.id,
      userAddress: account.accountAddress,
    });
  };

  const reward = async (payload: { id: string }) => {
    return rewardLottery({
      lotteryId: payload.id,
      userAddress: account.accountAddress,
    });
  };

  const create = async (lottery: Omit<ICreateLotteryDto, 'userAddress'>) => {
    await createLottery({
      name: lottery.name,
      claimDuration: lottery.claimDuration,
      summary: lottery.summary,
      limitUsers: lottery.limitUsers,
      checks: lottery.checks,
      userAddress: account.accountAddress,
      rewards: lottery.rewards,
      isFixedReward: lottery.isFixedReward,
    });
  };

  const update = async (lottery: Omit<ICreateLotteryDto, 'userAddress'>, id: string) => {
    await updateLottery({
      name: lottery.name,
      claimDuration: lottery.claimDuration,
      summary: lottery.summary,
      limitUsers: lottery.limitUsers,
      userAddress: account.accountAddress,
      checks: lottery.checks,
      lotteryId: id,
      rewards: lottery.rewards,
      isFixedReward: lottery.isFixedReward,
    });
  };

  const loadingUpdate = useUnit(updateLottery.pending);
  const loadingCreate = useUnit(createLottery.pending);
  const loadingStart = useUnit(startLottery.pending);
  const loadingStop = useUnit(stopLottery.pending);
  const loadingComplete = useUnit(completeLottery.pending);
  const loadingReward = useUnit(rewardLottery.pending);

  return {
    loading:
      loadingComplete ||
      loadingReward ||
      loadingUpdate ||
      loadingCreate ||
      loadingStop ||
      loadingStart,
    start,
    stop,
    create,
    update,
    complete,
    reward,
  };
}
