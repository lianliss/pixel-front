import React, { useState, useEffect } from 'react';
import { usePagination } from 'utils/hooks/usePagination.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import Box from '@mui/material/Box';
import { useLotteryList } from 'lib/Lottery/hooks/useLotteryList.ts';
import Stack from '@mui/material/Stack';
import { Button } from '@ui-kit/Button/Button.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Container from '@mui/material/Container';
import { useLotteryApi } from 'lib/Lottery/hooks/useLotteryApi.ts';
import toaster from 'services/toaster.tsx';
import { useNavigate } from 'react-router-dom';
import LotteryCard from 'lib/Lottery/components/LotteryCard.tsx';
import { ILotteryDto, LotteryCheckType } from 'lib/Lottery/api-server/lottery.types.ts';
import { useMiningStorage } from 'lib/Mining/hooks/useMiningStorage.ts';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';

import LotteryDialog from 'lib/Lottery/components/LotteryDialog.tsx';
import { getLotteryStatus } from 'lib/Lottery/utils/getLotteryStatus.ts';
import CircularProgress from '@mui/material/CircularProgress';
import bgImage from 'assets/img/lottery/bg.png';
import { isLotteryChecksCompleted } from 'lib/Lottery/utils/getLotteryHaveNftList.ts';
import { fromUtc } from 'utils/format/date';
import { getDefaultChainImage } from '@cfg/image.ts';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

function LotteryListPage() {
  const account = useAccount();
  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;
  const navigate = useNavigate();

  const minerAddress = MINER_CONTRACT_ID[account.chainId]?.core;
  const mining = useMiningStorage({ minerAddress, telegramId: telegramId });
  const drillLevel = mining.data?.extra?.speedLevel;
  const storageLevel = mining.data?.extra?.sizeLevel;

  const pagination = usePagination({ step: 20 });
  const lotteryList = useLotteryList({
    pagination,
  });
  const lotteryApi = useLotteryApi({
    userAddress: account.accountAddress,
  });

  const [lottery, setLottery] = useState<ILotteryDto | null>(null);

  const handleDetails = (l: ILotteryDto) => () => setLottery(l);
  const handleCheck = async (l: ILotteryDto) => {
    try {
      const lotteryUser = await lotteryApi.checkLottery(l.id);

      if (!!lotteryUser.userCompleteAt) {
        toaster.success('Lottery completed');
        await lotteryList.refetch();
      } else {
        toaster.warning('Lottery not completed');
      }

      await lotteryList.refetch();
    } catch (e) {
      toaster.captureException(e);
    }
  };

  // useEffect(() => {
  //   setLottery(lotteryList.data?.rows[0]);
  // }, [lotteryList.data?.rows]);
  useEffect(() => {
    if (!lottery || !lotteryList.data) {
      return;
    }

    const nextlottery = lotteryList.data.rows.find((el) => el.id === lottery.id);
    setLottery(nextlottery);
  }, [lottery, lotteryList.data]);

  return (
    <Box
      sx={{
        backgroundImage: `url(${getDefaultChainImage(account.chainId)})`,
        backgroundSize: 'cover',
        height: '100%',
      }}
    >
      <Container maxWidth='xs'>
        <Box>
          <Typography
            sx={{ mb: 0.5, mt: 3, fontSize: 18 }}
            fontWeight='bold'
            align='center'
            color='text.primary'
          >
            Lottery
          </Typography>

          <Typography
            variant='subtitle2'
            sx={{ mb: 2.5, opacity: 0.5 }}
            align='center'
            color='text.primary'
          >
            Get a chance to win a prize
          </Typography>

          {lotteryList.loading && !lotteryList.data && (
            <Box sx={{ display: 'flex', justifyContent: 'center', mt: 5 }}>
              <CircularProgress />
            </Box>
          )}

          {!lotteryList.data && !lotteryList.loading && !lotteryList.error && (
            <Typography align='center' color='text.primary'>
              No lotteries
            </Typography>
          )}

          {!lotteryList.loading && !!lotteryList.error && (
            <Typography align='center' color='text.primary'>
              Failed to load lotteries
            </Typography>
          )}

          {lotteryList.data && (
            <Stack direction='column' gap={2}>
              {lotteryList.data?.rows.map((lottery) => {
                const status = getLotteryStatus(lottery);
                const reward = lottery.rewards?.[0];
                const rewardAmount = reward?.context?.amount;
                const requireDrill = lottery.checks?.find(
                  (el) => el.type === LotteryCheckType.DrillLevel
                )?.context?.level;
                const requireStorage = lottery.checks?.find(
                  (el) => el.type === LotteryCheckType.StorageLevel
                )?.context?.level;

                return (
                  <div key={`lottery-${lottery.id}`}>
                    <LotteryCard
                      title={lottery.name}
                      status={status}
                      rewardAmount={rewardAmount}
                      limit={lottery.limitUsers}
                      current={lottery.currentUsers}
                      requireDrill={requireDrill}
                      requireStorage={requireStorage}
                      endedAt={lottery.endedAt ? lottery.endedAt : undefined}
                      startedAt={lottery.startedAt ? lottery.startedAt : undefined}
                      onDetails={handleDetails(lottery)}
                      drillLevel={drillLevel}
                      storageLevel={storageLevel}
                    />
                  </div>
                );
              })}
            </Stack>
          )}
        </Box>

        <LotteryDialog
          open={!!lottery}
          lottery={lottery}
          onClose={() => setLottery(null)}
          disabled={lotteryApi.disabled || !!lottery?.lotteryUser?.userCompleteAt}
          loading={lotteryApi.loading && !lottery?.lotteryUser}
          onCheck={handleCheck}
          drillLevel={drillLevel}
          storageLevel={storageLevel}
        />
      </Container>
    </Box>
  );
}

export default LotteryListPage;
