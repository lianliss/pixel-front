import React, { useState } from 'react';
import { AdminLayout } from 'lib/AdminV5/components/AdminLayout/AdminLayout.tsx';
import { usePagination } from 'utils/hooks/usePagination.ts';
import { LotteryTable } from 'lib/Lottery/components/admin/LotteryTable.tsx';
import { useAdminLotteryList } from 'lib/Lottery/hooks/useAdminLotteryList.ts';
import EditLotteryDialog from 'lib/Lottery/components/admin/EditLotteryDialog.tsx';
import { ICreateLotteryDto, ILotteryDto } from 'lib/Lottery/api-server/lottery.types.ts';
import toaster from 'services/toaster.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useAdminLotteryApi } from 'lib/Lottery/hooks/useAdminLotteryApi.ts';
import { StartLotteryDialog } from 'lib/Lottery/components/admin/StartLotteryDialog.tsx';
import { toUTC } from 'utils/format/date';

function AdminLotteryPage() {
  const account = useAccount();

  const pagination = usePagination({ step: 20 });
  const lotteryList = useAdminLotteryList({
    pagination,
  });
  const adminApi = useAdminLotteryApi();

  const [lottery, setLottery] = useState<ILotteryDto | true | null>(null);
  const [startDialog, setStartDialog] = useState<ILotteryDto | null>(null);

  const handleStart = async (lottery: ILotteryDto, payload: { start: Date; end: Date }) => {
    try {
      await adminApi.start({
        id: lottery.id,
        startedAt: toUTC(payload.start).getTime(),
        endedAt: toUTC(payload.end).getTime(),
      });

      setStartDialog(null);
      await lotteryList.refetch();
      toaster.success('Started');
    } catch (e) {
      console.log(e);
      toaster.captureException(e);
    }
  };

  const handleStop = async (payload: { id: string }) => {
    try {
      await adminApi.stop({
        id: payload.id,
      });

      await lotteryList.refetch();
      toaster.success('Stopped');
    } catch (e) {
      toaster.captureException(e);
    }
  };

  const handleComplete = async (payload: { id: string }) => {
    try {
      await adminApi.complete({
        id: payload.id,
      });

      await lotteryList.refetch();
      toaster.success('Setup completed');
    } catch (e) {
      toaster.captureException(e);
    }
  };

  const handleReward = async (payload: { id: string }) => {
    try {
      await adminApi.reward({
        id: payload.id,
      });

      await lotteryList.refetch();
      toaster.success('Run rewards');
    } catch (e) {
      toaster.captureException(e);
    }
  };

  const handleSave = async (lottery: Omit<ICreateLotteryDto, 'userAddress'>, id: string | null) => {
    try {
      if (id === null) {
        await adminApi.create(lottery);
      } else {
        await adminApi.update(lottery, id);
      }

      await lotteryList.refetch();
      setLottery(null);
    } catch (e) {
      toaster.captureException(e);
    }
  };

  // useEffect(() => {
  //   setLottery(true || lotteryList.data?.rows[0] || null);
  // }, [lotteryList.data]);
  //
  return (
    <AdminLayout>
      <LotteryTable
        rows={lotteryList.data?.rows}
        count={lotteryList.data?.count}
        pagination={pagination}
        loading={lotteryList.loading}
        onEdit={(l: ILotteryDto) => setLottery(l)}
        onStart={(l) => setStartDialog(l)}
        onStop={handleStop}
        onComplete={handleComplete}
        onReward={handleReward}
        onCreate={() => setLottery(true)}
      />

      <EditLotteryDialog
        open={lottery !== null}
        lottery={lottery}
        loading={adminApi.loading}
        onClose={() => setLottery(null)}
        onSave={handleSave}
      />

      <StartLotteryDialog
        open={!!startDialog}
        lottery={startDialog}
        onClose={() => setStartDialog(null)}
        onStart={handleStart}
        loading={adminApi.loading}
      />
    </AdminLayout>
  );
}

export default AdminLotteryPage;
