import { createEffect } from 'effector';
import { createStoreState } from 'utils/store/createStoreState.ts';
import {
  ICheckLotteryDto,
  ICompleteLotteryDto,
  ICreateLotteryDto,
  IFindLotteryDto,
  IGetLotteryDto,
  IGetLotteryStatusDto,
  IRewardLotteryDto,
  IStartLotteryDto,
  IStopLotteryDto,
  IUpdateLotteryDto,
} from 'lib/Lottery/api-server/lottery.types.ts';
import { lotteryApi } from 'lib/Lottery/api-server/lottery.api.ts';

// create lottery

export const createLottery = createEffect(async (opts: ICreateLotteryDto) => {
  const res = await lotteryApi.createLottery(opts);
  return res;
});

// check lottery

export const checkLottery = createEffect(async (opts: ICheckLotteryDto) => {
  const res = await lotteryApi.checkLottery(opts);
  return res;
});

// update lottery

export const updateLottery = createEffect(async (opts: IUpdateLotteryDto) => {
  const res = await lotteryApi.updateLottery(opts);
  return res;
});

// start

export const startLottery = createEffect(async (opts: IStartLotteryDto) => {
  const res = await lotteryApi.startLottery(opts);
  return res;
});

// stop

export const stopLottery = createEffect(async (opts: IStopLotteryDto) => {
  const res = await lotteryApi.stopLottery(opts);
  return res;
});

// complete

export const completeLottery = createEffect(async (opts: ICompleteLotteryDto) => {
  const res = await lotteryApi.completeLottery(opts);
  return res;
});

// reward

export const rewardLottery = createEffect(async (opts: IRewardLotteryDto) => {
  const res = await lotteryApi.rewardLottery(opts);
  return res;
});

// lottery admin list

export const loadAdminLotteryList = createEffect(async (opts: IFindLotteryDto) => {
  const res = await lotteryApi.adminFindLottery(opts);
  return res;
});

export const lotteryAdminListStore = createStoreState(loadAdminLotteryList);

// lottery list

export const loadLotteryList = createEffect(async (opts: IFindLotteryDto) => {
  const res = await lotteryApi.userFindLottery(opts);
  return res;
});

export const lotteryListStore = createStoreState(loadLotteryList);

// lottery item

export const loadLotteryItem = createEffect(async (opts: IGetLotteryDto) => {
  const res = await lotteryApi.getLottery(opts);
  return res;
});

export const lotteryItemStore = createStoreState(loadLotteryItem);

// lottery status

export const loadLotteryStatus = createEffect(async (opts: IGetLotteryStatusDto) => {
  const res = await lotteryApi.getLotteryStatus(opts);
  return res;
});

export const lotteryStatusStore = createStoreState(loadLotteryStatus, { loading: false });
