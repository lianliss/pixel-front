// dto

import { IPaginatedResult } from 'lib/Quests/api-server/quests.types.ts';
import { IPaginatedQuery } from 'lib/Marketplace/api-server/market.types.ts';
import { INftItemDto, NftItemRarity, NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';

// dto

export type ILotteryDto<TDate = number> = Omit<
  ILottery<TDate>,
  'startedAt' | 'checks' | 'rewards'
> & {
  startedAt: TDate | null;
  checks?: ILotteryCheckDto<TDate>[];
  rewards?: ILotteryRewardDto<TDate>[];
  lotteryUser: ILotteryUserDto<TDate> | null;
};

export enum LotteryCheckType {
  NftCount = 'NftCount',
  //
  DrillLevel = 'DrillLevel',
  StorageLevel = 'StorageLevel',
}

export enum LotteryRewardType {
  Token = 'Token',
}

export type ILotteryCheckDto<T extends LotteryCheckType, TDate = number> = ILotteryCheck<T, TDate>;

export type ILotteryUserDto<TDate = number> = Omit<ILotteryUser<TDate>, 'userRewards'> & {
  userRewards: ILotteryUserRewardDto<TDate>[] | null;
};

export type ILotteryRewardDto<TDate = number> = ILotteryReward<TDate>;

export type ILotteryUserRewardDto<TDate = number> = ILotteryUserReward<LotteryRewardType, TDate>;

// entity

export type ILottery<TDate = Date> = {
  id: string;

  name: string;
  summary: string;

  currentUsers: number;
  limitUsers: number;
  isFixedReward: boolean;

  completedAt: TDate | null;
  rewardedAt: TDate | null;
  startedAt: TDate | null;
  endedAt: TDate | null;
  createdAt: TDate;

  // relations
  checks?: ILotteryCheck<LotteryCheckType, TDate>[];
  rewards?: ILotteryReward<LotteryRewardType, TDate>[];
};

export type ILotteryCheckContextMap = {
  [LotteryCheckType.NftCount]: {
    count: number;
    address: string;
    nftTypeId?: string;
    nftTypeImage?: string;
    rarity?: NftItemRarity;
    slot?: NftItemSlot;
    collectionId?: string;
    collectionIndex?: number;
  };
  [LotteryCheckType.DrillLevel]: { level: number };
  [LotteryCheckType.StorageLevel]: { level: number };
};

export type ILotteryCheck<Type extends LotteryCheckType, TDate = Date> = {
  id: string;

  lotteryId: string;

  type: Type;
  context: ILotteryCheckContextMap[Type];

  createdAt: TDate;
};

export type ILotteryUser<TDate = Date> = {
  id: string;

  lotteryId: string;
  userAddress: string;

  completedAt: TDate | null;
  userCompleteAt: TDate | null;
  rewardedAt: TDate | null;
  createdAt: TDate;
};

export type ILotteryRewardContextMap = {
  [LotteryRewardType.Token]: { amount: number; address: string; chainId: number };
};

export type ILotteryReward<Type extends LotteryRewardType, TDate = Date> = {
  id: string;

  lotteryId: string;

  type: Type;
  context: ILotteryRewardContextMap[Type];

  createdAt: TDate;
};

export type ILotteryUserRewardContextMap = {
  [LotteryRewardType.Token]: { amount: number; address: string; chainId: number };
};

export type ILotteryUserReward<Type extends LotteryRewardType, TDate = Date> = {
  id: string;

  lotteryUserId: string;

  type: Type;
  context: ILotteryUserRewardContextMap[Type];

  rewardedAt: TDate | null;
  createdAt: TDate;

  lotteryUser?: ILotteryUser;
};

// request

export type IGetLotteryDto = { lotteryId: string } & { userAddress: string };

export type IGetLotteryStatusDto = { lotteryId: string } & { userAddress: string };

export type IFindLotteryDto = IPaginatedQuery & { userAddress: string };

export type ICreateLotteryCheckDto = {
  type: LotteryCheckType;
  context: ILotteryCheckContextMap[LotteryCheckType];
};

export type ICreateLotteryRewardDto = {
  type: LotteryRewardType;
  context: ILotteryRewardContextMap[LotteryRewardType];
};

export type ICreateLotteryDto = {
  name: string;
  summary: string;
  limitUsers: number;
  isFixedReward: boolean;
  checks: ICreateLotteryCheckDto[];
  rewards: ICreateLotteryRewardDto[];
} & { userAddress: string };

export type ICheckLotteryDto = { lotteryId: string } & { userAddress: string };

export type IStartLotteryDto = { lotteryId: string; startedAt: number; endedAt: number } & {
  userAddress: string;
};

export type IStopLotteryDto = { lotteryId: string } & { userAddress: string };

export type ICompleteLotteryDto = { lotteryId: string } & { userAddress: string };

export type IRewardLotteryDto = { lotteryId: string } & { userAddress: string };

export type IUpdateLotteryDto = {
  lotteryId: string;
} & ICreateLotteryDto;

// response

export type IGetLotteryResponse = ILotteryDto;

export type IGetLotteryStatusResponse = { nftList: INftItemDto[] };

export type IFindLotteryResponse = IPaginatedResult<ILotteryDto>;

export type ICreateLotteryResponse = ILotteryDto;

export type ICheckLotteryResponse = ILotteryUserDto;

export type IStartLotteryResponse = ILotteryDto;

export type IStopLotteryResponse = ILotteryDto;

export type IUpdateLotteryResponse = ILotteryDto;

export type ICompleteLotteryResponse = ILotteryDto;

export type IRewardLotteryResponse = ILotteryDto;
