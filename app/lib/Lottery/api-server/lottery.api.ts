'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  ICheckLotteryDto,
  ICheckLotteryResponse,
  ICompleteLotteryDto,
  ICompleteLotteryResponse,
  ICreateLotteryDto,
  ICreateLotteryResponse,
  IFindLotteryDto,
  IFindLotteryResponse,
  IGetLotteryDto,
  IGetLotteryResponse,
  IGetLotteryStatusDto,
  IGetLotteryStatusResponse,
  IRewardLotteryDto,
  IRewardLotteryResponse,
  IStartLotteryDto,
  IStartLotteryResponse,
  IStopLotteryDto,
  IStopLotteryResponse,
  IUpdateLotteryDto,
  IUpdateLotteryResponse,
} from 'lib/Lottery/api-server/lottery.types.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class LotteryApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async getLottery({ userAddress, ...params }: IGetLotteryDto): Promise<IGetLotteryResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetLotteryResponse },
      AxiosResponse<{ data: IGetLotteryResponse }>
    >(`/api/v1/lottery/get`, {
      baseURL: this.url,
      params,
    });

    return data.data;
  }

  async getLotteryStatus({
    userAddress,
    ...params
  }: IGetLotteryStatusDto): Promise<IGetLotteryStatusResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetLotteryStatusResponse },
      AxiosResponse<{ data: IGetLotteryStatusResponse }>
    >(`/api/v1/lottery/status`, {
      baseURL: this.url,
      params,
    });

    return data.data;
  }

  async adminFindLottery({
    userAddress,
    ...params
  }: IFindLotteryDto): Promise<IFindLotteryResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindLotteryResponse },
      AxiosResponse<{ data: IFindLotteryResponse }>
    >(`/api/v1/lottery/admin/find`, {
      baseURL: this.url,
      params,
    });

    return data.data;
  }

  async userFindLottery({
    userAddress,
    ...params
  }: IFindLotteryDto): Promise<IFindLotteryResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindLotteryResponse },
      AxiosResponse<{ data: IFindLotteryResponse }>
    >(`/api/v1/lottery/find`, {
      baseURL: this.url,
      params,
    });

    return data.data;
  }

  async createLottery({
    userAddress,
    ...payload
  }: ICreateLotteryDto): Promise<ICreateLotteryResponse> {
    const { data } = await axiosInstance.post<
      { data: ICreateLotteryResponse },
      AxiosResponse<{ data: ICreateLotteryResponse }>
    >(`/api/v1/lottery/create`, payload, {
      baseURL: this.url,
    });

    return data.data;
  }

  async checkLottery({
    userAddress,
    ...payload
  }: ICheckLotteryDto): Promise<ICheckLotteryResponse> {
    const { data } = await axiosInstance.post<
      { data: ICheckLotteryResponse },
      AxiosResponse<{ data: ICheckLotteryResponse }>,
      Omit<ICheckLotteryDto, 'userAddress'>
    >(`/api/v1/lottery/check`, payload, {
      baseURL: this.url,
    });

    return data.data;
  }

  async updateLottery({
    userAddress,
    ...payload
  }: IUpdateLotteryDto): Promise<IUpdateLotteryResponse> {
    const { data } = await axiosInstance.post<
      { data: IUpdateLotteryResponse },
      AxiosResponse<{ data: IUpdateLotteryResponse }>,
      Omit<IUpdateLotteryDto, 'userAddress'>
    >(`/api/v1/lottery/update`, payload, {
      baseURL: this.url,
    });

    return data.data;
  }

  async startLottery({
    userAddress,
    ...payload
  }: IStartLotteryDto): Promise<IStartLotteryResponse> {
    const { data } = await axiosInstance.post<
      { data: IStartLotteryResponse },
      AxiosResponse<{ data: IStartLotteryResponse }>,
      Omit<IStartLotteryDto, 'userAddress'>
    >(`/api/v1/lottery/start`, payload, {
      baseURL: this.url,
    });

    return data.data;
  }

  async stopLottery({ userAddress, ...payload }: IStopLotteryDto): Promise<IStopLotteryResponse> {
    const { data } = await axiosInstance.post<
      { data: IStopLotteryResponse },
      AxiosResponse<{ data: IStopLotteryResponse }>,
      Omit<IStopLotteryDto, 'userAddress'>
    >(`/api/v1/lottery/stop`, payload, {
      baseURL: this.url,
    });

    return data.data;
  }

  async completeLottery({
    userAddress,
    ...payload
  }: ICompleteLotteryDto): Promise<ICompleteLotteryResponse> {
    const { data } = await axiosInstance.post<
      { data: ICompleteLotteryResponse },
      AxiosResponse<{ data: ICompleteLotteryResponse }>,
      Omit<ICompleteLotteryDto, 'userAddress'>
    >(`/api/v1/lottery/complete`, payload, {
      baseURL: this.url,
    });

    return data.data;
  }

  async rewardLottery({
    userAddress,
    ...payload
  }: IRewardLotteryDto): Promise<IRewardLotteryResponse> {
    const { data } = await axiosInstance.post<
      { data: IRewardLotteryResponse },
      AxiosResponse<{ data: IRewardLotteryResponse }>,
      Omit<IRewardLotteryDto, 'userAddress'>
    >(`/api/v1/lottery/reward`, payload, {
      baseURL: this.url,
    });

    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const lotteryApi = new LotteryApi({ url: 'https://api.hellopixel.network' });
