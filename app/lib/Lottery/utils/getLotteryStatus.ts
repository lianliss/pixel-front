import { ILotteryDto } from 'lib/Lottery/api-server/lottery.types.ts';
import { fromUtc } from 'utils/format/date';

export function getLotteryStatus(lottery: ILotteryDto) {
  const started = lottery.startedAt && lottery.startedAt <= Date.now();
  const ended = lottery.endedAt && lottery.endedAt <= Date.now();

  if (ended && !lottery.lotteryUser?.userCompleteAt) {
    return 'failed';
  }
  if (lottery.lotteryUser?.userCompleteAt) {
    return 'success';
  }
  if (started && !ended) {
    return 'active';
  }

  return 'new';
}
