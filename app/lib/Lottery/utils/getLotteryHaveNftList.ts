import { ILotteryCheckDto, LotteryCheckType } from 'lib/Lottery/api-server/lottery.types.ts';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';

export function getLotteryHaveNftList(
  checks: ILotteryCheckDto<LotteryCheckType>[],
  nftList: INftItemDto[]
) {
  const result = checks.filter(
    (el) => el.type === LotteryCheckType.NftCount
  ) as ILotteryCheckDto<LotteryCheckType.NftCount>[];

  return result.map((check) => {
    let checkHaveNft = nftList;

    if (typeof check.context.nftTypeId !== 'undefined') {
      checkHaveNft = checkHaveNft.filter((el) => el.type.id === check.context.nftTypeId);
    }
    if (typeof check.context.rarity !== 'undefined') {
      checkHaveNft = checkHaveNft.filter((el) => el.type.rarity === check.context.rarity);
    }
    if (typeof check.context.slot !== 'undefined') {
      const slot = check.context.slot;
      checkHaveNft = checkHaveNft.filter((el) => el.type.slots.map((el) => el.slot).includes(slot));
    }
    if (typeof check.context.collectionId !== 'undefined') {
      checkHaveNft = checkHaveNft.filter(
        (el) => el.type.collection.id === check.context.collectionId
      );
    }

    return {
      check,
      count: checkHaveNft.length,
      image: checkHaveNft[0]?.type.uri,
    };
  });
}

export function isLotteryChecksCompleted(
  checks: ILotteryCheckDto<LotteryCheckType>[],
  nftList: INftItemDto[]
): boolean {
  const data = getLotteryHaveNftList(checks, nftList);

  return data.every((el) => el.count >= el.check.context.count);
}
