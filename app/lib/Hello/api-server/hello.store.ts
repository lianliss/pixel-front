import { createEffect, createStore } from 'effector';
import { AxiosError } from 'axios';
import {
  ICollectBountyUserRewardDto,
  ICreateBountyUserEntryDto,
  ICreateBountyUserRewardDto,
  IFindBountyEntriesDto,
  IGetCurrentBountyUserResponse,
  IGetBountyUserDto,
  IChangeBountyEntryStatusDto,
  IFindBountyUserEntriesDto,
} from 'lib/Bounty/api-server/bounty.types.ts';
import { bountyApi } from 'lib/Bounty/api-server/bounty.api.ts';
import { addStoreEffect, createStoreState } from 'utils/store/createStoreState.ts';
import { IClaimHelloDto, IGetHelloUserChainDto } from 'lib/Hello/api-server/hello.types.ts';
import { helloApi } from 'lib/Hello/api-server/hello.api.ts';

// claim

export const claimHelloUserChain = createEffect(async (opts: IClaimHelloDto) => {
  const res = await helloApi.claim(opts);
  return res;
});
// get

export const loadHelloUserChain = createEffect(async (opts: IGetHelloUserChainDto) => {
  const res = await helloApi.get(opts);
  return res;
});

export const helloStore = addStoreEffect(
  createStoreState(loadHelloUserChain, { loading: true }),
  claimHelloUserChain
);
