'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  IChangeBountyEntryStatusDto,
  IChangeBountyEntryStatusResponse,
  ICollectBountyUserRewardDto,
  ICollectBountyUserRewardResponse,
  ICreateBountyUserEntryDto,
  ICreateBountyUserEntryResponse,
  ICreateBountyUserRewardDto,
  ICreateBountyUserRewardResponse,
  IFindBountyEntriesDto,
  IFindBountyEntriesResponse,
  IFindBountyUserEntriesDto,
  IFindBountyUserEntriesResponse,
  IGetBountyUserDto,
  IGetBountyUserResponse,
  IGetCurrentBountyUserDto,
  IGetCurrentBountyUserResponse,
} from 'lib/Bounty/api-server/bounty.types.ts';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';
import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';
import { axiosInstance } from 'utils/libs/axios.ts';
import {
  IClaimHelloDto,
  IClaimHelloResponse,
  IGetHelloResponse,
  IGetHelloUserChainDto,
} from 'lib/Hello/api-server/hello.types.ts';

export class HelloApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async get({ chainId, userAddress }: IGetHelloUserChainDto): Promise<IGetHelloResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetHelloResponse },
      AxiosResponse<{ data: IGetHelloResponse }>
    >(`/api/v1/hello/get`, {
      baseURL: this.url,
      params: { chainId, userAddress },
    });

    return data.data;
  }

  async claim({ chainId }: IClaimHelloDto): Promise<IClaimHelloResponse> {
    const { data } = await axiosInstance.post<
      { data: IClaimHelloResponse },
      AxiosResponse<{ data: IClaimHelloResponse }>,
      IClaimHelloDto
    >(
      `/api/v1/hello/claim`,
      { chainId },
      {
        baseURL: this.url,
      }
    );

    return data.data;
  }
}

export const helloApi = new HelloApi({ url: 'https://api.hellopixel.network' });
