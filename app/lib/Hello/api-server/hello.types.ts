// request

export type IGetHelloUserChainDto = { userAddress: string; chainId: number };

export type IClaimHelloDto = {
  chainId: number;
};

// response

export type IGetHelloResponse = IHelloUserChainDto;

export type IClaimHelloResponse = IHelloUserChainDto;

// dto

export type IHelloUserChainDto<TDate = number> = IHelloUserChain<TDate>;

// entity

export type IHelloUserChain<TDate = Date> = {
  id: string;

  userAddress: string;
  chainId: number;

  claimedAt: TDate | null;
  rewardedAt: TDate | null;

  createdAt: TDate;
};
