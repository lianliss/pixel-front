import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react';
import { bountyUserStore, loadBountyUser } from 'lib/Bounty/api-server/bounty.store.ts';
import { useCallback, useEffect } from 'react';
import toaster from 'services/toaster.tsx';
import {
  claimHelloUserChain,
  helloStore,
  loadHelloUserChain,
} from 'lib/Hello/api-server/hello.store.ts';
import { Chain } from 'services/multichain/chains';

const ENABLED_CHAIN = {
  [Chain.UNITS]: true,
};

export function useHello(payload: { userAddress?: string } = {}) {
  const account = useAccount();
  const hello = useUnit(helloStore);
  const enabled = ENABLED_CHAIN[account.chainId];

  const refetch = useCallback(() => {
    if (account.isConnected) {
      loadHelloUserChain({
        chainId: account.chainId,
        userAddress: payload.userAddress || account.accountAddress.toLowerCase(),
      }).catch((err) => toaster.captureException(err));
    } else {
      helloStore.reset();
    }
  }, [account.isConnected, account.accountAddress, payload.userAddress]);

  useEffect(() => {
    if (enabled) {
      refetch();
    }
  }, [refetch, enabled]);

  return {
    data: hello.data,
    error: hello.error,
    loading: hello.loading,
    isFetched: hello.isFetched,
    refetch,
    enabled,
    received: !!hello.data?.claimedAt,
    clear: () => bountyUserStore.reset(),
    claim: () => claimHelloUserChain({ chainId: account.chainId }),
  };
}
