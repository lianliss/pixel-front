import { useUnit } from 'effector-react';
import { findWalletConnections, walletConnectionsStore } from 'lib/Connect/api-server/sdk.store.ts';
import { useEffect } from 'react';
import { useAccount } from '@chain/hooks/useAccount.ts';

export function useWalletConnections() {
  const account = useAccount();

  const state = useUnit(walletConnectionsStore);

  useEffect(() => {
    if (account.isConnected) {
      findWalletConnections({ userAddress: account.accountAddress }).then();
    }
  }, [account.accountAddress, account.isConnected]);

  return {
    data: state.data,
    loading: state.loading,
    error: state.error,
    refetch: () => findWalletConnections({ userAddress: account.accountAddress }),
  };
}
