export type IWalletRequestDto = {
  event: string;
  eventContext: object;
};

export type ICreateWalletDto = { address: string; chainId: number; userSignature: string };

export type IGetWalletRequestDto = { signature: string };

export type IConfirmWalletDto = { address: string; userSignature: string };

export type ICancelWalletDto = { referer: string } & { userAddress: string };

export type IFindWalletsDto = { userAddress: string };

export type IWalletDto = {
  id: string;
  address: string | null;
  userSignature: string;
  referrer: string | null;
};
