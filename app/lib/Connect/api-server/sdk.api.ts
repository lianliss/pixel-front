'use strict';

import axios from 'axios';
import {
  ICancelWalletDto,
  IConfirmWalletDto,
  ICreateWalletDto,
  IFindWalletsDto,
  IGetWalletRequestDto,
  IWalletDto,
  IWalletRequestDto,
} from 'lib/Connect/api-server/sdk.types.ts';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';
import {
  pixelMessageKey,
  pixelSignKey,
  telegramAuthKey,
} from '../../../shared/const/localStorage.ts';
import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';

import { axiosInstance } from 'utils/libs/axios.ts';

export class SdkApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async confirmWallet({ address, userSignature }: IConfirmWalletDto): Promise<void> {
    const { data } = await axiosInstance.post(
      `/api/v2/wallet/connection/confirm`,
      { userSignature },
      { baseURL: this.url }
    );
    return data.data;
  }

  async cancelWalletConnection({ referer, userAddress }: ICancelWalletDto): Promise<void> {
    const { data } = await axiosInstance.post(
      `/api/v2/wallet/connection/cancel`,
      { referer },
      { baseURL: this.url }
    );
    return data.data;
  }

  async getWalletRequest({ signature }: IGetWalletRequestDto): Promise<IWalletRequestDto> {
    const { data } = await axiosInstance.post(
      `/api/v2/wallet/request/get`,
      { signature, userSignature: '' },
      { baseURL: this.url }
    );
    return data.data;
  }

  async findWalletConnections({ userAddress }: IFindWalletsDto): Promise<IWalletDto[]> {
    const { data } = await axiosInstance.get(`/api/v2/wallet/connection/find`, {
      baseURL: this.url,
    });
    return data.data;
  }
}

export const sdkApi = new SdkApi({ url: 'https://api.hellopixel.network' });
