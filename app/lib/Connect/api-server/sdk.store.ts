import { createEffect, createStore } from 'effector';
import {
  ICancelWalletDto,
  IConfirmWalletDto,
  ICreateWalletDto,
  IFindWalletsDto,
  IGetWalletRequestDto,
  IWalletRequestDto,
} from 'lib/Connect/api-server/sdk.types.ts';
import { sdkApi } from 'lib/Connect/api-server/sdk.api.ts';
import { createStoreState } from 'utils/store/createStoreState.ts';

export const createWalletEffect = createEffect(async (params: ICreateWalletDto) => {
  const res = await sdkApi.createWallet(params);

  localStorage.setItem(params.userSignature, params.address);

  return res;
});

export const confirmWalletEffect = createEffect(async (params: IConfirmWalletDto) => {
  const res = await sdkApi.confirmWallet(params);

  // localStorage.setItem(params.userSignature, params.address);

  return res;
});

export const cancelWalletConnection = createEffect(async (params: ICancelWalletDto) => {
  const res = await sdkApi.cancelWalletConnection(params);

  // localStorage.setItem(params.userSignature, params.address);

  return res;
});

export const getWalletRequestEffect = createEffect(async (params: IGetWalletRequestDto) => {
  const res = await sdkApi.getWalletRequest(params);

  return res;
});

export const findWalletConnections = createEffect(async (params: IFindWalletsDto) => {
  const res = await sdkApi.findWalletConnections(params);

  return res;
});

export const walletRequestStore = createStore<IWalletRequestDto | null>(null).on(
  getWalletRequestEffect.doneData,
  (_, next) => next
);

export const walletConnectionsStore = createStoreState(findWalletConnections);
