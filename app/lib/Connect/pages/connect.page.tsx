import React, { useCallback, useEffect, useState } from 'react';
import {
  confirmWalletEffect,
  createWalletEffect,
  getWalletRequestEffect,
  walletRequestStore,
} from 'lib/Connect/api-server/sdk.store.ts';
import { useUnit } from 'effector-react';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import styles from './connect.module.scss';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import bgImage from 'assets/img/digital-soul/bg.png';
import CheckIcon from '@mui/icons-material/Check';
import CircularProgress from '@mui/material/CircularProgress';
import { useTranslation } from 'react-i18next';
import { IS_DEVELOP, IS_STAGE } from '@cfg/config.ts';
import { useParams, useSearchParams } from 'react-router-dom';

const getLocalStorageKey = (key: string) => `pixel-connect-${key}`;

// https://t.me/pixel_wallet_bot/wallet?startapp=674525070

function ConnectPage() {
  const account = useAccount();
  const [params] = useSearchParams();
  const { accountAddress, chainId, isConnected } = account;

  const { t } = useTranslation('connect');

  const loading = useUnit(createWalletEffect.pending);
  const walletRequest = useUnit(walletRequestStore);

  const [connected, setConnected] = useState(false);
  const [error, setError] = useState(false);

  const startAppParam = Telegram.WebApp.initDataUnsafe.start_param || params.get('startapp');

  const isConnect = startAppParam?.startsWith('auth_') || startAppParam?.startsWith('auth2_');
  const isSign = startAppParam?.startsWith('sig_');

  const onConfirm = useCallback(() => {
    if (!walletRequest || !window.pixelWallet?.selectedAddress) {
      return;
    }

    const sign = window.pixelWallet!.sign;
    sign('test').then(console.log);
  }, [walletRequest]);

  useEffect(() => {
    if (isConnected) {
      onConfirm();
    }
  }, [isConnected]);

  const connect = useCallback(() => {
    if (accountAddress && chainId) {
      // connect wallet
      if (startAppParam?.startsWith('auth_')) {
        const userSignature = startAppParam.replace('auth_', '');

        setError(false);

        createWalletEffect({
          userSignature,
          address: accountAddress,
          chainId,
        })
          .then(() => {
            setConnected(true);

            setTimeout(() => {
              Telegram.WebApp.close();
            }, 1000);
          })
          .catch(() => setError(true));
      }
      if (startAppParam?.startsWith('auth2_')) {
        const userSignature = startAppParam.replace('auth2_', '');

        setError(false);

        confirmWalletEffect({
          userSignature,
          address: accountAddress,
        })
          .then(() => {
            setConnected(true);

            setTimeout(() => {
              Telegram.WebApp.close();
            }, 1000);
          })
          .catch(() => setError(true));
      }

      // sign message
      if (startAppParam?.startsWith('sig_')) {
        const signature = startAppParam.replace('sig_', '');

        void getWalletRequestEffect({
          signature,
        });
      }
    }
  }, [chainId, accountAddress]);

  return (
    <div className={styles.root} style={{ backgroundImage: `url(${bgImage})` }}>
      <div className={styles.content}>
        {!startAppParam && <div>{t('not found param')}</div>}

        {!!isSign && walletRequest && (
          <>
            <Button onClick={onConfirm} variant='contained' color='primary' size='large'>
              {t('Confirm')}
            </Button>
          </>
        )}
        {!!isConnect && (
          <>
            <Typography variant='h1' fontWeight='bold' align='center' sx={{ mb: 1 }}>
              {t('Authorization')}
            </Typography>

            <Typography variant='caption' align='center' sx={{ mb: 5 }}>
              {t('Connect your Pixel Wallet to play the game or create a new one')}
            </Typography>

            {!connected && !loading && (
              <Button
                onClick={connect}
                disabled={loading}
                variant='contained'
                color='primary'
                size='large'
                fullWidth
              >
                {t('Connect Pixel Wallet')}
              </Button>
            )}
            {connected && !error && (
              <Typography
                align='center'
                fontWeight='bold'
                variant='h6'
                className={styles.connectedText}
                color='success.main'
              >
                {t('Connected')} <CheckIcon />
              </Typography>
            )}
            {!connected && loading && <CircularProgress />}
            {error && (
              <Typography align='center' fontWeight='bold' variant='h6' color='error.main'>
                {t('Connection failed')}
              </Typography>
            )}
          </>
        )}
      </div>
    </div>
  );
}

export default ConnectPage;
