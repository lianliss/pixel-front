import React from 'react';
import getFinePrice from 'utils/getFinePrice';
import { ProgressBar } from '@ui-kit/ProgressBar/ProgressBar.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Box from '@mui/material/Box';
import { getPercentage } from 'utils/format/number.ts';
import SlotDrone from 'lib/NFT/components/icons/SlotDrone';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';

type IMinerDroneProgressProps = {
  limit?: number;
  value?: number;
  loading?: boolean;
  isBack?: boolean;
};

function MinerDroneProgressUi({
  limit = 0,
  value = 0,
  loading,
  isBack = false,
}: IMinerDroneProgressProps) {
  const minedPercents = getPercentage(value, limit);
  const symbol = 'RESs';

  return (
    <div>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography fontWeight='bold' variant='subtitle2'>
          Raiding
        </Typography>
        <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-end' }}>
          <Typography color='text.secondary' variant='caption' sx={{ fontSize: 11 }}>
            {loading
              ? 'Loading...'
              : limit
              ? isBack
                ? 'The drone is back'
                : 'In progress'
              : 'No Drone'}
          </Typography>
          <Typography color='text.secondary' variant='caption' sx={{ opacity: 0.5, fontSize: 10 }}>
            {getFinePrice(limit)} {symbol}
          </Typography>
        </Box>
      </Box>
      <Box sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
        <Avatar variantStyle='outlined' sx={{ width: 30, height: 30 }}>
          <SlotDrone sx={{ fontSize: 18 }} />
        </Avatar>
        <Box sx={{ width: '100%' }}>
          <ProgressBar
            value={minedPercents}
            size='medium'
            total={100}
            // color={false ? 'success' : 'primary'}
          />
        </Box>
      </Box>
    </div>
  );
}

export default MinerDroneProgressUi;
