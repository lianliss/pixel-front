import React from 'react';
import { useUpdateInterval } from 'utils/hooks/useUpdateInterval';
import MinerDroneProgressUi from 'lib/MinerDrone/components/MinerDroneProgress/MinerDroneProgress.ui.tsx';
import { useUserMinerDrone } from 'lib/MinerDrone/hooks/useMinerDrone.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { MINER_DRONE_CONTRACT_ID } from 'lib/MinerDrone/api-contract/miner-drone.constants.ts';
import { getMinerDroneStatus } from 'lib/MinerDrone/utils/get-miner-drone-status.ts';

type IMinerDroneProgressProps = {};

function MinerDroneProgress({}: IMinerDroneProgressProps) {
  const account = useAccount();

  const minerDroneAddress = MINER_DRONE_CONTRACT_ID[account.chainId];

  const minerDroneData = useUserMinerDrone({
    minerDroneAddress,
    accountAddress: account.accountAddress,
  });
  const minerDrone = minerDroneData.data;

  const status = minerDrone ? getMinerDroneStatus(minerDrone) : null;

  useUpdateInterval(1000);

  return (
    <MinerDroneProgressUi
      limit={(minerDrone?.dustPerHour * minerDrone?.duration) / 1000 / 60 / 60}
      value={status?.amount}
      isBack={status?.isBack}
    />
  );
}

export default MinerDroneProgress;
