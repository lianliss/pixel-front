import React from 'react';
import { Button } from '@ui-kit/Button/Button.tsx';
import { styled, ThemeProvider, useTheme } from '@mui/material/styles';
import { MinerInfo } from 'lib/Mining/components/MinersSlider/components/MinerInfo/MinerInfo.tsx';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import { useMining } from 'lib/Mining/hooks/useMining.ts';
import { useTranslation } from 'react-i18next';
import BuyTokenAlertLazy from 'lib/TelegramStars/components/BuyTokenAlert/BuyTokenAlert.lazy.tsx';
import { IS_PAYABLE, SHOW_NO_GAS_TOKEN_ALERT, SHOW_NO_TRADE_TOKEN_ALERT } from '@cfg/app.ts';
import { Chain } from 'services/multichain/chains';
import MiningRateBlock from 'lib/Mining/components/MiningRateBlock/MiningRateBlock.tsx';
import { MiningStorageToken } from 'lib/Mining/components/MiningStorage/MiningStorageToken/MiningStorageToken.tsx';
import { ProgressBar } from '@ui-kit/ProgressBar/ProgressBar.tsx';
import BuyGasAlertLazy from 'lib/TelegramStars/components/BuyGasAlert/BuyGasAlert.lazy.tsx';
import Divider from '@mui/material/Divider';
import { useAccount } from '@chain/hooks/useAccount.ts';
import SlotDrone from 'lib/NFT/components/icons/SlotDrone';
import { getMiningDroneBtnText } from 'lib/MinerDrone/components/MinerDroneStorage/utils.ts';
import { useMinerDroneApi } from 'lib/MinerDrone/hooks/useMinerDroneApi.ts';
import toaster from 'services/toaster.tsx';
import { useUserMinerDrone } from 'lib/MinerDrone/hooks/useMinerDrone.ts';
import { MINER_DRONE_CONTRACT_ID } from 'lib/MinerDrone/api-contract/miner-drone.constants.ts';
import { MinerDroneCard } from 'lib/MinerDrone/components/MinerDroneCard/MinerDroneCard.tsx';
import { IntervalRender } from '@ui-kit/IntervalRender/IntervalRender.tsx';
import {
  getMinerDronePercent,
  getMinerDroneStatus,
} from 'lib/MinerDrone/utils/get-miner-drone-status.ts';
import { useChainUserSlotsDrone } from 'lib/Inventory/hooks/useChainUserSlotsDrone.ts';

import { getMinerDroneParams } from 'lib/MinerDrone/utils/get-miner-drone-params.ts';
import wait from 'utils/wait';
import { useDustBalance } from 'lib/MinerDrone/hooks/useDustBalance.ts';
import { useDustToken } from 'lib/MinerDrone/hooks/useDustToken.ts';
import { useTransactionConfirmation } from '@chain/hooks/useTransactionConfirmation.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

const StyledBox = styled(Paper)(({ theme }) => ({
  padding: '3px',
  border: 'solid 1px',
  borderColor: theme.palette.primary.main,
}));

type Props = {
  symbol?: string;
  symbolLoading?: boolean;
  icon?: any;
  minerAddress?: any;
  minerType?: any;
  payableAddress?: any;
  theme?: any;
  gasSymbol?: any;
  gasBalanceReload?: any;
  gasBalance?: any;
  gasBalanceLoading?: any;
  minerDisabled?: any;
  noGas?: any;
  noTradeToken?: any;
  soon?: any;

  txCost?: any;
  txCostSymbol?: any;
  txCostBot?: any;
  txCostSlot?: any;
  txCostExtra?: any;
  txCostLoading?: any;
  txCostReload?: any;
  txRate?: any;
  txRateLoading?: any;
  txCount?: any;
  txCountLoading?: any;
  txCostAddress?: any;

  karma?: any;
  access?: any;
};

function MiningDroneStorage({
  symbol,
  symbolLoading,
  icon,
  minerAddress,
  minerType,
  payableAddress,
  theme,
  gasSymbol,
  gasBalanceReload,
  gasBalance = 0,
  gasBalanceLoading = false,
  minerDisabled = false,
  noGas = false,
  noTradeToken = false,
  soon = false,
  txCost,
  txCostSymbol,
  txCostBot,
  txCostSlot,
  txCostExtra,
  txCostLoading = false,
  txCostReload,
  txRate,
  txRateLoading,
  txCount,
  txCountLoading,
  txCostAddress,
  karma,
  access,
}: Props) {
  const { t } = useTranslation('mining');
  const account = useAccount();
  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;

  const currentTheme = useTheme();
  const miningApi = useMining({
    minerAddress,
    payableAddress,
    type: minerType,
    payablePrice: txCost?.value,
    afterClaim: () => {
      txCostReload?.();
      gasBalanceReload?.();
    },
  });

  // ========== DRONE ==========
  const minerDroneAddress = MINER_DRONE_CONTRACT_ID[account.chainId];
  const minerDroneData = useUserMinerDrone({
    minerDroneAddress,
    accountAddress: account.accountAddress,
  });
  const minerDrone = minerDroneData.data;

  const droneApi = useMinerDroneApi();
  const droneSlot = useChainUserSlotsDrone({ telegramId: telegramId });
  const droneSlotParams = droneSlot.data?.itemType
    ? getMinerDroneParams(droneSlot.data.itemType)
    : null;
  const confirmation = useTransactionConfirmation({
    cacheKey: `drone`,
    txHash: droneApi.write.data, // || '0x00d0151f74fb4919a9dc98bfa18dba629a423a08c757a35e6c6ab133e3b44a9a',
  });

  const droneSpeed = droneSlotParams?.reward || 0;
  const droneMissions = minerDrone?.dispatches || droneSlotParams?.missions;
  const droneBroken = droneMissions <= 0;
  // ========== DRONE END ==========

  // ========== DUST ==========
  const dustToken = useDustToken();
  const dustBalance = useDustBalance();

  const dustBalanceAmount = dustBalance.data?.formatted || 0;
  const dustSymbol = dustToken.data?.symbol;
  const dustSymbolLoading = dustToken.isLoading;
  // ========== DUST END ==========

  const { isRefClaiming, isClaiming, isMiningDisabled, isMiningLoading, isMigrating } = miningApi;

  const isBadKarma = karma?.value < 5000;
  const notReady = !isMiningDisabled && (isClaiming || isRefClaiming || isMiningLoading);
  const isStart = !minerDrone?.startedAt && !!droneSlot.data;
  const isProgress = !!minerDrone && minerDrone.endedAt > Date.now();
  const isClaim = !isStart && !!minerDrone && minerDrone.endedAt <= Date.now();

  const isDisabled =
    isClaiming ||
    isRefClaiming ||
    isMiningDisabled ||
    isMigrating ||
    minerDisabled ||
    !access ||
    soon ||
    noGas ||
    noTradeToken ||
    isProgress ||
    confirmation.isPending ||
    droneBroken;
  const loading =
    isClaiming ||
    isMiningLoading ||
    isRefClaiming ||
    isMigrating ||
    (IS_PAYABLE && txCostLoading) ||
    droneApi.write.isPending;
  const miningLoading = isMiningLoading || isRefClaiming || isMigrating;

  const btnText = t(
    getMiningDroneBtnText({
      soon,
      disabled: isMiningDisabled,
      access,
      noGas,
      noTradeToken,
      isStart,
      isClaim,
      isProgress,
      isConfirmationPending: confirmation.isPending,
      confirmations: confirmation.confirmations,
      isBroken: droneBroken,
    })
  );

  const handleStart = async () => {
    try {
      await droneApi.start();
    } catch (e) {
      toaster.captureException(e);
    }
  };
  const handleClaim = async () => {
    try {
      await droneApi.stop();

      await wait(5000);
      await minerDroneData.refetch();
      await dustBalance.refetch();
    } catch (e) {
      toaster.captureException(e);
    }
  };

  return (
    <ThemeProvider theme={theme || currentTheme}>
      <MinerInfo icon={dustToken.data?.image} balance={dustBalanceAmount} />

      <MiningRateBlock
        sx={{ px: 1, mb: 2 }}
        // miner
        minerSymbol={dustSymbol}
        minerSymbolLoading={dustSymbolLoading}
        minerSpeed={droneSpeed}
        minerSpeedLoading={miningApi.loading}
        minerSpeedIcon={<SlotDrone color='primary' />}
        // tx cost
        txCost={txCost?.formatted}
        txCostSymbol={txCostSymbol}
        txCostLoading={txCostLoading}
        txRate={txRate}
        txRateLoading={txRateLoading}
        txCount={txCount}
        txCountLoading={txCountLoading}
        txCostExtra={txCostExtra}
        txCostBot={txCostBot}
        txCostSlot={txCostSlot}
        txCostAddress={txCostAddress}
        // gas
        gasSymbol={gasSymbol}
        gasBalance={gasBalance}
        gasBalanceLoading={gasBalanceLoading}
        // gasless
        gasless={miningApi.gasLess || 0}
        useGasless={miningApi.useGasLess || false}
        onToggleGasless={() => miningApi.updateGasLess(!miningApi.useGasLess)}
        // karma
        karma={karma}
      />

      <StyledBox
        elevation={0}
        // variant='outlined'
        sx={{ position: 'relative' }}
      >
        <Box>
          <IntervalRender
            render={() => {
              const status = minerDrone ? getMinerDroneStatus(minerDrone) : null;

              return (
                <MiningStorageToken
                  loading={miningLoading}
                  icon={<SlotDrone />}
                  title={'Raiding'}
                  amount={status?.amount}
                  maxAmount={droneSlotParams?.totalReward}
                  symbol={symbol}
                  isBadKarma={isBadKarma}
                  status={
                    droneSlot
                      ? !status
                        ? 'Idle'
                        : status?.isBack
                        ? 'The drone is back'
                        : status.time
                      : 'No Drone'
                  }
                />
              );
            }}
          />

          <Divider />

          <Box sx={{ px: 1, pb: 1, mt: 1 }}>
            <IntervalRender
              render={() => {
                const percent = minerDrone ? getMinerDronePercent(minerDrone) : null;

                return (
                  <ProgressBar
                    value={percent || 0}
                    size='medium'
                    total={100}
                    color={isBadKarma ? 'error' : 'primary'}
                  />
                );
              }}
            />

            <Button
              size='large'
              variant='contained'
              color={isBadKarma ? 'error' : 'primary'}
              disabled={isDisabled}
              loading={loading}
              onClick={isClaim ? handleClaim : handleStart}
              fullWidth
              sx={{ mt: 1 }}
            >
              {btnText}
            </Button>

            {typeof droneMissions === 'number' && (
              <Typography
                variant='caption'
                color='textSecondary'
                sx={{ mt: 1 }}
                align='center'
                component='p'
              >
                Available missions {droneMissions - (isStart ? 0 : 1)}
              </Typography>
            )}
          </Box>
        </Box>

        {[Chain.SONGBIRD, Chain.SKALE].includes(account.chainId) &&
          (noGas || SHOW_NO_GAS_TOKEN_ALERT) && (
            <BuyGasAlertLazy sx={{ mt: 2, px: 1, mb: 2 }} symbol={gasSymbol} />
          )}
        {[Chain.SKALE].includes(account.chainId) && (noTradeToken || SHOW_NO_TRADE_TOKEN_ALERT) && (
          <BuyTokenAlertLazy sx={{ mt: 2, px: 1, mb: 2 }} symbol={txCostSymbol} />
        )}
      </StyledBox>
    </ThemeProvider>
  );
}

export default MiningDroneStorage;
