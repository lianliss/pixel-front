export function getMiningDroneBtnText({
  soon,
  isStart,
  access,
  noGas,
  disabled,
  noTradeToken,
  isClaim,
  isProgress,
  isConfirmationPending = false,
  confirmations,
  isBroken = false,
}: {
  soon: boolean;
  noGas: boolean;
  disabled: boolean;
  access: boolean;
  isStart: boolean;
  isProgress: boolean;
  isClaim: boolean;
  noTradeToken?: boolean;
  isConfirmationPending?: boolean;
  isBroken?: boolean;
}) {
  if (soon) {
    return 'Soon';
  }

  if (noGas) {
    return 'No enough Gas';
  }
  if (noTradeToken) {
    return 'No enough Tokens';
  }

  if (disabled) {
    return 'NOT AVAILABLE';
  }

  if (!access) {
    return 'No Access';
  }

  if (isConfirmationPending) {
    return `Wait for Confirmations ${confirmations || ''}`;
  }

  if (isStart) {
    return 'START MINING';
  }
  if (isProgress) {
    return 'In progress';
  }
  if (isBroken) {
    return 'Drone is broken';
  }

  return 'CLAIM RESs';
}
