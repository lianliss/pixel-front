import { PaperProps } from '@mui/material/Paper';
import { MinerDroneCardUi } from 'lib/MinerDrone/components/MinerDroneCard/MinerDroneCard.ui.tsx';
import { useUserMinerDrone } from 'lib/MinerDrone/hooks/useMinerDrone.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { MINER_DRONE_CONTRACT_ID } from 'lib/MinerDrone/api-contract/miner-drone.constants.ts';
import { useChainUserSlotsDrone } from 'lib/Inventory/hooks/useChainUserSlotsDrone.ts';

import { useTransactionConfirmation } from '@chain/hooks/useTransactionConfirmation.ts';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

type Props = PaperProps;

export function MinerDroneCard(props: Props) {
  const { ...other } = props;

  const account = useAccount();
  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;

  const minerDroneAddress = MINER_DRONE_CONTRACT_ID[account.chainId];

  const minerDrone = useUserMinerDrone({
    minerDroneAddress,
    accountAddress: account.accountAddress,
  });
  const drone = minerDrone.data;

  const droneSlot = useChainUserSlotsDrone({ telegramId: telegramId });

  return (
    <MinerDroneCardUi
      {...other}
      symbol='RESs'
      drone={drone}
      droneNft={droneSlot.data?.itemType}
      loading={false}
    />
  );
}
