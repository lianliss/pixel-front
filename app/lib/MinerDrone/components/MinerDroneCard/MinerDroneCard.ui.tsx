import Paper, { PaperProps } from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import SlotDrone from 'lib/NFT/components/icons/SlotDrone';
import Box from '@mui/material/Box';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { IconButton } from '@ui-kit/IconButton/IconButton.tsx';
import ChevronRight from '@mui/icons-material/ChevronRight';
import Add from '@mui/icons-material/Add';
import Skeleton from '@mui/material/Skeleton';
import { IMinerDroneDto } from 'lib/MinerDrone/api-server/miner-drone.types.ts';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { getMinerDroneParams } from 'lib/MinerDrone/utils/get-miner-drone-params.ts';
import { useNavigate } from 'react-router-dom';

const Root = styled(Paper)(() => ({
  padding: '8px',
  display: 'flex',
  gap: 8,
  position: 'relative',
}));

type Props = PaperProps & {
  symbol?: string;
  loading?: boolean;
  drone: IMinerDroneDto | null;
  droneNft: INftItemDto | null;
};

export function MinerDroneCardUi(props: Props) {
  const { drone, droneNft, symbol, loading = false, ...other } = props;

  const navigate = useNavigate();

  const isExists = !loading && !!droneNft;
  const { image, speed, reward } = (droneNft && getMinerDroneParams(droneNft)) || {};
  // const speed = drone?.duration;
  // const reward = drone?.dustPerHour;
  const totalReward = (reward * speed) / 1000 / 60 / 60;
  //   const speed = droneNft?.type.mo
  // const image = droneNft?.type.uri;

  const handleClick = () => navigate('/inventory');

  return (
    <Root variant='outlined' elevation={0} onClick={handleClick} {...other}>
      <Avatar sx={{ width: 80, height: 80 }} variant='rounded' variantStyle='outlined' src={image}>
        {drone ? <SlotDrone sx={{ fontSize: 40 }} /> : <Add sx={{ fontSize: 40 }} />}
      </Avatar>

      <Box sx={{ display: 'flex', flexDirection: 'column', '& > span': { lineHeight: '18px' } }}>
        <Typography variant='subtitle1' color='text.primary' fontWeight='bold'>
          Striking
        </Typography>

        {isExists && (
          <>
            <Typography variant='caption' color='primary.main' sx={{ display: 'flex' }}>
              {loading ? <Skeleton sx={{ width: 30, mr: 1 }} /> : `${reward} ${symbol}`} / Hour
            </Typography>
            <Typography variant='caption' color='text.primary' sx={{ display: 'flex', gap: 0.5 }}>
              <Typography
                component='span'
                variant='inherit'
                color='text.primary'
                sx={{ opacity: 0.5 }}
              >
                Raid Duration:{' '}
              </Typography>
              <Typography
                component='span'
                variant='inherit'
                color='primary.main'
                sx={{ display: 'flex' }}
              >
                {loading ? <Skeleton sx={{ width: 30, mr: 1 }} /> : speed / 1000 / 60 / 60}h
              </Typography>
            </Typography>
            <Typography variant='caption' color='text.primary' sx={{ display: 'flex', gap: 0.5 }}>
              <Typography
                component='span'
                variant='inherit'
                color='text.primary'
                sx={{ opacity: 0.5 }}
              >
                Raid Reward:{' '}
              </Typography>
              <Typography
                component='span'
                variant='inherit'
                color='primary.main'
                sx={{ display: 'flex' }}
              >
                {loading ? <Skeleton sx={{ width: 30, mr: 1 }} /> : `${totalReward} ${symbol}`}
              </Typography>
            </Typography>
          </>
        )}
        {!isExists && (
          <>
            <Typography component='span' variant='inherit' color='primary.main'>
              You have no Drone
            </Typography>
            <Typography component='span' variant='inherit' color='primary.main'>
              Put the Drone to the Slot
            </Typography>
          </>
        )}
      </Box>

      <IconButton
        sx={{
          position: 'absolute',
          right: 0,
          top: '50%',
          transform: 'translateY(-50%)',
          opacity: 0.5,
        }}
      >
        <ChevronRight />
      </IconButton>
    </Root>
  );
}
