import { MINER_DUST_CONTRACT_ID } from 'lib/MinerDrone/api-contract/miner-drone.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useToken } from '@chain/hooks/useToken.ts';

export function useDustToken() {
  const account = useAccount();

  const dustAddress = MINER_DUST_CONTRACT_ID[account.chainId];

  const token = useToken({
    address: dustAddress,
    skip: !dustAddress,
  });

  return token;
}
