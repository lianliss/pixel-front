import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { MINER_DRONE_CONTRACT_ID } from 'lib/MinerDrone/api-contract/miner-drone.constants.ts';
import { MINER_DRONE_ABI } from 'lib/MinerDrone/api-contract/miner-drone.abi.ts';

export function useMinerDroneApi() {
  const account = useAccount();

  const minerDroneAddress = MINER_DRONE_CONTRACT_ID[account.chainId];

  const write = useWriteContract({
    address: minerDroneAddress,
    abi: MINER_DRONE_ABI,
  });

  const start = () => {
    return write.writeContractAsync({
      functionName: 'dispatchDrone',
    });
  };

  const stop = () => {
    return write.writeContractAsync({
      functionName: 'returnDroneAndClaimReward',
    });
  };

  return {
    write,
    start,
    stop,
  };
}
