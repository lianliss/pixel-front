import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { MINER_DRONE_ABI } from 'lib/MinerDrone/api-contract/miner-drone.abi.ts';
import wei from 'utils/wei';

export function useMiningUpgradeDronePrice({
  minerDroneAddress,
  userAddress,
}: {
  minerDroneAddress?: string;
  userAddress?: string;
}) {
  const state = useReadContract({
    address: minerDroneAddress,
    abi: MINER_DRONE_ABI,
    functionName: 'calculateMineUpgradePrice',
    args: [userAddress],
    skip: !minerDroneAddress || !userAddress,
    select: (res) => +wei.from(res),
  });

  return state;
}
