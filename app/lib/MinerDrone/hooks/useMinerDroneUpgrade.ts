import { MINER_DRONE_ABI } from 'lib/MinerDrone/api-contract/miner-drone.abi.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';

export function useMinerDroneUpgrade({ minerDroneAddress }: { minerDroneAddress?: string }) {
  const write = useWriteContract({
    address: minerDroneAddress,
    abi: MINER_DRONE_ABI,
  });
  const upgrade = () =>
    write.writeContractAsync({
      functionName: 'upgradeMine',
      args: [],
    });

  return {
    write,
    upgrade,
  };
}
