import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { MINER_DRONE_CONTRACT_ID } from 'lib/MinerDrone/api-contract/miner-drone.constants.ts';
import { MINER_DRONE_ABI } from 'lib/MinerDrone/api-contract/miner-drone.abi.ts';

export function useMinerDroneStorage() {
  const account = useAccount();

  const minerDroneAddress = MINER_DRONE_CONTRACT_ID[account.chainId];

  const speed = useReadContract({
    address: minerDroneAddress,
    abi: MINER_DRONE_ABI,
    functionName: 'calculateMiningSpeed',
    args: [account.accountAddress],
    skip: !account.isConnected,
  });

  return {
    data: 0,
    loading: speed.loading,
  };
}
