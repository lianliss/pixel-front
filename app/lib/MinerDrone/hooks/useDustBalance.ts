import { useAccount } from '@chain/hooks/useAccount.ts';
import { MINER_DUST_CONTRACT_ID } from 'lib/MinerDrone/api-contract/miner-drone.constants.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';

export function useDustBalance({ skip }: { skip?: boolean } = {}) {
  const account = useAccount();
  const address = MINER_DUST_CONTRACT_ID[account.chainId];

  return useBalance({
    address: account.address,
    token: address,
    skip: skip || !address || !account.address,
  });
}
