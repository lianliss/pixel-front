import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { MINER_DRONE_ABI } from 'lib/MinerDrone/api-contract/miner-drone.abi.ts';
import wei from 'utils/wei';

export function useMiningDroneLevel({
  minerDroneAddress,
  userAddress,
}: {
  minerDroneAddress?: string;
  userAddress?: string;
}) {
  const state = useReadContract({
    address: minerDroneAddress,
    abi: MINER_DRONE_ABI,
    functionName: 'mineLevel',
    args: [userAddress],
    skip: !minerDroneAddress || !userAddress,
    select: (res) => Number(res),
  });

  return state;
}
