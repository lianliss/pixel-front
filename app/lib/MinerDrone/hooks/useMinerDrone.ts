import { useCallback, useEffect, useMemo } from 'react';
import { useUnit } from 'effector-react';
import {
  loadUserMinerDrone,
  userMinerDroneStore,
} from 'lib/MinerDrone/api-server/miner-drone.store.ts';
import { getMockMinerDrone } from 'lib/MinerDrone/utils/mock.ts';

export function useUserMinerDrone({
  minerDroneAddress,
  accountAddress,
}: {
  minerDroneAddress: string;
  accountAddress?: string;
}) {
  const minerDrone = useUnit(userMinerDroneStore);

  const reloadMinerDrone = useCallback(() => {
    loadUserMinerDrone({
      userAddress: accountAddress?.toLowerCase(),
      minerDroneAddress: minerDroneAddress,
    }).then();
  }, [accountAddress, minerDroneAddress]);

  useEffect(() => {
    if (accountAddress) {
      reloadMinerDrone();
    }
  }, [reloadMinerDrone]);

  const mock = useMemo(() => getMockMinerDrone(), []);

  return {
    data: minerDrone.data, // || mock,
    loading: minerDrone.loading,
    refetch: reloadMinerDrone,
    isFetched: minerDrone.isFetched,
  };
}
