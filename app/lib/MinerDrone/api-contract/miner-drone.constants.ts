import { Chain } from 'services/multichain/chains';

export const MINER_DRONE_CONTRACT_ID = {
  [Chain.UNITS]: '0x454042025143552cf61ff11af37d6817f3c3d2d9'.toLowerCase(),
};

export const MINER_DUST_CONTRACT_ID = {
  [Chain.UNITS]: '0x7FfA8a0307fCa33fe57EAF7bd4d734e12F6989b7'.toLowerCase(),
};
