'use strict';

import axios, { AxiosResponse } from 'axios';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';

import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';
import {
  pixelMessageKey,
  pixelSignKey,
  telegramAuthKey,
} from '../../../shared/const/localStorage.ts';
import {
  IGetMinerDroneDto,
  IGetMinerDroneResponse,
} from 'lib/MinerDrone/api-server/miner-drone.types.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class MinerDroneApi {
  url: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async user({
    userAddress,
    minerDroneAddress,
  }: IGetMinerDroneDto): Promise<IGetMinerDroneResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetMinerDroneResponse },
      AxiosResponse<{ data: IGetMinerDroneResponse }>,
      IGetMinerDroneDto,
      {}
    >(`/api/v1/miner/drone/user`, {
      baseURL: this.url,
      params: {
        userAddress: userAddress.toLowerCase(),
        minerDroneAddress: minerDroneAddress.toLowerCase(),
      },
    });
    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const minerDroneApi = new MinerDroneApi({ url: 'https://api.hellopixel.network' });
