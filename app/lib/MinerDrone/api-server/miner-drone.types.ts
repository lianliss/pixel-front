// request

export type IGetMinerDroneDto = { userAddress: string; minerDroneAddress: string };

// response

export type IGetMinerDroneResponse = IMinerDroneDto;

// entity

export type IMinerDrone<TDate = Date> = {
  id: string;

  userAddress: string;
  minerDroneAddress: string;

  dustPerHour: number;
  duration: number;
  dispatches: number;

  startedAt: TDate | null;
  endedAt: TDate | null;
  claimedAt: TDate | null;
  createdAt: TDate;
};

// dto

export type IMinerDroneDto<TDate = number> = IMinerDrone<TDate>;
