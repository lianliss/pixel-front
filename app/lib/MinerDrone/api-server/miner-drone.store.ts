import { createEffect } from 'effector';
import { IGetMinerDroneDto } from 'lib/MinerDrone/api-server/miner-drone.types.ts';
import { minerDroneApi } from 'lib/MinerDrone/api-server/miner-drone.api.ts';
import { createStoreState } from 'utils/store/createStoreState.ts';

export const loadUserMinerDrone = createEffect(async (params: IGetMinerDroneDto) => {
  const res = await minerDroneApi.user(params);

  return res;
});

export const userMinerDroneStore = createStoreState(loadUserMinerDrone);
