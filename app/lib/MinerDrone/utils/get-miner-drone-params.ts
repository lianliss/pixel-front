import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import wei from 'utils/wei';

export function getMinerDroneParams(droneNft: INftItemDto) {
  const image = droneNft?.type.uri;
  const durationMod = droneNft?.type.mods.find((el) => el.modId === '22');
  const rewardMod = droneNft?.type.mods.find((el) => el.modId === '21');
  const missionsMod = droneNft?.type.mods.find((el) => el.modId === '23');
  const speed = durationMod ? Number(durationMod?.add) * 1000 : 0;
  const reward = rewardMod ? wei.from(rewardMod?.add) : 0;
  const totalReward = reward * (speed / 1000 / 60 / 60);
  const missions = missionsMod ? Number(missionsMod.add) : 0;

  return {
    image,
    speed,
    reward,
    totalReward,
    missions,
  };
}
