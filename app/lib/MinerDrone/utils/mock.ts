import { IMinerDroneDto } from 'lib/MinerDrone/api-server/miner-drone.types.ts';

export function getMockMinerDrone(): IMinerDroneDto {
  return {
    id: '',
    endedAt: new Date().getTime() + 1000 * 60 * 5,
    startedAt: new Date().getTime(),
    minerDroneAddress: '',
    claimedAt: new Date().getTime(),
    userAddress: '',
    dustPerHour: 1000,
    createdAt: new Date().getTime(),
    duration: 1000 * 60 * 60 * 2,
  };
}
