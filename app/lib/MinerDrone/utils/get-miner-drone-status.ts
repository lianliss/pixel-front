import { IMinerDroneDto } from 'lib/MinerDrone/api-server/miner-drone.types.ts';
import { getPercentage } from 'utils/format/number.ts';
import { diffTime, getTimeProgress, timeToDate } from 'utils/format/date';

export function getMinerDronePercent(drone: IMinerDroneDto) {
  const now = Date.now();
  const value = now - drone.startedAt;
  const percent = Math.min(getPercentage(value, drone.endedAt - drone.startedAt), 100);

  return percent;
}

export function getMinerDroneStatus(drone: IMinerDroneDto) {
  const now = Date.now();
  const value = now - drone.startedAt;
  const totalValue = (drone.dustPerHour * +drone.duration) / 1000 / 60 / 60;
  const percent = Math.min(getPercentage(value, drone.endedAt - drone.startedAt), 100);

  return {
    percent,
    isBack: drone.endedAt <= Date.now(),
    time: getTimeProgress(now, drone.endedAt),
    maxAmount: totalValue,
    amount: +((totalValue * percent) / 100).toFixed(4),
  };
}
