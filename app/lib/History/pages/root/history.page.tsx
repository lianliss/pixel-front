import { useUnit } from 'effector-react';
import { historyStore, loadHistory } from 'lib/History/api-server/history.store.ts';
import classes from './history.module.scss';
import React, { useEffect, useState } from 'react';
import { useAccount } from '@chain/hooks/useAccount.ts';
import toaster from 'services/toaster.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import styles from 'lib/Marketplace/components/MarketUserHistory/MarketUserHistory.module.scss';
import Pagination from '@mui/material/Pagination';
import { useTranslation } from 'react-i18next';
import HistoryGrid from 'lib/History/components/HistoryGrid/HistoryGrid.tsx';
import { useCoreToken } from '@chain/hooks/useCoreToken.ts';
import { useCoreBalance } from '@chain/hooks/useCoreBalance.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import BalanceSwiper from 'lib/History/components/BalanceSwiper/BalanceSwiper.tsx';
import { getDefaultChainImage } from '@cfg/image.ts';
import Divider from '@mui/material/Divider';
import Container from '@mui/material/Container';

const PAGE_ROWS = 10;

const HistoryPage = () => {
  const account = useAccount();
  // const accountAddress = '0x4B9Ac70305761c8f1c4e88AD6C54499b15674a07';

  const { t } = useTranslation('history');
  const { t: n } = useTranslation('notifications');

  const coreToken = useCoreToken();
  const tradeToken = useTradeToken();
  const coreBalance = useCoreBalance();

  const { state: history, loading, error } = useUnit(historyStore);
  const [loaded, setLoaded] = useState(false);
  const [page, setPage] = React.useState<number>(1);

  const pagesCount = Math.ceil(history?.count / PAGE_ROWS) || 0;

  useEffect(() => {
    if (!account.isConnected) {
      return;
    }

    loadHistory({
      userAddress: account.accountAddress.toLowerCase(),
      limit: PAGE_ROWS,
      offset: (page - 1) * PAGE_ROWS,
      chainId: account.chainId,
    })
      .then(() => setLoaded(true))
      .catch((e) => {
        (toaster as any).captureException(e, n('History loading failed'));
      });
  }, [account.accountAddress, account.isConnected, account.chainId, page]);

  return (
    <div
      className={classes.root}
      style={{ backgroundImage: `url(${getDefaultChainImage(account.chainId)})` }}
    >
      <Container maxWidth='xs'>
        <ChainSwitcher />

        <Typography variant='h2' fontWeight='bold' align='center' sx={{ mt: 2, mb: 2.5 }}>
          {t('Account History')}
        </Typography>

        <div style={{ padding: '0 16px', marginBottom: 16 }}>
          <BalanceSwiper />
        </div>

        <div className={classes.content}>
          {history?.count > 0 && (
            <>
              <Divider />
              <HistoryGrid
                rows={history.rows}
                coreSymbol={coreToken.data?.symbol}
                tradeSymbol={tradeToken?.symbol}
              />
              <Divider />
            </>
          )}
          {history?.count === 0 && loaded && !loading && (
            <Typography align='center'>{t('No data')}</Typography>
          )}
          {error && !loading && <Typography align='center'>{t('Loading failed')}</Typography>}
          {loading && history?.count === 0 && (
            <Typography align='center'>{t('Loading...')}</Typography>
          )}
        </div>

        {history?.count > PAGE_ROWS && (
          <div className={styles.pagination}>
            <Pagination
              count={pagesCount}
              color='primary'
              variant='outlined'
              shape='rounded'
              size='large'
              siblingCount={1}
              boundaryCount={0}
              page={page}
              onChange={(_, p) => setPage(p)}
            />
          </div>
        )}
      </Container>
    </div>
  );
};

export default HistoryPage;
