'use strict';
import React from 'react';
import { alpha, styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Skeleton from '@mui/material/Skeleton';
import getFinePrice from 'utils/getFinePrice';

const Root = styled(Paper)(({ theme }) => ({
  padding: 16,
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  backgroundColor: alpha('#A62CFF', 0.1),
}));
const Content = styled('div')(() => ({
  display: 'flex',
  gap: 8,

  '& > div': {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
}));

type Props = {
  image: string;
  symbol: string;
  name: string;
  amount: number;
  loading?: boolean;
  loadingBalance?: boolean;
};

function BalanceCard(props: Props) {
  const { image, symbol, name, amount, loading = false, loadingBalance = false } = props;

  return (
    <Root variant='outlined'>
      <Content>
        <Avatar
          color='primary'
          variant='rounded'
          variantStyle='outlined'
          src={image}
          sx={{ width: 56, height: 56, p: 1, '& > img': { objectFit: 'contain' } }}
        />
        <div>
          <Typography
            color='text.primary'
            fontWeight='bold'
            sx={{ fontSize: 18, lineHeight: '21px', minWidth: 50 }}
          >
            {loading ? <Skeleton /> : symbol}
          </Typography>
          <Typography color='text.primary' variant='caption' sx={{ opacity: 0.5, minWidth: 70 }}>
            {loading ? <Skeleton /> : name}
          </Typography>
        </div>
      </Content>

      <Typography color='text.primary' variant='h6' fontWeight='bold' sx={{ minWidth: 70 }}>
        {loadingBalance ? <Skeleton /> : getFinePrice(amount)}
      </Typography>
    </Root>
  );
}

export default BalanceCard;
