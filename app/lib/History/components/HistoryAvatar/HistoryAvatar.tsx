'use strict';
import React from 'react';
import { HistoryType } from 'lib/History/api-server/history.types.ts';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import { isHistoryRed } from 'lib/History/utils/utils.ts';
import { getHistoryIcon } from 'lib/History/utils/icons.ts';

type Props = { type: HistoryType };

function HistoryAvatar(props: Props) {
  const { type } = props;

  const Icon = getHistoryIcon(type);
  const isRed = isHistoryRed(type);

  return (
    <Avatar
      variant='rounded'
      variantStyle='outlined'
      color={isRed ? 'error' : 'success'}
      sx={{
        width: 40,
        height: 40,
      }}
    >
      <Icon sx={{ fontSize: 28 }} />
    </Avatar>
  );
}

export default HistoryAvatar;
