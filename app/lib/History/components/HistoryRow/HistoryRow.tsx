'use strict';
import React, { useMemo } from 'react';
import classes from './HistoryRow.module.scss';
import Paper from '@mui/material/Paper';
import { IHistoryDto } from 'lib/History/api-server/history.types.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { timeToDate } from 'utils/format/date';
import { IntervalRender } from '@ui-kit/IntervalRender/IntervalRender.tsx';
import { useTranslation } from 'react-i18next';
import HistoryAvatar from 'lib/History/components/HistoryAvatar/HistoryAvatar.tsx';
import { getHistoryTextInfo, isHistoryRed } from 'lib/History/utils/utils.ts';
import { openLink } from 'utils/open-link.ts';
import { createTxUrl } from '@chain/utils/url.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';

type Props = { data: IHistoryDto; coreSymbol?: string; tradeSymbol?: string };

function HistoryRow(props: Props) {
  const { data, coreSymbol, tradeSymbol } = props;

  const { t } = useTranslation('history');
  const account = useAccount();

  const textInfo = getHistoryTextInfo(data, { coreSymbol, tradeSymbol });
  const isRed = isHistoryRed(data.type);
  const isGreatDate = new Date().getTime() - data.createdAt > 1000 * 60 * 60 * 24;

  const txUrl = useMemo(() => {
    return null;
    // return data.txHash ? createTxUrl(account, data.txHash) : null;
  }, [account.chainId, data.txHash]);

  return (
    <Paper className={classes.root} elevation={0} sx={{ borderRadius: 0 }}>
      <HistoryAvatar type={data.type} />

      <div className={classes.content}>
        <Typography variant='subtitle2' color='text.primary'>
          {t(data.type)}
        </Typography>
        <Typography variant='caption' color='text.secondary'>
          {t(data.type + ' action')}
        </Typography>
        <Typography variant='caption' sx={{ lineHeight: '12px', fontSize: 10, opacity: 0.5 }}>
          {isGreatDate ? (
            <>
              {new Date(data.createdAt).toLocaleDateString('ru-RU')} |{' '}
              {new Date(data.createdAt).toLocaleTimeString('ru-RU').slice(0, -3)}
            </>
          ) : (
            <IntervalRender render={() => timeToDate(new Date(data.createdAt))} />
          )}
        </Typography>
      </div>

      <div className={classes.right}>
        {textInfo && (
          <Typography
            variant='subtitle1'
            color={isRed ? 'error.main' : 'success.main'}
            fontWeight='bold'
          >
            {textInfo}
          </Typography>
        )}
        {data.txHash && txUrl && (
          <Typography
            variant='caption'
            color='text.secondary'
            sx={{ fontSize: 10, lintHeight: 12, textDecoration: 'underline' }}
            onClick={() => {
              openLink(txUrl);
            }}
          >
            {data.txHash.slice(0, 5) + '..' + data.txHash.slice(-3)}
          </Typography>
        )}
      </div>
    </Paper>
  );
}

export default HistoryRow;
