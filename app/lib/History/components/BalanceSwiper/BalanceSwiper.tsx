import Swiper, { SwiperProps } from '@ui-kit/Swiper/Swiper.tsx';
import BalanceCard from 'lib/History/components/BalanceCard/BalanceCard.tsx';
import React from 'react';
import { useCoreToken } from '@chain/hooks/useCoreToken.ts';
import { useCoreBalance } from '@chain/hooks/useCoreBalance.ts';
import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useToken } from '@chain/hooks/useToken.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';

function RenderCard(props: { address: string }) {
  const account = useAccount();
  const token = useToken({
    address: props.address,
  });
  const balance = useBalance({
    address: account.accountAddress,
    skip: !account.isConnected,
    token: props.address,
  });

  return (
    <BalanceCard
      loading={token.isLoading}
      loadingBalance={balance.isLoading}
      symbol={token.data?.symbol}
      image={token.data?.image}
      amount={balance.data?.formatted}
      name={token.data?.name}
    />
  );
}

function BalanceSwiper(props: Omit<SwiperProps<string>, 'options' | 'renderOption'>) {
  const account = useAccount();

  const miners = MINER_CONTRACT_ID[account.chainId];

  if (!miners?.core) {
    return null;
  }

  return (
    <Swiper<string>
      options={[miners.core, ...(miners.second || []).filter((el) => el.startsWith('0x'))]}
      renderOption={(option) => <RenderCard address={option} />}
      {...props}
    />
  );
}

export default BalanceSwiper;
