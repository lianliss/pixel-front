'use strict';
import React from 'react';
import { HistoryType, IHistoryContext, IHistoryDto } from 'lib/History/api-server/history.types.ts';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import classes from 'lib/History/pages/root/history.module.scss';
import HistoryRow from 'lib/History/components/HistoryRow/HistoryRow.tsx';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Stack from '@mui/material/Stack';

type Props = { rows: IHistoryDto[]; coreSymbol?: string; tradeSymbol?: string };

function HistoryGrid(props: Props) {
  const { rows = [], coreSymbol, tradeSymbol } = props;

  return (
    <Stack direction='column' divider={<Divider />}>
      {rows.map((row) => (
        <HistoryRow
          data={row}
          key={`history-${row.id}`}
          coreSymbol={coreSymbol}
          tradeSymbol={tradeSymbol}
        />
      ))}
    </Stack>
  );
}

export default HistoryGrid;
