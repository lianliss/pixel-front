import { createEffect, createStore } from 'effector';
import { IFindHistoryDto, IHistoriesDto } from 'lib/History/api-server/history.types.ts';
import { historyApi } from 'lib/History/api-server/history.api.ts';
import { AxiosError } from 'axios';

// leaderboard

export const loadHistory = createEffect(async (params: IFindHistoryDto) => {
  const res = await historyApi.find({ ...params });
  return res;
});

export const historyStore = createStore({
  loading: true,
  error: null as Error | null,
  state: null as IHistoriesDto | null,
})
  .on(loadHistory.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadHistory.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadHistory.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));
