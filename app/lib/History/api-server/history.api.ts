'use strict';

import axios, { AxiosResponse } from 'axios';
import { IFindHistoryDto, IFindHistoryResponse } from 'lib/History/api-server/history.types.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class HistoryApi {
  url: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async find(params: IFindHistoryDto): Promise<IFindHistoryResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindHistoryResponse },
      AxiosResponse<{ data: IFindHistoryResponse }>
    >(`/api/v1/history/user`, { baseURL: this.url, params });

    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const historyApi = new HistoryApi({ url: 'https://api.hellopixel.network' });
