import { IPaginatedResult } from 'lib/Quests/api-server/quests.types.ts';
import { IPaginatedQuery, LeaderboardType } from 'lib/Leaderboard/api-server/leaderboard.types.ts';
import {
  AchievementPeriod,
  AchievementType,
} from 'lib/Achievement/api-server/achievement.types.ts';
import { NftItemRarity } from 'lib/NFT/api-server/nft.types.ts';

export enum HistoryType {
  NftReceived = 'NftReceived',
  ClaimMiner = 'ClaimMiner',
  // miner
  MinerSpeedUp = 'MinerSpeedUp',
  MinerStorageUp = 'MinerStorageUp',
  MinerRefStorageUp = 'MinerRefStorageUp',
  // leaderboard
  LeaderboardReward = 'LeaderboardReward',
  // achievement
  AchievementRang = 'AchievementRang',
  // quest
  QuestCompleted = 'QuestCompleted',
  // market
  MarketSellNft = 'MarketSellNft',
  MarketBuyNft = 'MarketBuyNft',
}

export type IHistoryContext = {
  // [HistoryType.NftReceived]: { nftItemId: string; nft?: IHistoryNft };
  [HistoryType.ClaimMiner]: { amount: number; contract: string };
  [HistoryType.MinerBurned]: { amount: number; contract: string };
  [HistoryType.MinerSpeedUp]: { level: number; contract: string };
  [HistoryType.MinerStorageUp]: { level: number; contract: string };
  [HistoryType.MinerRefStorageUp]: { level: number; contract: string };
  // leaderboard
  [HistoryType.LeaderboardReward]: {
    amount: number;
    season: number;
    position: number;
    type: LeaderboardType;
  };
  // achievement
  [HistoryType.AchievementRang]: {
    rang: number;
  };
  // quest
  [HistoryType.QuestCompleted]: {
    questId: number;
  };
  // market
  [HistoryType.MarketSellNft]: {
    rarity: NftItemRarity;
    price: number;
  };
  [HistoryType.MarketBuyNft]: {
    rarity: NftItemRarity;
    price: number;
  };
};

export type IHistory<T extends HistoryType = HistoryType, TDate = Date> = {
  id: string;

  userAddress: string;

  type: T;
  context: IHistoryContext[T];
  txHash?: string;

  createdAt: TDate;
};

export type IHistoryDto<TDate = number> = Omit<IHistory<HistoryType, TDate>, 'userAddress'>;

export type IHistoriesDto<TDate = number> = IPaginatedResult<IHistoryDto<TDate>>;

//

export type IFindHistoryDto = IPaginatedQuery & { userAddress: string; chainId?: number };
export type IFindHistoryResponse = IHistoriesDto;
