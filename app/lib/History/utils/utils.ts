import { HistoryType, IHistoryContext, IHistoryDto } from 'lib/History/api-server/history.types.ts';

export function getHistoryTextInfo(
  data: IHistoryDto,
  { coreSymbol, tradeSymbol }: { coreSymbol?: string; tradeSymbol?: string }
): string {
  switch (data.type) {
    case HistoryType.AchievementRang:
      return (data.context as IHistoryContext[HistoryType.AchievementRang]).rang + ' lvl';
    case HistoryType.MinerSpeedUp:
      return (data.context as IHistoryContext[HistoryType.MinerSpeedUp]).level + ' lvl';
    case HistoryType.MinerStorageUp:
      return (data.context as IHistoryContext[HistoryType.MinerStorageUp]).level + ' lvl';
    case HistoryType.MinerRefStorageUp:
      return (data.context as IHistoryContext[HistoryType.MinerRefStorageUp]).level + ' lvl';
    case HistoryType.MarketBuyNft:
      return (
        '-' +
        (data.context as IHistoryContext[HistoryType.MarketBuyNft]).price.toString() +
        ` ${tradeSymbol || ''}`
      );
    case HistoryType.MarketSellNft:
      return (
        '+' +
        (data.context as IHistoryContext[HistoryType.MarketSellNft]).price.toString() +
        ` ${tradeSymbol || ''}`
      );
    case HistoryType.LeaderboardReward:
      return (
        '+' +
        (data.context as IHistoryContext[HistoryType.LeaderboardReward]).amount +
        ` ${coreSymbol || ''}`
      );
  }
}

const isRed = {
  [HistoryType.MinerStorageUp]: true,
  [HistoryType.MinerSpeedUp]: true,
  [HistoryType.MinerRefStorageUp]: true,
  [HistoryType.MarketBuyNft]: true,
};

export function isHistoryRed(type: HistoryType): boolean {
  return !!isRed[type];
}
