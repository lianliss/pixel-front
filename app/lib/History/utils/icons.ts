import WhatshotIcon from '@mui/icons-material/Whatshot';
import { HistoryType } from 'lib/History/api-server/history.types.ts';
import { SvgIconProps } from '@mui/material/SvgIcon';
import React from 'react';
import BuildStorageIcon from '../../../shared/icons/BuildStorage';
import BuildDrillIcon from '../../../shared/icons/BuildDrill';
import BuildReferralStorageIcon from '../../../shared/icons/BuildReferralStorage';
import SellNftIcon from '../../../shared/icons/SellNft';
import BuyNftIcon from '../../../shared/icons/BuyNft';

const defaultIcon = WhatshotIcon;
const icons = {
  [HistoryType.MinerStorageUp]: BuildStorageIcon,
  [HistoryType.MinerSpeedUp]: BuildDrillIcon,
  [HistoryType.MinerRefStorageUp]: BuildReferralStorageIcon,
  [HistoryType.MarketSellNft]: SellNftIcon,
  [HistoryType.MarketBuyNft]: BuyNftIcon,
};

export function getHistoryIcon(type: HistoryType): React.FC<SvgIconProps> {
  return icons[type] || defaultIcon;
}
