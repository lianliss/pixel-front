import { Chain } from 'services/multichain/chains';

export const REFERRAL_MINER_PERCENT = {
  [Chain.SWISSTEST]: 5,
  [Chain.SONGBIRD]: 20,
  [Chain.SKALE]: 10,
  [Chain.SKALE_TEST]: 10,
  [Chain.COSTON2]: 20,
};

export const REFERRAL_SUB_MINER_PERCENT = {
  [Chain.SWISSTEST]: 0,
  [Chain.SONGBIRD]: 5,
  [Chain.SKALE]: 0,
  [Chain.SKALE_TEST]: 0,
  [Chain.COSTON2]: 0,
};
