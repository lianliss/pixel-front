import { useUnit } from 'effector-react';
import {
  loadUserReferralsCount,
  loadUserReferralsRewards,
  loadUserTopReferrals,
  userReferralsCountStore,
  userReferralsRewardsStore,
  userTopReferralsStore,
} from 'lib/Mining/api-server/miner.store.ts';
import { useEffect } from 'react';
import { Chain } from 'services/multichain/chains';
import { useAccount } from '@chain/hooks/useAccount.ts';

const MAP_CONTRACTS = {
  [Chain.SONGBIRD]: 'sgb-pixel',
  [Chain.SWISSTEST]: 'swtr-testnet-pixel',
  [Chain.SKALE]: 'skale-pixel',
  [Chain.SKALE_TEST]: 'skale-testnet-pixel',
  [Chain.FLARE]: 'flare-pixel',
  [Chain.COSTON2]: 'flare-testnet-pixel',
};

export function useFriendsCount() {
  const account = useAccount();
  const accountAddress = account.accountAddress?.toLowerCase();

  const referralsCountState = useUnit(userReferralsCountStore);

  useEffect(() => {
    const contractKey = MAP_CONTRACTS[account.chainId];

    if (account.isConnected && contractKey) {
      loadUserReferralsCount({
        address: accountAddress,
        contract: contractKey,
      }).then();
    }
  }, [account.isConnected, accountAddress, account.chainId]);

  return referralsCountState;
}

export function useFriends() {
  const account = useAccount();
  const accountAddress = account.accountAddress?.toLowerCase();

  const referralsCountState = useFriendsCount();
  const referralsRewardsState = useUnit(userReferralsRewardsStore);
  const topReferralsState = useUnit(userTopReferralsStore);

  useEffect(() => {
    const contractKey = MAP_CONTRACTS[account.chainId];

    if (account.isConnected && contractKey) {
      loadUserReferralsRewards({
        address: accountAddress,
        contract: contractKey,
      }).then();
      loadUserTopReferrals({
        address: accountAddress,
        contract: contractKey,
      }).then();
    }
  }, [account.isConnected, accountAddress, account.chainId]);

  return {
    count: referralsCountState,
    rewards: referralsRewardsState,
    top: topReferralsState,
  };
}
