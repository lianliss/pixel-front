import React, { useEffect } from 'react';
import styles from './Friends.module.scss';

import getFinePrice from 'utils/getFinePrice';
import toaster from 'services/toaster.tsx';
import copy from 'copy-to-clipboard';
import TelegramIcon from '@mui/icons-material/Telegram';

import { Chain } from 'services/multichain/chains';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useCoreToken } from '@chain/hooks/useCoreToken.ts';
import Paper from '@mui/material/Paper';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import CircularProgress from '@mui/material/CircularProgress';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';
import { useNavigate } from 'react-router-dom';
import { IconButton } from '@ui-kit/IconButton/IconButton.tsx';
import { useFriends } from 'lib/Friends/hooks/useFriends.ts';
import userImage from 'assets/img/lion-avatar.png';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import { displayAddress } from 'utils/format/chain.ts';
import { timeToDate } from 'utils/format/date';
import { REFERRAL_MINER_PERCENT, REFERRAL_SUB_MINER_PERCENT } from 'lib/Friends/utils/constants.ts';
import { getDefaultChainImage } from '@cfg/image.ts';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';

const getName = (row) => {
  if (row.name) {
    if (row.name.length > 13) {
      return row.name.slice(0, 6) + '...' + row.name.slice(-4);
    }

    return row.name;
  }

  return row.address.slice(0, 6) + '...' + row.address.slice(-4);
};

function Friends() {
  const account = useAccount();
  const userInfo = useSelfUserInfo();

  const navigate = useNavigate();
  const { chainId } = account;

  const referralPercent = REFERRAL_MINER_PERCENT[account.chainId];
  const referralSubPercent = REFERRAL_SUB_MINER_PERCENT[account.chainId];

  const {
    top: topReferralsState,
    rewards: referralsRewardsState,
    count: countReferralsState,
  } = useFriends();
  const { lastWeekActive = 0, total: count = 0 } = countReferralsState.state;

  const coreToken = useCoreToken();
  const coreSymbol = coreToken.data?.symbol;

  const copyLink = async () => {
    Telegram.WebApp.HapticFeedback.impactOccurred('heavy');
    copy(`https://t.me/pixel_wallet_bot/wallet?startapp=${userInfo.data?.telegramId}`);
    Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    toaster.warning('Your referral link copied to clipboard');
  };

  useBackAction('/wallet/mining');

  return (
    <div
      className={styles.friends}
      style={{ backgroundImage: `url(${getDefaultChainImage(account.chainId)})` }}
    >
      <ChainSwitcher chains={[Chain.SONGBIRD, Chain.SKALE, Chain.UNITS]} />

      <Container maxWidth='xs'>
        <div className={styles.friendsHeader}>
          <div className={styles.friendsHeaderAmount}>
            <Typography color='text.primary' variant='subtitle2'>
              {count} friend{count !== 1 && 's'}
            </Typography>
            <Typography color='text.primary' variant='subtitle2'>
              {lastWeekActive} active friend{lastWeekActive !== 1 && 's'}
            </Typography>
          </div>

          {countReferralsState.state?.parentAddress && (
            <div className={styles.friendsHeaderParent}>
              <Typography
                color='text.primary'
                variant='caption'
                style={{ textDecoration: 'underline' }}
                onClick={() => navigate(`/profile/${countReferralsState.state.parentAddress}`)}
              >
                Parent: {displayAddress(countReferralsState.state.parentAddress)}
              </Typography>
              {countReferralsState.state.parentAssignedAt && (
                <Typography color='text.primary' variant='caption'>
                  Assigned: {timeToDate(new Date(countReferralsState.state.parentAssignedAt))}
                </Typography>
              )}
            </div>
          )}

          {!!referralPercent && (
            <p>
              You will receive {referralPercent}% of the {coreSymbol}
              <br />
              mined by your friend
              {!!referralSubPercent && (
                <>
                  &nbsp;and {referralSubPercent}% of the
                  <br />
                  {coreSymbol} mined by your friend's friend
                </>
              )}
            </p>
          )}

          <Box sx={{ display: 'flex', gap: 2, mt: 1 }}>
            <Button
              variant='outlined'
              color='primary'
              component='a'
              href={'https://docs.hellopixel.network/hello-pixel/pixel-wallet/refer-a-friend'}
              target='_blank'
            >
              Full guide
            </Button>

            <Button variant='outlined' color='primary' component='a' onClick={copyLink}>
              Copy Invite Link
            </Button>
          </Box>

          <div style={{ display: 'flex', justifyContent: 'center', width: '100%', gap: 24 }}>
            <p>
              <b>Received total</b>
              <br />
              <div className={styles.fiendsStats}>
                <>
                  <span>{getFinePrice(referralsRewardsState.state?.total || 0)}</span> from friends
                </>
              </div>
            </p>

            <p>
              <b>Received last 24h</b>
              <br />
              <div className={styles.fiendsStats}>
                <>
                  <span>{getFinePrice(referralsRewardsState.state?.lastDay || 0)}</span> from
                  friends
                </>
              </div>
            </p>
          </div>

          {count >= 500 && (
            <>
              <Button
                variant='outlined'
                color='primary'
                component='a'
                href='https://t.me/+sBF6kdz0CWU5ZGEy'
                target='_blank'
              >
                🐋 Join to Whale room
              </Button>
            </>
          )}
        </div>
        <div className={styles.friendsTitle}>
          <Typography color='text.primary' fontWeight='bold' variant='subtitle2' align='center'>
            My Top friends
          </Typography>
        </div>

        <Paper
          sx={{ p: '16px 16px', pr: 1 }}
          variant='outlined'
          elevation={0}
          className={styles.friendsBlock}
        >
          {topReferralsState.state.map((el, index) => {
            const userName = getName(el);
            const redirectToProfile = () => {
              // backAction.createReturn();
              navigate(`/profile/${el.address}`);
            };

            return (
              <div className={styles.friendsEvent} key={index}>
                <div className={styles.friendsEventData}>
                  <div className={styles.friendsEventDataIcon} onClick={redirectToProfile}>
                    <img src={el.image || (userImage as string)} alt={''} />
                  </div>
                  <div className={styles.friendsEventDataText}>
                    <Typography
                      color='text.primary'
                      className={styles.friendsEventDataAction}
                      onClick={redirectToProfile}
                      style={{ textDecoration: 'underline' }}
                    >
                      {userName}
                    </Typography>
                  </div>
                </div>

                <div style={{ display: 'flex', alignItems: 'center', gap: 6 }}>
                  <div className={styles.amount}>
                    <div>
                      {getFinePrice(el.amount)} {coreSymbol}
                    </div>
                  </div>

                  <IconButton
                    component='a'
                    href={`https://t.me/${el.name}`}
                    disabled={!el.name}
                    size='small'
                    variant='outlined'
                    color='primary'
                    sx={{ p: '3px' }}
                  >
                    <TelegramIcon sx={{ fontSize: 16 }} />
                  </IconButton>
                </div>
              </div>
            );
          })}
          {topReferralsState.loaded &&
            !topReferralsState.state.length &&
            !topReferralsState.loading && (
              <Typography align='center' variant='subtitle1'>
                No active referrals found
                <br /> An active referral is a friend who has made one or more storage claim in the
                last week.
              </Typography>
            )}
          {topReferralsState.loading && !topReferralsState.loaded && (
            <CircularProgress sx={{ margin: '30px auto 30px auto' }} />
          )}
        </Paper>
      </Container>
    </div>
  );
}

export default Friends;
