import pixelImage from 'assets/img/digital-soul/pixel.png';
import buddhaImage from 'assets/img/digital-soul/buddha.png';
import pixelWarsImage from 'assets/img/digital-soul/pixel-wars.png';
import { IDigitalSoulProject } from 'lib/DigitalSoul/components/DigitalSoulProject/DigitalSoulProject.tsx';
import { openLink } from 'utils/open-link.ts';
import { IS_DEVELOP, IS_STAGE } from '@cfg/config.ts';

export const DIGITAL_SOUL_PROJECTS: IDigitalSoulProject[] = [
  {
    title: 'PIXEL Miner',
    info: 'Mine. Improve. Collect.',
    image: pixelImage as string,
    status: 'connected',
  },
  {
    title: 'PixelWars',
    info: 'Play. Craft. Survive.',
    image: pixelWarsImage as string,
    status: 'ready',
    referer: 'https://game.hellopixel.network/',
    onPlay: () => {
      openLink('https://t.me/PixelWarsGame_bot/play');
    },
  },
  {
    title: 'JADE BUDDHA',
    info: 'Create compounds to get JADE',
    image: buddhaImage as string,
    status: IS_DEVELOP || IS_STAGE ? 'ready' : 'soon',
    color: 'success',
    referer: 'https://beta.cryptojade.online/',
    onPlay: () => {
      openLink('https://t.me/cryptojade_dev_bot');
    },
  },
];
