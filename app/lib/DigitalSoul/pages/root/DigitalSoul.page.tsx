'use strict';
import React from 'react';
import styles from './DigitalSoul.module.scss';
import DigitalSoulProject, {
  IDigitalSoulProject,
} from 'lib/DigitalSoul/components/DigitalSoulProject/DigitalSoulProject.tsx';
import { DIGITAL_SOUL_PROJECTS } from 'lib/DigitalSoul/pages/root/constants.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import DigitalSoulProfile from 'lib/DigitalSoul/components/DigitalSoulProfile/DigitalSoulProfile.tsx';
import bgImage from 'assets/img/digital-soul/bg.png';
import { useTranslation } from 'react-i18next';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import { getDefaultChainImage } from '@cfg/image.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useWalletConnections } from 'lib/Connect/hooks/useWalletConnections.ts';
import { cancelWalletConnection } from 'lib/Connect/api-server/sdk.store.ts';
import toaster from 'services/toaster.tsx';
import { IWalletDto } from 'lib/Connect/api-server/sdk.types.ts';
import Container from '@mui/material/Container';

function DigitalSoulPage() {
  const { t } = useTranslation('digitalsoul');
  const account = useAccount();

  const connections = useWalletConnections();
  console.log(connections);

  const handleCancel = async (referer: string) => {
    try {
      await cancelWalletConnection({ referer, userAddress: account.accountAddress });
      await connections.refetch();
    } catch (e) {
      toaster.captureException(e);
    }
  };

  const map = connections.data?.reduce((acc, el) => ({ ...acc, [el.referrer]: el }), {}) || {};
  const arr: Array<{ project: IDigitalSoulProject; connection: IWalletDto | null }> =
    DIGITAL_SOUL_PROJECTS.map((el) => {
      return {
        project: el,
        connection: el.referer ? map[el.referer] : null,
      };
    });

  useBackAction('/wallet');

  return (
    <div
      className={styles.root}
      style={{
        backgroundImage: `url(${getDefaultChainImage(account.chainId)})`,
        backgroundSize: 'cover',
      }}
    >
      <Container maxWidth='xs'>
        <Typography
          fontWeight='bold'
          variant='h2'
          align='center'
          color='text.primary'
          className={styles.title}
        >
          {t('Digital Soul')}
        </Typography>

        <DigitalSoulProfile style={{ marginBottom: 8 }} />

        <Typography
          fontWeight='bold'
          align='center'
          color='text.primary'
          className={styles.projectsTitle}
        >
          {t('Connected Multiverses')}
        </Typography>

        <div className={styles.list}>
          {arr.map((el) => (
            <DigitalSoulProject
              key={`project-${el.project.title}`}
              data={el.project}
              connection={el.connection}
              onCancel={handleCancel}
            />
          ))}
        </div>
      </Container>
    </div>
  );
}

export default DigitalSoulPage;
