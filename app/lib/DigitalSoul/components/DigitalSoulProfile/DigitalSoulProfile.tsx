'use strict';
import React from 'react';
import styles from './DigitalSoulProfile.module.scss';
import Divider from '@mui/material/Divider';
import Skeleton from '@mui/material/Skeleton';
import clsx from 'clsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useTranslation } from 'react-i18next';
import Paper from '@mui/material/Paper';
import { darken, styled } from '@mui/material/styles';

const StyledRoot = styled(Paper)(({ theme }) => ({
  '& .MuiSkeleton-root': {
    backgroundColor: darken(theme.palette.background.paper, 0.2),
  },
}));

type Props = React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

function DigitalSoulProfile(props: Props) {
  const { className, ...other } = props;
  const { t } = useTranslation('common');

  return (
    <StyledRoot variant='outlined' className={clsx(styles.root, className)} {...other}>
      <div className={styles.toolbar}>
        <Skeleton variant='text' sx={{ fontSize: '1rem', width: '50%' }} animation={false} />
        <Skeleton variant='text' sx={{ fontSize: '1rem', width: '30%' }} animation={false} />
      </div>

      <Divider />

      <div className={styles.content}>
        <div className={styles.content1}>
          <Skeleton
            variant='circular'
            width={144}
            height={144}
            animation={false}
            // sx={{ border: 'solid 1px #081F3A' }}
          />

          <Paper variant='outlined' className={styles.info}>
            <Skeleton variant='text' sx={{ fontSize: '0.8rem', width: '70%' }} animation={false} />
            <Skeleton variant='text' sx={{ fontSize: '0.8rem', width: '70%' }} animation={false} />
            <Skeleton variant='text' sx={{ fontSize: '0.8rem', width: '50%' }} animation={false} />
            <Skeleton variant='text' sx={{ fontSize: '0.8rem', width: '30%' }} animation={false} />
          </Paper>
        </div>

        <Typography variant='h3' fontWeight='bold' align='center' className={styles.title}>
          {t('Coming soon')}
        </Typography>
      </div>
    </StyledRoot>
  );
}

export default DigitalSoulProfile;
