'use strict';
import React from 'react';
import styles from './DigitalSoulProject.module.scss';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Paper from '@mui/material/Paper';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useTranslation } from 'react-i18next';
import { IWalletDto } from 'lib/Connect/api-server/sdk.types.ts';

export type IDigitalSoulProject = {
  title: string;
  info: string;
  image: string;
  color?: string;
  status?: 'connected' | 'soon' | 'ready';
  onPlay?: () => void;
  disabled?: boolean;
  referer?: string;
};

type Props = {
  data: IDigitalSoulProject;
  connection?: IWalletDto | null;
  onCancel?: (referer: string) => void;
};

function DigitalSoulProject(props: Props) {
  const {
    data: { title, status, onPlay, image, info, color, disabled = false },
    connection,
    onCancel,
  } = props;
  const { t } = useTranslation('digitalsoul');

  return (
    <Paper className={styles.root} elevation={0}>
      <div className={styles.content}>
        <Avatar
          color={color}
          variant='rounded'
          variantStyle='filled'
          src={image}
          className={styles.avatar}
        />

        <div className={styles.info}>
          <Typography variant='h4' fontWeight='bold' color='text.primary'>
            {t(title)}
          </Typography>
          <Typography variant='sutitle2' color='text.secondary' className={styles.infoText}>
            {t(info)}
          </Typography>
        </div>

        <div className={styles.right}>
          {!connection && status === 'connected' && (
            <Typography variant='h4' color='success.main' fontWeight='bold'>
              {t('Connected')}
            </Typography>
          )}
          {!connection && (status === 'soon' || disabled) && (
            <Typography variant='h4' fontWeight='bold' color='text.secondary'>
              {t('SOON')}
            </Typography>
          )}
          {status === 'ready' && !disabled && (
            <Button
              variant='contained'
              color='primary'
              onClick={onPlay}
              disabled={!onPlay || disabled}
              fullWidth
            >
              {t('Play')}
            </Button>
          )}
        </div>
      </div>

      {connection && !disabled && (
        <Button
          variant='contained'
          color='primary'
          onClick={() => onCancel?.(connection.referrer)}
          disabled={!onCancel || disabled}
          fullWidth
        >
          {t('Disconnect')}
        </Button>
      )}
    </Paper>
  );
}

export default DigitalSoulProject;
