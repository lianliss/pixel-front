import { useUnit } from 'effector-react';
import {
  loadMarketOrdersOwned,
  marketOrdersOwnedStore,
} from 'lib/Marketplace/api-server/market.store.ts';
import { usePagination } from 'utils/hooks/usePagination.ts';
import { AxiosError } from 'axios';
import toaster from 'services/toaster.tsx';
import React from 'react';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useInterval } from 'utils/hooks/useInterval';

function useOwnedMarketOrders({
  nftAddress,
  marketAddress,
  search,
  skip,
}: {
  nftAddress: string;
  marketAddress: string;
  search?: string;
  skip?: boolean;
}) {
  const account = useAccount();
  const accountAddress = account.accountAddress;

  const ordersOwned = useUnit(marketOrdersOwnedStore);
  const pagination = usePagination({ total: ordersOwned.count, step: 100 });

  const load = () => {
    if (!marketAddress || !nftAddress || !accountAddress) {
      return;
    }
    loadMarketOrdersOwned({
      address: marketAddress,
      nftAddress: nftAddress,
      seller: accountAddress,
      limit: pagination.limit,
      offset: pagination.offset,
      search,
    })
      .then()
      .catch((e) => {
        const err = (e as AxiosError).response.data.error?.message;
        toaster.show({ message: err || 'Failed to load orders', intent: 'danger' });
      });
  };

  React.useEffect(() => {
    if (account.isConnected) {
      load();
    }
  }, [
    accountAddress,
    search,
    account.chainId,
    nftAddress,
    marketAddress,
    pagination.limit,
    pagination.offset,
  ]);

  useInterval(
    () => {
      if (!skip) {
        load();
      }
    },
    30_000,
    [accountAddress, search, account.chainId, nftAddress, marketAddress]
  );

  return {
    data: ordersOwned,
    pagination,
    load,
  };
}

export default useOwnedMarketOrders;
