import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { wei } from 'utils/index';
import wait from 'utils/wait';
import toaster from 'services/toaster.tsx';
import React from 'react';
import { INftContext } from 'lib/NFT/context/Nft.context.ts';
import { IMarketplaceContext } from 'lib/Marketplace/context/Marketplace.context.ts';
import { IReadContractState } from '@chain/hooks/useReadContract.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import NFT_ABI from 'lib/NFT/api-contract/nft.abi.ts';
import MARKET_ABI from 'lib/Marketplace/api-contract/market.abi.ts';
import { calcPublicationFeeWei } from 'lib/Marketplace/utils/price.ts';

const TRANSACTION_DELAY = 8000;
const ERC_20_ABI = require('const/ABI/Erc20Token');

export function useMarketplaceSell({
  nft,
  marketplace,
  onCompleted,
  allowanceState,
}: {
  nft: INftContext;
  marketplace: IMarketplaceContext;
  onCompleted?: () => Promise<void>;
  allowanceState: IReadContractState;
}) {
  const [sellPrice, setSellPrice] = React.useState<number | null>(null);
  const [sellingNft, setSellingNft] = React.useState<INftItemDto | null>(null);
  const [loading, setLoading] = React.useState<boolean>(false);
  const [success, setSuccess] = React.useState<INftItemDto | null>(null);

  const sellingDisabled = !nft.address || !marketplace.address;

  const allowanceUpdate = useWriteContract({
    abi: ERC_20_ABI,
    address: marketplace.tradeToken.address,
  });
  const nftApprove = useWriteContract({
    abi: NFT_ABI,
    address: nft.address,
  });
  const marketSell = useWriteContract({
    abi: MARKET_ABI,
    address: marketplace.wrapAddress,
  });

  const handleSell = async (nftItem: INftItemDto, price: number) => {
    if (sellingDisabled) {
      setSellPrice(null);
      setSellingNft(null);

      return;
    }

    setLoading(true);

    const publicationFeeWei = calcPublicationFeeWei({
      price,
      publicationFee: marketplace.publicationFee,
      decimals: marketplace.tradeToken.decimals,
    });

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      if (
        !marketplace.tradeToken.isGas &&
        allowanceState.data < wei.from(publicationFeeWei, marketplace.tradeToken.decimals)
      ) {
        console.log(`[buy] dont have allowance ${allowanceState.data}`);

        await allowanceUpdate.writeContractAsync({
          functionName: 'approve',
          args: [marketplace.wrapAddress, publicationFeeWei],
        });
      }

      await nftApprove.writeContractAsync({
        functionName: 'approve',
        args: [marketplace.address, nftItem.assetId],
      });

      await marketSell.writeContractAsync({
        functionName: 'createOrder',
        args: [
          nft.address,
          nftItem.assetId,
          wei.to(price, marketplace.tradeToken.decimals),
          new Date().getTime() + 60 * 60 * 24 * 365 * 5,
        ],
        overrides: { value: marketplace.tradeToken.isGas ? publicationFeeWei : undefined },
      });

      // await marketplace.api.sell({
      //   nftAddress: nft.address,
      //   assetId: nftItem.assetId,
      //   priceInWei: +wei.to(price, marketplace.tradeToken.decimals),
      //   expiredAt: new Date().getTime() + 60 * 60 * 24 * 365,
      //   publicationFee: publicationFeeWei,
      // });

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');

      await wait(TRANSACTION_DELAY);

      if (onCompleted) {
        await onCompleted();
      }

      setSellPrice(null);
      setSellingNft(null);
      setSuccess(nftItem);
    } catch (e) {
      console.error(e);
      toaster.captureException(e);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
      setSellPrice(null);
    } finally {
      setLoading(false);
    }
  };

  const handleSellConfirm = (_nftItem: INftItemDto, price: number) => {
    setSellPrice(price);
    Telegram.WebApp.HapticFeedback.impactOccurred('light');
  };

  return {
    loading,
    sellingNft,
    price: sellPrice,
    success,
    handleSelectItem: (nftItem: INftItemDto) => {
      setSellingNft(nftItem);
      Telegram.WebApp.HapticFeedback.impactOccurred('light');
    },
    handleSell,
    handleSellConfirm,
    handleCloseSell: () => {
      setSellingNft(null);
    },
    handleCloseSellConfirm: () => setSellPrice(null),
    handleCloseSuccessSell: () => {
      setSellPrice(null);
      setSellingNft(null);
      setSuccess(null);
    },
  };
}
