import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { wei } from 'utils/index';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { MARKET_WRAP_CONTRACTS } from 'lib/Marketplace/api-contract/contracts.ts';
import { MARKET_WRAP_ABI } from 'lib/Marketplace/api-contract/market-wrap.abi.ts';

import { useSelfUserInfo } from 'lib/User/hooks/useUserInfo.ts';

export function useMarketUserMod() {
  const userInfo = useSelfUserInfo();
  const telegramId = userInfo.data?.telegramId;
  const account = useAccount();
  const marketAddress = MARKET_WRAP_CONTRACTS[account.chainId];

  const result = useReadContract<number>({
    initData: 0,
    abi: MARKET_WRAP_ABI,
    address: marketAddress,
    functionName: 'extraMod',
    args: [telegramId],
    skip: !marketAddress || !telegramId,
    select: (bn) => {
      return +wei.from(bn, 4) || 1;
    },
  });

  return result;
}
