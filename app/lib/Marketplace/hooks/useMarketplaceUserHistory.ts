import React from 'react';
import {
  loadMarketUserOrdersHistory,
  marketUserOrdersHistoryStore,
} from 'lib/Marketplace/api-server/market.store.ts';
import { useUnit } from 'effector-react';

export function useMarketplaceUserHistory({
  marketAddress,
  nftAddress,
  accountAddress,
}: {
  nftAddress: string;
  marketAddress: string;
  accountAddress: string;
}) {
  const historyOrders = useUnit(marketUserOrdersHistoryStore);
  const loading = useUnit(loadMarketUserOrdersHistory.pending);
  const [historyPage, setHistoryPage] = React.useState<number>(1);

  const callLoadUserHistory = React.useCallback(() => {
    void loadMarketUserOrdersHistory({
      nftAddress: nftAddress,
      limit: 20,
      offset: (historyPage - 1) * 20,
      address: marketAddress,
      userAddress: accountAddress.toLowerCase(),
    });
  }, [accountAddress, nftAddress, marketAddress, historyPage]);

  return {
    load: callLoadUserHistory,
    changePage: setHistoryPage,
    page: historyPage,
    data: historyOrders,
    loading,
  };
}
