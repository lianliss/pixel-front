import React, { useEffect } from 'react';
import { useUnit } from 'effector-react';
import { loadNftList, nftListStore } from 'lib/NFT/api-server/nft.store.ts';
import { IFindNftItemsDto, NftItemRarity, NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';

const STEP = 16;

export function useOwnedNfts({
  nftAddress,
  accountAddress,
  excludeSoulbound,
  limit = STEP,
  rarities,
  slots,
  mods,
}: {
  nftAddress: string;
  accountAddress: string;
  excludeSoulbound?: boolean;
  limit: number;
  rarities?: NftItemRarity[];
  slots?: NftItemSlot[];
  mods?: string[];
}) {
  const nftList = useUnit(nftListStore);
  const loading = useUnit(loadNftList.pending);
  const [page, setPage] = React.useState<number>(1);

  const load = React.useCallback(async () => {
    if (accountAddress) {
      await loadNftList({
        limit: limit,
        offset: (page - 1) * limit,
        contractId: nftAddress,
        holder: accountAddress,
        soulbound: excludeSoulbound === true ? false : undefined,
        rarities,
        slots,
        mods,
      });
    }
  }, [accountAddress, nftAddress, page, excludeSoulbound, rarities, slots, mods]);

  return {
    load,
    changePage: setPage,
    page,
    data: nftList,
    loading,
  };
}
