import React, { useCallback, useEffect } from 'react';
import { TechnicalWorks } from 'ui/TechnicalWorks/TechnicalWorks.tsx';

import styles from './Marketplace.module.scss';
import { useAb } from 'lib/ab/context/ab.context.ts';
import { useMarketplace } from 'lib/Marketplace/context/Marketplace.context.ts';
import { useNft } from 'lib/NFT/context/Nft.context.ts';
import { useUnit } from 'effector-react';
import {
  loadMarketOrderGroups,
  loadMarketOrders,
  loadMarketOrdersHistory,
  loadMarketOrdersOwned,
  marketOrderGroupsStore,
  marketOrdersHistoryStore,
  marketOrdersOwnedStore,
  marketOrdersStore,
  updateMarketFavoriteGroup,
} from 'lib/Marketplace/api-server/market.store.ts';
import {
  IMarketOrderDto,
  IMarketOrderGroupDto,
  Order,
} from 'lib/Marketplace/api-server/market.types.ts';
import { IMarketFilterState } from 'lib/Marketplace/components/MarketFilterDialog/MarketFilterDialog.tsx';
import { STATUS_LIST } from 'lib/Marketplace/components/MarketToolbar/data.ts';
import toaster from 'services/toaster.tsx';
import { wei } from 'utils/index';
import wait from 'utils/wait';
import { useInterval } from 'utils/hooks/useInterval';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import BalanceToolbar from '../../components//BalanceToolbar/BalanceToolbar.tsx';
import MarketToolbar from '../../components//MarketToolbar/MarketToolbar.tsx';
import { Tabs } from '@ui-kit/Tabs/Tabs.tsx';
import { Tab } from '@ui-kit/Tab/Tab.tsx';
import MarketGrid from '../../components//MarketGrid/MarketGrid.tsx';
import MarketGridOwned from '../../components//MarketGridOwned/MarketGridOwned.tsx';
import { Loading } from 'utils/async/load-module';
import MarketSuccessSellDialog from 'lib/Marketplace/components/MarketSuccessSellDialog/MarketSuccessSellDialog.tsx';
import MarketSellConfirmDialog from 'lib/Marketplace/components/MarketSellConfirmDialog/MarketSellConfirmDialog.tsx';
import MarketSellDialog from 'lib/Marketplace/components/MarketSellDialog/MarketSellDialog.tsx';
import MarketOwnedItemsDialog from 'lib/Marketplace/components/MarketOwnedItemsDialog/MarketOwnedItemsDialog.tsx';
import MarketOfferDialog from 'lib/Marketplace/components/MarketOfferDialog/MarketOfferDialog.tsx';
import MarketCancelConfirmDialog from 'lib/Marketplace/components/MarketCancelConfirmDialog/MarketCancelConfirmDialog.tsx';
import MarketSuccessBuyDialog from 'lib/Marketplace/components/MarketSuccessBuyDialog/MarketSuccessBuyDialog.tsx';
import MarketBuyDialog from 'lib/Marketplace/components/MarketBuyDialog/MarketBuyDialog.tsx';
import MarketOrderDialog from 'lib/Marketplace/components/MarketOrderDialog/MarketOrderDialog.tsx';
import { AxiosError } from 'axios';
import LinearProgress from '@mui/material/LinearProgress';
import { useMarketplaceSell } from 'lib/Marketplace/hooks/useMarketplaceSell.ts';
import NotAvailableChain from '../../../../shared/blocks/NotAvailableChain/NotAvailableChain.tsx';
import MarketIcon from 'lib/Marketplace/components/icons/MarketIcon';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import { Chain } from 'services/multichain/chains';
import { useNavigate } from 'react-router-dom';
import { useAccount } from '@chain/hooks/useAccount.ts';
import MarketUserHistory from 'lib/Marketplace/components/MarketUserHistory/MarketUserHistory.tsx';
import { useMarketplaceUserHistory } from 'lib/Marketplace/hooks/useMarketplaceUserHistory.ts';
import { useOwnedNfts } from 'lib/Marketplace/hooks/useOwnedNfts.ts';
import MarketOrderPreviewDialog from 'lib/Marketplace/components/MarketOrderPreviewDialog/MarketOrderPreviewDialog.tsx';
import { getMarketRootStyles } from 'lib/Marketplace/pages/root/utils.ts';
import Paper from '@mui/material/Paper';
import { darken, styled } from '@mui/material/styles';
import { useBalance } from '@chain/hooks/useBalance.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import MARKET_ABI from 'lib/Marketplace/api-contract/market.abi.ts';
import { useSumNftStats } from 'lib/Inventory/components/StatsDialog/useSumNftStats.ts';
import { useGasBalance } from '@chain/hooks/useGasBalance.ts';
import ERC_20_ABI from 'lib/Wallet/api-contract/abi/erc-20.abi.ts';
import { useTranslation } from 'react-i18next';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import useOwnedMarketOrders from 'lib/Marketplace/hooks/useOwnedMarketOrders.ts';
import marketAnalytics from 'lib/Marketplace/api-analytics';
import { useMarketUserMod } from 'lib/Marketplace/hooks/useMarketUserMod.ts';
import Alert from '@mui/material/Alert';
import { MarketBadKarmaDialog } from 'lib/Marketplace/components/MarketBadKarmaDialog/MarketBadKarmaDialog.tsx';
import { getMarketUserSellFee } from 'lib/Marketplace/utils/get-market-user-sell-fee.ts';
import Container from '@mui/material/Container';
import { getDefaultChainImage } from '@cfg/image.ts';
import Divider from '@mui/material/Divider';
import { getMarketRecommendedPrice } from 'lib/Marketplace/utils/get-market-recommended-price.ts';
import { decodeErrorMessage } from 'utils/decodeErrorData.jsx';

const AVAILABLE_CHAINS = [Chain.SONGBIRD, Chain.SKALE, Chain.COSTON2, Chain.SKALE_TEST];

const StyledPaper = styled(Paper)(({ theme }) => ({
  // backgroundColor: darken(theme.palette.background.paper, 0.3),
}));

const TRANSACTION_DELAY = 8000;

enum TabEnum {
  general = 'general',
  owned = 'owned',
  history = 'history',
}

const MarketplacePage = () => {
  const ab = useAb();
  const marketplace = useMarketplace();
  const nft = useNft();
  const nftAddress = nft.address;
  const account = useAccount();
  const { chainId, accountAddress } = account;

  const balance = useBalance({
    address: accountAddress,
    token: marketplace.tradeToken.address,
    skip: !marketplace.tradeToken.address,
  });

  const { t } = useTranslation('marketplace');

  const gasBalance = useGasBalance({
    address: account.accountAddress,
    skip: !account.isConnected,
  });

  const nftStats = useSumNftStats({ autoLoad: true });

  // const tradeToken = useTradeToken();
  const navigate = useNavigate();

  // stores
  const orderGroups = useUnit(marketOrderGroupsStore);
  const ordersHistory = useUnit(marketOrdersHistoryStore);
  const orders = useUnit(marketOrdersStore);
  // TODO make 2 stores, for marketOrderGroup (owned items), for owned tab
  // const ordersOwned = useUnit(marketOrdersOwnedStore);
  const loadingGroups = useUnit(loadMarketOrderGroups.pending);
  const loadingOwned = useUnit(loadMarketOrdersOwned.pending);
  const loadingOrders = useUnit(loadMarketOrders.pending);

  const marketUserMod = useMarketUserMod();

  const allowanceState = useReadContract<bigint>({
    initData: 0 as bigint,
    abi: ERC_20_ABI,
    address: marketplace.tradeToken.address,
    functionName: 'allowance',
    args: [accountAddress, marketplace.wrapAddress],
    skip: !accountAddress || marketplace.tradeToken.isGas || !marketplace.tradeToken.isFetched,
  });
  const sellState = useMarketplaceSell({
    nft,
    marketplace,
    allowanceState,
    onCompleted: async () => {
      await ownedNftState.load();
      await ownedMarketOrders.load();

      setTimeout(() => {
        void ownedNftState.load();
        void ownedMarketOrders.load();
      }, 5000);
    },
  });

  const allowanceUpdate = useWriteContract({
    abi: ERC_20_ABI,
    address: marketplace.tradeToken.address,
  });
  const marketBuy = useWriteContract({
    abi: MARKET_ABI,
    address: marketplace.wrapAddress,
  });
  const marketCancel = useWriteContract({
    abi: MARKET_ABI,
    address: marketplace.wrapAddress,
  });

  // states
  const ownedNftState = useOwnedNfts({
    nftAddress: nft.address,
    accountAddress,
    excludeSoulbound: true,
    limit: 16,
  });
  const userHistoryState = useMarketplaceUserHistory({
    marketAddress: marketplace.address,
    accountAddress,
    nftAddress: nft.address,
  });
  const [tab, setTab] = React.useState<TabEnum>(TabEnum.general);
  const [sellDialog, setSellDialog] = React.useState<boolean>(false);
  const [onlyFavorite, setOnlyFavorite] = React.useState<boolean>(false);
  const [loading, setLoading] = React.useState<boolean>(false);
  const [search, setSearch] = React.useState<string>('');
  const [page, setPage] = React.useState<number>(1);
  const [selectedOrderGroup, setSelectedOrderGroup] = React.useState<IMarketOrderGroupDto | null>(
    null
  );
  const [showMarketBadKarma, setShowMarketBadKarma] = React.useState(false);
  const [orderForBuy, setOrderForBuy] = React.useState<IMarketOrderDto | null>(null);
  const [orderForCancel, setOrderForCancel] = React.useState<IMarketOrderDto | null>(null);
  const [orderPreview, setOrderPreview] = React.useState<IMarketOrderDto | null>(null);
  const [successBoughtOrder, setSuccessBoughtOrder] = React.useState<IMarketOrderDto | null>(null);
  const [itemForOffer, setItemForOffer] = React.useState<IMarketOrderGroupDto | null>(null);
  const [selectedOffer, setSelectedOffer] = React.useState<unknown | null>(null);
  const [filter, setFilter] = React.useState<IMarketFilterState>({
    status: STATUS_LIST[0].value,
    rarities: [],
    slots: [],
    mods: [],
    minPrice: undefined,
    maxPrice: undefined,
    favorites: false,
  });
  const [sort, setSort] = React.useState<Order>(Order.Asc);

  const ownedMarketOrders = useOwnedMarketOrders({
    nftAddress,
    marketAddress: marketplace.address,
    search: search,
    skip: tab !== TabEnum.owned,
  });

  const sellingDisabled = !nft.address || !marketplace.address;
  const userFee = getMarketUserSellFee({
    userMod: marketUserMod.data,
    fee: marketplace.fee,
    slotMod: nftStats.marketDiscount.sale,
  });

  // handlers
  const callLoadOrdersGroups = useCallback(() => {
    if (!marketplace.address || !nftAddress || !accountAddress) {
      return;
    }

    loadMarketOrderGroups({
      limit: 20,
      offset: (page - 1) * 20,
      priceFrom: filter.minPrice,
      priceTo: filter.maxPrice,
      rarities: filter.rarities,
      slots: filter.slots,
      mods: filter.mods,
      search: search || undefined,
      address: marketplace.address,
      nftAddress: nftAddress,
      userAddress: accountAddress,
      onlyFavorite,
      order: sort,
    })
      .then()
      .catch((e) => {
        const err = (e as AxiosError).response.data.error?.message;
        toaster.show({ message: err || 'Failed to load orders', intent: 'danger' });
      });
  }, [
    filter,
    marketplace.address,
    nftAddress,
    accountAddress,
    onlyFavorite,
    sort,
    page,
    marketplace.enabled,
    search,
  ]);

  const callLoadOrderGroupInfo = (orderGroup: IMarketOrderGroupDto) => {
    loadMarketOrders({
      limit: 10,
      offset: 0,
      nftAddress: orderGroup.nftAddress,
      nftTypeId: orderGroup.nftTypeId,
      address: marketplace.address,
    }).then();
    loadMarketOrdersHistory({
      nftAddress: orderGroup.nftAddress,
      nftTypeId: orderGroup.nftTypeId,
      limit: 10,
      offset: 0,
      address: marketplace.address,
    }).then();
  };

  const handleBuy = async (order: IMarketOrderDto) => {
    setLoading(true);

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      if (!marketplace.tradeToken.isGas && BigInt(allowanceState.data) < BigInt(order.priceInWei)) {
        console.log(`[buy] no allowance ${allowanceState.data}`);

        await allowanceUpdate.writeContractAsync({
          functionName: 'approve',
          args: [marketplace.wrapAddress, order.priceInWei],
        });
      }

      Telegram.WebApp.HapticFeedback.impactOccurred('light');
    } catch (e) {
      console.error('[handleBuy] approve', [marketplace.wrapAddress, order.priceInWei], e);
      toaster.captureException(e, 'Failed to approve token');
      setLoading(false);

      return;
    }
    try {
      await marketBuy.writeContractAsync({
        functionName: 'executeOrder',
        args: [nftAddress, +order.assetId, order.priceInWei],
        overrides: marketplace.tradeToken.isGas ? { value: order.priceInWei as bigint } : undefined,
      });
    } catch (e) {
      console.error('[handleBuy] execute', [nftAddress, +order.assetId, order.priceInWei], e);
      setLoading(false);
      toaster.captureException(e, 'Failed to execute order');
      return;
    }

    try {
      marketAnalytics.trackTrade({
        chainId: account.chainId,
        userAddress: account.accountAddress,
        nftAddress,
        price: order.price,
      });

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');

      await wait(TRANSACTION_DELAY);
      await callLoadOrdersGroups();
      await ownedMarketOrders.load();

      setSelectedOrderGroup(null);
      setOrderForBuy(null);
      setSuccessBoughtOrder(order);

      await balance.refetch();
    } catch (e) {
      toaster.captureException(e);
    } finally {
      setLoading(false);
    }
  };
  const handleCancel = async (order: IMarketOrderDto) => {
    setLoading(true);

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

      // await marketplace.api.cancel({
      //   nftAddress: order.nftAddress,
      //   assetId: order.assetId,
      // });
      await marketCancel.writeContractAsync({
        functionName: 'cancelOrder',
        args: [order.nftAddress, +order.assetId],
      });
      Telegram.WebApp.HapticFeedback.notificationOccurred('success');

      await wait(TRANSACTION_DELAY);
      await callLoadOrdersGroups();
      await ownedMarketOrders.load();
      await ownedNftState.load();

      setSelectedOrderGroup(null);
      setOrderForCancel(null);
    } catch (e) {
      toaster.captureException(e);
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
    } finally {
      setLoading(false);
    }
  };
  const handleCreateOffer = (item, value) => {};
  const handleEditOffer = (item, offer, value) => {};
  const handleSelectOrderGroup = (orderGroup: IMarketOrderGroupDto) => {
    setSelectedOrderGroup(orderGroup);

    Telegram.WebApp.HapticFeedback.impactOccurred('light');

    marketOrdersStore.reset();
    marketOrdersHistoryStore.reset();
    marketOrdersOwnedStore.reset();

    callLoadOrderGroupInfo(orderGroup);
  };
  const handleFavorite = async (groupId: string, favorite: boolean) => {
    if (!accountAddress) {
      return;
    }

    try {
      Telegram.WebApp.HapticFeedback.impactOccurred('medium');

      await updateMarketFavoriteGroup({
        favorite,
        orderGroupId: groupId,
        userAddress: accountAddress,
      });

      Telegram.WebApp.HapticFeedback.notificationOccurred('success');
    } catch (e) {
      Telegram.WebApp.HapticFeedback.notificationOccurred('error');
      toaster.captureException(e);
    }
  };

  // hooks
  useBackAction('/wallet');

  React.useEffect(() => {
    if (marketplace.enabled && tab === TabEnum.general) {
      callLoadOrdersGroups();
    }
  }, [callLoadOrdersGroups, tab, chainId]);
  useInterval(
    () => {
      if (marketplace.enabled) {
        if (tab === TabEnum.general) {
          callLoadOrdersGroups();
        }

        if (selectedOrderGroup) {
          callLoadOrderGroupInfo(selectedOrderGroup);
        }

        balance.refetch();
      }
    },
    30_000,
    [marketplace.enabled, accountAddress, filter, search, onlyFavorite, page, sort, tab, chainId]
  );

  // nft
  React.useEffect(() => {
    if (marketplace.enabled) {
      ownedNftState.load();
    }
  }, [marketplace.enabled, ownedNftState.load]);

  // history
  React.useEffect(() => {
    if (tab === TabEnum.history && marketplace.enabled && accountAddress) {
      void userHistoryState.load();
    }
  }, [tab, userHistoryState.load, marketplace.enabled, accountAddress]);
  useInterval(
    () => {
      if (tab === TabEnum.history && marketplace.enabled && accountAddress) {
        void userHistoryState.load();
      }
    },
    30_000,
    [tab, userHistoryState.load, marketplace.enabled, accountAddress]
  );
  useEffect(() => {
    if (marketUserMod.data > 1.2) {
      // setShowMarketBadKarma(true);
    }
  }, [marketUserMod.data]);
  useEffect(() => {
    if (sellState.sellingNft) {
      loadMarketOrders({
        limit: 2,
        offset: 0,
        nftAddress: sellState.sellingNft.contractId,
        nftTypeId: sellState.sellingNft.typeId,
        address: marketplace.address,
      }).then();
    }
  }, [sellState.sellingNft]);
  // useEffect(() => {
  //   setSelectedOrderGroup(orderGroups.rows[0] || null);
  // }, [orderGroups.rows]);

  return (
    <div
      className={styles.root}
      style={{
        backgroundImage: `url(${getDefaultChainImage(account.chainId)})`,
      }}
    >
      <Container
        maxWidth='xs'
        sx={{ px: 0, display: 'flex', flexDirection: 'column', gap: '12px' }}
      >
        {ab.isEnabled('market_tech_works') && <TechnicalWorks />}
        <ChainSwitcher chains={AVAILABLE_CHAINS} />

        {!marketplace.enabled && marketplace.loaded && (
          <div>
            <div className={styles.heading}>
              {/*<MarketIndexerStatus />*/}

              <Typography
                fontWeight='bold'
                variant='h6'
                className={styles.title}
                data-address={marketplace.address}
                data-disabled={!marketplace.enabled}
                color='text.primary'
              >
                {t('Marketplace')}
              </Typography>

              <BalanceToolbar balance={balance.data} tradeToken={marketplace.tradeToken} />
            </div>
            <NotAvailableChain
              title={t('Marketplace')}
              summary={t('Not available from current network')}
              icon={<MarketIcon />}
            />
          </div>
        )}
        {marketplace.enabled && (
          <>
            <div className={styles.heading}>
              <Typography
                fontWeight='bold'
                variant='h6'
                className={styles.title}
                data-address={marketplace.address}
                data-disabled={!marketplace.enabled}
                color='text.primary'
              >
                {t('Marketplace')}
              </Typography>

              <BalanceToolbar balance={balance.data} tradeToken={marketplace.tradeToken} />

              {marketUserMod.data > 1 && (
                <Alert
                  variant='outlined'
                  severity='warning'
                  onClick={() => setShowMarketBadKarma(true)}
                  sx={{ textDecoration: 'underline' }}
                >
                  Marketplace fees raised to {userFee * 100}%
                </Alert>
              )}

              <MarketToolbar
                filter={filter}
                onChangeFilter={(v) => {
                  setFilter(v);
                  setPage(1);
                }}
                search={search}
                onChangeSearch={(v) => {
                  setSearch(v);
                  setPage(1);
                }}
                hideFilter={tab !== TabEnum.general}
                hideFavorites={tab !== TabEnum.general}
                hideSort={tab !== TabEnum.general}
                onlyFavorites={onlyFavorite}
                onChangeOnlyFavorites={(v) => {
                  setOnlyFavorite(v);
                  setPage(1);
                }}
                sort={sort}
                onChangeSort={(v) => setSort(v)}
                loading={loadingGroups}
              />
            </div>

            <StyledPaper variant='outlined' elevation={0} className={styles.items__container}>
              <div className={styles.items__tabs}>
                <Tabs
                  onChange={(_, v) => {
                    setTab(v as TabEnum);
                    Telegram.WebApp.HapticFeedback.impactOccurred('light');
                  }}
                  value={tab}
                >
                  <Tab
                    id={TabEnum.general}
                    label={!onlyFavorite ? t('General') : t('Favorites')}
                    value={TabEnum.general}
                    count={orderGroups.count || undefined}
                  />
                  <Tab
                    id={TabEnum.owned}
                    label={t('My Offers')}
                    value={TabEnum.owned}
                    count={ownedMarketOrders.data.count || undefined}
                  />
                  <Tab id={TabEnum.history} label={t('History')} value={TabEnum.history} />
                </Tabs>
                <Divider />
              </div>

              {loadingGroups || loadingOwned ? (
                <LinearProgress />
              ) : (
                <div className={styles.loadingPlaceholder} />
              )}

              {tab === TabEnum.general && (
                <MarketGrid
                  rows={orderGroups.rows}
                  count={orderGroups.count}
                  onSelectOrderGroup={handleSelectOrderGroup}
                  tradeToken={marketplace.tradeToken}
                  onAddItemForSell={() => {
                    setSellDialog(true);
                    Telegram.WebApp.HapticFeedback.impactOccurred('light');
                  }}
                  onFavorite={handleFavorite}
                  hideFavorites={!accountAddress}
                  page={page}
                  onChangePage={(v) => setPage(v)}
                  ownedRows={ownedMarketOrders.data.rows}
                  isPending={loading}
                />
              )}

              {tab === TabEnum.owned && (
                <MarketGridOwned
                  rows={ownedMarketOrders.data.rows}
                  onSelectOrder={(order) => setOrderForCancel(order)}
                  tradeToken={marketplace.tradeToken}
                  onAddItemForSell={() => setSellDialog(true)}
                  count={ownedMarketOrders.data.count}
                  page={ownedMarketOrders.pagination.page}
                  onChangePage={ownedMarketOrders.pagination.setPage}
                  pageStep={ownedMarketOrders.pagination.step}
                />
              )}

              {tab === TabEnum.history && (
                <MarketUserHistory
                  rows={userHistoryState.data.rows}
                  count={userHistoryState.data.count}
                  tradeToken={marketplace.tradeToken}
                  accountAddress={accountAddress}
                  page={userHistoryState.page}
                  onChangePage={userHistoryState.changePage}
                  loading={userHistoryState.loading}
                  // onOrderSelected={v => setOrderPreview(v)}
                />
              )}
            </StyledPaper>

            {/*{(loading || sellState.loading) && <Loading fullScreen />}*/}

            {/*Окно item nft*/}
            {selectedOrderGroup && (
              <MarketOrderDialog
                orderGroup={selectedOrderGroup}
                ordersHistory={ordersHistory.rows}
                ordersHistoryCount={ordersHistory.count}
                orders={orders.rows}
                ordersCount={orders.count}
                ordersOwned={[]}
                offers={[]}
                walletAddress={accountAddress?.toLowerCase()}
                onClose={() => setSelectedOrderGroup(null)}
                onBuy={(order: IMarketOrderDto) => {
                  setOrderForBuy(order);
                  Telegram.WebApp.HapticFeedback.impactOccurred('light');
                }}
                onCancel={(order: IMarketOrderDto) => {
                  setOrderForCancel(order);
                  Telegram.WebApp.HapticFeedback.impactOccurred('light');
                }}
                onEditOffer={(item: IMarketOrderGroupDto, offer: unknown) => {
                  setItemForOffer(item);
                  setSelectedOffer(offer);
                }}
                onCreateOffer={(item: IMarketOrderGroupDto) => {
                  setItemForOffer(item);
                  setSelectedOffer(true);
                }}
                tradeToken={marketplace.tradeToken}
                balance={balance.data?.formatted || 0}
                isPending={loading}
              />
            )}

            {selectedOrderGroup && orderForBuy && (
              <MarketBuyDialog
                order={orderForBuy}
                orderGroup={selectedOrderGroup}
                onBuy={handleBuy}
                onClose={() => setOrderForBuy(null)}
                disabled={balance?.data.formatted < orderForBuy.price}
                tradeToken={marketplace.tradeToken}
                balance={balance.data}
                noGasBalance={!gasBalance.data?.formatted}
                isPending={loading}
              />
            )}
            {successBoughtOrder && (
              <MarketSuccessBuyDialog
                order={successBoughtOrder}
                onClose={() => setSuccessBoughtOrder(null)}
              />
            )}

            {orderForCancel && (
              <MarketCancelConfirmDialog
                order={orderForCancel}
                isPending={loading}
                onCancel={handleCancel}
                onClose={() => setOrderForCancel(null)}
                tradeToken={marketplace.tradeToken}
              />
            )}

            {orderPreview && (
              <MarketOrderPreviewDialog
                order={orderPreview}
                onClose={() => setOrderPreview(null)}
                tradeToken={marketplace.tradeToken}
              />
            )}

            {itemForOffer && (
              <MarketOfferDialog
                item={itemForOffer}
                offer={
                  !!selectedOffer && typeof selectedOffer === 'object' ? selectedOffer : undefined
                }
                onClose={() => {
                  setItemForOffer(null);
                  setSelectedOffer(null);
                }}
                onCreate={handleCreateOffer}
                onEdit={handleEditOffer}
              />
            )}

            {sellDialog && (
              <MarketOwnedItemsDialog
                nftList={ownedNftState.data.rows}
                count={ownedNftState.data.count}
                onSell={sellState.handleSelectItem}
                onClose={() => setSellDialog(false)}
                page={ownedNftState.page}
                onChangePage={ownedNftState.changePage}
                loading={ownedNftState.loading}
                noGasBalance={!gasBalance.data?.formatted}
              />
            )}

            {!sellState.success && sellState.sellingNft && (
              <MarketSellDialog
                // sell state
                item={sellState.sellingNft}
                onSell={sellState.handleSellConfirm}
                onClose={sellState.handleCloseSell}
                isPending={sellState.loading}
                // market
                fee={marketplace.fee}
                publicationFee={marketplace.publicationFee}
                tradeToken={marketplace.tradeToken}
                userMod={marketUserMod.data}
                // discount
                saleDiscount={nftStats.marketDiscount.sale}
                publicationDiscount={nftStats.marketDiscount.publication}
                // balance
                noGasBalance={!gasBalance.data?.formatted}
                balance={balance.data?.formatted}
                // recommended price
                recommendedPrice={getMarketRecommendedPrice(orders.rows)}
                recommendedPriceLoading={loadingOrders}
              />
            )}
            {!sellState.success && sellState.sellingNft && sellState.price && (
              <MarketSellConfirmDialog
                onClose={sellState.handleCloseSellConfirm}
                price={sellState.price}
                item={sellState.sellingNft}
                onSell={sellState.handleSell}
                disabled={sellingDisabled}
                tradeToken={marketplace.tradeToken}
                isPending={sellState.loading}
              />
            )}
            {sellState.success && (
              <MarketSuccessSellDialog
                nft={sellState.success}
                onClose={sellState.handleCloseSuccessSell}
              />
            )}

            <MarketBadKarmaDialog
              open={showMarketBadKarma}
              onClose={() => setShowMarketBadKarma(false)}
              mod={marketUserMod.data}
            />
          </>
        )}
      </Container>
    </div>
  );
};

export default MarketplacePage;
