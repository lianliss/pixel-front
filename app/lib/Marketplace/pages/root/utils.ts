import { SONGBIRD, SKALE_TEST } from 'services/multichain/chains';
import sgbBgImage from 'assets/img/market/sgb-bg.png';
import React from 'react';

const bgImageByChain = {
  [SONGBIRD]: sgbBgImage as string,
  [SKALE_TEST]: sgbBgImage as string,
};

export function getMarketRootStyles(chainId: number): React.CSSProperties {
  return {
    backgroundImage: bgImageByChain[chainId] ? `url(${bgImageByChain[chainId]})` : undefined,
  };
}
