import React from 'react';
import AbProvider from 'lib/ab/context/ab.provider.tsx';
import NftProvider from 'lib/NFT/context/Nft.provider.tsx';
import MarketplaceProvider from 'lib/Marketplace/context/Marketplace.provider.tsx';
import MarketplacePage from './marketplace.page.tsx';
import SlotsProvider from 'lib/Inventory/context/slots/Slots.provider.tsx';
import Container from '@mui/material/Container';

function MarketplaceContainer() {
  return (
    <AbProvider>
      <NftProvider>
        <SlotsProvider>
          <MarketplaceProvider>
            <MarketplacePage />
          </MarketplaceProvider>
        </SlotsProvider>
      </NftProvider>
    </AbProvider>
  );
}

export default MarketplaceContainer;
