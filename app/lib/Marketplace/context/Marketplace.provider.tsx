import React from 'react';
import MARKET_ABI from 'lib/Marketplace/api-contract/market.abi.ts';
import { IMarketTradeToken, MarketplaceContext } from './Marketplace.context.ts';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { MARKET_CONTRACTS, MARKET_WRAP_CONTRACTS } from 'lib/Marketplace/api-contract/contracts.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useTokenPrice } from '@chain/hooks/useTokenPrice.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useToken } from '@chain/hooks/useToken.ts';
import { wei } from 'utils/index';
import useFlushSlots from 'lib/Inventory/utils/useFlushSlots.ts';

export function useMarketplaceAcceptedToken({
  marketAddress,
  skip,
}: {
  marketAddress: string;
  skip?: boolean;
}) {
  return useReadContract<string>({
    abi: MARKET_ABI,
    address: marketAddress,
    functionName: 'acceptedToken',
    skip: !marketAddress || skip,
  });
}

function MarketplaceProvider(props: React.PropsWithChildren) {
  const account = useAccount();
  const { chainId, isConnected } = account;

  const tradeToken = useTradeToken();
  const flush = useFlushSlots();
  const coinPrice = useTokenPrice({
    address: tradeToken.data.address,
  });

  const marketAddress = MARKET_CONTRACTS[chainId];
  const marketWrapAddress = MARKET_WRAP_CONTRACTS[chainId];

  const acceptedTokenAddressState = useMarketplaceAcceptedToken({ marketAddress });
  const isGas = acceptedTokenAddressState.data === '0x0000000000000000000000000000000000000000';
  const feeState = useReadContract<number>({
    initData: 0,
    abi: MARKET_ABI,
    address: marketAddress,
    functionName: 'orderFee',
    skip: !marketAddress,
    select: (bn) => wei.from(bn, 4),
  });
  const publicationFeeState = useReadContract<number>({
    initData: 0,
    abi: MARKET_ABI,
    address: marketAddress,
    functionName: 'publicationFee',
    skip: !marketAddress,
    select: (bn) => wei.from(bn, 4),
  });
  const acceptedTokenState = useToken({
    address: acceptedTokenAddressState.data || undefined,
    skip: !acceptedTokenAddressState.data,
  });

  const marketTradeToken: IMarketTradeToken = {
    address: acceptedTokenAddressState.data,
    decimals: tradeToken.decimals,
    usdRate: coinPrice.data || 0,
    symbol: acceptedTokenState.data?.symbol,
    logoURI: tradeToken.logoURI,
    isFetched: acceptedTokenState.isFetched,
    isGas: isGas,
  };

  return (
    <MarketplaceContext.Provider
      value={{
        loading: false,
        loaded: isConnected && (marketAddress ? marketTradeToken.isFetched : true),
        enabled: !!marketAddress,
        address: marketAddress,
        wrapAddress: marketWrapAddress,
        fee: feeState.data || 0,
        publicationFee: publicationFeeState.data || 0,
        tradeToken: marketTradeToken,
      }}
    >
      {props.children}
    </MarketplaceContext.Provider>
  );
}

export default MarketplaceProvider;
