import React from 'react';

export type IMarketTradeToken = {
  address: string;
  decimals: number;
  usdRate: number;
  symbol: string;
  logoURI?: string;
  isFetched: boolean;
  isGas: boolean;
};

export type IMarketplaceContext = {
  enabled: boolean;
  loading: boolean;
  loaded: boolean;
  address: string;
  wrapAddress: string;
  fee: number;
  publicationFee: number;
  // acceptedToken?: string;
  tradeToken: IMarketTradeToken;
};

export const MarketplaceContext = React.createContext<IMarketplaceContext>(null);

export function useMarketplace(): IMarketplaceContext {
  return React.useContext(MarketplaceContext);
}
