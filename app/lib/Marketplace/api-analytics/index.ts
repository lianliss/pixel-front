import analytics from '../../../shared/analytics';

class MarketAnalytics {
  public trackTrade(payload: {
    chainId: number;
    userAddress: string;
    nftAddress: string;
    price: number;
  }) {
    analytics.track(
      'tradeNft',
      {
        userAddress: payload.userAddress,
        price: payload.price,
        sum: payload.price,
      },
      { nftAddress: payload.nftAddress, chainId: payload.chainId }
    );
  }
}

const marketAnalytics = new MarketAnalytics();

export default marketAnalytics;
