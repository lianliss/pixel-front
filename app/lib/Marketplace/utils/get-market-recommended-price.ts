import { IMarketOrderDto } from 'lib/Marketplace/api-server/market.types.ts';

export function getMarketRecommendedPrice(orders: IMarketOrderDto[]): number {
  const first = orders[0];
  const second = orders[1];

  if (!first) {
    return 0;
  }

  if (second) {
    return +((first.price + second.price) / 2).toFixed(2);
  }

  return +(first.price * 1.1).toFixed(2);
}
