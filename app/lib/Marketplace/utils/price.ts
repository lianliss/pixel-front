import { wei } from 'utils/index';

export function calcPublicationFeeWei({
  price,
  publicationFee,
  decimals,
}: {
  price: number;
  decimals: number;
  publicationFee: number;
}) {
  const publicationFeeWei = (
    BigInt(wei.to(price, decimals)) / BigInt(1 / publicationFee)
  ).toString();

  return publicationFeeWei;
}
