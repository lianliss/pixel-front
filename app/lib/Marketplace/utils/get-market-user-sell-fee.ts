export function getMarketUserSellFee(payload: {
  fee: number;
  userMod: number;
  slotMod: number;
}): number {
  return Math.min(1, (payload.fee || 1) * (payload.slotMod || 1) * (payload.userMod || 1));
}
