'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  IFindMarketOrdersResponse,
  IFindMarketOrderGroupsResponse,
  IFindMarketOrdersDto,
  IFindMarketOrderGroupsDto,
  IFindMarketOrderHistoryDto,
  IFindMarketOrderHistoryResponse,
  IFindMarketFavoritesDto,
  IFindMarketFavoritesResponse,
  IUpdateMarketFavoriteDto,
  IUpdateMarketFavoriteResponse,
  IMarketStatusResponse,
  IGetMarketUserResponse,
  IGetMarketUserDto,
} from './market.types';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';
import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';

import {
  pixelMessageKey,
  pixelSignKey,
  telegramAuthKey,
} from '../../../shared/const/localStorage.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class MarketApi {
  url: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async fetchOrders({
    limit = 10,
    offset = 0,
    seller,
    address,
    ...other
  }: IFindMarketOrdersDto): Promise<IFindMarketOrdersResponse> {
    const { data } = await axiosInstance.post<
      { data: IFindMarketOrdersResponse },
      AxiosResponse<{ data: IFindMarketOrdersResponse }>,
      IFindMarketOrdersDto
    >(
      `/api/v1/market/order/find`,
      { limit, offset, seller: seller?.toLowerCase(), address, ...other },
      { baseURL: this.url }
    );
    return data.data;
  }

  async fetchOrderGroups({
    limit = 10,
    offset = 0,
    userAddress,
    ...payload
  }: IFindMarketOrderGroupsDto & { userAddress: string }): Promise<IFindMarketOrderGroupsResponse> {
    const { data } = await axiosInstance.post<
      { data: IFindMarketOrderGroupsResponse },
      AxiosResponse<{ data: IFindMarketOrderGroupsResponse }>,
      IFindMarketOrderGroupsDto
    >(
      `/api/v1/market/order/group/find`,
      { limit, offset, ...payload },
      {
        baseURL: this.url,
      }
    );
    return data.data;
  }

  async fetchOrdersHistory({
    limit = 10,
    offset = 0,
    nftAddress,
    nftTypeId,
    address,
    sellerAddress,
    userAddress,
  }: IFindMarketOrderHistoryDto): Promise<IFindMarketOrderHistoryResponse> {
    const { data } = await axiosInstance.post<
      { data: IFindMarketOrderHistoryResponse },
      AxiosResponse<{ data: IFindMarketOrderHistoryResponse }>,
      IFindMarketOrderHistoryDto
    >(
      `/api/v1/market/order/history/find`,
      { limit, offset, nftAddress, nftTypeId, address, sellerAddress, userAddress },
      { baseURL: this.url }
    );
    return data.data;
  }

  async fetchFavorites({
    limit = 10,
    offset = 0,
    nftAddress,
    marketAddress,
    userAddress,
  }: IFindMarketFavoritesDto & { userAddress: string }): Promise<IFindMarketFavoritesResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindMarketFavoritesResponse },
      AxiosResponse<{ data: IFindMarketFavoritesResponse }>,
      IFindMarketFavoritesDto
    >(`/api/v1/market/order/favorite`, {
      baseURL: this.url,
      params: { limit, offset, nftAddress, marketAddress },
    });
    return data.data;
  }

  async updateFavorite({
    favorite,
    orderGroupId,
    userAddress,
  }: IUpdateMarketFavoriteDto & { userAddress: string }): Promise<IUpdateMarketFavoriteResponse> {
    const { data } = await axiosInstance.patch<
      { data: IUpdateMarketFavoriteResponse },
      AxiosResponse<{ data: IUpdateMarketFavoriteResponse }>,
      IUpdateMarketFavoriteDto
    >(
      `/api/v1/market/favorite`,
      { favorite, orderGroupId },
      {
        baseURL: this.url,
      }
    );
    return data.data;
  }

  async status(marketContract: string): Promise<IMarketStatusResponse> {
    const { data } = await axiosInstance.get<
      { data: IMarketStatusResponse },
      AxiosResponse<{ data: IMarketStatusResponse }>,
      { key: string }
    >(`/api/v1/market/${marketContract}/stats`, {
      baseURL: this.url,
    });
    return data.data;
  }

  async getMarketUser(payload: IGetMarketUserDto): Promise<IGetMarketUserResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetMarketUserResponse },
      AxiosResponse<{ data: IGetMarketUserResponse }>,
      IGetMarketUserDto
    >(`/api/v1/market/user`, {
      params: {
        userAddress: payload.userAddress,
        contract: payload.contract,
      },
      baseURL: this.url,
    });

    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const marketApi = new MarketApi({ url: 'https://api.hellopixel.network' });
