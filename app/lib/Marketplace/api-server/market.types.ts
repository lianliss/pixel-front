import {
  INftItemDto,
  INftTypeDto,
  NftItemRarity,
  NftItemSlot,
} from 'lib/NFT/api-server/nft.types.ts';

export type IPaginatedQuery = {
  limit: number;
  offset?: number;
};

export type IPaginatedResult<Row> = {
  rows: Row[];
  count: number;
};

export enum MarketOrderBy {
  Price = 'Price',
  Date = 'Date',
}

export enum Order {
  Asc = 'ASC',
  Desc = 'DESC',
}

//

export type IMarketOrderDto<TDate = number> = {
  id: string;
  // contracts info
  address: string; // marketplace contract address
  assetId: string; // nft id
  nftAddress: string; // nft contract address
  //
  txHash?: string;
  seller: string;
  price: number;
  priceInWei: string;
  expiresAt: TDate;

  totalPrice?: number;
  totalPriceUsd?: number;

  buyer?: string;
  canceled: boolean;

  completedAt?: TDate | null;
  cancelledAt: TDate | null;
  updatedAt: TDate;
  createdAt: TDate;

  nft: INftItemDto | null;
};

export type IMarketOrderGroupDto<TDate = number> = {
  id: string;
  // contracts info
  address: string; // marketplace contract address
  nftTypeId: string; // nft type id
  nftAddress: string; // nft contract address
  //
  ordersCount: number;
  minPrice: number;
  maxPrice: number;
  updatedAt: TDate;
  createdAt: TDate;
  favorite?: boolean;

  // relations
  nftType: INftTypeDto | null;
};

//

export type IGetMarketOrderDto = { id: string };

export type IFindMarketOrdersDto = {
  nftAddress?: string;
  nftTypeId?: string;
  address?: string;
  seller?: string;

  rarities?: NftItemRarity[];
  slots?: NftItemSlot[];
  mods?: string[];
  name?: string;
  priceFrom?: number;
  priceTo?: number;
  typeId?: string;
  search?: string;
} & IPaginatedQuery;

export type IFindMarketOrderHistoryDto = {
  nftTypeId?: string;
  nftAddress: string;
  address?: string;
  sellerAddress?: string;
  userAddress?: string;
} & IPaginatedQuery;

export type IFindMarketOrderGroupsDto = {
  onlyFavorite?: boolean;
  nftAddress?: string;
  address?: string;
  rarities?: NftItemRarity[];
  slots?: NftItemSlot[];
  priceFrom?: number;
  priceTo?: number;
  typeId?: string;
  mods?: string[];
  search?: string;
  orderBy?: MarketOrderBy;
  order?: Order;
  soulbound?: boolean;
} & IPaginatedQuery;

export type IGetMarketUserDto = {
  userAddress: string;
  contract: string; // sgb-market, skale-testnet-market
};

// favorite

export type IMarketFavorite<TDate = Date> = {
  id: string;
  // contracts info
  marketAddress: string; // marketplace contract address
  nftAddress: string; // nft contract address
  nftTypeId: string; // nft type id
  userAddress: string;
  groupId: string;
};

export type IMarketFavoriteDto<TDate = number> = IMarketFavorite<TDate>;
export type IMarketFavoritesDto = IPaginatedResult<IMarketFavoriteDto>;

export type IUpdateMarketFavoriteDto = {
  orderGroupId: string;
  favorite: boolean;
};

export type IUpdateMarketFavoriteResponse = boolean;

export type IFindMarketFavoritesDto = IPaginatedQuery & {
  marketAddress: string;
  nftAddress: string;
};

export type IFindMarketFavoritesResponse = Omit<IPaginatedResult<IMarketFavoriteDto>, 'count'>;

// response

export type IGetMarketOrderResponse = IMarketOrderDto;

export type IFindMarketOrdersResponse = {
  rows: IMarketOrderDto[];
  count: number;
};

export type IFindMarketOrderGroupsResponse = {
  rows: IMarketOrderGroupDto[];
  count: number;
};

export type IFindMarketOrderHistoryResponse = {
  rows: IMarketOrderDto[];
  count: number;
};

export type IMarketStatusResponse = {
  completedOrders: number;
  tradeVolume: number;
  lastDayTradeVolume: number;
  lastDayCompletedOrders: number;
  uniqueSellers: number;
  lastWeekUniqueSellers: number;
  currentOrders: number;
  lastDayNewOrders: number;
};

export type IGetMarketUserResponse = {
  tradeVolume: number;
  currentOrders: number;
  buyerOrders: number;
  receivedAmount: number;
  paidAmount: number;
  sellerOrders: number;
};
