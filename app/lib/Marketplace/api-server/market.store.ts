import { createEffect, createEvent, createStore } from 'effector';
import { marketApi } from 'lib/Marketplace/api-server/market.api.ts';
import {
  IFindMarketOrderGroupsDto,
  IFindMarketOrderHistoryDto,
  IFindMarketOrdersDto,
  IGetMarketUserDto,
  IGetMarketUserResponse,
  IMarketOrderDto,
  IMarketOrderGroupDto,
  IMarketStatusResponse,
  IUpdateMarketFavoriteDto,
} from 'lib/Marketplace/api-server/market.types.ts';
import { AxiosError } from 'axios';
import { clearMinerUser } from 'lib/Mining/api-server/miner.store.ts';

// favorite

export const updateMarketFavoriteGroup = createEffect(
  async (opts: IUpdateMarketFavoriteDto & { userAddress: string }) => {
    const res = await marketApi.updateFavorite(opts);
    return {
      favorite: opts.favorite,
      orderGroupId: opts.orderGroupId,
    };
  }
);

// order groups

export const loadMarketOrderGroups = createEffect(
  async (params: IFindMarketOrderGroupsDto & { userAddress: string }) => {
    const res = await marketApi.fetchOrderGroups(params);
    return res;
  }
);

export const marketOrderGroupsStore = createStore<{ rows: IMarketOrderGroupDto[]; count: number }>({
  rows: [],
  count: 0,
})
  .on(loadMarketOrderGroups.doneData, (prev, next) => next)
  .on(updateMarketFavoriteGroup.doneData, (prev, res) => {
    const rows = prev.rows;

    for (const row of rows) {
      if (row.id === res.orderGroupId) {
        row.favorite = res.favorite;
      }
    }

    return {
      rows,
      count: prev.count,
    };
  });

// orders

export const loadMarketOrders = createEffect(async (params: IFindMarketOrdersDto) => {
  const res = await marketApi.fetchOrders(params);
  return res;
});

export const marketOrdersStore = createStore<{ rows: IMarketOrderDto[]; count: number }>({
  rows: [],
  count: 0,
}).on(loadMarketOrders.doneData, (prev, next) => next);

// orders owned

export const loadMarketOrdersOwned = createEffect(async (params: IFindMarketOrdersDto) => {
  const res = await marketApi.fetchOrders(params);
  return res;
});

export const marketOrdersOwnedStore = createStore<{ rows: IMarketOrderDto[]; count: number }>({
  rows: [],
  count: 0,
}).on(loadMarketOrdersOwned.doneData, (prev, next) => next);

// orders history

export const loadMarketOrdersHistory = createEffect(async (params: IFindMarketOrderHistoryDto) => {
  const res = await marketApi.fetchOrdersHistory(params);
  return res;
});

export const marketOrdersHistoryStore = createStore<{ rows: IMarketOrderDto[]; count: number }>({
  rows: [],
  count: 0,
}).on(loadMarketOrdersHistory.doneData, (prev, next) => next);

// user orders history

export const loadMarketUserOrdersHistory = createEffect(
  async (params: IFindMarketOrderHistoryDto) => {
    const res = await marketApi.fetchOrdersHistory(params);
    return res;
  }
);

export const marketUserOrdersHistoryStore = createStore<{ rows: IMarketOrderDto[]; count: number }>(
  {
    rows: [],
    count: 0,
  }
).on(loadMarketUserOrdersHistory.doneData, (prev, next) => next);

// stats

export const loadMarketStats = createEffect(async (opts: { marketContract: string }) => {
  const res = await marketApi.status(opts.marketContract);
  return res;
});

export const marketStatsStore = createStore({
  loading: false,
  error: null as Error | null,
  state: null as IMarketStatusResponse | null,
})
  .on(loadMarketStats.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadMarketStats.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadMarketStats.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

// market user

export const loadMarketUser = createEffect(async (payload: IGetMarketUserDto) => {
  const res = await marketApi.getMarketUser(payload);
  return res;
});
export const clearMarketUser = createEvent();

export const marketUserStore = createStore({
  loading: true,
  error: null as Error | null,
  state: null as IGetMarketUserResponse | null,
})
  .on(clearMarketUser, () => ({ state: null, error: null, loading: false }))
  .on(loadMarketUser.doneData, (_prev, next) => ({ loading: false, error: null, state: next }))
  .on(loadMarketUser.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadMarketUser.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));
