import { Chain } from 'services/multichain/chains';

export const MARKET_CONTRACTS = {
  [Chain.COSTON2]: '0xcdC6132B862078D4eF91B93B6BA5aE64Ebde5624'.toLowerCase(),
  [Chain.SONGBIRD]: '0x9D597128E1FFDc0d2C6EbaAE8D93e7F9bEB838bf'.toLowerCase(),
  [Chain.SKALE_TEST]: '0x1772A7fd4080b0A80f45254507641CddbC10C5Ce'.toLowerCase(),
  [Chain.SKALE]: '0x9b9c4f527736fE53156199e36C6F4d7A8a195842'.toLowerCase(),
  [Chain.UNITS]: '0xA3D177F5B457e02048fAE06b854f686e5BFf939c'.toLowerCase(),
};

export const MARKET_WRAP_CONTRACTS = {
  ...MARKET_CONTRACTS,
  [Chain.SONGBIRD]: '0x70D31c1fEFe6bEB44ec740Cfc613a5aaC82bDBff'.toLowerCase(),
  [Chain.SKALE]: '0x41565e90554CC17DF677636e45F3a153655f85F5'.toLowerCase(),
  [Chain.UNITS]: '0x4A518D5f23C909bA97EF98A0A14d09305ACcba36'.toLowerCase(),
};

export function isMarketAvailable(chainId: Chain): boolean {
  return !!MARKET_CONTRACTS[chainId];
}
