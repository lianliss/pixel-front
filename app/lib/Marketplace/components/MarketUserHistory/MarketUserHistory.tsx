'use strict';
import React from 'react';
import styles from './MarketUserHistory.module.scss';
import { IMarketOrderDto } from 'lib/Marketplace/api-server/market.types.ts';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';
import MarketUserHistoryItem from 'lib/Marketplace/components/MarketUserHistory/components/MarketUserHistoryItem/MarketUserHistoryItem.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Pagination from '@mui/material/Pagination';
import { useTranslation } from 'react-i18next';

type Props = {
  rows: IMarketOrderDto[];
  tradeToken: IMarketTradeToken;
  loading?: boolean;
  count?: number;
  page?: number;
  onChangePage?: (v: number) => void;
  onOrderSelected?: (order: IMarketOrderDto) => void;
};

function MarketUserHistory(props: Props) {
  const {
    rows = [],
    tradeToken,
    accountAddress,
    loading = false,
    count,
    page,
    onChangePage,
    onOrderSelected,
  } = props;

  const { t } = useTranslation('marketplace');

  const userAddress = accountAddress?.toLowerCase();
  const pagesCount = Math.ceil(count / 20);

  return (
    <div className={styles.root}>
      {rows.map((row) => (
        <MarketUserHistoryItem
          data={row}
          tradeToken={tradeToken}
          accountAddress={userAddress}
          onSelected={onOrderSelected}
        />
      ))}
      {!rows.length && !loading && <Typography>{t('No history')}</Typography>}

      <div className={styles.pagination}>
        {count > 20 && (
          <Pagination
            count={pagesCount}
            color='primary'
            variant='outlined'
            shape='rounded'
            size='large'
            siblingCount={1}
            boundaryCount={0}
            page={page}
            onChange={(_, p) => onChangePage?.(p)}
          />
        )}
      </div>
    </div>
  );
}

export default MarketUserHistory;
