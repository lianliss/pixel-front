'use strict';
import React from 'react';
import styles from './MarketUserHistoryItem.module.scss';
import NftCollectionIcon from 'lib/NFT/components/NftCollectionIcon/NftCollectionIcon';
import { timeToDate } from 'utils/format/date';
import { useUpdateInterval } from 'utils/hooks/useUpdateInterval';
import { IMarketOrderDto } from 'lib/Marketplace/api-server/market.types.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';
import clsx from 'clsx';
import uriTransform from 'utils/uriTransform';
import Paper from '@mui/material/Paper';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { COSTON2, SONGBIRD } from 'services/multichain/chains';
import { useAccount } from '@chain/hooks/useAccount.ts';

const getSgbExplorerUrl = (hash: string) => `https://songbird-explorer.flare.network/tx/${hash}`;
const getFlareTestnetExplorerUrl = (hash: string) =>
  `https://coston2-explorer.flare.network/tx/${hash}`;
const MAP = { [SONGBIRD]: getSgbExplorerUrl, [COSTON2]: getFlareTestnetExplorerUrl };

const getExplorerUrlFactory = (chainId: number) => {
  return MAP[chainId];
};

type Props = {
  data: IMarketOrderDto;
  tradeToken: IMarketTradeToken;
  accountAddress?: string;
  onSelected?: (order: IMarketOrderDto) => void;
};

function MarketUserHistoryItem(props: Props) {
  const { data: order, tradeToken, accountAddress, onSelected } = props;

  const { chainId } = useAccount();
  const getExplorerUrl = getExplorerUrlFactory(chainId);

  const tradeTokenSymbol = tradeToken?.symbol || '';
  const isBuy = order.buyer?.toLowerCase() === accountAddress;
  const rarityColor = order.nft?.type?.rarity ? getNftRarityColor(order.nft.type.rarity) : '';

  useUpdateInterval(1000);

  return (
    <Paper
      elevation={0}
      variant='outlined'
      className={styles.row}
      onClick={() => {
        onSelected?.(order);
      }}
    >
      <div>
        <img
          src={uriTransform(order.nft?.type?.uri)}
          alt=''
          className={styles.img}
          style={{ borderColor: rarityColor }}
        />
      </div>

      <div className={styles.info}>
        <Typography variant='subtitle1' color='text.primary' noWrap className={styles.title}>
          {order.nft?.type?.name}
        </Typography>

        <div className={styles.price__section}>
          <NftCollectionIcon collectionId={order?.nft?.type?.collectionId} />

          <Typography className={styles.price} noWrap color='text.primary'>
            {+(+order.price).toFixed(2)} {tradeTokenSymbol}
            <span> | ${+(+order.totalPriceUsd).toFixed(2) || `<0.01`}</span>
            {order.txHash && getExplorerUrl && (
              <a href={getExplorerUrl(order.txHash)} target='_blank'>
                show
              </a>
            )}
          </Typography>
        </div>
      </div>

      <div className={styles.date}>
        <Typography
          variant='subtitle2'
          className={clsx({ [styles.isBuy]: isBuy, [styles.isSell]: !isBuy })}
        >
          {isBuy ? 'buy' : 'sell'}
        </Typography>
        <div>{timeToDate(new Date(order.completedAt || 0))}</div>
      </div>
    </Paper>
  );
}

export default MarketUserHistoryItem;
