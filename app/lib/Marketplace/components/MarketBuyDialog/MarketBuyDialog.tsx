'use strict';
import React from 'react';
import styles from './MarketBuyDialog.module.scss';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { IMarketOrderDto, IMarketOrderGroupDto } from 'lib/Marketplace/api-server/market.types.ts';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';
import { IUserBalance } from '@chain/hooks/useBalance.ts';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useTranslation } from 'react-i18next';
import { useNftItem } from 'lib/NFT/hooks/useNftItem.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

type Props = {
  onBuy?: (order: IMarketOrderDto) => void;
  onClose?: () => void;
  orderGroup: IMarketOrderGroupDto;
  order: IMarketOrderDto;
  disabled?: boolean;
  tradeToken: IMarketTradeToken;
  balance?: IUserBalance | null;
  noGasBalance?: boolean;
  isPending?: boolean;
};

function MarketBuyDialog(props: Props) {
  const {
    onBuy,
    onClose,
    orderGroup,
    order,
    disabled,
    tradeToken,
    balance,
    noGasBalance = false,
    isPending = false,
  } = props;

  const { t } = useTranslation('marketplace');
  const nftItemState = useNftItem({
    assetId: order.assetId,
    contractId: order.nftAddress,
  });

  const handleBuy = () => {
    if (onBuy) {
      onBuy(order);
    }
  };

  const nftName = orderGroup.nftType.name;

  return (
    <BottomDialog open onClose={onClose} title={t('Buy NFT')}>
      <div className={styles.root} style={{ marginTop: 8 }}>
        <div className={styles.nft}>
          <NFTCard
            nft={nftItemState.loading ? order.nft : nftItemState.state || order.nft}
            loading={nftItemState.loading}
          />
        </div>

        <div className={styles.priceBox}>
          <Typography className={styles.priceLabel}>{t('Price')}</Typography>
          <Typography color='text.secondary' className={styles.prices}>
            <span>
              {+order.price.toFixed(2)} {tradeToken.symbol}
            </span>
            {/*<span> | $2.00</span>*/}
          </Typography>
        </div>

        <Typography color='text.primary' variant='subtitle2' className={styles.balance}>
          <span>{t('Your balance:')}</span> {balance.formatted.toFixed(2)}{' '}
          <span>{balance.symbol}</span>
        </Typography>

        <Typography
          color='text.primary'
          variant='caption'
          sx={{ opacity: 0.5 }}
          component='p'
          align='center'
        >
          Token ID: {order.assetId}
        </Typography>

        <Button
          variant='contained'
          color='primary'
          fullWidth
          size='large'
          sx={{ mt: 2 }}
          onClick={handleBuy}
          disabled={!onBuy || !orderGroup || disabled || noGasBalance}
          loading={isPending}
        >
          {noGasBalance ? t('No gas') : `${t('Buy')} ${nftName}`}
        </Button>
      </div>
    </BottomDialog>
  );
}

export default React.memo(MarketBuyDialog);
