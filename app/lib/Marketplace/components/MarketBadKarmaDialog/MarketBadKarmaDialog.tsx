import Dialog, { DialogProps } from '@mui/material/Dialog';
import BottomDialog, { IBottomDialogProps } from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

type Props = Omit<IBottomDialogProps, 'children'> & { mod?: number };

export function MarketBadKarmaDialog(props: Props) {
  const { mod, ...other } = props;
  return (
    <BottomDialog title='Bad Karma' {...other}>
      <Typography component='p' align='center'>
        You have been detected engaging in repeated ecosystem abuse. As a result, your Karma has
        fallen into the negative zone.
      </Typography>

      <Typography component='p' align='center' sx={{ mt: 2, mb: 2 }}>
        Penalties applied:
        <Typography component='p'>- Increased transaction costs.</Typography>
        <Typography component='p'>- Marketplace fees increased.</Typography>
      </Typography>

      <Typography component='p' align='center'>
        To resolve this issue, please contact{' '}
        <Typography component='a' href='http://t.me/zukasti' target='_blank'>
          Support
        </Typography>{' '}
        and complete the KYC process.
      </Typography>
    </BottomDialog>
  );
}
