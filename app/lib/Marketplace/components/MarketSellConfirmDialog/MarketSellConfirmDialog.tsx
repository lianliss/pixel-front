'use strict';
import React from 'react';
import styles from './MarketSellConfirmDialog.module.scss';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { ITokenData } from '@chain/hooks/useToken.ts';
import { useTranslation } from 'react-i18next';
import { useNftItem } from 'lib/NFT/hooks/useNftItem.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';

type Props = {
  onSell?: (item: INftItemDto, price: number) => void;
  onClose?: () => void;
  item: INftItemDto;
  price: number;
  disabled?: boolean;
  tradeToken?: IMarketTradeToken;
  isPending?: boolean;
};

function MarketSellConfirmDialog(props: Props) {
  const { onSell, onClose, item, price, disabled, tradeToken, isPending = false } = props;

  const { t } = useTranslation('inventory');
  const nftItemState = useNftItem({
    assetId: item.assetId,
    contractId: item.contractId,
  });

  const handleSell = (e) => {
    e.stopPropagation();

    if (onSell) {
      onSell(item, price);
    }
  };

  const nftName = item?.type.name || 'unknown';

  return (
    <BottomDialog isOpen onClose={onClose} title={t('Do you want to sell your item?')}>
      <div className={styles.root} style={{ marginTop: 12 }}>
        <div className={styles.nft}>
          <NFTCard
            nft={nftItemState.loading ? item : nftItemState.state || item}
            loading={nftItemState.loading}
          />
        </div>

        <div className={styles.priceBox}>
          <Typography color='text.primary' className={styles.priceLabel}>
            Price
          </Typography>
          <Typography color='text.primary' className={styles.prices}>
            <span>
              {price} {tradeToken?.symbol}
            </span>
            <span> {tradeToken ? `| $${+(price * tradeToken.usdRate).toFixed(2)}` : ''}</span>
          </Typography>
        </div>

        <Button
          className={styles.button}
          variant='contained'
          color='primary'
          size='large'
          fullWidth
          onClick={handleSell}
          disabled={!onSell || !item || disabled}
          loading={isPending}
        >
          {t('Sell Item')}
        </Button>
      </div>
    </BottomDialog>
  );
}

export default React.memo(MarketSellConfirmDialog);
