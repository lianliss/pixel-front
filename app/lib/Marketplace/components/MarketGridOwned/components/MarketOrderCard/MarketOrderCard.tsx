'use strict';
import React from 'react';
import styles from './MarketOrderCard.module.scss';
import NftTypeIcon from 'lib/NFT/components/NftTypeIcon/NftTypeIcon.tsx';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import NftCollectionIcon from 'lib/NFT/components/NftCollectionIcon/NftCollectionIcon';
import clsx from 'clsx';
import { IMarketOrderDto } from 'lib/Marketplace/api-server/market.types.ts';
import { RarityChip } from 'lib/NFT/components/RarityChip/index.ts';
import uriTransform from 'utils/uriTransform';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

type Props = { data: IMarketOrderDto; tradeToken: any } & React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLElement>,
  HTMLElement
>;

function MarketOrderCard(props: Props) {
  const { data, className, tradeToken, ...other } = props;

  const symbol = tradeToken?.symbol || '';

  const price = data.price;
  const nftType = data.nft.type;

  const nftSlots = nftType.slots;
  const nftRarity = nftType.rarity;
  const nftName = nftType.name;
  const rarityColor = getNftRarityColor(nftRarity);

  return (
    <article className={clsx(styles.marketOrder, className)} {...other}>
      <div className={styles.marketOrder__preview}>
        <img src={uriTransform(nftType.uri)} alt='' className={styles.marketOrder__image} />
      </div>

      <div className={styles.marketOrder__dividerBox}>
        <hr className={styles.marketOrder__divider} style={{ borderColor: rarityColor }} />
        <RarityChip className={styles.marketOrder__rarity} rarity={nftRarity} />
        <NftTypeIcon
          className={styles.marketOrder__type}
          rarity={nftRarity}
          slot={nftSlots[0].slot}
        />
      </div>

      <div className={styles.marketOrder__content}>
        <p className={styles.marketOrder__title}>{nftName}</p>

        <div className={styles.marketOrder__infoPrices}>
          <NftCollectionIcon collectionId={nftType?.collectionId} />

          <div className={styles.marketOrder__priceBox}>
            <Typography className={styles.marketOrder__priceSgb} variant='caption' noWrap>
              {+price.toFixed(2)} <span>{symbol}</span>
            </Typography>
            <p className={styles.marketOrder__priceUsd}>
              ${(+price * tradeToken.usdRate).toFixed(2)}
            </p>
          </div>
        </div>
      </div>
    </article>
  );
}

export default MarketOrderCard;
