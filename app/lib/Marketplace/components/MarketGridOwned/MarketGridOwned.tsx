'use strict';
import React from 'react';
import styles from './MarketGridOwned.module.scss';
import { IMarketOrderDto } from 'lib/Marketplace/api-server/market.types.ts';
import MarketOrderCard from 'lib/Marketplace/components/MarketGridOwned/components/MarketOrderCard/MarketOrderCard.tsx';
import MarketAddOrder from 'lib/Marketplace/components/MarketGrid/components/MarketAddOrder/MarketAddOrder.tsx';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';
import { lighten, styled } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';
import Pagination from '@mui/material/Pagination';

const StyledGrid = styled('div')(({ theme }) => ({
  // backgroundColor: lighten(theme.palette.background.paper, 0.05),
}));

type Props = {
  rows: IMarketOrderDto[];
  onSelectOrder?: (order: IMarketOrderDto) => void;
  tradeToken: IMarketTradeToken;
  onAddItemForSell?: () => void;
  count?: number;
  page?: number;
  pageStep?: number;
  onChangePage?: (page: number) => void;
};

function MarketGridOwned(props: Props) {
  const {
    rows,
    onSelectOrder,
    tradeToken,
    onAddItemForSell,
    count = 0,
    page,
    onChangePage,
    pageStep = 20,
  } = props;

  const { t } = useTranslation('marketplace');

  const pagesCount = Math.ceil(count / pageStep);

  const handleSelectOrder = (row: IMarketOrderDto) => (e: any) => {
    e.stopPropagation();

    if (onSelectOrder) {
      onSelectOrder(row);
    }
  };

  return (
    <div className={styles.root}>
      <StyledGrid className={styles.grid}>
        {onAddItemForSell && <MarketAddOrder onClick={onAddItemForSell} />}
        {rows.map((row, i) => {
          if (!row.nft?.type) {
            return <div key={`market-item-${i}`}>{t('nft not found')}</div>;
          }

          return (
            <MarketOrderCard
              key={`market-order-${i}`}
              data={row}
              onClick={handleSelectOrder(row)}
              tradeToken={tradeToken}
            />
          );
        })}
      </StyledGrid>

      {count > pageStep && (
        <Pagination
          count={pagesCount}
          color='primary'
          variant='outlined'
          shape='rounded'
          size='large'
          siblingCount={1}
          boundaryCount={0}
          page={page}
          onChange={(_, p) => onChangePage?.(p)}
        />
      )}
    </div>
  );
}

export default MarketGridOwned;
