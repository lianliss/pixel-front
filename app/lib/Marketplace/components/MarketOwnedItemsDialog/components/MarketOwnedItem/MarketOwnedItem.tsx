import React from 'react';
import clsx from 'clsx';
import styles from './MarketOwnedItem.module.scss';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';

type Props = { item: INftItemDto; selected?: boolean } & React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>;

const MarketOwnedItem = (props: Props) => {
  const { item, selected, ...other } = props;

  const name = item?.type.name;
  const uri = item?.type.uri;
  const rarity = item?.type.rarity;

  return (
    <div className={clsx(styles.root, { [styles.selected]: selected })} {...other}>
      <div className={styles.box}>
        <div
          className={styles.preview}
          style={{
            backgroundImage: uri ? `url(${uri})` : undefined,
            borderColor: getNftRarityColor(rarity),
          }}
        />
      </div>

      <div className={styles.info}>
        {/*{count && <span className={`${PREFIX}__count`}>{count}/{total}</span>}*/}
        <span className={styles.label} style={{}}>
          {item ? name : 'Empty'}
        </span>
      </div>
    </div>
  );
};

export default MarketOwnedItem;
