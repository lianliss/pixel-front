'use strict';
import React from 'react';
import styles from './MarketOwnedItemsDialog.module.scss';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import MarketOwnedItem from 'lib/Marketplace/components/MarketOwnedItemsDialog/components/MarketOwnedItem/MarketOwnedItem.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import Pagination from '@mui/material/Pagination';
import LinearProgress from '@mui/material/LinearProgress';
import InventoryItem from 'lib/Inventory/components/InventoryItem/InventoryItem.tsx';
import { useTranslation } from 'react-i18next';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

type Props = {
  onSell?: (nft: INftItemDto) => void;
  onClose?: () => void;
  loading?: boolean;
  nftList?: INftItemDto[];
  count: number;
  page?: number;
  onChangePage?: (page: number) => void;
  noGasBalance?: boolean;
};

function MarketOwnedItemsDialog(props: Props) {
  const {
    onSell,
    onClose,
    nftList = [],
    count,
    page,
    onChangePage,
    loading = false,
    noGasBalance = false,
  } = props;

  const { t } = useTranslation('marketplace');

  const [selectedItem, setSelectedItem] = React.useState<INftItemDto | null>(null);

  const pagesCount = Math.ceil(count / 16);

  const handleSell = () => {
    if (onSell && selectedItem) {
      Telegram.WebApp.HapticFeedback.impactOccurred('light');

      onSell(selectedItem);
      setSelectedItem(null);
    }
  };

  return (
    <BottomDialog
      isOpen
      className={styles.root}
      onClose={onClose}
      footer={
        <div className={styles.footer}>
          {selectedItem && (
            <Button
              size='large'
              variant='contained'
              onClick={handleSell}
              disabled={!onSell || !selectedItem || noGasBalance}
              fullWidth
            >
              {noGasBalance ? t('No gas') : t('Sell Item')}
            </Button>
          )}

          {loading && <LinearProgress sx={{ width: '100%', mt: '-16px' }} />}

          {count > 16 && (
            <Pagination
              count={pagesCount}
              color='primary'
              variant='outlined'
              shape='rounded'
              size='large'
              siblingCount={1}
              boundaryCount={0}
              page={page}
              onChange={(_, p) => {
                onChangePage?.(p);
                setSelectedItem(null);
              }}
            />
          )}
        </div>
      }
    >
      <div>
        <Typography color='text.primary' fontWeight='bold' className={styles.title} sx={{ mb: 1 }}>
          {t('Sell item from your inventory')}
        </Typography>

        {nftList.length > 0 && (
          <div className={styles.grid}>
            {nftList.map((row) => {
              const selected = selectedItem && row.id === selectedItem.id;

              return (
                <InventoryItem
                  key={`item-${row.id}`}
                  item={row}
                  onClick={() => {
                    if (selected) {
                      setSelectedItem(null);
                    } else {
                      setSelectedItem(row);
                    }
                  }}
                  selected={selected}
                />
              );
            })}
          </div>
        )}

        {!nftList.length && <p>{t('Empty inventory')}</p>}
      </div>
    </BottomDialog>
  );
}

export default MarketOwnedItemsDialog;
