'use strict';
import React from 'react';
import styles from './MarketOrderDialog.module.scss';
import MarketOrders from 'lib/Marketplace/components/MarketOrderDialog/components/MarketOrders/MarketOrders.tsx';
import OrderStatsBlock from 'lib/Marketplace/components/MarketOrderDialog/components/OrderStatsBlock/OrderStatsBlock.tsx';
import ItemOffers from 'lib/Marketplace/components/MarketOrderDialog/components/ItemOffers/ItemOffers';
import ItemSellHistory from 'lib/Marketplace/components/MarketOrderDialog/components/ItemSellHistory/ItemSellHistory.tsx';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { deduplicate } from 'utils/array/deduplicate';
import { IMarketOrderDto, IMarketOrderGroupDto } from 'lib/Marketplace/api-server/market.types.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import uriTransform from 'utils/uriTransform';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';
import { useTranslation } from 'react-i18next';
import { useNftType } from 'lib/NFT/hooks/useNftType.ts';

type Props = {
  orderGroup: IMarketOrderGroupDto;
  onClose?: () => void;
  onBuy?: (order: IMarketOrderDto) => void;
  onCancel?: (order: IMarketOrderDto) => void;
  onEditOffer?: (orderGroup: IMarketOrderGroupDto, offer: unknown, price: number) => void;
  onCreateOffer?: (orderGroup: IMarketOrderGroupDto, price: number) => void;
  ordersHistory?: IMarketOrderDto[];
  ordersHistoryCount?: number;
  orders?: IMarketOrderDto[];
  ordersCount?: number;
  ordersOwned?: IMarketOrderDto[];
  walletAddress?: string;
  tradeToken: IMarketTradeToken;
  offers?: unknown[];
  balance?: number;
  isPending?: boolean;
};

function MarketOrderDialog(props: Props) {
  const {
    orderGroup,
    onClose,
    onBuy,
    onCancel,
    onEditOffer,
    onCreateOffer,
    ordersHistory = [],
    orders = [],
    ordersCount = 0,
    ordersOwned = [],
    walletAddress,
    tradeToken,
    balance,
    ordersHistoryCount = 0,
    isPending = false,
  } = props;

  const { t } = useTranslation('marketplace');
  const nftTypeState = useNftType({
    typeId: orderGroup.nftTypeId,
    // typeId: 'sgb-pixel-615', // recipe
    // typeId: 'sgb-pixel-500', // drone
  });

  if (!orderGroup) {
    return null;
  }

  const handleCancel = (order: IMarketOrderDto) => {
    if (onCancel) {
      onCancel(order);
    }
  };
  const handleBuy = (order: IMarketOrderDto) => {
    if (onBuy) {
      onBuy(order);
    }
  };
  const handleEditOffer = (offer: unknown) => {
    if (onEditOffer) {
      onEditOffer(orderGroup, offer, -1);
    }
  };
  const handleCreateOffer = () => {
    if (onCreateOffer && orderGroup) {
      onCreateOffer(orderGroup, -1);
    }
  };

  const nftName = orderGroup.nftType.name;
  const nftImage = uriTransform(orderGroup.nftType.uri);

  const allOrders = React.useMemo(
    () => deduplicate([...ordersOwned, ...orders], (el) => el.id) as IMarketOrderDto[],
    [orders, ordersOwned]
  );

  return (
    <BottomDialog isOpen onClose={onClose}>
      <div className={styles.root}>
        <div>
          <Typography color='text.primary' align='center' variant='h2' fontWeight='bold'>
            {nftName}
          </Typography>

          <img src={nftImage} alt='' className={styles.image} />

          <div className={styles.nft__desc}>
            <Typography variant='caption' color='button-icon'>
              {t('You can use this only in rare cases when you are hungry or traveling')}
            </Typography>
          </div>

          <div className={styles.blocks}>
            <OrderStatsBlock
              orderGroup={orderGroup}
              nftType={nftTypeState.state}
              nftTypeLoading={nftTypeState.loading}
              tradeToken={tradeToken}
            />

            <MarketOrders
              options={allOrders}
              count={ordersCount}
              onBuy={handleBuy}
              onCancel={handleCancel}
              walletAddress={walletAddress}
              tradeToken={tradeToken}
              balance={balance}
              isPending={isPending}
            />

            {false && (
              <ItemOffers
                options={[]}
                onEditOffer={handleEditOffer}
                onCreateOffer={handleCreateOffer}
              />
            )}

            {!!ordersHistory.length && (
              <ItemSellHistory
                options={ordersHistory}
                tradeToken={tradeToken}
                count={ordersHistoryCount}
              />
            )}

            <Typography
              color='text.primary'
              variant='caption'
              sx={{ opacity: 0.5 }}
              component='p'
              align='center'
            >
              Type ID: {orderGroup?.nftTypeId?.split('-').pop() || '-'}
            </Typography>
          </div>
        </div>
      </div>
    </BottomDialog>
  );
}

export default MarketOrderDialog;
