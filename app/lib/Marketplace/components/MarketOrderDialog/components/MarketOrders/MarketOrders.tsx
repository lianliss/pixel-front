'use strict';
import React from 'react';
import styles from './MarketOrders.module.scss';
import clsx from 'clsx';
import NftCollectionIcon from 'lib/NFT/components/NftCollectionIcon/NftCollectionIcon';
import BuyCart from 'lib/Marketplace/components/icons/BuyCart';
import { IMarketOrderDto } from 'lib/Marketplace/api-server/market.types.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Icon } from '@blueprintjs/core';
import { DropdownElement } from 'lib/ui';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';
import { Button } from '@ui-kit/Button/Button.tsx';
import Paper from '@mui/material/Paper';
import { useTranslation } from 'react-i18next';

type Props = {
  options: IMarketOrderDto[];
  count?: number;
  onBuy?: (order: IMarketOrderDto) => void;
  onCancel?: (order: IMarketOrderDto) => void;
  walletAddress?: string;
  tradeToken?: IMarketTradeToken;
  balance?: number;
  isPending?: boolean;
};

function MarketOrders(props: Props) {
  const {
    options = [] as IMarketOrderDto[],
    onBuy,
    onCancel,
    walletAddress,
    tradeToken,
    balance,
    count = 0,
    isPending = false,
  } = props;

  const { t } = useTranslation('marketplace');

  const tradeTokenSymbol = tradeToken?.symbol || '';
  const collectionId = options[0]?.nft?.type?.collectionId;

  const myOrders = options.filter(
    (order) => order.seller.toLowerCase() === walletAddress?.toLowerCase()
  );
  const externalOrders = options.filter(
    (order) => order.seller.toLowerCase() !== walletAddress?.toLowerCase()
  );
  const groups: Record<string, IMarketOrderDto[]> = externalOrders.reduce(
    (acc, option) => ({
      ...acc,
      [option.price]: [...(acc[option.price] || []), option],
    }),
    {}
  );
  const otherGroups = Object.keys(groups)
    .map((key) => ({ orders: groups[key], price: +key }))
    .sort((a, b) => a.price - b.price);

  const renderOrders = (opts: {
    orders: IMarketOrderDto[];
    price: number;
    count: number;
    isMy: boolean;
    logoURI: string;
  }) => {
    // const isMy = walletAddress && walletAddress.toLowerCase() === option.seller.toLowerCase();
    const disabledBuy = typeof balance === 'number' && balance < opts.price;

    return (
      <Paper
        elevation={0}
        variant='outlined'
        className={styles.row}
        sx={{ backgroundColor: 'transparent' }}
      >
        <div className={styles.rowInfo}>
          <NftCollectionIcon collectionId={collectionId} />

          <Typography className={styles.value} variant='caption' noWrap>
            {+(+opts.price).toFixed(2)} {tradeTokenSymbol}
            <span> {tradeToken ? `| ${+(+opts.price * tradeToken.usdRate).toFixed(2)}` : ''}</span>
          </Typography>
        </div>

        <span className={styles.rowQuantity}>{opts.count}</span>

        <div>
          {opts.isMy && (
            <Button
              variant='outlined'
              className={styles.btnBuy}
              onClick={() => {
                if (onCancel) {
                  onCancel(opts.orders[0]);
                }
              }}
              disabled={!onCancel}
            >
              Cancel
            </Button>
          )}
          {!opts.isMy && (
            <Button
              variant='contained'
              className={styles.btnBuy}
              onClick={() => {
                if (onBuy) {
                  onBuy(opts.orders[0]);
                }
              }}
              disabled={!onBuy || disabledBuy || isPending}
            >
              <BuyCart />
            </Button>
          )}
        </div>
      </Paper>
    );
  };

  return (
    <Paper variant='outlined' elevation={0} className={clsx(styles.root)}>
      <DropdownElement
        defaultExpanded
        dropElement={
          <>
            <hr />

            <div className={styles.header}>
              <span className={styles.label}>{t('Price')}</span>
              <span className={styles.label}>{t('Quantity')}</span>
              <span className={styles.label}>{t('Buy')}</span>
            </div>

            <hr />

            <div className={styles.list}>
              {myOrders.map((order, i) => {
                return (
                  <React.Fragment key={`listing-option-${i}`}>
                    {renderOrders({
                      orders: [order],
                      price: order.price,
                      isMy: true,
                      count: 1,
                      logoURI: order.logoURI,
                    })}
                  </React.Fragment>
                );
              })}
              {otherGroups.map((option, i) => {
                return (
                  <React.Fragment key={`listing-option-${i}`}>
                    {renderOrders({
                      orders: option.orders,
                      price: option.price,
                      isMy: false,
                      count: option.orders.length,
                      logoURI: option.logoURI,
                    })}
                  </React.Fragment>
                );
              })}
            </div>
          </>
        }
      >
        <div className={styles.title}>
          <Typography variant='body2' fontWeight='bold'>
            {t('Listings')} {count > 0 ? `(${count})` : ''}
          </Typography>

          <div className={styles.icon}>
            <div className='dropdown-icon'>
              <Icon icon={'chevron-down'} />
            </div>
          </div>
        </div>
      </DropdownElement>
    </Paper>
  );
}

export default MarketOrders;
