'use strict';
import React from 'react';
import styles from './ItemSellHistory.module.scss';
import clsx from 'clsx';
import NftCollectionIcon from 'lib/NFT/components/NftCollectionIcon/NftCollectionIcon';
import { timeToDate } from 'utils/format/date';
import { useUpdateInterval } from 'utils/hooks/useUpdateInterval';
import { wei } from 'utils';
import { IMarketOrderDto } from 'lib/Marketplace/api-server/market.types.ts';
import { convertToUsdPrice } from 'lib/Marketplace/utils.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { DropdownElement } from 'lib/ui';
import { Icon } from '@blueprintjs/core';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';
import Paper from '@mui/material/Paper';
import { useTranslation } from 'react-i18next';
import Divider from '@mui/material/Divider';

type Props = { options: IMarketOrderDto[]; tradeToken: IMarketTradeToken; count?: number };

function ItemSellHistory(props: Props) {
  const { options = [], tradeToken, count = 0 } = props;
  const [isOpen, setIsOpen] = React.useState(false);

  const { t } = useTranslation('marketplace');

  const tradeTokenDecimals = tradeToken?.decimals || 18;
  const tradeTokenSymbol = tradeToken?.symbol || '';
  const collectionId = options[0]?.nft?.type?.collectionId;

  useUpdateInterval(1000);

  return (
    <Paper variant='outlined' elevation={0} className={clsx(styles.root)}>
      <DropdownElement
        dropElement={
          <>
            <Divider />

            <div className={styles.header}>
              <span className={styles.label}>{t('Seller')}</span>
              <span className={styles.label}>{t('Sale price')}</span>
              <span className={styles.label}>{t('Sale date')}</span>
            </div>

            <Divider />

            <div className={styles.list}>
              {options.map((order, i) => {
                return (
                  <Paper
                    elevation={0}
                    variant='outlined'
                    key={`sell-option-${i}`}
                    className={styles.row}
                    sx={{ backgroundColor: 'transparent' }}
                  >
                    <span className={styles.rowSeller}>{order.seller}</span>

                    <div className={styles.rowInfo}>
                      <NftCollectionIcon collectionId={collectionId} />

                      <div className={styles.price__section}>
                        <Typography className={styles.price} color='text.primary' noWrap>
                          {+(+order.price).toFixed(2)} {tradeTokenSymbol}
                          <span> | ${+(+order.totalPriceUsd).toFixed(2)}</span>
                        </Typography>
                      </div>
                    </div>

                    <Typography color='text.primary' variant='caption' className={styles.rowDate}>
                      {timeToDate(new Date(order.completedAt || 0))}
                    </Typography>
                  </Paper>
                );
              })}
            </div>
          </>
        }
      >
        <div className={styles.title}>
          <Typography variant='body2' fontWeight='bold'>
            {t('Item Sell History')} {count > 0 ? `(${count})` : ''}
          </Typography>

          <div className={styles.icon}>
            <div className='dropdown-icon'>
              <Icon icon={'chevron-down'} />
            </div>
          </div>
        </div>
      </DropdownElement>
    </Paper>
  );
}

export default ItemSellHistory;
