import styles from 'lib/Marketplace/components/MarketOrderDialog/components/OrderStatsBlock/OrderStatsBlock.module.scss';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { IMinerClaimerTypeDto } from 'lib/MinerClaimer/api-server/miner-claimer.types.ts';
import { getLocaleTime } from 'utils/format/date';
import { getMinerClaimerSessionTImeStr } from 'lib/MinerClaimer/utils/getMinerClaimerSessionTImeStr.ts';

function MarketDroneParams(props: { droneType: IMinerClaimerTypeDto }) {
  const { droneType } = props;

  const { t } = useTranslation('marketplace');

  const isTimed = droneType.isTimeSession;

  return (
    <div className={styles.block}>
      <span className={styles.label}>{t('Claimer')}</span>
      <div className={styles.stats}>
        {!isTimed && (
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <span className={styles.statLabel}>Duration</span>
            <span>{`${droneType.sessionClaims} ${t('claims')}`}</span>
          </div>
        )}

        {isTimed && (
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <span className={styles.statLabel}>Duration</span>
            <span>{getMinerClaimerSessionTImeStr(+droneType.sessionDuration)}</span>
          </div>
        )}

        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <span className={styles.statLabel}>Durability</span>
          <span>{getLocaleTime(+droneType.duration)}</span>
        </div>

        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <span className={styles.statLabel}>Commission discount</span>
          <span>{`-${((1 - droneType.txPriceMod || 0) * 100).toFixed(2)}%`}</span>
        </div>
      </div>
    </div>
  );
}

export default MarketDroneParams;
