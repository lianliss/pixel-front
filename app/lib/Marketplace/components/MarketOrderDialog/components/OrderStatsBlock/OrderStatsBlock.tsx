'use strict';
import React from 'react';
import styles from './OrderStatsBlock.module.scss';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import { uppercaseFirst } from 'utils/format/string';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import { getNftSlotLabel } from 'lib/NFT/utils/getNftSlotLabel.ts';
import { IMarketOrderGroupDto } from 'lib/Marketplace/api-server/market.types.ts';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';
import { useTranslation } from 'react-i18next';
import { INftTypeDto } from 'lib/NFT/api-server/nft.types.ts';
import MarketNftParams from 'lib/Marketplace/components/MarketOrderDialog/components/MarketNftParams/MarketNftParams.tsx';
import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';
import CircularProgress from '@mui/material/CircularProgress';
import MarketDroneParams from 'lib/Marketplace/components/MarketOrderDialog/components/MarketDroneParams/MarketDroneParams.tsx';
import Paper from '@mui/material/Paper';

type Props = {
  orderGroup: IMarketOrderGroupDto;
  tradeToken: IMarketTradeToken;
  nftType?: INftTypeDto;
  nftTypeLoading?: boolean;
};

function OrderStatsBlock(props: Props) {
  const { orderGroup, tradeToken, nftTypeLoading = false, nftType: itemNftType } = props;

  const { t } = useTranslation('marketplace');

  const tradeTokenDecimals = tradeToken?.decimals || 18;
  const tradeTokenSymbol = tradeToken?.symbol || '';

  const nftType = itemNftType?.recipe
    ? itemNftType.droneRecipe?.resultType || itemNftType
    : itemNftType;
  const nftRarity = orderGroup.nftType.rarity;
  const nftRarityLabel = getNftRarityLabel(nftRarity);
  const rarityColor = getNftRarityColor(nftRarity);
  const nftCollection = orderGroup.nftType.collection?.name || 'unknown';
  const nftSlotLabel = getNftSlotLabel(orderGroup.nftType.slots[0].slot);
  const nftMods = nftType?.mods;

  const recipe = nftType?.droneRecipe;
  const minerDroneType = nftType?.minerDroneType;

  const createdDate = new Date(orderGroup.createdAt).toLocaleString('RU-ru').slice(0, -3);
  const price = orderGroup.minPrice;

  return (
    <Paper variant='outlined' elevation={0} className={styles.info}>
      <Stack direction='column' gap={1} divider={<Divider />}>
        <div className={styles.block}>
          <span className={styles.label}>{t('Rarity')}</span>
          <span style={{ color: rarityColor }}>{uppercaseFirst(nftRarityLabel)}</span>
        </div>

        {nftTypeLoading && <CircularProgress sx={{ mx: 'auto', mt: 5 }} />}

        {!nftTypeLoading && nftMods?.length && <MarketNftParams mods={nftMods} />}

        {!nftTypeLoading && minerDroneType && <MarketDroneParams droneType={minerDroneType} />}
      </Stack>

      <Stack direction='column' gap={1} divider={<Divider />}>
        <div className={styles.block}>
          <span className={styles.label}>{t('Collection')}</span>
          <span>{nftCollection}</span>
        </div>

        <div className={styles.block}>
          <span className={styles.label}>{t('Slot')}</span>
          <span className={styles.statLabel}>{nftSlotLabel}</span>
        </div>

        <div className={styles.block}>
          <span className={styles.label}>{t('Creation day')}</span>
          <span className={styles.statLabel}>{createdDate}</span>
        </div>

        <div className={styles.block}>
          <span className={styles.label}>{t('Min sell price')}</span>
          <span className={styles.statLabel}>
            {(+price).toFixed(2)} <span>{tradeTokenSymbol}</span>/ $
            {+(+price * tradeToken.usdRate).toFixed(2)}
          </span>
        </div>
      </Stack>
    </Paper>
  );
}

export default OrderStatsBlock;
