'use strict';
import React from 'react';
import styles from './ItemOffers.module.scss';
import clsx from 'clsx';
import NftCollectionIcon from 'lib/NFT/components/NftCollectionIcon/NftCollectionIcon';
import BuyCart from 'lib/Marketplace/components/icons/BuyCart';
import EditIcon from 'lib/Marketplace/components/icons/EditIcon';
import { timeToDate } from 'utils/format/date';
import { useUpdateInterval } from 'utils/hooks/useUpdateInterval';

function ItemOffers(props) {
  const { options = [], onEditOffer, onCreateOffer } = props;

  useUpdateInterval(1000);

  const handleEditOffer = (offer) => () => {
    if (onEditOffer) {
      onEditOffer(offer);
    }
  };
  const handleCreateOffer = () => {
    if (onCreateOffer) {
      onCreateOffer();
    }
  };

  return (
    <div className={clsx(styles.root)}>
      <span className={styles.title}>Offers</span>

      <hr />

      <div className={styles.header}>
        <span className={styles.label}>Price</span>
        <span className={styles.label}>Quantity</span>
        <span className={styles.label}>Offer date</span>
      </div>

      <hr />

      <div className={styles.list}>
        {options.map((option, i) => {
          return (
            <div
              key={`listing-option-${i}`}
              className={clsx(styles.row, { [styles.row_my]: option.owned })}
            >
              <div className={styles.rowInfo}>
                <NftCollectionIcon />

                <div>
                  <span>{+option.sgb.toFixed(1)} SGB </span>
                  <span> | ${+option.usd.toFixed(2)}</span>
                </div>
              </div>

              <span className={styles.rowQuantity}>1</span>

              <div className={styles.rowDate}>
                {timeToDate(option.date)}
                {option.owned && (
                  <button
                    className={styles.btnBuy}
                    onClick={handleEditOffer(option)}
                    disabled={!onEditOffer}
                  >
                    <EditIcon />
                  </button>
                )}
              </div>
            </div>
          );
        })}
      </div>

      <button className={styles.btnOffer} onClick={handleCreateOffer}>
        Offer to buy
      </button>
    </div>
  );
}

export default ItemOffers;
