import styles from 'lib/Marketplace/components/MarketOrderDialog/components/OrderStatsBlock/OrderStatsBlock.module.scss';
import {
  getNftStatsValue,
  isNftStatDust,
  isNftStatPercent,
} from 'lib/NFT/utils/getNftStatsValue.ts';
import { getNftStatLabel } from 'lib/NFT/utils/getNftStatLabel';
import StatValue from 'lib/NFT/components/StatValue/StatValue.tsx';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { INftStatDto } from 'lib/NFT/api-server/nft.types.ts';

function MarketNftParams(props: { mods: INftStatDto[] }) {
  const { mods = [] } = props;

  const { t } = useTranslation('marketplace');

  return (
    <div className={styles.block}>
      <span className={styles.label}>{t('Modification')}</span>
      <div className={styles.stats}>
        {mods.map((mod) => {
          const value = getNftStatsValue(mod);
          const isPercent = isNftStatPercent(mod.modId);
          const isDust = isNftStatDust(mod.modId);

          return (
            <div key={`stat-key-${mod.modId}`}>
              <span className={styles.statLabel}>{getNftStatLabel(mod.modId)}</span>
              <StatValue
                value={value || 0}
                percent={isPercent}
                buff={mod.buff}
                prefix={value > 0 ? '+' : ''}
                postfix={isPercent ? '%' : isDust ? 'PXLd ' : ' PXLs'}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default MarketNftParams;
