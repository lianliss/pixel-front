import { getNftStatLabel } from 'lib/NFT/utils/getNftStatLabel';
import { NftItemRarity, NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';

export const STATUS_LIST = [
  { label: 'All', value: '*' },
  { label: 'Listed', value: 'listed' },
  { label: 'On auction', value: 'auction' },
  { label: 'New', value: 'new' },
  { label: 'Has offers', value: 'hasOffers' },
];
export const RARITY_LIST = [
  NftItemRarity.Common,
  NftItemRarity.Epic,
  NftItemRarity.UnCommon,
  NftItemRarity.Legendary,
  NftItemRarity.Rare,
];
export const MODS_LIST = [0, 1, 2, 3, 4, 6, 7, 8, 16, 17, 20].map((v) => ({
  label: getNftStatLabel(v),
  value: v,
}));

export const SLOTS_LIST = [
  NftItemSlot.Artifact,
  NftItemSlot.Belt,
  NftItemSlot.Backpack,
  NftItemSlot.Pocket0,
  NftItemSlot.Drill,
  NftItemSlot.Back,
  NftItemSlot.LoyaltyCard,
  NftItemSlot.Ring,
  NftItemSlot.MinerDrone,
  NftItemSlot.Recipe,
];
