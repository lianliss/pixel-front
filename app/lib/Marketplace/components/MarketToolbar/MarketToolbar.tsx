'use strict';
import React from 'react';
import styles from './MarketToolbar.module.scss';
import FilterIcon from 'lib/Marketplace/components/icons/FilterIcon';
import { Input } from '@ui-kit/Input/Input.tsx';
import SearchIcon from 'lib/Marketplace/components/icons/SearchIcon';
import MarketFilterDialog, {
  IMarketFilterState,
} from 'lib/Marketplace/components/MarketFilterDialog/MarketFilterDialog.tsx';
import HeartIcon from 'lib/Marketplace/components/icons/HeartIcon';
import FavoritesActiveIcon from 'lib/Marketplace/components/icons/FavoritesActiveIcon';
import ImportExportIcon from '@mui/icons-material/ImportExport';
import { Order } from 'lib/Marketplace/api-server/market.types.ts';
import { useTranslation } from 'react-i18next';

type Props = {
  filter: IMarketFilterState;
  onChangeFilter?: (filter: IMarketFilterState) => void;
  search?: string;
  onChangeSearch?: (search: string) => void;
  hideFilter?: boolean;
  hideFavorites?: boolean;
  hideSort?: boolean;
  onlyFavorites?: boolean;
  onChangeOnlyFavorites?: (v: boolean) => void;
  sort?: Order;
  onChangeSort?: (sort: Order) => void;
  loading?: boolean;
};

function MarketToolbar(props: Props) {
  const {
    filter,
    onChangeFilter,
    search,
    onChangeSearch,
    hideFilter,
    hideFavorites,
    onlyFavorites = false,
    onChangeOnlyFavorites,
    sort,
    onChangeSort,
    hideSort = false,
    loading = false,
  } = props;

  const { t } = useTranslation('marketplace');

  const [blockSort, setBlockSort] = React.useState(false);
  const [openFilters, setOpenFilters] = React.useState(false);
  const [isFilterChanged, setIsFilterChanged] = React.useState(false);
  const [isOrderChanged, setIsOrderChanged] = React.useState(false);
  const [localSearch, setLocalSearch] = React.useState('');
  const timeoutRef = React.useRef<number | null>(null);
  const blockTimeoutRef = React.useRef<number | null>(null);

  const handleChangeFilters = (nextFilter: IMarketFilterState, isChanged: boolean) => {
    setOpenFilters(false);
    setIsFilterChanged(isChanged);
    onChangeFilter?.(nextFilter);
  };

  const handleChangeSort = () => {
    const next = sort === Order.Asc ? Order.Desc : Order.Asc;

    setIsOrderChanged(next !== Order.Asc);
    onChangeSort?.(next);
    setBlockSort(true);
    Telegram.WebApp.HapticFeedback.impactOccurred('light');
  };

  const handleChangeSearch = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setLocalSearch(e.target.value);

    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }

    timeoutRef.current = setTimeout(() => {
      if (onChangeSearch) {
        onChangeSearch(e.target.value);
      }
    }, 300);
  };

  const handleFavoriteClick = () => {
    onChangeOnlyFavorites?.(!onlyFavorites);
    Telegram.WebApp.HapticFeedback.impactOccurred('light');
  };

  React.useEffect(() => {
    return () => {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
      }
    };
  }, []);

  React.useEffect(() => {
    if (!blockSort) {
      return () => {};
    }

    setBlockSort(true);

    blockTimeoutRef.current = setTimeout(() => {
      setBlockSort(false);
    }, 2000);

    return () => {
      if (blockTimeoutRef.current) {
        clearTimeout(blockTimeoutRef.current);
      }
    };
  }, [blockSort]);

  React.useEffect(() => {
    setLocalSearch(search);
  }, [search]);

  return (
    <div className={styles.root} data-filter={openFilters.toString()}>
      <div className={styles.toolbar}>
        {!hideFilter && (
          <button
            className={styles.btn}
            onClick={() => {
              Telegram.WebApp.HapticFeedback.impactOccurred('light');
              setOpenFilters(true);
            }}
          >
            <FilterIcon />
            {isFilterChanged && <div className={styles.changedBadge} />}
          </button>
        )}

        <Input
          placeholder={t('Search NFT')}
          startAdornment={<SearchIcon />}
          value={localSearch}
          onChange={handleChangeSearch}
          fullWidth
        />

        {!hideFavorites && (
          <button className={styles.btn} onClick={handleFavoriteClick} disabled={loading}>
            {!onlyFavorites ? <HeartIcon /> : <FavoritesActiveIcon />}
          </button>
        )}
        {false && (
          <button className={styles.btn} onClick={() => setOpenFilters(true)}>
            <FilterIcon />
          </button>
        )}
        {!hideSort && (
          <button className={styles.btn} onClick={handleChangeSort} disabled={loading || blockSort}>
            <ImportExportIcon style={{ transform: 'scale(1.2)' }} />
            {isOrderChanged && <div className={styles.changedBadge} />}
          </button>
        )}
      </div>

      {openFilters && (
        <MarketFilterDialog
          onClose={() => setOpenFilters(false)}
          value={filter}
          onChange={handleChangeFilters}
        />
      )}
    </div>
  );
}

export default MarketToolbar;
