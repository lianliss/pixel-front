'use strict';
import React from 'react';
import styles from './MarketSellDialog.module.scss';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { formatNumberSpacing } from 'utils/number';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import { ITokenData } from '@chain/hooks/useToken.ts';
import Alert from '@mui/material/Alert';
import { useTranslation } from 'react-i18next';
import BuyTokenAlertLazy from 'lib/TelegramStars/components/BuyTokenAlert/BuyTokenAlert.lazy.tsx';
import { SHOW_NO_GAS_TOKEN_ALERT, SHOW_NO_TRADE_TOKEN_ALERT } from '@cfg/app.ts';
import BuyGasAlertLazy from 'lib/TelegramStars/components/BuyGasAlert/BuyGasAlert.lazy.tsx';
import { useNftItem } from 'lib/NFT/hooks/useNftItem.ts';
import { getMarketUserSellFee } from 'lib/Marketplace/utils/get-market-user-sell-fee.ts';
import { IMarketOrderDto } from 'lib/Marketplace/api-server/market.types.ts';
import MarketOrders from 'lib/Marketplace/components/MarketOrderDialog/components/MarketOrders/MarketOrders.tsx';
import Skeleton from '@mui/material/Skeleton';

type Props = {
  onSell?: (nft: INftItemDto, price: number) => void;
  onClose?: () => void;
  item: INftItemDto;
  fee: number | null;
  publicationFee: number | null;
  tradeToken?: ITokenData;
  saleDiscount?: number;
  publicationDiscount?: number;
  noGasBalance?: boolean;
  balance?: number;
  userMod?: number;
  // orders
  recommendedPrice?: number;
  recommendedPriceLoading?: boolean;
  isPending?: boolean;
};

function MarketSellDialog(props: Props) {
  const {
    onSell,
    onClose,
    item,
    fee: rawFee,
    publicationFee: rawPublicationFee,
    tradeToken,
    saleDiscount = 1,
    publicationDiscount = 1,
    noGasBalance = false,
    balance,
    userMod = 0,
    recommendedPrice,
    recommendedPriceLoading = false,
    isPending = false,
  } = props;

  const symbol = tradeToken?.symbol || '-';

  const { t } = useTranslation('inventory');
  const nftItemState = useNftItem({
    assetId: item.assetId,
    contractId: item.contractId,
  });

  const [value, setValue] = React.useState('');
  const price = +value;

  const fee = getMarketUserSellFee({ userMod: userMod, fee: rawFee, slotMod: saleDiscount });
  const publicationFee = Number(rawPublicationFee) * publicationDiscount;
  const feePrice = React.useMemo(() => {
    return price * Number(fee);
  }, [price, fee]);
  const publicationFeePrice = React.useMemo(() => {
    return price * publicationFee;
  }, [price, publicationFee]);
  const noFeeBalance =
    !balance || (publicationFee && typeof balance === 'number' && publicationFeePrice > balance);

  const handleSell = (e) => {
    e.stopPropagation();

    if (onSell) {
      onSell(item, price);
    }
  };

  const isInvalid = Number.isNaN(price) || !price;
  const btnText = noFeeBalance
    ? t('Fee balance to low')
    : noGasBalance
    ? t('No gas')
    : price
    ? `${t('Apply for')} ${price} ${tradeToken.symbol}`
    : t('Sell Item');

  return (
    <BottomDialog open title={t('Do you want to sell your item?')} onClose={onClose}>
      <div className={styles.root}>
        {/*<h2 className={styles.title}>{t('Do you want to sell your item?')}</h2>*/}
        {/*<p className={styles.caption}>*/}
        {/*  Do you want to buy an additional pocket for an artifact that will help you in extracting pixels*/}
        {/*</p>*/}

        <div className={styles.input}>
          <Typography color='text.primary' className={styles.label}>
            {t('Price')}
          </Typography>

          <TextField
            placeholder={t('Enter amount')}
            type='number'
            value={value}
            fullWidth
            onChange={(e) => setValue(e.target.value)}
            onWheel={(e) => e.target.blur()}
            InputProps={{
              startAdornment: <div style={{ paddingRight: 8 }}>{symbol}</div>,
            }}
            helperText={
              <div style={{ display: 'flex', flexDirection: 'column' }}>
                {publicationFeePrice ? (
                  <span>{`${t('Publication fee')} ${publicationFee * 100}%: ${formatNumberSpacing(
                    +publicationFeePrice.toFixed(2)
                  )} ${symbol}`}</span>
                ) : undefined}
                {feePrice ? (
                  <span>{`${t('Commission')} ${+(fee * 100).toFixed(2)}%: ${formatNumberSpacing(
                    +feePrice.toFixed(2)
                  )} ${symbol}`}</span>
                ) : undefined}
              </div>
            }
          />

          {userMod > 1 && (
            <Alert variant='outlined' severity='warning'>
              Marketplace fees raised to {fee * 100}%
            </Alert>
          )}

          {((noFeeBalance && !noGasBalance) || SHOW_NO_TRADE_TOKEN_ALERT) && (
            <BuyTokenAlertLazy variant='filled' text={t('Not enough', { symbol })} />
          )}
          {(noGasBalance || SHOW_NO_GAS_TOKEN_ALERT) && (
            <BuyGasAlertLazy
              variant='filled'
              text={t('Not enough gas for the lot on the market.')}
            />
          )}
        </div>

        <div className={styles.nft}>
          <NFTCard
            nft={nftItemState.loading ? item : nftItemState.state || item}
            loading={nftItemState.loading}
          />
        </div>

        <div style={{ marginBottom: 16 }}>
          <Typography
            align='center'
            fontWeight='bold'
            color='text.secondary'
            variant='subtitle2'
            sx={{ display: 'flex', gap: '4px', justifyContent: 'center' }}
          >
            Recommended Price
            <Typography
              component='span'
              variant='inherit'
              color='text.primary'
              sx={{
                display: 'flex',
                gap: '4px',
                textDecoration: recommendedPrice ? 'underline' : undefined,
              }}
              onClick={recommendedPrice ? () => setValue(recommendedPrice.toString()) : undefined}
            >
              {recommendedPriceLoading ? <Skeleton sx={{ width: 30 }} /> : recommendedPrice || '-'}{' '}
              {symbol}
            </Typography>
          </Typography>
        </div>

        <Button
          variant='contained'
          color='primary'
          size='large'
          fullWidth
          onClick={handleSell}
          disabled={!onSell || !item || isInvalid || noGasBalance || noFeeBalance}
          loading={isPending}
        >
          {btnText}
        </Button>
      </div>
    </BottomDialog>
  );
}

export default React.memo(MarketSellDialog);
