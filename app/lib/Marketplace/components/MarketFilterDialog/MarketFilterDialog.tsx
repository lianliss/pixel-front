'use strict';
import React from 'react';
import styles from './MarketFilterDialog.module.scss';
import clsx from 'clsx';
import NftTypeIcon from 'lib/NFT/components/NftTypeIcon/NftTypeIcon.tsx';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import {
  MODS_LIST,
  RARITY_LIST,
  SLOTS_LIST,
  STATUS_LIST,
} from 'lib/Marketplace/components/MarketToolbar/data.ts';
// import TextField from 'ui/TextField';
import Divider from 'ui/Divider';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useTradeToken } from '@chain/hooks/useTradeToken.ts';
import { NftItemRarity, NftItemSlot } from 'lib/NFT/api-server/nft.types.ts';
import { getNftRarityLabel } from 'lib/NFT/utils/getNftRarityLabel.ts';
import { getNftSlotLabel } from 'lib/NFT/utils/getNftSlotLabel.ts';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Checkbox } from '@ui-kit/Checkbox/Checkbox.tsx';
import { useTranslation } from 'react-i18next';

export type IMarketFilterState = {
  status: string;
  rarities?: NftItemRarity[];
  slots?: NftItemSlot[];
  mods?: string[];
  minPrice?: number;
  maxPrice?: number;
};

type Props = {
  onClose?: () => void;
  value: IMarketFilterState;
  onChange?: (value: IMarketFilterState, reset: boolean) => void;
};

function MarketFilterDialog(props: Props) {
  const { onClose, onChange, value } = props;

  const { t } = useTranslation('marketplace');

  const tradeToken = useTradeToken();

  const [status, setStatus] = React.useState<string>(STATUS_LIST[0].value);
  const [rarities, setRarities] = React.useState<NftItemRarity[]>([]);
  const [slots, setSlots] = React.useState<NftItemSlot[]>([]);
  const [mods, setMods] = React.useState<string[]>([]);
  const [minPrice, setMinPrice] = React.useState<string>('');
  const [maxPrice, setMaxPrice] = React.useState<string>('');

  const handleChangeRarity = (rarity) => () => {
    if (rarities.includes(rarity)) {
      setRarities([...rarities.filter((el) => el !== rarity)]);
    } else {
      setRarities([...rarities, rarity]);
    }
  };
  const handleChangeSlot = (slot) => () => {
    if (slots.includes(slot)) {
      setSlots(slots.filter((el) => el !== slot));
    } else {
      setSlots([...slots, slot]);
    }
  };
  const handleChangeMod = (mod) => () => {
    if (mods.includes(mod)) {
      setMods(mods.filter((el) => el !== mod));
    } else {
      setMods([...mods, mod]);
    }
  };

  const handleChangeMinPrice = (e) => {
    if (Number.isNaN(+e.target.value)) {
      return;
    }

    setMinPrice(e.target.value);
  };
  const handleChangeMaxPrice = (e) => {
    if (Number.isNaN(+e.target.value)) {
      return;
    }

    setMaxPrice(e.target.value);
  };

  const handleClear = () => {
    onChange(
      {
        status: STATUS_LIST[0].value,
        slots: [],
        mods: [],
        rarities: [],
      },
      false
    );
  };
  const handleDone = () => {
    if (!onChange) {
      return;
    }

    onChange(
      {
        status,
        slots,
        mods,
        rarities,
        minPrice: minPrice ? +minPrice : undefined,
        maxPrice: maxPrice ? +maxPrice : undefined,
      },
      true
    );
    Telegram.WebApp.HapticFeedback.impactOccurred('medium');
  };
  const handleApplyPrice = () => {
    handleDone();
  };

  React.useEffect(() => {
    setStatus(value.status);
    setRarities(value.rarities);
    setSlots(value.slots);
    setMods(value.mods);
    setMinPrice(typeof value.minPrice === 'number' ? value.minPrice.toString() : '');
    setMaxPrice(typeof value.maxPrice === 'number' ? value.maxPrice.toString() : '');
  }, [value]);

  return (
    <BottomDialog
      isOpen
      onClose={onClose}
      PaperProps={{ className: styles.dialog }}
      footer={
        <div className={styles.btns}>
          <Button size='large' variant='outlined' onClick={handleClear}>
            {t('Clear All')}
          </Button>
          <Button size='large' variant='contained' onClick={handleDone}>
            {t('Done')}
          </Button>
        </div>
      }
    >
      <div className={styles.root}>
        {false && (
          <div>
            <h2 className={styles.title}>Status</h2>
            <div className={styles.statusBox}>
              {STATUS_LIST.map((option) => {
                return (
                  <button
                    key={`filter-status-${option.value}`}
                    className={clsx(styles.statusBtn, {
                      [styles.statusBtn_selected]: option.value === status,
                    })}
                    onClick={() => setStatus(option.value)}
                  >
                    {t(option.label)}
                  </button>
                );
              })}
            </div>
          </div>
        )}

        {false && <Divider />}

        <div>
          <Typography className={styles.title} fontWeight='bold' variant='body1'>
            {t('Price')} {tradeToken.symbol}
          </Typography>

          {/*<Select options={[{ label: 'SGB', value: 'sgb' }]} style={{ marginBottom: 16 }} />*/}

          <div className={styles.priceMinMax}>
            <TextField
              type='number'
              placeholder={t('Min')}
              min={1}
              size='small'
              value={minPrice}
              onChange={handleChangeMinPrice}
            />
            <span>to</span>
            <TextField
              type='number'
              placeholder={t('Max')}
              size='small'
              max={1_000_000_000}
              value={maxPrice}
              onChange={handleChangeMaxPrice}
            />
          </div>
        </div>

        <Divider />

        <div>
          <Typography className={styles.title} variant='body1' fontWeight='bold'>
            {t('Slot')}
          </Typography>

          <div className={clsx(styles.checkboxes, styles.checkboxes_2cols)}>
            {SLOTS_LIST.map((slot) => (
              <div className={styles.checkbox__wrapper} key={`filter-slot-${slot}`}>
                <Checkbox
                  checked={slots.includes(slot)}
                  className={styles.checkbox}
                  onChange={handleChangeSlot(slot)}
                />
                <div className={styles.checkbox__label}>
                  <NftTypeIcon slot={slot} compact size='small' /> {getNftSlotLabel(slot)}
                </div>
              </div>
            ))}
          </div>
        </div>

        <Divider />

        <div>
          <Typography className={styles.title} variant='body1' fontWeight='bold'>
            {t('Rarity')}
          </Typography>

          <div className={clsx(styles.checkboxes, styles.checkboxes_2cols)}>
            {RARITY_LIST.map((rarity) => (
              <div className={styles.checkbox__wrapper} key={`filter-rarity-${rarity}`}>
                <Checkbox
                  style={{ color: getNftRarityColor(rarity) }}
                  onChange={handleChangeRarity(rarity)}
                  checked={rarities.includes(rarity)}
                />
                <div className={styles.checkbox__label}>{getNftRarityLabel(rarity)}</div>
              </div>
            ))}
          </div>
        </div>

        <Divider />

        <div>
          <Typography className={styles.title} variant='body1' fontWeight='bold'>
            {t('Modifiers')}
          </Typography>

          <div className={styles.checkboxes}>
            {MODS_LIST.map((mod) => (
              <div className={styles.checkbox__wrapper} key={`filter-mod-${mod}`}>
                <Checkbox onClick={handleChangeMod(mod.value)} checked={mods.includes(mod.value)} />
                <div className={styles.checkbox__label}> {t(mod.label)} </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </BottomDialog>
  );
}

export default React.memo(MarketFilterDialog);
