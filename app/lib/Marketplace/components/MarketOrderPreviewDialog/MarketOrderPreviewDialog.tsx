'use strict';
import React from 'react';
import styles from './MarketOrderPreviewDialog.module.scss';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { IMarketOrderDto } from 'lib/Marketplace/api-server/market.types.ts';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';

type Props = {
  onClose?: () => void;
  order: IMarketOrderDto;
  tradeToken: IMarketTradeToken;
};

function MarketOrderPreviewDialog(props: Props) {
  const { onClose, order, tradeToken } = props;

  const price = order.price;
  const symbol = tradeToken?.symbol || '';

  return (
    <BottomDialog isOpen onClose={onClose}>
      <div className={styles.root}>
        <h2 className={styles.title}>Order preview</h2>

        <div className={styles.nft}>
          <NFTCard nft={order.nft} />
        </div>

        <div className={styles.priceBox}>
          <span className={styles.priceLabel}>Price</span>
          <span className={styles.prices}>
            <span>
              {price} {symbol}
            </span>
            <p className={styles.priceUsd}>${(+price * tradeToken.usdRate).toFixed(2)}</p>
          </span>
        </div>
      </div>
    </BottomDialog>
  );
}

export default MarketOrderPreviewDialog;
