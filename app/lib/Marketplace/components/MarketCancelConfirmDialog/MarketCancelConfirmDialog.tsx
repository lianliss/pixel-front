'use strict';
import React from 'react';
import styles from './MarketCancelConfirmDialog.module.scss';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { IMarketOrderDto } from 'lib/Marketplace/api-server/market.types.ts';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useTranslation } from 'react-i18next';
import { useNftItem } from 'lib/NFT/hooks/useNftItem.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

type Props = {
  onCancel?: (order: IMarketOrderDto) => void;
  onClose?: () => void;
  order: IMarketOrderDto;
  tradeToken: IMarketTradeToken;
  isPending?: boolean;
};

function MarketCancelConfirmDialog(props: Props) {
  const { onCancel, onClose, order, tradeToken, isPending = false } = props;

  const { t } = useTranslation('marketplace');
  const nftItemState = useNftItem({
    assetId: order.assetId,
    contractId: order.nftAddress,
  });

  const handleCancel = () => {
    if (onCancel) {
      onCancel(order);
    }
  };

  const price = order.price;
  const symbol = tradeToken?.symbol || '';

  return (
    <BottomDialog isOpen onClose={onClose}>
      <div className={styles.root}>
        <h2 className={styles.title}>{t('Cancel selling order')}</h2>
        <p className={styles.caption}>{t('Do you want to cancel selling order')}</p>

        <div className={styles.nft}>
          <NFTCard
            nft={nftItemState.loading ? order.nft : nftItemState.state || order.nft}
            loading={nftItemState.loading}
          />
        </div>

        <div className={styles.priceBox}>
          <span className={styles.priceLabel}>Price</span>
          <span className={styles.prices}>
            <span>
              {price} {symbol}
            </span>
            <p className={styles.priceUsd}>${(+price * tradeToken.usdRate).toFixed(2)}</p>
          </span>

          <Typography
            color='text.primary'
            variant='caption'
            sx={{ opacity: 0.5 }}
            component='p'
            align='center'
          >
            Token ID: {order.assetId}
          </Typography>
        </div>

        <Button
          className={styles.button}
          variant='contained'
          color='primary'
          size='large'
          fullWidth
          onClick={handleCancel}
          disabled={!onCancel || !order}
          loading={isPending}
        >
          {t('Cancel order')}
        </Button>
      </div>
    </BottomDialog>
  );
}

export default MarketCancelConfirmDialog;
