'use strict';
import React from 'react';
import styles from './MarketSuccessBuyDialog.module.scss';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { useNavigate } from 'react-router-dom';
import { Button } from '@ui-kit/Button/Button.tsx';
import { IMarketOrderDto } from 'lib/Marketplace/api-server/market.types.ts';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import { useTranslation } from 'react-i18next';
import { useNftItem } from 'lib/NFT/hooks/useNftItem.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

type Props = { onClose?: () => void; order: IMarketOrderDto };

function MarketSuccessBuyDialog(props: Props) {
  const { onClose, order } = props;

  const { t } = useTranslation('marketplace');
  const nftItemState = useNftItem({
    assetId: order.assetId,
    contractId: order.nftAddress,
  });

  const navigate = useNavigate();
  const redirectToInventory = () => {
    navigate('/inventory');
  };
  const redirectToMarket = () => {
    navigate('/nft-market');
  };

  return (
    <BottomDialog isOpen onClose={onClose}>
      <div className={styles.root}>
        <Typography color='text.primary' fontWeight='bold' className={styles.title}>
          {t('Congratulations')}
        </Typography>
        <Typography color='text.primary' variant='subtitle2' className={styles.caption}>
          {t('You bought an NFT')}
        </Typography>
        <Typography color='text.primary' variant='subtitle2' className={styles.caption2}>
          {t('You can find it in Inventory')}
        </Typography>

        <div className={styles.nft} style={{ marginTop: 12 }}>
          <NFTCard
            nft={nftItemState.loading ? order.nft : nftItemState.state || order.nft}
            loading={nftItemState.loading}
          />
        </div>

        <Button
          size='large'
          variant='contained'
          className={styles.button}
          onClick={onClose}
          fullWidth
        >
          {t('Back to Marketplace')}
        </Button>

        <Button
          size='large'
          variant='outlined'
          style={{ marginTop: 10 }}
          onClick={redirectToInventory}
          fullWidth
        >
          {t('Open inventory')}
        </Button>
      </div>
    </BottomDialog>
  );
}

export default MarketSuccessBuyDialog;
