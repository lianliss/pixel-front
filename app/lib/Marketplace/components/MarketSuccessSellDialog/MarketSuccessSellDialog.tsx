'use strict';
import React from 'react';
import styles from './MarketSuccessSellDialog.module.scss';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { INftItemDto } from 'lib/NFT/api-server/nft.types.ts';
import NFTCard from 'lib/NFT/components/NFTCard/NFTCard.tsx';
import { useTranslation } from 'react-i18next';
import { useNftItem } from 'lib/NFT/hooks/useNftItem.ts';

type Props = { onClose?: () => void; nft: INftItemDto };

function MarketSuccessSellDialog(props: Props) {
  const { onClose, nft } = props;

  const { t } = useTranslation('inventory');
  const nftItemState = useNftItem({
    assetId: nft.assetId,
    contractId: nft.contractId,
  });

  return (
    <BottomDialog isOpen onClose={onClose}>
      <div className={styles.root}>
        <h2 className={styles.title}>{t('Congratulations')}</h2>
        <p className={styles.caption}>{t('You have listed an item for sale!')}</p>

        <div className={styles.nft}>
          <NFTCard
            nft={nftItemState.loading ? nft : nftItemState.state || nft}
            loading={nftItemState.loading}
          />
        </div>

        <Button
          size='large'
          variant='contained'
          className={styles.button}
          onClick={onClose}
          fullWidth
        >
          {t('Back to marketplace')}
        </Button>
      </div>
    </BottomDialog>
  );
}

export default React.memo(MarketSuccessSellDialog);
