'use strict';
import React from 'react';
import styles from './BalanceToolbar.module.scss';
import clsx from 'clsx';
import getFinePrice from 'utils/getFinePrice';
import { useMarketStats } from 'lib/Dashboard/hooks/useMarketStats.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { formatNumber } from 'utils/number';
import Skeleton from '@mui/material/Skeleton';
import { IUserBalance } from '@chain/hooks/useBalance.ts';
import { ITokenData } from '@chain/hooks/useToken.ts';
import { darken, alpha, styled } from '@mui/material/styles';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain, SKALE_TEST } from 'services/multichain/chains';
import { useTranslation } from 'react-i18next';

const StyledRoot = styled('div')(({ theme }) => ({
  border: 'solid 1px',
  backgroundColor: alpha(theme.palette.primary.main, 0.15),
  borderColor: alpha(theme.palette.primary.main, 0.4),

  [`&.chain${Chain.SKALE_TEST}`]: {
    backgroundColor: alpha(theme.palette.primary.main, 0.05),
    borderColor: alpha(theme.palette.primary.main, 0.3),
  },
  [`&.chain${Chain.SKALE}`]: {
    backgroundColor: alpha(theme.palette.primary.main, 0.05),
    borderColor: alpha(theme.palette.primary.main, 0.3),
  },
}));

type Props = { className?: string; balance: IUserBalance | null; tradeToken?: ITokenData };

function BalanceToolbar(props: Props) {
  const { className, balance, tradeToken, ...other } = props;

  const { t } = useTranslation('marketplace');

  const { chainId } = useAccount();
  const { data, loading } = useMarketStats();

  return (
    <StyledRoot className={clsx(styles.root, { [`chain${chainId}`]: true }, className)} {...other}>
      <div className={styles.top}>
        <Typography variant='h6' component='span' color='text.secondary'>
          {t('Balance')}:{' '}
        </Typography>
        <Typography variant='h6' color='text.primary' component='span' className={styles.value}>
          {balance ? getFinePrice(balance.formatted) : 0}
        </Typography>
        <Typography variant='h6' color='text.primary' component='span' className={styles.prefix}>
          {tradeToken?.symbol}
        </Typography>
      </div>

      <div className={styles.bottom}>
        <Typography component='span'>
          <Typography variant='subtitle2' component='span' color='textSecondary'>
            {t('Orders')}:{' '}
          </Typography>
          <Typography
            variant='subtitle2'
            component='span'
            fontWeight='bold'
            color='text.primary'
            sx={{ display: 'inline-flex', alignItems: 'center' }}
          >
            {loading ? (
              <Skeleton variant='text' component='span' width={40} sx={{ fontSize: '0.8rem' }} />
            ) : (
              data?.currentOrders || 0
            )}
          </Typography>
        </Typography>

        <Typography component='span'>
          <Typography variant='subtitle2' component='span' color='textSecondary'>
            {t('24h volume:')}{' '}
          </Typography>
          <Typography
            variant='subtitle2'
            component='span'
            fontWeight='bold'
            color='text.primary'
            sx={{ display: 'inline-flex', alignItems: 'center', gap: '2px' }}
          >
            {loading ? (
              <Skeleton variant='text' component='span' width={40} sx={{ fontSize: '0.8rem' }} />
            ) : (
              formatNumber(data?.lastDayTradeVolume || 0)
            )}
            <Typography
              variant='subtitle2'
              component='span'
              fontWeight='bold'
              color='text.primary'
              className={styles.prefix}
            >
              {tradeToken?.symbol}
            </Typography>
          </Typography>
        </Typography>
      </div>
    </StyledRoot>
  );
}

export default BalanceToolbar;
