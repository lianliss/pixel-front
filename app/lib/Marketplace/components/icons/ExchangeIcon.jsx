import React from 'react';

function HeartIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='20' height='20' viewBox='0 0 20 20' fill='none'>
      <path
        d='M19 10C19 7.3345 17.8412 4.93964 16 3.29168C14.4077 1.86656 12.3051 1 10 1C7.69494 1 5.59227 1.86656 4 3.29168M1 10C1 14.9706 5.02944 19 10 19C12.3051 19 14.4077 18.1334 16 16.7083'
        stroke='#4E99DE'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
}

export default HeartIcon;
