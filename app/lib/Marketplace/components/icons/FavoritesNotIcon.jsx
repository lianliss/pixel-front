import React from 'react';

function FavoritesNotIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='18' height='16' viewBox='0 0 18 16' fill='none'>
      <path
        opacity='0.3'
        d='M17 5.38873C17 11.7963 9 15 9 15C9 15 1 11.7963 1 5.38873C1 0.965265 5.8 -1.01874 9 3.7868C12.2 -1.01874 17 0.965265 17 5.38873Z'
        stroke='white'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
}

export default FavoritesNotIcon;
