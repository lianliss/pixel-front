import React from 'react';

function FilterIcon() {
  return (
    <svg width='26' height='14' viewBox='0 0 26 14' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M1 1H25'
        stroke='#4E99DE'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M5 7H21'
        stroke='#4E99DE'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M9 13H17'
        stroke='#4E99DE'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
}

export default FilterIcon;
