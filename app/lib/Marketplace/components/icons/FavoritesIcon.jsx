import React from 'react';

function FavoritesIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 16 16' fill='none'>
      <path
        d='M16 5.38873C16 11.7963 8 15 8 15C8 15 0 11.7963 0 5.38873C0 0.965265 4.8 -1.01874 8 3.7868C11.2 -1.01874 16 0.965265 16 5.38873Z'
        fill='#8527D4'
      />
    </svg>
  );
}

export default FavoritesIcon;
