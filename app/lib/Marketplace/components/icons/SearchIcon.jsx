import React from 'react';

function SearchIcon() {
  return (
    <svg width='18' height='18' viewBox='0 0 18 18' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <circle
        cx='7.5'
        cy='7.5'
        r='6.5'
        stroke='#4E99DE'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M13 13L17 17'
        stroke='#4E99DE'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
}

export default SearchIcon;
