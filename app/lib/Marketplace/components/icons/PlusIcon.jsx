import React from 'react';

function PlusIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='46' height='46' viewBox='0 0 46 46' fill='none'>
      <rect x='22' width='2' height='46' rx='1' fill='#07AA34' />
      <rect
        y='24'
        width='2'
        height='46'
        rx='0.999999'
        transform='rotate(-90 0 24)'
        fill='#07AA34'
      />
    </svg>
  );
}

export default PlusIcon;
