import React from 'react';

function EditIcon() {
  return (
    <svg width='22' height='22' viewBox='0 0 22 22' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M14.6041 3.68567L18.3143 7.39588M2.23674 16.053L1 21L5.94695 19.7633L20.2758 5.43441C20.7395 4.97057 21 4.34154 21 3.68567C21 3.02979 20.7395 2.40076 20.2758 1.93692L20.0631 1.7242C19.5992 1.2605 18.9702 1 18.3143 1C17.6585 1 17.0294 1.2605 16.5656 1.7242L2.23674 16.053Z'
        stroke='white'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
}

export default EditIcon;
