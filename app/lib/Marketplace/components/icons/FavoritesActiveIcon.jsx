import React from 'react';

function FavoritesActiveIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='22' height='20' viewBox='0 0 22 20' fill='none'>
      <path
        d='M21 6.64265C21 14.881 11 19 11 19C11 19 1 14.881 1 6.64265C1 0.95534 7 -1.59552 11 4.58302C15 -1.59552 21 0.95534 21 6.64265Z'
        fill='#4E99DE'
        stroke='#4E99DE'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
}

export default FavoritesActiveIcon;
