import React from 'react';

export default function MarketIcon() {
  return (
    <svg
      width='110'
      height='120'
      viewBox='0 0 110 120'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M55.2452 23.6911L86.6885 41.845V78.1526L55.2452 96.3064L23.8018 78.1526V41.845L55.2452 23.6911Z'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M32.1602 72.826V47.1739L43.7036 79.2391V40.7608'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M52.6816 83.0869V34.3478L60.3773 38.1956'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M52.6816 59.9999L60.3773 59.9999'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M68.0742 43.326L78.3351 49.7391'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M73.2031 47.1739V75.3912'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M93.8217 37.3088L85.8794 41.9808M16.668 82.6934C19.3489 81.1164 21.9129 79.6081 24.3833 78.1549'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M16.6665 37.3088L24.3819 41.8473M93.8203 82.6934C91.1398 81.1166 88.5757 79.6083 86.1049 78.1549'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M55.248 14.6152C55.248 17.8715 55.248 20.8649 55.248 23.6921M55.248 105.384V96.3074'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <circle
        cx='55.2452'
        cy='7.80769'
        r='6.80769'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <circle
        cx='100.448'
        cy='33.905'
        r='6.80769'
        transform='rotate(60 100.448 33.905)'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <circle
        cx='100.443'
        cy='86.0972'
        r='6.80769'
        transform='rotate(120 100.443 86.0972)'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <circle
        cx='55.2452'
        cy='112.192'
        r='6.80769'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <circle
        cx='10.0434'
        cy='86.0961'
        r='6.80769'
        transform='rotate(60 10.0434 86.0961)'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <circle
        cx='10.0443'
        cy='33.9036'
        r='6.80769'
        transform='rotate(120 10.0443 33.9036)'
        stroke='currentcolor'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
}
