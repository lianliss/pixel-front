'use strict';
import React from 'react';
import styles from './MarketOfferDialog.module.scss';
import WalletPopup from 'ui/WalletPopup/WalletPopup';
import { IMarketOrderGroupDto } from 'lib/Marketplace/api-server/market.types.ts';
import { useTranslation } from 'react-i18next';

type Props = {
  onCreate?: (nft: IMarketOrderGroupDto, price: number) => void;
  onEdit?: (nft: IMarketOrderGroupDto, offer: unknown, price: number) => void;
  onClose?: () => void;
  item: IMarketOrderGroupDto;
  offer: any;
};

function MarketOfferDialog(props: Props) {
  const { onCreate, onEdit, onClose, item, offer } = props;

  const { t } = useTranslation('marketplace');

  const [value, setValue] = React.useState(offer?.sgb || '');

  const handleCreateOffer = () => {
    if (onCreate && item) {
      onCreate(item, value);
    }
  };
  const handleEditOffer = () => {
    if (onEdit && offer && item) {
      onEdit(item, offer, value);
    }
  };

  const invalid = !value.length || Number.isNaN(+value) || +value === 0;

  return (
    <WalletPopup onClose={onClose}>
      <div className={styles.root}>
        <h2 className={styles.title}>{t('Offer to buy')}</h2>
        <p className={styles.caption}>
          {t('You can create offer to buy this item with your price')}
        </p>

        <p className={styles.priceLabel}>{t('Price SGB')}</p>

        <input
          className={styles.input}
          placeholder={t('Enter amount')}
          type='number'
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />

        <button
          className={styles.btnApply}
          onClick={handleCreateOffer}
          disabled={!onCreate || !item || invalid}
        >
          {t('Apply')}
        </button>

        {offer && (
          <>
            <hr className={styles.cancelDivider} />
            <button
              className={styles.btnCancel}
              disabled={!onEdit || !item || invalid}
              onClick={handleEditOffer}
            >
              {t('Cancel offer')}
            </button>
          </>
        )}
      </div>
    </WalletPopup>
  );
}

export default MarketOfferDialog;
