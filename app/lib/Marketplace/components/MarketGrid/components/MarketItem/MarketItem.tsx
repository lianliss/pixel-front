'use strict';
import React, { useEffect, useState } from 'react';
import styles from './MarketItem.module.scss';
import NftTypeIcon from 'lib/NFT/components/NftTypeIcon/NftTypeIcon.tsx';
import { getNftRarityColor } from 'lib/NFT/utils/getNftRarityColor.ts';
import NftCollectionIcon from 'lib/NFT/components/NftCollectionIcon/NftCollectionIcon';
import clsx from 'clsx';
import { IMarketOrderGroupDto } from 'lib/Marketplace/api-server/market.types.ts';
import { RarityChip } from 'lib/NFT/components/RarityChip/index.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import FavoritesIcon from 'lib/Marketplace/components/icons/FavoritesIcon';
import FavoritesNotIcon from 'lib/Marketplace/components/icons/FavoritesNotIcon';
import uriTransform from 'utils/uriTransform';
import axios from 'axios';
import { convertToUsdPrice } from 'lib/Marketplace/utils.ts';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';
import Paper from '@mui/material/Paper';

type Props = {
  data: IMarketOrderGroupDto;
  tradeToken: IMarketTradeToken;
  onFavorite?: (groupId: string, favorite: boolean) => void;
  hideFavorites?: boolean;
  isOwned?: boolean;
} & React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;

function MarketItem(props: Props) {
  const { data, className, tradeToken, onFavorite, hideFavorites, isOwned, ...other } = props;

  const symbol = tradeToken?.symbol || '';

  const price = data.minPrice;

  const nftSlots = data.nftType.slots;
  const nftRarity = data.nftType.rarity;
  const nftName = data.nftType.name;
  const rarityColor = getNftRarityColor(nftRarity);

  const handleAddToFavorites = (e) => {
    e.stopPropagation();
    onFavorite?.(data.id, !data.favorite);
  };

  return (
    <Paper elevation={0} square className={clsx(styles.marketItem, className)} {...other}>
      {isOwned && <div className={styles.marketItem_owned} />}

      {!hideFavorites && (
        <button className={styles.favorites__btn} onClick={handleAddToFavorites}>
          {data.favorite ? <FavoritesIcon /> : <FavoritesNotIcon />}
        </button>
      )}

      <div className={styles.marketItem__preview}>
        <img src={uriTransform(data.nftType.uri)} alt='' className={styles.marketItem__image} />
      </div>

      <div className={styles.marketItem__dividerBox}>
        <hr className={styles.marketItem__divider} style={{ borderColor: rarityColor }} />
        <RarityChip className={styles.marketItem__rarity} rarity={nftRarity} />
        <NftTypeIcon
          className={styles.marketItem__type}
          rarity={nftRarity}
          slot={nftSlots[0].slot}
        />
      </div>

      <div className={styles.marketItem__content}>
        <p className={styles.marketItem__title}>{nftName}</p>

        <div className={styles.marketItem__infoPrices}>
          <NftCollectionIcon collectionId={data.nftType?.collectionId} />

          <div className={styles.marketItem__priceBox}>
            <Typography className={styles.marketItem__priceSgb} variant='caption' noWrap>
              {+price.toFixed(2)} <span>{symbol}</span>
            </Typography>
            <p className={styles.marketItem__priceUsd}>
              ${(+price * tradeToken.usdRate).toFixed(2)}
            </p>
          </div>
        </div>
      </div>
    </Paper>
  );
}

export default MarketItem;
