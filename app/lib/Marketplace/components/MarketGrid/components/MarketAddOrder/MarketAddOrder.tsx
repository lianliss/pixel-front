'use strict';
import React from 'react';
import styles from './MarketAddOrder.module.scss';
import PlusIcon from 'lib/Marketplace/components/icons/PlusIcon';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Paper from '@mui/material/Paper';
import { lighten, styled } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';

type Props = {
  onClick?: (o) => void;
};

function MarketAddOrder(props: Props) {
  const { onClick } = props;

  const { t } = useTranslation('marketplace');

  return (
    <Paper elevation={0} square className={styles.root} onClick={onClick}>
      <div className={styles.addItem__inner}>
        <div className={styles.inner__border}>
          <PlusIcon />
          <Typography variant='subtitle2' align='center' sx={{ color: 'success.main' }}>
            {t('Add item for sale')}
          </Typography>
        </div>
      </div>
    </Paper>
  );
}

export default MarketAddOrder;
