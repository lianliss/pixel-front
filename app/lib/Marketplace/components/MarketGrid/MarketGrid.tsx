'use strict';
import React, { useEffect, useState } from 'react';
import styles from './MarketGrid.module.scss';
import { IMarketOrderDto, IMarketOrderGroupDto } from 'lib/Marketplace/api-server/market.types.ts';
import MarketAddOrder from 'lib/Marketplace/components/MarketGrid/components/MarketAddOrder/MarketAddOrder.tsx';
import MarketItem from './components/MarketItem/MarketItem.tsx';
import Pagination from '@mui/material/Pagination';
import { IMarketTradeToken } from 'lib/Marketplace/context/Marketplace.context.ts';
import { styled, lighten } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';

const StyledGrid = styled('div')(({ theme }) => ({
  backgroundColor: lighten(theme.palette.background.paper, 0.05),
}));

type Props = {
  rows: IMarketOrderGroupDto[];
  count: number;
  onSelectOrderGroup?: (orderGroup: IMarketOrderGroupDto) => void;
  tradeToken: IMarketTradeToken;
  onAddItemForSell?: () => void;
  onFavorite?: (groupId: string, favorite: boolean) => void;
  hideFavorites?: boolean;
  page?: number;
  onChangePage?: (page: number) => void;
  ownedRows: IMarketOrderDto[];
};

function MarketGrid(props: Props) {
  const {
    rows,
    count,
    onSelectOrderGroup,
    tradeToken,
    onAddItemForSell,
    onFavorite,
    hideFavorites,
    page,
    onChangePage,
    ownedRows,
  } = props;

  const { t } = useTranslation('marketplace');

  const ownedByNftType = React.useMemo(
    () => Object.fromEntries(ownedRows.map((order) => [order.nft.typeId, true])),
    [ownedRows]
  );

  const pagesCount = Math.ceil(count / 20);

  const handleSelectOrderGroup = (row: IMarketOrderGroupDto) => (e: any) => {
    e.stopPropagation();

    if (onSelectOrderGroup) {
      onSelectOrderGroup(row);
    }
  };

  return (
    <div className={styles.root}>
      <StyledGrid className={styles.grid}>
        {onAddItemForSell && <MarketAddOrder onClick={onAddItemForSell} />}

        {rows.map((row, i) => {
          if (!row.nftType) {
            return (
              <div key={`market-item-${i}`}>
                {t('nft not found')} {row.nftTypeId}
              </div>
            );
          }

          return (
            <MarketItem
              key={`market-item-${row.id}`}
              data={row}
              onClick={handleSelectOrderGroup(row)}
              tradeToken={tradeToken}
              onFavorite={onFavorite}
              hideFavorites={hideFavorites}
              isOwned={ownedByNftType[row.nftTypeId]}
            />
          );
        })}
      </StyledGrid>
      {count > 20 && (
        <Pagination
          count={pagesCount}
          color='primary'
          variant='outlined'
          shape='rounded'
          size='large'
          siblingCount={1}
          boundaryCount={0}
          page={page}
          onChange={(_, p) => onChangePage?.(p)}
        />
      )}
    </div>
  );
}

export default MarketGrid;
