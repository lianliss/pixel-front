export enum BountyUserEntryRewardType {
  Pixel = 'Pixel',
  SGB = 'SGB',
  SKALE = 'SKALE',
}

export enum BountyUserEntryRewardStatus {
  Created = 'Created',
  Pending = 'Pending',
  Completed = 'Completed',
}

export enum BountyEntryStatus {
  Created = 'Created',
  Confirmed = 'Confirmed',
  Declined = 'Declined',
  Rewarded = 'Rewarded',
}

export type IBountyUserEntry<TDate = Date> = {
  id: string;

  bountyUserId: string;

  link: string;
  message: string;
  status: BountyEntryStatus;
  comment: string | null;

  createdAt: TDate;

  // relations
  bountyUser: IBountyUser<TDate> | null;
  bountyReward: IBountyUserReward<TDate> | null;
};

export type IBountyUserEntryDto<TDate = number> = Omit<
  IBountyUserEntry<TDate>,
  'bountyReward' | 'bountyUser'
> & {
  bountyUser: IBountyUserDto<TDate> | null;
  bountyReward: IBountyUserRewardDto | null;
};

export type IBountyUser<TDate = Date> = {
  id: string;

  telegramId: number;
  userAddress: string;

  nextAt?: TDate;
  createdAt: TDate;

  balances?: IBountyUserBalance<TDate>[];
};

export type IBountyUserDto<TDate = number> = Omit<IBountyUser<TDate>, 'balances'> & {
  balances?: IBountyUserBalanceDto<TDate>[];
};

export type IBountyUserRewardDto<TDate = number> = IBountyUserReward<TDate>;

export enum BountyUserRewardType {
  Pixel = 'Pixel',
  SGB = 'SGB',
  SKALE = 'SKALE',
}

export enum BountyUserRewardStatus {
  Created = 'Created',
  Pending = 'Pending',
  Completed = 'Completed',
}

export type IBountyUserRewardContext = {
  [BountyUserRewardType.SGB]: { amount: number };
  [BountyUserRewardType.SKALE]: { amount: number };
  [BountyUserRewardType.Pixel]: { amount: number; chainId: number };
};

export type IBountyUserReward<TDate = Date> = {
  id: string;

  bountyUserId: string;
  type: BountyUserRewardType;
  context: IBountyUserRewardContext[BountyUserRewardType];

  status: BountyUserRewardStatus;
  rewardedAt: TDate | null;
  createdAt: TDate;
};

export type IBountyUserBalanceContext = {
  [BountyUserEntryRewardType.SGB]: { amount: number };
  [BountyUserEntryRewardType.SKALE]: { amount: number };
  [BountyUserEntryRewardType.Pixel]: { amount: number; chainId: number };
};

export type IBountyUserBalance<TDate = Date> = {
  id: string;

  bountyUserId: string;

  type: BountyUserEntryRewardType;
  context: IBountyUserBalanceContext[BountyUserEntryRewardType];
  status: BountyUserEntryRewardStatus;

  collectAt: TDate | null;
  rewardedAt: TDate | null;
  updatedAt: TDate;
  createdAt: TDate;
};

export type IBountyUserBalanceDto<TDate = number> = IBountyUserBalance<TDate>;

// response
export type IGetCurrentBountyUserResponse = IBountyUserDto | null;

export type IGetBountyUserResponse = IBountyUserDto | null;

export type ICreateBountyUserEntryResponse = IBountyUserEntryDto;

export type ICollectBountyUserRewardResponse = IBountyUserRewardDto;

export type ICreateBountyUserRewardResponse = IBountyUserRewardDto;

export type IFindBountyEntriesResponse = { rows: IBountyUserEntryDto[]; count: number };

export type IFindBountyUserEntriesResponse = { rows: IBountyUserEntryDto[]; count: number };

export type IChangeBountyEntryStatusResponse = IBountyUserEntryDto;

// request

export type IFindBountyEntriesDto = { limit: number; offset: number; userAddress: string };

export type IFindBountyUserEntriesDto = { limit: number; offset: number; bountyUserId: string } & {
  userAddress: string;
};

export type IGetBountyUserDto = { userAddress: string; adminAddress: string };

export type IGetCurrentBountyUserDto = { userAddress: string };

export type ICreateBountyUserEntryDto = {
  link: string;
  message: string;
} & { userAddress: string };

export type ICollectBountyUserRewardDto = {
  bountyUserRewardId: string;
} & { userAddress: string };

export type ICreateBountyUserRewardDto = {
  type: BountyUserRewardType;
  bountyEntryId: string;
  context: IBountyUserRewardContext[BountyUserRewardType];
} & { adminAddress: string };

export type IChangeBountyEntryStatusDto = {
  bountyEntryId: string;
  status: BountyEntryStatus;
  comment?: string;
} & { adminAddress: string };
