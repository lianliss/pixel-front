'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  IChangeBountyEntryStatusDto,
  IChangeBountyEntryStatusResponse,
  ICollectBountyUserRewardDto,
  ICollectBountyUserRewardResponse,
  ICreateBountyUserEntryDto,
  ICreateBountyUserEntryResponse,
  ICreateBountyUserRewardDto,
  ICreateBountyUserRewardResponse,
  IFindBountyEntriesDto,
  IFindBountyEntriesResponse,
  IFindBountyUserEntriesDto,
  IFindBountyUserEntriesResponse,
  IGetBountyUserDto,
  IGetBountyUserResponse,
  IGetCurrentBountyUserDto,
  IGetCurrentBountyUserResponse,
} from 'lib/Bounty/api-server/bounty.types.ts';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';
import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class BountyApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async getCurrent({
    userAddress,
  }: IGetCurrentBountyUserDto): Promise<IGetCurrentBountyUserResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetCurrentBountyUserResponse },
      AxiosResponse<{ data: IGetCurrentBountyUserResponse }>
    >(`/api/v1/bounty/get`, {
      baseURL: this.url,
    });

    return data.data;
  }

  async getUser({ userAddress, adminAddress }: IGetBountyUserDto): Promise<IGetBountyUserResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetBountyUserResponse },
      AxiosResponse<{ data: IGetBountyUserResponse }>
    >(`/api/v1/bounty/get/user`, {
      baseURL: this.url,
      params: { userAddress },
    });

    return data.data;
  }

  async findEntries({
    userAddress,
    limit,
    offset,
  }: IFindBountyEntriesDto): Promise<IFindBountyEntriesResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindBountyEntriesResponse },
      AxiosResponse<{ data: IFindBountyEntriesResponse }>
    >(`/api/v1/bounty/entry/find`, {
      baseURL: this.url,
      params: { limit, offset },
    });

    return data.data;
  }

  async findUserEntries({
    bountyUserId,
    userAddress,
    limit,
    offset,
  }: IFindBountyUserEntriesDto): Promise<IFindBountyUserEntriesResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindBountyUserEntriesResponse },
      AxiosResponse<{ data: IFindBountyUserEntriesResponse }>
    >(`/api/v1/bounty/user/entry/find`, {
      baseURL: this.url,
      params: { limit, offset, bountyUserId },
    });

    return data.data;
  }

  async createEntry({
    userAddress,
    ...payload
  }: ICreateBountyUserEntryDto): Promise<ICreateBountyUserEntryResponse> {
    const { data } = await axiosInstance.post<
      { data: ICreateBountyUserEntryResponse },
      AxiosResponse<{ data: ICreateBountyUserEntryResponse }>,
      Omit<ICreateBountyUserEntryDto, 'userAddress'>
    >(`/api/v1/bounty/create/entry`, payload, {
      baseURL: this.url,
    });

    return data.data;
  }

  async createReward({
    adminAddress,
    ...payload
  }: ICreateBountyUserRewardDto): Promise<ICreateBountyUserRewardResponse> {
    const { data } = await axiosInstance.post<
      { data: ICreateBountyUserRewardResponse },
      AxiosResponse<{ data: ICreateBountyUserRewardResponse }>,
      Omit<ICreateBountyUserRewardDto, 'adminAddress'>
    >(`/api/v1/bounty/create/reward`, payload, {
      baseURL: this.url,
    });

    return data.data;
  }

  async collectReward({
    userAddress,
    ...payload
  }: ICollectBountyUserRewardDto): Promise<ICollectBountyUserRewardResponse> {
    const { data } = await axiosInstance.post<
      { data: ICollectBountyUserRewardResponse },
      AxiosResponse<{ data: ICollectBountyUserRewardResponse }>,
      Omit<ICollectBountyUserRewardDto, 'userAddress'>
    >(`/api/v1/bounty/collect/reward`, payload, {
      baseURL: this.url,
    });

    return data.data;
  }

  async changeEntryStatus({
    adminAddress,
    ...payload
  }: IChangeBountyEntryStatusDto): Promise<IChangeBountyEntryStatusResponse> {
    const { data } = await axiosInstance.post<
      { data: IChangeBountyEntryStatusResponse },
      AxiosResponse<{ data: IChangeBountyEntryStatusResponse }>,
      Omit<IChangeBountyEntryStatusDto, 'adminAddress'>
    >(`/api/v1/bounty/entry/status/update`, payload, {
      baseURL: this.url,
    });

    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const bountyApi = new BountyApi({ url: 'https://api.hellopixel.network' });
