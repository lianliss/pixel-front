import { createEffect, createStore } from 'effector';
import { AxiosError } from 'axios';
import {
  ICollectBountyUserRewardDto,
  ICreateBountyUserEntryDto,
  ICreateBountyUserRewardDto,
  IFindBountyEntriesDto,
  IGetCurrentBountyUserResponse,
  IGetBountyUserDto,
  IChangeBountyEntryStatusDto,
  IFindBountyUserEntriesDto,
} from 'lib/Bounty/api-server/bounty.types.ts';
import { bountyApi } from 'lib/Bounty/api-server/bounty.api.ts';
import { createStoreState } from 'utils/store/createStoreState.ts';

// chest user stats

export const loadCurrentBountyUser = createEffect(async (opts: IGetBountyUserDto) => {
  const res = await bountyApi.getCurrent(opts);
  return res;
});

export const currentBountyUserStore = createStore({
  loading: true,
  error: null as Error | null,
  state: null as IGetCurrentBountyUserResponse | null,
})
  .on(loadCurrentBountyUser.doneData, (_prev, next) => ({
    loading: false,
    error: null,
    state: next,
  }))
  .on(loadCurrentBountyUser.fail, (_prev, next) => ({
    loading: false,
    error: (next.error as AxiosError)?.response?.data?.error || next.error,
    state: null,
  }))
  .on(loadCurrentBountyUser.pending, (prev, loading) => ({
    loading: loading,
    error: prev.error,
    state: prev.state,
  }));

//

export const collectBountyReward = createEffect(async (opts: ICollectBountyUserRewardDto) => {
  const res = await bountyApi.collectReward(opts);
  return res;
});

export const createBountyReward = createEffect(async (opts: ICreateBountyUserRewardDto) => {
  const res = await bountyApi.createReward(opts);
  return res;
});

export const changeBountyEntryStatus = createEffect(async (opts: IChangeBountyEntryStatusDto) => {
  const res = await bountyApi.changeEntryStatus(opts);
  return res;
});

// bounty entries

export const createBountyEntry = createEffect(async (opts: ICreateBountyUserEntryDto) => {
  const res = await bountyApi.createEntry(opts);
  return res;
});

export const loadBountyEntries = createEffect(async (opts: IFindBountyEntriesDto) => {
  const res = await bountyApi.findEntries(opts);
  return res;
});

export const bountyEntriesStore = createStoreState(loadBountyEntries);

// bounty user entries

export const loadBountyUserEntries = createEffect(async (opts: IFindBountyUserEntriesDto) => {
  const res = await bountyApi.findUserEntries(opts);
  return res;
});

export const bountyUserEntriesStore = createStoreState(loadBountyUserEntries);

// bounty user

export const loadBountyUser = createEffect(async (opts: IGetBountyUserDto) => {
  const res = await bountyApi.getUser(opts);
  return res;
});

export const bountyUserStore = createStoreState(loadBountyUser);
