import {
  BountyUserRewardType,
  IBountyUserBalanceDto,
  IBountyUserDto,
  IBountyUserRewardDto,
} from 'lib/Bounty/api-server/bounty.types.ts';

export type IBountyUserRewardsMap = {
  pxlReward?: IBountyUserBalanceDto;
  sgbReward?: IBountyUserBalanceDto;
  sklReward?: IBountyUserBalanceDto;
};

export function getBountyUserRewards(bountyUser: IBountyUserDto): IBountyUserRewardsMap {
  const pxlReward = bountyUser.balances?.find((el) => el.type == BountyUserRewardType.Pixel);
  const sgbReward = bountyUser.balances?.find((el) => el.type == BountyUserRewardType.SGB);
  const sklReward = bountyUser.balances?.find((el) => el.type == BountyUserRewardType.SKALE);

  return {
    pxlReward,
    sgbReward,
    sklReward,
  };
}
