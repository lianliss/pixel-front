import React, { useState } from 'react';
import { AdminLayout } from 'lib/AdminV5/components/AdminLayout/AdminLayout.tsx';
import { BountyEntryTable } from 'lib/Bounty/components/admin/BountyEntryTable.tsx';
import { useBountyEntries } from 'lib/Bounty/hooks/useBountyEntries.ts';
import { usePagination } from 'utils/hooks/usePagination.ts';
import {
  BountyEntryStatus,
  BountyUserEntryRewardType,
  BountyUserRewardType,
  IBountyUserEntryDto,
} from 'lib/Bounty/api-server/bounty.types.ts';
import BountyUserDialog from 'lib/Bounty/components/admin/BountyUserDialog.tsx';
import { useBountyUser } from 'lib/Bounty/hooks/useBountyUser.ts';
import toaster from 'services/toaster.tsx';
import { changeBountyEntryStatus, createBountyReward } from 'lib/Bounty/api-server/bounty.store.ts';
import { Chain } from 'services/multichain/chains';
import { useAccount } from '@chain/hooks/useAccount.ts';

function AdminBountyPage() {
  const account = useAccount();

  const pagination = usePagination({ step: 20 });
  const bountyEntries = useBountyEntries({
    pagination,
  });

  const [selectedEntry, setSelectedEntry] = useState<IBountyUserEntryDto | null>(null);
  const bountyUserState = useBountyUser({ userAddress: selectedEntry?.bountyUser?.userAddress });

  const handleReward = async (
    entry: IBountyUserEntryDto,
    type: BountyUserRewardType,
    amount: number
  ) => {
    if (!selectedEntry || !account.isConnected) {
      return;
    }

    try {
      await createBountyReward({
        type,
        bountyEntryId: entry.id,
        context: {
          amount,
          chainId: type === BountyUserRewardType.SKALE ? Chain.SKALE : Chain.SONGBIRD,
        },
        adminAddress: account.accountAddress,
      });

      await bountyUserState.refetch();
    } catch (e) {
      toaster.captureException(e);
    }
  };
  const handleEntryStatus = async (
    entry: IBountyUserEntryDto,
    status: BountyEntryStatus,
    comment: string | null
  ) => {
    if (!account.isConnected) {
      return;
    }

    try {
      await changeBountyEntryStatus({
        status,
        bountyEntryId: entry.id,
        comment: comment || undefined,
        adminAddress: account.accountAddress,
      });

      await bountyEntries.refetch();
      setSelectedEntry(null);
    } catch (e) {
      toaster.captureException(e);
    }
  };

  return (
    <AdminLayout>
      <BountyEntryTable
        rows={bountyEntries.data?.rows}
        count={bountyEntries.data?.count}
        pagination={pagination}
        onSelect={(v) => setSelectedEntry(v)}
      />

      <BountyUserDialog
        open={!!selectedEntry}
        onClose={() => {
          setSelectedEntry(null);
          bountyUserState.clear();
        }}
        bountyUser={bountyUserState.data}
        bountyEntry={selectedEntry}
        loading={bountyUserState.loading}
        onReward={handleReward}
        onEntryStatus={handleEntryStatus}
      />
    </AdminLayout>
  );
}

export default AdminBountyPage;
