import Box from '@mui/material/Box';
import BountyCard from 'lib/Bounty/components/BountyCard.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import image1 from 'assets/img/bounty/bounty-1.png';
import image2 from 'assets/img/bounty/bounty-2.png';
import image3 from 'assets/img/bounty/bounty-3.png';
import rewardSkl from 'assets/img/bounty/bounty-reward-skl.png';
import rewardSgb from 'assets/img/bounty/bounty-reward-sgb.svg';
import rewardPxl from 'assets/img/bounty/bounty-reward-pixel.svg';
import Stack from '@mui/material/Stack';
import BountyBalanceCard from 'lib/Bounty/components/BountyBalanceCard.tsx';
import { useCurrentBountyUser } from 'lib/Bounty/hooks/useCurrentBountyUser.ts';
import { BountyUserRewardType } from 'lib/Bounty/api-server/bounty.types.ts';
import { useMemo } from 'react';
import { createBountyEntry } from 'lib/Bounty/api-server/bounty.store.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import toaster from 'services/toaster.tsx';
import { useUnit } from 'effector-react';
import { useTranslation } from 'react-i18next';
import Container from '@mui/material/Container';
import { BountyForm } from 'lib/Bounty/components/BountyForm.tsx';
import { useBackAction } from '../../../shared/telegram/useBackAction.ts';
import { getBountyUserRewards, IBountyUserRewardsMap } from 'lib/Bounty/utils/utils.ts';
import { BountyUserEntries } from 'lib/Bounty/components/BountyUserEntries.tsx';
import { useBountyUserEntries } from 'lib/Bounty/hooks/useBountyUserEntries.ts';
import { getDefaultChainImage } from '@cfg/image.ts';

function BountyPage() {
  const { t } = useTranslation();
  const account = useAccount();
  const bountyUser = useCurrentBountyUser();
  const actionLoading = useUnit(createBountyEntry.pending);

  const entries = useBountyUserEntries({ bountyUserId: bountyUser.data?.id });

  const rewards = useMemo((): IBountyUserRewardsMap => {
    return bountyUser.data ? getBountyUserRewards(bountyUser.data) : {};
  }, [bountyUser.data]);

  const handleSend = async (payload: { link: string; text: string }) => {
    try {
      await createBountyEntry({
        message: payload.text,
        link: payload.link,
        userAddress: account.accountAddress,
      });

      bountyUser.refetch();
    } catch (e) {
      toaster.captureException(e);
    }
  };

  useBackAction('/wallet');

  return (
    <Box
      sx={{ pt: 2, pb: 4 }}
      style={{ backgroundImage: `url(${getDefaultChainImage(account.chainId)})` }}
    >
      <Container maxWidth='xs'>
        <Typography variant='h6' fontWeight='bold' align='center' color='text.primary'>
          {t('BOUNTY PROGRAM')}
        </Typography>
        <Typography variant='subtitle2' color='text.secondary' align='center' sx={{ mt: 0.5 }}>
          {t('Promote Hello Pixel to get rewards')}
        </Typography>

        <Box sx={{ display: 'flex', flexDirection: 'column', gap: 1, mt: 1.25, width: '100%' }}>
          <Box sx={{ display: 'flex', gap: 1, width: '100%', '& > *': { flex: 1 } }}>
            <BountyCard
              title={t('Make Content about the Game')}
              image={image1 as string}
              index={1}
            />
            <BountyCard
              title={t('Post it on any social media')}
              image={image2 as string}
              index={2}
            />
          </Box>
          <BountyCard
            title={t('Get rewarded up to')}
            image={image3 as string}
            index={3}
            subtitle='100 000 SGB'
            rawImage
          />
        </Box>

        <BountyForm
          sx={{ mt: 2 }}
          nextAt={bountyUser.data?.nextAt}
          loading={actionLoading}
          onSubmit={handleSend}
        />

        <Typography
          variant='h6'
          fontWeight='bold'
          align='center'
          color='text.primary'
          sx={{ mt: 2 }}
        >
          {t('BOUNTY Balance')}
        </Typography>

        <Stack direction='column' gap={1} sx={{ mt: 1 }}>
          <BountyBalanceCard
            symbol='PXLs'
            amount={rewards.pxlReward?.context.amount || 0}
            title='Pixel Shard'
            image={rewardPxl as string}
            disabled
            soon
          />
          <BountyBalanceCard
            symbol='SGB'
            amount={rewards.sgbReward?.context.amount || 0}
            title='Songbird'
            image={rewardSgb as string}
            disabled
            soon
          />
          <BountyBalanceCard
            symbol='SKL'
            amount={rewards.sklReward?.context.amount || 0}
            title='SKALE'
            image={rewardSkl as string}
            disabled
            soon
          />
        </Stack>

        {entries.data?.rows.length && (
          <BountyUserEntries sx={{ mt: 2 }} rows={entries.data?.rows} />
        )}
      </Container>
    </Box>
  );
}

export default BountyPage;
