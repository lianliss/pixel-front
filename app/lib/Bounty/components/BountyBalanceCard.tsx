import Paper from '@mui/material/Paper';
import { alpha, styled } from '@mui/material/styles';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import Box from '@mui/material/Box';
import { Button } from '@ui-kit/Button/Button.tsx';

const StyledPaper = styled(Paper)(({ theme }) => ({
  padding: 8,
  background: alpha(theme.palette.primary.main, 0.1),
  border: 'solid 1px',
  borderColor: alpha(theme.palette.primary.main, 0.3),

  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
}));
const StyledAvatar = styled(Avatar)(() => ({
  width: 64,
  height: 64,
}));

type Props = {
  image: string;
  title: string;
  symbol: string;
  amount: number;
  disabled?: boolean;
  soon?: boolean;
  onClaim?: () => void;
};

function BountyBalanceCard(props: Props) {
  const { image, title, amount, symbol, disabled = false, soon = false, onClaim } = props;

  return (
    <StyledPaper variant='outlined'>
      <Box sx={{ display: 'flex', gap: 1 }}>
        <StyledAvatar variant='rounded' variantStyle='outlined' src={image} />

        <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
          <Typography
            variant='subtitle1'
            fontWeight='bold'
            color='text.primary'
            sx={{ lineHeight: '21px' }}
          >
            {title}
          </Typography>
          <Typography
            variant='subtitle1'
            fontWeight='bold'
            color='text.primary'
            sx={{ mt: '2px', fontSize: 18, lineHeight: '21px' }}
          >
            {amount}{' '}
            <Typography variant='inherit' component='span' sx={{ opacity: 0.5 }}>
              {symbol}
            </Typography>
          </Typography>
        </Box>
      </Box>

      <Button
        variant='contained'
        color='success'
        size='large'
        sx={{ maxWidth: soon ? 150 : 112, mr: 1 }}
        disabled={!onClaim || disabled || soon}
        onClick={onClaim}
      >
        {`Collect ${soon ? '(Soon)' : ''}`}
      </Button>
    </StyledPaper>
  );
}

export default BountyBalanceCard;
