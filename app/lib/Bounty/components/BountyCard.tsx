import Paper from '@mui/material/Paper';
import { alpha, styled } from '@mui/material/styles';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import { Img } from '@ui-kit/Img/Img.tsx';

const StyledPaper = styled(Paper)(({ theme }) => ({
  padding: '24px 24px',
  position: 'relative',
  background: alpha(theme.palette.primary.main, 0.1),
  border: 'solid 1px',
  borderColor: alpha(theme.palette.primary.main, 0.3),

  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
}));
const Index = styled(Typography)(() => ({
  top: 4,
  left: 8,
  position: 'absolute',
}));
const StyledAvatar = styled(Avatar)(() => ({
  width: 104,
  height: 104,
  margin: '0px 8px',
}));
const StyledImg = styled(Img)(() => ({
  height: 125,
}));

type Props = { image: string; index: number; title: string; subtitle?: string; rawImage?: boolean };

function BountyCard(props: Props) {
  const { image, index, title, subtitle, rawImage } = props;

  return (
    <StyledPaper variant='outlined'>
      <Index color='primary' variant='caption' fontWeight='bold'>
        {index}
      </Index>

      {rawImage ? (
        <StyledImg src={image} />
      ) : (
        <StyledAvatar variant='rounded' variantStyle='outlined' src={image} />
      )}

      <Typography variant='subtitle2' align='center' color='text.primary' sx={{ mt: 1.25 }}>
        {title}
      </Typography>
      {subtitle && (
        <Typography variant='h6' fontWeight='bold' color='text.primary' align='center'>
          {subtitle}
        </Typography>
      )}
    </StyledPaper>
  );
}

export default BountyCard;
