import Box, { BoxProps } from '@mui/material/Box';
import { IBountyUserEntryDto } from 'lib/Bounty/api-server/bounty.types.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import Stack from '@mui/material/Stack';
import { BountyUserEntry } from 'lib/Bounty/components/BountyUserEntry.tsx';
import Paper from '@mui/material/Paper';
import React from 'react';
import Divider from '@mui/material/Divider';

type Props = BoxProps & { rows: IBountyUserEntryDto[] };

export function BountyUserEntries(props: Props) {
  const { rows = [] as IBountyUserEntryDto[], ...other } = props;

  return (
    <Box {...other}>
      <Typography fontWeight='bold' align='center' variant='h6' color='text.primary'>
        Payments
      </Typography>
      <Paper sx={{ mt: 1, p: 2 }}>
        <Stack direction='column' gap={2} divider={<Divider />}>
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              gap: 2,

              '& > *': {
                flex: 1,
                display: 'block',
              },
            }}
          >
            <Typography variant='subtitle2' color='text.primary'>
              ID
            </Typography>
            <Typography variant='subtitle2' color='text.primary'>
              Status
            </Typography>
            <Typography variant='subtitle2' color='text.primary'>
              Reward
            </Typography>
          </Box>

          {rows.map((el) => (
            <BountyUserEntry entry={el} key={`entry-${el.id}`} />
          ))}
        </Stack>
      </Paper>
    </Box>
  );
}
