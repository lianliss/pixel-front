import Paper from '@mui/material/Paper';
import TableContainer from '@mui/material/TableContainer';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableCell from '@mui/material/TableCell';
import TableBody from '@mui/material/TableBody';
import React from 'react';
import { BountyEntryStatus, IBountyUserEntryDto } from 'lib/Bounty/api-server/bounty.types.ts';
import { TablePagination, TableRow } from '@mui/material';
import { IPaginationResult } from 'utils/hooks/usePagination.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Link } from 'react-router-dom';
import { Button } from '@ui-kit/Button/Button.tsx';

type Props = {
  rows: IBountyUserEntryDto[];
  count: number;
  pagination?: IPaginationResult;
  onSelect?: (row: IBountyUserEntryDto) => void;
};

export function BountyEntryTable(props: Props) {
  const { rows = [], count = 0, pagination, onSelect } = props;

  return (
    <Paper variant='outlined' sx={{ overflow: 'hidden' }}>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell sx={{ width: '70px' }} />
              <TableCell sx={{ width: '100px' }}>User</TableCell>
              <TableCell sx={{ width: 90 }}>Status</TableCell>
              <TableCell sx={{ width: '150px' }}>Link</TableCell>
              <TableCell sx={{ maxWidth: 700 }}>Message</TableCell>
              <TableCell sx={{ width: '180px' }}>Date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={`row-${row.id}`}>
                <TableCell>
                  <Button variant='outlined' color='primary' onClick={() => onSelect?.(row)}>
                    Show
                  </Button>
                </TableCell>
                <TableCell>
                  <Link to={`/profile/${row.bountyUser?.userAddress}`}>
                    <Typography variant='caption'>{row.bountyUser?.userAddress || '-'}</Typography>
                  </Link>
                </TableCell>
                <TableCell>
                  <Typography
                    variant='caption'
                    color={
                      row.status === BountyEntryStatus.Declined
                        ? 'error.main'
                        : row.status === BountyEntryStatus.Created
                        ? 'warning.main'
                        : 'success.main'
                    }
                  >
                    {row.status}
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography
                    variant='caption'
                    component='a'
                    href={row.link || '#'}
                    target='_blank'
                    noWrap
                    sx={{
                      textOverflow: 'ellipsis',
                      maxWidth: 150,
                      width: 150,
                      overflow: 'hidden',
                      display: 'block',
                    }}
                  >
                    {row.link || '-'}
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography variant='caption' sx={{ maxWidth: 700, wordBreak: 'break-word' }}>
                    {row.message || '-'}
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography variant='caption'>
                    {new Date(row.createdAt).toLocaleString('ru-RU')}
                  </Typography>
                </TableCell>
              </TableRow>
            ))}
            {!rows.length && (
              <TableRow>
                <TableCell colSpan={6}>No data</TableCell>
              </TableRow>
            )}
          </TableBody>

          <TablePagination
            count={count}
            page={(pagination?.page || 1) - 1}
            rowsPerPage={pagination?.step || 20}
            onPageChange={(e, v) => pagination?.setPage(v)}
            rowsPerPageOptions={[20]}
          />
        </Table>
      </TableContainer>
    </Paper>
  );
}
