import React, { useMemo, useState } from 'react';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import Box from '@mui/material/Box';
import { DialogTitle } from '@mui/material';
import {
  BountyEntryStatus,
  BountyUserRewardType,
  IBountyUserDto,
  IBountyUserEntryDto,
} from 'lib/Bounty/api-server/bounty.types.ts';
import Stack from '@mui/material/Stack';
import BountyBalanceCard from 'lib/Bounty/components/BountyBalanceCard.tsx';
import rewardPxl from 'assets/img/bounty/bounty-reward-pixel.svg';
import rewardSgb from 'assets/img/bounty/bounty-reward-sgb.svg';
import rewardSkl from 'assets/img/bounty/bounty-reward-skl.png';
import { getBountyUserRewards, IBountyUserRewardsMap } from 'lib/Bounty/utils/utils.ts';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import NumberField from '@ui-kit/NumberField/NumberField.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { ButtonGroup } from '@ui-kit/ButtonGroup/ButtonGroup.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import Paper from '@mui/material/Paper';

type Props = DialogProps & {
  bountyUser?: IBountyUserDto;
  bountyEntry?: IBountyUserEntryDto;
  loading?: boolean;
  onReward?: (entry: IBountyUserEntryDto, type: BountyUserRewardType, amount: number) => void;
  onEntryStatus?: (
    entry: IBountyUserEntryDto,
    status: BountyEntryStatus,
    comment: string | undefined
  ) => void;
};

function BountyUserDialog(props: Props) {
  const { bountyUser, bountyEntry, onClose, onReward, loading, onEntryStatus, ...other } = props;

  const [type, setType] = useState<BountyUserRewardType>(BountyUserRewardType.Pixel);
  const [amount, setAmount] = useState<string>('');
  const [comment, setComment] = useState<string>('');

  const rewards = useMemo((): IBountyUserRewardsMap => {
    return bountyUser ? getBountyUserRewards(bountyUser) : {};
  }, [bountyUser]);

  const disabledReward = !+amount;
  const isConfirmed = bountyEntry?.status === BountyEntryStatus.Confirmed;
  const isRewarded = bountyEntry?.status === BountyEntryStatus.Rewarded;
  const isDeclined = bountyEntry?.status === BountyEntryStatus.Declined;

  const handleReward = () => {
    setAmount('');
    setType(BountyUserRewardType.Pixel);

    onReward?.(bountyEntry, type, +amount);
  };

  const content = (
    <Box>
      <Paper variant='outlined' sx={{ p: 2 }}>
        <Typography variant='body2' sx={{ wordBreak: 'break-word' }}>
          {bountyEntry?.link}
        </Typography>
        <Typography variant='body2' sx={{ mt: 1, wordBreak: 'break-word' }}>
          {bountyEntry?.message}
        </Typography>
      </Paper>

      <Stack direction={{ md: 'row', xs: 'column' }} gap={3} sx={{ mt: 4 }}>
        {isDeclined && (
          <Stack direction='column' gap={1} sx={{ width: '100%' }}>
            <Typography color='error' sx={{ width: '100%' }}>
              Declined
            </Typography>

            <Typography variant='subtitle1' sx={{ width: '100%' }}>
              Comment:
            </Typography>
            <Typography color='error' variant='body2' sx={{ width: '100%' }}>
              {bountyEntry?.comment || '-'}
            </Typography>
          </Stack>
        )}
        {!isConfirmed && !isDeclined && !isRewarded && (
          <Stack direction='column' gap={3} sx={{ width: '100%' }}>
            <ButtonGroup variant='outlined' fullWidth>
              <Button
                color='success'
                onClick={() => onEntryStatus?.(bountyEntry, BountyEntryStatus.Confirmed, undefined)}
              >
                Confirm
              </Button>
              <Button
                color='error'
                onClick={() =>
                  onEntryStatus?.(bountyEntry, BountyEntryStatus.Declined, comment || undefined)
                }
              >
                Decline
              </Button>
            </ButtonGroup>

            <TextField
              label='Comment'
              minRows={3}
              multiline
              value={comment}
              onChange={(e) => setComment(e.target.value)}
            />
          </Stack>
        )}
        {isRewarded && (
          <Box sx={{ width: '100%' }}>
            <Typography>
              Reward: {bountyEntry.bountyReward?.type} {bountyEntry.bountyReward?.context.amount}
            </Typography>
          </Box>
        )}
        {isConfirmed && (
          <Stack direction='column' gap={1} sx={{ width: '100%' }}>
            <FormControl fullWidth>
              <InputLabel id='demo-simple-select-label'>Reward Type</InputLabel>
              <Select
                value={type}
                label='Reward Type'
                onChange={(e) => setType(e.target.value as BountyUserRewardType)}
              >
                {[
                  BountyUserRewardType.SGB,
                  BountyUserRewardType.SKALE,
                  BountyUserRewardType.Pixel,
                ].map((type) => (
                  <MenuItem key={type} value={type}>
                    {type}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <NumberField
              min={1}
              max={10_000}
              step={10}
              label='Amount'
              placeholder='Enter amount'
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
            />

            <Button
              fullWidth
              variant='contained'
              color='primary'
              size='large'
              disabled={disabledReward || !onReward}
              onClick={handleReward}
            >
              Send Reward
            </Button>
          </Stack>
        )}

        <Stack direction='column' gap={1} sx={{ width: '100%' }}>
          <BountyBalanceCard
            symbol='PXLs'
            amount={rewards.pxlReward?.context.amount || 0}
            title='Pixel Shard'
            image={rewardPxl as string}
            disabled
          />
          <BountyBalanceCard
            symbol='SGB'
            amount={rewards.sgbReward?.context.amount || 0}
            title='Songbird'
            image={rewardSgb as string}
            disabled
          />
          <BountyBalanceCard
            symbol='SKL'
            amount={rewards.sklReward?.context.amount || 0}
            title='SKALE'
            image={rewardSkl as string}
            disabled
          />
        </Stack>
      </Stack>
    </Box>
  );

  return (
    <Dialog onClose={onClose} {...other} fullWidth maxWidth='lg' scroll='body'>
      <DialogTitle sx={{ overflow: 'hidden' }}>
        User {loading ? 'Loading...' : bountyUser?.userAddress}
      </DialogTitle>
      <div style={{ padding: 32 }}>{content}</div>
    </Dialog>
  );
}

export default BountyUserDialog;
