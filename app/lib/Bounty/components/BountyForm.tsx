import Paper, { PaperProps } from '@mui/material/Paper';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { TextField } from '@ui-kit/TextField/TextField.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { fromUtc, getLocaleTime } from 'utils/format/date';
import { useTranslation } from 'react-i18next';
import { useState } from 'react';
import { styled } from '@mui/material/styles';
import { isValidUrl } from 'utils/validate/url.ts';
import Alert from '@mui/material/Alert';

const StyledPaper = styled(Paper)(() => ({
  padding: '16px',
}));

type Props = PaperProps & {
  nextAt?: number;
  onSubmit?: (payload: { text: string; link: string }) => void;
  loading?: boolean;
};

export function BountyForm(props: Props) {
  const { nextAt, onSubmit, loading, ...other } = props;

  const { t } = useTranslation();

  const [link, setLink] = useState('');
  const [text, setText] = useState('');

  const disabledNext = !!nextAt && nextAt && fromUtc(nextAt) > Date.now();
  const disabled = loading || disabledNext;
  const isValidLink = isValidUrl(link);

  return (
    <StyledPaper elevation={0} variant='outlined' {...other}>
      <Typography variant='subtitle1' fontWeight='bold' align='center' color='text.primary'>
        {t('Send')}
      </Typography>

      <TextField
        variant='outlined'
        fullWidth
        sx={{ mt: 1.25 }}
        size='small'
        label={t('Insert Link')}
        placeholder={t('Link')}
        disabled={disabledNext || loading}
        value={link}
        required
        error={link && !isValidLink}
        helperText={!link || isValidLink ? undefined : t('Invalid url')}
        onChange={(e) => setLink(e.target.value)}
      />

      <TextField
        variant='outlined'
        fullWidth
        sx={{ mt: 1.5 }}
        size='small'
        multiline
        minRows={5}
        maxRows={10}
        placeholder={t('Provide info')}
        label={t('Info message')}
        disabled={disabledNext || loading}
        value={text}
        required
        onChange={(e) => setText(e.target.value)}
      />

      {disabledNext && (
        <Alert severity='success' variant='outlined' sx={{ mt: 2 }}>
          The form has already been sent, wait for the next one
        </Alert>
      )}

      <Button
        variant='contained'
        color='primary'
        fullWidth
        sx={{ mt: 2 }}
        size='large'
        disabled={disabled || !isValidLink}
        onClick={() => {
          onSubmit?.({ link, text });
        }}
        loading={loading}
      >
        {disabledNext ? getLocaleTime(nextAt - Date.now() || 0) : t('Send')}
      </Button>
    </StyledPaper>
  );
}
