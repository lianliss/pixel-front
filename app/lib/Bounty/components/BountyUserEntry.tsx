import Box, { BoxProps } from '@mui/material/Box';
import {
  BountyEntryStatus,
  BountyUserEntryRewardStatus,
  IBountyUserEntryDto,
} from 'lib/Bounty/api-server/bounty.types.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { styled } from '@mui/material/styles';
import { formatUUID } from 'utils/format/id.ts';
import HelpBadgePopover from '@ui-kit/HelpBadge/HelpBadgePopover.tsx';

const Root = styled(Box)(() => ({
  display: 'flex',
  gap: 16,

  '& > *': {
    flex: 1,
  },
}));

type Props = BoxProps & { entry: IBountyUserEntryDto };

export function BountyUserEntry(props: Props) {
  const { entry, ...other } = props;

  return (
    <Root {...other}>
      <Typography># {formatUUID(entry.id)}</Typography>
      <div>
        <HelpBadgePopover title={entry.comment}>
          <Typography
            sx={{ pr: 2 }}
            fontWeight='bold'
            color={
              entry.status === BountyEntryStatus.Created
                ? 'text.secondary'
                : entry.status === BountyEntryStatus.Declined
                ? 'error.main'
                : 'success.main'
            }
          >
            {entry.status}
          </Typography>
        </HelpBadgePopover>
      </div>
      <Typography fontWeight='bold'>
        {entry.bountyReward
          ? `${entry.bountyReward.context.amount} ${entry.bountyReward.type}`
          : '-'}
      </Typography>
    </Root>
  );
}
