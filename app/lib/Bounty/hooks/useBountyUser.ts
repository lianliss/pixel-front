import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react';
import { bountyUserStore, loadBountyUser } from 'lib/Bounty/api-server/bounty.store.ts';
import { useCallback, useEffect } from 'react';
import toaster from 'services/toaster.tsx';

export function useBountyUser(payload: { userAddress?: string }) {
  const account = useAccount();
  const bountyUser = useUnit(bountyUserStore);

  const refetch = useCallback(() => {
    if (account.isConnected && payload.userAddress) {
      loadBountyUser({
        userAddress: payload.userAddress,
        adminAddress: account.accountAddress,
      }).catch((err) => toaster.captureException(err));
    } else {
      bountyUserStore.reset();
    }
  }, [account.isConnected, account.accountAddress, payload.userAddress]);

  useEffect(() => {
    refetch();
  }, [refetch]);

  return {
    data: bountyUser.data,
    error: bountyUser.error,
    loading: bountyUser.loading,
    refetch,
    clear: () => bountyUserStore.reset(),
  };
}
