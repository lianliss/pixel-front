import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react';
import { bountyEntriesStore, loadBountyEntries } from 'lib/Bounty/api-server/bounty.store.ts';
import { useCallback, useEffect } from 'react';
import toaster from 'services/toaster.tsx';
import { IPaginationResult } from 'utils/hooks/usePagination.ts';

export function useBountyEntries({ pagination }: { pagination?: IPaginationResult }) {
  const account = useAccount();
  const entries = useUnit(bountyEntriesStore);

  const refetch = useCallback(async () => {
    if (account.isConnected) {
      try {
        await loadBountyEntries({
          limit: pagination?.limit || 100,
          offset: pagination?.offset || 0,
          userAddress: account.accountAddress,
        });
      } catch (e) {
        toaster.captureException(e);
      }
    }
  }, [account.accountAddress, account.isConnected, pagination?.limit, pagination?.offset]);

  useEffect(() => {
    refetch();
  }, [refetch]);
  useEffect(() => {
    pagination?.setTotal(entries.data?.count || 0);
  }, [entries.data?.count]);

  return {
    data: entries.data,
    error: entries.error,
    loading: entries.loading,
    refetch,
  };
}
