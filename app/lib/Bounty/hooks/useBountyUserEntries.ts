import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react';
import {
  bountyEntriesStore,
  bountyUserEntriesStore,
  loadBountyEntries,
  loadBountyUserEntries,
} from 'lib/Bounty/api-server/bounty.store.ts';
import { useCallback, useEffect } from 'react';
import toaster from 'services/toaster.tsx';
import { IPaginationResult } from 'utils/hooks/usePagination.ts';

export function useBountyUserEntries({
  pagination,
  bountyUserId,
}: {
  pagination?: IPaginationResult;
  bountyUserId?: string;
}) {
  const account = useAccount();
  const entries = useUnit(bountyUserEntriesStore);

  const refetch = useCallback(async () => {
    if (account.isConnected && bountyUserId) {
      try {
        await loadBountyUserEntries({
          limit: 20,
          offset: 0,
          userAddress: account.accountAddress,
          bountyUserId,
        });
      } catch (e) {
        toaster.captureException(e);
      }
    }
  }, [
    account.accountAddress,
    account.isConnected,
    bountyUserId,
    pagination?.limit,
    pagination?.offset,
  ]);

  useEffect(() => {
    refetch();
  }, [refetch]);
  useEffect(() => {
    pagination?.setTotal(entries.data?.count || 0);
  }, [entries.data?.count]);

  return {
    data: entries.data,
    error: entries.error,
    loading: entries.loading,
    refetch,
  };
}
