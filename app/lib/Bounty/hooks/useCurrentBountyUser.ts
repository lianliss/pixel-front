import { useAccount } from '@chain/hooks/useAccount.ts';
import { useUnit } from 'effector-react';
import {
  currentBountyUserStore,
  loadCurrentBountyUser,
} from 'lib/Bounty/api-server/bounty.store.ts';
import { useCallback, useEffect } from 'react';
import toaster from 'services/toaster.tsx';

export function useCurrentBountyUser() {
  const account = useAccount();
  const bountyUser = useUnit(currentBountyUserStore);

  const refetch = useCallback(() => {
    if (account.isConnected) {
      loadCurrentBountyUser({ userAddress: account.accountAddress }).catch((err) =>
        toaster.captureException(err)
      );
    }
  }, [account.isConnected, account.accountAddress]);

  useEffect(() => {
    refetch();
  }, [refetch]);

  return {
    data: bountyUser.state,
    error: bountyUser.error,
    loading: bountyUser.loading,
    refetch,
  };
}
