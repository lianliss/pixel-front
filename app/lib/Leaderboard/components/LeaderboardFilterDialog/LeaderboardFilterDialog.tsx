import React from 'react';
import styles from './LeaderboardFilterDialog.module.scss';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';
import FormLabel from '@mui/material/FormLabel';
import RadioGroup from '@mui/material/RadioGroup';
import Divider from '@mui/material/Divider';
import Stack from '@mui/material/Stack';
import { Button } from '@ui-kit/Button/Button.tsx';
import {
  ILeaderboardSeason,
  ILeaderboardSeasonDto,
  LeaderBoardGroup,
} from 'lib/Leaderboard/api-server/leaderboard.types.ts';
import { formatTimestampToDMFG } from 'lib/Leaderboard/utils/formatDate.ts';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useTranslation } from 'react-i18next';

export type ILeaderboardFilterDialogState = {
  group: LeaderBoardGroup;
  season: ILeaderboardSeason['id'] | '-1';
};

type Props = {
  open?: boolean;
  onClose?: () => void;

  state?: ILeaderboardFilterDialogState;
  onChange?: (state: ILeaderboardFilterDialogState) => void;

  seasons?: ILeaderboardSeasonDto[];
  isElders?: boolean;
};

const defaultState: ILeaderboardFilterDialogState = {
  group: LeaderBoardGroup.Default,
  season: '-1',
};

export const LeaderboardFilterDialog = (props: Props) => {
  const {
    open,
    onClose,
    state = { group: LeaderBoardGroup.Default, season: -1 },
    onChange,
    seasons,
    isElders = false,
  } = props;

  const { t } = useTranslation('leaderboard');

  const isSeasons = seasons?.length > 0;

  const [filterState, setFilterState] = React.useState<ILeaderboardFilterDialogState>({
    ...state,
  });

  const sortedSeasons = seasons.sort((a, b) => b.index - a.index);
  const currentSeason = sortedSeasons[0];

  const handleChange = () => {
    onChange?.(filterState);
    onClose?.();
  };
  const handleReset = () => {
    setFilterState({ ...defaultState });
    onChange?.({ ...defaultState });
    onClose?.();
  };
  const handleLocalChange = (key: keyof ILeaderboardFilterDialogState) => (_, value: string) => {
    setFilterState({ ...filterState, [key]: value });
    Telegram.WebApp.HapticFeedback.impactOccurred('light');
  };

  return (
    <BottomDialog
      isOpen={open}
      onClose={onClose}
      PaperProps={{ className: styles.dialog }}
      footer={
        <div style={{ padding: '12px 24px 32px 24px', display: 'flex', gap: 12 }}>
          <Button size='large' variant='outlined' color='primary' fullWidth onClick={handleReset}>
            {t('Clear All')}
          </Button>
          <Button size='large' variant='contained' fullWidth onClick={handleChange}>
            {t('Apply')}
          </Button>
        </div>
      }
    >
      <Stack direction='column' spacing={2}>
        <FormControl className={styles.formControl}>
          <FormLabel id='demo-radio-buttons-group-label'>League</FormLabel>
          <RadioGroup
            value={filterState.group}
            name='radio-buttons-group'
            onChange={handleLocalChange('group')}
          >
            <FormControlLabel
              value={LeaderBoardGroup.Default}
              control={<Radio />}
              label='Default'
            />

            {isElders && (
              <FormControlLabel
                value={LeaderBoardGroup.Old}
                control={<Radio />}
                label={
                  <>
                    {t('Elders')} <span>({t('Registration before')} 31.05.2024)</span>
                  </>
                }
              />
            )}
            {isElders && (
              <FormControlLabel
                value={LeaderBoardGroup.New}
                control={<Radio />}
                label={
                  <>
                    {t('Newbies')} <span>({t('Registration from')} 01.06.2024)</span>
                  </>
                }
              />
            )}

            <FormControlLabel
              value={LeaderBoardGroup.Total}
              control={<Radio />}
              label={
                <>
                  {t('Total')} <span>({t('no rewards')})</span>
                </>
              }
            />
          </RadioGroup>
        </FormControl>

        {isSeasons && <Divider />}

        {isSeasons && (
          <FormControl className={styles.formControl}>
            <FormLabel id='demo-radio-buttons-group-label'>Season</FormLabel>
            <RadioGroup
              value={filterState.season}
              name='radio-buttons-season'
              onChange={handleLocalChange('season')}
            >
              <div className={styles.row}>
                <FormControlLabel
                  value='-1'
                  control={<Radio />}
                  label={`Current Season ${currentSeason?.index || ''}`}
                />
                {!!currentSeason?.pixelTotal && (
                  <Typography variant='caption' fontWeight='bold' className={styles.prizeLabel}>
                    {t('Total reward pool:')}{' '}
                    <Typography
                      variant='caption'
                      fontWeight='bold'
                      color='primary'
                      component='span'
                    >
                      {currentSeason.pixelTotal || 0}
                    </Typography>
                  </Typography>
                )}
              </div>
              {sortedSeasons.length > 0 &&
                sortedSeasons.map((season) => (
                  <div key={`season-${season.id}`} className={styles.row}>
                    <FormControlLabel
                      value={season.id}
                      control={<Radio />}
                      label={
                        <>
                          {t('Season')} {season.index}{' '}
                          <span>
                            ({t('from')} {formatTimestampToDMFG(season.startedAt)} to{' '}
                            {formatTimestampToDMFG(season.endedAt)})
                          </span>
                        </>
                      }
                    />
                    {!!season.pixelTotal && (
                      <Typography variant='caption' fontWeight='bold' className={styles.prizeLabel}>
                        {t('Total reward pool:')}{' '}
                        <Typography
                          variant='caption'
                          fontWeight='bold'
                          color='primary'
                          component='span'
                        >
                          {season.pixelTotal || 0}
                        </Typography>
                      </Typography>
                    )}
                  </div>
                ))}
            </RadioGroup>
          </FormControl>
        )}
      </Stack>
    </BottomDialog>
  );
};
