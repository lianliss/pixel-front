import { ILeaderboardFilterDialogState } from 'lib/Leaderboard/components/LeaderboardFilterDialog/LeaderboardFilterDialog.tsx';
import { LeaderBoardGroup } from 'lib/Leaderboard/api-server/leaderboard.types.ts';

export function isLeaderboardFilterChanged(state: ILeaderboardFilterDialogState): boolean {
  return state.season !== '-1' || state.group !== LeaderBoardGroup.Default;
}
