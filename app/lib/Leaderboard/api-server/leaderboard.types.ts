import { IPaginatedResult } from 'lib/Quests/api-server/quests.types.ts';
import { IUserInfoDto } from 'lib/User/api-server/user.types.ts';

export type IPaginatedQuery = {
  limit: number;
  offset?: number;
};

// enums
export enum LeaderboardType {
  Transactions = 'Transactions',
  BurnedTokens = 'BurnedTokens', // Amount of PXLs spent
  ClaimedTokens = 'ClaimedTokens', // from miner
  ReferralRewards = 'ReferralRewards', // from referral
  ReferralCount = 'ReferralCount', // Number of referrals
  TradeVolume = 'TradeVolume',
}

export enum LeaderboardRewardType {
  Pixel = 'Pixel',
}

export enum LeaderBoardGroup {
  Default = 'Default',
  Old = 'Old',
  New = 'New',
  Total = 'Total',
}

// query dto

export type IFindLeaderboardUsersDto = { leaderboardId: string; chainId: number } & IPaginatedQuery;

export type IFindLeaderboardsDto = IPaginatedQuery & {
  group?: LeaderBoardGroup;
  seasonId?: string;
  chainId: number;
};

// query response

export type IFindLeaderboardUsersResponse = IPaginatedResult<ILeaderboardUserDto>;

export type IFindLeaderboardsResponse = IPaginatedResult<ILeaderboardDto>;

export type IGetLeaderboardResponse = ILeaderboardDto;

export type IGetLeaderboardUserResponse = { row: ILeaderboardUserDto; position: number } | null;

export type IFindLeaderboardSeasonsResponse = IPaginatedResult<ILeaderboardSeasonDto>;

// entity

export type ILeaderboard<TDate = Date> = {
  id: string;
  title: string;
  contractId: string;
  type: LeaderboardType;
  group: LeaderBoardGroup;
  seasonId: string;

  speedLevel?: number;
  storageLevel?: number;
  referralLevel?: number;

  referralsCount?: number;
  transactionsCount?: number;
  claimsCount?: number;
  burnedTokens?: number;
  claimedTokens?: number;
  referralTokens?: number;

  rewardPlaces: number;

  disabled: boolean;
  startedAt?: TDate;
  rewardedAt?: TDate;
  endedAt?: TDate;
  userCreatedAfter?: TDate;
  createdAt: TDate;
};

export type ILeaderboardReward<TDate = Date> = {
  id: string;
  boardId: string;
  type: LeaderboardRewardType;
  context: ILeaderboardRewardContextMap[LeaderboardRewardType];
};

export type ILeaderboardRewardContextMap = {
  [LeaderboardRewardType.Pixel]: { amount: number; symbol?: string };
};

export type ILeaderboardRewardDto<TDate = number> = ILeaderboardReward<TDate>;

export type ILeaderboardDto<TDate = number> = Omit<
  ILeaderboard<TDate>,
  'endedAt' | 'startedAt' | 'userCreatedAfter' | 'rewards' | 'rewardedAt'
> & {
  startedAt: TDate | null;
  endedAt: TDate | null;
  rewardedAt: TDate | null;
  userCreatedAfter: TDate | null;
  rewards?: ILeaderboardRewardDto<TDate>[];
};

export type ILeaderboardUser<TDate = Date> = {
  id: string;
  userId: string;
  leaderboardId: string;

  speedLevel: number;
  storageLevel: number;
  referralLevel: number;

  referralsCount: number;
  transactionsCount: number;
  claimsCount: number;
  burnedTokens: number;
  claimedTokens: number;
  referralTokens: number;
  points: number;

  position: number;

  completedAt?: TDate;
  updatedAt: TDate;
  createdAt: TDate;

  //
  username?: string;
};

export type ILeaderboardUserDto<TDate = number> = Omit<ILeaderboardUser<TDate>, 'completedAt'> & {
  completedAt: TDate | null;
  userInfo: IUserInfoDto | null;
};

export type ILeaderboardSeasonDto<TDate = number> = ILeaderboardSeason<TDate>;

export type ILeaderboardSeason<TDate = Date> = {
  id: string;
  pixelTotal: number;
  startedAt: TDate;
  index: number;
  endedAt: TDate;
  createdAt: TDate;
};
