import { createEffect, createStore } from 'effector';
import {
  IFindLeaderboardsDto,
  IFindLeaderboardUsersDto,
  IGetLeaderboardUserResponse,
  ILeaderboardDto,
  ILeaderboardSeasonDto,
  ILeaderboardUserDto,
} from 'lib/Leaderboard/api-server/leaderboard.types.ts';
import { leaderboardApi } from 'lib/Leaderboard/api-server/leaderboard.api.ts';
import { IPaginatedResult } from 'lib/Quests/api-server/quests.types.ts';

// leaderboard

export const loadLeaderboards = createEffect(
  async (params: IFindLeaderboardsDto & { address: string }) => {
    const res = await leaderboardApi.find({ ...params });
    return res?.rows ?? [];
  }
);

export const leaderboardsStore = createStore<ILeaderboardDto[]>([]).on(
  loadLeaderboards.doneData,
  (prev, next) => next
);

export const loadLeaderboard = createEffect(async (params: { id: string }) => {
  const res = await leaderboardApi.get(params);
  return res;
});

export const leaderboardStore = createStore<ILeaderboardDto | null>(null).on(
  loadLeaderboard.doneData,
  (prev, next) => next
);

// leaderboard users

export const loadLeaderboardUsers = createEffect(async (params: IFindLeaderboardUsersDto) => {
  const res = await leaderboardApi.findUsers(params);
  return {
    ...res,
    leaderboardId: params.leaderboardId,
  };
});

export const leaderboardUsersStore = createStore<
  Record<string, IPaginatedResult<ILeaderboardUserDto>>
>({}).on(loadLeaderboardUsers.doneData, (prev, next) => {
  return { ...prev, [next.leaderboardId]: { rows: next.rows, count: next.count } };
});

// leaderboard user

export const loadLeaderboardUser = createEffect(async (params: { id: string; userId: string }) => {
  const res = await leaderboardApi.getUser(params);
  return {
    result: res,
    leaderboardId: params.id,
  };
});

export const leaderboardUserStore = createStore<Record<string, IGetLeaderboardUserResponse | null>>(
  {}
).on(loadLeaderboardUser.doneData, (prev, next) => {
  return { ...prev, [next.leaderboardId]: next.result };
});

// seasons
export const loadLeaderboardsSeasons = createEffect(async (params: { address: string }) => {
  const res = await leaderboardApi.seasons({ ...params });
  return res?.rows ?? [];
});

export const leaderboardsSeasonsStore = createStore<ILeaderboardSeasonDto[]>([]).on(
  loadLeaderboardsSeasons.doneData,
  (prev, next) => next
);
