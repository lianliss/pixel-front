'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  IFindLeaderboardsDto,
  IFindLeaderboardSeasonsResponse,
  IFindLeaderboardsResponse,
  IFindLeaderboardUsersDto,
  IFindLeaderboardUsersResponse,
  IGetLeaderboardResponse,
  IGetLeaderboardUserResponse,
  ILeaderboardSeasonDto,
} from 'lib/Leaderboard/api-server/leaderboard.types.ts';
import {
  pixelMessageKey,
  pixelSignKey,
  telegramAuthKey,
} from '../../../shared/const/localStorage.ts';

import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';
import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class LeaderboardApi {
  url: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async get({ id }: { id: string }): Promise<IGetLeaderboardResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetLeaderboardResponse },
      AxiosResponse<{ data: IGetLeaderboardResponse }>
    >(`/api/v1/leaderboard/${id}`, { baseURL: this.url });

    return data.data;
  }

  async find({
    address,
    ...params
  }: IFindLeaderboardsDto & { address: string }): Promise<IFindLeaderboardsResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindLeaderboardsResponse },
      AxiosResponse<{ data: IFindLeaderboardsResponse }>
    >(`/api/v1/leaderboard/find`, { baseURL: this.url, params });

    return data.data;
  }

  async findUsers({
    leaderboardId,
    limit,
    offset,
  }: IFindLeaderboardUsersDto): Promise<IFindLeaderboardUsersResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindLeaderboardUsersResponse },
      AxiosResponse<{ data: IFindLeaderboardUsersResponse }>
    >(`/api/v1/leaderboard/users?limit=${limit}&offset=${offset}&leaderboardId=${leaderboardId}`, {
      baseURL: this.url,
    });

    return data.data;
  }

  async getUser({
    id,
    userId,
  }: {
    id: string;
    userId: string;
  }): Promise<IGetLeaderboardUserResponse> {
    const { data } = await axiosInstance.get<
      { data: IGetLeaderboardUserResponse },
      AxiosResponse<{ data: IGetLeaderboardUserResponse }>
    >(`/api/v1/leaderboard/${id}/users/${userId}`, { baseURL: this.url });

    return data.data;
  }

  async seasons({
    address,
    chainId,
  }: {
    address: string;
    chainId: number;
  }): Promise<IFindLeaderboardSeasonsResponse> {
    const { data } = await axiosInstance.get<
      { data: IFindLeaderboardSeasonsResponse },
      AxiosResponse<{ data: IFindLeaderboardSeasonsResponse }>
    >(`/api/v1/leaderboard/seasons`, {
      baseURL: this.url,
      params: { chainId },
    });

    return data.data;
  }
}

// 'http://127.0.0.1:4000' ||
export const leaderboardApi = new LeaderboardApi({ url: 'https://api.hellopixel.network' });
