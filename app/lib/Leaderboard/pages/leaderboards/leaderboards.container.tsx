import React from 'react';
import AbProvider from 'lib/ab/context/ab.provider.tsx';
import LeaderboardsPage from 'lib/Leaderboard/pages/leaderboards/leaderboards.page.tsx';

export default function () {
  return (
    <AbProvider>
      <LeaderboardsPage />
    </AbProvider>
  );
}
