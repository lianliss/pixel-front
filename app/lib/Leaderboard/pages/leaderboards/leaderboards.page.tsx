import React, { useEffect, useMemo } from 'react';
import styles from './leaderboards.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { LeaderboardToolbar } from 'lib/Leaderboard/pages/leaderboards/components/leaderboard-toolbar/LeaderboardToolbar.tsx';
import { LeaderboardContent } from 'lib/Leaderboard/pages/leaderboards/components/leaderboard-content/LeaderboardContent.tsx';
import { useUnit } from 'effector-react';
import {
  leaderboardsSeasonsStore,
  leaderboardsStore,
  loadLeaderboards,
  loadLeaderboardsSeasons,
} from 'lib/Leaderboard/api-server/leaderboard.store.ts';
import toaster from 'services/toaster.tsx';
import { LeaderBoardGroup, LeaderboardType } from 'lib/Leaderboard/api-server/leaderboard.types.ts';
import Badge from '@mui/material/Badge';
import { Chain, COSTON2, SKALE_TEST, SONGBIRD } from 'services/multichain/chains';
import NotAvailableChain from '../../../../shared/blocks/NotAvailableChain/NotAvailableChain.tsx';
import { IconButton } from '@ui-kit/IconButton/IconButton.tsx';
import FilterListIcon from '@mui/icons-material/FilterList';
import {
  ILeaderboardFilterDialogState,
  LeaderboardFilterDialog,
} from 'lib/Leaderboard/components/LeaderboardFilterDialog/LeaderboardFilterDialog.tsx';
import { isLeaderboardFilterChanged } from 'lib/Leaderboard/components/LeaderboardFilterDialog/utils.ts';
import { useMounted } from '../../../../shared/hooks/useMounted.ts';
import { useAb } from 'lib/ab/context/ab.context.ts';
import { TechnicalWorks } from 'ui/TechnicalWorks/TechnicalWorks.tsx';
import { LeaderboardHeader } from 'lib/Leaderboard/pages/leaderboards/components/leaderboard-header/LeaderboardHeader.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import { useTranslation } from 'react-i18next';
import Container from '@mui/material/Container';

const AVAILABLE_CHAINS = [Chain.SKALE_TEST, Chain.SKALE, Chain.SONGBIRD, Chain.UNITS];

function LeaderboardsPage() {
  const ab = useAb();
  const account = useAccount();
  const { chainId, accountAddress } = account;
  const mounted = useMounted();

  const { t } = useTranslation('leaderboard');
  const { t: n } = useTranslation('notifications');

  // stores
  const leaderboards = useUnit(leaderboardsStore);
  const loading = useUnit(loadLeaderboards.pending);
  const seasons = useUnit(leaderboardsSeasonsStore);

  // states
  const [activeLeaderboard, setActiveLeaderboard] = React.useState<LeaderboardType>(
    LeaderboardType.ReferralCount
  );
  // Фильтрация лидербордов в диалоговом окне
  const [filterOpen, setFilterOpen] = React.useState(false);
  const [filterState, setFilterState] = React.useState<ILeaderboardFilterDialogState>({
    group: LeaderBoardGroup.Default,
    season: '-1',
  });

  // memos
  const leaderboard = useMemo(() => {
    return leaderboards.find((el) => el.type === activeLeaderboard);
  }, [leaderboards, activeLeaderboard]);

  // effects
  useEffect(() => {
    if (!accountAddress) {
      return;
    }

    loadLeaderboards({
      limit: 10,
      offset: 0,
      address: accountAddress,
      group: filterState.group,
      chainId,
      seasonId: filterState.season === '-1' ? undefined : filterState.season,
    }).catch((error) => {
      (toaster as any).captureException(error, n('Leaderboard loading failed'));
    });
  }, [accountAddress, filterState, chainId]);
  useEffect(() => {
    loadLeaderboardsSeasons({ address: accountAddress, chainId }).catch((error) => {
      (toaster as any).captureException(error, n('Leaderboard seasons loading failed'));
    });
  }, [chainId]);

  useEffect(() => {
    if (leaderboards[0] && !leaderboards.find((el) => el.type === activeLeaderboard)) {
      setActiveLeaderboard(leaderboards[0].type);
    }
  }, [leaderboards, activeLeaderboard]);

  const onChangeActiveLeaderboard = (type: LeaderboardType) => {
    setActiveLeaderboard(type);
    Telegram.WebApp.HapticFeedback.impactOccurred('light');
  };

  if (chainId && !AVAILABLE_CHAINS.includes(chainId)) {
    return <NotAvailableChain title={t('Not available on this Network')} />;
  }
  if (ab.isEnabled('leaderboard_tech_works')) {
    return <TechnicalWorks />;
  }

  return (
    <div className={styles.screen}>
      <Container maxWidth='xs' disableGutters>
        <div className={styles.toolbar}>
          <Badge
            variant={isLeaderboardFilterChanged(filterState) ? 'dot' : undefined}
            color='success'
          >
            <IconButton
              color='primary'
              variant='outlined'
              shape='square'
              onClick={() => {
                setFilterOpen(true);
                Telegram.WebApp.HapticFeedback.impactOccurred('light');
              }}
            >
              <FilterListIcon />
            </IconButton>
          </Badge>

          <Typography variant='h2' fontWeight='bold' align='center'>
            {t('Leaderboard')}
          </Typography>

          <ChainSwitcher chains={AVAILABLE_CHAINS} isRelative />
        </div>

        {!leaderboard && !loading && mounted && (
          <Typography align='center' style={{ marginTop: 16 }}>
            {t('No leaderboards')}
          </Typography>
        )}

        <>
          {/* Prize / Started Date / Reset Date */}
          {leaderboard?.group !== LeaderBoardGroup.Total && (
            <LeaderboardHeader
              leaderboard={leaderboard}
              loading={loading}
              seasons={seasons}
              chainId={chainId}
            />
          )}

          {/* Type Leaderboards tabs */}
          <LeaderboardToolbar
            loading={loading}
            leaderboards={leaderboards}
            activeLeaderboard={activeLeaderboard}
            onChangeActiveLeaderboard={onChangeActiveLeaderboard}
          />

          {leaderboard && (
            /*Content / Users */
            <LeaderboardContent leaderboard={leaderboard} />
          )}
        </>
      </Container>

      <LeaderboardFilterDialog
        open={filterOpen}
        onClose={() => setFilterOpen(false)}
        state={filterState}
        onChange={(v) => {
          setFilterState(v);
          Telegram.WebApp.HapticFeedback.impactOccurred('medium');
        }}
        seasons={seasons}
        isElders={chainId === Chain.SONGBIRD}
      />
    </div>
  );
}

export default LeaderboardsPage;
