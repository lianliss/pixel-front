import { LeaderboardType } from 'lib/Leaderboard/api-server/leaderboard.types.ts';

export const getPercentFromTheFund = (fund: number, reward: number) => {
  if (!reward || !fund) return '0%';
  return `${((reward / fund) * 100)?.toFixed(2)}%` ?? '0%';
};

export function calcLeaderboardReward(payload: {
  places: number;
  place: number;
  totalAmount: number;
}): number {
  const top = payload.places - payload.place + 1;
  const bottom1 = payload.places * (payload.places + 1);
  const bottom2 = bottom1 / 2;

  return +((top / bottom2) * payload.totalAmount).toFixed(2);
}

export function getPointPrefixLeaderboard(leaderboardType: LeaderboardType, chainId: number) {
  switch (leaderboardType) {
    case LeaderboardType.Transactions:
      return 'Txs';
    case LeaderboardType.ReferralCount:
      return 'Ref';
    case LeaderboardType.TradeVolume:
      return chainId === 19 ? 'SGB' : 'sFUEL';
    default:
      return 'PXLs';
  }
}
