import styles from './style.module.scss';
import logo from '../../../../../../../styles/svg/logo_icon.svg';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { formatNumberSpacing } from 'utils/number';
import { formatTimestamp } from 'lib/Leaderboard/utils/formatDate.ts';
import { IntervalRender } from '@ui-kit/IntervalRender/IntervalRender.tsx';
import {
  getLeaderboardDateStr,
  getLeaderboardStartDateStr,
} from 'lib/Leaderboard/utils/getLeaderboardDateStr.ts';
import React from 'react';
import {
  ILeaderboardDto,
  ILeaderboardSeasonDto,
} from 'lib/Leaderboard/api-server/leaderboard.types.ts';
import Skeleton from '@mui/material/Skeleton';
import { isLeaderboardStarted } from '../../../../utils/index.ts';
import clsx from 'clsx';
import CheckIcon from '@mui/icons-material/Check';
import Paper from '@mui/material/Paper';
import { darken, styled, alpha } from '@mui/material/styles';
import { Chain } from 'services/multichain/chains';
import { useTranslation } from 'react-i18next';

const StyledSkeletonRoot = styled('div')(({ theme }) => ({
  border: `solid 1px ${theme.palette.primary.main}`,
  background: `linear-gradient(92.95deg, ${alpha(theme.palette.primary.main, 0.4)} 1.57%, ${alpha(
    darken(theme.palette.primary.main, 0.7),
    0.4
  )} 100%)`,

  [`&.chain${Chain.SKALE}`]: {
    background: `linear-gradient(92.95deg, ${alpha(
      theme.palette.primary.main,
      0.05
    )} 1.57%, ${alpha(darken(theme.palette.primary.main, 0.7), 0.4)} 100%)`,
  },

  '& > .MuiSkeleton-root': {
    backgroundColor: darken(theme.palette.background.paper, 0.3),
    border: `solid 1px ${theme.palette.primary.main}`,
  },
}));

const StyledRoot = styled(Paper)(({ theme }) => ({
  border: `solid 1px ${theme.palette.primary.main}`,
  background: `linear-gradient(92.95deg, ${alpha(theme.palette.primary.main, 0.4)} 1.57%, ${alpha(
    darken(theme.palette.primary.main, 0.7),
    0.4
  )} 100%)`,
  [`&.chain${Chain.SKALE}`]: {
    background: `linear-gradient(92.95deg, ${alpha(
      theme.palette.primary.main,
      0.05
    )} 1.57%, ${alpha(darken(theme.palette.primary.main, 0.7), 0.4)} 100%)`,
  },
}));
const StyledPaper = styled(Paper)(({ theme }) => ({
  backgroundColor: darken(theme.palette.background.paper, 0.3),
  border: `solid 1px ${theme.palette.primary.main}`,
}));

type Props = {
  leaderboard: ILeaderboardDto;
  loading: boolean; // load leaderboards
  seasons: ILeaderboardSeasonDto[];
  chainId?: number;
};
export const LeaderboardHeader = (props: Props) => {
  const { leaderboard, loading, seasons, chainId } = props;

  const { t } = useTranslation('leaderboard');

  /*todo: мы не учитываем тип награды, пока только pxls*/
  const rewardType = leaderboard?.rewards?.[0]?.type;
  const rewardContext = leaderboard?.rewards?.[0]?.context;
  const isNotStarted = !!leaderboard && !isLeaderboardStarted(leaderboard);

  const currentSeason = seasons.find((season) => String(season.id) === leaderboard?.seasonId);

  if (!leaderboard && loading) {
    return (
      <StyledSkeletonRoot
        className={clsx(styles.leaderboardHead__skeleton, chainId && `chain${chainId}`)}
      >
        <Skeleton className={styles.leaderboard__skeleton} variant='rectangular' />
      </StyledSkeletonRoot>
    );
  }
  if (!leaderboard && !loading) {
    return null;
  }

  return (
    <StyledRoot className={clsx(styles.leaderboard__head, chainId && `chain${chainId}`)}>
      <StyledPaper elevation={0} className={styles.prize__fund}>
        <Typography className={styles.title__prize} color='primary' fontWeight='bold' variant='h6'>
          {t('The Prize Fund')}
          {leaderboard.rewardedAt && <CheckIcon sx={{ color: 'success.main' }} />}
        </Typography>

        <div className={styles.rewards}>
          {/* Общая награда */}
          <div className={styles.info__section}>
            <Typography className={styles.title} variant='h4' light>
              {t('Season total')}
            </Typography>
            <Typography fontWeight='bold' color='primary'>
              {`${formatNumberSpacing(currentSeason?.pixelTotal ?? 0)} PXLs`}
            </Typography>
          </div>

          <img className={styles.icon} src={logo as string} alt='logo' />

          {/* Награда текущего лидерборда */}
          <div className={clsx(styles.info__section, styles.infoSectionRight)}>
            <Typography className={styles.title} variant='h4' light noWrap>
              {t('Current leaderboard')}
            </Typography>
            <Typography fontWeight='bold' sx={{ color: 'success.main' }}>
              {`${rewardContext?.amount ? formatNumberSpacing(rewardContext.amount) : '0'} ${
                rewardContext?.symbol ?? ''
              }`}
            </Typography>
          </div>
        </div>
      </StyledPaper>
      <div className={styles.time__area}>
        <div className={styles.startTime}>
          <Typography className={styles.label} variant='caption' light>
            {t('Started')}
          </Typography>
          <Typography className={styles.time} variant='caption' light>
            {formatTimestamp(leaderboard.startedAt)}
          </Typography>
        </div>
        {!isNotStarted && (
          <div className={styles.endTime}>
            <Typography className={styles.label} variant='caption' light>
              {t('Reset in')}
            </Typography>
            <IntervalRender
              render={() => (
                <Typography className={styles.time} variant='caption' light>
                  {getLeaderboardDateStr(leaderboard)}
                </Typography>
              )}
            />
          </div>
        )}
        {isNotStarted && (
          <div className={styles.endTime}>
            <Typography className={styles.label} variant='caption' light>
              {t('Time to start')}
            </Typography>
            <IntervalRender
              render={() => (
                <Typography className={styles.time} variant='caption' light>
                  {getLeaderboardStartDateStr(leaderboard)}
                </Typography>
              )}
            />
          </div>
        )}
      </div>
    </StyledRoot>
  );
};
