import React, { useMemo } from 'react';
import styles from './styles.module.scss';
import clsx from 'clsx';
import { ILeaderboardDto, LeaderboardType } from 'lib/Leaderboard/api-server/leaderboard.types.ts';
import Skeleton from '@mui/material/Skeleton';
import { darken, styled, lighten, alpha } from '@mui/material/styles';
// import { IS_DEVELOP, IS_STAGE } from '@cfg/config.ts';
import burnedTokensImage from '../../../../components/icons/BurnedTokens.svg';
import claimedTokensImage from '../../../../components/icons/ClaimedTokens.svg';
import referralCountImage from '../../../../components/icons/ReferralCount.svg';
import referralRewardsImage from '../../../../components/icons/ReferralRewards.svg';
import txImage from '../../../../components/icons/Transactions.svg';
import tradeVolumeImage from '../../../../components/icons/TradeVolume.svg';

const images = {
  [LeaderboardType.BurnedTokens]: burnedTokensImage,
  [LeaderboardType.ClaimedTokens]: claimedTokensImage,
  [LeaderboardType.ReferralCount]: referralCountImage,
  [LeaderboardType.ReferralRewards]: referralRewardsImage,
  [LeaderboardType.TradeVolume]: tradeVolumeImage,
  [LeaderboardType.Transactions]: txImage,
};

const StyledNavItem = styled('div')(({ theme }) => ({
  border: `solid 1px ${theme.palette.divider}`,
  backgroundColor: 'transparent',

  ['&.active']: {
    backgroundColor: alpha(theme.palette.primary.main, 0.2),
  },
}));

const LEADERBOARDS = [
  LeaderboardType.ReferralCount,
  LeaderboardType.ReferralRewards,
  LeaderboardType.ClaimedTokens,
  LeaderboardType.BurnedTokens,
  LeaderboardType.Transactions,
  LeaderboardType.TradeVolume,
];

interface Props {
  loading: boolean;
  leaderboards: ILeaderboardDto[];
  activeLeaderboard: LeaderboardType | null;
  onChangeActiveLeaderboard: (type: LeaderboardType) => void;
}

export const LeaderboardToolbar = (props: Props) => {
  const { activeLeaderboard, onChangeActiveLeaderboard, leaderboards = [], loading } = props;

  const availableBoards = useMemo(
    () => Object.fromEntries(leaderboards.map((board) => [board.type, true])),
    [leaderboards]
  );

  return (
    <>
      {!leaderboards.length && loading && (
        <div
          className={styles.toolbarPanel__skeleton}
          style={{
            gridTemplateColumns: `repeat(${LEADERBOARDS.length}, 1fr)`,
          }}
        >
          {LEADERBOARDS.map((item) => (
            <Skeleton key={`load__${item}`} className={styles.skeleton} variant='rectangular' />
          ))}
        </div>
      )}

      {leaderboards.length > 0 && (
        <div className={styles.toolbar__panel}>
          <nav className={styles.panel__nav}>
            {LEADERBOARDS.map((type, index) => {
              if (!availableBoards[type]) {
                return null;
              }

              const isActive = activeLeaderboard === type;

              return (
                <StyledNavItem
                  key={`nav-key-${type}`}
                  className={clsx(styles.nav__item, { active: isActive })}
                  onClick={() => onChangeActiveLeaderboard(type)}
                >
                  <img className={styles.icon} src={images[type]} alt='icon' />
                </StyledNavItem>
              );
            })}
          </nav>
        </div>
      )}
    </>
  );
};
