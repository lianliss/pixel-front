import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

const PlaceIcon = (props: React.SVGProps<SVGElement>) => {
  return (
    <SvgIcon>
      <svg
        width='8'
        height='8'
        viewBox='0 0 8 8'
        fill='currentColor'
        xmlns='http://www.w3.org/2000/svg'
        {...props}
      >
        <rect y='3' width='2' height='5' rx='1' fill='currentColor' />
        <rect x='3' width='2' height='8' rx='1' fill='currentColor' />
        <rect x='6' y='4' width='2' height='4' rx='1' fill='currentColor' />
      </svg>
    </SvgIcon>
  );
};

export default PlaceIcon;
