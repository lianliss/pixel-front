import React, { useMemo } from 'react';
import styles from './LeaderboardUserRow.module.scss';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import {
  ILeaderboardUserDto,
  LeaderboardType,
} from 'lib/Leaderboard/api-server/leaderboard.types.ts';
import PlaceIcon from './Icon/PlaceIcon.tsx';
import clsx from 'clsx';
import logo from '../../../../../../../styles/svg/logo_icon.svg';
import { formatNumberSpacing } from 'utils/number';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import { getPercentFromTheFund } from 'lib/Leaderboard/pages/leaderboards/utils.ts';
import { useTranslation } from 'react-i18next';
import { alpha, styled } from '@mui/material/styles';
import Box from '@mui/material/Box';

const StyledRoot = styled(Box)(({ theme }) => ({
  variants: [
    {
      props: {
        status: 'active',
      },
      style: {
        opacity: 1,
        backgroundColor: alpha(theme.palette.primary.main, 0.3),
      },
    },
    {
      props: {
        status: 'none',
      },
      style: {
        opacity: 0.5,
        backgroundColor: alpha(theme.palette.primary.main, 0.3),
      },
    },
  ],
}));

interface Props {
  user: ILeaderboardUserDto | null;
  reward: number;
  isRewardPlace: boolean;
  isYou: boolean;
  pointPrefix: string;
  fund?: number;
  type: LeaderboardType;
  redirectProfile?: () => void;
}

const formatUserName = (username: string) => {
  if (username) {
    return username.length > 15 ? username.slice(0, 15) + '..' : username;
  }

  return undefined;
};

const LeaderboardUserRow = ({
  user,
  reward = 0,
  isYou = true,
  isRewardPlace = false,
  pointPrefix,
  fund = 0,
  type,
  redirectProfile,
}: Props) => {
  const position = user?.position;
  const isBroken = useMemo(() => {
    return user?.updatedAt && new Date().getTime() - user.updatedAt > 1000 * 60 * 60 * 16;
  }, [user]);

  const { t } = useTranslation('leaderboard');

  const multPoints = type === LeaderboardType.BurnedTokens ? 2 : 1;
  const ownerState = {
    status: isYou ? (user ? 'active' : 'none') : undefined,
  };

  return (
    <StyledRoot
      className={clsx(styles.user__placeCard, (!user || isYou) && styles.user__current)}
      ownerState={ownerState}
      aria-label={String(!!user)}
    >
      <div className={styles.col}>
        <div className={styles.wrap}>
          <Avatar
            sx={{ width: 56, height: 56, fontSize: position > 10_000 ? 13 : 20 }}
            variantStyle='outlined'
            onClick={redirectProfile}
            src={user?.userInfo?.image}
          >
            {position || '-'}
          </Avatar>

          <div className={styles.user__info}>
            {user && (
              <Typography
                variant='caption'
                color={isRewardPlace ? 'primary.main' : 'text.primary'}
                onClick={redirectProfile}
              >
                <PlaceIcon style={{ fontSize: 8, color: isBroken ? 'error.main' : undefined }} />{' '}
                {user?.position}
              </Typography>
            )}
            {user ? (
              <Typography
                color='text.primary'
                onClick={redirectProfile}
                sx={{ textDecoration: 'underline' }}
              >
                {!isYou
                  ? formatUserName(user.username) ||
                    user?.userId.slice(0, 8) + '...' + user?.userId.slice(-5)
                  : t('You')}
              </Typography>
            ) : (
              <Typography color='text.primary'>{t('You are not participating')}</Typography>
            )}
            <div className={styles.balance}>
              {reward > 0 && (
                <>
                  <img className={styles.pixel} src={logo as string} alt='logo' />
                  <Typography variant='caption' color='text.secondary'>
                    {isRewardPlace ? reward : t('This a not prize place')}
                  </Typography>
                  {reward && (
                    <Typography color='primary.main' className={styles.percent} variant='caption'>
                      ({getPercentFromTheFund(fund, reward)})
                    </Typography>
                  )}
                </>
              )}
            </div>
          </div>
        </div>
      </div>
      <div className={styles.col}>
        <Typography className={styles.points} noWrap color='success.main'>
          {user?.points ? formatNumberSpacing(user.points * multPoints) : 0} {pointPrefix}
        </Typography>
      </div>
    </StyledRoot>
  );
};

export default LeaderboardUserRow;
