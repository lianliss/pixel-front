import { useState, useEffect } from 'react';

const limit = 5;
const usePagination = (initialPage = 1, totalItems) => {
  const [page, setPage] = useState(initialPage);
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    setPage(1);
    setOffset(0);
  }, []);

  const handlePageChange = (newPage) => {
    setPage(newPage);
    setOffset((newPage - 1) * limit);
  };

  const onNextPage = () => handlePageChange(page + 1);
  const onPrevPage = () => handlePageChange(page - 1);
  const onFirstPage = () => handlePageChange(1);
  const onLastPage = () => {
    const totalPages = Math.ceil(totalItems / limit);
    handlePageChange(totalPages);
  };

  return {
    page,
    offset,
    limit,
    onNextPage,
    onPrevPage,
    onFirstPage,
    onLastPage,
    setPage,
    setOffset,
    handlePageChange,
    reset: () => {
      setPage(1);
      setOffset(0);
    },
  };
};

export default usePagination;
