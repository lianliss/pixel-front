import React, { useCallback, useEffect, useState } from 'react';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

import styles from './styles.module.scss';

import {
  leaderboardUsersStore,
  leaderboardUserStore,
  loadLeaderboardUser,
  loadLeaderboardUsers,
} from 'lib/Leaderboard/api-server/leaderboard.store.ts';
import toaster from 'services/toaster.tsx';
import { useUnit } from 'effector-react';
import LeaderboardUserRow from 'lib/Leaderboard/pages/leaderboards/components/leaderboard-user-row/LeaderboardUserRow.tsx';
import LinearProgress from '@mui/material/LinearProgress';
import {
  IGetLeaderboardUserResponse,
  ILeaderboardDto,
  ILeaderboardUserDto,
} from 'lib/Leaderboard/api-server/leaderboard.types.ts';
import {
  calcLeaderboardReward,
  getPointPrefixLeaderboard,
} from 'lib/Leaderboard/pages/leaderboards/utils.ts';
import { useInterval } from 'utils/hooks/useInterval';
import usePagination from 'lib/Leaderboard/pages/leaderboards/components/leaderboard-content/usePagination.ts';
import { LeaderboardUsersPagination } from 'lib/Leaderboard/pages/leaderboards/components/leaderboard-content/leaderboard-users-pagination/LeaderboardUsersPagination.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { darken, styled } from '@mui/material/styles';
import { IPaginatedResult } from 'lib/Quests/api-server/quests.types.ts';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { useBackAction } from '../../../../../../shared/telegram/useBackAction.ts';
import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';

const StyledTitle = styled('div')(({ theme }) => ({
  backgroundColor: darken(theme.palette.background.default, 0.1),
  padding: '12px',
}));

interface Props {
  leaderboard: ILeaderboardDto;
}

export const LeaderboardContent = ({ leaderboard }: Props) => {
  const account = useAccount();
  const navigate = useNavigate();
  const accountAddress = account.accountAddress?.toLowerCase();

  const { t } = useTranslation('leaderboard');

  const [pulling, setPulling] = React.useState(false);
  const leaderboardUsersMap = useUnit(leaderboardUsersStore);
  const leaderboardUsers: IPaginatedResult<ILeaderboardUserDto> = leaderboardUsersMap[
    leaderboard.id
  ] || { rows: [], count: 0 };
  const isLoading = useUnit(loadLeaderboardUsers.pending);
  const leaderboardUserMap = useUnit(leaderboardUserStore);
  const leaderboardUser: IGetLeaderboardUserResponse | null =
    leaderboardUserMap[leaderboard.id] || null;

  const rewardContext = leaderboard.rewards?.[0]?.context;

  const {
    page,
    offset,
    limit,
    reset: resetPagination,
    ...propsPaginate
  } = usePagination(1, leaderboardUsers.count);

  const callLoadLeaderboardsData = useCallback(async () => {
    try {
      await loadLeaderboardUsers({ leaderboardId: leaderboard.id, limit, offset });
      if (accountAddress) {
        await loadLeaderboardUser({ id: leaderboard.id, userId: accountAddress });
      }
    } catch (error) {
      (toaster as unknown).captureException(error, 'Leaderboard user loading failed');
    }
  }, [leaderboard, accountAddress, offset]);

  useEffect(() => {
    void callLoadLeaderboardsData();
  }, [leaderboard, offset, accountAddress]);
  useEffect(() => {
    resetPagination();
  }, [leaderboard]);

  useInterval(
    async () => {
      setPulling(true);
      await callLoadLeaderboardsData();
      setPulling(false);
    },
    30000,
    [callLoadLeaderboardsData]
  );
  useEffect(() => {
    propsPaginate.onFirstPage();
  }, [leaderboard]);
  useBackAction('/wallet');

  const isAccountInCurrentPage = leaderboardUsers.rows.some((row) => row.userId === accountAddress);
  const isRewardPlace = leaderboardUser?.position <= leaderboard.rewardPlaces;
  const userReward =
    calcLeaderboardReward({
      places: leaderboard.rewardPlaces,
      place: leaderboardUser?.position || 100_000,
      totalAmount: rewardContext?.amount || 0,
    }) || 0;

  const pointPrefix = getPointPrefixLeaderboard(leaderboard.type, account.chainId);
  const isUpPosition = leaderboardUser?.position < page * limit;

  const userRow = !isAccountInCurrentPage && (
    <LeaderboardUserRow
      user={leaderboardUser}
      isYou
      reward={userReward}
      isRewardPlace={isRewardPlace}
      pointPrefix={pointPrefix}
      fund={rewardContext?.amount}
      type={leaderboard.type}
    />
  );

  return (
    <>
      <div className={styles.content}>
        <StyledTitle>
          <Typography variant='h3' color='text.primary' fontWeight='bold' align='center' noWrap>
            {t(leaderboard.title)}
          </Typography>
        </StyledTitle>

        <div className={styles.linear__progress}>{isLoading && !pulling && <LinearProgress />}</div>

        {isUpPosition && userRow}

        {leaderboardUsers.rows.length > 0 && (
          <>
            <Divider />
            <Stack direction='column' divider={<Divider />} className={styles.users__list}>
              {leaderboardUsers.rows.map((user, index) => {
                const currentUser = accountAddress ? user.userId === accountAddress : false;
                const isRewardPlace = user.position <= leaderboard.rewardPlaces;
                const reward = calcLeaderboardReward({
                  places: leaderboard.rewardPlaces,
                  place: user.position,
                  totalAmount: rewardContext?.amount || 0,
                });
                return (
                  <React.Fragment key={`place-item-${user.id}`}>
                    <LeaderboardUserRow
                      user={user}
                      isYou={currentUser}
                      reward={reward}
                      isRewardPlace={isRewardPlace}
                      pointPrefix={pointPrefix}
                      fund={rewardContext?.amount || 0}
                      type={leaderboard.type}
                      redirectProfile={() => {
                        // backAction.createReturn();
                        navigate(`/profile/${user.userId}`);
                      }}
                    />
                  </React.Fragment>
                );
              })}
            </Stack>
            <Divider />
          </>
        )}

        {/* Если текущий юзер в списке на этой странице то доп карточку не показываем */}
        {!isUpPosition && userRow}

        <div className={styles.linear__progress}>{isLoading && !pulling && <LinearProgress />}</div>

        {/* Pagination */}
        {leaderboardUsers.count > 0 && (
          <LeaderboardUsersPagination
            limit={limit}
            page={page}
            leaderboardUser={leaderboardUser}
            leaderboardUsers={leaderboardUsers}
            {...propsPaginate}
          />
        )}
      </div>
    </>
  );
};
