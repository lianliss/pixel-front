import React from 'react';

import styles from './styles.module.scss';
import { Button } from '@ui-kit/Button/Button.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import UserIcon from 'lib/Leaderboard/pages/leaderboards/components/leaderboard-content/icon/UserIcon.tsx';
import { ILeaderboardUserDto } from 'lib/Leaderboard/api-server/leaderboard.types.ts';
import { IPaginatedResult } from 'lib/Quests/api-server/quests.types.ts';
import { useTranslation } from 'react-i18next';

type Props = {
  limit: number;
  page: number;
  handlePageChange: (page: number) => void;
  onFirstPage: () => void;
  onPrevPage: () => void;
  onNextPage: () => void;
  onLastPage: () => void;
  leaderboardUser: ILeaderboardUserDto;
  leaderboardUsers: IPaginatedResult<ILeaderboardUserDto>;
};
export const LeaderboardUsersPagination = (props: Props) => {
  const {
    limit,
    page,
    onFirstPage,
    onPrevPage,
    onLastPage,
    onNextPage,
    leaderboardUser,
    leaderboardUsers,
    handlePageChange,
  } = props;
  const { t } = useTranslation('common');

  const onMePage = () => {
    if (leaderboardUser) {
      const pageNumber = Math.ceil(leaderboardUser.position / limit);
      handlePageChange(pageNumber);
    }
  };
  return (
    <div className={styles.pagination}>
      <Button color='primary' variant='contained' disabled={page === 1} onClick={onFirstPage}>
        <Typography noWrap variant='caption'>
          {`< ${t('First')}`}
        </Typography>
      </Button>
      <Button color='primary' variant='contained' disabled={page === 1} onClick={onPrevPage}>
        <Typography variant='caption'>{limit + ' <'}</Typography>
      </Button>

      <Button color='primary' variant='contained' disabled={!leaderboardUser} onClick={onMePage}>
        <UserIcon />
      </Button>

      <Button
        color='primary'
        variant='contained'
        disabled={
          leaderboardUsers.count <= limit || Math.ceil(leaderboardUsers.count / limit) === page
        }
        onClick={onNextPage}
      >
        <Typography variant='caption'>{limit + ' >'}</Typography>
      </Button>
      <Button
        color='primary'
        variant='contained'
        disabled={
          leaderboardUsers.count <= limit || Math.ceil(leaderboardUsers.count / limit) === page
        }
        onClick={onLastPage}
      >
        <Typography noWrap variant='caption'>
          {`${t('Last')} >`}
        </Typography>
      </Button>
    </div>
  );
};
