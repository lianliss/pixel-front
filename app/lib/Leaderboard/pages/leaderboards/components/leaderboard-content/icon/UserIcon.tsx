import React from 'react';

const UserIcon = () => {
  return (
    <svg width='24' height='22' viewBox='0 0 24 22' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <circle
        cx='12'
        cy='7.31704'
        r='5.94595'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
        stroke='currentColor'
      />
      <path
        d='M0.999975 20.6288C2.78068 16.3064 7.035 13.2637 12 13.2637C16.965 13.2637 21.2193 16.3064 23 20.6288'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
        stroke='currentColor'
      />
    </svg>
  );
};

export default UserIcon;
