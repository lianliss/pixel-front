import moment from 'moment';
import { ILeaderboardDto } from 'lib/Leaderboard/api-server/leaderboard.types.ts';

export function isLeaderboardStarted(leaderboard: ILeaderboardDto) {
  const now = moment().utc().utcOffset('+03:00').toDate();
  const target = moment(new Date(leaderboard.startedAt))
    .subtract(moment().utcOffset(), 'm')
    .toDate();

  return target <= now;
}
