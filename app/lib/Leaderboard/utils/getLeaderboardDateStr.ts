import { ILeaderboardDto } from 'lib/Leaderboard/api-server/leaderboard.types.ts';
import { diffTime } from 'utils/format/date';

export function getLeaderboardDateStr(board: ILeaderboardDto): string | null {
  const end = board.endedAt;

  if (!end) {
    return null;
  }

  const endDate = new Date(end);
  const endUtc = Date.UTC(
    endDate.getUTCFullYear(),
    endDate.getUTCMonth(),
    endDate.getUTCDate(),
    endDate.getUTCHours(),
    endDate.getUTCMinutes(),
    endDate.getUTCSeconds()
  );

  const nowUtc = Date.now();

  if (end < nowUtc) {
    return new Date(end).toLocaleDateString('ru-RU');
  }

  const timeDiff = endUtc - nowUtc;

  const days = Math.floor(timeDiff / (1000 * 3600 * 24));
  const hours = Math.floor((timeDiff % (1000 * 3600 * 24)) / (1000 * 3600));
  const minutes = Math.floor((timeDiff % (1000 * 3600)) / (1000 * 60));
  const seconds = Math.floor((timeDiff % (1000 * 60)) / 1000);
  return `${days}d ${hours}h ${minutes}m ${seconds}s`;

  // if (end < new Date().getTime()) {
  //   return new Date(end).toLocaleDateString('ru-RU');
  // }
  //
  // return diffTime(new Date(end));
}

export function getLeaderboardStartDateStr(board: ILeaderboardDto): string | null {
  const target = board.startedAt;

  if (!target) {
    return null;
  }

  if (target < new Date().getTime()) {
    return new Date(target).toLocaleDateString('ru-RU');
  }

  return diffTime(new Date(target));
}
