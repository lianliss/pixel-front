import React from 'react';
import PropTypes from 'prop-types';

import { ButtonDeprecated as Button } from 'ui';
import networkTypes from './constants/networks';

import './TestnetOverlay.scss';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useSwitchChain } from '@chain/hooks/useSwitchChain.ts';

const TestnetOverlay = (props) => {
  const account = useAccount();
  const { switchToChain } = useSwitchChain();
  const { chainId, isConnected } = account;

  const Body = ({ isChainChanger }) => {
    let text = 'Connection error';

    return (
      <div className='TestnetOverlay__wrap'>
        <div className='TestnetOverlay__background'></div>
        <div className='TestnetOverlay__content'>
          <h2>{text}</h2>
          <p>
            {'Please switch chain to'}&nbsp;
            {networkTypes[19]}.
          </p>
          <div className='TestnetOverlay__buttons'>
            {chainId && (
              <Button
                onClick={() => {
                  switchToChain(19);
                }}
                large
                primary
              >
                Switch your chain
              </Button>
            )}
          </div>
        </div>
      </div>
    );
  };

  if (isConnected && chainId !== 19) {
    return <Body isChainChanger />;
  } else {
    return <></>;
  }
};

TestnetOverlay.propTypes = {
  networks: PropTypes.array,
};

TestnetOverlay.defaultProps = {
  networks: [],
};

export default TestnetOverlay;
