import SGB_1 from 'assets/img/telegram-stars/sgb/SGB_1.png';
import SGB_2 from 'assets/img/telegram-stars/sgb/SGB_2.png';
import SGB_3 from 'assets/img/telegram-stars/sgb/SGB_3.png';
import SGB_4 from 'assets/img/telegram-stars/sgb/SGB_4.png';
import SGB_5 from 'assets/img/telegram-stars/sgb/SGB_5.png';
import SGB_6 from 'assets/img/telegram-stars/sgb/SGB_6.png';
import SGB_7 from 'assets/img/telegram-stars/sgb/SGB_7.png';

import SKALE_1 from 'assets/img/telegram-stars/skale/SKL01.png';
import SKALE_2 from 'assets/img/telegram-stars/skale/SKL02.png';
import SKALE_3 from 'assets/img/telegram-stars/skale/SKL03.png';
import SKALE_4 from 'assets/img/telegram-stars/skale/SKL04.png';
import SKALE_5 from 'assets/img/telegram-stars/skale/SKL05.png';
import SKALE_6 from 'assets/img/telegram-stars/skale/SKL06.png';
import SKALE_7 from 'assets/img/telegram-stars/skale/SKL07.png';
import { Chain } from 'services/multichain/chains';
import { SKALE_TOKEN } from 'services/multichain/token.constants.ts';

export const BUY_TOKEN_FOR_STARS_INFO = {
  [Chain.SONGBIRD]: {
    token: '0x0000000000000000000000000000000000000000',
    images: {
      0: SGB_1,
      1: SGB_2,
      2: SGB_3,
      3: SGB_4,
      4: SGB_5,
      5: SGB_6,
      6: SGB_7,
    },
  },
  [Chain.SKALE]: {
    token: SKALE_TOKEN.address,
    images: {
      0: SKALE_1,
      1: SKALE_2,
      2: SKALE_3,
      3: SKALE_4,
      4: SKALE_5,
      5: SKALE_6,
      6: SKALE_7,
    },
  },
};

export const BUY_TOKEN_FOR_STARS_IMAGE: Partial<Record<Chain, string>> = {
  [Chain.SONGBIRD]: require('assets/img/telegram-stars/sgb/preview.png'),
  [Chain.SKALE]: require('assets/img/telegram-stars/skale/preview.png'),
};
