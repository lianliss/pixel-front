import React from 'react';
import toaster from 'services/toaster.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain } from 'services/multichain/chains';
import { useUnit } from 'effector-react';
import {
  createStarsInvoiceEffect,
  loadStarsPricesEffect,
  starsStore,
} from 'lib/TelegramStars/api-server/stars.store.ts';
import { IS_DEVELOP } from '@cfg/config.ts';

function useStars({ skip }: { skip?: boolean }) {
  const { chainId, accountAddress } = useAccount();

  const starsPrices = useUnit(starsStore);

  const isValidNetwork = [Chain.SONGBIRD, Chain.SKALE].includes(chainId);
  const isStarsAvailable = !!Object.keys(starsPrices).length && isValidNetwork;

  React.useEffect(() => {
    starsStore.reset();
  }, [chainId]);
  React.useEffect(() => {
    if (isValidNetwork && !skip) {
      loadStarsPricesEffect({ address: accountAddress, chainId }).catch(console.error);
    }
  }, [chainId, accountAddress, skip]);

  const openInvoice = async (starsAmount: number) => {
    return new Promise(async (resolve, reject) => {
      try {
        const data = await createStarsInvoiceEffect({
          address: accountAddress,
          amount: starsAmount,
          chainId,
        });
        // const data = await apiStarsInvoice(starsAmount);
        const { invoice } = data;

        Telegram.WebApp.openInvoice(invoice, (status) => {
          if (status === 'failed') {
            reject(new Error('failed'));
          }
          if (['paid', 'cancelled'].includes(status)) {
            resolve();
          }
        });
      } catch (error) {
        toaster.logError(error, '[onGetInvoice]');
        reject(error);
      }
    });
  };

  return {
    isStarsAvailable,
    openInvoice,
    prices: starsPrices,
  };
}

export default useStars;
