import useStars from './useStars.ts';
import { useMemo } from 'react';
import { SgbByuCard } from 'lib/TelegramStars/components/BuyTokenDialog/BuyTokenDialog.tsx';
import { BUY_TOKEN_FOR_STARS_INFO } from 'lib/TelegramStars/utils/images.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useToken } from '@chain/hooks/useToken.ts';
import { Chain } from 'services/multichain/chains';

function useBuyTokenForStars() {
  const account = useAccount();
  const stars = useStars({});

  const { images, token: tokenAddress } = BUY_TOKEN_FOR_STARS_INFO[account.chainId] || {
    images: [],
  };

  const token = useToken({
    address: tokenAddress,
  });

  const optionsWithPrice = useMemo(() => {
    const prices = Object.entries(stars.prices).map(
      ([stars, tokens], i): SgbByuCard => ({
        image: images[i] || images[0],
        stars: +stars,
        price: +tokens,
      })
    );

    return prices;
  }, [stars.prices, images]);

  const handleBuy = (payload: { amountSgb: number; starsAmount: number }) => {
    Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

    stars
      .openInvoice(payload.starsAmount)
      .then(() => {
        Telegram.WebApp.HapticFeedback.notificationOccurred('success');
      })
      .catch(() => {
        Telegram.WebApp.HapticFeedback.notificationOccurred('error');
      });
  };

  return {
    options: optionsWithPrice,
    handleBuy,
    token,
    available: stars.isStarsAvailable,
    isGas: [Chain.SONGBIRD].includes(account.chainId),
  };
}

export default useBuyTokenForStars;
