import React from 'react';
import styles from './styles.module.scss';

import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import Skeleton from '@mui/material/Skeleton';
import { useTranslation } from 'react-i18next';
import Box from '@mui/material/Box';
import IconTgStar from '@ui-kit/icon/common/IconTgStar.tsx';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';

const StyledPaper = styled(Paper)(({ theme }) => ({
  border: `solid 1px ${theme.palette.primary.main}`,
}));

type Props = {
  handler: () => void;
  symbol?: string;
  disabled?: boolean;
  image?: string;
};

export const BuyTokenCard = ({ handler, symbol, image, disabled = false }: Props) => {
  const { t } = useTranslation('buyTokens');
  return (
    <StyledPaper variant='outlined' elevation={0} className={styles.sgbBuy_card}>
      <Box sx={{ position: 'relative' }}>
        <Avatar src={`${image}`} sx={{ width: 56, height: 56 }} />
        <IconTgStar sx={{ position: 'absolute', bottom: 0, right: 0 }} />
      </Box>
      <div className={styles.card__info}>
        <Typography color='text.primary' className={styles.sgb}>
          {symbol ? symbol : <Skeleton />}
        </Typography>
        <Typography color='text.secondary' className={styles.description}>
          {t('Buy for Stars', { symbol })}
        </Typography>
      </div>
      <div className={styles.card__control}>
        <Button variant='contained' onClick={handler} disabled={!symbol || disabled}>
          <Typography variant='caption'>
            {t('Buy')} {symbol}
          </Typography>
        </Button>
      </div>
    </StyledPaper>
  );
};
