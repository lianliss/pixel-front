import React, { useMemo } from 'react';
import styles from './styles.module.scss';
import {
  BuyTokenDialog,
  SgbByuCard,
} from 'lib/TelegramStars/components/BuyTokenDialog/BuyTokenDialog.tsx';
import useStars from '../../../TelegramStars/hooks/useStars.ts';
import { useToken } from '@chain/hooks/useToken.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import {
  BUY_TOKEN_FOR_STARS_IMAGE,
  BUY_TOKEN_FOR_STARS_INFO,
} from 'lib/TelegramStars/utils/images.ts';
import { BuyTokenCard } from 'lib/TelegramStars/components/BuyTokenCard/BuyTokenCard.tsx';

type Props = {};

export const BuyTokenForStars = (props: Props) => {
  const [isOpenDialog, setOpenDialog] = React.useState(false);

  const account = useAccount();
  const { images, token: tokenAddress } = BUY_TOKEN_FOR_STARS_INFO[account.chainId] || {
    images: [],
  };

  const stars = useStars({});
  const token = useToken({
    address: tokenAddress,
  });
  const symbol = token.data?.symbol;

  const optionsWithPrice = useMemo(() => {
    const prices = Object.entries(stars.prices).map(
      ([stars, tokens], i): SgbByuCard => ({
        image: images[i] || images[0],
        stars: +stars,
        price: +tokens,
      })
    );

    return prices;
  }, [stars.prices, images]);
  const image = BUY_TOKEN_FOR_STARS_IMAGE[account.chainId];

  const onOpen = () => setOpenDialog(true);

  const onClose = () => setOpenDialog(false);

  const handleBuy = (payload: { amountSgb: number; starsAmount: number }) => {
    Telegram.WebApp.HapticFeedback.impactOccurred('heavy');

    stars
      .openInvoice(payload.starsAmount)
      .then(() => {
        Telegram.WebApp.HapticFeedback.notificationOccurred('success');
      })
      .catch(() => {
        Telegram.WebApp.HapticFeedback.notificationOccurred('error');
      });
  };

  return (
    <div className={styles.cards__wrapper}>
      <BuyTokenCard
        handler={onOpen}
        image={image}
        symbol={symbol}
        disabled={!stars.isStarsAvailable}
      />
      <BuyTokenDialog
        onClose={onClose}
        isOpen={isOpenDialog}
        onBuy={handleBuy}
        disabled={!stars.isStarsAvailable}
        symbol={symbol}
        options={optionsWithPrice}
      />
    </div>
  );
};
