import React from 'react';
import Box, { BoxProps } from '@mui/material/Box';
import Alert, { AlertProps } from '@mui/material/Alert';
import { BuyTokenDialog } from 'lib/TelegramStars/components/BuyTokenDialog/BuyTokenDialog.tsx';
import useBuyTokenForStars from 'lib/TelegramStars/hooks/useBuyTokenForStars.ts';
import { useTranslation } from 'react-i18next';
import { styled } from '@mui/material/styles';

const StyledRoot = styled(Box)(() => ({
  width: '100%',
  boxSizing: 'border-box',
}));

type Props = Omit<BoxProps, 'children'> & {
  variant?: AlertProps['severity'];
  text?: string;
  symbol?: string;
};

function BuyGasAlertComponent(props: Props) {
  const { t } = useTranslation('notifications');
  const { variant = 'outlined', text = t('No enough gas'), symbol, ...other } = props;

  const [open, setOpen] = React.useState(false);
  const { options, token, handleBuy, available, isGas } = useBuyTokenForStars();
  const isEnabled = available && isGas;

  return (
    <StyledRoot {...other}>
      <Alert
        severity='warning'
        variant={variant}
        onClick={isEnabled ? () => setOpen(true) : undefined}
      >{`${text} ${symbol ? `(${symbol})` : ''} ${
        isEnabled ? `, ${t('click to pay')}` : ''
      }`}</Alert>

      <BuyTokenDialog
        onClose={() => setOpen(false)}
        isOpen={open}
        onBuy={handleBuy}
        disabled={!isEnabled}
        symbol={token.data?.symbol || ''}
        options={options}
      />
    </StyledRoot>
  );
}

export default BuyGasAlertComponent;
