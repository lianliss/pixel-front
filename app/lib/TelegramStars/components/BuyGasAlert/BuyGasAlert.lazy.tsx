import React from 'react';

const BuyGasAlertLazy = React.lazy(() => import('./BuyGasAlert.tsx'));

export default BuyGasAlertLazy;
