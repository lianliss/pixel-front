import React from 'react';
import styles from './styles.module.scss';

import Skeleton from '@mui/material/Skeleton';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';
import IconTgStar from '@ui-kit/icon/common/IconTgStar.tsx';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import { useTranslation } from 'react-i18next';
import { SgbByuCard } from 'lib/TelegramStars/components/BuyTokenDialog/BuyTokenDialog.tsx';

type Props = {
  data: SgbByuCard;
  onBuy?: (payload: { starsAmount: number; amountSgb: number }) => void;
  disabled?: boolean;
  symbol?: string;
  index?: number;
};

const mapToTextLabel = {
  0: 'For New Players',
  2: 'Most Popular',
  4: 'Best Choice',
  6: 'Most Profitable',
};

export const BuyCard = ({ data, onBuy, symbol, index }: Props) => {
  const { image, stars, price, disabled = false } = data;
  const { t } = useTranslation('wallet');

  const handleBuy = () => {
    onBuy?.({ starsAmount: stars, amountSgb: price });
  };

  return (
    <div className={styles.sgbBuy_card}>
      <Avatar src={`${image}`} variant='square' sx={{ width: 56, height: 56 }} />
      <div className={styles.card__info}>
        {mapToTextLabel[index] && (
          <label className={styles.label} aria-label={`${index}`}>
            {mapToTextLabel[index]}
          </label>
        )}
        <Typography
          className={styles.amountSgb}
          fontWeight='bold'
          sx={{ display: 'flex', gap: '6px' }}
        >
          {price || '?'} <span>{symbol ? symbol : <Skeleton width='50px' />}</span>
        </Typography>
        <Typography className={styles.description} variant='caption' noWrap>
          <IconTgStar />
          {stars}
        </Typography>
      </div>
      <div className={styles.card__control}>
        <Button
          className={styles.button}
          variant='contained'
          fullWidth
          onClick={handleBuy}
          disabled={disabled || !price}
        >
          <Typography variant='caption' noWrap>
            {t('Buy for')} {stars}
          </Typography>
          <IconTgStar />
        </Button>
      </div>
    </div>
  );
};
