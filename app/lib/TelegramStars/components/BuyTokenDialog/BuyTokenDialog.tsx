import React from 'react';
import BottomDialog from '@ui-kit/BottomDialog/BottomDialog.tsx';
import styles from './styles.module.scss';
import { BuyCard } from './BuyCard/BuyCard.tsx';
import Skeleton from '@mui/material/Skeleton';
import { useTranslation } from 'react-i18next';
import { Typography } from '@ui-kit/Typography/Typography.tsx';

export interface SgbByuCard {
  image: string;
  stars: number;
  price?: number;
}

type Props = {
  isOpen: boolean;
  onClose: () => void;
  disabled?: boolean;
  options: SgbByuCard[];
  onBuy?: (payload: { starsAmount: number; amountSgb: number }) => void;
  symbol?: string;
};

export const BuyTokenDialog = ({
  isOpen,
  onClose,
  onBuy,
  disabled,
  symbol,
  options = [],
}: Props) => {
  const { t } = useTranslation('buyTokens');

  return (
    <BottomDialog open={isOpen} onClose={onClose}>
      <div className={styles.modal__content}>
        <Typography fontWeight='bold' align='center' sx={{ display: 'flex', gap: '4px', mb: 2 }}>
          {t('Buy')}{' '}
          {symbol ? <span>{symbol}</span> : <Skeleton width='50px' sx={{ m: '0 6px' }} />}
          {t('for Stars')}
        </Typography>

        <div className={styles.cards__list}>
          {options.map((card, i) => (
            <BuyCard
              key={`sgb-card-${i}`}
              data={card}
              onBuy={onBuy}
              index={i}
              disabled={disabled}
              symbol={symbol}
            />
          ))}
        </div>

        {!options.length && <p>No options</p>}
      </div>
    </BottomDialog>
  );
};
