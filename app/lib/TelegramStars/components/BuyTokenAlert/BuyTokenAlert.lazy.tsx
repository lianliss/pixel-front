import React from 'react';

const BuyTokenAlertLazy = React.lazy(() => import('./BuyTokenAlert.tsx'));

export default BuyTokenAlertLazy;
