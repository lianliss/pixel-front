import React from 'react';
import Box, { BoxProps } from '@mui/material/Box';
import Alert, { AlertProps } from '@mui/material/Alert';
import { BuyTokenDialog } from 'lib/TelegramStars/components/BuyTokenDialog/BuyTokenDialog.tsx';
import useBuyTokenForStars from 'lib/TelegramStars/hooks/useBuyTokenForStars.ts';
import { useTranslation } from 'react-i18next';

type Props = Omit<BoxProps, 'children'> & {
  variant?: AlertProps['severity'];
  text?: string;
  symbol?: string;
};

function BuyTokenAlertComponent(props: Props) {
  const { t } = useTranslation('notifications');
  const { variant = 'outlined', text = 'No enough tokens ({{symbol}})', symbol, ...other } = props;

  const [open, setOpen] = React.useState(false);
  const { options, token, handleBuy, available } = useBuyTokenForStars();

  return (
    <Box {...other}>
      <Alert
        severity='warning'
        variant={variant}
        onClick={available ? () => setOpen(true) : undefined}
      >
        {t(text + (available ? ', click to pay' : ''), { symbol })}
      </Alert>

      <BuyTokenDialog
        onClose={() => setOpen(false)}
        isOpen={open}
        onBuy={handleBuy}
        disabled={!available}
        symbol={token.data?.symbol || ''}
        options={options}
      />
    </Box>
  );
}

export default BuyTokenAlertComponent;
