'use strict';

import axios, { AxiosResponse } from 'axios';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';
import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';

import {
  pixelMessageKey,
  pixelSignKey,
  telegramAuthKey,
} from '../../../shared/const/localStorage.ts';
import { axiosInstance } from 'utils/libs/axios.ts';

export class StarsApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async loadPrices({
    address,
    chainId,
  }: {
    address: string;
    chainId: number;
  }): Promise<Record<string, number>> {
    const { data } = await axiosInstance.get<
      Record<string, number>,
      AxiosResponse<Record<string, number>>
    >(`/mining/stars`, { baseURL: this.url, params: { chainId } });
    return data;
  }

  async createInvoice({
    amount,
    address,
    chainId,
  }: {
    amount: number;
    address: string;
    chainId: number;
  }) {
    const { data } = await axiosInstance.get<
      { invoice: string },
      AxiosResponse<{ invoice: string }>
    >(`/mining/stars/invoice`, { baseURL: this.url, params: { amount, chainId } });
    return data;
  }
}

// 'http://127.0.0.1:4000' ||
export const starsApi = new StarsApi({ url: 'https://hellopixel.network/api' });
