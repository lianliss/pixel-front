import { createEffect, createStore } from 'effector';
import { starsApi } from 'lib/TelegramStars/api-server/stars.api.ts';

export const loadStarsPricesEffect = createEffect(
  async (params: { address: string; chainId: number }) => {
    const res = await starsApi.loadPrices(params);
    return res;
  }
);

export const createStarsInvoiceEffect = createEffect(
  async (params: { address: string; amount: number; chainId: number }) => {
    const res = await starsApi.createInvoice(params);
    return res;
  }
);

export const starsStore = createStore<Record<string, number>>({}).on(
  loadStarsPricesEffect.doneData,
  (_prev, next) => next
);
