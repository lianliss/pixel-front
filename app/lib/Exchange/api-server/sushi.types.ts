export type TTrueOrFalseString = 'true' | 'false';

export type ISushiApiParams = {
  tokenIn: string;
  tokenOut: string;
  amount: string;
  gasPrice: string;
  to: string;
  feeReceiver: string;
  fee: string;
};

export interface ISushiApiPayload extends ISushiApiParams {
  enableFee: TTrueOrFalseString;
  includeTransaction: TTrueOrFalseString;
  includeRoute: TTrueOrFalseString;
}

export type ISushiRouteStep = {
  assumedAmountIn: string;
  assumedAmountOut: string;
  liquidityProvider: string;
  poolAddress: string;
  poolFee: number;
  poolName: string;
  poolType: string;
  share: number;
  tokenFrom: number;
  tokenTo: number;
};

export type ISushiToken = {
  address: string;
  symbol: string;
  name: string;
  decimals: number;
};

export type ISushiTx = {
  data: string;
  from: string;
  to: string;
};

export type ISushiRoute = {
  amountIn: string;
  assumedAmountOut: string;
  gasSpent: number;
  priceImpact: number;
  route: [ISushiRouteStep];
  routeProcessorAddr: string;
  status: string;
  swapPrice: number;
  tokenFrom: number;
  tokenTo: number;
  tokens: [ISushiToken];
  tx: ISushiTx;
};
