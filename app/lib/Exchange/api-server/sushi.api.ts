'use strict';

import axios, { AxiosResponse } from 'axios';
import {
  ISushiApiParams,
  ISushiApiPayload,
  ISushiRoute,
} from 'lib/Exchange/api-server/sushi.types.ts';
import {
  IFindNftCollectionsDto,
  IFindNftCollectionsResponse,
} from 'lib/NFT/api-server/nft.types.ts';

export class SushiApi {
  url!: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async getBestRoute(chainId: number, payload: ISushiApiParams): Promise<ISushiRoute> {
    const params: ISushiApiPayload = {
      enableFee: 'true',
      includeTransaction: 'true',
      includeRoute: 'true',
      ...payload,
    };
    const response = await axios.get(`/${chainId}`, {
      baseURL: this.url,
      params,
    });

    return response.data;
  }
}

export const sushiApi = new SushiApi({ url: 'https://api.sushi.com/swap/v5' });
