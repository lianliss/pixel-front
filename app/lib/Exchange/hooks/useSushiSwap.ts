import React, { useEffect, useState } from 'react';
import wei from 'utils/wei';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import ERC_20_ABI from 'lib/Wallet/api-contract/abi/erc-20.abi.ts';
import { sushiApi } from 'lib/Exchange/api-server/sushi.api.ts';
import { ISushiRouteStep, ISushiTx } from 'lib/Exchange/api-server/sushi.types.ts';
import { useConfig, useSendTransaction } from 'wagmi';
import { wagmiConfig } from '@chain/wagmi.ts';
import { waitForTransactionReceipt } from '@wagmi/core';
import exchangeAnalytics from 'lib/Exchange/api-analytics';

function useSushiSwap({ fromToken, toToken }: { fromToken?: IToken; toToken?: IToken }) {
  const fromAddress = fromToken ? fromToken.address || fromToken.wrapAddress : null;
  const toAddress = toToken ? toToken.address || toToken.wrapAddress : null;

  const config = useConfig();
  const account = useAccount();
  const { sendTransactionAsync } = useSendTransaction();

  const [isApproving, setIsApproving] = React.useState(false);
  const [isProcessing, setIsProcessing] = React.useState(false);
  const [isCalculating, setIsCalculating] = React.useState(false);

  const [routerAddress, setRouterAddress] = React.useState<string | null>(null);
  const [fromAmount, setFromAmount] = useState<string>('');
  const [outAmount, setOutAmount] = React.useState<number>(0);
  const [route, setRoute] = React.useState<[ISushiRouteStep]>([]);
  const [price, setPrice] = React.useState<number>(0);
  const [priceImpact, setPriceImpact] = React.useState<number>(0);
  const [txPayload, setTxPayload] = React.useState<ISushiTx | null>(null);

  const refetch = () => {
    const amount = Number(fromAmount) || 1;
    if (!fromToken || !toToken || !amount || !account.chainId || !account.accountAddress) return;
    setIsCalculating(true);
    sushiApi
      .getBestRoute(account.chainId, {
        tokenIn: fromAddress,
        tokenOut: toAddress,
        amount: wei.to(amount, fromToken.decimals),
        gasPrice: '100000',
        to: account.accountAddress,
        feeReceiver: '0x73bC332384Bc90F542Afe3FE5cc5dFcB97827AAE',
        fee: '0.0025',
      })
      .then((data) => {
        const priceDecimals = fromToken.decimals - toToken.decimals;
        const price = Number((data.swapPrice * 10 ** priceDecimals).toFixed(6));

        setRouterAddress(data.routeProcessorAddr);
        setOutAmount(wei.from(data.assumedAmountOut, toToken.decimals));
        setRoute(data.route);
        setPrice(price);
        setPriceImpact(data.priceImpact);
        setTxPayload(data.tx);
      })
      .catch((error) => {
        console.error('[useUniSwap][refetch]', error);
      })
      .finally(() => {
        setIsCalculating(false);
      });
  };

  const readAllowance = useReadContract({
    abi: ERC_20_ABI,
    address: fromAddress,
    skip: !routerAddress || !Number(fromAmount),
    functionName: 'allowance',
    args: [account.accountAddress, routerAddress],
  });

  const isNeedApprove = React.useMemo(() => {
    if (!fromAddress || !fromAmount || !readAllowance.data) {
      return true;
    } else {
      const amount = BigInt(wei.to(fromAmount, fromToken.decimals));
      return readAllowance.data < amount;
    }
  }, [readAllowance.data, fromAmount, fromAddress]);

  const writeFromToken = useWriteContract({
    abi: ERC_20_ABI,
    address: fromAddress,
  });

  const onApprove = async (token: IToken, fromAmount: number) => {
    setIsApproving(true);

    try {
      await writeFromToken.writeContractAsync({
        functionName: 'approve',
        args: [routerAddress, wei.to(fromAmount, token.decimals)],
      });

      await readAllowance.refetch();
      setIsApproving(false);
    } catch (error) {
      setIsApproving(false);

      throw error;
    }
  };
  const onSwap = async (): Promise<string> => {
    setIsProcessing(true);

    try {
      if (!fromToken || !toToken || !fromAmount) {
        throw new Error('Wrong tokens or amounts');
      }

      const txHash = await sendTransactionAsync(txPayload);

      exchangeAnalytics.trackSwap({
        chainId: account.chainId,
        toToken: toToken.symbol,
        fromAmount: +fromAmount || 0,
        toAmount: +outAmount || 0,
        fromToken: fromToken.symbol,
        userAddress: account.accountAddress,
      });

      try {
        await waitForTransactionReceipt(config, {
          confirmations: 1,
          hash: txHash,
          pollingInterval: 1_000,
        });
      } catch (e) {
        console.error(e);
      }

      await readAllowance.refetch();
      setIsProcessing(false);

      return txHash;
    } catch (error) {
      if (error.data?.message?.includes('INSUFFICIENT_OUTPUT_AMOUNT')) {
        throw new Error('Price changed, please retry');
      }

      setIsProcessing(false);
      throw error;
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      refetch();
    }, 5000);

    refetch();

    return () => clearInterval(interval);
  }, [account.chainId, fromAddress, toAddress, account.accountAddress, fromAmount]);

  return {
    address: routerAddress,
    fromAmount,
    setFromAmount,
    outAmount,
    route,
    price,
    priceImpact,
    isApproving,
    isNeedApprove: isNeedApprove && !fromToken?.isGas,
    isProcessing,
    isCalculating,
    approve: onApprove,
    swap: onSwap,
    refetch,
    resetApproved: () => {
      setIsApproving(false);
    },
  };
}

export default useSushiSwap;
