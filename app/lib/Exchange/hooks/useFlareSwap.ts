import React, { useEffect } from 'react';
import wei from 'utils/wei';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import { useWriteContract } from '@chain/hooks/useWriteContract.ts';
import ERC_20_ABI from 'lib/Wallet/api-contract/abi/erc-20.abi.ts';
import { SWAP_V2_ROUTER_CONTRACT_ID } from 'lib/Exchange/api-contract/swap.constants.ts';
import BLAZE_SWAP_ROUTER_ABI from 'lib/Exchange/api-contract/ABI/BlazeSwapRouter.abi.ts';

function useFlareSwap({ fromToken, toToken }: { fromToken?: IToken; toToken?: IToken }) {
  const { chainId } = useAccount();
  const exchangeAddress = SWAP_V2_ROUTER_CONTRACT_ID[chainId];

  const fromAddress = fromToken ? fromToken.address || fromToken.wrapAddress : null;
  const toAddress = toToken ? toToken.address || toToken.wrapAddress : null;

  const account = useAccount();

  const [isApproving, setIsApproving] = React.useState(false);
  const [isNeedApprove, setIsNeedApprove] = React.useState(true);
  const [isProcessing, setIsProcessing] = React.useState(false);

  const reserveState = useReadContract({
    initData: [0, 0],
    abi: BLAZE_SWAP_ROUTER_ABI,
    address: exchangeAddress,
    functionName: 'getReserves',
    args: [fromAddress, toAddress],
    skip: !account.isConnected || fromAddress === toAddress || !fromAddress || !toAddress,
  });
  const pairData = useReadContract({
    abi: BLAZE_SWAP_ROUTER_ABI,
    address: exchangeAddress,
    functionName: 'pairFor',
    args: [fromAddress, toAddress],
    skip: !account.isConnected || fromAddress === toAddress || !fromAddress || !toAddress,
  });
  const isExistsPair = !!pairData.data;

  // const amountInData = useReadContract({
  //     abi: ABI,
  //     address: exchangeAddress,
  //     functionName: 'getAmountIn',
  //     skip: true,
  // });
  // const amountOutData = useReadContract({
  //     abi: ABI,
  //     address: exchangeAddress,
  //     functionName: 'getAmountOut',
  //     skip: true,
  // });
  const reserves = reserveState.data;

  const writeFromToken = useWriteContract({
    abi: ERC_20_ABI,
    address: fromAddress,
  });
  const writeExchange = useWriteContract({
    abi: BLAZE_SWAP_ROUTER_ABI,
    address: exchangeAddress,
  });

  const rate = React.useMemo(() => {
    if (!fromToken || !toToken) return 1;

    const from = wei.from(reserves[0], fromToken.decimals);
    const to = wei.from(reserves[1], toToken.decimals);

    return to / from;
  }, [reserves]);

  const getDeadline = () => {
    return Math.floor(Date.now() / 1000) + 20 * 60;
  };
  const onApprove = async (token: IToken, fromAmount: number) => {
    setIsApproving(true);

    try {
      await writeFromToken.writeContractAsync({
        functionName: 'approve',
        args: [exchangeAddress, wei.to(fromAmount, token.decimals)],
      });

      setIsNeedApprove(false);
    } catch (error) {
      setIsApproving(false);

      throw error;
    }
  };
  const onSwap = async (
    fromToken: IToken,
    toToken: IToken,
    fromAmount: number,
    toAmount: number
  ) => {
    setIsProcessing(true);

    try {
      if (!fromToken || !toToken || !fromAmount || !toAmount) {
        throw new Error('Wrong tokens or amounts');
      }

      const path = [fromAddress, toAddress];

      if (!fromToken.address) {
        await writeExchange.writeContractAsync({
          functionName: 'swapExactNATForTokens',
          args: [wei.to(toAmount, toToken.decimals), path, account.accountAddress, getDeadline()],
          overrides: {
            value: wei.to(fromAmount, fromToken.decimals) as bigint,
          },
        });
      } else if (!toToken.address) {
        await writeExchange.writeContractAsync({
          functionName: 'swapExactTokensForNAT',
          args: [
            wei.to(fromAmount, toToken.decimals),
            wei.to(toAmount, toToken.decimals),
            path,
            account.accountAddress,
            getDeadline(),
          ],
        });
      } else {
        await writeExchange.writeContractAsync({
          functionName: 'swapExactTokensForTokens',
          args: [
            wei.to(fromAmount, toToken.decimals),
            wei.to(toAmount, toToken.decimals),
            path,
            account.accountAddress,
            getDeadline(),
          ],
        });
      }

      setIsProcessing(false);
    } catch (error) {
      if (error.data?.message?.includes('INSUFFICIENT_OUTPUT_AMOUNT')) {
        throw new Error('Price changed, please retry');
      }

      setIsProcessing(false);
      throw error;
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      reserveState.refetch();
    }, 5000);

    return () => clearInterval(interval);
  }, [account.chainId, fromAddress, toAddress, account.accountAddress]);

  return {
    address: exchangeAddress,
    rate,
    isApproving,
    isNeedApprove: isNeedApprove && !fromToken?.isGas,
    isProcessing,
    isExistsPair,
    approve: onApprove,
    swap: onSwap,
    reserves,
    loadingPair: pairData.loading,
    fetchReserves: () => reserveState.refetch(),
    resetApproved: () => {
      setIsApproving(false);
      setIsNeedApprove(true);
    },
  };
}

export default useFlareSwap;
