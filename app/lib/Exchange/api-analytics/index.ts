import analytics from '../../../shared/analytics';

class ExchangeAnalytics {
  public trackSwap(payload: {
    chainId: number;
    userAddress: string;
    fromToken: string;
    toToken: string;
    fromAmount: number;
    toAmount: number;
  }) {
    analytics.track(
      'exchangeSwap',
      {
        userAddress: payload.userAddress,
        fromAmount: payload.fromAmount,
        toAmount: payload.toAmount,
        sum: payload.fromAmount,
      },
      { fromToken: payload.fromToken, toToken: payload.toToken, chainId: payload.chainId }
    );
  }
}

const exchangeAnalytics = new ExchangeAnalytics();

export default exchangeAnalytics;
