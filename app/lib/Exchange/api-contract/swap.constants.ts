import { Chain } from 'services/multichain/chains';

export const SWAP_V2_ROUTER_CONTRACT_ID = {
  [Chain.FLARE]: '0xe3A1b355ca63abCBC9589334B5e609583C7BAa06',
  [Chain.SKALE]: '0x4cddf8D1473df386b926ec14b23bfbD566CE827a',
};

export const SWAP_V3_ROUTER_CONTRACT_ID = {
  [Chain.SKALE]: '0xd20a95C4470458aa00B1dF11EDA08c12ac29b858',
};

export const SWAP_V2_FACTORY_CONTRACT_ID = {
  [Chain.SKALE]: '0x1aaF6eB4F85F8775400C1B10E6BbbD98b2FF8483',
};

export const SWAP_V3_FACTORY_CONTRACT_ID = {
  [Chain.SKALE]: '0x51d15889b66A2c919dBbD624d53B47a9E8feC4bB',
};

export const SWAP_ALLOWED_TOKENS = {
  [Chain.FLARE]: ['FINU', 'FLR'],
  [Chain.SKALE]: ['SKL', 'USDC'],
};

export const SWAP_DEFAULT_FEE = 100;
