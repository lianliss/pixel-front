import React, { useEffect, useState } from 'react';
import styles from './ExchangerSwap.module.scss';
import getFinePrice from 'utils/getFinePrice';
import { useTranslation } from 'react-i18next';
import { Button } from '@ui-kit/Button/Button';
import TokenAmountField from 'lib/Exchange/components/TokenAmountField/TokenAmountField';
import { IconExchange } from '@ui-kit/icon/common/IconExchange';
import { Typography } from '@ui-kit/Typography/Typography';
import Alert from '@mui/material/Alert';
import { openLink } from 'utils/open-link.ts';
import { createTxUrl } from '@chain/utils/url.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';

type Props = {
  tokens: IToken[];
  fromToken?: IToken;
  toToken?: IToken;
  fromBalance?: number;
  toBalance?: number;
  onChangeFromToken?: (t: IToken) => void;
  onChangeToToken?: (t: IToken) => void;
  rate?: number;
  onApprove?: (token: IToken, value: number) => void;
  onSwap?: (
    from: IToken,
    to: IToken,
    fromAmount: number,
    toAmount: number
  ) => Promise<string | null>;
  isNoLiquidity?: boolean;
  isApproving?: boolean;
  isProcessing?: boolean;
  isNeedApprove?: boolean;
  isExistsPair?: boolean;
  loadingPair?: boolean;
  fromAmount?: string;
  toAmount?: string;
  onChangeFromAmount?: (v: string) => void;
  onChangeToAmount?: (v: string) => void;
};

function ExchangeWidget(props: Props) {
  const {
    tokens = [],
    fromToken,
    toToken,
    fromBalance = 0,
    toBalance = 0,
    onChangeFromToken,
    onChangeToToken,
    rate = 1,
    onApprove,
    onSwap,
    isNoLiquidity,
    isApproving,
    isProcessing,
    isNeedApprove,
    isExistsPair = false,
    fromAmount,
    toAmount,
    onChangeFromAmount,
    onChangeToAmount,
    loadingPair = false,
  } = props;

  const account = useAccount();
  const { t } = useTranslation('exchanger');
  const [swapped, setSwapped] = useState<{
    amount: string;
    txHash: string;
    txLink?: string;
  } | null>(null);
  const fromSymbol = fromToken?.symbol || '';
  const toSymbol = toToken?.symbol || '';
  const rateText =
    rate >= 0.01
      ? `1 ${fromSymbol} ≈ ${getFinePrice(rate)} ${toSymbol}`
      : `${getFinePrice(1 / rate)} ${fromSymbol} ≈ ${1} ${toSymbol}`;

  const handleApprove = () => {
    if (!fromToken || Number.isNaN(+fromAmount) || !Number(fromAmount)) {
      return;
    }

    onApprove?.(fromToken, +fromAmount);
  };
  const handleSwap = () => {
    if (
      !fromToken ||
      !toToken ||
      Number.isNaN(+fromAmount) ||
      !Number(fromAmount) ||
      Number.isNaN(+toAmount) ||
      !Number(toAmount)
    ) {
      return;
    }

    onSwap?.(fromToken, toToken, +fromAmount, +toAmount).then((txHash) =>
      setSwapped({ amount: toAmount, txHash, txLink: createTxUrl(account, txHash) })
    );
  };
  const resetSwapped = () => {
    if (swapped !== null) {
      setSwapped(null);
    }
  };

  return (
    <div className={styles.root}>
      <div className=''>
        <div className={styles.content}>
          {!isExistsPair && !loadingPair && (
            <Alert severity='warning' variant='filled' sx={{ width: '100%' }}>
              Pair not found
            </Alert>
          )}

          <TokenAmountField
            balance={fromBalance}
            value={fromAmount}
            onChange={(e) => {
              onChangeFromAmount?.(e);
              resetSwapped();
            }}
            token={fromToken}
            tokens={tokens}
            disabled={isApproving || !isExistsPair}
            onChangeToken={(t) => {
              onChangeFromToken?.(t);
              resetSwapped();
            }}
            disabledTokens={[fromToken?.symbol, toToken?.symbol]}
          />

          <div>
            <div
              className={styles.switchIcon}
              onClick={() => {
                onChangeFromToken?.(toToken);
                onChangeToToken?.(fromToken);
                onChangeFromAmount?.(+toAmount === 0 ? '' : toAmount);
                resetSwapped();
              }}
            >
              <IconExchange />
            </div>
          </div>

          <TokenAmountField
            balance={toBalance}
            value={toAmount}
            onChange={onChangeToAmount}
            token={toToken}
            tokens={tokens}
            onChangeToken={onChangeToToken}
            disabledTokens={[fromToken?.symbol, toToken?.symbol]}
            disabled
          />
        </div>

        {swapped !== null && (
          <Alert
            component='a'
            onClick={() => {
              if (!swapped.txLink) {
                return;
              }

              openLink(swapped.txLink);
            }}
            severity='success'
            variant='outlined'
            sx={{ width: '100%', mt: 2, textDecoration: swapped.txLink ? 'underline' : undefined }}
          >
            {t('Successfully swapped')}: {getFinePrice(+swapped.amount)} {toToken?.symbol}
          </Alert>
        )}

        {!!rate && (
          <div className={styles.rate}>
            <Typography component='span' variant='subtitle1'>
              {t('Price')}
            </Typography>
            <Typography component='span' variant='subtitle1'>
              {rateText}
            </Typography>
          </div>
        )}

        <div className={styles.actions}>
          {!!isNeedApprove && (
            <Button
              size='large'
              color='success'
              variant='contained'
              disabled={!fromAmount || !toAmount || isApproving || !isExistsPair}
              loading={isApproving}
              onClick={handleApprove}
              fullWidth
            >
              {t('Approve')} {fromSymbol}
            </Button>
          )}
          <Button
            size='large'
            color='primary'
            variant='contained'
            disabled={
              !fromAmount ||
              !toAmount ||
              isProcessing ||
              isNeedApprove ||
              !fromToken ||
              !toToken ||
              !isExistsPair
            }
            loading={isProcessing}
            onClick={handleSwap}
            fullWidth
          >
            {isNoLiquidity ? t('No liquidity found') : t('Swap')}
          </Button>
        </div>
      </div>
    </div>
  );
}

export default ExchangeWidget;
