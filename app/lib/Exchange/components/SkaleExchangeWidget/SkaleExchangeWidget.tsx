import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import ExchangeWidget from 'lib/Exchange/components/ExchangeWidget/ExchangeWidget.tsx';
import { DISPLAY_TOKENS, WRAP_TOKENS } from 'services/multichain/initialTokens';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';
import toaster from 'services/toaster.tsx';
import useSushiSwap from 'lib/Exchange/hooks/useSushiSwap.ts';
import { SWAP_ALLOWED_TOKENS } from 'lib/Exchange/api-contract/swap.constants.ts';

function SkaleExchangeWidget() {
  const account = useAccount();
  const { t } = useTranslation('exchanger');

  const tokensFilter = SWAP_ALLOWED_TOKENS[account.chainId];
  const tokens = useMemo(() => {
    const wrapToken = WRAP_TOKENS[account.chainId];
    const arr = [
      ...(DISPLAY_TOKENS[account.chainId] || []).filter(
        (el) => !el.symbol.startsWith('W') && tokensFilter.includes(el.symbol)
      ),
    ];
    const coin = arr.find((el) => !el.address || el.isGas);

    if (wrapToken && coin) {
      coin.wrapAddress = wrapToken?.address;
    }

    return arr;
  }, [account.chainId]);

  const [fromToken, setFromToken] = useState<IToken | null>(null);
  const [toToken, setToToken] = useState<IToken | null>(null);

  const sushi = useSushiSwap({
    fromToken,
    toToken,
  });

  const fromBalance = useBalance({
    address: account.accountAddress,
    token: fromToken?.isGas ? undefined : fromToken?.address,
    skip: !account.isConnected || !fromToken,
  });
  const toBalance = useBalance({
    address: account.accountAddress,
    token: toToken?.isGas ? undefined : toToken?.address,
    skip: !account.isConnected || !toToken,
  });

  const disabledAmountOut =
    !fromToken || !toToken || !Number(sushi.fromAmount) || !account.isConnected;

  const amountOut = disabledAmountOut ? '0.00' : sushi.outAmount.toFixed(6);

  const handleApprove = async (token: IToken, amount: number) => {
    try {
      await sushi.approve(token, amount);
    } catch (error) {
      toaster.logError(error, '[onApprove]');
    }
  };
  const handleSwap = async (): Promise<string | null> => {
    try {
      const txHash = await sushi.swap();

      await fromBalance.refetch();
      await toBalance.refetch();

      sushi.setFromAmount('');

      return txHash;
    } catch (error) {
      console.error('[handleSwap]', error);
      await sushi.refetch();
      toaster.logError(error, '[onSwap]');
    }
  };

  useEffect(() => {
    setFromToken(tokens[0]);
    setToToken(tokens[1]);
  }, [tokens]);

  return (
    <ExchangeWidget
      tokens={tokens}
      fromToken={fromToken}
      toToken={toToken}
      fromBalance={fromBalance.data?.formatted}
      toBalance={toBalance.data?.formatted}
      onChangeFromToken={(t) => {
        setFromToken(t);
        sushi.resetApproved();
      }}
      onChangeToToken={(t) => setToToken(t)}
      isApproving={sushi.isApproving}
      isNeedApprove={sushi.isNeedApprove}
      isNoLiquidity={false}
      isProcessing={sushi.isProcessing}
      isExistsPair={true}
      rate={sushi.price}
      onApprove={handleApprove}
      onSwap={handleSwap}
      fromAmount={sushi.fromAmount}
      toAmount={amountOut}
      loadingPair={sushi.isCalculating}
      onChangeFromAmount={(v) => {
        sushi.setFromAmount(v);
      }}
    />
  );
}

export default SkaleExchangeWidget;
