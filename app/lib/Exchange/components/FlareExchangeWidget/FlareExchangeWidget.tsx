import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import ExchangeWidget from 'lib/Exchange/components/ExchangeWidget/ExchangeWidget.tsx';
import { DISPLAY_TOKENS, WRAP_TOKENS } from 'services/multichain/initialTokens';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useBalance } from '@chain/hooks/useBalance.ts';
import useFlareSwap from 'lib/Exchange/hooks/useFlareSwap.ts';
import toaster from 'services/toaster.tsx';
import { useReadContract } from '@chain/hooks/useReadContract.ts';
import wei from 'utils/wei';
import BLAZE_SWAP_ROUTER_ABI from 'lib/Exchange/api-contract/ABI/BlazeSwapRouter.abi.ts';
import exchangeAnalytics from 'lib/Exchange/api-analytics';

function FlareExchangeWidget() {
  const account = useAccount();
  const { t } = useTranslation('exchanger');

  const tokens = useMemo(() => {
    const wrapToken = WRAP_TOKENS[account.chainId];
    const arr = [
      ...(DISPLAY_TOKENS[account.chainId] || []).filter((el) => !el.symbol.startsWith('W')),
    ];
    const coin = arr.find((el) => !el.address || el.isGas);

    if (wrapToken && coin) {
      coin.wrapAddress = wrapToken?.address;
    }

    return arr;
  }, [account.chainId]);

  const [fromToken, setFromToken] = useState<IToken | null>(null);
  const [toToken, setToToken] = useState<IToken | null>(null);
  const [fromAmount, setFromAmount] = useState<string>('');
  // const [toAmount, setToAmount] = useState<string>('');
  const isNoLiquidity = false;

  const flareSwap = useFlareSwap({
    fromToken,
    toToken,
  });

  const fromBalance = useBalance({
    address: account.accountAddress,
    token: fromToken?.isGas ? undefined : fromToken?.address,
    skip: !account.isConnected || !fromToken,
  });
  const toBalance = useBalance({
    address: account.accountAddress,
    token: toToken?.isGas ? undefined : toToken?.address,
    skip: !account.isConnected || !toToken,
  });

  const disabledAmountOut =
    !fromToken ||
    !flareSwap.reserves ||
    !Number(fromAmount) ||
    !account.isConnected ||
    !Number(flareSwap.reserves?.[0]) ||
    !Number(flareSwap.reserves?.[1]);

  const amountOutState = useReadContract({
    abi: BLAZE_SWAP_ROUTER_ABI,
    address: flareSwap.address,
    functionName: 'getAmountOut',
    args: disabledAmountOut
      ? []
      : [
          wei.to(+fromAmount, fromToken?.decimals),
          flareSwap.reserves?.[0].toString(),
          flareSwap.reserves?.[1].toString(),
        ],
    skip: disabledAmountOut,
    select: (res) => wei.from(res),
  });
  const amountOut = fromAmount && amountOutState.data ? Number(amountOutState.data).toFixed(6) : '';

  const handleApprove = async (token: IToken, amount: number) => {
    try {
      await flareSwap.approve(token, amount);
    } catch (error) {
      toaster.logError(error, '[onApprove]');
    }
  };
  const handleSwap = async (
    fromToken: IToken,
    toToken: IToken,
    fromAmount: number,
    toAmount: number
  ): Promise<string | null> => {
    try {
      await flareSwap.swap(fromToken, toToken, fromAmount, toAmount);

      exchangeAnalytics.trackSwap({
        chainId: account.chainId,
        toToken: toToken.symbol,
        fromAmount,
        toAmount,
        fromToken: fromToken.symbol,
        userAddress: account.accountAddress,
      });

      await fromBalance.refetch();
      await toBalance.refetch();

      setFromAmount('');

      return null;
    } catch (error) {
      console.log(error);
      await flareSwap.fetchReserves();
      await amountOutState.refetch();
      toaster.logError(error, '[onSwap]');
    }
  };

  useEffect(() => {
    setFromToken(tokens[0]);
    setToToken(tokens[1]);
  }, [tokens]);

  return (
    <ExchangeWidget
      tokens={tokens}
      fromToken={fromToken}
      toToken={toToken}
      fromBalance={fromBalance.data?.formatted}
      toBalance={toBalance.data?.formatted}
      onChangeFromToken={(t) => {
        setFromToken(t);
        flareSwap.resetApproved();
      }}
      onChangeToToken={(t) => setToToken(t)}
      isApproving={flareSwap.isApproving}
      isNeedApprove={flareSwap.isNeedApprove}
      isNoLiquidity={isNoLiquidity}
      isProcessing={flareSwap.isProcessing}
      isExistsPair={flareSwap.isExistsPair}
      rate={flareSwap.rate}
      onApprove={handleApprove}
      onSwap={handleSwap}
      fromAmount={fromAmount}
      toAmount={amountOut}
      loadingPair={flareSwap.loadingPair}
      onChangeFromAmount={(v) => {
        // amountOutState.clear();
        setFromAmount(v);
      }}
    />
  );
}

export default FlareExchangeWidget;
