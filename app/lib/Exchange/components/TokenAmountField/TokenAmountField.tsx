import { TextField } from '@ui-kit/TextField/TextField.tsx';
import Box from '@mui/material/Box';
import { Avatar } from '@ui-kit/Avatar/Avatar.tsx';
import Select from '@mui/material/Select';
import getFinePrice from 'utils/getFinePrice';
import React from 'react';
import styles from './TokenAmountField.module.scss';
import MenuItem from '@mui/material/MenuItem';
import { ITokenData } from '@chain/hooks/useToken.ts';

type Props = {
  balance?: number;
  value?: string;
  onChange?: (value: string) => void;
  tokens?: IToken[];
  token?: IToken;
  onChangeToken?: (t: IToken) => void;
  disabledTokens?: string[];
};

function TokenAmountField({
  balance,
  value,
  onChange,
  tokens = [],
  token,
  onChangeToken,
  disabledTokens = [],
  disabled,
}: Props) {
  return (
    <TextField
      className={styles.root}
      InputProps={{
        className: styles.inputBase,
        startAdornment: (
          <Select
            sx={{ background: 'transparent' }}
            startAdornment={<Avatar src={token?.logoURI} sx={{ mr: 1 }} />}
            variant='outlined'
            size='small'
            className={styles.select}
            value={token?.symbol || ''}
            onChange={(e) => {
              const t = tokens.find((el) => el.symbol === e.target.value);

              if (t) {
                onChangeToken?.(t);
              }
            }}
          >
            {tokens.map((token: ITokenData) => (
              <MenuItem
                key={token.symbol}
                value={token.symbol}
                disabled={disabledTokens.includes(token.symbol)}
              >
                {token.symbol}
              </MenuItem>
            ))}
          </Select>
        ),
      }}
      fullWidth
      placeholder='0.00'
      type='number'
      value={value}
      onChange={(e) => onChange(e.target.value.replace(/[^\d.-]+/g, ''))}
      inputProps={{
        className: styles.input,
      }}
      disabled={disabled}
      helperText={
        <div onClick={() => onChange(balance?.toString() || '0')} style={{ width: 'fit-content' }}>
          Balance: {getFinePrice(balance)} {token?.symbol || ''}
        </div>
      }
    />
  );
}

export default TokenAmountField;
