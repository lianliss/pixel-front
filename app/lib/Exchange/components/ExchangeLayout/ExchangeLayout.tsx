import React from 'react';
import { Tabs } from '@ui-kit/Tabs/Tabs.tsx';
import { Tab } from '@ui-kit/Tab/Tab.tsx';
import Paper from '@mui/material/Paper';
import { useLocation, useNavigate } from 'react-router-dom';
import ChainSwitcher from '../../../../widget/ChainSwitcher/ChainSwitcher.tsx';
import { Chain } from 'services/multichain/chains';
import { useTranslation } from 'react-i18next';
import { BRIDGE_CHAINS, EXCHANGE_CHAINS } from '@cfg/app.ts';
import Container from '@mui/material/Container';

const MAP_NAVIGATE = {
  swap: '/exchange',
  bridge: '/bridge',
};
const MAP_TABS = {
  '/exchange': 'swap',
  '/bridge': 'bridge',
  '/exchange/': 'swap',
  '/bridge/': 'bridge',
};

function ExchangeLayout({ children }) {
  const navigate = useNavigate();
  const location = useLocation();
  const { t } = useTranslation('bridge');

  const tab = MAP_TABS[location.pathname];

  return (
    <Container maxWidth='xs'>
      <Paper variant='outlined' elevation={2} style={{ padding: 16, margin: '24px 0' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between', gap: '24px' }}>
          <Tabs
            style={{ width: '100%' }}
            value={tab}
            sx={{ mt: -1 }}
            onChange={(e, v) => {
              navigate(MAP_NAVIGATE[v]);
            }}
          >
            <Tab label={t('Swap')} value='swap' />
            <Tab label={t('Bridge')} value='bridge' />
          </Tabs>

          <ChainSwitcher chains={[...EXCHANGE_CHAINS, ...BRIDGE_CHAINS]} isRelative />
        </div>

        <div style={{ marginTop: 12 }}>{children}</div>
      </Paper>
    </Container>
  );
}

export default ExchangeLayout;
