import React from 'react';
import styles from './Exchanger.module.scss';
import { Chain } from 'services/multichain/chains';
import NotAvailableChain from '../../../../shared/blocks/NotAvailableChain/NotAvailableChain.tsx';
import ExchangeLayout from 'lib/Exchange/components/ExchangeLayout/ExchangeLayout.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { useBackAction } from '../../../../shared/telegram/useBackAction.ts';
import { useTranslation } from 'react-i18next';
import { EXCHANGE_CHAINS } from '@cfg/app.ts';

const LazyWidget = {
  [Chain.SKALE]: React.lazy(
    () => import('../../components/SkaleExchangeWidget/SkaleExchangeWidget.tsx')
  ),
  [Chain.FLARE]: React.lazy(
    () => import('../../components/FlareExchangeWidget/FlareExchangeWidget.tsx')
  ),
};

function Exchanger() {
  const account = useAccount();
  const Widget = EXCHANGE_CHAINS.includes(account.chainId) && LazyWidget[account.chainId];

  const { t } = useTranslation('exchanger');

  useBackAction('/wallet/');

  return (
    <ExchangeLayout>
      <div className={styles.root}>
        {Widget ? (
          <Widget />
        ) : (
          <NotAvailableChain
            title={t('Not Available')}
            summary={t('Exchange not available on current network')}
            chainId={Chain.SKALE}
          />
        )}
      </div>
    </ExchangeLayout>
  );
}

export default Exchanger;
