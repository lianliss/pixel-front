import React from 'react';
import toaster from "services/toaster";
import wait from "utils/wait";
import {useAccount} from "@chain/hooks/useAccount.ts";
import {useWriteContract} from "@chain/hooks/useWriteContract.ts";
import {useReadContract} from "@chain/hooks/useReadContract.ts";
import {createEvent, createStore} from "effector";
import {CONTRACT_ADDRESSES} from "services/multichain/contracts";
import {readContract} from "@wagmi/core";
import {wagmiConfig} from "@chain/wagmi";
import {IS_WAGMI, ZERO_ADDRESS} from "@cfg/config";
import {useSelfUserInfo} from "lib/User/hooks/useUserInfo";
// import {useUnit} from "effector-react";

const updateIsMigration = createEvent();
export const isMigrationStore = createStore({ progress: false, status: undefined });
isMigrationStore.on(updateIsMigration, (_, n) => n);

function UseMigrationToV5() {
  const account = useAccount();
  const { chainId } = account;
    const userInfo = useSelfUserInfo();
    const telegramId = userInfo.data?.telegramId;

    const V4 = CONTRACT_ADDRESSES[chainId]?.pixel || {};
    const V5 = CONTRACT_ADDRESSES[chainId]?.v5 || {};
    const isHaveMigration = !!V5?.coreMigration;

  const inventoryKey = `inventory-checked-${chainId}`;
  const slotsKey = `slots-checked-${chainId}`;

  const writeCoreMigration = useWriteContract({
      abi: require('const/ABI/V5/coreMigration'),
      address: V5.coreMigration,
  });
    const writeSlotsMigration = useWriteContract({
        abi: require('const/ABI/V5/slotsMigration'),
        address: V5.slotsMigration,
    });
    const writeNftMigration = useWriteContract({
        abi: require('const/ABI/V5/nftMigration'),
        address: V5.nftMigration,
    });

    //
    const oldNftData = useReadContract({
        abi: require('const/ABI/PXL/NFT'),
        address: V4.nft,
        skip: true,
        functionName: 'getOwnerTokensCount',
        select: res => Number(res),
    });
    const oldSlotsData = useReadContract({
        abi: require('const/ABI/PXL/Slots'),
        address: V4.slots,
        skip: true,
        functionName: 'getUserSlots'
    });
    const oldSlotsMigratedData = useReadContract({
        abi: require('const/ABI/V5/slotsMigration'),
        address: V5.slotsMigration,
        skip: true,
        functionName: 'migrated'
    });

    const fetchOldSlots = async () => {
        const result = await readContract(wagmiConfig, {
            abi: require('const/ABI/PXL/Slots'),
            address: V4.slots,
            functionName: 'getUserSlots',
            args: [telegramId],
        });

        return result;
    };
    const fetchOldSlotsMigrated = async () => {
        const result = await readContract(wagmiConfig, {
            abi: require('const/ABI/V5/slotsMigration'),
            address: V5.slotsMigration,
            functionName: 'migrated',
            args: [telegramId],
        });

        return result;
    };
    const fetchOldNftCount = async () => {
        const result = await readContract(wagmiConfig, {
                abi: require('const/ABI/PXL/NFT'),
                address: V4.nft,
                functionName: 'getOwnerTokensCount',
                 args: [account.accountAddress]
        });

        return Number(result);
    };



    // const isHaveMigration = !!network.contractAddresses.v5?.coreMigration;

  const migrateToV5 = async (skip) => {
    try {
      if (!isHaveMigration) return;
      updateIsMigration({ progress: true, status: 'Loading contracts' });
      // const V4 = network.contractAddresses.pixel;
      // const V5 = network.contractAddresses.v5;
      // const core = await getContract(require('const/ABI/V5/coreMigration'), V5.coreMigration);
      // const oldNFT = await getContract(require('const/ABI/PXL/NFT'), V4.nft);
      // const nft = await getContract(require('const/ABI/V5/nftMigration'), V5.nftMigration);
      // const slots = await getContract(require('const/ABI/V5/slotsMigration'), V5.slotsMigration);
      await wait(3000);
      // let tx;

      if (skip !== 'core' && skip !== 'slots') {
        updateIsMigration({ progress: true, status: 'Updating mining core' });
        // tx = await transaction(core, 'migrate', []);
        await writeCoreMigration.writeContractAsync({
            functionName: 'migrate',
        });
        // await getTransactionReceipt(tx);
      }

      if (skip !== 'slots') {
        updateIsMigration({ progress: true, status: 'Deploying new inventory slots' });
        // tx = await transaction(slots, 'migrate', []);
          await writeSlotsMigration.writeContractAsync({
              functionName: 'migrate',
          })
        // await getTransactionReceipt(tx);
      }

      const LIMIT = 25;
      let total;

      if (IS_WAGMI) {
          total = await fetchOldNftCount();
      } else {
          total = await oldNftData.refetch({
              args: [account.accountAddress]
          });
      }

      updateIsMigration({ progress: true, status: `Moving your NFTs ${0}/${total}` });

      for (let i = 0; i < Math.floor(total / LIMIT); i++) {
          await writeNftMigration.writeContractAsync({
              functionName: 'migrate',
              args: [LIMIT]
          })
        // tx = await transaction(nft, 'migrate', [LIMIT]);
        // await getTransactionReceipt(tx);
        updateIsMigration({ progress: true, status: `Moving your NFTs ${(i * LIMIT + LIMIT)}/${total}` });
      }
      const tail = total % LIMIT;
      if (tail) {
          await writeSlotsMigration.writeContractAsync({
              functionName: 'migrate',
              args: [tail]
          })
        // tx = await transaction(nft, 'migrate', [tail]);
        // await getTransactionReceipt(tx);

        updateIsMigration({ progress: true, status: `Moving your NFTs ${total}/${total}` });
      }
      await wait(1000);
      updateIsMigration({ progress: false });
      window.localStorage.setItem(inventoryKey, 'true');
      window.localStorage.setItem(slotsKey, 'true');
    } catch (error) {
      console.log('[migrateToV5]', error);
      toaster.logError(error);

      toaster.error('Migration Failed');
      updateIsMigration({ progress: false, status: '' });
      throw error;
    }
  }

  const checkOldInventory = async () => {
    try {
      if (!isHaveMigration) return;
      // const isInventoryChecked = window.localStorage.getItem(inventoryKey) === 'true';
      // const isSlotsChecked = window.localStorage.getItem(slotsKey) === 'true';
      //if (isInventoryChecked && isSlotsChecked) return;
      // const V4 = network.contractAddresses.pixel;
      // const V5 = network.contractAddresses.v5;
      // const oldNFT = await getContract(require('const/ABI/PXL/NFT'), V4.nft);
      // const oldSlots = await getContract(require('const/ABI/PXL/Slots'), V4.slots);
      // const slotsMigration = await getContract(require('const/ABI/V5/slotsMigration'), V5.slotsMigration);

        let total = 0;
        if (IS_WAGMI) {
            total = await fetchOldNftCount();
        }
        else {
            await oldNftData.refetch({
                args: [account.accountAddress]
            });
        }

        let oldSlotsResult;
        if (IS_WAGMI) {
            oldSlotsResult = await fetchOldSlots();
        } else {
            oldSlotsResult = await oldSlotsData.refetch({
                args: [telegramId],
            });
        }

        let isSlotsMigrated = false;
        if (IS_WAGMI) {
            isSlotsMigrated = await fetchOldSlotsMigrated();
        } else {
            isSlotsMigrated = await oldSlotsMigratedData.refetch({
                args: [telegramId],
            });
        }

      //   const data = await Promise.all([
      //   oldNFT.methods.getOwnerTokensCount(accountAddress).call(),
      //   oldSlots.methods.getUserSlots(telegramId).call(),
      //   slotsMigration.methods.migrated(telegramId).call(),
      // ]);
      const haveNFT = !!Number(total);
      const haveSlots = !!oldSlotsResult?.find(s => !!Number(s.tokenId));
      const slotsMigrated = isSlotsMigrated || telegramId === 2001352970;
      // pushPixel({
      //   haveNFT,
      //   haveSlots,
      //   isSlotsMigrated: slotsMigrated,
      //   isMigrateBySlots: haveSlots && !slotsMigrated,
      //   isMigrateByNFT: haveNFT,
      // }, 'migration');
      if (haveSlots && !slotsMigrated) {
        await migrateToV5('core');
      } else if (haveNFT) {
        await migrateToV5('slots');
      }
      window.localStorage.setItem(inventoryKey, 'true');
      window.localStorage.setItem(slotsKey, 'true');
    } catch (error) {
      console.error('[checkOldInventory]', error);
    }
  }

  return {
    isHaveMigration,
    migrateToV5,
    checkOldInventory,
  }
}

export default UseMigrationToV5;
