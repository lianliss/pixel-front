export default [
  { type: 'constructor', stateMutability: 'nonpayable', inputs: [] },
  {
    type: 'error',
    name: 'AccessControlBadConfirmation',
    inputs: [],
  },
  {
    type: 'error',
    name: 'AccessControlUnauthorizedAccount',
    inputs: [
      { type: 'address', name: 'account', internalType: 'address' },
      {
        type: 'bytes32',
        name: 'neededRole',
        internalType: 'bytes32',
      },
    ],
  },
  {
    type: 'event',
    name: 'BonusDrop',
    inputs: [
      { type: 'address', name: 'account', internalType: 'address', indexed: true },
      {
        type: 'uint64',
        name: 'userId',
        internalType: 'uint64',
        indexed: true,
      },
      { type: 'uint256', name: 'amount', internalType: 'uint256', indexed: false },
    ],
    anonymous: false,
  },
  {
    type: 'event',
    name: 'BuyChestPixel',
    inputs: [
      { type: 'address', name: 'account', internalType: 'address', indexed: true },
      {
        type: 'uint256',
        name: 'marketItem',
        internalType: 'uint256',
        indexed: true,
      },
      { type: 'uint256', name: 'chestId', internalType: 'uint256', indexed: true },
      {
        type: 'uint256',
        name: 'price',
        internalType: 'uint256',
        indexed: false,
      },
    ],
    anonymous: false,
  },
  {
    type: 'event',
    name: 'BuyChestSGB',
    inputs: [
      { type: 'address', name: 'account', internalType: 'address', indexed: true },
      {
        type: 'uint256',
        name: 'marketItem',
        internalType: 'uint256',
        indexed: true,
      },
      { type: 'uint256', name: 'chestId', internalType: 'uint256', indexed: true },
      {
        type: 'uint256',
        name: 'price',
        internalType: 'uint256',
        indexed: false,
      },
    ],
    anonymous: false,
  },
  {
    type: 'event',
    name: 'ItemDrop',
    inputs: [
      { type: 'address', name: 'account', internalType: 'address', indexed: true },
      {
        type: 'uint256',
        name: 'chestId',
        internalType: 'uint256',
        indexed: true,
      },
      { type: 'uint256', name: 'typeId', internalType: 'uint256', indexed: true },
      {
        type: 'uint256',
        name: 'tokenId',
        internalType: 'uint256',
        indexed: false,
      },
    ],
    anonymous: false,
  },
  {
    type: 'event',
    name: 'Recieved',
    inputs: [
      { type: 'address', name: 'account', internalType: 'address', indexed: true },
      {
        type: 'uint256',
        name: 'amount',
        internalType: 'uint256',
        indexed: false,
      },
    ],
    anonymous: false,
  },
  {
    type: 'event',
    name: 'RoleAdminChanged',
    inputs: [
      { type: 'bytes32', name: 'role', internalType: 'bytes32', indexed: true },
      {
        type: 'bytes32',
        name: 'previousAdminRole',
        internalType: 'bytes32',
        indexed: true,
      },
      { type: 'bytes32', name: 'newAdminRole', internalType: 'bytes32', indexed: true },
    ],
    anonymous: false,
  },
  {
    type: 'event',
    name: 'RoleGranted',
    inputs: [
      { type: 'bytes32', name: 'role', internalType: 'bytes32', indexed: true },
      {
        type: 'address',
        name: 'account',
        internalType: 'address',
        indexed: true,
      },
      { type: 'address', name: 'sender', internalType: 'address', indexed: true },
    ],
    anonymous: false,
  },
  {
    type: 'event',
    name: 'RoleRevoked',
    inputs: [
      { type: 'bytes32', name: 'role', internalType: 'bytes32', indexed: true },
      {
        type: 'address',
        name: 'account',
        internalType: 'address',
        indexed: true,
      },
      { type: 'address', name: 'sender', internalType: 'address', indexed: true },
    ],
    anonymous: false,
  },
  {
    type: 'event',
    name: 'SendChest',
    inputs: [
      { type: 'address', name: 'account', internalType: 'address', indexed: true },
      {
        type: 'uint256',
        name: 'chestId',
        internalType: 'uint256',
        indexed: true,
      },
    ],
    anonymous: false,
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [{ type: 'bytes32', name: '', internalType: 'bytes32' }],
    name: 'DEFAULT_ADMIN_ROLE',
    inputs: [],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [{ type: 'bytes32', name: '', internalType: 'bytes32' }],
    name: 'EDITOR_ROLE',
    inputs: [],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [{ type: 'bytes32', name: '', internalType: 'bytes32' }],
    name: 'MINTER_ROLE',
    inputs: [],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [{ type: 'bytes32', name: '', internalType: 'bytes32' }],
    name: 'MULTIPLIER_ROLE',
    inputs: [],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [{ type: 'uint256', name: '', internalType: 'uint256' }],
    name: '_getChance',
    inputs: [
      { type: 'uint256', name: 'chestId', internalType: 'uint256' },
      {
        type: 'uint8',
        name: 'rarity',
        internalType: 'uint8',
      },
      { type: 'uint64', name: 'userId', internalType: 'uint64' },
    ],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [{ type: 'uint256', name: '', internalType: 'uint256' }],
    name: '_rollRarity',
    inputs: [
      { type: 'uint256', name: 'chestId', internalType: 'uint256' },
      {
        type: 'uint64',
        name: 'userId',
        internalType: 'uint64',
      },
    ],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [{ type: 'uint256', name: '', internalType: 'uint256' }],
    name: 'addChestItem',
    inputs: [
      { type: 'uint256', name: 'chestIndex', internalType: 'uint256' },
      {
        type: 'uint256',
        name: 'slotIndex',
        internalType: 'uint256',
      },
      { type: 'uint256', name: 'rarity', internalType: 'uint256' },
      {
        type: 'uint256',
        name: 'typeId',
        internalType: 'uint256',
      },
    ],
  },
  {
    type: 'function',
    stateMutability: 'payable',
    outputs: [],
    name: 'buyChest',
    inputs: [{ type: 'uint256', name: 'itemId', internalType: 'uint256' }],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'clearChestItems',
    inputs: [{ type: 'uint256', name: 'chestIndex', internalType: 'uint256' }],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [{ type: 'uint256', name: '', internalType: 'uint256' }],
    name: 'createChest',
    inputs: [{ type: 'uint256[6]', name: 'ratios', internalType: 'uint256[6]' }],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [{ type: 'uint256', name: '', internalType: 'uint256' }],
    name: 'createMarketItem',
    inputs: [
      { type: 'uint256', name: 'chestId', internalType: 'uint256' },
      {
        type: 'uint256',
        name: 'count',
        internalType: 'uint256',
      },
      { type: 'uint256', name: 'pixelPrice', internalType: 'uint256' },
      {
        type: 'uint256',
        name: 'sgbPrice',
        internalType: 'uint256',
      },
    ],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [
      {
        type: 'tuple',
        name: '',
        internalType: 'struct Chest',
        components: [
          { type: 'uint256[][6][]', name: 'items', internalType: 'uint256[][6][]' },
          {
            type: 'uint256[6]',
            name: 'ratios',
            internalType: 'uint256[6]',
          },
        ],
      },
    ],
    name: 'getChest',
    inputs: [{ type: 'uint256', name: 'chestId', internalType: 'uint256' }],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [
      {
        type: 'tuple[]',
        name: '',
        internalType: 'struct Chest[]',
        components: [
          { type: 'uint256[][6][]', name: 'items', internalType: 'uint256[][6][]' },
          {
            type: 'uint256[6]',
            name: 'ratios',
            internalType: 'uint256[6]',
          },
        ],
      },
    ],
    name: 'getChests',
    inputs: [],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [{ type: 'uint256', name: '', internalType: 'uint256' }],
    name: 'getChestsCount',
    inputs: [],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [
      {
        type: 'tuple[]',
        name: '',
        internalType: 'struct MarketItem[]',
        components: [
          { type: 'uint256', name: 'chestId', internalType: 'uint256' },
          {
            type: 'uint256',
            name: 'count',
            internalType: 'uint256',
          },
          { type: 'uint256', name: 'pixelPrice', internalType: 'uint256' },
          {
            type: 'uint256',
            name: 'sgbPrice',
            internalType: 'uint256',
          },
        ],
      },
    ],
    name: 'getMarket',
    inputs: [],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [
      {
        type: 'tuple',
        name: '',
        internalType: 'struct MarketItem',
        components: [
          { type: 'uint256', name: 'chestId', internalType: 'uint256' },
          {
            type: 'uint256',
            name: 'count',
            internalType: 'uint256',
          },
          { type: 'uint256', name: 'pixelPrice', internalType: 'uint256' },
          {
            type: 'uint256',
            name: 'sgbPrice',
            internalType: 'uint256',
          },
        ],
      },
    ],
    name: 'getMarketItem',
    inputs: [{ type: 'uint256', name: 'itemId', internalType: 'uint256' }],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [{ type: 'uint256', name: '', internalType: 'uint256' }],
    name: 'getMarketItemsCount',
    inputs: [],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [{ type: 'bytes32', name: '', internalType: 'bytes32' }],
    name: 'getRoleAdmin',
    inputs: [{ type: 'bytes32', name: 'role', internalType: 'bytes32' }],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'grantRole',
    inputs: [
      { type: 'bytes32', name: 'role', internalType: 'bytes32' },
      {
        type: 'address',
        name: 'account',
        internalType: 'address',
      },
    ],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [{ type: 'bool', name: '', internalType: 'bool' }],
    name: 'hasRole',
    inputs: [
      { type: 'bytes32', name: 'role', internalType: 'bytes32' },
      {
        type: 'address',
        name: 'account',
        internalType: 'address',
      },
    ],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'renounceRole',
    inputs: [
      { type: 'bytes32', name: 'role', internalType: 'bytes32' },
      {
        type: 'address',
        name: 'callerConfirmation',
        internalType: 'address',
      },
    ],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'revokeRole',
    inputs: [
      { type: 'bytes32', name: 'role', internalType: 'bytes32' },
      {
        type: 'address',
        name: 'account',
        internalType: 'address',
      },
    ],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'sendChest',
    inputs: [
      { type: 'uint256', name: 'chestId', internalType: 'uint256' },
      {
        type: 'address',
        name: 'account',
        internalType: 'address',
      },
    ],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'setChestRatios',
    inputs: [
      { type: 'uint256', name: 'chestId', internalType: 'uint256' },
      {
        type: 'uint256[6]',
        name: 'ratios',
        internalType: 'uint256[6]',
      },
    ],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'setCoreAddress',
    inputs: [{ type: 'address', name: 'newAddress', internalType: 'address' }],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'setMarketItemCount',
    inputs: [
      { type: 'uint256', name: 'itemId', internalType: 'uint256' },
      {
        type: 'uint256',
        name: 'count',
        internalType: 'uint256',
      },
    ],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'setMaxPXLsBonus',
    inputs: [{ type: 'uint256', name: 'amount', internalType: 'uint256' }],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'setNFTAddress',
    inputs: [{ type: 'address', name: 'newAddress', internalType: 'address' }],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'setReceiver',
    inputs: [{ type: 'address', name: 'account', internalType: 'address payable' }],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'setUserChances',
    inputs: [
      { type: 'uint64', name: 'userId', internalType: 'uint64' },
      {
        type: 'uint24[6]',
        name: 'chances',
        internalType: 'uint24[6]',
      },
    ],
  },
  {
    type: 'function',
    stateMutability: 'view',
    outputs: [{ type: 'bool', name: '', internalType: 'bool' }],
    name: 'supportsInterface',
    inputs: [{ type: 'bytes4', name: 'interfaceId', internalType: 'bytes4' }],
  },
  {
    type: 'function',
    stateMutability: 'nonpayable',
    outputs: [],
    name: 'updateMarketItem',
    inputs: [
      { type: 'uint256', name: 'itemId', internalType: 'uint256' },
      {
        type: 'uint256',
        name: 'pixelPrice',
        internalType: 'uint256',
      },
      { type: 'uint256', name: 'sgbPrice', internalType: 'uint256' },
    ],
  },
  { type: 'receive', stateMutability: 'payable' },
];
