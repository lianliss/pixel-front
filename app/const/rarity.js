export const RARITY_COLORS = [
  '#ffffff',
  '#ffffff',
  '#84ff00',
  '#0048ff',
  '#9900ff',
  '#ffaa00',
];

export const RARITY_NAMES = [
  'PXLs Drop',
  'Common',
  'Uncommon',
  'Rare',
  'Epic',
  'Legendary',
];