const getConfig = (color = "#ffaa00") => {
  const baseEmitterConfig = (direction, position) => {
    return {
      direction,
      rate: {
        quantity: 50,
        delay: 0.1
      },
      size: {
        width: 0,
        height: 0
      },
      spawnColor: {
        value: color,
        animation: {
          h: {
            enable: false,
            offset: {
              min: -1.4,
              max: 1.4
            },
            speed: 2,
            sync: false
          },
          l: {
            enable: true,
            offset: {
              min: 40,
              max: 60
            },
            speed: 0,
            sync: false
          }
        }
      },
      position
    };
  };
  
  const configs = {
    background: {
      color: "#000"
    },
    particles: {
      angle: {
        value: 0,
        offset: 10
      },
      move: {
        enable: true,
        outModes: {
          top: "none",
          default: "destroy"
        },
        gravity: {
          enable: false
        },
        speed: { min: 5, max: 40 },
        decay: 0.01
      },
      number: {
        value: 0,
        limit: 500
      },
      opacity: {
        value: 1
      },
      shape: {
        type: ["circle"]
      },
      size: {
        value: { min: 1, max: 3 },
        animation: {
          count: 1,
          startValue: "min",
          enable: true,
          speed: 5,
          sync: true
        }
      },
      rotate: {
        value: {
          min: 0,
          max: 360
        },
        direction: "random",
        animation: {
          enable: true,
          speed: 60
        }
      },
      tilt: {
        direction: "random",
        enable: false,
        value: {
          min: 0,
          max: 360
        },
        animation: {
          enable: false,
          speed: 60
        }
      },
      roll: {
        darken: {
          enable: true,
          value: 25
        },
        enable: false,
        speed: {
          min: 15,
          max: 25
        }
      },
      wobble: {
        distance: 30,
        enable: true,
        speed: {
          min: -15,
          max: 15
        }
      }
    },
    emitters: [
      baseEmitterConfig("top", { x: 50, y: 100 }),
    ]
  };
  return configs;
}

export default getConfig;
