'use strict';

export const INITIAL_APP_STATE = {
    account: null,
    isAuthorized: true,
    settings: {},
    adaptive: false,
    gasless: 0,
    isHideMenuButton: false,
    starsPrices: {},
};
export default INITIAL_APP_STATE;
