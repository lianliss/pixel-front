'use strict';

export const INITIAL_MINERS_STATE = {
  second: []
};

export const INITIAL_MINER_STATE = {
  claimed: 0,
  mined: 0,
  rewardPerSecond: 0.000001,
  sizeLimit: 100,
  claimTimestamp: undefined,
  lastGetTimestamp: Date.now(),
  isMiningLoading: true,
  isClaiming: false,
}

export default INITIAL_MINERS_STATE;
