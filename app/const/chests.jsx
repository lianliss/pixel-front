import {COSTON2, FLARE, SONGBIRD, SWISSTEST} from "services/multichain/chains";

export const SONGBIRD_CHESTS = {
  0: 'Unreachable',
  1: 'Gratitude Legendary',
  2: 'Gratitude Epic',
  3: 'Gratitude Rare',
  4: 'Weekly',
  5: 'Developer',
  6: 'StressTest',
  7: 'OGCommunity',
  8: 'PXLs Box',
  9: 'SGB Box',
  10: 'PXLs Box II',
  11: 'SGB Box II',
};

export const SWISS_CHESTS = {
  0: 'Unreachable',
  1: 'NFT box first',
  2: 'NFT box second',
  3: 'Money box',
  4: 'Greetings box',
  5: 'Quests box',
  6: 'Progress box',
}

export const FLARE_CHESTS = {
  0: 'Unreachable',
  1: 'FINU chest',
}

export const COSTON2_CHESTS = {
  0: 'Unreachable',
  1: 'Market Test',
}

export const getChestNames = chainId => {
  switch (chainId) {
    case SWISSTEST:
      return SWISS_CHESTS;
    case SONGBIRD:
      return SONGBIRD_CHESTS;
    case FLARE:
      return FLARE_CHESTS;
    case COSTON2:
      return COSTON2_CHESTS;
    default:
      return {
        0: 'Unreachable',
      };
  }
}