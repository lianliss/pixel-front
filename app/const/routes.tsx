'use strict';
import React from 'react';
import { ADMIN_ACCOUNTS } from '@cfg/config.ts';
import { isMarketAvailable } from 'lib/Marketplace/api-contract/contracts';
import { isSlotsAvailable } from 'lib/Inventory/api-contract/slots.constants';

import SvgProfile from '@ui-kit/icon/generated/sidebar/Profile.tsx';
import SvgDigitalSoul from '@ui-kit/icon/generated/sidebar/DigitalSoul.tsx';
import SvgWallet from '@ui-kit/icon/generated/sidebar/Wallet.tsx';
import SvgMiner from '@ui-kit/icon/generated/sidebar/Miner.tsx';
import SvgInventory from '@ui-kit/icon/generated/sidebar/Inventory.tsx';
import SvgQuest from '@ui-kit/icon/generated/sidebar/Quest.tsx';
import SvgMarketplace from '@ui-kit/icon/generated/sidebar/Marketplace.tsx';
import SvgForge from '@ui-kit/icon/generated/sidebar/Forge.tsx';
import SvgDashboard from '@ui-kit/icon/generated/sidebar/Dashboard.tsx';
import SvgAchievements from '@ui-kit/icon/generated/sidebar/Achievements.tsx';
import SvgLeaderboard from '@ui-kit/icon/generated/sidebar/Leaderboard.tsx';
import SvgFriends from '@ui-kit/icon/generated/sidebar/Friends.tsx';
import SwapHorizIcon from '@mui/icons-material/SwapHoriz';
import SwapHorizontalCircleIcon from '@mui/icons-material/SwapHorizontalCircle';
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import AppSettingsAltIcon from '@mui/icons-material/AppSettingsAlt';
import SwapVerticalCircleIcon from '@mui/icons-material/SwapVerticalCircle';
import RocketLaunchIcon from '@mui/icons-material/RocketLaunch';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard';
import LightbulbCircleOutlined from '@mui/icons-material/LightbulbCircleOutlined';
import HeatPumpIcon from '@mui/icons-material/HeatPump';
import ViewInArIcon from '@mui/icons-material/ViewInAr';
import EvStationIcon from '@mui/icons-material/EvStation';
import SchoolIcon from '@mui/icons-material/School';
import InfoIcon from '@mui/icons-material/Info';
import AutoAwesomeIcon from '@mui/icons-material/AutoAwesome';
import CableIcon from '@mui/icons-material/Cable';
import HistoryIcon from '@mui/icons-material/History';
import LogoutIcon from '@mui/icons-material/Logout';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import BrokenImageIcon from '@mui/icons-material/BrokenImage';
import AssignmentIcon from '@mui/icons-material/Assignment';
import Inventory2Icon from '@mui/icons-material/Inventory2';
import FireTruckIcon from '@mui/icons-material/FireTruck';
import {BRIDGE_CHAINS, EXCHANGE_CHAINS} from "@cfg/app";
import ApartmentIcon from '@mui/icons-material/Apartment';
import CategoryIcon from '@mui/icons-material/Category';

export interface IRoute {
  path?: string;
  link?: string;
  title: string;
  svg?: React.FC<any>;
  render?: React.LazyExoticComponent<any>;
  isAvailable?: (a: unknown) => boolean;
  isSidebar?: (a: unknown) => boolean;
  disabled?: boolean;
  onClick?: (account: any) => void;
}

export interface IRoutes {
  home: IRoute;
  swap: IRoute;
  dex: IRoute;
  wallet: IRoute;
  inventory: IRoute;
  admin: IRoute;
  walletRecover: IRoute;
  walletMining: IRoute;
  walletFriends: IRoute;
  walletBuild: IRoute;
  walletSettings: IRoute;
  walletQuests: IRoute;
  walletQuestsTab: IRoute;
  walletToken: IRoute;
  walletTransfer: IRoute;
  dashboard: IRoute;
  airdrop: IRoute;
  p2p: IRoute;
  exchange: IRoute;
  exchangeCurrency: IRoute;
  farming: IRoute;
  extractor: IRoute;
  collection: IRoute;
  nftMarket: IRoute;
  leaderboards: IRoute;
  achievements: IRoute;
  nftStacking: IRoute;
  borrow: IRoute;
  education: IRoute;
  about: IRoute;
  preseed: IRoute;
  connect: IRoute;
  digitalSoul: IRoute;
  history: IRoute;
  myProfile: IRoute;
  profile: IRoute;
  adminNft: IRoute;
  adminQuests: IRoute;
  adminChests: IRoute;
  adminChestSales: IRoute;
  adminUserPreview: IRoute;
  adminRecipes: IRoute;
  forgeClaimer: IRoute;
  forgeRandomNft: IRoute;
  bridge: IRoute;
  docs: IRoute;
  connectWallet: IRoute;
  bounty: IRoute;
  lotteryList: IRoute;
  adminBounty: IRoute;
  adminBlender: IRoute;
  adminLottery: IRoute;
  adminQuest: IRoute;
}

const routes: IRoutes = {
  home: {
    path: '/',
    title: 'Wallet',
    svg: SvgWallet,
    render: React.lazy(() => import('lib/Wallet/pages/wallet')),
  },
  exchange: {
    path: '/exchange/',
    title: 'Exchange',
    svg: SwapHorizontalCircleIcon,
    render: React.lazy(() => import('lib/Exchange/pages/root')),
  },
  bounty: {
    path: '/bounty/',
    title: 'Bounty',
    svg: ApartmentIcon,
    render: React.lazy(() => import('lib/Bounty/pages/root')),
  },
  lotteryList: {
    path: '/lottery',
    title: 'Lottery',
    svg: CategoryIcon,
    render: React.lazy(() => import('lib/Lottery/pages/list')),
  },
  adminBounty: {
    path: '/admin/bounty',
    title: 'Admin Bounty',
    svg: ApartmentIcon,
    render: React.lazy(() => import('lib/Bounty/pages/admin.tsx')),
  },
  adminBlender: {
    path: '/admin/blender',
    title: 'Admin Blender',
    svg: ApartmentIcon,
    render: React.lazy(() => import('lib/Forge/pages/admin-blender')),
  },
  adminQuest: {
    path: '/admin/quest',
    title: 'Admin Quests',
    svg: ApartmentIcon,
    render: React.lazy(() => import('lib/Quests/pages/admin.tsx')),
  },
  adminLottery: {
    path: '/admin/lottery',
    title: 'Admin Lottery',
    svg: ApartmentIcon,
    render: React.lazy(() => import('lib/Lottery/pages/admin.tsx')),
  },
  dex: {
    path: '/swap',
    title: 'Swap',
    svg: SwapHorizIcon,
    render: React.lazy(() => import('lib/Dex/pages/ExchangePage'))
  },
  wallet: {
    path: '/wallet/',
    title: 'Wallet',
    svg: SvgWallet,
    render: React.lazy(() => import('lib/Wallet/pages/wallet')),
  },
  walletRecover: {
    path: '/wallet/recover',
    title: 'Wallet Recover',
    svg: AccountCircleIcon,
    render: React.lazy(() => import('lib/Wallet/RecoverWalletForm/RecoverWalletForm.js')),
  },
  walletMining: {
    path: '/wallet/mining/',
    title: 'Wallet Mining',
    svg: SvgMiner,
    render: React.lazy(() => import('../lib/Mining/pages/root/index.jsx')),
  },
  walletFriends: {
    path: '/wallet/friends/',
    title: 'Invite Friends',
    svg: SvgFriends,
    render: React.lazy(() => import('lib/Friends/pages/root/index.ts')),
  },
  walletBuild: {
    path: '/wallet/build/',
    title: 'Wallet Build',
    svg: AccountCircleIcon,
    render: React.lazy(() => import('../lib/Mining/pages/build/index.jsx')),
  },
  inventory: {
    path: '/inventory/',
    title: 'Inventory',
    svg: SvgInventory,
    render: React.lazy(() => import('../lib/Inventory/pages/root/index.jsx')),
    isAvailable: (a) => isSlotsAvailable(a?.chainId),
  },
  walletQuests: {
    path: '/wallet/quests/',
    title: 'Quests',
    svg: SvgQuest,
    render: React.lazy(() => import('../lib/Quests/pages/root/index.jsx')),
  },
  walletQuestsTab: {
    path: '/wallet/quests/:tab',
    title: 'Wallet Quests',
    svg: SvgQuest,
    render: React.lazy(() => import('../lib/Quests/pages/root/index.jsx')),
  },
  walletToken: {
    path: '/wallet/token/:symbol',
    title: 'Wallet Token',
    svg: AccountCircleIcon,
    render: React.lazy(() => import('../lib/Wallet/pages/token/index.jsx')),
  },
  dashboard: {
    path: '/dashboard/',
    title: 'Dashboard',
    svg: SvgDashboard,
    render: React.lazy(() => import('../lib/Dashboard/pages/root/index.jsx')),
    disabled: false,
  },
  nftMarket: {
    path: '/nft-market',
    title: 'NFT Market',
    svg: SvgMarketplace,
    render: React.lazy(() => import('../lib/Marketplace/pages/root/index.jsx')),
    isAvailable: (a) => isMarketAvailable(a?.chainId),
  },
  leaderboards: {
    path: '/leaderboard/',
    title: 'Leaderboard',
    svg: SvgLeaderboard,
    render: React.lazy(() => import('../lib/Leaderboard/index.jsx')),
  },
  achievements: {
    path: '/achievement/',
    title: 'Achievements',
    svg: SvgAchievements,
    render: React.lazy(() => import('../lib/Achievement/pages/achievements/index.jsx')),
  },
  digitalSoul: {
    path: '/digital-soul/',
    title: 'Digital Soul',
    svg: SvgDigitalSoul,
    render: React.lazy(() => import('../lib/DigitalSoul/pages/root/index.jsx')),
  },
  profile: {
    path: '/profile/:address',
    title: 'Profile',
    svg: SvgProfile,
    render: React.lazy(() => import('lib/User/pages/profile')),
  },
  forgeClaimer: {
    path: '/forge/smart-claimer',
    title: 'Forge',
    svg: SvgForge,
    render: React.lazy(() => import('lib/Forge/pages/miner-claimer/ForgeDroneMiner.container.tsx')),
  },
  forgeRandomNft: {
    path: '/forge/random-nft',
    title: 'NFT Blender',
    svg: SvgForge,
    render: React.lazy(() => import('lib/Forge/pages/random-nft/ForgeRandomNft.container.tsx')),
  },
  admin: {
    path: '/admin',
    title: 'Admin',
    svg: AdminPanelSettingsIcon,
    isAvailable: (account) => {
      return ADMIN_ACCOUNTS.includes(account?.accountAddress?.toLowerCase());
    },
    render: React.lazy(() => import('../lib/AdminV5/pages/old/index.jsx')),
  },
  walletSettings: {
    path: '/wallet/settings/',
    title: 'Settings',
    svg: AppSettingsAltIcon,
    render: React.lazy(() => import('lib/Wallet/pages/settings')),
    isSidebar: (account) => account.isConnected,
  },
  walletTransfer: {
    path: '/wallet/transfer/:symbol',
    title: 'Wallet Transfer',
    svg: AccountCircleIcon,
    render: React.lazy(() => import('../lib/Wallet/pages/transfer/index.jsx')),
  },
  airdrop: {
    path: '/airdrop/',
    title: 'Airdrop',
    svg: CardGiftcardIcon,
    render: React.lazy(() => import('../lib/Airdrop/index.jsx')),
  },
  p2p: {
    path: '/p2p/',
    title: 'P2P',
    svg: SwapVerticalCircleIcon,
    render: React.lazy(() => import('../lib/Dashboard/pages/root/index.jsx')),
    disabled: true,
  },
  exchangeCurrency: {
    path: '/exchange/:from/:to',
    title: 'Exchange',
    svg: SwapHorizIcon,
    render: React.lazy(() => import('lib/Exchange/pages/root')),
    isAvailable: (a) => EXCHANGE_CHAINS.includes(a?.chainId),
  },
  farming: {
    path: '/farming/',
    title: 'Farming',
    svg: RocketLaunchIcon,
    render: React.lazy(() => import('../lib/Dashboard/pages/root/index.jsx')),
    disabled: true,
  },
  extractor: {
    path: '/extractor/',
    title: 'Pixel Extractor',
    svg: LightbulbCircleOutlined,
    render: React.lazy(() => import('../lib/Dashboard/pages/root/index.jsx')),
    disabled: true,
  },
  collection: {
    path: '/collection/',
    title: 'Collection',
    svg: HeatPumpIcon,
    render: React.lazy(() => import('../lib/Dashboard/pages/root/index.jsx')),
    disabled: true,
  },
  nftStacking: {
    path: '/nft-stacking/',
    title: 'NFT Stacking',
    svg: ViewInArIcon,
    render: React.lazy(() => import('../lib/Dashboard/pages/root/index.jsx')),
    disabled: true,
  },
  borrow: {
    path: '/borrow/',
    title: 'Borrow',
    svg: EvStationIcon,
    render: React.lazy(() => import('../lib/Dashboard/pages/root/index.jsx')),
    disabled: true,
  },
  education: {
    path: '/education/',
    title: 'Pixel Education',
    svg: SchoolIcon,
    render: React.lazy(() => import('../lib/Dashboard/pages/root/index.jsx')),
    disabled: true,
  },
  about: {
    path: '/about/',
    title: 'About',
    svg: InfoIcon,
    render: React.lazy(() => import('../lib/Dashboard/pages/root/index.jsx')),
    disabled: true,
  },
  preseed: {
    path: '/preseed/',
    title: 'Invest',
    svg: AutoAwesomeIcon,
    render: React.lazy(() => import('../lib/Preseed/index.jsx')),
    disabled: false,
  },
  connect: {
    path: '/connect',
    title: 'Connect',
    svg: CableIcon,
    render: React.lazy(() => import('../lib/Connect/index.js')),
    disabled: false,
  },
  history: {
    path: '/history',
    title: 'History',
    svg: HistoryIcon,
    render: React.lazy(() => import('../lib/History/pages/root/index.js')),
  },
  myProfile: {
    path: '/profile',
    title: 'Profile',
    svg: SvgProfile,
    render: React.lazy(() => import('lib/User/pages/profile')),
  },

  // ADMIN
  adminNft: {
    path: '/editor',
    title: 'Editor NFT',
    svg: BrokenImageIcon,
    render: React.lazy(() => import('lib/AdminV5/pages/nft-editor')),
  },
  adminQuests: {
    path: '/editor/quests',
    title: 'Editor Quests',
    svg: AssignmentIcon,
    render: React.lazy(() => import('../lib/AdminV5/pages/quests/index.jsx')),
  },
  adminChestSales: {
    path: '/editor/sales',
    title: 'Admin Sales',
    svg: ApartmentIcon,
    render: React.lazy(() => import('app/lib/Chests/pages/admin-chest-sales')),
  },
  adminChests: {
    path: '/editor/chests',
    title: 'Admin Chests',
    svg: ApartmentIcon,
    render: React.lazy(() => import('app/lib/Chests/pages/admin-chests')),
  },
  adminUserPreview: {
    path: '/editor/user',
    title: 'Admin User',
    svg: Inventory2Icon,
    render: React.lazy(() => import('../lib/AdminV5/pages/user-preview/index.jsx')),
  },
  adminRecipes: {
    path: '/admin/recipe',
    title: 'Admin Recipes',
    svg: Inventory2Icon,
    render: React.lazy(() => import('../lib/Forge/pages/admin-recipes')),
  },
  bridge: {
    path: '/bridge/',
    title: 'Bridge',
    svg: FireTruckIcon,
    render: React.lazy(() => import('../lib/Bridge/pages/root/index.tsx')),
    isSidebar: (a) => BRIDGE_CHAINS.includes(a?.chainId),
  },

  docs: {
    title: 'Docs',
    link: 'https://docs.hellopixel.network/hello-pixel/pixel-introduce',
    svg: HelpOutlineIcon,
  },
};

export default routes;
