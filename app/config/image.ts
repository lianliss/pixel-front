import sgbImage from 'assets/img/mining/bg.png';
import unitsImage from 'assets/img/mining/units-bg.png';
import newYearImage from 'assets/img/decor/new-year/bg.png';
// import skaleImage from 'assets/img/mining/skale-bg.png';
import {Chain} from "services/multichain/chains";
import {IS_DECOR_ENABLED} from "@cfg/app.ts";

export const getDefaultChainImage = (chainId: number): string | undefined => {
    if (IS_DECOR_ENABLED) {
        return newYearImage as string;
    }

    if (chainId === Chain.UNITS) {
        return unitsImage as string;
    }
    if ([Chain.SONGBIRD].includes(chainId)) {
        return sgbImage as string;
    }
    if ([Chain.SKALE, Chain.SKALE_TEST].includes(chainId)) {
        return sgbImage as string;
    }

    return undefined;
}
