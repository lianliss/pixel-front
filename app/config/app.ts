import {IS_DEVELOP, IS_STAGE, IS_TELEGRAM} from "@cfg/config.ts";
import {Chain} from "services/multichain/chains";

const IS_TEST_FEATURE = (IS_DEVELOP || IS_STAGE);

export const DAILY_CHEST_CHAINS = [Chain.SKALE, Chain.SKALE_TEST, Chain.SONGBIRD];

export const WEEKLY_CHEST_CHAINS = [Chain.UNITS];

export const BOXES_CHEST_CHAINS = [
    // Chain.SONGBIRD,
    Chain.SKALE_TEST,
    Chain.SKALE,
];

// profile

export const IS_PROFILE_NFT = true;

export const IS_PROFILE_CHEST = (IS_STAGE || IS_DEVELOP);

export const IS_PROFILE_KARMA = (IS_STAGE || IS_DEVELOP);

// Other

const ENABLED_DEV_ALERTS = false;

export const IS_FORGE = (IS_STAGE || IS_DEVELOP);

export const IS_EXCHANGE = (IS_STAGE || IS_DEVELOP);
export const IS_EXCHANGE_SKALE = true;

export const IS_DEX = IS_DEVELOP;

export const EXCHANGE_CHAINS = [Chain.FLARE, ...(IS_EXCHANGE_SKALE ? [Chain.SKALE] : [])];

export const BRIDGE_CHAINS = [Chain.POLYGON_MAINNET, Chain.SKALE];

export const SHOW_NO_TRADE_TOKEN_ALERT = (IS_DEVELOP) && ENABLED_DEV_ALERTS;

export const SHOW_NO_GAS_TOKEN_ALERT = (IS_DEVELOP) && ENABLED_DEV_ALERTS;

export const IS_LANG = true;

// global

export const IS_PAYABLE = true;

export const IS_BOUNTY = true;

export const IS_BRIDGE = true;

export const IS_LOTTERY = (IS_DEVELOP || IS_STAGE);

export const IS_MOBILE_RELATIVE_SIDEBAR = false && (IS_DEVELOP || IS_STAGE);

export const IS_MOBILE = IS_TELEGRAM || !!navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i) || ('ontouchstart' in window);

// autoclaimer

export const IS_MINER_DRONE = true;

export const IS_FORGE_PAGE = true;

export const IS_SKALE_SMART_CLAIMER = true;

// craft

export const IS_FORGE_CRAFT = IS_DEVELOP;

// miner

export const IS_MINER_ZAR = true;

export const IS_SKALE_CHEST = true;

export const IS_DAILY_CHEST_ENABLED = [Chain.SONGBIRD, ...(IS_SKALE_CHEST ? [Chain.SKALE] : [])];

export const IS_WEEKLY_CHEST_ENABLED = [...(IS_TEST_FEATURE ? [Chain.UNITS] : [])];

export const TUTORIAL_INVENTORY_ENABLED = IS_TEST_FEATURE;

export const TUTORIAL_MINING_ENABLED = IS_TEST_FEATURE;

export const TUTORIAL_MINING_BUILD_ENABLED = IS_TEST_FEATURE;

export const IS_DECOR_ENABLED = false;

// chest daily

export const IS_CHEST_DAILY_SATOSHI = true;

// drone

export const IS_DRONE = IS_DEVELOP || IS_STAGE;

export const DRONE_CHAINS = [
    // Chain.SONGBIRD,
        ...(IS_DRONE ?  [Chain.UNITS] : []),
];

export const IS_CONFIRM_TX = IS_TELEGRAM || IS_DEVELOP;

export const IS_SHOP = true;
