export const IS_WAGMI = true;

import get from 'lodash/get';
import { telegramIdKey } from '../shared/const/localStorage.ts';

export const APP_DOM_ID = 'app';
export const APP_DOM_CLASS = 'app';

export const APP_DEFAULT_LANGUAGE = 'en';

export const IS_DEVELOP =
  import.meta.env.VITE_NODE_ENV === 'development' || import.meta.env.NODE_ENV === 'development';
export const IS_MAX = import.meta.env.VITE_BOT === 'MAX';
export const IS_HAN = import.meta.env.VITE_BOT === 'HAN';
export const IS_STAGE = import.meta.env.VITE_BOT === 'STAGE';
export const IDENTITY_TOKEN = import.meta.env.VITE_IDENTITY_TOKEN;
export const PROJECT_VERSION = import.meta.env.VITE_VERSION;
export const BUILD_ID = import.meta.env.VITE_BUILD_ID;
export const IS_DEVELOP_FEATURE = IS_DEVELOP || IS_STAGE;

const loc = document.location;
export const API_URL = IS_DEVELOP
  ? 'https://hellopixel.network' //'http://localhost:3000'
  : `${loc.protocol}//${location.host}`;
export const STORAGE_URL = import.meta.env.STORAGE_URL;

export const IS_TELEGRAM = !!get(window, 'Telegram.WebApp.initData', '').length;
export const IS_WALLET_CONTEXT = IS_TELEGRAM || !!localStorage.getItem(telegramIdKey);
export const DISABLE_BACKEND = !IS_TELEGRAM;

export const ADMIN_ACCOUNTS = [
  '0xcC4452CE72f300a7d4ac48c8C09E00d57Fe1BB7a'.toLowerCase(), // Deployer
  '0x9F6627E8aDF52430be0abF77d40654aB8F82deF4'.toLowerCase(), // Test Wallet
  '0x69F8B48c8AdF4A2E9de9e08d9922c3c916b7252D'.toLowerCase(),
  '0x50915E1bc60D4E3B7bb9Cda870F5891B97e0e603'.toLowerCase(), // Max
];

export const EDITOR_ACCOUNTS = [
  '0xcC4452CE72f300a7d4ac48c8C09E00d57Fe1BB7a'.toLowerCase(), // Deployer
  '0xD7676650180bCB4135D77bB26Db6925F481a789E'.toLowerCase(), // Zuka & Max
  '0x366067Cf009BB2047C107C8BBC32704ba239bd92'.toLowerCase(), // zuka
  '0xAd4fD368C324Af5DD33Fe82087bE5FD9F883f953'.toLowerCase(),
  '0xA494A48d52d12a25eC1416478defc4C650e832Eb'.toLowerCase(),
  '0x50915E1bc60D4E3B7bb9Cda870F5891B97e0e603'.toLowerCase(), // Max
  '0x8E71B0cAb46d78Ab232fED54B6A27C0d11C9fE49'.toLowerCase(), // Vital
  '0x7D25ccd34A204f875C39eCFdD47B5D659689d39c'.toLowerCase(), // Vital 2
  '0x4B9Ac70305761c8f1c4e88AD6C54499b15674a07'.toLowerCase(), // max
  '0x6e20F7cD3ACA2F03FF6ACbD4959Ef8375b6866D3'.toLowerCase(), // max server
  '0x6E069DC2deFfacB3921A26836969d8f1BA49F96D'.toLowerCase(), // market bot
  '0x9C87A0F0A18F148294B54DE5526b6eE66878D3F7'.toLowerCase(), // leaderboard
  '0xCeA672d2A45ef064911D15096a6C7EDc3557e847'.toLowerCase(), // chests
  '0xcec46bf84FcA2F118A17A44124726BF549B42AbF'.toLowerCase(), // Neznaika
];

export const STORAGE_KEY_CHAIN_ID = 'wallet-chainId';

export const ON_BLUR_CLOSE_TIMEOUT = 60000 * 15;

export const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

export const IS_TON_ENABLED = IS_TELEGRAM;

export const TON_CONNECT_MANIFEST_URL = IS_STAGE
  ? 'https://hellopixel.network/static/tonconnect.stage.json'
  : IS_DEVELOP
    ? 'https://hellopixel.network/static/tonconnect.dev.json'
    : 'https://hellopixel.network/static/tonconnect.json';

export const TON_CONNECT_RETURN_TMA_URL = IS_STAGE
  ? 'https://t.me/pixel_wallet_bot/wallet'
  : IS_DEVELOP
    ? 'https://t.me/hp_wallet_bot/dev'
    : 'https://t.me/stage_pixel_bot/stage';