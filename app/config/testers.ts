const testers = {
    ['0x4B9Ac70305761c8f1c4e88AD6C54499b15674a07'.toLowerCase()]: true, // max
    ['0xceBD2e185A825644083C197c685EC563887A1E59'.toLowerCase()]: true,
    ['0x9F6627E8aDF52430be0abF77d40654aB8F82deF4'.toLowerCase()]: true,
    ['0x6E069DC2deFfacB3921A26836969d8f1BA49F96D'.toLowerCase()]: true,
    ['0x366067Cf009BB2047C107C8BBC32704ba239bd92'.toLowerCase()]: true, // zuka
    ['0xcC4452CE72f300a7d4ac48c8C09E00d57Fe1BB7a'.toLowerCase()]: true, // lianliss
}

export const isTester = (accountAddress: string): boolean => {
    return testers[accountAddress.toLowerCase()] || false;
}
