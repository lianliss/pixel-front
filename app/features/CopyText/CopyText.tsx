import React from 'react';
import toaster, { ToasterInterface } from 'services/toaster.tsx';
import { useTranslation } from 'react-i18next';

interface Callback {
  copy(): void;
}

type Props = {
  text: string;
  children({ Copy }: Callback): JSX.Element;
};

const CopyText = ({ children, text }: Props) => {
  const { t } = useTranslation('notifications');

  const copyToClipboard = async () => {
    if (text) {
      await navigator.clipboard.writeText(text);
    }
    (toaster as ToasterInterface).success(`${t('Successfully copied')}!`);
  };

  return (
    <div>
      {children({
        copy: () => {
          void copyToClipboard();
        },
      })}
    </div>
  );
};

export default CopyText;
