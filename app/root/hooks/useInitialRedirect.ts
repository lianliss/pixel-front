import React from 'react';
import { TelegramApp } from 'utils/libs/telegram.ts';
import { IS_TELEGRAM } from '@cfg/config.ts';
import { useNavigate } from 'react-router-dom';
import routes from '../../const/routes.tsx';
import {useSwitchChain} from "@chain/hooks/useSwitchChain.ts";
import {Chain} from "services/multichain/chains";
import {STORAGE_KEY_CHAIN_ID} from "@chain/connectors/pixel.provider.ts";

const CHAIN_PREFIX = {
  skale: Chain.SKALE,
  unit0: Chain.UNITS,
}

export function useInitialRedirect() {
  const navigate = useNavigate();
  const { switchChainAsync } = useSwitchChain();

  React.useEffect(() => {
    if (IS_TELEGRAM) {
      const startParam = TelegramApp.initDataUnsafe.start_param;

      if (!startParam) {
        return;
      }

      if (!startParam.length || !Number.isNaN(Number(startParam))) {
        navigate(routes.wallet.path);
      } else if (startParam.startsWith('auth_') || startParam.startsWith('auth2_') || startParam.startsWith('sig_')) {
        navigate(routes.connect.path);
      } else if (startParam.startsWith('chain_')) {
        const [_chainPrefix, chainName, ...route] = startParam.split('_');
        console.log('[pixel]', `setup initial chain ${chainName}:${CHAIN_PREFIX[chainName]}`);

        if (chainName in CHAIN_PREFIX) {
          localStorage.setItem(STORAGE_KEY_CHAIN_ID, CHAIN_PREFIX[chainName]);

          switchChainAsync({ chainId: CHAIN_PREFIX[chainName] }).then(() => {
            const chainPath = route.join('_').replaceAll('-', '/');

            navigate(`/${chainPath}`);
          }).catch(console.error);
        }
      } else {
        const path = startParam.replaceAll('-', '/');

        navigate(`/${path}`);
      }
    }
  }, []);
}
