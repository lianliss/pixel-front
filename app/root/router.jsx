'use strict';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter, Routes, Route, useMatch, useLocation } from 'react-router-dom';
import Page from 'shared/layout/Page/Page';
import { Loading } from 'utils/async/load-module';
import routes from 'const/routes.tsx';
import { useInitialRedirect } from 'root/hooks/useInitialRedirect.ts';
import { useAccount } from '@chain/hooks/useAccount';
import analytics from 'shared/analytics';
import { Tutorial } from 'app/widget/tutorial';

function PageContainer({ path, render: Render, component, title, params = {} }) {
  const match = useMatch(path);

  useEffect(() => {
    analytics.trackPage();
  }, [match.pathname]);

  useInitialRedirect();

  return (
    <Page {...{ match, title }}>
      {!!Render ? (
        <React.Suspense fallback={<Loading/>}>
          {/*<BackAction/>*/}
          <Render {...match.params} {...params} />
        </React.Suspense>
      ) : (
        component
      )}
    </Page>
  );
}

function Router() {
  const ac = useAccount();
  const account = useSelector((state) => state.App.account);

  return (
    <BrowserRouter>
      <Routes>
        {Object.keys(routes).map((name) => {
          const { path, lib, component, isAvailable } = routes[name];
          if (
            typeof isAvailable === 'function' &&
            !isAvailable({ ...account, chainId: ac.chainId })
          ) {
            return null;
          }

          return <Route path={path} key={name} element={<PageContainer {...routes[name]} />} />;
        })}
      </Routes>
    </BrowserRouter>
  );
}

export default Router;
