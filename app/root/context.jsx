'use strict';
/**
 * @module app/contexts/Context
 */
import React from 'react';
import { Provider } from 'react-redux';
import Web3Provider, { useWeb3 } from 'services/web3Provider';
import ConnectWalletProvider from 'services/ModalProvider';
import WalletProvider from 'lib/Wallet/context/WalletProvider';
import { THEME_MAP, ThemeProvider } from 'shared/theme/theme.ts';
import WagmiProvider from '../shared/chain/wagmi.provider.tsx';
import { useAccount } from '@chain/hooks/useAccount';
import {TransactionConfirmProvider} from "shared/blocks/TransactionConfirm/TransactionConfirm.provider";
import {EntryScreenProvider} from "shared/blocks/EntryScreen/EntryScreen.provider";
import {SwitchChainProvider} from "app/widget/SwitchChainProvider/SwitchChainProvider";
import {EntryTONProvider} from "shared/blocks/EntryTON/EntryTON.provider";
import {TONProvider} from "services/ton/TON.provider";
import useIsTonChain from "services/ton/hooks/useIsTonChain";
import {Chain} from "services/multichain/chains.js";
import {CssBaseline} from "@mui/material";

export const Context = React.createContext({});
export const ContextConsumer = (Component) => (props) =>
  (
    <Context.Consumer>
      {(values) => <Component {...props} contextValues={values} />}
    </Context.Consumer>
  );

function WrapThemeProvider(props) {
  const account = useAccount() || { chain: 19 };
  const {isTonChain} = useIsTonChain();
  const theme = isTonChain
    ? THEME_MAP[Chain.TON]
    : THEME_MAP[account.chainId] || THEME_MAP.default;

  return <ThemeProvider theme={theme}>{props.children}</ThemeProvider>;
}

export const ContextProvider = ({ id, store, shadowNode, children }) => {
  return (
    <StrictMode disabled>
      <Context.Provider value={{ id, store, shadowNode }}>
        <Provider store={store}>
            <WagmiProvider>
              <SwitchChainProvider>
              <Web3Provider>
                <TONProvider>
                <WrapThemeProvider>
                  <WalletProvider>
                    <ConnectWalletProvider>
                      {/*<CssBaseline/>*/}
                      <TransactionConfirmProvider>
                        <EntryScreenProvider>
                          <EntryTONProvider>
                            {children}
                          </EntryTONProvider>
                        </EntryScreenProvider>
                      </TransactionConfirmProvider>
                    </ConnectWalletProvider>
                  </WalletProvider>
                </WrapThemeProvider>
                </TONProvider>
              </Web3Provider>
              </SwitchChainProvider>
            </WagmiProvider>
        </Provider>
      </Context.Provider>
    </StrictMode>
  );
};

export const StrictMode = (props) =>
  props.disabled ? props.children : <React.StrictMode>{props.children}</React.StrictMode>;

export default ContextProvider;
