'use strict';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Loading } from 'utils/async/load-module';
import get from 'lodash/get';
import { appUpdateAccount, appSetAdaptive } from 'slices/App';
import Router from './router';
import 'process';
import { IS_TELEGRAM, PROJECT_VERSION } from '@cfg/config.ts';
import { adaptiveSelector, networkIDSelector } from 'app/store/selectors';

import { useAccount } from '@chain/hooks/useAccount.ts';
import { loadOraclePrices } from 'lib/Wallet/api-server/oracle.store';
import { useConnect } from '@chain/hooks/useConnect';
import { Chain } from 'services/multichain/chains';
import {getSelfUserInfo} from "lib/User/api-server/user.store";
import {createSign, hasSign} from "@chain/auth/create-sign";
import {isBlockedAddress, isBlockedTelegram} from "lib/User/utils/blocked.users";
import AccountBlocked from "app/widget/AccountBlocked/AccountBlocked";
import {TechnicalWorks} from "ui/TechnicalWorks/TechnicalWorks";
import {useSelfUserInfo} from "lib/User/hooks/useUserInfo";
import {pixelStorage} from "@chain/connectors/pixel.provider";

const IS_TECH_WORKS = false;

function AppContainer() {
  const account = useAccount();

  const userInfo = useSelfUserInfo();
  const isSign = hasSign(account.accountAddress);

  const dispatch = useDispatch();
  const isAdaptive = useSelector(adaptiveSelector);
  const [isLoading, setIsLoading] = React.useState(false);
  const connect = useConnect();

  if (IS_TECH_WORKS) {
    return <TechnicalWorks/>
  }

  function onResize() {
    if (IS_TELEGRAM) {
      dispatch(appSetAdaptive(true));
      return;
    }
    if (isAdaptive && window.innerWidth >= 800) {
      dispatch(appSetAdaptive(false));
    }
    if (!isAdaptive && window.innerWidth < 800) {
      dispatch(appSetAdaptive(true));
    }
  }

  React.useEffect(() => {
    window.addEventListener('resize', onResize.bind(this));
    onResize();

    // if (IS_TELEGRAM) {
    //   telegram
    //     .getPrivateKey()
    //     .then((privateKey) => {
    //       const account = getAccount(wagmiConfig);
    //
    //       if (!account.isConnected || account.isReconnecting) {
    //         // wconnect(wagmiConfig, { connector: pixelConnectorState });
    //       }
    //
    //       setIsLoading(false);
    //     })
    //     .catch((error) => {
    //       console.error('[container][getPrivateKey]', error);
    //       setIsLoading(false);
    //     });
    // } else {
    //   setIsLoading(false);
    // }

    return () => {
      window.removeEventListener('resize', onResize.bind(this));
    };
  }, []);
  useEffect(() => {
    if (account.isConnected && !hasSign(account.accountAddress)) {
      createSign(account.accountAddress).then();
    }
  }, [account.isConnected, account.accountAddress]);

  const isDark = ![Chain.SWISSTEST].includes(account.chainId);

  const className = ['container', isDark ? 'bp5-dark' : ''];
  if (IS_TELEGRAM) {
    className.push('telegram');
  }
  className.push(`chain${account.chainId}`);

  React.useEffect(() => {
    document.body.setAttribute('class', `chain${account.chainId}`);
  }, [account.chainId]);

  useEffect(() => {
    loadOraclePrices({}).then();
  }, []);
  useEffect(() => {
    if (account.isConnected && isSign) {
      getSelfUserInfo({ userAddress: account.accountAddress }).then();
    }
  }, [account.isConnected, account.accountAddress, isSign]);

  if (IS_TELEGRAM && isBlockedTelegram(userInfo.data?.telegramId) || isBlockedAddress(account.accountAddress)) {
    return <AccountBlocked/>
  }

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div
      className={className.join(' ')}
      data-version={PROJECT_VERSION}
    >
      <Router />
    </div>
  );
}

export default connect(
  (state) => ({
    account: get(state, 'App.account'),
    adaptive: get(state, 'App.adaptive'),
    networkID: get(state, 'App.networkID'),
  }),
  (dispatch) =>
    bindActionCreators(
      {
        appUpdateAccount,
        appSetAdaptive,
      },
      dispatch
    ),
  null,
  { pure: true }
)(AppContainer);
