'use strict';
import '@babel/polyfill';
import React, { Suspense } from 'react';
import { render } from 'react-dom';
import { createStore } from 'app/store';
import onDomReady from 'app/utils/dom/on-dom-ready';
import getQueryParams from 'app/utils/get-query-params';
import getAppNode from 'app/utils/dom/getNode';
import { IS_TELEGRAM } from '@cfg/config.ts';
import { Loading } from 'utils/async/load-module';
import { ErrorBoundary } from 'react-error-boundary';
import FallbackComponent from 'shared/blocks/FallbackComponent/FallbackComponent';

const LazyContext = React.lazy(() => import('./context.jsx'));
const LazyContainer = React.lazy(() => import('./container.jsx'));

if (IS_TELEGRAM) {
  const oldLog = console.log;
  console.log = (...params) => {
    if (
      params &&
      params.length &&
      typeof params[0] === 'string' &&
      params[0].includes('[Telegram.WebView]')
    )
      return;
    oldLog(...params);
  };
}

class Root extends React.PureComponent {
  static displayName = 'Root';

  store = createStore();

  static start() {
    console.debug('Start React.PureComponent index.jsx');
    onDomReady(() => this.initialize());
  }

  static initialize() {
    console.debug('Start initialize index.jsx');
    const parentNodes = getAppNode();
    if (!parentNodes || !parentNodes.length) {
      throw new Error('Error initializing Store. Missing Application Placeholder Div.');
    }

    const meta = document.createElement('meta');
    meta.name = 'viewport';
    meta.content = 'initial-scale=1';
    document.getElementsByTagName('head')[0].appendChild(meta);

    const shadowNode = parentNodes[0];
    const queryData = getQueryParams(document.location.href);
    const props = {
      shadowNode,
      className: 'app-root',
      queryData,
    };
    render(<Root {...props} />, shadowNode);
  }

  render() {
    const {
      store,
      props: { shadowNode },
      className,
    } = this;
    const props = {
      store,
      shadowNode,
      contextValues: {
        store,
        shadowNode,
      },
    };
    return (
      <ErrorBoundary fallbackRender={FallbackComponent}>
        <Suspense fallback={Loading()}>
          <LazyContext {...props}>
            <LazyContainer />
          </LazyContext>
        </Suspense>
      </ErrorBoundary>
    );
  }
}

export default Root;
