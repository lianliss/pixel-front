import { DEFAULT_CHAIN, SONGBIRD } from 'services/multichain/chains';
import { privateKeyToAccount, sign } from 'web3-eth-accounts';
import { STORAGE_KEY_CHAIN_ID } from '@cfg/config.ts';
import { Wallet, JsonRpcProvider } from 'ethers6';

let pKey;

class PixelWallet {
  selectedAddress = null;
  networkVersion = Number(window.localStorage.getItem(STORAGE_KEY_CHAIN_ID)) || DEFAULT_CHAIN;
  chainId = '0x' + this.networkVersion.toString(16);

  constructor(web3Provider, provider) {
    this.eth = web3Provider.ethHost;
    this.provider = provider;
    this.web3 = web3Provider;
  }

  getEth = () => {
    return this.web3.eth;
  };

  getProvider = () => {
    return this.getEth().provider;
  };

  connect(privateKey) {
    pKey = privateKey;
    const account = privateKeyToAccount(privateKey);
    this.selectedAddress = account.address;
    this.sign = (data) => {
      return sign(data, privateKey);
    };
    this.signTransaction = (tx) => {
      return this.getSigner().signTransaction(tx);
    };
    this.sendTransaction = (tx) => {
      return this.getSigner().sendTransaction(tx);
    };
    this.encrypt = account.encrypt;
    return account;
  }

  getSigner = (privateKey = pKey) => {
    const provider = new JsonRpcProvider(this.getProvider().clientUrl);

    const wallet = new Wallet(privateKey, provider);

    return wallet;
  };

  sign = async (data) => {
    console.error('[PixelWallet][sign] Wallet is not connected (3)');
  };

  signTransaction = async (tx) => {
    console.error('[PixelWallet][signTransaction] Wallet is not connected');
  };

  sendTransaction = async (tx) => {
    console.error('[PixelWallet][sendTransaction] Wallet is not connected');
  };

  encrypt = (password) => {
    console.error('[PixelWallet][encrypt] Wallet is not connected');
  };

  listeners = {
    connect: [],
    accountsChanged: [],
    chainChanged: [],
    disconnect: [],
    message: [],
  };

  emit = (eventName, params) => {
    console.log('[PixelWaller][emit]', eventName, params, this.listeners[eventName]);
    this.listeners[eventName].map((callback) => {
      callback(params);
    });
  };

  on = (eventType, callback) => {
    const listeners = this.listeners[eventType];
    if (!listeners.find((l) => l === callback)) {
      listeners.push(callback);
    }
  };

  removeListener = (eventType, callback) => {
    this.listeners[eventType] = this.listeners[eventType].filter((l) => l !== callback);
  };

  request = async ({ method, params = [] }) => {
    try {
      switch (method) {
        case 'eth_requestAccounts':
          return [this.selectedAddress];
        case 'eth_sendTransaction':
          const transaction = await this.sendTransaction(params[0]);
          console.log('[PixelWallet][eth_sendTransaction] transaction', transaction);
          return transaction.hash;
        case 'personal_sign':
          return this.sign(params[0]).signature;
        case 'wallet_switchEthereumChain':
          this.chainId = params[0].chainId;
          this.networkVersion = Number(params[0].chainId);
          this.eth = this.getEth();
          this.provider = this.getProvider();
          this.emit('chainChanged', params[0].chainId);
          return true;
        case 'wallet_addEthereumChain':
        default:
          return null;
      }
    } catch (error) {
      console.error('[PixelWallet][request]', method, error);
      throw error;
    }
  };
}

export default PixelWallet;
