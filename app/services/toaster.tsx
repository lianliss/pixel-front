import React from 'react';
import { OverlayToaster, Toaster, Intent } from '@blueprintjs/core';
import getFinePrice from 'utils/getFinePrice';
import { processError } from 'utils';
import get from 'lodash/get';
import { AxiosError } from 'axios';
import {BaseError} from "@wagmi/core";
import {decodeContractErrorMessage} from "utils/decodeErrorData.jsx";

export interface ToasterInterface extends Toaster {
  success: (message: string) => void;
  error: (message: string) => void;
  warning: (message: string) => void;
  gas: (gas: number, symbol?: string) => void;
  logError: (error: Error, tag?: string) => void;
  captureException: (e: any, defaultError?: string) => void;
}

const toaster: Toaster & {
  success: (message: string) => void;
  error: (message: string) => void;
  warning: (message: string) => void;
  gas: (gas: number, symbol?: string) => void;
  logError: (err: Error, tag?: string) => void;
  captureException: (err: Error, defaultError?: string) => void;
} = OverlayToaster.create({
  position: 'top',
  usePortal: true,
  className: 'bp5-dark',
}) as any;

toaster.success = (message: string) => {
  if (typeof toaster.show === 'function') {
    toaster.show({
      intent: Intent.SUCCESS,
      message,
      icon: 'endorsed',
    });
  }
};

toaster.error = (message: string) => {
  if (typeof toaster.show === 'function') {
    toaster.show({
      intent: Intent.DANGER,
      message,
      icon: 'error',
      timeout: 10000,
    });
  }
};

toaster.warning = (message: string) => {
  if (typeof toaster.show === 'function') {
    toaster.show({
      intent: Intent.WARNING,
      message,
      icon: 'warning-sign',
    });
  }
};

toaster.gas = (gas: number, symbol: string = 'SGB') => {
  if (typeof toaster.show === 'function') {
    toaster.show({
      intent: Intent.WARNING,
      message: (
        <>
          Not enough gas
          <br />
          Need {getFinePrice(gas)} {symbol}
        </>
      ),
      icon: 'warning-sign',
    });
  }
};

toaster.logError = (error: AxiosError | Error | BaseError, tag: string) => {
  let defaultMessage = error.message;

  // axios error
  if ((error as AxiosError).isAxiosError) {
    toaster.error(`${tag ? tag + ' ' : ''}${(error as AxiosError).response?.data?.message || defaultMessage}`);
    return;
  }

  // wagmi error
  if (error.cause?.reason) {
    toaster.error(`${tag ? tag + ' ' : ''}${error.cause.reason} (${error.name})`);
    return;
  }
  if (error.shortMessage) {
    toaster.error(`${tag ? tag + ' ' : ''}${error.shortMessage}`);
    return;
  }
  if (error.details) {
    toaster.error(error.details);
    return;
  }

  const details = processError(error);
  const message = decodeContractErrorMessage(get(details, 'message', ''));
  console.log('TOASTER LOG MESSAGE', message);

  if (message.includes('[504]') || message.includes('[502]')) {
    toaster.error('Service temporarily unavailable. Please try again later');
    return;
  }

  if (details.isGas) {
    toaster.gas(details.gas);
  } else {
    toaster.error(details.message);
  }

  return details;
};

toaster.captureException = (e: any, defaultError: string = 'Internal server error') => {
  let errMessage = e.response?.data?.error?.message || e.response?.data?.message || (e.abi ? e.shortMessage : undefined) || (e as Error).message;

  errMessage = decodeContractErrorMessage(errMessage);
  console.error(errMessage, e);
  toaster.show({ message: errMessage || defaultError, intent: Intent.DANGER });
};

export default toaster;
