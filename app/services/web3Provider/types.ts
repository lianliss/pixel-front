interface IToken {
  name: string;
  symbol: string;
  address: string;
  chainId: number;
  decimals: number;
  logoURI?: string | null;
  isCustom?: boolean;
  price?: number;
  isPrice?: boolean;
  isGas?: boolean;
  wrapAddress?: string;
}
