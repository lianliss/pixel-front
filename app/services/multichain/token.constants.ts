import {BaseChainToken, Token} from "services/web3Provider/Token";
import {Chain, SKALE} from "services/multichain/chains";

// sgb

export const SGB_PIXEL = new Token(
    'Pixel Shard',
    'PXLs',
    '0x7B3D7cfEe6BD2B3042B8F04D90b137D9Ed06Ea74',
    Chain.SONGBIRD,
    18,
    require('assets/img/token/common/pixel.svg'), //require('styles/svg/logo_icon.svg'),
    false,
    0,
    false,
    true,
);

export const SGB_TOKEN = new BaseChainToken(
    'Songbird',
    'SGB',
    Chain.SONGBIRD,
    18,
    'https://s2.coinmarketcap.com/static/img/coins/64x64/12186.png',
    0.006753
);

export const SGB_JADE_TOKEN = new Token(
    'JADEs Shard',
    'JADEs',
    '0xf4e4697Cc38f73B334daE3D41cAcAC489eFF3439',
    Chain.SONGBIRD,
    18,
    require('assets/mining/jade.png'),
    false,
    0,
    false,
    true,
);

// skale testnet

export const SKALE_TESTNET_TOKEN =  new Token(
    'SKALE',
    'SKL',
    '0x6c71319b1F910Cf989AD386CcD4f8CC8573027aB',
    Chain.SKALE_TEST,
    18,
    'https://s2.coinmarketcap.com/static/img/coins/64x64/5691.png',
);

// skale

export const SKALE_TOKEN = new Token(
    'SKALE',
    'SKL',
    '0xE0595a049d02b7674572b0d59cd4880Db60EDC50',
    Chain.SKALE,
    18,
    'https://s2.coinmarketcap.com/static/img/coins/64x64/5691.png',
);

export const SKALE_PXL_TOKEN = new Token(
    'Pixel Shard',
    'PXLs',
    '0x049eEE4028Fb4fF8591Ea921F15843Cd2d9e9f94',
    Chain.SKALE,
    18,
    require('styles/svg/logo_icon.svg'),
    false,
    0,
    false,
    true,
);

export const SKALE_GAS_TOKEN = new BaseChainToken(
    'sFUEL',
    'sFUEL',
    Chain.SKALE,
    18,
    require('assets/img/token/skale-testnet/gas.png'),
    0,
);

export const SKALE_ZAR_TOKEN = new Token(
    'ZarGates',
    'ZAR',
    '0xbedcCEbB051AD2b8ED8bBEF2C87499278Aeb0345',
    Chain.SKALE,
    18,
    require('assets/img/token/skale/second-1.svg') as string,
    false,
    0,
    false,
    false,
);

// UNITS

export const UNITS_TOKEN = new BaseChainToken(
    'UNIT Zero',
    'UNIT0',
    Chain.UNITS,
    18,
    require('assets/img/token/unit/native.png') as string,
    0,
);

export const UNITS_PIXEL_TOKEN = new Token(
    'Pixel Shard',
    'PXLs',
    '0x10b641D9fa73f1564e8d52641aE72DEC5B13d7Bc'.toLowerCase(),
    Chain.UNITS,
    18,
    require('styles/svg/logo_icon.svg'),
    false,
    0,
    false,
    true,
);

export const UNITS_PIXEL_DUST_TOKEN = new Token(
    'Pixel Dust',
    'PXLd',
    '0x7FfA8a0307fCa33fe57EAF7bd4d734e12F6989b7'.toLowerCase(),
    Chain.UNITS,
    18,
    require('styles/svg/logo_dust.svg'),
    false,
    0,
    false,
    true,
);
