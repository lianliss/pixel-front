export interface IChain {
  networkID: string;
  title: string;
  fiatDecimals: number;
  scan: string;
  defaultSymbol: string;
  hops: number;
  icon: string;
}
