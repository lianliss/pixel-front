import { SONGBIRD } from 'services/multichain/chains.js';
import { IMinersList } from 'services/multichain/miners/miners.types.ts';
import { buddaTheme } from '../../../shared/theme/theme.ts';

export const SECOND_MINERS = {
  [SONGBIRD]: [
    {
      title: 'Buddha Storage',
      symbol: 'JADEs',
      icon: require('assets/mining/jade.png'),
      theme: 'green',
      themeConfig: buddaTheme,
      contractAddress: '0xf4e4697Cc38f73B334daE3D41cAcAC489eFF3439',
      migrationAddress: '0xcc59224C9Fb08986398325c82f2BEF2130B0c0BD',
      tokenInfo: 'https://t.me/jadebuddha',
      projectInfo: 'https://jadebuddha.io',
      requirements: {
        speedLevel: 9,
      },
      abKey: 'budda_miner_enabled',
    },
  ],
};
