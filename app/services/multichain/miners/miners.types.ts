export type IMinerRequirements = {
  speedLevel?: number;
  sizeLevel?: number;
}

export type IMinerThemes = "green" | "yellow";

export type ISecondMiner = {
  title: string;
  symbol: string;
  icon?: any;
  contractAddress: string;
  requirements: IMinerRequirements;
  theme?: IMinerThemes;
  tokenInfo?: string;
  projectInfo?: string;
  abKey?: string;
}

export type IMinersList = {
  [index: number]: [ISecondMiner]
}