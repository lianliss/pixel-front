// Chain ids
export const TON = 0;
export const ETHEREUM_MAINNET = 1;
export const BSC_MAINNET = 56;
export const BSC_TESTNET = 97;
export const POLYGON_MAINNET = 137;
export const ARBITRUM_MAINNET = 42161;
export const POLYGON_MUMBAI = 80001;
export const SONGBIRD = 19;
export const FLARE = 14;
export const COSTON2 = 114;
export const SKALE_TEST = 1444673419;
export const SKALE = 2046399126;
export const SWISSTEST = 1291;
//export const FLARE = 14;
export const UNIT0 = 88811;

export const Chain = {
  TON,
  SKALE,
  SKALE_TEST,
  SWISSTEST,
  SONGBIRD,
  UNITS: 88811,
  UNIT0,
  COSTON2,
  FLARE,
  POLYGON_MAINNET,
}

// Chain is mainnet.
export const isMainnet = {
  [ETHEREUM_MAINNET]: true,
  [BSC_MAINNET]: true,
  [BSC_TESTNET]: true,
  [POLYGON_MAINNET]: true,
  [ARBITRUM_MAINNET]: true,
  [POLYGON_MUMBAI]: true,
  [SONGBIRD]: true,
  [FLARE]: true,
  [COSTON2]: true,
  [SKALE_TEST]: true,
  [SKALE]: true,
  [SWISSTEST]: true,
  [UNIT0]: true,
};

// Chain IDs that are integrated.
export const FINE_CHAIN_IDS = [
  ETHEREUM_MAINNET,
  BSC_MAINNET,
  BSC_TESTNET,
  POLYGON_MAINNET,
  ARBITRUM_MAINNET,
  POLYGON_MUMBAI,
  FLARE,
  COSTON2,
  SONGBIRD,
  SKALE_TEST,
  SKALE,
  SWISSTEST,
  UNIT0,
];

export const DEFAULT_CHAIN = SONGBIRD;

export const SWISS_CHAINS = [SWISSTEST];

export const NETWORKS_DATA = {
  [TON]: {
    networkID: 'TON',
    title: 'TON Blockchain',
    fiatDecimals: 18,
    scan: 'https://tonscan.org',
    defaultSymbol: 'TON',
    icon: require('assets/svg/ton_symbol.svg'),
    hops: 0,
  },
  [ETHEREUM_MAINNET]: {
    networkID: 'ETH',
    title: 'Ethereum',
    fiatDecimals: 6,
    scan: 'https://etherscan.io',
    defaultSymbol: 'ETH',
    hops: 0,
  },
  [BSC_MAINNET]: {
    networkID: 'BSC',
    title: 'BSC',
    fiatDecimals: 18,
    scan: 'https://bscscan.com',
    defaultSymbol: 'BNB',
    hops: 1,
  },
  [BSC_TESTNET]: {
    networkID: 'BSCTest',
    title: 'Testnet',
    fiatDecimals: 18,
    scan: 'https://testnet.bscscan.com',
    defaultSymbol: 'BNB',
    hops: 0,
  },
  [POLYGON_MAINNET]: {
    networkID: 'PLG',
    title: 'Polygon',
    fiatDecimals: 6,
    scan: 'https://polygonscan.com',
    defaultSymbol: 'POL',
    icon: require('assets/svg/polygon.svg'),
    hops: 0,
  },
  [POLYGON_MUMBAI]: {
    networkID: 'MUM',
    title: 'Testnet',
    fiatDecimals: 6,
    scan: 'https://mumbai.polygonscan.com',
    defaultSymbol: 'POL',
    hops: 0,
  },
  [ARBITRUM_MAINNET]: {
    networkID: 'ARB',
    title: 'Arbitrum',
    fiatDecimals: 6,
    scan: 'https://arbiscan.io',
    defaultSymbol: 'ETH',
    hops: 0,
  },
  [SONGBIRD]: {
    networkID: 'SGB',
    title: 'Songbird',
    fiatDecimals: 18,
    scan: 'https://songbird-explorer.flare.network',
    icon: require('assets/svg/songbird.svg'),
    defaultSymbol: 'SGB',
    hops: 0,
  },
  [SKALE_TEST]: {
    networkID: 'SKALE_TEST',
    title: 'SKALE Europa Testnet',
    fiatDecimals: 18,
    scan: 'https://juicy-low-small-testnet.explorer.testnet.skalenodes.com',
    icon: require('assets/svg/skale.svg'),
    defaultSymbol: 'sFUEL',
    hops: 0,
  },
  [SKALE]: {
    networkID: SKALE,
    title: 'SKALE Europa',
    fiatDecimals: 18,
    scan: 'https://elated-tan-skat.explorer.mainnet.skalenodes.com',
    icon: require('assets/svg/skale.svg'),
    defaultSymbol: 'sFUEL',
    hops: 0,
  },
  [SWISSTEST]: {
    networkID: 'SWISSTEST',
    title: 'Swisstronik Testnet',
    fiatDecimals: 18,
    scan: 'https://explorer-evm.testnet.swisstronik.com/',
    icon: require('assets/svg/swisstronik.svg'),
    defaultSymbol: 'SWTR',
    hops: 0,
  },
  [FLARE]: {
    networkID: 'FLR',
    title: 'Flare',
    fiatDecimals: 18,
    scan: 'https://flare-explorer.flare.network',
    icon: require('assets/svg/flare.svg'),
    defaultSymbol: 'FLR',
    hops: 0,
  },
  [COSTON2]: {
    networkID: 'COSTON2',
    title: 'Flare Coston 2 Testnet',
    fiatDecimals: 18,
    scan: 'https://coston2-explorer.flare.network',
    icon: require('assets/svg/coston2.svg'),
    defaultSymbol: 'C2FLR',
    hops: 0,
    isMarketUseGas: true,
  },
  [Chain.UNIT0]: {
    networkID: 'UNIT0',
    title: 'UNIT Zero',
    fiatDecimals: 18,
    scan: 'https://explorer.unit0.dev',
    icon: require('assets/img/token/unit/native.png'),
    defaultSymbol: 'UNIT0',
    hops: 0,
    isMarketUseGas: true,
  },
};

export const getTokenSymbol = (chainId) => {
  return NETWORKS_DATA[chainId]?.defaultSymbol ?? '';
};

export const MINING_CONFIG = {
  [SONGBIRD]: {
    baseSpeed: 0.01,
    baseSize: 6,
    difficulty: 10,
    refPercent1: 0.2,
    refPercent2: 0.05,
  },
  [SKALE_TEST]: {
    baseSpeed: 0.01,
    baseSize: 6,
    difficulty: 10,
    refPercent1: 0.05,
    // refPercent2: 0.05,
  },
  [SKALE]: {
    baseSpeed: 0.01,
    baseSize: 6,
    difficulty: 10,
    refPercent1: 0.2,
    refPercent2: 0.05,
  },
  [SWISSTEST]: {
    baseSpeed: 0.008,
    baseSize: 5,
    difficulty: 1,
    refPercent1: 0.05,
    refPercent2: 0,
  },
  [COSTON2]: {
    baseSpeed: 0.01,
    baseSize: 6,
    difficulty: 10,
    refPercent1: 0.2,
    refPercent2: 0.05,
  },
  [UNIT0]: {
    baseSpeed: 0.01,
    baseSize: 6,
    difficulty: 10 * 2.4,
    refPercent1: 0.2,
    refPercent2: 0.05,
  },
};
