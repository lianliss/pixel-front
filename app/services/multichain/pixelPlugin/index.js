import { ETH_DATA_FORMAT, Web3, Web3EthPluginBase } from "web3";
import { decryptNodeResponseWithPublicKey, encryptDataFieldWithPublicKey } from "@swisstronik/utils";
import {
  // Address,
  // BlockNumberOrTag,
  // Bytes,
  // DataFormat,
  DEFAULT_RETURN_FORMAT,
  // Numbers,
  // Transaction,
  // TransactionCall,
  // TransactionWithFromAndToLocalWalletIndex,
  // TransactionWithFromLocalWalletIndex,
  // TransactionWithToLocalWalletIndex
} from "web3-types";
import Eth, {
  call as vanillaCall,
  estimateGas as vanillaEstimateGas,
  // sendTransaction as vanillaSendTransaction
} from "web3-eth";
import {
  format,
  numberToHex,
} from "web3-utils";
import {SWISS_CHAINS, SWISSTEST} from "services/multichain/chains";
import {noderealRPC} from "services/multiwallets/multiwalletsDifference";

export class PixelPlugin extends Web3EthPluginBase {
  pluginNamespace = "pixel";
  static eth;
  static swissEth = new Eth(noderealRPC[SWISSTEST]);

  static getIsSwissTransaction = async transaction => {
    const chainId = Number(await PixelPlugin.eth.getChainId());
    return SWISS_CHAINS.includes(chainId) && transaction.data && transaction.to;
  }

  encryptTransaction = async (
    transaction,
  ) => {
    try {
      if (await PixelPlugin.getIsSwissTransaction(transaction)) {
        const nodePublicKey = await this.getNodePublicKey();
        const [encryptedData] = encryptDataFieldWithPublicKey(nodePublicKey, transaction.data);
        // console.log(transaction, encryptedData)
        return {
          ...transaction,
          data: encryptedData,
        }
      } else {
        return transaction;
      }
    } catch (error) {
      console.error('[encryptTransaction]', error);
    }
  }

  call = async (
    transaction,
    blockNumber = this.defaultBlock,
    returnFormat = DEFAULT_RETURN_FORMAT,
  ) => {
    if (await PixelPlugin.getIsSwissTransaction(transaction)) {
      const nodePublicKey = await this.getNodePublicKey();
      const [encryptedData, encryptionKey] = encryptDataFieldWithPublicKey(nodePublicKey, transaction.data);
      // console.log('call', transaction, blockNumber);
      transaction.data = encryptedData;
      const result = await vanillaCall(this, transaction, blockNumber, returnFormat);
      const decrypted = decryptNodeResponseWithPublicKey(nodePublicKey, result, encryptionKey);
      return format({ format: "bytes" }, decrypted, returnFormat);
    } else {
      return vanillaCall(this, transaction, blockNumber, returnFormat);
    }
  }

  estimateGas = async (
    transaction,
    blockNumber = this.defaultBlock,
    returnFormat = DEFAULT_RETURN_FORMAT,
  ) => {
    const encryptedTransaction = await this.encryptTransaction(transaction);
    return vanillaEstimateGas(this, encryptedTransaction, blockNumber, returnFormat);
  }

  link(parentContext) {
    super.link(parentContext);
    PixelPlugin.eth = parentContext;
  }

  getNodePublicKey = async () => {
    const blockNum = await PixelPlugin.swissEth.getBlockNumber();
    return await PixelPlugin.swissEth.requestManager.send({
      method: "eth_getNodePublicKey",
      params: [numberToHex(blockNum)]
    })
  }
}
