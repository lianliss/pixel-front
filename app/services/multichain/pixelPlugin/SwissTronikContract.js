import { PixelPlugin } from "./index.js";
import { decodeParameters } from 'web3-eth-abi';
import {
  // AbiInput,
  // BlockNumberOrTag,
  Contract,
  // ContractAbi,
  // PayableCallOptions,
} from "web3";

export class SwissTronikContract extends Contract {
  contractInstance;
  address;
  jsonInterface;
  plugin = PixelPlugin;

  constructor(jsonInterface, address, options) {
    super(jsonInterface, address, options);

    this.jsonInterface = jsonInterface;
    this.address = address;

    this.contractInstance = new Contract(jsonInterface, address, options);
    this.plugin = PixelPlugin.eth.pixel;
  }

  get methods() {
    return new Proxy(
      {},
      {
        get: (_, methodName) => {
          return (...args) => {
            const method = this.contractInstance.methods[methodName](
              ...args
            );
            const data = method.encodeABI();

            return {
              call: async (tx, block) => {
                const responseMessage = await this.plugin.call(
                  {
                    to: this.address,
                    data,
                    ...tx,
                  },
                  block,
                );

                const input = (
                  this.jsonInterface.find(x => x.name === methodName)
                )?.outputs;

                const decodedCall =
                  decodeParameters(
                    input,
                    responseMessage
                  );

                return decodedCall.__length__ === 1
                  ? decodedCall[0]
                  : decodedCall;
              },
              estimateGas: async (tx, block) => {
                return await this.plugin.estimateGas(
                  {
                    to: this.address,
                    data,
                    ...tx,
                  },
                  block,
                );
              },
              encodeABI: () => {
                return data;
              },
              createAccessList: (options, block) => {
                return method.createAccessList(options, block);
              }
            }
          };
        },
      }
    );
  }
}
