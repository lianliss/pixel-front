import React from 'react';
import {getIsTonActiveStorage, setIsTonActiveStorage} from "services/ton/utils.ts";
import {TonConnectUIProvider} from "@tonconnect/ui-react";
import { THEME } from '@tonconnect/ui';
import {IS_TON_ENABLED, TON_CONNECT_MANIFEST_URL, TON_CONNECT_RETURN_TMA_URL} from "@cfg/config.ts";
export const TONContext = React.createContext();

export function TONProvider(props: React.PropsWithChildren) {

  const [isTonChain, setIsTonChain] = React.useState(getIsTonActiveStorage());

  const setTonChainEnabled = (isActive: boolean) => {
    setIsTonActiveStorage(isActive);
    setIsTonChain(isActive);
  }

  return <TonConnectUIProvider manifestUrl={TON_CONNECT_MANIFEST_URL}
                               twaReturnUrl={TON_CONNECT_RETURN_TMA_URL}
                               uiPreferences={{
                                 theme: THEME.DARK,
                               }}>
    <TONContext.Provider value={{
      isTonChain: isTonChain && IS_TON_ENABLED,
      setTonChainEnabled,
    }}>
      {props.children}
    </TONContext.Provider>
  </TonConnectUIProvider>
}