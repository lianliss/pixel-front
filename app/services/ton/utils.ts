const TON_ACTIVE_KEY = 'ton-active';
export const getIsTonActiveStorage = (): boolean => {
  return localStorage.getItem(TON_ACTIVE_KEY) !== 'no';
}

export const setIsTonActiveStorage = (isActive: boolean) => {
  if (isActive) {
    localStorage.removeItem(TON_ACTIVE_KEY);
  } else {
    localStorage.setItem(TON_ACTIVE_KEY, 'no');
  }
}