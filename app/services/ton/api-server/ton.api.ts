'use strict';

import axios, { AxiosResponse } from 'axios';

export class TonApi {
  url: string;

  constructor(opts: { url: string }) {
    this.url = opts.url;
  }

  async getBalance(address) {
    const { data } = await axios.get<
      { data: string },
      AxiosResponse<{ data: string }>
    >(`${this.url}/getAddressBalance`, {
      baseURL: this.url,
      params: { address },
    });
    return data;
  }
}

export const tonApi = new TonApi({ url: 'https://ton-mainnet.core.chainstack.com/f2a2411bce1e54a2658f2710cd7969c3/api/v2' });
