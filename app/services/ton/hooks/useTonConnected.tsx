import React from 'react';
import {useTonWallet} from "@tonconnect/ui-react";

export function useTonConnected() {
  const wallet = useTonWallet();

  return !!wallet;
}

export default useTonConnected;
