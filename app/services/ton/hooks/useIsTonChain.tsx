import React from 'react';
import useTon from "services/ton/hooks/useTon.tsx";

export interface IUseIsTonChain {
  isTonChain: boolean,
  tonEnable: () => void,
  tonDisable: () => void,
}

export function useIsTonChain(): IUseIsTonChain {
  const ton = useTon();

  const tonEnable = () => ton.setTonChainEnabled(true);
  const tonDisable = () => ton.setTonChainEnabled(false);

  return {
    isTonChain: ton.isTonChain,
    tonEnable,
    tonDisable,
  }
}

export default useIsTonChain;

