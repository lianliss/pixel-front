import React from 'react';

export function useTonPrice() {
  return {
    data: 3.56,
    isLoading: false,
    isFetched: true,
    refetch: async () => {},
  };
}

export default useTonPrice;
