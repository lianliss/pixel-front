import React from 'react';
import {useTonConnectUI} from "@tonconnect/ui-react";

export function useTonDisconnect() {

  const [tonConnectUI, setOptions] = useTonConnectUI();

  return () => {
    tonConnectUI.disconnect().then().catch(error => {
      console.error('[useTonDisconnect][onDisconnect]', error);
    })
  }
}

export default useTonDisconnect;
