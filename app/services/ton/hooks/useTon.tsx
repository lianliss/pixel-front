import React from 'react';
import {TONContext} from "services/ton/TON.provider.tsx";

export function useTon() {
  return React.useContext(TONContext)
}

export default useTon;
