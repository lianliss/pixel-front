import React from 'react';
import {useTonAddress} from "@tonconnect/ui-react";
import {tonApi} from "services/ton/api-server/ton.api.ts";
import wei from "utils/wei.jsx";

export function useTonBalance() {

  const accountAddress = useTonAddress();

  const [balance, setBalance] = React.useState(0);
  const [isLoading, setIsLoading] = React.useState(false);
  const [isFetched, setIsFetched] = React.useState(false);
  const [isError, setIsError] = React.useState(false);
  const [error, setError] = React.useState();

  const onGetBalance = async () => {
    try {
      setIsLoading(true);
      setIsError(false);
      setIsFetched(false);
      const result = await tonApi.getBalance(accountAddress);
      setBalance(wei.from(result.result, 9));
      setIsFetched(true);
    } catch (error) {
      console.error('[onGetBalance]', error);
      setError(error);
      setIsError(true);
    }
    setIsLoading(false);
  }

  React.useEffect(() => {
    if (accountAddress) {
      onGetBalance().then();
    }
  }, [accountAddress])

  return {
    data: balance,
    isLoading,
    isFetched,
    isError,
    error,
    refetch: onGetBalance,
  };
}

export default useTonBalance;
