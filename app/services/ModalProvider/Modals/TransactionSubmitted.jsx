import React from "react";
import {ButtonDeprecated} from 'ui';

function TransactionSubmitted({txLink, symbol, addToken}) {
  return <>
    <p>
      Transaction Submitted
    </p>
    <p>
      <a href={txLink}>See transaction</a>
    </p>
    <ButtonDeprecated onClick={addToken}>
      Add {symbol} to wallet
    </ButtonDeprecated>
  </>
}

export default TransactionSubmitted;
