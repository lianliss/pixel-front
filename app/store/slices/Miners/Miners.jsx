import {createSlice} from "@reduxjs/toolkit";
import INITIAL_MINERS_STATE, {INITIAL_MINER_STATE} from "const/schemas/miners";
import {default as lodashFill} from "lodash-es/fill";

const setMinerValue = (state, payload, key) => {
  state.second[payload.index] = {
    ...INITIAL_MINER_STATE,
    ...state.second[payload.index],
    [key]: payload.value,
  }
}

export const MinersSlice = createSlice({
  name: 'Miners',
  initialState: {
    ...INITIAL_MINERS_STATE
  },
  reducers: {
    fill: (state, {payload}) => {
      state.second = lodashFill([], INITIAL_MINER_STATE, 0, payload);
    },
    setMinerValue: (state, {payload}) => setMinerValue(state, payload, payload.key || 'value'),
    setClaimed: (state, {payload}) => setMinerValue(state, payload, 'claimed'),
    setMined: (state, {payload}) => setMinerValue(state, payload, 'mined'),
    setRewardPerSecond: (state, {payload}) => setMinerValue(state, payload, 'rewardPerSecond'),
    setSizeLimit: (state, {payload}) => setMinerValue(state, payload, 'sizeLimit'),
    setClaimTimestamp: (state, {payload}) => setMinerValue(state, payload, 'claimTimestamp'),
    setLastGetTimestamp: (state, {payload}) => setMinerValue(state, payload, 'lastGetTimestamp'),
    setIsMiningLoading: (state, {payload}) => setMinerValue(state, payload, 'isMiningLoading'),
    setIsClaiming: (state, {payload}) => setMinerValue(state, payload, 'isClaiming'),
  },
});

export const {
  fill,
  setClaimed,
  setMined,
  setRewardPerSecond,
  setSizeLimit,
  setClaimTimestamp,
  setLastGetTimestamp,
  setIsMiningLoading,
  setIsClaiming,
} = MinersSlice.actions;

export default MinersSlice.reducer;
