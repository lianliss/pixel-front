import minersReducer from './Miners';

// Export actions -- START;
export * from './Miners';
// Export actions -- END;

export default minersReducer;
