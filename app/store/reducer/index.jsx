import {combineReducers} from "redux";

import App from "slices/App";
import Dapp from "slices/Dapp";
import Miners from "slices/Miners";

export const reducer = combineReducers({
	App,
	Dapp,
  Miners,
});

export default reducer;
