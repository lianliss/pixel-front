import { IS_DEVELOP } from '@cfg/config.ts';

const uriTransform = (uri) => {
  return IS_DEVELOP
    ? uri
    : uri.replace('gateway.dedrive.io/v1/access', 'hellopixel.network/api/dedrive');
};

export default uriTransform;
