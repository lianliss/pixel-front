const pushPixel = (obj, sub) => {
  if (typeof window.Pixel === 'undefined') {
    window.Pixel = {};
  }
  if (sub) {
    if (typeof window.Pixel[sub] === 'undefined') {
      window.Pixel[sub] = {};
    }
    Object.assign(window.Pixel[sub], obj);
  } else {
    Object.assign(window.Pixel, obj);
  }
}

export default pushPixel;
