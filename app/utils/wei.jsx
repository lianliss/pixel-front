// const {fromWei, toWei, toBigInt} = require('web3-utils');
import { fromWei, toWei, toBigInt } from 'web3-utils';

const DEFAULT_DECIMALS = 18;

const wei = {
  from: (bigNumber, decimals = DEFAULT_DECIMALS) => {
    const value = Number(fromWei(bigNumber, "ether"));
    return value * 10 ** (DEFAULT_DECIMALS - decimals);
  },
  to: (value, decimals = DEFAULT_DECIMALS) => {
    if (value === 0 || value === '0') return '0';
    return toWei(
      Number(value / 10 ** (DEFAULT_DECIMALS - decimals)).toFixed(
        DEFAULT_DECIMALS
      ),
      "ether"
    );
  },
  bn: toBigInt,
};

export default wei;
