import {wei} from "app/utils/index";
import get from "lodash/get";

const processError = (error) => {
  if (typeof error !== 'object') return 'Undefined error';
  const message = get(error, 'data.message', get(error, 'message'));
  try {
    if (typeof message !== 'string') {
      return {
        message: 'Unknown error',
      }
    }
    if (message.indexOf('Internal JSON-RPC error.') >= 0) {
      const internal = JSON.parse(message.split('Internal JSON-RPC error.')[1]);
      return {
        message: internal.message,
        isRpc: true,
      };
    } else {
      if (message.indexOf('insufficient funds') >= 0 || message.indexOf(' gas ') >= 0) {
        const want = message.split('want ')[1].split('(')[1].split(')')[0];
        return {
          message,
          isGas: true,
          gas: wei.from(want),
        };
      } else {
        return {
          message,
        };
      }
    }
  } catch (processError) {
    console.error('[processError]', processError, error);
    return {
      message,
    };
  }
};

export default processError;
