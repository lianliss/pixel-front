import moment from 'moment';

const TIME_IN_SECOND = 1000;
const TIME_IN_MINUTE = TIME_IN_SECOND * 60;
const TIME_IN_HOUR = TIME_IN_MINUTE * 60;
const TIME_IN_DAY = TIME_IN_HOUR * 24;
const TIME_IN_MONTH = TIME_IN_DAY * 31;

export function timeToDate(date) {
    const diff = new Date().getTime() - date.getTime();

    if (diff < TIME_IN_MINUTE) {
        const seconds = Math.floor(diff / TIME_IN_SECOND);
        return `${seconds} sec ago`;
    } else if (diff < TIME_IN_HOUR) {
        const minutes = Math.floor(diff / TIME_IN_MINUTE);
        return `${minutes} min ago`;
    } else if (diff < TIME_IN_DAY) {
        const hours = Math.floor(diff / TIME_IN_HOUR);
        return `${hours} hour ago`;
    } else if (diff < TIME_IN_MONTH) {
        const days = Math.floor(diff / TIME_IN_DAY);
        return `${days} day ago`;
    } else {
        const months = Math.floor(diff / TIME_IN_MONTH);
        return `${months} month ago`;
    }
}

export function toUTC(date) {
    const inverseOffset = moment().utcOffset();
    const target = moment(date).subtract(inverseOffset / 60, 'hour');

    return target.toDate();
}

export function fromUtc(date) {
    const inverseOffset = moment().utcOffset();
    const target = moment(date).add(inverseOffset / 60, 'hour');

    return target.toDate();
}

export function diffTime(date, short = false) {
    const now = moment();
    const target = moment(fromUtc(date));

    if (target < now) {
        return `${!short ? '0d ' : ''}0h 0m 0s`;
    }

    const days = (target.diff(now, 'days'));
    now.add(days, 'days');
    const hours = ((target).diff(now, 'hours'));
    now.add(hours, 'hours');
    const minutes = (target.diff(now, 'minutes'));
    now.add(minutes, 'minutes');
    const seconds = (target.diff(now, 'seconds'));

    return [...(!short ? [`${days}d`] : []), `${hours}h`, `${minutes}m`, `${seconds}s`].join(' ');
}

export function getTimeProgress(start, end) {
    const now = moment(start);
    const target = moment(end);

    const days = (target.diff(now, 'days'));
    now.add(days, 'days');
    const hours = ((target).diff(now, 'hours'));
    now.add(hours, 'hours');
    const minutes = (target.diff(now, 'minutes'));
    now.add(minutes, 'minutes');
    const seconds = (target.diff(now, 'seconds'));

    return [`${hours}h`, `${minutes}m`, `${seconds}s`].join(' ');
}

export function getLocaleTime(time) {
    const days = Math.floor(time / TIME_IN_DAY);
    const hours = Math.floor((time - days * TIME_IN_DAY) / TIME_IN_HOUR);

    if (!days && !hours) {
        return 'less then 1 hour';
    }

    return [days && `${days} days`, hours && `${hours} hours`].filter(Boolean).join(' ');
}

export function getDateDays(time) {
    const value = +time

    return +(value / TIME_IN_DAY).toFixed(1);
}
