export function formatUUID(id: string): string {
  return (id.slice(0, 4) + '..' + id.slice(-3)).toLowerCase();
}
