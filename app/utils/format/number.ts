export function getPercentage(value: number, total: number): number {
  return total === 0 ? 0 : (value / total) * 100;
}

export function getPercentageText(value: number, total: number): string {
  if (!value) {
    return '0%';
  }

  const percent = +getPercentage(value, total).toFixed(2);

  return percent ? `${percent}%` : `<0.00%`;
}
