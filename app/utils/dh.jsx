import { DH, PrimeUtils } from 'will-dh';
import { dhPrivateKey, dhPublicKey } from 'shared/const/localStorage.ts';

const DH_PRIME_KEY = '2611891123';
const DH_GENERATOR = '1723';

export async function getDH() {
  try {
    let privateKey = window.localStorage.getItem(dhPrivateKey);
    let publicKey = window.localStorage.getItem(dhPublicKey);

    const dh = new DH();
    dh.prime = DH_PRIME_KEY;
    dh.generator = DH_GENERATOR;
    if (!privateKey || !publicKey) {
      // Generate new keys
      privateKey = await PrimeUtils.findPrime(30);
      dh.privateKey = privateKey;
      dh.computePublicKey();
      window.localStorage.setItem(dhPrivateKey, privateKey);
      window.localStorage.setItem(dhPublicKey, dh.publicKey);
    } else {
      // Use old keys
      dh.privateKey = privateKey;
      dh.publicKey = publicKey;
    }
    return dh;
  } catch (error) {
    console.error('[getDH]', error);
  }
}

export async function getEncryptor(otherPublic) {
  try {
    const dh = await getDH();
    dh.otherPublicKey = otherPublic;
    dh.computeSharedKey();
    return {
      decrypt: (encrypted) => dh.decrypt(encrypted),
      encrypt: (data) => dh.encrypt(data),
      publicKey: dh.publicKey,
    };
  } catch (error) {
    console.error('[getEncryptor]', error);
  }
}
