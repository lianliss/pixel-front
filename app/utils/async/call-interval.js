export function callInterval(handler, intervals = [5000, 10000]) {
    for(const interval of intervals) {
        setTimeout(handler, interval);
    }
}
