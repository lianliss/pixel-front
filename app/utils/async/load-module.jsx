import React from 'react';
import image from 'styles/svg/loading.svg';
// import { OverlayToaster } from '@blueprintjs/core';
import clsx from 'clsx';

// const toaster = OverlayToaster.createAsync({ position: 'top', usePortal: true });

export function Loading(props = {}) {
  const { text, fullScreen } = props;

  return (
    <div className={clsx('module-loading', { 'module-loading_fullScreen': fullScreen })}>
      <img className='loading' src={image} />
      <div className='module-loading-text'>{text || 'Loading'}</div>
    </div>
  );
}

// class LoadModule extends React.PureComponent {
//   /**
//    * Lazy loader for any lib module which path starts from 'lib/...'
//    * @param lib {String} - module path after 'lib/'
//    * @param root {String} - module path after 'root/'
//    * @param modal {String} - module path after 'services/ModalProvider/Modals/'
//    * @returns {XML}
//    */
//
//   static processError(error, name) {
//     console.error(`[LoadModule] ${name}`, error);
//     typeof toaster.show === 'function' &&
//       toaster.show({
//         intent: 'danger',
//         message: `Module load error. There is a new version available. Clearing cache...`,
//         icon: 'error',
//         timeout: 10000,
//       });
//     try {
//       // caches.keys().then((names) => {
//       //   names.forEach((name) => {
//       //     caches.delete(name);
//       //   });
//       //   //setTimeout(() => window.location.reload(true), 2000);
//       // });
//     } catch (error) {
//       console.error('[LoadModule] caches', error);
//       typeof toaster.show === 'function' &&
//         toaster.show({
//           intent: 'danger',
//           message: `Can't clear cache automatically. Please clear the cache of your browser`,
//           icon: 'error',
//           timeout: 10000,
//         });
//     }
//   }
//
//   render() {
//     const { lib, root, modal } = this.props;
//     let LazyComponent;
//
//     switch (true) {
//       case !!root:
//         LazyComponent = lazy(() =>
//           /* @vite-ignore */
//           import(`../../root/${root}`).catch((err) => LoadModule.processError(err, root))
//         );
//         break;
//       case !!modal:
//         LazyComponent = lazy(() =>
//           /* @vite-ignore */
//           import(`../../services/ModalProvider/Modals/${modal}`).catch((err) =>
//             LoadModule.processError(err, modal)
//           )
//         );
//         break;
//       case !!lib:
//       default:
//         LazyComponent = lazy(() =>
//             /* @vite-ignore */
//           import(`../../lib/${lib}`).catch((err) => LoadModule.processError(err, lib))
//         );
//     }
//
//     return (
//       <Suspense fallback={Loading()}>
//         <LazyComponent {...this.props} />
//       </Suspense>
//     );
//   }
// }
//
// export default LoadModule;
