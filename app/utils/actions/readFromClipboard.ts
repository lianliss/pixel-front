export async function readFromClipboard() {
  const text = await navigator.clipboard.readText();

  return text;
}
