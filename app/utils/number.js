export function formatNumber(value) {
    if (value >= 1_000_000_000) {
        return (value / 1_000_000_000).toFixed(2) + ' B';
    }
    if (value >= 1_000_000) {
        return (value / 1_000_000).toFixed(2) + ' M';
    }
    if (value >= 1_000) {
        return (value / 1_000).toFixed(2) + ' K';
    }

    return value;
}


export function formatNumberSpacing(value) {
    const [start, decimals] = value.toString().split('.');
    let str = start.split('').reverse();
    let result = [];

    for(let i = 0; i < str.length; i += 3) {
        result.push(str.slice(i, i + 3).reverse().join(''));
    }

    return result.reverse().join(' ') + (decimals ? `.${decimals}` : '');
}

