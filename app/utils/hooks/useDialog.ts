import React from 'react';

export function useDialog() {
  const [open, setOpen] = React.useState(false);

  return {
    open,
    onClose: () => setOpen(false),
    hide: () => setOpen(false),
    show: () => setOpen(true),
  };
}
