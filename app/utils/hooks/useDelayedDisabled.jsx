import React from 'react';

export function useDelayedDisabled(delay = 5000) {
    const [disabled, setDisabled] = React.useState(false);
    const timeoutRef = React.useRef();

    const call = () => {
        setDisabled(true);
        timeoutRef.current = setTimeout(() => setDisabled(false), delay);
    };

    React.useEffect(() => {
        return () => {
            if (timeoutRef.current) {
                clearTimeout(timeoutRef.current);
            }
        }
    }, []);

    return {
        disabled,
        call,
    }
}
