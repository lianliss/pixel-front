import React from 'react';

export type IPaginationResult = {
  pages: number;
  enabled: boolean;
  page: number;
  setPage: (v: number) => void;
  limit: number;
  offset: number;
  step: number;
  setTotal: (v: number) => void;
  total: number;
};

export function usePagination({
  total: externalTotal = 0,
  step = 24,
}: {
  total?: number;
  step?: number;
}): IPaginationResult {
  const [page, setPage] = React.useState<number>(1);
  const [total, setTotal] = React.useState<number>(0);

  const currentTotal = externalTotal || total;
  const pagesCount = Math.ceil(currentTotal / step);

  return {
    total: currentTotal,
    pages: pagesCount,
    enabled: currentTotal > step,
    page,
    setPage: (v: number) => setPage(v),
    limit: step,
    offset: (page - 1) * step,
    step,
    setTotal: (v: number) => setTotal(v),
  };
}
