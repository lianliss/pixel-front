import React from 'react';

const defaultDeps = [];

export function useInterval(handler, ms = 1000, deps = defaultDeps) {
    React.useEffect(() => {
        const intervalId = setInterval(handler, ms);

        return () => {
            clearInterval(intervalId);
        }
    }, deps);
}
