import React from 'react';

export function useUpdateInterval(ms = 1000) {
    const [date, setDate] = React.useState(new Date());

    React.useEffect(() => {
        const intervalId = setInterval(() => {
            setDate(new Date());
        }, ms);

        return () => {
            clearInterval(intervalId);
        }
    }, []);

    return date;
}
