import axios from 'axios';
import { pixelMessageKey, pixelSignKey, telegramAuthKey } from '../../shared/const/localStorage.ts';
import { getAccount } from '@wagmi/core';
import { wagmiConfig } from '@chain/wagmi.ts';
import { IDENTITY_TOKEN, IS_STAGE } from '@cfg/config.ts';
import hmacSha256 from 'crypto-js/hmac-sha256';
import encoderHex from 'crypto-js/enc-hex';

export const axiosInstance = axios.create();

axiosInstance.interceptors.request.use(
  async function (req) {
    const telegramInfo = Telegram.WebApp.initData || localStorage.getItem(telegramAuthKey);
    const account = getAccount(wagmiConfig);

    if (account.address) {
      let hash = hmacSha256(account.address, 'pixel-signature').toString(encoderHex);
      const key = `pixel-sign-${hash}`;
      let sign = window.localStorage.getItem(key);

      const headers = {
        'pixel-message': hash || localStorage.getItem(pixelMessageKey),
        'pixel-sign': sign || localStorage.getItem(pixelSignKey),
        identity: IDENTITY_TOKEN,
        isprod: IS_STAGE ? 'false' : 'true',
      };

      Object.assign(req.headers, headers);
    }

    if (telegramInfo) {
      req.headers[telegramAuthKey] = telegramInfo;
    }

    return req;
  },
  (err) => {
    return Promise.reject(err);
  }
);
