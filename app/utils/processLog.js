import LogsDecoder from "logs-decoder";
import {decodeParameter} from "web3-eth-abi";

function processLog(logs, ABIs) {
  try {
    console.log('processLog', logs, ABIs);
    const logsDecoder = LogsDecoder.create();
    ABIs.map(abi => {
      logsDecoder.addABI(abi);
    });
    const decoded = logsDecoder.decodeLogs(logs);
    console.log('decoded', decoded);
    return decoded.map(log => {
      const events = {};
      log.events.map(event => {
        const {type, value, name} = event;
        if (type !== 'address') {
          try {
            event.value = decodeParameter(type, value);
          } catch (error) {
          
          }
        }
        events[name] = event.value;
      })
      return events;
    });
  } catch (error) {
    console.error('[processLog]', error);
  }
}

export default processLog;

