import { IS_TELEGRAM } from '@cfg/config.ts';

type IStrategy = {
  getItem(key: string): Promise<string | null>;
  setItem(key: string, value: string | null): Promise<void>;
  removeItem(key: string): Promise<void>;
};

class LocalStrategy implements IStrategy {
  public async getItem(key: string) {
    return localStorage.getItem(key);
  }

  public async setItem(key: string, value: string) {
    return localStorage.setItem(key, value);
  }

  public async removeItem(key: string) {
    return localStorage.removeItem(key);
  }
}

class TelegramStrategy implements IStrategy {
  public async getItem(key: string) {
    return new Promise((resolve, reject) => {
      Telegram.WebApp.CloudStorage.getItem(key, (err, data) => {
        if (err) {
          return reject(err);
        }

        resolve(data);
      });
    });
  }

  public async setItem(key: string, value: string) {
    return new Promise((resolve, reject) => {
      Telegram.WebApp.CloudStorage.setItem(key, value, (err, data) => {
        if (err) {
          return reject(err);
        }

        resolve(data);
      });
    });
  }

  public async removeItem(key: string) {
    return new Promise((resolve, reject) => {
      Telegram.WebApp.CloudStorage.removeItem(key, (err, data) => {
        if (err) {
          return reject(err);
        }

        resolve(data);
      });
    });
  }
}

export class CloudStorage implements IStrategy {
  constructor(protected readonly strategy: IStrategy) {}

  public async getItem(key: string) {
    return this.strategy.getItem(key);
  }

  public async setItem(key: string, value: string) {
    return this.strategy.setItem(key, value);
  }

  public async removeItem(key: string) {
    return this.strategy.removeItem(key);
  }
}

export const cloudStorage = new CloudStorage(
  IS_TELEGRAM ? new TelegramStrategy() : new LocalStrategy()
);
export const storage = new CloudStorage(new LocalStrategy());
