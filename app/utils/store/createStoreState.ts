import {createStore, Effect, StoreWritable} from 'effector';
import { AxiosError } from 'axios';

export type IStoreState<Data> = {
  loading: boolean;
  error: Error | null;
  data: Data | undefined;
  isFetched: boolean;
}

export function createStoreState<Input, Result>(
  effect: Effect<Input, Result>,
  { loading = true }: { loading?: boolean } = {}
) {
  const store = createStore<IStoreState<Result>>({
    loading: loading,
    error: null as Error | null,
    data: undefined as Result | undefined,
    isFetched: false,
  })
    .on(effect.doneData, (_prev, next) => ({
      loading: false,
      error: null,
      data: next,
      isFetched: true,
    }))
    .on(effect.fail, (_prev, next) => ({
      loading: false,
      error: (next.error as AxiosError)?.response?.data?.error || next.error,
      data: undefined,
      isFetched: false,
    }))
    .on(effect.pending, (prev, loading) => ({
      loading: loading,
      error: prev.error,
      data: prev.data,
      isFetched: prev.isFetched,
    }));

  return store;
}

export function addStoreEffect<Input, Result>(
    store: StoreWritable<IStoreState<Result>>,
    effect: Effect<Input, Result>,
) {
  store
      .on(effect.doneData, (_prev, next) => ({
        loading: false,
        error: null,
        data: next,
        isFetched: true,
      }))
      .on(effect.fail, (_prev, next) => ({
        loading: false,
        error: (next.error as AxiosError)?.response?.data?.error || next.error,
        data: undefined,
        isFetched: false,
      }))
      .on(effect.pending, (prev, loading) => ({
        loading: loading,
        error: prev.error,
        data: prev.data,
        isFetched: prev.isFetched,
      }));

  return store;
}
