import {decodeParameter} from "web3-eth-abi";

const ERROR_SIGNATURE = '08c379a0';
const DELEGATE_CALL_FAILED_SIGNATURE = 'f7a01e4d';

const splitCallData = bytesString => {
  const signature = bytesString.slice(2, 10);
  const bytes = bytesString.slice(10);
  console.log('splitCallData', signature, bytes);
  return {
    signature,
    bytes: `0x${bytes}`,
  }
}

const decodeErrorData = data => {
  const params = splitCallData(data);
  switch (params.signature) {
    case ERROR_SIGNATURE:
      return decodeParameter('string', params.bytes);
    case DELEGATE_CALL_FAILED_SIGNATURE:
      return decodeErrorData(decodeParameter('bytes', params.bytes));
    default:
      console.error('[decodeErrorData] Unknown error signature', params.signature, params);
      return 'Unknown error signature';
  }
}

export const decodeContractErrorMessage = message => {
  if (message.includes("unknown custom error")) {
    try {
      const split = message.split(' data="')[1].split('"')[0];
      return decodeErrorData(split);
    } catch (e) {
      console.error('[decodeContractErrorMessage]', message);
      return message || "Unknown custom error";
    }
  } else {
    return message;
  }
}
