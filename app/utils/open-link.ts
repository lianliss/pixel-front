import { IS_TELEGRAM } from '@cfg/config.ts';
import { IS_MOBILE } from '@cfg/app.ts';

export function openLink(url: string) {
  if (IS_TELEGRAM && url.startsWith('https://t.me') && IS_MOBILE) {
    Telegram.WebApp.openTelegramLink(url);
  } else {
    window.open(url, '_blank');
  }
}
