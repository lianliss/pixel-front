export function deduplicate(arr, getKey = item => item.id) {
    const map = {};

    for(const item of arr) {
        const key = getKey(item);

        if (key in map) {
            continue;
        }

        map[key] = item;
    }

    return Object.values(map);
}
