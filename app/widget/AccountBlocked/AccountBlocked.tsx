import {Typography} from "@ui-kit/Typography/Typography.tsx";
import Box from "@mui/material/Box";
import React from "react";
import Container from "@mui/material/Container";
import {Button} from "@ui-kit/Button/Button.tsx";

function AccountBlocked() {
    return (
        <Box sx={{ width: '100%', height: '100vh' }}>
           <Container sx={{ height: '100vh', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
               <Typography variant='h2' fontWeight='bold' align='center'>Account Blocked</Typography>
               <Typography align='center' sx={{ mt: 2 }}>
                   Ваш аккаунт был ограничен в связи с несанкционированным присвоением средств проекта через выявленную программную уязвимость. Данное действие квалифицируется как кража и в соответствии со статьей 158 Уголовного кодекса Российской Федерации наказывается лишением свободы на срок до пяти лет.
               </Typography>

               <Button component='a' href='https://t.me/zukasti' variant='contained' color='primary' sx={{ mt: 3 }}>Связаться с администрацией</Button>
           </Container>
        </Box>
    )
}

export default AccountBlocked;
