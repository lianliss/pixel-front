import { Chain } from '../../../services/multichain/chains';
import clsx from 'clsx';
import CheckIcon from '@mui/icons-material/Check';
import IconPixelMining from '../../../shared/ui/icon/pixel/IconPixelMining.tsx';

import styles from '../Chain.module.scss';
import {alpha, styled} from "@mui/material/styles";
import Box from "@mui/material/Box";

const StyledRoot = styled(Box)(({ ownerState, theme }) => ({
  variants: [
    {
      props: {
        disabled: true,
      },
      style: {
        opacity: 0.5,
      },
    },
    {
      props: {
        active: true,
      },
      style: {
        backgroundColor: alpha(theme.palette.primary.main, 0.2),
      },
    },
  ],
}));

type Props = {
  option;
  isActive;
  isDisabled;
  onChainClick;
  isMiningIconVisible?: boolean;
  isClickAvailable?: boolean;
  isSoon?: boolean;
};

const ChainItem = ({
  option,
  isActive,
  isDisabled,
  onChainClick,
  isMiningIconVisible = false,
  isClickAvailable = false,
                     isSoon = false,
}: Props) => {
  const chain = option.data;
  const isMiningNetwork =
    isMiningIconVisible &&
    [Chain.SWISSTEST, Chain.SKALE, Chain.SONGBIRD, Chain.UNITS].includes(Number(option.chainId));

  const ownerState = {
    disabled: isDisabled,
    active: isActive,
  }

  return (
    <StyledRoot
      onClick={() => !isActive && (!isDisabled || isClickAvailable) && !isSoon && onChainClick(option.chainId)}
      ownerState={ownerState}
      className={clsx(styles.chainItem, {
        [styles.disabled]: isDisabled,
        [styles.active]: isActive,
      })}
    >
      <div className={styles.chainItemImage}>{chain.icon && <img src={chain.icon} alt='' />}</div>
      <div className={styles.chainItemTitle}>{chain.title}{isSoon ? ` (Soon)` : ''}</div>

      {isActive && (
        <div className={styles.chainItemActive}>
          <CheckIcon sx={{ color: 'success.main' }} />
        </div>
      )}

      {isMiningIconVisible && isMiningNetwork && (
        <div className={styles.chainMiningIcon}>
          <IconPixelMining />
        </div>
      )}
    </StyledRoot>
  );
};

export default ChainItem;
