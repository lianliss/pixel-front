import React, { useEffect, useMemo, useState } from 'react';
import styles from './Chain.module.scss';

import WalletPopup from '../../lib/ui/WalletPopup/WalletPopup';

import { Chain, NETWORKS_DATA } from '../../services/multichain/chains';
import toaster from '../../services/toaster';
import clsx from 'clsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import {IS_DEVELOP, IS_STAGE, IS_TON_ENABLED} from '../../config/config.ts';
import { useSwitchChain } from '@chain/hooks/useSwitchChain.ts';
import ChainItem from './ChainItem/ChainItem.tsx';
import { Button } from '../../shared/ui/Button/Button.tsx';
import { useTranslation } from 'react-i18next';
import BottomDialog from '../../shared/ui/BottomDialog/BottomDialog.tsx';
import useIsTonChain from "services/ton/hooks/useIsTonChain.tsx";

const CHAINS_LIST = [
  Chain.SONGBIRD,
  Chain.SWISSTEST,
  Chain.SKALE,
  Chain.UNITS,
  Chain.FLARE,
  Chain.POLYGON_MAINNET,
  Chain.COSTON2,
  Chain.SKALE_TEST,
];
const disabledChains = [
  Chain.SWISSTEST,
  // Chain.SKALE,
  Chain.COSTON2,
  Chain.SKALE_TEST,
  Chain.UNITS,
];

const TON_CHAIN = {
  chainId: Chain.TON,
  data: NETWORKS_DATA[Chain.TON],
}

const getIsSoon = (chainId: number): boolean => {
  if (chainId === Chain.UNITS) {
    return !(IS_STAGE || IS_DEVELOP);
  }

  return false;
}

function ChainSwitcher({ isRelative, chains: availableChains, children, disabled = false, style = {} }) {
  const account = useAccount();
  const {switchChain} = useSwitchChain();
  const { chainId } = account;
  const { t } = useTranslation('notifications');
  const { t: c } = useTranslation('common');

  const [choose, setChoose] = useState(false);
  const {
    isTonChain,
    tonEnable,
    tonDisable,
  } = useIsTonChain();

  const currentChain = useMemo(() => isTonChain
    ? TON_CHAIN.data
    : NETWORKS_DATA[chainId], [chainId, isTonChain]);

  const chains = useMemo(
    () =>
      CHAINS_LIST.filter((el) =>
        IS_DEVELOP || IS_STAGE ? true : !disabledChains.includes(el)
      ).map((cId) => ({ chainId: cId, data: NETWORKS_DATA[cId] })),
    []
  );

  useEffect(() => {
    if (!CHAINS_LIST.includes(chainId) && chainId) {
      console.info(`[switch] chain from ${chainId}`,)
      switchChain({ chainId: CHAINS_LIST[0] });
    }
  }, [chainId, switchChain]);

  const onChainClick = async (chainId) => {
    try {
      tonDisable();
      const chain = NETWORKS_DATA[chainId];
      await switchChain({ chainId: Number(chainId) });
      setChoose(false);

      if (chain?.data) {
        toaster.warning(`${t('Network changed to')} ${chain.data.title}`);
      }
    } catch (error) {
      toaster.logError(error, '[onChainClick]');
    }
  };

  const onTonEnable = () => {
    tonEnable();
    setChoose(false);
  }

  if (!chainId) {
    return null;
  }

  return (
    <>
      {children ? (
        <div onClick={disabled ? undefined : () => setChoose(true)}>{children}</div>
      ) : (
        <Button
          sx={{
            borderRadius: '16px',
            minWidth: 40,
            padding: 0,
            position: isRelative ? 'relative' : 'absolute',
          }}
          style={style}
          variant='outlined'
          className={clsx(styles.chainButton, { [styles.chainButton_absolute]: !isRelative })}
          onClick={disabled ? undefined : () => setChoose(true)}
        >
          <img src={currentChain?.icon} alt='' />
        </Button>
      )}
      {choose && (
        <BottomDialog title={c('Change to')} isOpen={choose} onClose={() => setChoose(false)} style={{zIndex: 12000}}>
          <div className={styles.chainChains}>
            {IS_TON_ENABLED && <ChainItem
              option={TON_CHAIN}
              isActive={isTonChain}
              isDisabled={!IS_TON_ENABLED}
              onChainClick={onTonEnable}
            />}
            {chains.map((option, index) => {
              const isSoon = getIsSoon(option.chainId);

              const isActive = (!isTonChain)
                && chainId === Number(option.chainId);
              const isDisabled =
                (availableChains && !availableChains.includes(Number(option.chainId))) ||
                disabledChains.includes(option.chainId) || isSoon;

              return (
                <ChainItem
                  key={index}
                  option={option}
                  isActive={isActive}
                  isDisabled={isDisabled}
                  isSoon={isSoon}
                  onChainClick={onChainClick}
                  isMiningIconVisible
                  isClickAvailable={IS_DEVELOP || IS_STAGE}
                />
              );
            })}
          </div>
        </BottomDialog>
      )}
    </>
  );
}

export default ChainSwitcher;
