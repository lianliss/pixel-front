import React from 'react';
// @ts-ignore
import styles from './styles.module.scss';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useTranslation } from 'react-i18next';
import {ITutorialStep} from "../../config/config.tsx";
import Paper from "@mui/material/Paper";

type Props = {
  stepTutorial: number;
  handleClickOpenConfirmSkipDialog: () => void;
  handlePrevTutorialStep: () => void;
  handleComplete: () => void;
  handleNextTutorialStep: (step?: number) => void;
  steps: ITutorialStep[];
};

export const TutorialFooter: React.FC<Props> = ({
  stepTutorial,
  handleClickOpenConfirmSkipDialog,
  handlePrevTutorialStep,
  handleComplete,
  handleNextTutorialStep,
  steps,
}) => {
  const { t } = useTranslation('tutorial');

  return (
    <Paper elevation={0} square className={styles.footer__tutorial} sx={{ pointerEvents: 'initial'}}>
      <div className={styles.btns}>
        {stepTutorial === 1 ? (
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              gap: '6px',
            }}
          >
            <Button
              size='large'
              variant='outlined'
              fullWidth
              onClick={handleClickOpenConfirmSkipDialog}
            >
              {t('Skip')}
            </Button>
            <Typography fontSize={10}>{t('Not recommended')}</Typography>
          </Box>
        ) : (
          <Button
            size='large'
            variant='outlined'
            onClick={handlePrevTutorialStep}
            disabled={stepTutorial === steps.length}
          >
            <Typography sx={{ fontSize: 14 }} fontWeight='bold'>
              {t('Back')}
            </Typography>
          </Button>
        )}

        <Button
          size='large'
          variant='contained'
          onClick={stepTutorial === steps.length ? handleComplete : () => handleNextTutorialStep()}
        >
          {stepTutorial === steps.length ? t('To finish') : t('Next step')}
        </Button>
      </div>
    </Paper>
  );
};
