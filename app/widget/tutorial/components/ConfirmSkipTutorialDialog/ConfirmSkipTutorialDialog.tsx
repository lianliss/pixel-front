import React from 'react';
import Dialog from '@mui/material/Dialog';
import { DialogActions, DialogContentText, DialogTitle } from '@mui/material';
import DialogContent from '@mui/material/DialogContent';
import Button from '@mui/material/Button';
import { useTranslation } from 'react-i18next';

type Props = {
  isOpen: boolean;
  handleClose: () => void;
  handleComplete: () => void;
};

const ConfirmSkipTutorialDialog = (props: Props) => {
  const { isOpen, handleClose, handleComplete } = props;
  const { t } = useTranslation('tutorial');

  return (
    <Dialog open={isOpen} onClose={handleClose} style={{ zIndex: 9999 }}>
      <DialogTitle id='alert-dialog-title'>{t('Skip the tutorial')}?</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {t('You can return to the tutorial at any time in the settings')}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>{t('Cancel')}</Button>
        <Button onClick={handleComplete} autoFocus>
          {t('Skip')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmSkipTutorialDialog;
