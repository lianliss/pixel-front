import React, { ReactElement, ReactNode } from 'react';

// @ts-ignore
import styles from './styles.module.scss';
import TextCloud from 'app/widget/tutorials/components/icon/TextCloud';

type Props = {
  children: ReactNode;
  icon?: ReactElement;
  position?: 'start' | 'end' | 'center';
};
const TutorialHint = ({ children, icon, position = 'center' }: Props) => {
  return (
    <div className={styles.info__aria} style={{ justifyContent: position }}>
         {icon && icon}
         <div className={styles.info__cloud}>
             <TextCloud />
             <div className={styles.text__cloud}>{children}</div>
         </div>
    </div>
  );
};

export default TutorialHint;
