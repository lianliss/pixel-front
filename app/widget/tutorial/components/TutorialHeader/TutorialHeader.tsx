import React from 'react';
// @ts-ignore
import styles from './styles.module.scss';
import Button from '@mui/material/Button';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useTranslation } from 'react-i18next';
import Paper from "@mui/material/Paper";

type Props = {
  handleSkipClick: () => void;
};

export const TutorialHeader = ({ handleSkipClick }: Props) => {
  const { t } = useTranslation('tutorial');

  return (
    <Paper elevation={0} square className={styles.header__tutorial} sx={{ pointerEvents: 'initial'}}>
      <Button className={styles.tutorial__skip} onClick={handleSkipClick}>
        <Typography sx={{ fontSize: 14 }} fontWeight='bold'>
          {t('Skip')}
        </Typography>
      </Button>
    </Paper>
  );
};
