import React from 'react';

import {ITutorialConfig} from '../../config/config.tsx';
import { TutorialHeader } from '../TutorialHeader/TutorialHeader.tsx';
import { TutorialStage } from '../../stage/TutorialStage.tsx';
import { TutorialFooter } from '../TutorialFooter/TutorialFooter.tsx';

// @ts-ignore
import styles from './styles.module.scss';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useTranslation } from 'react-i18next';
import { CurrentTutorial } from '../../useTutorial.tsx';
import {useFindTargetElement} from "../../stage/useFindTargetElement.tsx";
import {useTargetElementClick} from "../../stage/useTargetElementClick.tsx";
import {useUpTargetElement} from "../../stage/useUpTargetElement.tsx";

type Props = {
  tutorial: CurrentTutorial;
  config: ITutorialConfig;
  stepTutorial: number;
  handleClickOpenConfirmSkipDialog: () => void;
  handleNextTutorialStep: (step?: number) => void;
  handlePrevTutorialStep: () => void;
  handleComplete: () => void;
  isFooter: boolean;
  isLast?: boolean;
};

const TutorialContent = ({
  tutorial,
  config,
  stepTutorial,
  handleClickOpenConfirmSkipDialog,
  handleNextTutorialStep,
  handlePrevTutorialStep,
  handleComplete,
  isFooter = true,
  isLast = false,
}: Props) => {
  const { t } = useTranslation('tutorial');

  const stage = config.steps[stepTutorial - 1];

  // Только вытягиваем целевой элемент
  useFindTargetElement(stepTutorial, stage);
  useUpTargetElement(stepTutorial, stage);

  // Тут логика в зависимости от текущего туториала
  useTargetElementClick(v => {
    if (v > config.steps.length) {
      handleComplete();
    } else {
      handleNextTutorialStep(v);
    }
  }, stage);

  return (
    <div className={styles.tutorial__inner}>
      <TutorialHeader handleSkipClick={handleClickOpenConfirmSkipDialog} />

      <div className={styles.content__tutorial}>
        <TutorialStage
          steps={config.steps}
          currentStep={stepTutorial}
        />

        {/*{isLast && (*/}
        {/*  <Button size='large' variant='contained' onClick={handleComplete}>*/}
        {/*    {t('To finish')}*/}
        {/*  </Button>*/}
        {/*)}*/}
      </div>

      {isFooter && (
        <TutorialFooter
          stepTutorial={stepTutorial}
          handleClickOpenConfirmSkipDialog={handleClickOpenConfirmSkipDialog}
          handlePrevTutorialStep={handlePrevTutorialStep}
          handleComplete={handleComplete}
          handleNextTutorialStep={handleNextTutorialStep}
          steps={config.steps}
        />
      )}
    </div>
  );
};

export default TutorialContent;
