import { useEffect } from 'react';
import {ITutorialStep} from "../config/config.tsx";

const applyStyles = (element) => {
  if (element) {
    element.style.zIndex = 1300;
  }
};

export const useUpTargetElement = (currentStage: number, stage: ITutorialStep) => {
  // Только вытягиваем целевой элемент
  useEffect(() => {

    const element = document.getElementById(stage.targetContainerElement);

    if (element) {
      applyStyles(element);
    } else {
      const intervalId = setInterval(() => {
        const newElement = document.getElementById(stage.targetContainerElement);

        if (newElement) {
          applyStyles(newElement);
          clearInterval(intervalId);
        }
      }, 500);
    }

    return () => {
      if (element) {
        element.style.zIndex = '';
      }
    };
  }, [currentStage, stage.targetContainerElement]);
};
