import { TutorialStep } from '../config/config.tsx';
import { useEffect } from 'react';
import { useInventoryChangeTabStageAction } from 'lib/Inventory/components/InventoryTutorial/hooks/useInventoryChangeTabStageAction.tsx';
import {useTargetElementClick} from "./useTargetElementClick.tsx";

export const useTargetActionTutorial = (
  handleNextTutorialStep: (step?: number) => void,
  handleComplete: () => void,
  stage: TutorialStep,
  tutorialId: string
) => {
  // Todo: Пока так, временно, пока не придумал решение лучше
  if (tutorialId.includes('inventory')) {
    useInventoryChangeTabStageAction(handleNextTutorialStep);

    useEffect(() => {
      if (stage.id === 5) {
        console.log('Это пятый этап');
      }
    }, [stage]);
  }
};
