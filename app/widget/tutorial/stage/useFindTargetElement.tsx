import { useEffect } from 'react';
import {ITutorialStep} from "../config/config.tsx";

const applyStyles = (element) => {
  if (element) {
    element.style.zIndex = 1300;
    element.style.border = '2px solid var(--color-primary)';
    element.style.boxShadow = '0 0 30px rgba(163, 248, 218, 0.5)';
    element.style.transition = 'transform 0.5s ease';
    element.style.overflow = 'hidden';
  }
};
const cleanStyles = (element) => {
  if (element) {
    element.style.zIndex = '';
    element.style.border = '';
    element.style.boxShadow = '';
    element.style.transition = '';
    element.style.overflow = '';
  }
}

export const useFindTargetElement = (currentStage: number, stage: ITutorialStep) => {
  // Только вытягиваем целевой элемент
  useEffect(() => {

    const element = document.getElementById(stage.targetElement);

    if (element) {
      applyStyles(element);
    } else {
      const intervalId = setInterval(() => {
        const newElement = document.getElementById(stage.targetElement);
        if (newElement) {
          applyStyles(newElement);
          clearInterval(intervalId);
        }
      }, 500);
    }

    return () => {
      cleanStyles(element);
      setTimeout(cleanStyles, 200);
    };
  }, [currentStage, stage.targetElement]);
};
