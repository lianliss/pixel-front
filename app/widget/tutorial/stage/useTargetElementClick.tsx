import { useEffect } from 'react';
import {ITutorialStep} from "../config/config.tsx";

export const useTargetElementClick = (
  handleNextTutorialStep: (step?: number) => void,
  step: ITutorialStep
) => {
  useEffect(() => {
    if (step.targetClickElement) {
      const element = document.getElementById(step.targetClickElement);

      if (element) {
        const handleClick = () => {
          handleNextTutorialStep(step.id + 1);
        };

        element.addEventListener('click', handleClick);

        return () => {
          element.removeEventListener('click', handleClick);
        };
      }
    }
  }, [step]);
};
