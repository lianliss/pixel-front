import React from 'react';
import TutorialHint from '../components/TutorialHint/TutorialHint.tsx';
import PixelHelloIcon from '../../tutorials/components/icon/PixelHelloIcon.tsx';
import { Typography } from '@ui-kit/Typography/Typography.tsx';
import { useTranslation } from 'react-i18next';
import {ITutorialStep} from "../config/config.tsx";

type Props = {
    currentStep: number;
  steps: ITutorialStep[];
};

export const TutorialStage = (props: Props) => {
  const { t } = useTranslation('tutorial');
  const { steps, currentStep } = props;
  const stage = steps[currentStep - 1];

  return (
    <TutorialHint icon={<PixelHelloIcon />} position={stage.hintPosition}>
      <Typography sx={{ fontSize: 14 }} textAlign='center' fontWeight='bold'>
        {t(`${stage.description}`)}
      </Typography>
    </TutorialHint>
  );
};
