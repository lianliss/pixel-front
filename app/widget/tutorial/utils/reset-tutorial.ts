import {getTutorialName, Tutorials} from "../config/config.tsx";

export function resetTutorials() {
    localStorage.removeItem(getTutorialName(Tutorials.inventoryBox));
    localStorage.removeItem(getTutorialName(Tutorials.mining));
    localStorage.removeItem(getTutorialName(Tutorials.miningBuild));
}
