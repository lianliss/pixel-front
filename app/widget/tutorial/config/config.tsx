export enum Tutorials {
  inventoryBox = 'inventoryBox',
  mining = 'mining',
  miningBuild = 'miningBuild',
}

export type ITutorialStep = {
  id: number;
  description: string;
  targetElement?: string;
  targetContainerElement?: string;
  targetClickElement?: string;
  targetClickType?: 'next' | 'complete';
  hintPosition: 'start' | 'end' | 'center';
  isModalVersion?: boolean;
  isFooter?: boolean;
  isLast?: boolean;
};

export type ITutorialConfig = {
  steps: ITutorialStep[];
}

export const getTutorialName = (tutorial: Tutorials) => {
  return `${tutorial}_tutorial`;
};
