import React, { useEffect } from 'react';
import { getTutorialName, Tutorials } from './config/config.tsx';
import {createEvent, createStore} from "effector";
import {useUnit} from "effector-react";

const currentTutorialStore = createStore<CurrentTutorial | null>(null);
const setTutorial = createEvent<CurrentTutorial | null>();

currentTutorialStore.on(setTutorial, (prev, next) => next);

export interface CurrentTutorial {
  tutorial: Tutorials;
  completed: boolean;
  stepTutorial: number;
  ready: boolean;
}

export const getTutorialInitialState = (tutorial: Tutorials): CurrentTutorial => ({
  tutorial: tutorial,
  completed: false,
  stepTutorial: 1,
  ready: true,
});

export const getCompleteTutorialState = (tutorial: Tutorials): CurrentTutorial => ({
  tutorial: tutorial,
  completed: true,
  stepTutorial: 1,
  ready: false,
});

export const getNextStepTutorialState = (tutorial: Tutorials, newStep: number) => ({
  tutorial: tutorial,
  completed: false,
  stepTutorial: newStep,
  ready: true,
});


export const useTutorial = () => {
  const tutorial = useUnit(currentTutorialStore);

  // clean
  useEffect(() => {
    return () => {
      setTutorial(null);
    }
  }, []);

  // start
  const startTutorial = (tutorial: Tutorials, initialData: CurrentTutorial | null) => {
    localStorage.setItem(
        getTutorialName(tutorial),
        JSON.stringify(initialData ? initialData : getTutorialInitialState(tutorial))
    );

    setTutorial(initialData ? initialData : getTutorialInitialState(tutorial));
  };

  // complete
  const completeTutorial = () => {
    void localStorage.setItem(
        getTutorialName(tutorial.tutorial),
        JSON.stringify(getCompleteTutorialState(tutorial.tutorial))
    );

    setTutorial(null);
  };

  // check
  const checkRunTutorial = (tutorial: Tutorials) => {
    const tutorialStrData = localStorage.getItem(getTutorialName(tutorial));
    const tutorialData = tutorialStrData ? JSON.parse(tutorialStrData) : null;

    if (tutorialData && !tutorialData.completed) {
      return {
        isStart: true,
        data: tutorialData,
      };
    }

    return {
      isStart: !tutorialData,
      data: null,
    };
  };

  return {
    checkRunTutorial,
    startTutorial,
    tutorial,
    completeTutorial,
  }
};
