import React, { lazy, useCallback, useEffect } from 'react';
import {CurrentTutorial, getNextStepTutorialState, useTutorial} from './useTutorial.tsx';
import { useSwitchChain } from '@chain/hooks/useSwitchChain.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { Chain } from 'services/multichain/chains';

// @ts-ignore
import styles from './styles.module.scss';
import {getTutorialName, ITutorialConfig} from './config/config.tsx';
import {Backdrop} from "@mui/material";
import TutorialContent from "./components/TutorialContent/TutorialContent.tsx";
import ConfirmSkipTutorialDialog from "./components/ConfirmSkipTutorialDialog/ConfirmSkipTutorialDialog.tsx";
import Container from "@mui/material/Container";

type Props = {
  value: CurrentTutorial;
  step: number;
  onChangeStep?: (step: number) => void;
  config: ITutorialConfig;
  onComplete?: () => void;
};

export const Tutorial = ({ step: stepTutorial, onChangeStep: setStepTutorial, config, onComplete, value: currentTutorial }: Props) => {
  const {switchChain} = useSwitchChain();
  const account = useAccount();

  const { isConnected, accountAddress } = account;

  const [openConfirmSkipDialog, setOpenConfirmSkipDialog] = React.useState(false);

  /** Проверка сети и подключенного кошелька */
  useEffect(() => {
    if (account.chainId !== Chain.SONGBIRD && accountAddress && isConnected) {
      // void switchChain(Chain.SONGBIRD);
    }
  }, [account.chainId]);

  /** Проверка подключенного кошелька и конфига перед рендером тутора */
  if (!accountAddress || !isConnected) {
    return null;
  }

  const handleComplete = useCallback(() => {
    onComplete?.();
    setOpenConfirmSkipDialog(false);
  }, [onComplete]);

  const handleClickOpenConfirmSkipDialog = () => {
    setOpenConfirmSkipDialog(true);
  };

  const handleCloseConfirmSkipDialog = () => {
    setOpenConfirmSkipDialog(false);
  };

  const handleNextTutorialStep = useCallback(
    (step?: number) => {
      if (!currentTutorial) {
        console.log(currentTutorial);
        return;
      }
      if (step) {
        setStepTutorial(step);
        void localStorage.setItem(
          getTutorialName(currentTutorial.tutorial),
          JSON.stringify(getNextStepTutorialState(currentTutorial.tutorial, step))
        );
      } else {
        setStepTutorial(stepTutorial + 1);
        void localStorage.setItem(
          getTutorialName(currentTutorial.tutorial),
          JSON.stringify(getNextStepTutorialState(currentTutorial.tutorial, stepTutorial + 1))
        );
      }
    },
    [stepTutorial, currentTutorial]
  );

  const handlePrevTutorialStep = useCallback(() => {
    if (!currentTutorial) {
      console.log(currentTutorial);
      return;
    }
    setStepTutorial(stepTutorial - 1);
    void localStorage.setItem(
      getTutorialName(currentTutorial.tutorial),
      JSON.stringify(getNextStepTutorialState(currentTutorial.tutorial, stepTutorial - 1))
    );
  }, [stepTutorial, currentTutorial]);

  /** Проверка готовности текущего туториала */
  if (!currentTutorial || !currentTutorial.ready || currentTutorial.completed) {
    return null;
  }

  const tutorial = currentTutorial;
  const stepData = config.steps[stepTutorial - 1];

  const isModalVersion = stepData.isModalVersion;
  const isFooter = stepData.isFooter;
  const isLast = stepData.isLast;

  return (
    <Backdrop open className={`${styles.tutorial} ${isModalVersion && styles.over}`} sx={{ bottom: 0, backgroundColor: 'transparent', pointerEvents: 'none', userSelect: 'none', zIndex: 9000 }}>
      <React.Suspense fallback={<div>Loading...</div>}>
      <Container maxWidth='xs' sx={{ width: '100%', height: '100%'}}>
        <TutorialContent
            tutorial={tutorial}
            config={config}
            stepTutorial={stepTutorial}
            handleClickOpenConfirmSkipDialog={handleClickOpenConfirmSkipDialog}
            handleNextTutorialStep={handleNextTutorialStep}
            handlePrevTutorialStep={handlePrevTutorialStep}
            handleComplete={handleComplete}
            isFooter={isFooter}
            isLast={isLast}
        />
      </Container>
      </React.Suspense>

      <ConfirmSkipTutorialDialog
        isOpen={openConfirmSkipDialog}
        handleClose={handleCloseConfirmSkipDialog}
        handleComplete={handleComplete}
      />
    </Backdrop>
  );
};
