import React, { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { langKey } from '../../shared/const/localStorage.ts';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

import styles from './styles.module.scss';
import { Typography } from '../../shared/ui/Typography/Typography.tsx';
import { useTranslation } from 'react-i18next';

import IconUs from '../../shared/ui/icon/flags/IconUs.tsx';
import IconRu from '../../shared/ui/icon/flags/IconRu.tsx';

interface Props {
  options: {
    title: string;
    value: Language;
    icon: JSX.Element;
  }[];

  value: Language;
  setValue: (value: Language) => void;
  compact?: boolean;
}

const SelectLang = ({ options, value, setValue, compact }: Props) => {
  const { t } = useTranslation('tutorial');

  const handleChange = (event: SelectChangeEvent) => {
    setValue(event.target.value as Language);
  };

  return (
    <div className={styles.switcher}>
      {!compact && <Typography sx={{fontSize: 14}} fontWeight='bold'>
        {t('Select language')}
      </Typography>}
      <Select
        size='small'
        value={value}
        onChange={handleChange}
        sx={{
          '& .MuiSelect-select': {
            display: 'flex',
          },
        }}
      >
        {options.map((option) => (
          <MenuItem key={`lg-${option.value}`} value={option.value}>
            {option.icon}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
};

export enum Language {
  ru = 'ru',
  en = 'en',
}

const LANG_OPTIONS = [
  {
    title: 'Ru',
    value: Language.ru,
    icon: <IconRu />,
  },
  {
    title: 'En',
    value: Language.en,
    icon: <IconUs />,
  },
];

export const LangSwitcher = ({ compact }) => {
  const navigate = useNavigate();
  const location = useLocation();
  const { _, i18n } = useTranslation();

  const onChangeLanguage = (selectedLang: Language) => {
    void i18n.changeLanguage(selectedLang);
    localStorage.setItem(langKey, selectedLang);
    navigate(location.pathname, { state: { lang: selectedLang } });
  };

  return (
    <SelectLang
      options={LANG_OPTIONS}
      setValue={onChangeLanguage}
      value={i18n.language as Language}
      compact={compact}
    />
  );
};
