import Dialog, {DialogProps} from "@mui/material/Dialog";
import ConnectWalletContent from "./ConnectWalletContent.tsx";
import {styled} from "@mui/material/styles";
import Paper from "@mui/material/Paper";

const StyledPaper = styled(Paper)(() => ({
    padding: '24px 32px'
}));

export function ConnectWalletDialog(props: Omit<DialogProps, 'children'>) {
    return <Dialog fullWidth maxWidth='xs' PaperComponent={StyledPaper}  {...props}>
        <ConnectWalletContent/>
    </Dialog>
}
