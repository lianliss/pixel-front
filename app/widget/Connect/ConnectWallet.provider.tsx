import React, {useEffect} from "react";

import {ConnectWalletDialog} from "./ConnectWalletDialog.tsx";
import {useConnectWallet} from "./ConnectWallet.store.ts";
import styles from "lib/Wallet/pages/wallet/Wallet.module.scss";
import SignUp from "lib/Wallet/pages/signup";
import {useAccount} from "@chain/hooks/useAccount.ts";
import {pixelStorage} from "@chain/connectors/pixel.provider.ts";

function ConnectWalletProvider(props) {
  const connectWallet = useConnectWallet();
  const account = useAccount();
  const isConnected = account.isConnected;
  const isConnector = !!account.connector?.getChainId;

  useEffect(() => {
    if (isConnected && (!isConnector || !pixelStorage.getPrivateKey())) {
      localStorage.removeItem('wagmi.store');
      localStorage.removeItem('wagmi.recentConnectorId');
      // location.reload();
    }
  }, [isConnector, isConnected]);

  return <>
    {isConnected ? props.children : <div className={styles.loading}><SignUp/></div>}
    <ConnectWalletDialog open={connectWallet.open} onClose={() => connectWallet.hide()}/>
  </>
}

export default ConnectWalletProvider;
