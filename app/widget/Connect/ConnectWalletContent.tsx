import React from 'react';
import { useSelector } from 'react-redux';

// Components
import { Icon } from '@blueprintjs/core';

// Utils
import { useWeb3 } from 'services/web3Provider.jsx';
import { classNames as cn } from 'utils/index.jsx';
import * as CONNECTORS from 'services/multiwallets/connectors.js';
import { getConnectorObject } from 'services/multiwallets/multiwalletsDifference.js';

// Styles
import './ConnectWalletContent.scss';
import { useAccount } from '@chain/hooks/useAccount.ts';
import {useConnect} from "@chain/hooks/useConnect.ts";
import {hideConnectWallet} from "./ConnectWallet.store.ts";
import {metaMask} from "@wagmi/connectors";
import {pixelConnectorState} from "@chain/wagmi.ts";

function ConnectWalletContent(props) {
  const account = useAccount();
  const { accountAddress, chainId, isConnected } = account;
  const connect = useConnect();

  const handlePixelConnect = () => {
    connect({ connector: pixelConnectorState });
  };

  const handleMetamaskWallet = () => {
    const isMetamask = getConnectorObject(CONNECTORS.METAMASK);

    if (isMetamask) {
      connect({ connector: metaMask() });
      return;
    }

    window.open(
      `https://metamask.app.link/dapp/hellopixel.network${window.location.pathname}`,
      '_blank'
    );
  };

  const handleTrustWallet = () => {
    const isTrust = getConnectorObject(CONNECTORS.TRUST_WALLET);
    if (isTrust) {
      connect();
      return;
    }

    window.open(`trust://open_url?coin_id=60&url=${window.location.href}`, '_blank');
  };

  React.useEffect(() => {
    if (isConnected) {
      hideConnectWallet();
    }
  }, [isConnected]);

  return (
    <div className={'ConnectToWalletModal'}>
      <div className='ConnectToWalletModal__wallets'>
        <div className='row'>
          {/*<Wallet*/}
          {/*    title='Pixel Wallet'*/}
          {/*    icon={require('assets/social/pixel.png')}*/}
          {/*    style={{ background: '#3375bb' }}*/}
          {/*    onClick={handlePixelConnect}*/}
          {/*/>*/}
          <Wallet
            title='Metamask'
            icon={require('assets/social/metaMask.svg')}
            style={{ background: '#fff' }}
            onClick={handleMetamaskWallet}
          />
          <Wallet
              title='Trust Wallet'
              icon={require('assets/social/trustWallet.svg')}
              style={{ background: '#3375bb' }}
              onClick={handleTrustWallet}
              disabled
          />
        </div>
        <div className='row'>
          {/*<Wallet*/}
          {/*    title='Trust Wallet'*/}
          {/*    icon={require('assets/social/trustWallet.svg')}*/}
          {/*    style={{ background: '#3375bb' }}*/}
          {/*    onClick={handleTrustWallet}*/}
          {/*    disabled*/}
          {/*/>*/}
          <Wallet
            title='WalletConnect'
            icon={require('assets/social/walletConnect.svg')}
            style={{
              background: 'radial-gradient(100% 100% at 0% 50%, #5D9DF6 0%, #006FFF 100%)',
            }}
            disabled
            onClick={() => connect()}
          />
        </div>
      </div>
      {/*<span className="action-text">*/}
      {/*  <Icon icon={"help"} />*/}
      {/*  &nbsp;{"How to connect"}*/}
      {/*</span>*/}
    </div>
  );
}

const Wallet = ({ icon = null, title = '', disabled = false, style = {}, onClick = () => {} }) => {
  return (
    <div className={cn({ ConnectToWalletModal__wallet: true, disabled })} onClick={onClick}>
      <div className='ConnectToWalletModal__wallet__icon' style={style}>
        <img src={icon} />
      </div>
      <div className='ConnectToWalletModal__title'>{title}</div>
    </div>
  );
};

export default ConnectWalletContent;
