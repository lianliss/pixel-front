import {createEvent, createStore} from "effector";
import {useUnit} from "effector-react";

export const connectWalletStore = createStore({
    open: false,
});
export const showConnectWallet = createEvent();
export const hideConnectWallet = createEvent();

connectWalletStore.on(showConnectWallet, (_prev, _payload) => ({ open: true }));
connectWalletStore.on(hideConnectWallet, (_prev, _payload) => ({ open: false }));

export function useConnectWallet() {
    const state = useUnit(connectWalletStore);

    return {
        open: state.open,
        show: showConnectWallet,
        hide: hideConnectWallet,
    }
}
