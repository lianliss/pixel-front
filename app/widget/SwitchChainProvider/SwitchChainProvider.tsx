import {useAccount} from "@chain/hooks/useAccount.ts";
import React from "react";
import {Chain} from "services/multichain/chains";
import NotAvailableChain from "../../shared/blocks/NotAvailableChain/NotAvailableChain.tsx";
import Box from "@mui/material/Box";

export function SwitchChainProvider(props: React.PropsWithChildren) {
    const account = useAccount();

    if (!account.chainId || Object.values(Chain).includes(account.chainId)) {
        return props.children;
    }

    return <Box sx={{ px: 5, display: 'flex', alignItems: 'center', width: '100%', height: '100vh', justifyContent: 'center' }}>
        <NotAvailableChain chainId={Chain.SONGBIRD} title={`Not Available Chain (${account.chainId || '-'})`} />
    </Box>
}
