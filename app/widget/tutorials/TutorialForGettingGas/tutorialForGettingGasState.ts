import { createStore, createEvent } from 'effector';

const visibleTutorial = createEvent();
const startTutorial = createEvent();
const endTutorial = createEvent();
const completedTutorial = createEvent();
const nextStep = createEvent();
const prevStep = createEvent();

const $tutorialForGettingGasState = createStore({
  isStarted: false,
  isVisible: false,
  step: 0,
})
  .on(visibleTutorial, (state) => ({ ...state, isVisible: true }))
  .on(startTutorial, (state) => ({ ...state, isStarted: true }))
  .on(nextStep, (state) => ({ ...state, step: state.step + 1 }))
  .on(prevStep, (state) => ({ ...state, step: state.step - 1 }))
  .on(endTutorial, (state) => ({ ...state, isStarted: false, isVisible: false, step: 0 }))
  .on(completedTutorial, (state) => ({ ...state, isStarted: false, isVisible: false, step: 0 }));

export {
  $tutorialForGettingGasState,
  visibleTutorial,
  startTutorial,
  endTutorial,
  nextStep,
  completedTutorial,
};
