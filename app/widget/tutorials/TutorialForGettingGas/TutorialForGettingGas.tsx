import React, { useEffect } from 'react';
import useTutorial from '../hooks/useTutorial.tsx';
import TutorialModal from '@ui-kit/TutorialModal/TutorialModal.tsx';
import { LangSwitcher } from '../../../widget/LangSwitcher/LangSwitcher.tsx';
import TutorialWelcomeScreen from '../components/TutorialWelcomeScreen/TutorialWelcomeScreen.tsx';
import { tutorialForGettingGasKey } from '../../../shared/const/localStorage.ts';
import { configTutorials, TUTORIAL_ENABLED } from '../config.tsx';
import { Chain } from 'services/multichain/chains';
import { useSwitchChain } from '@chain/hooks/useSwitchChain.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';

type WithTutorialProps = {
  tutorialStorageKey?: string;
  started?: boolean;
};

const withTutorial = <P extends object>(WrappedComponent: React.ComponentType<P>) => {
  return (props: P & WithTutorialProps) => {
    const { started, ...restProps } = props;
    const {switchChain} = useSwitchChain();
    const account = useAccount();
    const { isConnected, accountAddress } = account;

    const { isVisible, isStarted, onStart, onEnd, onNext, step } =
      useTutorial(tutorialForGettingGasKey);
    const { steps } = configTutorials[tutorialForGettingGasKey];

    useEffect(() => {
      if (
        TUTORIAL_ENABLED &&
        isVisible &&
        account.chainId !== Chain.SONGBIRD &&
        accountAddress &&
        isConnected
      ) {
        // switchChain(Chain.SONGBIRD);
      }
    }, [isVisible, account.chainId]);

    return (
      <>
        {TUTORIAL_ENABLED && accountAddress && isConnected && (
          <>
            {!isStarted && started && (
              <TutorialModal
                open={isVisible}
                onSkip={onEnd}
                rightButton={
                  <div>
                    <LangSwitcher />
                  </div>
                }
              >
                {!isStarted && <TutorialWelcomeScreen closeModal={onEnd} startTutorial={onStart} />}
              </TutorialModal>
            )}
            {isStarted && (
              <TutorialModal skipButton target={steps[step].target} open={isVisible} onSkip={onEnd}>
                {steps[step].content}
              </TutorialModal>
            )}
          </>
        )}
        <WrappedComponent {...(restProps as P)} />
      </>
    );
  };
};

export const TutorialForGettingGasProvider = withTutorial(({ children }) => <>{children}</>);
