import React, { useRef, memo, useState, useEffect, useMemo } from 'react';
import Typography from '@mui/material/Typography';
import styles from './styles.module.scss';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Button } from '@ui-kit/Button/Button.tsx';
import useElementTutorial from '../../../hooks/useElementTutorial.ts';
import TutorialInfoSection from '../../../components/TutorialInfoSection/TutorialInfoSection.tsx';
import PixelIcon from '../../../components/icon/PixelIcon.tsx';
import useTutorial from '../../../hooks/useTutorial.tsx';
import { tutorialForGettingGasKey } from '../../../../../shared/const/localStorage.ts';
import { checkQuest } from 'lib/Quests/api-server/quests.store.ts';
import toaster from 'services/toaster.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import { tutorialQuestId } from '../../../config.tsx';

type Props = {
  target?: string;
  icon: JSX.Element;
};

const CheckQuestScreen = ({ target, icon }: Props) => {
  const { t } = useTranslation('tutorial');
  const account = useAccount();
  const { accountAddress } = account;
  const arrowRef = useRef<HTMLDivElement | null>(null);
  const navigate = useNavigate();
  const [isDisabled, setIsDisabled] = useState(true);
  const [_, setCheckQuest] = useState(false);
  const { onCompleted } = useTutorial(tutorialForGettingGasKey);

  const { t: n } = useTranslation('notifications');

  const onCheckQuest = async () => {
    await checkQuest({ questId: tutorialQuestId, address: accountAddress })
      .then((isCompleted) => {
        if (isCompleted) {
          onCompleted();
          navigate('/wallet/mining');
        } else {
          (toaster as any).warning(`Quest not completed`);
        }
      })
      .catch((e) => {
        (toaster as any).captureException(e, n('Check quest failed'));
      });
  };

  const handleStartAction = () => {
    const overlayElement = document.getElementById('#tutorials-overlay');
    if (overlayElement) {
      overlayElement.style.marginBottom = '210px';
      overlayElement.style.bottom = '0';
      overlayElement.style.height = 'initial';
    }
  };

  const handleCompleted = () => {
    void onCheckQuest();
  };
  const handleClick = () => {
    setCheckQuest((prev) => !prev);
  };

  useElementTutorial({
    elementId: target,
    arrowRef: arrowRef,
    handleClick: handleClick,
    isAnimationPulse: false,
    arrowPosition: {
      position: 'top-left',
    },
    handleNoElement: () => {},
    handleStartAction,
  });

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsDisabled(false);
    }, 2000);

    return () => {
      clearTimeout(timer);
    };
  }, []);

  return (
    <div className={styles.tutorial}>
      <div className={styles.info__section}>
        <TutorialInfoSection icon={icon}>
          <Typography
            className={styles.info__text}
            sx={{ fontSize: 14 }}
            dangerouslySetInnerHTML={{ __html: t('Complete') }}
          />
        </TutorialInfoSection>
        <Button
          variant='contained'
          fullWidth
          onClick={handleCompleted}
          style={{ marginTop: 10 }}
          disabled={isDisabled}
        >
          {t('Next step')}
        </Button>
      </div>
    </div>
  );
};

export default memo(CheckQuestScreen);
