import React, { memo, useRef } from 'react';

import Typography from '@mui/material/Typography';
// @ts-ignore
import styles from './styles.module.scss';
import { useTranslation } from 'react-i18next';
import TutorialInfoSection from '../../../components/TutorialInfoSection/TutorialInfoSection.tsx';
import ArrowTutorial from '../../../components/icon/ArrowTutorial.tsx';
import useElementTutorial from '../../../hooks/useElementTutorial.ts';
import useTutorial from '../../../hooks/useTutorial.tsx';
import { tutorialForGettingGasKey } from '../../../../../shared/const/localStorage.ts';

type Props = {
  target?: string;
  icon: JSX.Element;
};

const GoToQuestsScreen = ({ target, icon }: Props) => {
  const { t } = useTranslation('tutorial');
  const arrowRef = useRef<HTMLDivElement | null>(null);
  const { onNext } = useTutorial(tutorialForGettingGasKey);

  useElementTutorial({
    elementId: target,
    arrowRef: arrowRef,
    handleClick: onNext,
    isAnimationPulse: true,
    arrowPosition: {
      position: 'top-left',
      top: 35,
      left: 45,
    },
  });

  return (
    <div className={styles.tutorial}>
      <div className={styles.info__section}>
        <TutorialInfoSection icon={icon}>
          <Typography
            className={styles.info__text}
            sx={{ fontSize: 14 }}
            dangerouslySetInnerHTML={{ __html: t('First of all') }}
          />
        </TutorialInfoSection>
      </div>

      <div className={`${styles.pointLeftAnim}`} ref={arrowRef}>
        <ArrowTutorial />
      </div>
    </div>
  );
};

export default memo(GoToQuestsScreen);
