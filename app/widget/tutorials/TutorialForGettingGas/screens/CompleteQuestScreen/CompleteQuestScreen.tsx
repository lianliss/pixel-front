import React, { useRef, memo, useState } from 'react';
import Typography from '@mui/material/Typography';
// @ts-ignore
import styles from './styles.module.scss';
import { useTranslation } from 'react-i18next';
import useTutorial from '../../../hooks/useTutorial.tsx';
import { tutorialForGettingGasKey } from '../../../../../shared/const/localStorage.ts';
import useElementTutorial from '../../../hooks/useElementTutorial.ts';
import TutorialInfoSection from '../../../components/TutorialInfoSection/TutorialInfoSection.tsx';
import PixelIcon from '../../../components/icon/PixelIcon.tsx';
import ArrowTutorial from '../../../components/icon/ArrowTutorial.tsx';

type Props = {
  target?: string;
  icon: JSX.Element;
};

const CompleteQuestScreen = ({ target, icon }: Props) => {
  const { t } = useTranslation('tutorial');
  const arrowRef = useRef<HTMLDivElement | null>(null);
  const [_, setShouldRerender] = useState(false);
  const { onNext } = useTutorial(tutorialForGettingGasKey);

  const handleNoElement = () => {
    setShouldRerender((prevState) => !prevState);
  };

  useElementTutorial({
    elementId: target,
    arrowRef: arrowRef,
    handleClick: onNext,
    isAnimationPulse: false,
    arrowPosition: {
      position: 'bottom-right',
      right: -70,
      bottom: 10,
    },
    handleNoElement,
  });

  return (
    <div className={styles.tutorial}>
      <div className={styles.info__section}>
        <TutorialInfoSection icon={icon}>
          <Typography
            className={styles.info__text}
            sx={{ fontSize: 14 }}
            dangerouslySetInnerHTML={{ __html: t('Get free gas') }}
          />
        </TutorialInfoSection>
      </div>

      <div className={`${styles.pointBottomAnim}`} ref={arrowRef}>
        <ArrowTutorial style={{ transform: 'rotate(90deg) translateX(50%)' }} />
      </div>
    </div>
  );
};

export default memo(CompleteQuestScreen);
