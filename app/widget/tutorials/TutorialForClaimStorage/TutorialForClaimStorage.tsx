import React, { useEffect } from 'react';
import TutorialModal from '@ui-kit/TutorialModal/TutorialModal.tsx';
import { tutorialForClaimStorageKey } from '../../../shared/const/localStorage.ts';
import {configTutorials, TUTORIAL_ENABLED} from '../config.tsx';
import useTutorialForClaimStorage from '../hooks/useTutorialForClaimStorage.tsx';
import ClaimStorageScreen from './screens/ClaimStorageScreen/ClaimStorageScreen.tsx';
import { Chain } from 'services/multichain/chains';
import { useSwitchChain } from '@chain/hooks/useSwitchChain.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';

type WithTutorialProps = {
  tutorialStorageKey?: string;
  started?: boolean;
};

const withTutorial = <P extends object>(WrappedComponent: React.ComponentType<P>) => {
  return (props: P & WithTutorialProps) => {
    const { started, ...restProps } = props;
    const {switchChain} = useSwitchChain();
    const account = useAccount();
    const { isVisible, isStarted, onStart, onEnd, step } = useTutorialForClaimStorage(
      tutorialForClaimStorageKey
    );
    const { steps } = configTutorials[tutorialForClaimStorageKey];

    useEffect(() => {
      if (TUTORIAL_ENABLED && isVisible && account.chainId !== Chain.SONGBIRD) {
        switchChain({ chainId: Chain.SONGBIRD });
      }
    }, [isVisible, account.chainId]);

    return (
      <>
        {TUTORIAL_ENABLED && (
          <>
            {!isStarted && started && account.chainId === Chain.SONGBIRD && (
              <TutorialModal skipButton open={isVisible} onSkip={onEnd}>
                {!isStarted && <ClaimStorageScreen target='#ClaimStorageScreen' />}
              </TutorialModal>
            )}
            {isStarted && account.chainId === Chain.SONGBIRD && (
              <TutorialModal skipButton target={steps[step].target} open={isVisible} onSkip={onEnd}>
                {steps[step].content}
              </TutorialModal>
            )}
          </>
        )}
        <WrappedComponent {...(restProps as P)} />
      </>
    );
  };
};

export const TutorialForClaimStorageProvider = withTutorial(({ children }) => <>{children}</>);
