import React, { memo } from 'react';
import Typography from '@mui/material/Typography';
import styles from './styles.module.scss';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useTranslation } from 'react-i18next';
import TutorialInfoSection from '../../../components/TutorialInfoSection/TutorialInfoSection.tsx';
import useTutorialForClaimStorage from '../../../hooks/useTutorialForClaimStorage.tsx';
import { tutorialForClaimStorageKey } from '../../../../../shared/const/localStorage.ts';

type Props = {
  contentKey: string;
  icon: JSX.Element;
};

const TutorialInfoScreen = ({ contentKey, icon }: Props) => {
  const { t } = useTranslation('tutorial');
  const { onNext } = useTutorialForClaimStorage(tutorialForClaimStorageKey);

  return (
    <div className={styles.tutorial}>
      <div className={styles.info__section}>
        <TutorialInfoSection icon={icon}>
          <Typography
            className={styles.info__text}
            sx={{ fontSize: 14 }}
            dangerouslySetInnerHTML={{ __html: t(contentKey) }}
          />
        </TutorialInfoSection>

        <Button variant='contained' fullWidth onClick={onNext} style={{ marginTop: 10 }}>
          {t('Next step')}
        </Button>
      </div>
    </div>
  );
};

export default memo(TutorialInfoScreen);
