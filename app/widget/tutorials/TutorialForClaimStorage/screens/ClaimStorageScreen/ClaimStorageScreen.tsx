import React, { useRef, memo, useEffect } from 'react';
import Typography from '@mui/material/Typography';
import styles from './styles.module.scss';
import { useTranslation } from 'react-i18next';
import useElementTutorial from '../../../hooks/useElementTutorial.ts';
import TutorialInfoSection from '../../../components/TutorialInfoSection/TutorialInfoSection.tsx';
import ArrowTutorial from '../../../components/icon/ArrowTutorial.tsx';
import useTutorialForClaimStorage from '../../../hooks/useTutorialForClaimStorage.tsx';
import { tutorialForClaimStorageKey } from '../../../../../shared/const/localStorage.ts';
import useMiningCheckStorage from './useMiningCheckStorage.ts';
import { PixelEmoji4 } from '../../../components/PXLEmoji/PixelEmoji.tsx';
import { Button } from '@ui-kit/Button/Button.tsx';

type Props = {
  target?: string;
};

const ClaimStorageScreen = ({ target }: Props) => {
  const { t } = useTranslation('tutorial');
  const arrowRef = useRef<HTMLDivElement | null>(null);
  const { onStart, setRestartTimestamp, onNext } = useTutorialForClaimStorage(
    tutorialForClaimStorageKey
  );

  useEffect(() => {
    const targetElement = document.getElementById(target);

    if (!targetElement) {
      return;
    }

    const children = targetElement.children;

    const originalChildrenDisplay = [];
    for (let i = 1; i < children.length; i++) {
      originalChildrenDisplay[i] = children[i].style.display;
      children[i].style.display = 'none';
    }

    const firstChild = children[0];
    const firstChildChildren = firstChild.children;

    const originalFirstChildChildrenDisplay = [];
    for (let j = 1; j < firstChildChildren.length; j++) {
      originalFirstChildChildrenDisplay[j] = firstChildChildren[j].style.display;
      firstChildChildren[j].style.display = 'none';
    }

    const storageClaimButtonElement = document.getElementById('#storageClaimButton');
    const element = document.getElementById('#miningHeader');
    const originalElementDisplay = element.style.display;
    const originalTargetHeight = targetElement.style.height;
    const originalTargetMarginTop = targetElement.style.marginTop;

    element.style.display = 'none';
    targetElement.style.height = '230px';
    targetElement.style.marginTop = '70px';

    const handleEvent = (event) => {
      event.stopPropagation();
      onStart();
    };

    storageClaimButtonElement.addEventListener('click', handleEvent);
    storageClaimButtonElement.style.zIndex = '1000';

    return () => {
      element.style.display = originalElementDisplay;
      targetElement.style.height = originalTargetHeight;
      targetElement.style.marginTop = originalTargetMarginTop;

      for (let i = 1; i < children.length; i++) {
        children[i].style.display = originalChildrenDisplay[i];
      }

      for (let j = 1; j < firstChildChildren.length; j++) {
        firstChildChildren[j].style.display = originalFirstChildChildrenDisplay[j];
      }

      storageClaimButtonElement.removeEventListener('click', handleEvent);
    };
  }, []);

  useElementTutorial({
    elementId: target,
    arrowRef: arrowRef,
    handleClick: () => console.log('handleClick'),
    isAnimationPulse: false,
    arrowPosition: {
      position: 'bottom-left',
      left: 60,
      bottom: 90,
    },
    handleNoElement: () => {},
  });

  const { isFull, hours, minutes, seconds } = useMiningCheckStorage();

  // useEffect(() => {
  //   if (!isFull) {
  //     const totalSeconds = hours * 3600 + minutes * 60 + seconds;
  //     const timestamp = new Date(Date.now() + totalSeconds * 1000);
  //     setRestartTimestamp(timestamp);
  //   }
  // }, [isFull, hours, minutes, seconds]);

  return (
    <div className={styles.tutorial}>
      <div className={styles.info__section}>
        <TutorialInfoSection icon={<PixelEmoji4 />}>
          {!isFull ? (
            <Typography
              className={styles.info__text}
              sx={{ fontSize: 14 }}
              dangerouslySetInnerHTML={{
                __html: t('Wait for the storage to fill', {
                  timerText: `${hours}h ${minutes}m ${seconds}s`,
                }),
              }}
            />
          ) : (
            <Typography
              className={styles.info__text}
              sx={{ fontSize: 14 }}
              dangerouslySetInnerHTML={{ __html: t('Now you can') }}
            />
          )}
        </TutorialInfoSection>
        {!isFull && (
          <Button variant='contained' fullWidth onClick={onNext} style={{ marginTop: 10 }}>
            {t('Next step')}
          </Button>
        )}
      </div>

      {isFull && (
        <div className={`${styles.pointTopAnim}`} ref={arrowRef}>
          <ArrowTutorial style={{ transform: 'rotate(90deg) translateX(-80%)' }} />
        </div>
      )}
    </div>
  );
};

export default memo(ClaimStorageScreen);
