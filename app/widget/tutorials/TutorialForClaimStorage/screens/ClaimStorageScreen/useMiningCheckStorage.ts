import { MINER_CONTRACT_ID } from 'lib/Mining/api-contract/miner.constants.ts';
import { useMining } from 'lib/Mining/hooks/useMining.ts';
import { useAccount } from '@chain/hooks/useAccount.ts';

function useMiningCheckStorage() {
  const account = useAccount();
  const minerAddress = MINER_CONTRACT_ID[account.chainId].core;
  const miningApi = useMining({
    minerAddress,
  });

  const {
    mined,
    sizeLimit,
    rewardPerSecond,
    claimTimestamp,
    updatedAt: lastGetTimestamp,
    extra: {} = {},
  } = miningApi.data || {};

  const spaceLeft = sizeLimit - mined;
  let secondsLeft = spaceLeft / rewardPerSecond;
  const hours = Math.floor(secondsLeft / 3600);
  secondsLeft %= 3600;
  const minutes = Math.floor(secondsLeft / 60);
  const seconds = Math.floor(secondsLeft % 60);
  const isFull = mined === sizeLimit;

  return { isFull, hours, minutes, seconds };
}

export default useMiningCheckStorage;
