import React, { useEffect, useRef } from 'react';

import Typography from '@mui/material/Typography';
import styles from './styles.module.scss';
import { useTranslation } from 'react-i18next';
import useTutorialForClaimStorage from '../../../hooks/useTutorialForClaimStorage.tsx';
import { tutorialForClaimStorageKey } from '../../../../../shared/const/localStorage.ts';
import useElementTutorial from '../../../hooks/useElementTutorial.ts';
import TutorialInfoSection from '../../../components/TutorialInfoSection/TutorialInfoSection.tsx';
import PixelIcon from '../../../components/icon/PixelIcon.tsx';
import ArrowTutorial from '../../../components/icon/ArrowTutorial.tsx';
import { PixelEmoji4 } from '../../../components/PXLEmoji/PixelEmoji.tsx';

type Props = {
  target?: string;
};

const UpgradeStorageScreen = ({ target }: Props) => {
  const { t } = useTranslation('tutorial');
  const arrowRef = useRef<HTMLDivElement | null>(null);
  const { onCompleted } = useTutorialForClaimStorage(tutorialForClaimStorageKey);

  const handleClick = () => {
    onCompleted();
  };

  const handleNoElement = () => {};

  const handleStartAction = () => {};

  useEffect(() => {
    const elementWrapper = document.getElementById(target);
    const element = document.getElementById('miningButtons_list_id');
    const children = element.children;

    for (let i = 0; i < children.length; i++) {
      if (i === 1) {
        children[i].style.transform = 'scale(1.1)';
      } else {
        children[i].style.opacity = '0.1';
      }
    }

    elementWrapper.style.zIndex = 'var(--z-index-tutorial-content)';

    return () => {
      const element = document.getElementById('miningButtons_list_id');
      const children = element.children;

      element.style.zIndex = '';

      for (let i = 0; i < children.length; i++) {
        if (i === 1) {
          children[i].style.transform = '';
        } else {
          children[i].style.opacity = '';
        }
      }
    };
  }, []);

  useElementTutorial({
    elementId: 'targetMiningButton',
    arrowRef: arrowRef,
    handleClick: handleClick,
    isAnimationPulse: false,
    arrowPosition: {
      position: 'top-left',
      left: 16,
      top: 40,
    },
    handleNoElement,
    handleStartAction,
  });

  return (
    <div className={styles.tutorial}>
      <div className={styles.info__section}>
        <TutorialInfoSection icon={<PixelEmoji4 />}>
          <Typography
            className={styles.info__text}
            sx={{ fontSize: 14 }}
            dangerouslySetInnerHTML={{ __html: t('Now you can Build') }}
          />
        </TutorialInfoSection>
      </div>

      <div className={`${styles.pointBottomAnim}`} ref={arrowRef}>
        <ArrowTutorial style={{ transform: 'rotate(-90deg) translateX(150%)' }} />
      </div>
    </div>
  );
};

export default UpgradeStorageScreen;
