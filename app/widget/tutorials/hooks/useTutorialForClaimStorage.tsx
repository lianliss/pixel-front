import { useEffect } from 'react';
import { cloudStorage } from 'utils/storage.ts';
import { useUnit } from 'effector-react';
import {
  $tutorialForGettingGasState,
  visibleTutorial,
  startTutorial,
  endTutorial,
  nextStep,
  completedTutorial,
} from '../TutorialForGettingGas/tutorialForGettingGasState.ts';
import { useNavigate } from 'react-router-dom';
import {
  tutorialForClaimStorageKey,
  tutorialForGettingGasKey,
} from '../../../shared/const/localStorage.ts';

const defaultTutorialStorageData = {
  isSkipped: false,
  isCompleted: false,
  currentStep: 0,
  restartTimestamp: null,
};

const useTutorialForClaimStorage = (tutorialStorageKey) => {
  const navigate = useNavigate();
  const { isVisible, isStarted, step } = useUnit($tutorialForGettingGasState);

  const onNext = () => {
    nextStep();
  };

  const onVisible = () => {
    visibleTutorial();
  };

  const onStart = () => {
    startTutorial();
  };

  const onSkip = () => {
    startTutorial();
  };

  const setRestartTimestamp = (timestamp: Date) => {
    void cloudStorage.setItem(
      tutorialStorageKey,
      JSON.stringify({
        isSkipped: false,
        isCompleted: false,
        currentStep: step,
        restartTimestamp: timestamp,
      })
    );
  };

  const onEnd = () => {
    endTutorial();
    void cloudStorage.setItem(
      tutorialStorageKey,
      JSON.stringify({
        isSkipped: true,
        isCompleted: false,
        currentStep: 0,
        restartTimestamp: null,
      })
    );
  };

  const onCompleted = () => {
    completedTutorial();
    void cloudStorage.setItem(
      tutorialStorageKey,
      JSON.stringify({
        isSkipped: false,
        isCompleted: true,
        currentStep: 0,
        restartTimestamp: null,
      })
    );
  };

  const resetTutorial = () => {
    void cloudStorage.removeItem(tutorialForClaimStorageKey);
    navigate('/wallet/mining');
    window.location.reload();
  };

  useEffect(() => {
    if (tutorialStorageKey) {
      cloudStorage.getItem(tutorialStorageKey).then((tutorialData) => {
        const { isCompleted, isSkipped, restartTimestamp, currentStep } = tutorialData
          ? JSON.parse(tutorialData)
          : defaultTutorialStorageData;

        if (!(isCompleted || isSkipped)) {
          if (!restartTimestamp) {
            cloudStorage.getItem(tutorialForGettingGasKey).then((tutorialData) => {
              const {isCompleted} = tutorialData
                  ? JSON.parse(tutorialData)
                  : defaultTutorialStorageData;

              if (isCompleted) {
                onVisible();
              }
            });

            return;
          }

          if (restartTimestamp) {
            const currentTime = Date.now();
            const restartTime = new Date(restartTimestamp).getTime();

            if (currentTime >= restartTime) {
              onVisible();
            }
          }
        }
      });
    }
  }, [tutorialStorageKey, startTutorial]);

  return {
    isVisible,
    isStarted,
    onEnd,
    resetTutorial,
    onStart,
    onSkip,
    step,
    onNext,
    onCompleted,
    setRestartTimestamp,
  };
};

export default useTutorialForClaimStorage;
