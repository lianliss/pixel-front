import { useEffect } from 'react';
import { cloudStorage } from 'utils/storage.ts';
import { useUnit } from 'effector-react';
import {
  $tutorialForGettingGasState,
  visibleTutorial,
  startTutorial,
  endTutorial,
  nextStep,
  completedTutorial,
} from '../TutorialForGettingGas/tutorialForGettingGasState.ts';
import { useNavigate } from 'react-router-dom';
import {
  tutorialForClaimStorageKey,
  tutorialForGettingGasKey,
} from '../../../shared/const/localStorage.ts';

const defaultTutorialStorageData = {
  isSkipped: false,
  isCompleted: false,
  currentStep: 0,
  restartTimestamp: null,
};

const useTutorial = (tutorialStorageKey) => {
  const navigate = useNavigate();
  const { isVisible, isStarted, step } = useUnit($tutorialForGettingGasState);

  const onNext = () => {
    nextStep();
  };

  const onVisible = () => {
    visibleTutorial();
  };

  const onStart = () => {
    startTutorial();
  };

  const onSkip = () => {
    startTutorial();
  };

  const onEnd = () => {
    endTutorial();
    void cloudStorage.setItem(
      tutorialForGettingGasKey,
      JSON.stringify({
        isSkipped: true,
        isCompleted: false,
        currentStep: 0,
        restartTimestamp: null,
      })
    );
  };

  const onCompleted = () => {
    completedTutorial();
    void cloudStorage.setItem(
      tutorialForGettingGasKey,
      JSON.stringify({
        isSkipped: false,
        isCompleted: true,
        currentStep: 0,
        restartTimestamp: null,
      })
    );
  };

  const resetTutorial = () => {
    void cloudStorage.removeItem(tutorialForGettingGasKey);
    void cloudStorage.removeItem(tutorialForClaimStorageKey);
    navigate('/wallet');
    window.location.reload();
  };

  useEffect(() => {
    if (tutorialStorageKey) {
      cloudStorage.getItem(tutorialStorageKey).then((tutorialData) => {
        const { isCompleted, isSkipped, currentStep } = tutorialData
          ? JSON.parse(tutorialData)
          : defaultTutorialStorageData;

        if (!isCompleted && !isSkipped) {
          console.log('show tutorial');
          onVisible();
        }
      });
    }
  }, [tutorialStorageKey, startTutorial]);

  return {
    isVisible,
    isStarted,
    onEnd,
    resetTutorial,
    onStart,
    onSkip,
    step,
    onNext,
    onCompleted,
  };
};

export default useTutorial;
