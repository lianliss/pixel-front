import { useEffect, RefObject, useCallback } from 'react';

type ArrowPositionAdjustment = {
  position: 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right';
  left?: number;
  right?: number;
  top?: number;
  bottom?: number;
};

type UseElementTutorialProps = {
  elementId: string;
  arrowRef: RefObject<HTMLDivElement>;
  handleClick: () => void;
  isAnimationPulse: boolean;
  arrowPosition: ArrowPositionAdjustment;
  handleNoElement?: () => void;
  handleStartAction?: () => void;
};

const useElementTutorial = ({
  elementId,
  arrowRef,
  handleClick,
  isAnimationPulse,
  arrowPosition,
  handleNoElement,
  handleStartAction,
}: UseElementTutorialProps) => {
  const handleElementClick = useCallback(() => {
    if (handleClick) {
      handleClick();
    }
  }, [handleClick]);

  const positionArrow = () => {
    const element = document.getElementById(elementId);
    if (element && arrowRef.current) {
      const rect = element.getBoundingClientRect();
      const adjustment = arrowPosition || { position: 'top-left' };

      arrowRef.current.style.position = 'absolute';

      if (adjustment.position.includes('left')) {
        arrowRef.current.style.left = `${rect.left + (adjustment.left || 0)}px`;
      } else if (adjustment.position.includes('right')) {
        arrowRef.current.style.left = `${
          rect.right - arrowRef.current.offsetWidth + (adjustment.right || 0)
        }px`;
      }

      if (adjustment.position.includes('top')) {
        arrowRef.current.style.top = `${
          rect.top - arrowRef.current.offsetHeight + (adjustment.top || 0)
        }px`;
      } else if (adjustment.position.includes('bottom')) {
        arrowRef.current.style.top = `${rect.bottom + (adjustment.bottom || 0)}px`;
      }
    }
  };

  useEffect(() => {
    positionArrow();
    window.addEventListener('resize', positionArrow);

    if (handleStartAction) {
      handleStartAction();
    }

    const element = document.getElementById(elementId);
    if (!element) {
      if (handleNoElement) {
        handleNoElement();
      }
      console.warn(`No element with ID "${elementId}" found.`);
      return;
    }

    element.addEventListener('click', handleElementClick);

    const timeoutId = setTimeout(() => {
      element.style.zIndex = 'var(--z-index-tutorial-content)';

      if (isAnimationPulse) {
        element.classList.add('pulse');
      }
    }, 500);

    return () => {
      element.style.zIndex = '';
      element.classList.remove('pulse');
      element.removeEventListener('click', handleElementClick);
      window.removeEventListener('resize', positionArrow);
      clearTimeout(timeoutId);
    };
  }, [elementId, isAnimationPulse, handleStartAction, handleNoElement, handleElementClick]);
};

export default useElementTutorial;
