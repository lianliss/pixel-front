import React from 'react';

const PixelIcon = () => {
  return (
    <svg
      width='241'
      height='82'
      viewBox='0 0 241 82'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <g filter='url(#filter0_i_2500_88257)'>
        <path
          d='M8 0.999999C8 0.447714 8.44772 0 9 0H232C232.552 0 233 0.447715 233 1V81C233 81.5523 232.552 82 232 82H8.99999C8.44771 82 8 81.5523 8 81V0.999999Z'
          fill='white'
        />
      </g>
      <g filter='url(#filter1_i_2500_88257)'>
        <path
          d='M0 9C0 8.44771 0.447715 8 1 8H240C240.552 8 241 8.44772 241 9V73C241 73.5523 240.552 74 240 74H1C0.447715 74 0 73.5523 0 73V9Z'
          fill='white'
        />
      </g>
      <g filter='url(#filter2_i_2500_88257)'>
        <path
          d='M4 5C4 4.44771 4.44772 4 5 4H236C236.552 4 237 4.44772 237 5V77C237 77.5523 236.552 78 236 78H5C4.44771 78 4 77.5523 4 77V5Z'
          fill='white'
        />
      </g>
      <defs>
        <filter
          id='filter0_i_2500_88257'
          x='8'
          y='0'
          width='225'
          height='82'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feBlend mode='normal' in='SourceGraphic' in2='BackgroundImageFix' result='shape' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
            result='hardAlpha'
          />
          <feOffset dy='1' />
          <feComposite in2='hardAlpha' operator='arithmetic' k2='-1' k3='1' />
          <feColorMatrix type='matrix' values='0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.03 0' />
          <feBlend mode='normal' in2='shape' result='effect1_innerShadow_2500_88257' />
        </filter>
        <filter
          id='filter1_i_2500_88257'
          x='0'
          y='8'
          width='241'
          height='66'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feBlend mode='normal' in='SourceGraphic' in2='BackgroundImageFix' result='shape' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
            result='hardAlpha'
          />
          <feOffset dy='1' />
          <feComposite in2='hardAlpha' operator='arithmetic' k2='-1' k3='1' />
          <feColorMatrix type='matrix' values='0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.03 0' />
          <feBlend mode='normal' in2='shape' result='effect1_innerShadow_2500_88257' />
        </filter>
        <filter
          id='filter2_i_2500_88257'
          x='4'
          y='4'
          width='233'
          height='74'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feBlend mode='normal' in='SourceGraphic' in2='BackgroundImageFix' result='shape' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
            result='hardAlpha'
          />
          <feOffset dy='1' />
          <feComposite in2='hardAlpha' operator='arithmetic' k2='-1' k3='1' />
          <feColorMatrix type='matrix' values='0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.03 0' />
          <feBlend mode='normal' in2='shape' result='effect1_innerShadow_2500_88257' />
        </filter>
      </defs>
    </svg>
  );
};

export default PixelIcon;
