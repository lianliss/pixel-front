import React from 'react';

const PixelHelloIcon = () => {
  return (
    <svg
      width='100'
      height='100'
      viewBox='0 0 100 100'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      xmlnsXlink='http://www.w3.org/1999/xlink'
    >
      <rect width='100' height='100' fill='url(#pattern0_2494_84376)' />
      <defs>
        <pattern
          id='pattern0_2494_84376'
          patternContentUnits='objectBoundingBox'
          width='1'
          height='1'
        >
          <use xlinkHref='#image0_2494_84376' transform='scale(0.01)' />
        </pattern>
        <image
          id='image0_2494_84376'
          width='100'
          height='100'
          xlinkHref='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAED0lEQVR4nO2cQW7aQBSGseQrVKrCMVjAvtuSXiLqtgfIIrDIAbqteomSbrOHRa7QXapIPYSbAE/yvHjwGzP2/GP+bxEHiIHx//55/9iBckKgKFO/AeJCQcCgIGBQEDAoCBgUBAwKAgYFAYOCgEFBwKAgYFAQMCgIGBQEDAoCBgUBY0yCVOp2keRdDCCIHqgesO9x/XfkQhxSvf34s9lvJv+eDnfO18XhjswK45Qg+wHt7txxbZ//7rfffk4bH9fUDkwWByQ1OTukevvx/ebZccaH2WGbq2OCBVlMrxodo+8nAwmCxkIVgnaKdszDxp1y0RxTF0S/wZMOkN7xOhU4t0k8QXKhshSAdoqwvD4U1uJoEB1SUjum9A2w9kb3WzpgOEFyoaqnKivaKdJDBJmKpeBSO8YriO4Zvl5y/fku9DX1QKGaampyckhjQXTdXwpMtvpxHVpgBJEp4vHlx367mK6cx29nK+t6xJly9N9xRW8UBIAqRqh4l7o8rUg7Rgqo1lOKvgUp6hWqBy6VrJ2haah45zZTml0QVKouqcrqFFmP6NQ1GkH0usXXK3z7kciCoKYq6/O3FcZQvaQuyMle4uP+ye0t1v0a4mRh2nHkIDqkGiIEWFMXvCBibVmXyDrkDOiMcwTpkaqPVBU7dfXdS5oEMa1LHl+6vWCtd9AZRkFGnaqsr58qjncWJLR30Bn5OKRxahya0NTVVy85JcjJXmKFK/F4gowyVaGf6+pNEK7EM+0hbalqHumKnVxqDg0jbakrdi+xCOL0kgC4zsjEIaaQcH88aVlVtjooCld/335fliuTU1Kd6woRhBU/MoeYUtX82DOszgjl18PKcZT1PyDbUlesXpK8qZPEgsQ6V1WonjEUfacuOgSM8lJX5KEMlbroEDDKoZxh7R27Y+qR9YKkondP3jGFyfPK64RiTV1yZmDzex3US+gQMErUVHV7XEnHTlNdnRGauj59/LrfbibroOejQ8Ao0VPVDvTDpNbUFdpL6BAwytSpCoVtxy9AaEtdob2EDgEjuiC5OUPwfWTP6qzl7Opk6rL2EjoEjLKvVNV1Tk6FrHvkeow13cn4pHdIL9Gpy9pL6BAwogmi585cnCFoZ8g1fals63jEKb5e0na9hA4Bo4zVO3QlpGarPoTahu6B0lPO/YaI0NRFh1zKOkRXyNAs1HeW9IUep2zlfjnXtZjo49CcuugQMJiyzkQ7Qm5b0b2EDgEj+ldrtD0eWkG5oY+DnA2WdU0bdAgY5whS1FecvnNZwlidsfXMDDLeNme89g75lesQRGJMWY5TSLfjJ7CHgBFTEH6gJwJ0CBgUBAwKAgYFAYOCgEFBwKAgYFAQMCgIGBQEDAoCBgUBg4KAQUHAoCBgUBAw/gP1HeExtrOGuwAAAABJRU5ErkJggg=='
        />
      </defs>
    </svg>
  );
};

export default PixelHelloIcon;
