import React from 'react';
import styles from './styles.module.scss';
import Typography from '@mui/material/Typography';
import { Button } from '@ui-kit/Button/Button.tsx';
import { useTranslation } from 'react-i18next';

type Props = {
  closeModal: () => void;
  startTutorial: () => void;
};

const TutorialConfirmDialog = ({ closeModal, startTutorial }: Props) => {
  const { t } = useTranslation('tutorial');

  return (
    <div className={styles.confirm}>
      <Typography fontWeight='bold' sx={{ fontSize: 16 }}>
        {t('Do you want to take the training?')}
      </Typography>
      <Typography medium sx={{ fontSize: 12, color: '#7EA1CA' }}>
        {t('You can always restart it in the settings.')}
      </Typography>

      <div className={styles.btns}>
        <Button size='large' variant='outlined' fullWidth onClick={closeModal}>
          <Typography noWrap sx={{ fontSize: 14 }}>
            {t('Skip')}
          </Typography>
        </Button>
        <Button size='large' variant='contained' fullWidth onClick={startTutorial}>
          <Typography noWrap sx={{ fontSize: 14 }}>
            {t('Take the training')}
          </Typography>
        </Button>
      </div>
    </div>
  );
};

export default TutorialConfirmDialog;
