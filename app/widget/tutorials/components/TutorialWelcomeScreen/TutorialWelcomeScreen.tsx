import React, { memo } from 'react';
import Typography from '@mui/material/Typography';
import PixelHelloIcon from '../icon/PixelHelloIcon.tsx';
import TutorialConfirmDialog from './TutorialConfirmDialog/TutorialConfirmDialog.tsx';
import styles from './styles.module.scss';
import { useTranslation } from 'react-i18next';
import TutorialInfoSection from '../TutorialInfoSection/TutorialInfoSection.tsx';
import { PixelEmoji1 } from '../PXLEmoji/PixelEmoji.tsx';

type Props = {
  closeModal: () => void;
  startTutorial: () => void;
};

const TutorialWelcomeScreen = ({ closeModal, startTutorial }: Props) => {
  const { t } = useTranslation('tutorial');

  return (
    <div className={styles.tutorial__screen}>
      <div className={styles.info__section} style={{ bottom: '45%' }}>
        <TutorialInfoSection icon={<PixelEmoji1 />}>
          <Typography className={styles.info__text} sx={{ fontSize: 14 }}>
            <span>
              {t('Hello my friend. I’m Pixel.')}
              <br />
              {t('Welcome to my PIXEL World.')}
            </span>
          </Typography>
        </TutorialInfoSection>
      </div>

      <div className={styles.confirm__section}>
        <TutorialConfirmDialog closeModal={closeModal} startTutorial={startTutorial} />
      </div>
    </div>
  );
};

export default memo(TutorialWelcomeScreen);
