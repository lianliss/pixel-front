import React from 'react';

import Emoji from './PXLEmoji_1.png';
import Emoji2 from './PXLEmoji_2.png';
import Emoji3 from './PXLEmoji_3.png';
import Emoji4 from './PXLEmoji_4.png';
import Emoji5 from './PXLEmoji_5.png';
import Emoji6 from './PXLEmoji_6.png';
import Emoji7 from './PXLEmoji_7.png';

export const PixelEmoji1 = () => {
  return <img style={{ width: 87, height: 65 }} src={Emoji} alt='PixelEmoji1' />;
};

export const PixelEmoji2 = () => {
  return <img style={{ width: 87, height: 65 }} src={Emoji2} alt='PixelEmoji2' />;
};

export const PixelEmoji3 = () => {
  return <img style={{ width: 87, height: 65 }} src={Emoji3} alt='PixelEmoji3' />;
};

export const PixelEmoji4 = () => {
  return <img style={{ width: 87, height: 65 }} src={Emoji4} alt='PixelEmoji4' />;
};

export const PixelEmoji5 = () => {
  return <img style={{ width: 87, height: 65 }} src={Emoji5} alt='PixelEmoji5' />;
};

export const PixelEmoji6 = () => {
  return <img style={{ width: 87, height: 65 }} src={Emoji6} alt='PixelEmoji6' />;
};

export const PixelEmoji7 = () => {
  return <img style={{ width: 87, height: 65 }} src={Emoji7} alt='PixelEmoji7' />;
};
