import React, { ReactElement, ReactNode } from 'react';

import styles from './styles.module.scss';
import TextCloud from '../icon/TextCloud.tsx';

type Props = {
  children: ReactNode;
  icon?: ReactElement;
  position?: 'start' | 'end' | 'center';
};
const TutorialInfoSection = ({ children, icon, position = 'center' }: Props) => {
  return (
    <div className={styles.info__aria} style={{ justifyContent: position }}>
      {icon && icon}
      <div className={styles.info__cloud}>
        <TextCloud />
        <div className={styles.text__cloud}>{children}</div>
      </div>
    </div>
  );
};

export default TutorialInfoSection;
