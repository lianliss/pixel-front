import {
  tutorialForClaimStorageKey,
  tutorialForGettingGasKey,
} from '../../shared/const/localStorage.ts';
import GoToQuestsScreen from './TutorialForGettingGas/screens/GoToQuestsScreen/GoToQuestsScreen.tsx';
import CompleteQuestScreen from './TutorialForGettingGas/screens/CompleteQuestScreen/CompleteQuestScreen.tsx';
import CheckQuestScreen from './TutorialForGettingGas/screens/CheckQuestScreen/CheckQuestScreen.tsx';
import UpgradeStorageScreen from './TutorialForClaimStorage/screens/UpgradeStorageScreen/UpgradeStorageScreen.tsx';
import TutorialInfoScreen from './TutorialForClaimStorage/screens/TutorialInfoScreen/TutorialInfoScreen.tsx';
import {
  PixelEmoji2,
  PixelEmoji3,
  PixelEmoji5,
  PixelEmoji6,
} from './components/PXLEmoji/PixelEmoji.tsx';

export const tutorialQuestId = 262;
export const configTutorials = {
  [tutorialForGettingGasKey]: {
    steps: [
      {
        target: '#GoToQuestsScreenTarget',
        content: <GoToQuestsScreen target='#GoToQuestsScreenTarget' icon={<PixelEmoji2 />} />,
      },
      {
        target: '#CompleteQuestScreenTarget',
        content: <CompleteQuestScreen target='#CompleteQuestScreenTarget' icon={<PixelEmoji3 />} />,
      },
      {
        target: '#CheckQuestScreenTarget',
        content: <CheckQuestScreen target='#CheckQuestScreenTarget' icon={<PixelEmoji3 />} />,
      },
    ],
  },
  [tutorialForClaimStorageKey]: {
    steps: [
      {
        target: '',
        content: <TutorialInfoScreen contentKey='Congratulations' icon={<PixelEmoji5 />} />,
      },
      {
        target: '',
        content: <TutorialInfoScreen contentKey='Every blockchain' icon={<PixelEmoji6 />} />,
      },
      {
        target: '',
        content: <TutorialInfoScreen contentKey='A transaction' icon={<PixelEmoji2 />} />,
      },
      {
        target: '',
        content: <TutorialInfoScreen contentKey='You can always' icon={<PixelEmoji2 />} />,
      },
      {
        target: 'miningButtons_id',
        content: <UpgradeStorageScreen target='miningButtons_id' />,
      },
    ],
  },
};
export const TUTORIAL_ENABLED = true;
