import { createStore, createEvent } from 'effector';
import {IS_MOBILE, IS_MOBILE_RELATIVE_SIDEBAR} from "@cfg/app.ts";

export const toggleSidebar = createEvent();
export const closeSidebar = createEvent();
export const collapseSidebar = createEvent();
export const hideSidebar = createEvent();

type SidebarState = {
  mode: 'permanent' | 'temporary';
  openState: boolean;
};

const initialStore: SidebarState = {
  mode: IS_MOBILE ? IS_MOBILE_RELATIVE_SIDEBAR ? 'permanent' : "temporary" : "permanent",
  openState: !IS_MOBILE,
};

export const $sidebarState = createStore(initialStore)
  .on(toggleSidebar, (state) => ({ ...state, openState: !state.openState, mode: IS_MOBILE ? (IS_MOBILE_RELATIVE_SIDEBAR ? (!state.openState ? 'temporary' : "permanent" ): 'temporary') : 'permanent' }))
  .on(closeSidebar, (state) => ({ ...state, openState: false, mode: IS_MOBILE ? IS_MOBILE_RELATIVE_SIDEBAR ? 'permanent' : "temporary" : 'permanent' }))
  // .on(collapseSidebar, (state) => ({ ...state, mode: 'temporary' }))
  // .on(hideSidebar, (state) => ({
  //   ...state,
  //   mode: state.mode === 'temporary' ? 'permanent' : 'temporary',
  // }));
