import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiDrawer from '@mui/material/Drawer';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import {
  $sidebarState,
  closeSidebar,
  collapseSidebar,
  hideSidebar,
  toggleSidebar,
} from './sidebar.store.ts';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import {
  EDITOR_ACCOUNTS,
  IS_DEVELOP,
  IS_DEVELOP_FEATURE,
  IS_STAGE,
  IS_TELEGRAM,
  PROJECT_VERSION,
} from '@cfg/config.ts';
import routes, { IRoute } from '../../const/routes.tsx';
import { useAccount } from '@chain/hooks/useAccount.ts';
import {
  IS_BOUNTY,
  IS_BRIDGE, IS_FORGE_CRAFT,
  IS_FORGE_PAGE,
  IS_LANG,
  IS_LOTTERY,
  IS_MOBILE,
  IS_MOBILE_RELATIVE_SIDEBAR
} from '@cfg/app.ts';
import {LangSwitcher} from '../LangSwitcher/LangSwitcher.tsx';
import { drawerWidth, getLeftDrawerStyles, getRightDrawerStyles } from './utils.ts';
import {Link, useNavigate} from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import includes from 'lodash/includes';
import QueuePlayNextIcon from '@mui/icons-material/QueuePlayNext';
import clsx from 'clsx';

import styles from './styles.module.scss';
import SvgIcon from '@mui/material/SvgIcon';
import { classNames } from '../../shared/lib/classNames.ts';
import useTutorial from '../tutorials/hooks/useTutorial.tsx';
import ChainSwitcher from '../ChainSwitcher/ChainSwitcher.tsx';
import { useEffect } from 'react';
import {useUnit} from "effector-react";
import Logo from "./Logo";
import {resetTutorials} from "../tutorial/utils/reset-tutorial.ts";
import LogoutIcon from "@mui/icons-material/Logout";
import {useDisconnect} from "@chain/hooks/useDisconnect.ts";

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  ...theme.mixins.toolbar,
}));

const Drawer = styled(MuiDrawer, {
  // shouldForwardProp: (prop) => prop !== 'open',
})(({ theme }) => ({
  flexShrink: 0,
  whiteSpace: 'nowrap',
  boxSizing: 'border-box',

  '& .MuiDrawer-paper': {
    width: drawerWidth,
  },

  '&.MuiDrawer-docked': {
    width: drawerWidth,

    '&.collapsed': {
      width: 57,
    }
  }
}));

const label = { inputProps: { 'aria-label': 'Hide sidebar' } };

interface IMenuSection {
  isAvailable: (account: any) => boolean;
  menu: IRoute[];
}

const sections: IMenuSection[] = [
  {
    isAvailable: (account) => true,
    menu: [
      routes.myProfile,
      routes.walletFriends,
      routes.wallet,
      routes.walletMining,
      routes.inventory,
      routes.walletQuests,
      ...(IS_FORGE_PAGE ? [routes.forgeClaimer] : []),
      ...(IS_BRIDGE ? [routes.bridge] : []),
    ],
  },
  {
    isAvailable: (account) => true,
    menu: [
      // ...(IS_DEVELOP_FEATURE ? [routes.dex] : []),
      routes.dashboard,
      routes.nftMarket,
      routes.leaderboards,
      routes.achievements,
      ...(IS_LOTTERY ? [routes.lotteryList] : []),
      routes.digitalSoul,
      routes.history,

      routes.admin,
    ],
  },
  {
    isAvailable: (account) => EDITOR_ACCOUNTS.includes(account?.accountAddress?.toLowerCase()),
    menu: [routes.adminNft,routes.adminBlender, routes.adminChests, routes.adminChestSales,  routes.adminQuest, routes.adminBounty, routes.adminLottery],
  },
  {
    isAvailable: (account) => true,
    menu: [routes.collection],
  },
  {
    isAvailable: (account) => true,
    menu: [routes.docs, ...(IS_BOUNTY ? [routes.bounty] : []),  routes.preseed,  routes.walletSettings],
  },
];

const getAvailableMenus = (account) =>
  sections.reduce((acc, section) => {
    if (section.isAvailable(account)) {
      return [...acc, ...section.menu];
    }
    return acc;
  }, []);

export const Sidebar = ({ match }) => {
  const theme = useTheme();
  const account = useAccount();
  const navigate = useNavigate();

  const { resetTutorial } = useTutorial('');
  const { t } = useTranslation('menu');

  const { openState: open, mode } = useUnit($sidebarState);
  const isPermanent = mode === 'permanent';
  const hidden = mode === 'hidden';
  const isCompact = (isPermanent && !open);

  const handleClick = (item) => (e) => {
    e.preventDefault();

    navigate(item.path);

    if (IS_MOBILE) {
      closeSidebar();
    }

    Telegram.WebApp.HapticFeedback.impactOccurred('medium');

    if (typeof item.onClick === 'function') {
      return item.onClick(account);
    }
  };

  const availableSections = sections.filter((section) => section.isAvailable(account));
  const menus = getAvailableMenus(account);
  const currentPath = match?.pattern?.path;

  const activeMenu = menus.find((menu) => menu.path === currentPath);
  const matchMenus = currentPath ? menus.filter((menu) => includes(currentPath, menu.path)) : [];
  const lastMatchingMenu = matchMenus[matchMenus.length - 1] || {};

  const getIsActive = (path) => {
    return activeMenu ? path === activeMenu.path : path === lastMatchingMenu.path;
  };

  // useEffect(() => {
  //   if (currentPath.includes('inventory')) {
  //     hideSidebar();
  //   }
  // }, [currentPath]);

  const onChangeHideSidebar = () => {
    closeSidebar();
    hideSidebar();
  };

  return (
    <Box sx={{ display: 'flex', height: '100%', width: IS_MOBILE && IS_MOBILE_RELATIVE_SIDEBAR ? 57 : undefined }} className={styles.sidebar}>
      <Drawer
        variant={isPermanent ? 'permanent' : 'temporary'}
        open={open}
        anchor='left'
        onClose={() => closeSidebar()}
        sx={{
          height: '100%',
          ...getLeftDrawerStyles(theme, open, isPermanent, hidden)
        }}
        className={clsx({ collapsed: isPermanent && !open })}
        PaperProps={{
          className: styles.paper,
        }}
      >
        <DrawerHeader sx={{ justifyContent: open ? 'space-between' :  'flex-start' }}>
          <Logo
              style={{ width: '80%'}}
              onClick={() => {
                navigate(routes.wallet.path);
              }}
          />

          <IconButton onClick={() => toggleSidebar()}>
            {open && <ChevronLeftIcon />}
            {!open && <ChevronRightIcon />}

          </IconButton>
        </DrawerHeader>
        <Divider />


        {IS_LANG && (
            <>
            <ListItem disablePadding sx={{ px:isCompact ? 0 :  2, py: 1 }}>
              <LangSwitcher compact={isCompact}/>
            </ListItem>
              <Divider />
            </>
        )}

        {IS_MOBILE && IS_MOBILE_RELATIVE_SIDEBAR && <Box sx={{flexGrow: 1, padding: 1}}>
          <ChainSwitcher isRelative={true}/>
        </Box>}
        {IS_MOBILE && IS_MOBILE_RELATIVE_SIDEBAR && <Divider/>}

        {availableSections.map((section, index) => (
            <React.Fragment  key={`list-${index}`}>
              {index > 0 && <Divider/>}
          <List>
            {section.menu.map((item, itemIndex) => {
              if (typeof item.isAvailable === 'function' && !item.isAvailable(account) || typeof item.isSidebar === 'function' && !item.isSidebar(account)) return null;
              // if (item.disabled) return null;
              const path = item?.path;
              const isActive = getIsActive(path);
              const Icon = item.svg;

              if (item.link) {
                return (
                  <ListItem key={itemIndex} disablePadding>
                    <Link to={item.disabled ? '#' : item.link} target='_blank' style={{ width: '100%' }}>
                      <ListItemButton disabled={item.disabled}>
                        <ListItemIcon>
                          <SvgIcon color='secondary'>
                            <Icon/>
                          </SvgIcon>
                        </ListItemIcon>
                        {!isCompact && <ListItemText primary={item.title} sx={{color: 'secondary.main'}}/>}
                      </ListItemButton>
                    </Link>
                  </ListItem>
                );
              }

              return (
                <ListItem key={itemIndex} disablePadding>

                    <ListItemButton disabled={item.disabled} selected={isActive} onClick={handleClick(item)}>
                      <ListItemIcon
                          sx={{ color: 'secondary.main' }}
                      >
                        {/*<SvgIcon color={isActive ? 'primary' : 'secondary'}>{item.svg}</SvgIcon>*/}
                        <SvgIcon>
                          <Icon/>
                        </SvgIcon>
                      </ListItemIcon>
                      {!isCompact && <ListItemText primary={t(item.title)} sx={{color: 'secondary.main'}}/>}
                    </ListItemButton>
                </ListItem>
              );
            })}
          </List>
            </React.Fragment>
        ))}

        {/*{(IS_STAGE || IS_DEVELOP) && (*/}
        {/*  <ListItem disablePadding sx={{ pl: 2 }}>*/}
        {/*    <ListItemIcon  sx={{ color: 'secondary.main' }}>*/}
        {/*      <LangSwitcherMini />*/}
        {/*    </ListItemIcon>*/}
        {/*    <ListItemText primary={t('Language')} sx={{ color: 'secondary.main' }} />*/}
        {/*  </ListItem>*/}
        {/*)}*/}

        {/*<List>*/}
        {/*  <ListItem disablePadding>*/}
        {/*    <ListItemButton sx={{ margin: 0 }} onClick={() => onChangeHideSidebar()}>*/}
        {/*      <ListItemIcon>*/}
        {/*        <Switch {...label} defaultChecked size='small' checked={!isPermanent} />*/}
        {/*      </ListItemIcon>*/}
        {/*      <ListItemText primary={t('Hide')} />*/}
        {/*    </ListItemButton>*/}
        {/*  </ListItem>*/}
        {/*</List>*/}
        {/*<Divider />*/}

        {/*{account.isConnected && <ListItem disablePadding>*/}
        {/*  <ListItemButton onClick={handleLogout}>*/}
        {/*    <ListItemIcon sx={{color: 'secondary.main'}}>*/}
        {/*      <SvgIcon>*/}
        {/*        <LogoutIcon/>*/}
        {/*      </SvgIcon>*/}
        {/*    </ListItemIcon>*/}
        {/*    <ListItemText primary={t('Logout')} sx={{color: 'secondary.main'}}/>*/}
        {/*  </ListItemButton>*/}
        {/*</ListItem>}*/}

        <ListItem disablePadding>
          <ListItemButton onClick={() => {
            resetTutorial();
            resetTutorials();
          }}>
            <ListItemIcon sx={{ color: 'secondary.main' }}>
              <SvgIcon>
                <QueuePlayNextIcon />
              </SvgIcon>
            </ListItemIcon>
            {!isCompact && <ListItemText primary={t('Reset Tutorial')} sx={{color: 'secondary.main'}}/>}
          </ListItemButton>
        </ListItem>

        <ListItem disablePadding sx={{ pb: 2 }}>
          <ListItemButton>
              <span>{isCompact ? '' : 'version:'} {PROJECT_VERSION}</span>
          </ListItemButton>
        </ListItem>
      </Drawer>
    </Box>
  );
};
