import { CSSObject, Theme } from '@mui/material/styles';

export const drawerWidth = 250;

const openedMixin = (theme: Theme): CSSObject => ({
  width: drawerWidth,
  overflowX: 'hidden',
});

const rolledUpMixin = (theme: Theme): CSSObject => ({
  overflowX: 'hidden',
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up('sm')]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const closedMixin = (theme: Theme): CSSObject => ({
  overflowX: 'hidden',
  width: 0,
});

export const getLeftDrawerStyles = (theme, open, collapsed, hidden) => ({
  ...(open && collapsed && { '& .MuiDrawer-paper': openedMixin(theme) }),
  ...(!open && collapsed && { '& .MuiDrawer-paper': rolledUpMixin(theme) }),
  ...(open && hidden && { '& .MuiDrawer-paper': openedMixin(theme) }),
  ...(!open && hidden && { '& .MuiDrawer-paper': closedMixin(theme) }),
});

export const getRightDrawerStyles = (theme, open, collapsed, hidden) => ({
  ...(open &&
    collapsed && { '& .MuiPaper-root': { ...openedMixin(theme), } }),
  ...(!open &&
    collapsed && { '& .MuiDrawer-paper': { ...rolledUpMixin(theme), } }),
  ...(open &&
    hidden && { '& .MuiDrawer-paper': { ...openedMixin(theme), } }),
  ...(!open &&
    hidden && { '& .MuiDrawer-paper': { ...closedMixin(theme), } }),
});
