import React from 'react';

const Lock = (props) => (
    <svg width="24" height="27" viewBox="0 0 24 27" fill="none" xmlns="http://www.w3.org/2000/svg"{ ...props}>
        <path fillRule="evenodd" clipRule="evenodd"
              d="M7 9C3.13401 9 0 12.134 0 16V20C0 23.866 3.13401 27 7 27H17C20.866 27 24 23.866 24 20V16C24 12.134 20.866 9 17 9H7ZM10 17C10 15.8954 10.8954 15 12 15C13.1046 15 14 15.8954 14 17C14 17.5925 13.7424 18.1248 13.3331 18.491V20.3333C13.3331 21.0697 12.7361 21.6667 11.9997 21.6667C11.2633 21.6667 10.6664 21.0697 10.6664 20.3333V18.4905C10.2574 18.1243 10 17.5922 10 17Z"
              fill="white"/>
        <path d="M17 9V6C17 3.23858 14.7614 1 12 1V1C9.23858 1 7 3.23869 7 6.00011C7 7.03642 7 8.02972 7 9"
              stroke="white" strokeWidth="2"/>
    </svg>
);

export default Lock;
